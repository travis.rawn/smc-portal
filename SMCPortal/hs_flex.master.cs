﻿using SensibleDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class hs_flex : System.Web.UI.MasterPage
{
    //MembershipUser CurrentUser;
    SM_Tools tools = new SM_Tools();
    //SMCPortalDataContext PDC = new SMCPortalDataContext();
    // Rep SensibleRep = new Rep();
    //RzTools rzt = new RzTools();
    public sm_crypto smcrypto = new sm_crypto();
    string returnUrl;
    //public string showingCompany { get { return lblShowingCompany.Text; } set { lblShowingCompany.Text = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SM_Security.CheckForBlockedIP();
            UpdateDescriptionMetaTag();
            NetworkLogic.SetSessionID(HttpContext.Current);
            //CurrentUser = SM_Security.LoadCurrentUser();
            HideMenuItems();
            //ShowHideTitleDiv();
            HandleLoginControls();
            LoadCopyright();
            LoadCompanyLabel();
            if (SM_Global.CurrentUser != null)
            {
                //If problem user, they should get stopped here
                if (SM_Security.HandleUserSecurityValidation())
                {
                    LoadMenuItems(true);
                    //Return URL path
                    returnUrl = Tools.Strings.SanitizeInput(Request.QueryString["ReturnUrl"]);
                    if (!string.IsNullOrEmpty(returnUrl))
                        Response.Redirect(returnUrl, false);
                }
                else
                    throw new Exception("Sorry, this account is currently not authorized for access.  Please contact your Sensible Micro representative for a resolution.");
            }
            tools.LoadSuccessQueryString(Request);
        }
        catch (Exception ex)
        {
            string error = ex.Message;
            //Careful showing errors here.  It can divulge sensitive info
            //tools.HandleResult("fail", ex.Message);

        }


    }


    public void LoadCompanyLabel()
    {
        lblShowingCompany.Visible = false;

        if (SM_Global.CurrentUser == null)
            return;

        //Only show label for Admin, internal, and demo users.
        if (!tools.IsInAnyRole(SM_Global.CurrentUser.UserName, new List<string>() { "sm_internal", "admin" }) && !SM_Global.isDemoUser)
            return;

        //Internal / Admin Users / Demo Users
        lblShowingCompany.Visible = true;
        if (SM_Global.isDemoUser)
            lblShowingCompany.Text = "Demo User: [Demo Company]";
        else if (SM_Global.PreviewCompany != null)
            lblShowingCompany.Text = SM_Global.PreviewUser.UserName + ": [" + SM_Global.PreviewCompany.companyname + "]";
        else if (SM_Global.CurrentCompany != null)
            lblShowingCompany.Text = SM_Global.CurrentUser.UserName + ": [" + SM_Global.CurrentCompany.companyname + "]";
    }
    public void ToggleTitleDiv(bool show)
    {
        divTitle.Visible = show;
    }

    public void ToggleBannerDiv(bool show)
    {
        divBanner.Visible = show;
    }

    public void UpdateDescriptionMetaTag(string description = null)
    {
        string defaultDescription = "The Sensible Micro Portal.  Here customers can view Inspections, order history,  as well as access our library of tools and services for component intelligence, BOM Management / forecasting, surplus liquidation and much more.";
        if (string.IsNullOrEmpty(Page.MetaDescription))
            Page.MetaDescription = defaultDescription;


    }


    private void HandleLoginControls()
    {
        bool isUserLoggedIn = HttpContext.Current.User.Identity.IsAuthenticated;
        liLogout_Nav.Visible = isUserLoggedIn;
        liLogin_Nav.Visible = !isUserLoggedIn;
        liLogout_Footer.Visible = isUserLoggedIn;
        liLogin_Footer.Visible = !isUserLoggedIn;
        liRegister.Visible = !isUserLoggedIn;

    }

    private void LoadCopyright()
    {
        lblCopyright.InnerText = "© " + DateTime.Today.Year.ToString() + " Sensible Micro Corporation ";
    }


    private void HideMenuItems()
    {
        LoadMenuItems(false);
    }


    protected void LoadMenuItems(bool show)
    {

        if (!HttpContext.Current.User.Identity.IsAuthenticated)
        {
            navPortal.Visible = false;
            return;
        }
        
        if (Page.User.IsInRole(SM_Enums.PortalRole.sm_internal.ToString()))
            ShowInternal();
        
        if (Page.User.IsInRole("admin"))
            ShowAdmin();

        if (Page.User.IsInRole(SM_Enums.PortalRole.sm_warehouse.ToString()))
            ShowWarehouse();

    }



    private void ShowInternal()
    {
        mnuInternal.Visible = true;
        spcrResources.Visible = true;

    }

    private void ShowWarehouse()
    {
        mnuInventoryUpdater.Visible = true;
    }

    private void ShowAdmin()
    {
        mnuAdmin.Visible = true;
    }




    protected void lbLogout_Click(object sender, EventArgs e)
    {
        SM_Security.Logout();
        Response.Redirect("~/login.aspx", false);
    }

    protected void btnNavSearch_Click(object sender, EventArgs e)
    {
        DoNavSearch();
    }

    private void DoNavSearch()
    {
        string strSearch = txtSearchParts.Value.Trim().ToUpper();
        strSearch = HttpUtility.UrlEncode(strSearch);
        Response.Redirect("/public/search/part_search.aspx?pn=" + strSearch, false);
    }

    protected void lbNavSearch_Click(object sender, EventArgs e)
    {
        DoNavSearch();
    }


}
