﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="secure_members_Default_f" %>

<asp:Content ID="Content0" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="TitleContent" runat="server">
    <h1 class="relative">Sensible Micro Portal</h1>
    <div class="paragraph-large ">
        <p>
            Welcome to the Sensible Micro Portal. 
        </p>
    </div>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="section-container padding-top__medium padding-bottom__small section-header__container">
        <div class="portal-card-grid">

            <a href="/public/search/part_search.aspx" class="portal-card portal-card-grid__item">
                <img src="https://www.sensiblemicro.com/hubfs/icon__search__lblue.svg" alt="" class="portal-card__icon">
                <h2 class="portal-card__title">Preferred Parts Search
                </h2>
                <div class="portal-card__desc">
                    <p>Gain component intelligence such as Datasheet, PDN, Environmental Data as well as view our preferred market available stock.</p>
                </div>
            </a>

            <a href="/secure_members/buyers/Orders.aspx" class="portal-card portal-card-grid__item">
                <img src="https://www.sensiblemicro.com/hubfs/parts-icon.svg" alt="" class="portal-card__icon">
                <h2 class="portal-card__title">Order Center
                </h2>
                <div class="portal-card__desc">
                    <p>Track your orders, view their status, and cross reference them with our component intelligence platform for Datasheet, PDN, and other useful data.</p>
                </div>
            </a>

            <a href="/secure_members/qc_center/my_qc.aspx" class="portal-card portal-card-grid__item">
                <img src="https://www.sensiblemicro.com/hubfs/check.svg" alt="" class="portal-card__icon">
                <h2 class="portal-card__title">Quality Center
                </h2>
                <div class="portal-card__desc">
                    <p>View your IDEA 1010 Inspection reports, including hi-res imagery captured in our state of the art in-house component inspection facility.</p>
                </div>
            </a>

        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="SubMain" runat="Server">
    <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
        <div id="hs_cos_wrapper_module_1557320680122665" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">

            <div class="background__blue offer-section">

                <div class="section-container__narrow padding-top__large padding-bottom__medium section-header__container">
                    <div class="main-content align__center">
                        <h2 class="relative">The Sensible Approach
                        </h2>
                        <div class="paragraph-large">
                            <p>Are you looking for the perfect soucing solution? Request a quote today to find the parts you need.</p>
                            <p><a href="https://info.sensiblemicro.com/request-quote" target="_blank">Request a Quote</a></p>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!--end widget-span -->
</asp:Content>
