﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="my_account.aspx.cs" Inherits="secure_members_my_account" %>

<%@ MasterType VirtualPath="~/hs_flex.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        label {
            font-weight: bold;
            font-size: 16px;
        }

        .accountImage {
            max-height: 300px;
        }

        .title {
            font-size: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-6">

            <label class="title">View / Update your Profile Settings:</label>

            <div class="form-group row">
                <label for="lblUserName" class="col-sm-3 col-form-label">User Name:</label>
                <div class="col-sm-9">
                    <asp:Label ID="lblUserName" runat="server"></asp:Label>
                </div>
            </div>

            <div class="form-group row">
                <label for="txtFirstName" class="col-sm-3 col-form-label">First Name:</label>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <label for="txtLastName" class="col-sm-3 col-form-label">Last Name:</label>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <label for="txtLastName" class="col-sm-3 col-form-label">Email:</label>
                <div class="col-sm-9">
                    <asp:Label ID="lblEmailAddress" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form-group row">
                <label for="txtLastName" class="col-sm-3 col-form-label">Company:</label>
                <div class="col-sm-9">
                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="/Account/password_change.aspx">Change Your Password</a>
                </div>

            </div>




        </div>
        <div class="col-sm-6">
            <asp:Panel ID="pnlCurrentRep" Visible="false" runat="server">
                <label class="title">Your Rep: </label>
                <em><a href="mailto:sales@sensiblemicro.com" id="aRepEmail" runat="server" target="_blank"></a></em>
                <img id="imgHeadshot" runat="server" class="img-responsive accountImage" src="#" alt="" />
            </asp:Panel>

        </div>
    </div>
    <asp:Panel ID="pnlInternal" runat="server" Visible="false">
        <div class="row">
            <div class="col-sm-12">
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <label class="title">Preview the portal as:</label>
                <asp:DropDownList ID="ddlPortalPreviewUser" runat="server" />
                <br />
                <asp:LinkButton ID="lbUpdatePortalPreviewUser" runat="server" OnClick="lbUpdatePortalPreviewUser_Click">Update</asp:LinkButton>
                <asp:LinkButton ID="lbResetPortalPreviewUser" runat="server" OnClick="lbResetPortalPreviewUser_Click">Reset</asp:LinkButton>
                <br />
                <br />

                <asp:UpdatePanel ID="upPreviewProfile" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="lbUpdatePortalPreviewUser" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="lbResetPortalPreviewUser" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>

                        <div class="form-group row">
                            <label for="lblPreviewUserName" class="col-sm-3 col-form-label">User Name:</label>
                            <div class="col-sm-9">
                                <asp:Label ID="lblPreviewUserName" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lblPreviewFirstName" class="col-sm-3 col-form-label">First Name:</label>
                            <div class="col-sm-9">
                                <asp:Label ID="lblPreviewFirstName" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lblPreviewLastName" class="col-sm-3 col-form-label">Last Name:</label>
                            <div class="col-sm-9">
                                <asp:Label ID="lblPreviewLastName" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lblPreviewCompany" class="col-sm-3 col-form-label">Company:</label>
                            <div class="col-sm-9">
                                <asp:Label ID="lblPreviewCompany" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lblPreviewEmail" class="col-sm-3 col-form-label">Email:</label>
                            <div class="col-sm-9">
                                <asp:Label ID="lblPreviewEmail" runat="server"></asp:Label>
                            </div>
                        </div>




                    </ContentTemplate>
                </asp:UpdatePanel>


            </div>
            <div class="col-sm-6">
                <asp:Panel ID="pnlPreviewRep" Visible="false" runat="server">
                    <h4>Preview Rep: </h4>
                    <em><a href="mailto:sales@sensiblemicro.com" id="aPreviewRep" runat="server" target="_blank"></a></em>
                    <img id="imgPreviewRepImage" runat="server" class="img-responsive accountImage" src="#" alt="" />
                </asp:Panel>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

