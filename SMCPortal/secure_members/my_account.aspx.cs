﻿using SensibleDAL.dbml;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_members_my_account : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadProfileData();
            LoadPreviewProfileData();
        }
        //Load the Headshot
        LoadRep();
    }

    private void LoadRep()
    {
        Rep rep = null;
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            if (SM_Global.isDemoUser)
                rep = pdc.Reps.Where(w => w.RzRepID == "24172ac97b0f4d9688ae9945bc64d395").FirstOrDefault();//Joe Mar          
            else
                rep = pdc.Reps.Where(w => w.RzRepID == SM_Global.CurrentProfile.SensibleRepID).FirstOrDefault();
        }

        if (rep == null)
        {
            pnlCurrentRep.Visible = false;
            return;
        }
            
        pnlCurrentRep.Visible = true;

      
        imgHeadshot.Src = SM_Global.GetHeadshotImagePath(rep);//Null = default logo.
        //lblRepName.Text = rep.FullName;
        aRepEmail.InnerText = rep.FullName;
        aRepEmail.HRef = "mailto:" + rep.Email.Trim().ToLower();

    }

    private void LoadProfileData()
    {
        if (SM_Global.isDemoUser)
        {
            lblUserName.Text = "demo@some-email.com";
            txtFirstName.Text = "Demo";
            txtLastName.Text = "User";
            lblCompanyName.Text = "Demo Company";
            lblEmailAddress.Text = "demo@some-email.com";
        }
        else
        {
            lblUserName.Text = SM_Global.CurrentUser.UserName;
            txtFirstName.Text = Profile.FirstName;
            txtLastName.Text = Profile.LastName;
            lblCompanyName.Text = Profile.CompanyName ?? "<Not Set>";
            lblEmailAddress.Text = SM_Global.CurrentUser.Email ?? "<Not Set>";
        }



    }



    private void SaveProfileData()
    {
        Profile.FirstName = txtFirstName.Text;
        Profile.LastName = txtLastName.Text;
    }

    private void LoadPreviewProfileData()
    {
        List<string> previewUsers = new List<string>() { SM_Enums.PortalRole.portal_admin.ToString(), SM_Enums.PortalRole.admin.ToString(), SM_Enums.PortalRole.sm_internal.ToString() };
        if (tools.IsInAnyRole(System.Web.Security.Membership.GetUser().UserName, previewUsers))
        {
            //Set internal Settings panel Visible
            pnlInternal.Visible = true;
            //Fill the Display As Properties
            LoadPreviewProperties();
        }

    }

    private void LoadPreviewProperties()
    {
       


        lblPreviewFirstName.Text = "[Not Set]";
        lblPreviewLastName.Text = "[Not Set]";
        lblPreviewCompany.Text = "[Not Set]";
        lblPreviewUserName.Text = "[Not Set]";
        lblPreviewEmail.Text = "[Not Set]";

        LoadPreviewUsersDDl();
        if (SM_Global.PreviewUser == null)
            return;

     

        ProfileCommon previewProfile = SM_Global.PreviewProfile;
        if (previewProfile == null)
            throw new Exception("Empty Preview Profile");


        lblPreviewFirstName.Text = previewProfile.FirstName ?? "[Not Set]";
        lblPreviewLastName.Text = previewProfile.LastName ?? "[Not Set]";
        lblPreviewCompany.Text = previewProfile.CompanyName ?? "[Not Set]";
        lblPreviewUserName.Text = previewProfile.UserName ?? "[Not Set]";
        lblPreviewEmail.Text = SM_Global.PreviewUser.Email ?? "[Not Set]";


        Rep PreviewRep = null;
        using (sm_portalEntities pdc = new sm_portalEntities())
            PreviewRep = pdc.Reps.Where(w => w.RzRepID == SM_Global.PreviewProfile.SensibleRepID).FirstOrDefault();
        if (PreviewRep == null)
        {
            pnlPreviewRep.Visible = false;
            return;
        }


        pnlPreviewRep.Visible = true;

        imgPreviewRepImage.Src = SM_Global.GetHeadshotImagePath(PreviewRep);//Null = default logo.
        //lblRepName.Text = rep.FullName;
        aPreviewRep.InnerText = PreviewRep.FullName;
        aPreviewRep.HRef = "mailto:" + PreviewRep.Email.Trim().ToLower();

    }

    private void LoadPreviewUsersDDl()
    {
        List<User> portalUsers = new List<User>();


        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            portalUsers = pdc.Users.OrderBy(o => o.UserName).ToList();
        }
        //testing 
        //List<string> nonSensiblePortalIds = portalUsers.Where(w => w.UserName.Contains("@")).Select(s => s.UserId.ToString()).ToList();

        ddlPortalPreviewUser.DataSource = portalUsers;
        ddlPortalPreviewUser.DataTextField = "UserName";
        ddlPortalPreviewUser.DataValueField = "UserId";
        ddlPortalPreviewUser.Items.Add(new ListItem("--Choose Preview User--", "0"));
        ddlPortalPreviewUser.AppendDataBoundItems = true;
        ddlPortalPreviewUser.DataBind();
        ddlPortalPreviewUser.SelectedValue = "0";
        if (SM_Global.PreviewUser != null)
            ddlPortalPreviewUser.SelectedValue = SM_Global.PreviewUser.ProviderUserKey.ToString();
    }

    protected void lbUpdatePortalPreviewUser_Click(object sender, EventArgs e)
    {
        string previewUserName = ddlPortalPreviewUser.SelectedItem.Text;
        SM_Global.PreviewUser = System.Web.Security.Membership.GetUser(previewUserName);
        LoadPreviewProfileData();      
        //Refresh the page to get the updated labels.
        Response.Redirect(Request.RawUrl, false);

    }

    private void UpdateMasterPageShowingCompany()
    {

        ((hs_flex)this.Master).LoadCompanyLabel();


    }

    protected void lbResetPortalPreviewUser_Click(object sender, EventArgs e)
    {
        SM_Global.PreviewUser = null;
        LoadPreviewProfileData();
        UpdateMasterPageShowingCompany();
    }
}