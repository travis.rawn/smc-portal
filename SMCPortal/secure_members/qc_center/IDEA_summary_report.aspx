﻿<%@ Page Title="IDEA Summary Report" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="IDEA_summary_report.aspx.cs" Inherits="secure_sm_IDEA_insp_SM_Inspection_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="/Content/css/sm_reporting.css" rel="stylesheet" />
    <style>
        .btn_download {
            margin-bottom: 15px;
        }

        .hs-button-md {
            font-size: 10px;
            padding: 10px 30px 10px 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="LeadContent" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6 class="relative">IDEA-STD-1010 Inspection Summary</h6>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">


    <div role="alert" runat="server" visible="false" id="divPageResult">
        <asp:Label ID="lblPageResult" runat="server"></asp:Label>
    </div>

    <div id="divReport" class="report-container" runat="server">
        <div class="row">
            <div class="col-sm-12">
                <asp:Button ID="btnCreatePDF" runat="server" Text="Download PDF" CssClass="hs-button hs-button-md btn_download" OnClick="btnCreatePDF_Click" data-intro="Download a PDF version of this report" data-position="right" OnClientClick="slideAlert( 'success', 'Your PDF is being generated and will be downloaded momentarily.');" ToolTip="Download PDF" />
            </div>
        </div>

        <%--Header Section--%>
        <div id="header">
            <%--Header Title   --%>
            <div class="page-header">
                <div class="row  report-section-title">
                    <div class="col-sm-6">
                        <label>IDEA-STD-1010 Inspection Summary</label>

                    </div>
                    <div class="col-sm-4">
                        <label>
                            <asp:Label ID="lblCustomerName" runat="server"></asp:Label></label>
                    </div>

                    <div class="col-sm-2">
                        <label>ID:<asp:Label ID="lblInspID" runat="server"></asp:Label></label>
                    </div>

                </div>
            </div>
            <%--Header Details--%>
            <div class="well well-sm report-section-data">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Date:</label><asp:Label ID="lblDate" runat="server"></asp:Label>
                    </div>
                    <div class="col-sm-6">
                        <label>Inspector:</label><asp:Label ID="lblInspector" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Part Number:</label><asp:Label ID="lblPartNumber" runat="server"></asp:Label>
                    </div>
                    <div class="col-sm-6">
                        <label>Manufacturer:</label><asp:Label ID="lblManufacturer" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Quantity:</label><asp:Label ID="lblQuantity" runat="server"></asp:Label>
                    </div>
                    <div class="col-sm-6">
                        <label>Date Code:</label><asp:Label ID="lblDateCode" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Lot Number:</label><asp:Label ID="lblLotNumber" runat="server"></asp:Label>
                    </div>
                    <div class="col-sm-6">
                        <label>Country of Origin:</label><asp:Label ID="lblCoo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Packaging:</label><asp:Label ID="lblPackaging" runat="server"></asp:Label>
                    </div>
                    <div class="col-sm-6">
                        <label>Package Type:</label><asp:Label ID="lblPackageType" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Inspection Result:</label><asp:Label ID="lblResult" runat="server"></asp:Label>
                    </div>
                </div>

                <%--Header Imagery--%>
                <div id="divHeaderImagery" class="well well-sm" runat="server" style="padding: 20px;">
                    <div class="row" id="HeaderImagery">
                        <div class="row">
                            <div class="col-sm-12" data-intro="Click images to see a larger view" data-position="top">
                                <label>Component Imagery:</label><em>(click to enlarge)</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbHeaderImageTop">
                                    <asp:Image ID="imgHeaderImageTop" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>

                            </div>
                            <div class="row">
                                <asp:Label ID="lblHeaderImageTopText" runat="server"></asp:Label>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbHeaderImageBot">
                                    <asp:Image ID="imgHeaderImageBot" runat="server" CssClass="img-responsive img-rounded thumbnail" />

                                </a>

                            </div>
                            <div class="row">
                                <asp:Label ID="lblHeaderImageBotText" runat="server"></asp:Label>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbHeaderImageOther">
                                    <asp:Image ID="imgHeaderImageOther" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblHeaderImageOtherText" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>




            </div>
        </div>
        <%--Initial Inspection--%>
        <div id="Initual Inspection">
            <%--Initial Inspection Title--%>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 text-center report-section-title" style="background-color: lightgray">
                        <label>Initial Inspection (IDEA 10.2.1)</label>
                    </div>
                </div>
            </div>
            <%--Initial Inspection Data--%>
            <div class="well well-sm  report-section-data">
                <div class="row">
                    <div class="col-sm-8">
                        <label>Verify the part number, manufacturer, and quantity match the purchase order and packing slip.</label>
                    </div>
                    <div class="col-sm-4">
                        <asp:Label ID="lblInit1" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <label>Confirm the date code meets any restrictions specified on the purchase agreement.</label>
                    </div>
                    <div class="col-sm-4">
                        <asp:Label ID="lblInit4" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <%--Detail Visual Inspection--%>
        <div id="divDetail">
            <%--Detail Inspection Title--%>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 text-center  report-section-title" style="background-color: lightgray">
                        <label>Detailed Inspection (Visual) (IDEA 10.3.1)</label>
                    </div>
                </div>
            </div>
            <%--Detail Inspection Data--%>
            <div class="well well-sm   report-section-data">
                <div class="row">
                    <div class="col-sm-8">
                        <label>Verify the logo and markings match the manufacturer's specifications.</label>
                    </div>
                    <div class="col-sm-4">
                        <asp:Label ID="lblDetail1" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <label>Are leads in acceptable condition?</label>
                    </div>
                    <div class="col-sm-4">
                        <asp:Label ID="lblDetail11" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <%--Solvents--%>
        <div id="divSolvent" runat="server">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 text-center  report-section-title" style="background-color: lightgray">
                        <label>Detailed Inspection (Solvents) (IDEA 10.3.2)</label>
                    </div>
                </div>
            </div>
            <%--Solvent Data--%>
            <div class="well well-sm   report-section-data">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Perform device marking test.</label>
                    </div>
                    <div class="col-sm-9">
                        <asp:Label ID="lblSolvent1" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label>Perform device surface test.</label>
                    </div>
                    <div class="col-sm-9">
                        <asp:Label ID="lblSolvent2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label>Perform scrape test (as needed).</label>
                    </div>
                    <div class="col-sm-9">
                        <asp:Label ID="lblSolvent3" runat="server"></asp:Label>
                    </div>
                </div>

                <div id="divSolventImagery" class="well well-sm" runat="server">

                    <div class="row">
                        <div class="col-sm-12">
                            <label>Solvent Imagery:</label>
                            <em>(click to enlarge)</em>
                        </div>
                    </div>
                    <div class="row">

                        <asp:Panel CssClass="col-sm-4" ID="divSolventImageryLeft" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbSolventImageryLeft">
                                    <asp:Image ID="imgSolventImageryLeft" CssClass="img-responsive img-rounded thumbnail" runat="server" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblSolventImageryLeft" runat="server"></asp:Label>
                            </div>

                        </asp:Panel>
                        <asp:Panel CssClass="col-sm-4" ID="divSolventImageryMid" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbSolventImageryMid">
                                    <asp:Image ID="imgSolventImageryMid" CssClass="img-responsive img-rounded thumbnail" runat="server" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblSolventImageryMid" runat="server"></asp:Label>
                            </div>

                        </asp:Panel>
                        <asp:Panel CssClass="col-sm-4" ID="divSolventImageryRight" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbSolventImageryRight">
                                    <asp:Image ID="imgSolventImageryRight" CssClass="img-responsive img-rounded thumbnail" runat="server" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblSolventImageryRight" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>

                    </div>
                </div>
            </div>
        </div>
        <%--Mechanical--%>
        <div id="divMech" runat="server">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 text-center  report-section-title" style="background-color: lightgray">
                        <div class="row">
                            <label>Detailed Inspection (Mechanical) (IDEA 10.3.3)</label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Mechanical Data--%>
            <div class="well well-sm   report-section-data">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Record dimensions:</label>
                    </div>
                    <div class="col-sm-9">
                        <asp:Label ID="lblMech" runat="server"></asp:Label>
                    </div>
                </div>
                <%-- Mech Imagery--%>
                <%--<div class="well well-sm" id="divMechImagery" runat="server" style="padding: 20px;">--%>
                <div id="divMechImagery" class="well well-sm" runat="server">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Mechanical Imagery:</label><em>(click to enlarge)</em>
                        </div>
                    </div>
                    <div class="row">
                        <%--  <div class="col-sm-12">--%>
                        <asp:Panel CssClass="col-sm-4" ID="divMechImageryLeft" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbMechImageryLeft">
                                    <asp:Image ID="imgMechImageryLeft" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>

                            </div>
                            <div class="row">
                                <asp:Label ID="lblMechImageryLeft" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:Panel CssClass="col-sm-4" ID="divMechImageryMid" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbMechImageryMid">
                                    <asp:Image ID="imgMechImageryMid" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblMechImageryMid" runat="server"></asp:Label>
                            </div>

                        </asp:Panel>
                        <asp:Panel CssClass="col-sm-4" ID="divMechImageryRight" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbMechImageryRight">
                                    <asp:Image ID="imgMechImageryRight" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblMechImageryRight" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <%-- </div>--%>
                    </div>
                </div>
            </div>
        </div>
        <%--Additional Testing--%>
        <div id="divAddTesting" runat="server">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 text-center  report-section-title" style="background-color: lightgray">
                        <div class="row">
                            <label>Additional Testing Performed</label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Additional Testing Data--%>
            <div class="well well-sm   report-section-data">
                <div class="row">
                    <div class="col-sm-1">
                        <label>Details:</label>
                    </div>
                    <div class="col-sm-11">
                        <asp:Label ID="lblAddTesting" runat="server"></asp:Label>
                    </div>
                </div>
                <%-- Additional Testing Imagery--%>
                <div id="divAddTestingImagery" class="well well-sm" runat="server">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Additional Testing Imagery:</label><em>(click to enlarge)</em>
                        </div>
                    </div>
                    <div class="row">
                        <%--<div class="col-sm-12">--%>
                        <asp:Panel CssClass="col-sm-4" ID="divAddTestingImageryLeft" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbAddTestingImageryLeft">
                                    <asp:Image ID="imgAddTestingImageryLeft" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblAddTestingImageryLeft" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:Panel CssClass="col-sm-4" ID="divAddTestingImageryMid" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbAddTestingImageryMid">
                                    <asp:Image ID="imgAddTestingImageryMid" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblAddTestingImageryMid" runat="server"></asp:Label>
                            </div>

                        </asp:Panel>
                        <asp:Panel CssClass="col-sm-4" ID="divAddTestingImageryRight" runat="server">
                            <div class="row">
                                <a class="fancybox" data-fancybox="gallery" runat="server" id="fbAddTestingImageryRight">
                                    <asp:Image ID="imgAddTestingImageryRight" runat="server" CssClass="img-responsive img-rounded thumbnail" />
                                </a>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblAddTestingImageryRight" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>


        


    </div>

    <%-- <script src="../../Scripts/wheelzoom.js"></script>
    <script>
        wheelzoom(document.querySelector('img.zoom'));
    </script>--%>
</asp:Content>

