﻿<%@ Page Title="Quality Center" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="my_qc.aspx.cs" Inherits="secure_members_qc_center_my_qc" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/sm_checkbox.ascx" TagPrefix="uc1" TagName="sm_checkbox" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="LeadContent" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6 class="relative">Quality Center</h6>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">



    <div role="alert" class="alert alert-dismissable fade in" runat="server" id="divPageResult" visible="false">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <asp:Label ID="lblPageResult" runat="server"></asp:Label>
    </div>

    <div class="card">
        <div class="card-heading">
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <uc1:sm_checkbox runat="server" ID="smcb" />
                    <hr />
                    <asp:GridView ID="gvMyQC" runat="server" GridLines="None" AutoGenerateColumns="False" DataKeyNames="ID" OnRowDataBound="gvMyQC__RowDataBound" Width="100%">
                        <EmptyDataTemplate>
                            <div class="alert alert-warning" style="text-align: center; font-size: 14px;">
                                No data found for your account.  Use the above checkbox to preview some of our testing capabilities.
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Report" SortExpression="ID">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="hlinkViewInspection" Target="_blank">View</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Type" HeaderText="Type" ReadOnly="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" />
                            <asp:BoundField DataField="Company" HeaderText="Company" SortExpression="Company" />
                            <asp:BoundField DataField="PO" HeaderText="My PO" />
                            <asp:BoundField DataField="Part" HeaderText="Part Number" />
                            <asp:BoundField DataField="Date" HeaderText="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>



    <asp:SqlDataSource ID="SqlDataSourceInsp" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
</asp:Content>

