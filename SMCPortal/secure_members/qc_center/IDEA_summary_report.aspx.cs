﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Linq;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_sm_IDEA_insp_SM_Inspection_report : System.Web.UI.Page
{

    RzDataContext RZDC = new RzDataContext();
    SM_Quality_Logic smq = new SM_Quality_Logic();
    sm_binaryEntities smb = new sm_binaryEntities();
    SM_PDF smpdf = new SM_PDF();

    SM_Tools tools = new SM_Tools();
    //string MySQLconstr = ConfigurationManager.ConnectionStrings["MYSQL_ConnectionString"].ConnectionString;
    insp_head Header;
    insp_detail Detail;
    List<insp_images> Images = new List<insp_images>();



    public int insp_id
    {
        get
        {
            if (ViewState["insp_id"] != null)
                return (int)ViewState["insp_id"];
            else
                return 0;

        }
        set
        {
            ViewState["insp_id"] = value;
        }
    }


    public List<int> ApprovedInspID
    {
        get
        {
            if (ViewState["ApprovedInspID"] == null)
                ViewState["ApprovedInspID"] = new List<int>();
            return (List<int>)ViewState["ApprovedInspID"];
        }
        set
        {
            ViewState["ApprovedInspID"] = value;
        }
    }

    //public DataTable dtImages
    //{
    //    get
    //    {
    //        if (ViewState["dtImages"] == null)
    //            ViewState["dtImages"] = new DataTable();
    //        return (DataTable)ViewState["dtImages"];
    //    }
    //    set
    //    {
    //        ViewState["dtImages"] = value;
    //    }
    //}




    public List<insp_images> HeaderImages = new List<insp_images>();


    public List<insp_images> SolventImages = new List<insp_images>();

    public List<insp_images> MechImages = new List<insp_images>();

    public List<insp_images> AddTestingImages = new List<insp_images>();



    protected void Page_Load(object sender, EventArgs e)
    {
        tools.ShowNavHelp(Page);

        divPageResult.Visible = false;
        GetInspectionID();
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            Header = pdc.insp_head.SingleOrDefault(h => h.insp_id == insp_id);
            Detail = pdc.insp_detail.SingleOrDefault(d => d.insp_id == insp_id);
        }
           


        if (!Page.IsPostBack)
        {
            if (insp_id == 0)
            {
                HandleResult("No Inspeciton ID available.", true);
                divReport.Visible = false;
                return;
            }
            else
                lblInspID.Text = insp_id.ToString();

            if (!CheckPermissions() && !Header.is_demo) //10427 = sample report
            {

                HandleResult("Sorry, you are not authorized to view this report.", true);
                divReport.Visible = false;
                return;
            }

            if (!CheckComplete(Header))
            {
                HandleResult("This report is still in progress. Please check back again soon.", true);
                btnCreatePDF.Visible = false;
                divReport.Visible = false;
                return;
            }
            try
            {
                LoadReport();
            }
            catch (Exception ex)
            {
                HandleResult(ex.Message, true);
            }



        }

    }

    protected void GetInspectionsOwnedByUser()
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            if (Roles.IsUserInRole("sm_internal"))
            {
                var query = from h in pdc.insp_head
                                //where h.sales_email == Membership.GetUser().Email
                            where GetCompaniesOwnedByUser().Contains(h.customer_uid)
                            select h;

                foreach (insp_head h in query)
                {
                    ApprovedInspID.Add(h.insp_id);
                }
            }
            else
            {
                var query = from h in pdc.insp_head
                            where h.customer_uid == Profile.CompanyID
                            select h;

                foreach (insp_head h in query)
                {
                    ApprovedInspID.Add(h.insp_id);
                }
            }
        }

           

    }
    protected List<string> GetCompaniesOwnedByUser()
    {
        List<string> companyIDs = new List<string>();
        var query = (from c in RZDC.companies
                         //where h.sales_email == Membership.GetUser().Email
                     where c.base_mc_user_uid == Profile.RzUserID
                     select c);
        foreach (company c in query)
        {
            companyIDs.Add(c.unique_id);
        }
        return companyIDs;
    }
    protected void GetInspectionID()
    {
        string strInspID = Tools.Strings.SanitizeInput(Request.QueryString["insp_id"]);
        if (!string.IsNullOrEmpty(strInspID))
        {
            if (strInspID == "sample")
            {

                ViewState["insp_id"] = 10427;
                insp_id = 10427;
                //sql = "SELECT CAST(insp_date as date) [Date], [customer_po] [My PO], [fullpartnumber] [Part Number], [insp_id] [ID], customer_name [Customer Name] FROM [insp_head] WHERE (insp_id = '10428') ORDER BY [completed_date] DESC";
                //SqlDataSourceInsp.SelectParameters.Add("CPO", CPO);
            }
            else
            {
                ViewState["insp_id"] = Convert.ToInt32(strInspID);
                insp_id = Convert.ToInt32(ViewState["insp_id"]);
            }





        }


    }

    protected bool CheckComplete(insp_head h)
    {
        if (h == null || h.is_insp_complete != true)
        {
            return false;
        }
        return true;
    }

    protected void LoadHeaderValues()
    {
        //Header = PDC.insp_heads.SingleOrDefault(h => h.insp_id == insp_id);
        if (Header != null)
        {

            //Customer Name
            lblCustomerName.Text = Header.customer_name;

            //Inspection Date
            if (String.IsNullOrEmpty(Header.completed_date.ToString()))
                lblDate.Text = "Pending";
            else
                lblDate.Text = Header.completed_date.ToString() ?? "Pending";
            //Inspector
            lblInspector.Text = Header.inspector;
            //Part Number
            lblPartNumber.Text = Header.fullpartnumber;
            //Manufacturer
            lblManufacturer.Text = Header.manufacturer;
            //Quantity  

            lblQuantity.Text = GetShownQuantity();
            //Datecode
            lblDateCode.Text = Header.date_code;
            //Lot Number
            lblLotNumber.Text = Header.lot;
            //COO
            lblCoo.Text = Header.coo;
            //Packaging
            lblPackaging.Text = Header.packaging;
            //Package Type
            lblPackageType.Text = Header.package_type;
            // Inspection Result
            if (String.IsNullOrEmpty(Header.result))
                lblResult.Text = "Pending";
            else
                lblResult.Text = Header.result ?? "Pending";

            //HeaderImageText
            //lblHeaderImageTopText.Text = GetImageText("HeaderImageTop");
            //lblHeaderImageBotText.Text = GetImageText("HeaderImageBot");
            //lblHeaderImageOtherText.Text = GetImageText("HeaderImageOther");

        }
    }

    private string GetShownQuantity()
    {
        //if (tools.IsInternalUser())//IF CUstomer, show the customer QTY
        return smq.GetCustomerInspectionQuantity(Header).ToString();
        //else return Header.quantity.ToString();
    }

    protected void LoadDetailValues()
    {
        //Initial Inspection
        lblInit1.Text = convertResult(Detail.initial1_result, Detail.initial1_text);
        lblInit4.Text = convertResult(Detail.initial4_result, Detail.initial4_text);

        //Detail Inspection
        lblDetail1.Text = convertResult(Detail.detail1_result, Detail.detail1_text);
        lblDetail11.Text = convertResult(Detail.detail11_result, Detail.detail11_text);

        //Solvents
        if (Detail.is_solvent == true)
        {
            divSolvent.Visible = true;
            lblSolvent1.Text = convertResult(Detail.solvent1_result, Detail.solvent1_text);
            lblSolvent2.Text = convertResult(Detail.solvent2_result, Detail.solvent2_text);
            lblSolvent3.Text = convertResult(Detail.solvent3_result, Detail.solvent3_text);

            ////Solvent Image Text
            //if (SolventImages.Count > 0)
            //{
            //    lblSolventImageryLeft.Text = GetImageText("SolventImageryLeft");
            //    if (SolventImages.Count > 1)
            //        lblSolventImageryMid.Text = GetImageText("SolventImageryMid");
            //    if (SolventImages.Count > 2)
            //        lblSolventImageryRight.Text = GetImageText("SolventImageryRight");
            //}


        }
        else
        {
            divSolvent.Visible = false;
        }

        //Mech
        if (Detail.is_mech == true)
        {
            divMech.Visible = true;
            lblMech.Text = convertResult(Detail.mech2_text) + " ," + convertResult(Detail.mech3_text);

            ////Mech Image Text
            //if (MechImages.Count > 0)
            //{
            //    lblMechImageryLeft.Text = GetImageText("MechImageryLeft");
            //    if (MechImages.Count > 1)
            //        lblMechImageryMid.Text = GetImageText("MechImageryMid");
            //    if (MechImages.Count > 2)
            //        lblMechImageryRight.Text = GetImageText("MechImageryRight");
            //}

        }
        else
            divMech.Visible = false;

        //Additional Testing
        if (Detail.is_addtesting == true)
        {
            divAddTesting.Visible = true;
            lblAddTesting.Text = convertResult(Detail.addtesting_text);

            ////AddTesting Image Text
            //if (AddTestingImages.Count > 0)
            //{
            //    lblAddTestingImageryLeft.Text = GetImageText("AddTestingImageryLeft");
            //    if (AddTestingImages.Count > 1)
            //        lblAddTestingImageryMid.Text = GetImageText("AddTestingImageryMid");
            //    if (AddTestingImages.Count > 2)
            //        lblAddTestingImageryRight.Text = GetImageText("AddTestingImageryRight");
            //}

        }
        else
            divAddTesting.Visible = false;
    }

    protected string convertResult(string result, string othertext = null)
    {
        switch (result)
        {
            case "1":
                return "Pass";
            case "2":
                {
                    //For overall "Pass" reports, we consider all subsections a pass per the customer's PO
                    if (Header.result == "Pass")
                        return "Pass";
                    else //else, if the overall test is a fail, we return fail.  Customers don't see failed reports.
                        return "Fail";
                }

            case "3":
                //return "Other: "+othertext;
                //return "Accepted";
                return "Pass";
            default:
                return result;
        }
    }


    //protected DataTable GetImageDataTable()
    //{

    //    dtImages.Columns.Add("img_blob", typeof(byte[]));
    //    dtImages.Columns.Add("insp_id", typeof(String));
    //    dtImages.Columns.Add("img_description", typeof(String));
    //    dtImages.Columns.Add("insp_image_id", typeof(String));
    //    dtImages.Columns.Add("insp_section_id", typeof(String));
    //    dtImages.Columns.Add("insp_type", typeof(String));
    //    dtImages.Columns.Add("unique_id", typeof(int));
    //    using (MySqlConnection con = new MySqlConnection(MySQLconstr))
    //    {
    //        using (MySqlCommand cmd = new MySqlCommand())
    //        {
    //            cmd.CommandText = "SELECT img_blob, insp_id, img_description, insp_image_id, insp_section_id, unique_id, insp_type FROM insp_images WHERE insp_id = '" + insp_id + "' AND insp_type = 'IDEA' ORDER BY unique_id";
    //            cmd.Connection = con;
    //            using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
    //            {
    //                sda.Fill(dtImages);
    //            }
    //        }
    //    }
    //    dtImages.DefaultView.Sort = "unique_id";
    //    return dtImages;
    //}

    protected void LoadImages()
    {

        if (Header.source_insp_id > 0)
            Images = smb.insp_images.Where(w => w.insp_id == Header.source_insp_id && w.insp_type == "IDEA").ToList();
        else
            Images = smb.insp_images.Where(w => w.insp_id == insp_id && w.insp_type == "IDEA").ToList();
        LoadSectionImages();



        HeaderImages = Images.Where(s => s.insp_section_id.Contains("Header")).ToList();
        SolventImages = Images.Where(s => s.insp_section_id.Contains("Solvents")).ToList();
        MechImages = Images.Where(s => s.insp_section_id.Contains("Mech")).ToList();
        AddTestingImages = Images.Where(s => s.insp_section_id.Contains("AddTesting")).ToList();


        bindHeaderImages(HeaderImages);
        bindImageSections(SolventImages, "SolventImagery");
        bindImageSections(MechImages, "MechImagery");
        bindImageSections(AddTestingImages, "AddTestingImagery");


    }

    protected string GetImage(insp_images i)
    {
        return "~/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + "IDEA";
    }



    protected byte[] GetHeaderImageByte(string SectionID)
    {


        if (SectionID.Contains("Header"))
        {

            // byte[] bytes = dtImages.AsEnumerable()
            //.Where(p => p.Field<string>("insp_section_id") == SectionID)
            //.Select(p => p.Field<byte[]>("img_blob"))
            //.FirstOrDefault();
            string imagePath = Images
           .Where(p => p.insp_section_id == SectionID)
           .Select(p => p.img_path_web)
           .FirstOrDefault();
            if (string.IsNullOrEmpty(imagePath))
                throw new Exception("Invalid Image");
            Byte[] headerImageByte = tools.LoadImageFromPath(imagePath);
            if (headerImageByte == null)
                throw new Exception("Invalid Image");

            return headerImageByte;

        }

        else
            return null;
    }



    protected void LoadSectionImages()
    {

        foreach (insp_images i in Images)
        {

            if (i.insp_section_id.Contains("Solvents"))
                SolventImages.Add(i);
            if (i.insp_section_id.Contains("Mech"))
                MechImages.Add(i);
            if (i.insp_section_id.Contains("Head"))
                HeaderImages.Add(i);
            if (i.insp_section_id.Contains("AddTesting"))
                AddTestingImages.Add(i);
        }
    }
    private void bindHeaderImages(List<insp_images> SectionImagesList)
    {
        if (HeaderImages.Count <= 0 || HeaderImages == null)
        {
            divHeaderImagery.Visible = false;
            return;
        }

        insp_images i;
        i = HeaderImages.Where(w => w.insp_section_id == "HeaderImageTop").FirstOrDefault();
        if (i != null)
        {
            imgHeaderImageTop.ImageUrl = GetImage(i);
            fbHeaderImageTop.HRef = imgHeaderImageTop.ImageUrl;
        }

        i = HeaderImages.Where(w => w.insp_section_id == "HeaderImageBot").FirstOrDefault();
        if (i != null)
        {
            imgHeaderImageBot.ImageUrl = GetImage(i);
            fbHeaderImageBot.HRef = imgHeaderImageBot.ImageUrl;

        }

        i = HeaderImages.Where(w => w.insp_section_id == "HeaderImageOther").FirstOrDefault();
        if (i != null)
        {
            imgHeaderImageOther.ImageUrl = GetImage(i);
            fbHeaderImageOther.HRef = imgHeaderImageOther.ImageUrl;
        }

    }


    protected void bindImageSections(List<insp_images> SectionImagesList, string SectionID)
    {
        //divMechImageryLeft
        //divAddTestingImageryLeft
        if (SectionImagesList.Count <= 0 || SectionImagesList == null)
        {
            HideSectionImages(SectionID);
            return;
        }

        System.Web.UI.WebControls.Panel leftDiv = (System.Web.UI.WebControls.Panel)Page.Master.FindControl("MainContent").FindControl("div" + SectionID + "Left");
        System.Web.UI.WebControls.Panel midDiv = (System.Web.UI.WebControls.Panel)Page.Master.FindControl("MainContent").FindControl("div" + SectionID + "Mid");
        System.Web.UI.WebControls.Panel rightDiv = (System.Web.UI.WebControls.Panel)Page.Master.FindControl("MainContent").FindControl("div" + SectionID + "Right");
        System.Web.UI.WebControls.Image leftImage = (System.Web.UI.WebControls.Image)Page.Master.FindControl("MainContent").FindControl("img" + SectionID + "Left");
        System.Web.UI.WebControls.Image midImage = (System.Web.UI.WebControls.Image)Page.Master.FindControl("MainContent").FindControl("img" + SectionID + "Mid");
        System.Web.UI.WebControls.Image rightImage = (System.Web.UI.WebControls.Image)Page.Master.FindControl("MainContent").FindControl("img" + SectionID + "Right");
        HtmlAnchor leftAnchor = (HtmlAnchor)Page.Master.FindControl("MainContent").FindControl("fb" + SectionID + "Left");
        HtmlAnchor midAnchor = (HtmlAnchor)Page.Master.FindControl("MainContent").FindControl("fb" + SectionID + "Mid");
        HtmlAnchor rightAnchor = (HtmlAnchor)Page.Master.FindControl("MainContent").FindControl("fb" + SectionID + "Right");

        switch (SectionImagesList.Count)
        {
            case 1:
                {
                    //Show only middle Image
                    leftDiv.Visible = true;
                    midDiv.Visible = true;
                    rightDiv.Visible = true;
                    leftAnchor.Visible = false;
                    rightAnchor.Visible = false;
                    midImage.ImageUrl = GetImage(SectionImagesList[0]);
                    midAnchor.HRef = midImage.ImageUrl;
                    break;
                }


            case 2:
                {
                    //Show Left and Mid, hide right
                    leftDiv.Visible = true;
                    leftImage.ImageUrl = GetImage(SectionImagesList[0]);
                    leftAnchor.HRef = leftImage.ImageUrl;
                    midDiv.Visible = true;
                    midImage.ImageUrl = GetImage(SectionImagesList[1]);
                    midAnchor.HRef = midImage.ImageUrl;

                    rightDiv.Visible = false;
                    rightAnchor.Visible = false;
                    rightImage.Visible = false;
                    break;
                }
            case 3:
                {
                    leftDiv.Visible = true;
                    leftImage.ImageUrl = GetImage(SectionImagesList[0]);
                    leftAnchor.HRef = leftImage.ImageUrl;
                    midDiv.Visible = true;
                    midImage.ImageUrl = GetImage(SectionImagesList[1]);
                    midAnchor.HRef = midImage.ImageUrl;
                    rightDiv.Visible = true;
                    rightImage.ImageUrl = GetImage(SectionImagesList[2]);
                    rightAnchor.HRef = rightImage.ImageUrl;
                    break;
                }

            default:
                {
                    leftDiv.Visible = false;
                    leftAnchor.Visible = false;
                    leftImage.Visible = false;
                    midDiv.Visible = false;
                    midAnchor.Visible = false;
                    midAnchor.Visible = false;
                    rightDiv.Visible = false;
                    rightAnchor.Visible = false;
                    rightDiv.Visible = false;
                    break;
                }
        }
    }

    private void HideSectionImages(string sectionID)
    {

        switch (sectionID)
        {
            case "SolventImagery":
                {
                    divSolventImagery.Visible = false;
                    break;
                }
            case "MechImagery":
                {
                    divMechImagery.Visible = false;
                    break;
                }
            case "AddTestingImagery":
                {
                    divAddTestingImagery.Visible = false;
                    break;
                }
        }

    }

    protected void DownloadPDF()
    {
        LoadImages();
        byte[] bytes;//Trying to handle this as a memorystream so I don't need to save and delete.
        //string path = Server.MapPath("~/temp");
        Rectangle r = new Rectangle(PageSize.LETTER);
        //the below floats (points) equate to about 1/72nd of an inch.       

        using (var MS = new System.IO.MemoryStream())
        {
            Document document = new Document(r, 60f, 60f, 20f, 36f);

            //Get All Images for Report

            //Spacer Column
            PdfPCell cellSpacer = new PdfPCell(new Phrase());
            cellSpacer.Border = Rectangle.NO_BORDER;

            //Need to embed the font so we get access to special ASCII characters like Ω (alt+234) and µ (alt-230)
            string fontpath = HttpContext.Current.Server.MapPath("~/Content/fonts/arial.ttf");


            //Font brown = new Font(FontFactory.GetFont("COURIER", 9f, Font.NORMAL, new BaseColor(163, 21, 21)));
            //Font lightblue = new Font(FontFactory.GetFont("COURIER", 9f, Font.NORMAL, new BaseColor(43, 145, 175)));
            //Font courier = new Font(FontFactory.GetFont("COURIER", 9f));
            //Font georgia = FontFactory.GetFont("georgia", 10f, Font.NORMAL);

            //FonttheFont.Bold = FontFactory.GetFont("georgia", 10f, Font.BOLD);
            //Font NormalFont = FontFactory.GetFont("georgia", 10f, Font.NORMAL);
            //Font SmallFont = FontFactory.GetFont("georgia", 8f, Font.BOLD);


            //KT USing THese
            //FonttheFont.Bold = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.BOLD);
            //Font NormalFont = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10f, Font.NORMAL);
            //Font SmallFont = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 8f, Font.BOLD);

            //Font
            SM_PDFFont theFont = SM_PDFFont.GetFont("Montserrat");



            string FileName = Guid.NewGuid() + ".pdf";

            //KT Call the Page Event class
            //PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(path + "/" + FileName, FileMode.Create));
            PdfWriter writer = PdfWriter.GetInstance(document, MS);
            //PdfWriter.GetInstance(document, new FileStream(path + "/" + FileName, FileMode.Create));
            //fWriter.GetInstance(document, new FileStream(path + "/iTextSharp_Test.pdf", FileMode.Create));
            writer.PageEvent = new PDFFooter();


            document.Open();

            string imagepath = Server.MapPath("~/Images");
            Image pngLogo = Image.GetInstance(imagepath + "/pdf_logo_2018.png");
            pngLogo.ScalePercent(23);


            PdfPTable HeaderTable = new PdfPTable(12);
            HeaderTable.WidthPercentage = 100;

            PdfPCell cellLogo = new PdfPCell(pngLogo);
            cellLogo.Colspan = 3;
            cellLogo.Border = Rectangle.NO_BORDER;
            cellLogo.PaddingBottom = 5f;
            HeaderTable.AddCell(cellLogo);


            PdfPCell cellTitle1 = smpdf.CreateCell("IDEA-STD-1010 ", theFont.H0Light, 4, null, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0);

            HeaderTable.AddCell(cellTitle1);

            PdfPCell cellTitle2 = smpdf.CreateCell("INSPECTION SUMMARY", theFont.H0, 5, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0);
            HeaderTable.AddCell(cellTitle2);

            //PdfPCell cellRev = new PdfPCell(new Phrase(""));//Removed Doc# Per JenC
            //cellRev.Border = Rectangle.NO_BORDER;
            //cellRev.Colspan = 2;
            //cellRev.VerticalAlignment = Element.ALIGN_MIDDLE;
            //cellRev.HorizontalAlignment = Element.ALIGN_CENTER;
            //HeaderTable.AddCell(cellRev);


            //Customer Name
            //Title
            Phrase pCustomerTitle = new Phrase();
            Chunk cCustomerTitle = new Chunk("Customer:", theFont.Bold);
            pCustomerTitle.Add(cCustomerTitle);
            PdfPCell cellCustomerTitle = new PdfPCell(pCustomerTitle);
            cellCustomerTitle.Colspan = 2;
            HeaderTable.AddCell(cellCustomerTitle);
            //Text
            Phrase pCustomerText = new Phrase();
            Chunk cCustomerText = new Chunk(Header.customer_name, theFont.Normal);
            PdfPCell cellCustomerText = new PdfPCell(pCustomerText);
            pCustomerText.Add(cCustomerText);
            cellCustomerText.Colspan = 4;
            HeaderTable.AddCell(cellCustomerText);


            //Customer PO
            //Title
            Phrase pCustomerPOTitle = new Phrase();
            Chunk cCustomerPOTitle = new Chunk("Customer PO:", theFont.Bold);
            pCustomerPOTitle.Add(cCustomerPOTitle);
            PdfPCell cellCustomerPOTitle = new PdfPCell(pCustomerPOTitle);
            cellCustomerPOTitle.Colspan = 2;
            HeaderTable.AddCell(cellCustomerPOTitle);
            //Text
            Phrase pCustomerPOText = new Phrase();
            Chunk cCustomerPOText = new Chunk(Header.customer_po, theFont.Normal);
            pCustomerPOText.Add(cCustomerPOText);
            PdfPCell cellCustomerPOText = new PdfPCell(pCustomerPOText);
            cellCustomerPOText.Colspan = 4;
            HeaderTable.AddCell(cellCustomerPOText);


            //Manufacturer
            //Title
            Phrase pMFGTitle = new Phrase();
            Chunk cMFGTitle = new Chunk("Manufacturer:", theFont.Bold);
            pMFGTitle.Add(cMFGTitle);
            PdfPCell cellMFGTitle = new PdfPCell(pMFGTitle);
            cellMFGTitle.Colspan = 2;
            HeaderTable.AddCell(cellMFGTitle);
            //Text
            Phrase pSMFGText = new Phrase();
            Chunk cMFGText = new Chunk(Header.manufacturer, theFont.Normal);
            pSMFGText.Add(cMFGText);
            PdfPCell cellMFGText = new PdfPCell(pSMFGText);
            cellMFGText.Colspan = 4;
            HeaderTable.AddCell(cellMFGText);


            //Sales Order
            //Title
            Phrase pSalesOrderTitle = new Phrase();
            Chunk cSalesOrderTitle = new Chunk("Sales Order:", theFont.Bold);
            pSalesOrderTitle.Add(cSalesOrderTitle);
            PdfPCell cellSalesOrderTitle = new PdfPCell(pSalesOrderTitle);
            cellSalesOrderTitle.Colspan = 2;
            HeaderTable.AddCell(cellSalesOrderTitle);
            //Text
            Phrase pSalesOrderText = new Phrase();
            string SalesOrderText;
            if (!String.IsNullOrEmpty(Header.ordernumber_sales))
                SalesOrderText = Header.ordernumber_sales;
            else
                SalesOrderText = "No Sale";
            Chunk cSalesOrderText = new Chunk(SalesOrderText, theFont.Normal);
            pSalesOrderText.Add(cSalesOrderText);
            PdfPCell cellSalesOrderText = new PdfPCell(pSalesOrderText);
            cellSalesOrderText.Colspan = 4;
            HeaderTable.AddCell(cellSalesOrderText);

            //Part Number
            //Title
            Phrase pPartNumberTitle = new Phrase();
            Chunk cPartNumberTitle = new Chunk("Part Number:", theFont.Bold);
            pPartNumberTitle.Add(cPartNumberTitle);
            PdfPCell cellPartNumberTitle = new PdfPCell(pPartNumberTitle);
            cellPartNumberTitle.Colspan = 2;
            HeaderTable.AddCell(cellPartNumberTitle);
            //Text
            Phrase pPartNumberText = new Phrase();
            Chunk cPartNumberText = new Chunk(Header.fullpartnumber, theFont.Normal);
            pPartNumberText.Add(cPartNumberText);
            PdfPCell cellPartNumberText = new PdfPCell(pPartNumberText);
            cellPartNumberText.Colspan = 4;
            HeaderTable.AddCell(cellPartNumberText);


            //Date Code
            //Title
            Phrase pDateCodeTitle = new Phrase();
            Chunk cDateCodeTitle = new Chunk("Date Code:", theFont.Bold);
            pDateCodeTitle.Add(cDateCodeTitle);
            PdfPCell cellDateCodeTitle = new PdfPCell(pDateCodeTitle);
            cellDateCodeTitle.Colspan = 2;
            HeaderTable.AddCell(cellDateCodeTitle);
            //Text
            Phrase pDateCodeText = new Phrase();
            Chunk cDateCodeText = new Chunk(Header.date_code, theFont.Normal);
            pDateCodeText.Add(cDateCodeText);
            PdfPCell cellDateCodeText = new PdfPCell(pDateCodeText);
            cellDateCodeText.Colspan = 4;
            HeaderTable.AddCell(cellDateCodeText);

            //Quantity
            //Title
            Phrase pQuantityTitle = new Phrase();
            Chunk cQuantityTitle = new Chunk("Quantity:", theFont.Bold);
            pQuantityTitle.Add(cQuantityTitle);
            PdfPCell cellQuantityTitle = new PdfPCell(pQuantityTitle);
            cellQuantityTitle.Colspan = 2;
            HeaderTable.AddCell(cellQuantityTitle);
            //Text
            Phrase pQuantityText = new Phrase();
            Chunk cQuantityText = new Chunk(GetShownQuantity(), theFont.Normal);
            pQuantityText.Add(cQuantityText);
            PdfPCell cellQuantityText = new PdfPCell(pQuantityText);
            cellQuantityText.Colspan = 4;
            HeaderTable.AddCell(cellQuantityText);

            //Package Type
            //Title
            Phrase pPkgTypeTitle = new Phrase();
            Chunk cPkgTypeTitle = new Chunk("Package Type:", theFont.Bold);
            pPkgTypeTitle.Add(cPkgTypeTitle);
            PdfPCell cellPkgTypeTitle = new PdfPCell(pPkgTypeTitle);
            cellPkgTypeTitle.Colspan = 2;
            HeaderTable.AddCell(cellPkgTypeTitle);
            //Text
            Phrase pPkgTypeText = new Phrase();
            Chunk cPkgTypeText = new Chunk(Header.package_type, theFont.Normal);
            pPkgTypeText.Add(cPkgTypeText);
            PdfPCell cellPkgTypeText = new PdfPCell(pPkgTypeText);
            cellPkgTypeText.Colspan = 4;
            HeaderTable.AddCell(cellPkgTypeText);

            //Packaging
            //Title
            Phrase pPkgTitle = new Phrase();
            Chunk cPkgTitle = new Chunk("Packaging:", theFont.Bold);
            pPkgTitle.Add(cPkgTitle);
            PdfPCell cellPkgTitle = new PdfPCell(pPkgTitle);
            cellPkgTitle.Colspan = 2;
            HeaderTable.AddCell(cellPkgTitle);
            //Text
            Phrase pPkgText = new Phrase();
            Chunk cPkgText = new Chunk(Header.packaging, theFont.Normal);
            pPkgText.Add(cPkgText);
            PdfPCell cellPkgText = new PdfPCell(pPkgText);
            cellPkgText.Colspan = 4;
            HeaderTable.AddCell(cellPkgText);

            //Coo
            //
            Phrase pCOOTitle = new Phrase();
            Chunk cCOOTitle = new Chunk("COO:", theFont.Bold);
            pCOOTitle.Add(cCOOTitle);
            PdfPCell cellCOOTitle = new PdfPCell(pCOOTitle);
            cellCOOTitle.Colspan = 2;
            HeaderTable.AddCell(cellCOOTitle);
            //Text
            Phrase pCOOText = new Phrase();
            Chunk cCooText = new Chunk(Header.coo, theFont.Normal);
            pCOOText.Add(cCooText);
            PdfPCell cellCOOText = new PdfPCell(pCOOText);
            cellCOOText.Colspan = 4;
            HeaderTable.AddCell(cellCOOText);

            //Lot
            //Title
            Phrase pLotTitle = new Phrase();
            Chunk cLotTitle = new Chunk("Lot:", theFont.Bold);
            pLotTitle.Add(cLotTitle);
            PdfPCell cellLotTitle = new PdfPCell(pLotTitle);
            cellLotTitle.Colspan = 2;
            HeaderTable.AddCell(cellLotTitle);
            //Text
            Phrase pLotText = new Phrase();
            Chunk cLotText = new Chunk(Header.lot, theFont.Normal);
            pLotText.Add(cLotText);
            PdfPCell cellLotText = new PdfPCell(pLotText);
            cellLotText.Colspan = 4;
            HeaderTable.AddCell(cellLotText);

            //Result
            //Title
            Phrase pResultTitle = new Phrase();
            Chunk cResultTitle = new Chunk("Result:", theFont.Bold);
            pResultTitle.Add(cResultTitle);
            PdfPCell cellResultTitle = new PdfPCell(pResultTitle);
            cellResultTitle.Colspan = 2;
            HeaderTable.AddCell(cellResultTitle);
            //Text
            Phrase pResultText = new Phrase();
            string Result = "In Progress.";
            if (!String.IsNullOrEmpty(Header.result))
                if (Header.result.ToLower() == "pass" || (Header.result.ToLower() == "fail"))
                    Result = Header.result;
            Chunk cResultText = new Chunk(Result, theFont.Normal);
            pResultText.Add(cResultText);
            PdfPCell cellResultText = new PdfPCell(pResultText);
            cellResultText.Colspan = 4;
            HeaderTable.AddCell(cellResultText);
            document.Add(HeaderTable);



            if (HeaderImages.Count > 0)
            {
                //Header Imagery Table
                PdfPTable HeaderImageryTable = new PdfPTable(8);
                HeaderImageryTable.WidthPercentage = 100;
                HeaderImageryTable.SpacingAfter = 5;
                HeaderImageryTable.SpacingBefore = 5;
                //Get Images



                Image imgHeaderImageTop = Image.GetInstance(GetHeaderImageByte("HeaderImageTop"));
                imgHeaderImageTop.ScalePercent(45);
                Image imgHeaderImageBot = Image.GetInstance(GetHeaderImageByte("HeaderImageBot"));
                imgHeaderImageBot.ScalePercent(45);
                Image imgHeaderImageOther = Image.GetInstance(GetHeaderImageByte("HeaderImageOther"));
                imgHeaderImageOther.ScalePercent(45);

                //HeaderImagery - Label            
                PdfPCell cellHeaderImageryLabel = new PdfPCell(new Phrase(new Chunk("Component Imagery:", theFont.Bold)));
                cellHeaderImageryLabel.Colspan = 8;
                cellHeaderImageryLabel.PaddingBottom = 10;
                cellHeaderImageryLabel.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageryLabel);


                //HeaderImageTop - Image
                PdfPCell cellHeaderImageTop = new PdfPCell(imgHeaderImageTop, true);
                cellHeaderImageTop.HorizontalAlignment = Element.ALIGN_CENTER;
                cellHeaderImageTop.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellHeaderImageTop.Colspan = 2;
                cellHeaderImageTop.FixedHeight = 85f;
                cellHeaderImageTop.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageTop);

                //Spacer Column 1
                PdfPCell cellHeaderImageSpacer1 = new PdfPCell(new Phrase());
                cellHeaderImageSpacer1.Border = Rectangle.NO_BORDER;
                cellHeaderImageSpacer1.FixedHeight = 85f;
                HeaderImageryTable.AddCell(cellHeaderImageSpacer1);

                //HeaderImageBot - Image
                PdfPCell cellHeaderImageBot = new PdfPCell(imgHeaderImageBot, true);
                cellHeaderImageBot.HorizontalAlignment = Element.ALIGN_CENTER;
                cellHeaderImageBot.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellHeaderImageBot.Colspan = 2;
                cellHeaderImageBot.FixedHeight = 85f;
                cellHeaderImageBot.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageBot);

                //Spacer Column 2
                PdfPCell cellHeaderImageSpacer2 = new PdfPCell(new Phrase());
                cellHeaderImageSpacer2.Border = Rectangle.NO_BORDER;
                cellHeaderImageSpacer2.FixedHeight = 85f;
                HeaderImageryTable.AddCell(cellHeaderImageSpacer2);

                //HeaderImageOther - Image
                PdfPCell cellHeaderImageOther = new PdfPCell(imgHeaderImageOther, true);
                cellHeaderImageOther.HorizontalAlignment = Element.ALIGN_CENTER;
                cellHeaderImageOther.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellHeaderImageOther.Colspan = 2;
                cellHeaderImageOther.FixedHeight = 85f;
                cellHeaderImageOther.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageOther);

                //HeaderImageTop - Text              
                PdfPCell cellHeaderImageTopText = new PdfPCell(new Phrase(new Chunk(Images.Where(w => w.insp_section_id == "HeaderImageTop").Select(s => s.img_description).FirstOrDefault(), theFont.Normal)));
                cellHeaderImageTopText.Colspan = 2;
                cellHeaderImageTopText.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageTopText);

                //Spacer Column 1
                PdfPCell cellHeaderTextSpacer1 = new PdfPCell(new Phrase());
                cellHeaderTextSpacer1.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderTextSpacer1);

                //HeaderImageBot - Text
                PdfPCell cellHeaderImageBotText = new PdfPCell(new Phrase(new Chunk(Images.Where(w => w.insp_section_id == "HeaderImageBot").Select(s => s.img_description).FirstOrDefault(), theFont.Normal)));
                cellHeaderImageBotText.Colspan = 2;
                cellHeaderImageBotText.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageBotText);

                //Spacer Column 2
                PdfPCell cellHeaderTextSpacer2 = new PdfPCell(new Phrase());
                cellHeaderTextSpacer2.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderTextSpacer2);

                //HeaderImageOther - Text
                //DataRow HeaderImageOtherRow = dtImages.Select("insp_section_id = 'HeaderImageOther'").FirstOrDefault();
                //Object hio = HeaderImageOtherRow.Field<string>("img_description");
                PdfPCell cellHeaderImageOtherText = new PdfPCell(new Phrase(new Chunk(Images.Where(w => w.insp_section_id == "HeaderImageOther").Select(s => s.img_description).FirstOrDefault(), theFont.Normal)));
                cellHeaderImageOtherText.Colspan = 2;
                cellHeaderImageOtherText.Border = Rectangle.NO_BORDER;
                HeaderImageryTable.AddCell(cellHeaderImageOtherText);

                //Commit Header Imagery Table
                document.Add(HeaderImageryTable);
            }


            //Initial Inspection Table
            PdfPTable InitTable = new PdfPTable(8);
            InitTable.WidthPercentage = 100;
            InitTable.SpacingBefore = 5;
            InitTable.SpacingAfter = 5;

            //Initiial Inspection - Title
            PdfPCell cellInitTitle = new PdfPCell(new Phrase(new Chunk("Initial Inspection (IDEA 10.2.1)", theFont.Bold)));
            cellInitTitle.Colspan = 8;
            cellInitTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellInitTitle.BackgroundColor = new BaseColor(224, 224, 224);
            InitTable.AddCell(cellInitTitle);

            //Initiial Inspection - Content
            //Init1
            PdfPCell cellInit1Text = new PdfPCell(new Phrase(new Chunk("Verify the part number, manufacturer, and quantity match the purchase order and packing slip.", theFont.Bold)));
            cellInit1Text.Colspan = 6;
            InitTable.AddCell(cellInit1Text);
            PdfPCell cellInit1Result = new PdfPCell(new Phrase(convertResult(Detail.initial1_result, Detail.initial1_text), theFont.Normal));
            cellInit1Result.Colspan = 2;
            InitTable.AddCell(cellInit1Result);
            //Init4
            PdfPCell cellInit4Text = new PdfPCell(new Phrase(new Chunk("Confirm the date code meets any restrictions specified on the purchase agreement.", theFont.Bold)));
            cellInit4Text.Colspan = 6;
            InitTable.AddCell(cellInit4Text);
            PdfPCell cellInit4Result = new PdfPCell(new Phrase(convertResult(Detail.initial4_result, Detail.initial4_text), theFont.Normal));
            cellInit4Result.Colspan = 2;
            InitTable.AddCell(cellInit4Result);
            //Add Table
            document.Add(InitTable);




            //Detailed Visual Table
            PdfPTable DetailVisTable = new PdfPTable(8);
            DetailVisTable.WidthPercentage = 100;
            DetailVisTable.SpacingAfter = 5;
            //Detail Visual Title
            PdfPCell cellDetailVisTitle = new PdfPCell(new Phrase(new Chunk("Detailed Inspection(Visual)(IDEA 10.3.1)", theFont.Bold)));
            cellDetailVisTitle.Colspan = 8;
            cellDetailVisTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellDetailVisTitle.BackgroundColor = new BaseColor(224, 224, 224);
            DetailVisTable.AddCell(cellDetailVisTitle);

            //Detail1
            PdfPCell cellDetailVis1Text = new PdfPCell(new Phrase(new Chunk("Verify the logo and markings match the manufacturer's specifications.", theFont.Bold)));
            cellDetailVis1Text.Colspan = 6;
            DetailVisTable.AddCell(cellDetailVis1Text);
            PdfPCell cellDetailVis1Result = new PdfPCell(new Phrase(convertResult(Detail.detail1_result, Detail.detail1_text), theFont.Normal));
            cellDetailVis1Result.Colspan = 2;
            DetailVisTable.AddCell(cellDetailVis1Result);
            //Detail11
            PdfPCell cellDetailVis11Text = new PdfPCell(new Phrase(new Chunk("Are leads in acceptable condition?", theFont.Bold)));
            cellDetailVis11Text.Colspan = 6;
            DetailVisTable.AddCell(cellDetailVis11Text);
            PdfPCell cellDetailVis11Result = new PdfPCell(new Phrase(convertResult(Detail.detail11_result, Detail.detail11_text), theFont.Normal));
            cellDetailVis11Result.Colspan = 2;
            InitTable.AddCell(cellDetailVis11Result);
            DetailVisTable.AddCell(cellDetailVis11Result);
            //Add Table
            document.Add(DetailVisTable);

            //Solvents Table

            //Get Count of List
            //Get List byte[] of Images of Images (Order By Unique_id (int)        
            //Bind images using byte[] from list
            if (SolventImages.Count > 0)
            {

                PdfPTable SolventsTable = new PdfPTable(8);
                SolventsTable.WidthPercentage = 100;
                SolventsTable.SpacingAfter = 5;

                //Solvents Title
                PdfPCell cellSolventsTitle = new PdfPCell(new Phrase(new Chunk("Detailed Inspection (Solvents) (IDEA 10.3.2)", theFont.Bold)));
                cellSolventsTitle.Colspan = 8;
                cellSolventsTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellSolventsTitle.BackgroundColor = new BaseColor(224, 224, 224);
                SolventsTable.AddCell(cellSolventsTitle);

                //Solvent1
                PdfPCell cellSolvents1Text = new PdfPCell(new Phrase(new Chunk("Perform device marking test.", theFont.Bold)));
                cellSolvents1Text.Colspan = 6;
                SolventsTable.AddCell(cellSolvents1Text);
                PdfPCell cellSolvents1Result = new PdfPCell(new Phrase(convertResult(Detail.solvent1_result, Detail.solvent1_text), theFont.Normal));
                cellSolvents1Result.Colspan = 2;
                SolventsTable.AddCell(cellSolvents1Result);
                //Solvent2
                PdfPCell cellSolvents2Text = new PdfPCell(new Phrase(new Chunk("Perform device surface test.", theFont.Bold)));
                cellSolvents2Text.Colspan = 6;
                SolventsTable.AddCell(cellSolvents2Text);
                PdfPCell cellSolvents2Result = new PdfPCell(new Phrase(convertResult(Detail.solvent2_result, Detail.solvent2_text), theFont.Normal));
                cellSolvents2Result.Colspan = 2;
                SolventsTable.AddCell(cellSolvents2Result);
                //Solvent3
                PdfPCell cellSolvents3Text = new PdfPCell(new Phrase(new Chunk("Perform scrape test (as needed).", theFont.Bold)));
                cellSolvents2Text.Colspan = 6;
                SolventsTable.AddCell(cellSolvents3Text);
                PdfPCell cellSolvents3Result = new PdfPCell(new Phrase(convertResult(Detail.solvent3_result, Detail.solvent1_text), theFont.Normal));
                cellSolvents3Result.Colspan = 2;
                SolventsTable.AddCell(cellSolvents3Result);

                //Add Table
                document.Add(SolventsTable);

                //Solvents ImageryTable
                PdfPTable SolventsImageryTable = new PdfPTable(8);
                SolventsImageryTable.WidthPercentage = 100;
                SolventsImageryTable.SpacingAfter = 5;

                PdfPCell cellSolventImageryLabel = null;

                byte[] SolventsImageByte1 = null;
                byte[] SolventsImageByte2 = null;
                byte[] SolventsImageByte3 = null;
                PdfPCell cellSolventsImage1 = null;
                PdfPCell cellSolventsImage2 = null;
                PdfPCell cellSolventsImage3 = null;
                string txtSolvents1 = null;
                string txtSolvents2 = null;
                string txtSolvents3 = null;
                PdfPCell cellSolventsText1 = null;
                PdfPCell cellSolventsText2 = null;
                PdfPCell cellSolventsText3 = null;



                //Bind the Image Variables
                if (SolventImages.Count > 0)
                {
                    //SolventImagery - Label            
                    cellSolventImageryLabel = new PdfPCell(new Phrase(new Chunk("Solvent Imagery:", theFont.Bold)));
                    cellSolventImageryLabel.Colspan = 8;
                    cellSolventImageryLabel.PaddingBottom = 10;
                    cellSolventImageryLabel.Border = Rectangle.NO_BORDER;
                    SolventsImageryTable.AddCell(cellSolventImageryLabel);


                    //Image
                    //SolventsImageByte1 = SolventImages[0].img_blob;
                    SolventsImageByte1 = tools.LoadImageFromPath(SolventImages[0].img_path_web);
                    Image imgSolvents1 = Image.GetInstance(SolventsImageByte1);
                    imgSolvents1.ScalePercent(45);
                    cellSolventsImage1 = new PdfPCell(imgSolvents1, true);
                    cellSolventsImage1.FixedHeight = 85f;
                    cellSolventsImage1.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellSolventsImage1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellSolventsImage1.Border = Rectangle.NO_BORDER;

                    //Text
                    txtSolvents1 = SolventImages[0].img_description;
                    cellSolventsText1 = new PdfPCell(new Phrase(new Chunk(txtSolvents1, theFont.Normal)));
                    cellSolventsText1.Border = Rectangle.NO_BORDER;
                }
                if (SolventImages.Count > 1)
                {
                    //Image
                    //SolventsImageByte2 = SolventImages[1].img_blob;
                    SolventsImageByte2 = tools.LoadImageFromPath(SolventImages[1].img_path_web);
                    tools.LoadImageFromPath(SolventImages[0].img_path_web);
                    Image imgSolvents2 = Image.GetInstance(SolventsImageByte2);
                    imgSolvents2.ScalePercent(45);
                    cellSolventsImage2 = new PdfPCell(imgSolvents2, true);
                    cellSolventsImage2.FixedHeight = 85f;
                    cellSolventsImage2.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellSolventsImage2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellSolventsImage2.Border = Rectangle.NO_BORDER;

                    //Text
                    txtSolvents2 = SolventImages[1].img_description;
                    cellSolventsText2 = new PdfPCell(new Phrase(new Chunk(txtSolvents2, theFont.Normal)));
                    cellSolventsText2.Border = Rectangle.NO_BORDER;
                }
                if (SolventImages.Count > 2)
                {
                    //Image
                    //SolventsImageByte3 = SolventImages[2].img_blob;
                    SolventsImageByte3 = tools.LoadImageFromPath(SolventImages[2].img_path_web);
                    Image imgSolvents3 = Image.GetInstance(SolventsImageByte3);
                    imgSolvents3.ScalePercent(45);
                    cellSolventsImage3 = new PdfPCell(imgSolvents3, true);
                    cellSolventsImage3.FixedHeight = 85f;
                    cellSolventsImage3.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellSolventsImage3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellSolventsImage3.Border = Rectangle.NO_BORDER;

                    //Text
                    txtSolvents3 = SolventImages[2].img_description;
                    cellSolventsText3 = new PdfPCell(new Phrase(new Chunk(txtSolvents3, theFont.Normal)));
                    cellSolventsText3.Border = Rectangle.NO_BORDER;
                }
                //Add Images:                
                switch (SolventImages.Count)
                {


                    case 1:
                        cellSolventsImage1.Colspan = 8;
                        SolventsImageryTable.AddCell(cellSolventsImage1);
                        break;
                    case 2:
                        cellSolventsImage1.Colspan = 4;
                        cellSolventsImage1.PaddingLeft = 5;
                        SolventsImageryTable.AddCell(cellSolventsImage1);
                        cellSolventsImage2.Colspan = 4;
                        cellSolventsImage2.PaddingRight = 5;
                        SolventsImageryTable.AddCell(cellSolventsImage2);
                        break;
                    case 3:
                        cellSolventsImage1.Colspan = 2;
                        SolventsImageryTable.AddCell(cellSolventsImage1);
                        SolventsImageryTable.AddCell(cellSpacer);
                        cellSolventsImage2.Colspan = 2;
                        SolventsImageryTable.AddCell(cellSolventsImage2);
                        SolventsImageryTable.AddCell(cellSpacer);
                        cellSolventsImage3.Colspan = 2;
                        SolventsImageryTable.AddCell(cellSolventsImage3);
                        break;
                }


                //Add Text Fields Below Images
                switch (SolventImages.Count)
                {
                    case 1:
                        cellSolventsText1.Colspan = 8;
                        SolventsImageryTable.AddCell(cellSolventsText1);
                        break;
                    case 2:
                        cellSolventsText1.Colspan = 4;
                        cellSolventsText1.PaddingLeft = 5;
                        SolventsImageryTable.AddCell(cellSolventsText1);
                        cellSolventsText2.Colspan = 4;
                        cellSolventsText2.PaddingRight = 5;
                        SolventsImageryTable.AddCell(cellSolventsText2);
                        break;
                    case 3:
                        cellSolventsText1.Colspan = 2;
                        SolventsImageryTable.AddCell(cellSolventsText1);
                        SolventsImageryTable.AddCell(cellSpacer);
                        cellSolventsText2.Colspan = 2;
                        SolventsImageryTable.AddCell(cellSolventsText2);
                        SolventsImageryTable.AddCell(cellSpacer);
                        cellSolventsText3.Colspan = 2;
                        SolventsImageryTable.AddCell(cellSolventsText3);
                        break;
                }

                document.Add(SolventsImageryTable);
            }

            //Mech Table
            PdfPTable MechTable = new PdfPTable(8);

            if (MechImages.Count > 0)
            {
                //int[] intMechTableWidth = { 10,10,10,10,10,10,10,10 };
                //MechTable.SetWidths(intMechTableWidth);

                MechTable.WidthPercentage = 100;
                float[] colWidths = new float[] { 10f, 20f, 30f, 10f };
                //Mech Title
                PdfPCell cellMechTitle = new PdfPCell(new Phrase(new Chunk("Detailed Inspection (Mechanical) (IDEA 10.3.3)", theFont.Bold)));
                cellMechTitle.Colspan = 8;
                cellMechTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellMechTitle.BackgroundColor = new BaseColor(224, 224, 224);
                MechTable.AddCell(cellMechTitle);

                //Mech1
                PdfPCell cellMech1Text = new PdfPCell(new Phrase(new Chunk("Record dimensions:", theFont.Bold)));
                cellMech1Text.Colspan = 5;
                MechTable.AddCell(cellMech1Text);
                PdfPCell cellMech1Result = new PdfPCell(new Phrase(convertResult(Detail.mech2_text + " X " + Detail.mech3_text), theFont.Normal));
                cellMech1Result.Colspan = 3;
                MechTable.AddCell(cellMech1Result);
                //Add Table
                document.Add(MechTable);


                //Mech ImageryTable
                PdfPTable MechImageryTable = new PdfPTable(8);
                MechImageryTable.WidthPercentage = 100;
                MechImageryTable.SpacingBefore = 5;

                PdfPCell cellMechImageryLabel = null;

                byte[] MechImageByte1 = null;
                byte[] MechImageByte2 = null;
                byte[] MechImageByte3 = null;
                PdfPCell cellMechImage1 = null;
                PdfPCell cellMechImage2 = null;
                PdfPCell cellMechImage3 = null;
                string txtMech1 = null;
                string txtMech2 = null;
                string txtMech3 = null;
                PdfPCell cellMechText1 = null;
                PdfPCell cellMechText2 = null;
                PdfPCell cellMechText3 = null;




                //Bind the Image Variables
                if (MechImages.Count > 0)
                {

                    cellMechImageryLabel = new PdfPCell(new Phrase(new Chunk("Dimensional Imagery:", theFont.Bold)));
                    cellMechImageryLabel.Colspan = 8;
                    cellMechImageryLabel.PaddingBottom = 10;
                    cellMechImageryLabel.Border = Rectangle.NO_BORDER;
                    MechImageryTable.AddCell(cellMechImageryLabel);

                    //Image
                   // MechImageByte1 = MechImages[0].img_blob;
                    MechImageByte1 = tools.LoadImageFromPath(MechImages[0].img_path_web);
                    Image imgMech1 = Image.GetInstance(MechImageByte1);
                    imgMech1.ScalePercent(45);
                    cellMechImage1 = new PdfPCell(imgMech1, true);
                    //imgMech1.ScaleToFit(cellMechImage1);
                    cellMechImage1.FixedHeight = 85f;
                    cellMechImage1.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellMechImage1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellMechImage1.Border = Rectangle.NO_BORDER;

                    //Text
                    txtMech1 = MechImages[0].img_description;
                    cellMechText1 = new PdfPCell(new Phrase(new Chunk(txtMech1, theFont.Normal)));
                    cellMechText1.Border = Rectangle.NO_BORDER;
                }
                if (MechImages.Count > 1)
                {
                    //Image
                    //MechImageByte2 = MechImages[1].img_blob;
                    MechImageByte2 = tools.LoadImageFromPath(MechImages[1].img_path_web);
                    Image imgMech2 = Image.GetInstance(MechImageByte2);
                    imgMech2.ScalePercent(45);
                    cellMechImage2 = new PdfPCell(imgMech2, true);
                    //imgMech2.ScaleToFit(cellMechImage2);
                    cellMechImage2.FixedHeight = 85f;
                    cellMechImage2.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellMechImage2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellMechImage2.Border = Rectangle.NO_BORDER;

                    //Text
                    txtMech2 = MechImages[1].img_description;
                    cellMechText2 = new PdfPCell(new Phrase(new Chunk(txtMech2, theFont.Normal)));
                    cellMechText2.Border = Rectangle.NO_BORDER;
                }
                if (MechImages.Count > 2)
                {
                    //Image
                    //MechImageByte3 = MechImages[2].img_blob;
                    MechImageByte3 = tools.LoadImageFromPath(MechImages[2].img_path_web);

                    Image imgMech3 = Image.GetInstance(MechImageByte3);
                    //imgMech3.ScaleAbsolute(100, 100);  
                    imgMech3.ScalePercent(45);
                    cellMechImage3 = new PdfPCell(imgMech3, true);
                    //imgMech3.ScaleToFit(cellMechImage3);
                    cellMechImage3.FixedHeight = 85f;
                    cellMechImage3.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellMechImage3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellMechImage3.Border = Rectangle.NO_BORDER;

                    //Text
                    txtMech3 = MechImages[2].img_description;
                    cellMechText3 = new PdfPCell(new Phrase(new Chunk(txtMech2, theFont.Normal)));
                    cellMechText3.Border = Rectangle.NO_BORDER;
                }


                //Add Cells and Build Table Layout based on exsiting count of images.
                switch (MechImages.Count)
                {
                    case 1:
                        cellMechImage1.Colspan = 8;
                        MechImageryTable.AddCell(cellMechImage1);
                        break;
                    case 2:
                        cellMechImage1.Colspan = 4;
                        cellMechImage1.PaddingLeft = 5;
                        MechImageryTable.AddCell(cellMechImage1);
                        cellMechImage2.Colspan = 4;
                        cellMechImage2.PaddingRight = 5;
                        MechImageryTable.AddCell(cellMechImage2);
                        break;
                    case 3:
                        cellMechImage1.Colspan = 2;
                        MechImageryTable.AddCell(cellMechImage1);
                        MechImageryTable.AddCell(cellSpacer);
                        cellMechImage2.Colspan = 2;
                        MechImageryTable.AddCell(cellMechImage2);
                        MechImageryTable.AddCell(cellSpacer);
                        cellMechImage3.Colspan = 2;
                        MechImageryTable.AddCell(cellMechImage3);
                        break;
                }

                //Add Text Fields Below Images
                switch (MechImages.Count)
                {
                    case 1:
                        cellMechText1.Colspan = 8;
                        cellMechText1.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellMechText1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        MechImageryTable.AddCell(cellMechText1);
                        break;
                    case 2:
                        cellMechText1.Colspan = 4;
                        cellMechText1.PaddingLeft = 5;
                        cellMechText1.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellMechText1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        MechImageryTable.AddCell(cellMechText1);
                        cellMechText2.Colspan = 4;
                        cellMechText2.PaddingRight = 5;
                        cellMechText2.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellMechText2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        MechImageryTable.AddCell(cellMechText2);
                        break;
                    case 3:
                        cellMechText1.Colspan = 2;
                        cellMechText1.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellMechText1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        MechImageryTable.AddCell(cellMechText1);
                        MechImageryTable.AddCell(cellSpacer);
                        cellMechText2.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellMechText2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cellMechText2.Colspan = 2;
                        MechImageryTable.AddCell(cellMechText2);
                        MechImageryTable.AddCell(cellSpacer);
                        cellMechText3.Colspan = 2;
                        cellMechText3.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellMechText3.VerticalAlignment = Element.ALIGN_MIDDLE;
                        MechImageryTable.AddCell(cellMechText3);
                        break;
                }
                document.Add(MechImageryTable);
            }


            //Additional Testing Imagery Table
            PdfPTable AddTestingTable = new PdfPTable(8);

            if (AddTestingImages.Count > 0)
            {
                AddTestingTable.WidthPercentage = 100;
                float[] colWidths = new float[] { 10f, 20f, 30f, 10f };
                //AddTesting Title
                PdfPCell cellAddTestingTitle = new PdfPCell(new Phrase(new Chunk("Additional Testing Performed", theFont.Bold)));
                cellAddTestingTitle.Colspan = 8;
                cellAddTestingTitle.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellAddTestingTitle.BackgroundColor = new BaseColor(224, 224, 224);
                AddTestingTable.AddCell(cellAddTestingTitle);

                //Additional Testing Text
                PdfPCell cellAddTestingText = new PdfPCell(new Phrase(new Chunk("Details:", theFont.Bold)));
                cellAddTestingText.Colspan = 2;
                AddTestingTable.AddCell(cellAddTestingText);
                PdfPCell cellAddTestingResult = new PdfPCell(new Phrase(convertResult(Detail.addtesting_text), theFont.Normal));
                cellAddTestingResult.Colspan = 6;
                AddTestingTable.AddCell(cellAddTestingResult);
                //Add Table
                //document.Add(AddTestingTable);


                //Additional Testing ImageryTable
                PdfPTable AddTestingImageryTable = new PdfPTable(8);
                AddTestingImageryTable.WidthPercentage = 100;
                AddTestingImageryTable.SpacingBefore = 5;

                PdfPCell cellAddTestingImageryLabel = null;

                byte[] AddTestingImageByte1 = null;
                byte[] AddTestingImageByte2 = null;
                byte[] AddTestingImageByte3 = null;
                PdfPCell cellAddTestingImage1 = null;
                PdfPCell cellAddTestingImage2 = null;
                PdfPCell cellAddTestingImage3 = null;
                string txtAddTesting1 = null;
                string txtAddTesting2 = null;
                string txtAddTesting3 = null;
                PdfPCell cellAddTestingText1 = null;
                PdfPCell cellAddTestingText2 = null;
                PdfPCell cellAddTestingText3 = null;




                //Bind the Image Variables
                if (AddTestingImages.Count > 0)
                {

                    cellAddTestingImageryLabel = new PdfPCell(new Phrase(new Chunk("Additional Testing Imagery:", theFont.Bold)));
                    cellAddTestingImageryLabel.Colspan = 8;
                    cellAddTestingImageryLabel.PaddingBottom = 10;
                    cellAddTestingImageryLabel.Border = Rectangle.NO_BORDER;
                    AddTestingImageryTable.AddCell(cellAddTestingImageryLabel);

                    //Image
                    //AddTestingImageByte1 = AddTestingImages[0].img_blob;
                    AddTestingImageByte1 = tools.LoadImageFromPath(AddTestingImages[0].img_path_web);
                    Image imgAddTesting1 = Image.GetInstance(AddTestingImageByte1);
                    imgAddTesting1.ScalePercent(45);
                    cellAddTestingImage1 = new PdfPCell(imgAddTesting1, true);
                    //imgAddTesting1.ScaleToFit(cellAddTestingImage1);
                    cellAddTestingImage1.FixedHeight = 85f;
                    cellAddTestingImage1.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellAddTestingImage1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellAddTestingImage1.Border = Rectangle.NO_BORDER;

                    //Text
                    txtAddTesting1 = AddTestingImages[0].img_description;
                    cellAddTestingText1 = new PdfPCell(new Phrase(new Chunk(txtAddTesting1, theFont.Normal)));
                    cellAddTestingText1.Border = Rectangle.NO_BORDER;
                }
                if (AddTestingImages.Count > 1)
                {
                    //Image
                    //AddTestingImageByte2 = AddTestingImages[1].img_blob;
                    AddTestingImageByte2 = tools.LoadImageFromPath(AddTestingImages[1].img_path_web);

                    Image imgAddTesting2 = Image.GetInstance(AddTestingImageByte2);
                    imgAddTesting2.ScalePercent(45);
                    cellAddTestingImage2 = new PdfPCell(imgAddTesting2, true);
                    //imgAddTesting2.ScaleToFit(cellAddTestingImage2);
                    cellAddTestingImage2.FixedHeight = 85f;
                    cellAddTestingImage2.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellAddTestingImage2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellAddTestingImage2.Border = Rectangle.NO_BORDER;

                    //Text
                    txtAddTesting2 = AddTestingImages[1].img_description;
                    cellAddTestingText2 = new PdfPCell(new Phrase(new Chunk(txtAddTesting2, theFont.Normal)));
                    cellAddTestingText2.Border = Rectangle.NO_BORDER;
                }
                if (AddTestingImages.Count > 2)
                {
                    //Image
                    //AddTestingImageByte3 = AddTestingImages[2].img_blob;
                    AddTestingImageByte3 = tools.LoadImageFromPath(AddTestingImages[2].img_path_web);

                    Image imgAddTesting3 = Image.GetInstance(AddTestingImageByte3);
                    //imgAddTesting3.ScaleAbsolute(100, 100);  
                    imgAddTesting3.ScalePercent(45);
                    cellAddTestingImage3 = new PdfPCell(imgAddTesting3, true);
                    //imgAddTesting3.ScaleToFit(cellAddTestingImage3);
                    cellAddTestingImage3.FixedHeight = 85f;
                    cellAddTestingImage3.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellAddTestingImage3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellAddTestingImage3.Border = Rectangle.NO_BORDER;

                    //Text
                    txtAddTesting3 = AddTestingImages[2].img_description;
                    cellAddTestingText3 = new PdfPCell(new Phrase(new Chunk(txtAddTesting2, theFont.Normal)));
                    cellAddTestingText3.Border = Rectangle.NO_BORDER;
                }


                //Add Cells and Build Table Layout based on exsiting count of images.
                switch (AddTestingImages.Count)
                {
                    case 1:
                        cellAddTestingImage1.Colspan = 8;
                        AddTestingImageryTable.AddCell(cellAddTestingImage1);
                        break;
                    case 2:
                        cellAddTestingImage1.Colspan = 4;
                        cellAddTestingImage1.PaddingLeft = 5;
                        AddTestingImageryTable.AddCell(cellAddTestingImage1);
                        cellAddTestingImage2.Colspan = 4;
                        cellAddTestingImage2.PaddingRight = 5;
                        AddTestingImageryTable.AddCell(cellAddTestingImage2);
                        break;
                    case 3:
                        cellAddTestingImage1.Colspan = 2;
                        AddTestingImageryTable.AddCell(cellAddTestingImage1);
                        AddTestingImageryTable.AddCell(cellSpacer);
                        cellAddTestingImage2.Colspan = 2;
                        AddTestingImageryTable.AddCell(cellAddTestingImage2);
                        AddTestingImageryTable.AddCell(cellSpacer);
                        cellAddTestingImage3.Colspan = 2;
                        AddTestingImageryTable.AddCell(cellAddTestingImage3);
                        break;
                }

                //Add Text Fields Below Images
                switch (AddTestingImages.Count)
                {
                    case 1:
                        cellAddTestingText1.Colspan = 8;
                        cellAddTestingText1.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellAddTestingText1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        AddTestingImageryTable.AddCell(cellAddTestingText1);
                        break;
                    case 2:
                        cellAddTestingText1.Colspan = 4;
                        cellAddTestingText1.PaddingLeft = 5;
                        cellAddTestingText1.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellAddTestingText1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        AddTestingImageryTable.AddCell(cellAddTestingText1);
                        cellAddTestingText2.Colspan = 4;
                        cellAddTestingText2.PaddingRight = 5;
                        cellAddTestingText2.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellAddTestingText2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        AddTestingImageryTable.AddCell(cellAddTestingText2);
                        break;
                    case 3:
                        cellAddTestingText1.Colspan = 2;
                        cellAddTestingText1.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellAddTestingText1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        AddTestingImageryTable.AddCell(cellAddTestingText1);
                        AddTestingImageryTable.AddCell(cellSpacer);
                        cellAddTestingText2.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellAddTestingText2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cellAddTestingText2.Colspan = 2;
                        AddTestingImageryTable.AddCell(cellAddTestingText2);
                        AddTestingImageryTable.AddCell(cellSpacer);
                        cellAddTestingText3.Colspan = 2;
                        cellAddTestingText3.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellAddTestingText3.VerticalAlignment = Element.ALIGN_MIDDLE;
                        AddTestingImageryTable.AddCell(cellAddTestingText3);
                        break;
                }



                //Prevent This table from getting split, i.e. force it to new page by nesting inside another row (which by default itextsharp won't split rows)

                PdfPTable nesting = new PdfPTable(1);
                nesting.WidthPercentage = 100;
                nesting.KeepTogether = true;
                PdfPCell cell1 = new PdfPCell(AddTestingTable);
                PdfPCell cell2 = new PdfPCell(AddTestingImageryTable);
                cell2.Border = Rectangle.NO_BORDER;
                nesting.AddCell(cell1);
                nesting.AddCell(cell2);
                document.Add(nesting);
            }


            document.Close();
            bytes = MS.ToArray();
        }

        string filename = "Sensible_Micro_IDEA_Summary_" + insp_id.ToString() + ".pdf";
        //Transmit the File
        Response.Clear();
        Response.ContentType = "Application/pdf";
        Response.ClearHeaders();
        Response.ClearContent();

        //Tell the browser that you want the file downloaded (ideally) and give it a pretty filename
        Response.AddHeader("content-disposition", "attachment;filename=" + filename);
        //Write our bytes to the stream
        Response.BinaryWrite(bytes);
        //Close the stream (otherwise ASP.Net might continue to write stuff on our behalf)
        Response.End();
    }


    private PdfPTable GetNewSectionTable()
    {

        PdfPTable ret = smpdf.CreateTable(1, 100);
        ret.DefaultCell.Border = Rectangle.NO_BORDER;
        ret.KeepTogether = true;
        return ret;
    }

    protected void LoadReport()
    {

        //GetImageDataTable();
        LoadImages();
        //LoadImageText();
        LoadHeaderValues();
        LoadDetailValues();



    }

    protected void btnCreatePDF_Click(object sender, EventArgs e)
    {
        try
        {


            DownloadPDF();
            HandleResult("PDF created successfully", false);
        }
        catch (DocumentException dex)
        {
            HandleResult(dex.Message, true);
        }
        catch (IOException ioex)
        {
            HandleResult(ioex.Message, true);
        }
        catch (Exception ex)
        {
            HandleResult(ex.Message, true);
        }
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/secure_members/qc_center/my_qc.aspx", false);
    }



    protected bool CheckPermissions()
    {


        if (Roles.IsUserInRole("sm_internal_executive") || Roles.IsUserInRole("sm_insp_user") || Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_internal"))
        {
            return true;
        }
        //else if (Roles.IsUserInRole("sm_internal"))
        //{
        //    if (IsDistySalesAllowed())
        //        return true;
        //    if (GetApprovedUsersInternal())
        //        return true;
        //}
        else
        {
            GetInspectionsOwnedByUser();  //Populate ApprovedInspID
            if (ApprovedInspID.Contains(insp_id))
                return true;
        }

        return false;


    }




    private bool GetApprovedUsersInternal()
    {
        company c = null;
        n_user companyOwner = null;
        n_user relationshipManager = null;
        List<n_user> approvedUsers = new List<n_user>();

        if (!string.IsNullOrEmpty(Header.customer_uid))
        {
            c = RZDC.companies.Where(w => w.unique_id == Header.customer_uid).FirstOrDefault();
            if (c != null)
            {
                companyOwner = RZDC.n_users.Where(w => w.unique_id == c.base_mc_user_uid).FirstOrDefault();
                if (companyOwner != null)
                {
                    approvedUsers.Add(companyOwner);
                    relationshipManager = RZDC.n_users.Where(w => w.assistant_to_uid == companyOwner.unique_id).FirstOrDefault();
                    {
                        if (relationshipManager != null)
                        {
                            approvedUsers.Add(relationshipManager);
                        }

                    }
                }
            }
        }
        if (approvedUsers.Select(s => s.unique_id).ToList().Contains(Profile.RzUserID))
            return true;
        return false;
    }

    private bool IsDistySalesAllowed()
    {

        //Disty Sales
        RzTools rzt = new RzTools();

        company c = RZDC.companies.Where(w => w.unique_id == Header.customer_uid).FirstOrDefault();  //if The company type or agent is like Vendor
        n_user u = null;
        if (c != null)
            if (c.agent.ToLower() == "vendor" || c.companytype.ToLower().Contains("vendor"))
                u = RZDC.n_users.Where(w => w.unique_id == Profile.RzUserID).FirstOrDefault(); //If the current user is in disty sales
        if (u != null)
            if (rzt.isUserOnTeam(u, "distributor sales"))
                return true;
        return false;
    }

    protected void HandleResult(string message, bool fail)
    {
        divPageResult.Visible = true;
        lblPageResult.Text = message;
        if (fail)
            divPageResult.Attributes["class"] = "alert alert-danger";
        else
            divPageResult.Attributes["class"] = "alert alert-success";
        return;

    }

}


public class PDFFooter : PdfPageEventHelper
{
    // write on top of document
    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        base.OnOpenDocument(writer, document);
        //PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        //tabFot.SpacingAfter = 10F;
        //PdfPCell cell;
        //tabFot.TotalWidth = 300F;
        //cell = new PdfPCell(new Phrase("Header"));
        //tabFot.AddCell(cell);
        //tabFot.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
    }

    // write on start of each page
    public override void OnStartPage(PdfWriter writer, Document document)
    {
        base.OnStartPage(writer, document);
    }

    // write on end of each page
    public override void OnEndPage(PdfWriter writer, Document document)
    {
        base.OnEndPage(writer, document);
        //PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        PdfPTable tabFot = new PdfPTable(8);
        PdfPCell cell;
        Font SmallFont = FontFactory.GetFont("georgia", 10f, Font.NORMAL);
        tabFot.TotalWidth = document.Right - document.Left;
        tabFot.HorizontalAlignment = Element.ALIGN_CENTER;
        string currentYear = DateTime.Today.Year.ToString();
        cell = new PdfPCell(new Phrase(new Chunk("\u00A9Copyright " + currentYear + " - Sensible Micro Corporation", SmallFont)));
        cell.Border = Rectangle.NO_BORDER;
        cell.Colspan = 8;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        tabFot.AddCell(cell);
        //KT setting the xstart position to 40, only way I can center footer.
        tabFot.WriteSelectedRows(0, -1, 40, document.Bottom, writer.DirectContent);

        //Footer Table
        //PdfPTable FooterTable = new PdfPTable(8);
        //FooterTable.WidthPercentage = 100;
        //PdfPCell cellFooterText = new PdfPCell(new Phrase(new Chunk("\u00A9 Copyright 2016 Sensible Micro Corporation", SmallFont)));
        //cellFooterText.Colspan = 8;
        //cellFooterText.Border = Rectangle.NO_BORDER;
        //cellFooterText.HorizontalAlignment = Element.ALIGN_CENTER;
        //FooterTable.AddCell(cellFooterText);
        //document.Add(FooterTable);
    }

    //write on close of document
    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
        base.OnCloseDocument(writer, document);
    }
}