﻿using System;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using SensibleDAL;
using System.Collections.Generic;

public partial class secure_members_qc_center_my_qc : System.Web.UI.Page
{

    SM_Tools tools = new SM_Tools();
    RzDataContext rdc = new RzDataContext();
    SM_Quality_Logic ql = new SM_Quality_Logic();
    bool ShowSamples = false;
    company CurrentCompany;
    string CompanyID;
    string CustomerPO;
    //http://localhost:50477/secure_members/qc_center/my_qc.aspx?cpo=C111947&ci=05d8cf3efd664006a294ad37ef252cf2&utm_source=Invoice-Email&utm_medium=EMAIL&utm_campaign=YOUR_ORDER_SHIPPED

    protected void smcb_CheckedChanged(object sender, EventArgs e)
    {
        ShowSamples = smcb.isChecked;

        BindMyQCGrid();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        smcb.CheckedChanged += new EventHandler(smcb_CheckedChanged);
        //gvMyQC.theGridView.SelectedIndexChanged += new EventHandler(gvMyQC_SelectedIndexChanged);
        LoadCheckbox();
        tools.ShowNavHelp(Page);

        LoagQuerystringVariables();

        if (!Page.IsPostBack)
            BindMyQCGrid();


    }

    private void LoagQuerystringVariables()
    {
        //Customer PO
        string strCpo = Tools.Strings.SanitizeInput(Request.QueryString["cpo"]);
        if (!string.IsNullOrEmpty(strCpo))
            CustomerPO = strCpo;

        //Company ID
        string strCompanyID = Tools.Strings.SanitizeInput(Request.QueryString["ci"]);
        if (!string.IsNullOrEmpty(strCompanyID))
            CompanyID = strCompanyID;

    }

    private void LoadCheckbox()
    {
        smcb.theText = "View Sample GCAT and IDEA test reports.";
    }

    private void BindMyQCGrid()
    {
        DataTable dt = GetMyQCData();
        if (dt == null)
        {
            gvMyQC.DataSource = null;
            gvMyQC.DataBind();
        }
        else
        {
            var query = dt.AsEnumerable().OrderByDescending(o => o.Field<DateTime>("date")).Select(s => new
            {
                ID = s.Field<string>("id"),
                Type = s.Field<string>("testtype"),
                Part = s.Field<string>("part_number"),
                Date = s.Field<DateTime>("date").ToShortDateString(),
                PO = s.Field<string>("customer_po"),
                Company = s.Field<string>("companyname"),
            });
            gvMyQC.DataSource = query;
            gvMyQC.DataBind();



            if (gvMyQC.Rows.Count > 0) //Required for DataTables
            {
                gvMyQC.HeaderRow.TableSection = TableRowSection.TableHeader;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "headerDatatable", "loadHeaderDatatable();", true);
                tools.LoadGridDataDataTablesNetDefault(Page, gvMyQC);
            }
        }






    }


    protected DataTable GetMyQCData()
    {



        if (SM_Global.isDemoUser)
        {
            ShowSamples = true;
            smcb.Visible = false;
        }

        DataTable dt = new DataTable();
        string searchstring = null;

        if (ShowSamples)
        {
            return ql.GetGCATandIDEADataTable(null, searchstring, "Both", true);
        }
        else
        {
            //Set teh current company (Preview / Internal View / CUstomer)
            SetCurrentCompany();           

            if (CurrentCompany != null)
                return ql.GetGCATandIDEADataTable(CurrentCompany.unique_id, searchstring, "Both", false, CustomerPO);
            else
                return null;

        }
    }

    private void SetCurrentCompany()
    {
        //IF Current User is internal or admin, and there is a companyid and cpo present, allow access.
        if (AllowInternalCompanyPreviewAccess())
            return;
        //If querystring provided, load CurrentCompany from that.
        if (LoadCompanyByQuerystring())
            return;
        //Finally we're here, we're pulling from Global Company.
        CurrentCompany = SM_Global.CurrentCompany;


    }

    private bool AllowInternalCompanyPreviewAccess()
    {
        if (Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_internal"))
        {
            //Check if Preview Company is Currently Set
            if (SM_Global.PreviewCompany != null)
            {
                CurrentCompany = SM_Global.PreviewCompany;
                return true;
            }
        }
        return false;
    }

    private bool LoadCompanyByQuerystring()
    {
        if (string.IsNullOrEmpty(CompanyID))
            return false;
        using (RzDataContext rdc = new RzDataContext())
        {
            company c = rdc.companies.Where(w => w.unique_id == CompanyID).FirstOrDefault();
            if (c != null)
            {
                //For Customers, in case they got a copy of another customer's URL, need to confirm they own this.
                if (!Roles.IsUserInRole("admin") || !Roles.IsUserInRole("sm_internal"))
                    if (c.unique_id != Profile.CompanyID)
                        return false;
                //If we're here, we're either internal, or the querysting is matches customer company ID.
                CurrentCompany = c;
                return true;
            }
        }
        return false;
    }

    protected void lbClear_Click(object sender, EventArgs e)
    {

        var uri = new Uri(Page.Request.Url.ToString());
        string path = uri.GetLeftPart(UriPartial.Path);
        Page.Response.Redirect(path, false);
    }
    protected void lbSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindMyQCGrid();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    protected void HandleResult(string message, bool fail)
    {
        divPageResult.Visible = true;
        lblPageResult.Text = message;
        if (fail)
            divPageResult.Attributes["class"] = "alert alert-danger";
        else
            divPageResult.Attributes["class"] = "alert alert-success";
        return;
    }



    protected void gvMyQC__RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string testtype = e.Row.Cells[1].Text;
            string id = e.Row.Cells[2].Text;

            HyperLink hyperlink = (HyperLink)e.Row.FindControl("hlinkViewInspection");
            hyperlink.Target = "_Blank";
            switch (testtype)
            {
                case "GCAT":
                    {
                        hyperlink.NavigateUrl = "~/secure_members/qc_center/gcat_report.aspx?id=" + id;
                        break;
                    }
                case "IDEA":
                    {
                        hyperlink.NavigateUrl = "~/secure_members/qc_center/IDEA_summary_report.aspx?insp_id=" + id;
                        break;
                    }
            }


            if (e.Row.RowIndex == 0)
            {
                e.Row.Attributes.Add("data-intro", "Click the icon to access your reports where you can view high-resolution imagery, inspection details, and download PDF versions");
                e.Row.Attributes.Add("data-position", "left");
            }

        }
    }

    protected void ddlTestType_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindMyQCGrid();
    }

    protected void ldsMyQC_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        e.Result = GetMyQCData();
    }

    protected void gvMyQC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindMyQCGrid();
    }

    protected void gvMyQC_SelectedIndexChanged(object sender, EventArgs e)
    {

        GridView gv = (GridView)sender;
        GridViewRow row = gv.SelectedRow;
        if (row != null)
        {
            string id = row.Cells[1].Text;
            string type = row.Cells[2].Text;
            string url = GetReportUrl(type, id);
            Response.Redirect(url, false);
        }
    }

    private string GetReportUrl(string type, string id)
    {
        switch (type)
        {
            case "GCAT":
                return "~/secure_members/qc_center/gcat_report.aspx?id=" + id;
            case "IDEA":
                return "~/secure_members/qc_center/IDEA_summary_report.aspx?insp_id=" + id;
        }
        return null;
    }

    protected void gvMyQC_Sorting(object sender, GridViewSortEventArgs e)
    {
        BindMyQCGrid();
    }
}