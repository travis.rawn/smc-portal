﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_members_qc_center_gcat_report : System.Web.UI.Page
{
    private int GCATID
    {
        get { return Convert.ToInt32(ViewState["GCATID"] ?? GetGCATID()); }
        set { ViewState["GCATID"] = value; }
    }
    private string MPN
    {
        get { return ViewState["MPN"].ToString(); }
        set { ViewState["MPN"] = value; }
    }

    private string MFG
    {
        get { return ViewState["MFG"].ToString(); }
        set { ViewState["MFG"] = value; }
    }
    private int MPNID
    {
        get { return Convert.ToInt32(ViewState["MPNID"]); }
        set { ViewState["MPNID"] = value; }
    }

    private List<string> SelectedTests
    {
        get { return (List<string>)ViewState["SelectedTests"]; }
        set { ViewState["SelectedTests"] = value; }
    }



    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    gcatDataContext ciq = new gcatDataContext();
    RzDataContext rdc = new RzDataContext();
    sm_binaryEntities smb = new sm_binaryEntities();
    SM_Quality_Logic ql = new SM_Quality_Logic();

    ordhed_sale sale;
    orddet_line line;

    ValidationReport vr;
    ManufacturerPart mp;
    CustomerPart cp;
    MarkingPermanency mperm;
    XRay xray;
    HeatedSolvent hs;
    Decapsulation decap;
    Solderability sol;
    EPROM ep;
    SentryVI sv;
    XRF xrf;
    BasicElectrical be;
    ExtendedTesting et;
    MoistureBaking mb;
    Summary_new sum;
    SM_GCAT gcat;
    List<insp_images> listExtendedImages = new List<insp_images>();

    public List<InspectionImageManagerControl> ImageManagerControlList { get; private set; }

    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreLoad(e);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        tools.ShowNavHelp(Page);

        try
        {
            init();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
            return;
        }

    }

    protected void init()
    {
        LoadImageManagerControlList();
        SetImageControlProperties();
        if (!Page.IsPostBack)
        {
            string strGcatID = Tools.Strings.SanitizeInput(Request.QueryString["id"]);
            if (!string.IsNullOrEmpty(strGcatID))
            {
                if (Convert.ToInt32(strGcatID) != 0)
                {
                    GCATID = Convert.ToInt32(strGcatID);
                    if (!check_security())
                        throw new Exception("Not a valid GCAT ID for this user.");
                    gcat = new SM_GCAT(GCATID);
                    LoadAll();
                }
            }

        }
    }

    protected int GetGCATID()
    {
        int gcatid;
        //check querystring
        if (Tools.Strings.SanitizeInput(Request.QueryString["id"]) == null)//NEw GCAT
        {
            return 0;
        }
        else
        {
            if (vr != null)
                gcatid = Convert.ToInt32(vr.ValidationReportID);
            else
                return 0;
        }
        return gcatid;

    }
    protected bool check_security()
    {
        string userName = System.Web.Security.Membership.GetUser().UserName;
        if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_internal"))
            return true;
        List<int> AllowedGCATIDs = new List<int>();
        AllowedGCATIDs = ciq.ValidationReports.Where(w => w.company_uid == Profile.CompanyID).Select(s => s.ValidationReportID).ToList();
        AllowedGCATIDs.AddRange(ciq.ValidationReports.Where(w => w.is_demo ?? false == true).Select(s => s.ValidationReportID).ToList());
        if (!AllowedGCATIDs.Contains(GCATID))
            return false;
        else
            return true;
    }

    protected void LoadAll()
    {
        if (!Page.IsPostBack)
        {
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
            if (vr == null)
            {
                tools.HandleResultJS("Invalid GCAT ID.", true, Page);
                return;
            }
            //LoadPageControls();
            if (vr.is_deleted != true)
            {
                GetManufacturerPart(vr.MPNID);
                ShowMainGCATForm();
                LoadHeader();
                LoadRequiredTests();
                LoadComponentIdentity();
                LoadVisualComponent();
                LoadVisualSurface();
                LoadMarkingPerm();
                LoadXray();
                LoadHeatedSolvents();
                LoadDecap();
                LoadSolderability();
                LoadEprom();
                LoadBasicElectrical();
                LoadSentry();
                LoadXRF();
                LoadExtendedTesting();
                LoadMoistureBaking();
                LoadExtendedImagery();
                LoadSummary();
                MarkImported();
                if (!vr.is_imported)
                    MarkImported();
            }
            else
            {
                ShowStatusDiv("danger", "deleted");
                //pnlManufacturerPartSearch.Visible = false;
            }
        }
    }

    private void LoadExtendedImagery()
    {
        listExtendedImages = ql.LoadInspectionImagery("GCAT", GCATID, "ExtendedImagery");
        if (listExtendedImages.Count > 0)
        {
            tabExtendedImagery.Visible = true;
            aExtendedImagery.Visible = true;
            //LoadSectionImagery("ExtendedImagery", true);
            iig.ImageList = listExtendedImages;
            iig.ShowDescription = true;
            iig.EnableControls = false;
            iig.LoadGallery();
        }
        else
        {
            aExtendedImagery.Visible = false;
            tabExtendedImagery.Visible = false;
        }

    }

    private void MarkImported() //Use this after initial load from old system to new.  When yes, check on image load so subsequently deleted (purposefully) images won't get re-imported
    {
        if (vr == null)
            return;
        vr.is_imported = true;
        ciq.SubmitChanges();
    }

    protected void LoadHeader()
    {
        //Date & ID
        lblReportID.Text = vr.ValidationReportID.ToString();
        lblDate.Text = vr.ReportDate.Value.ToString("d");

        pnlHeaderData.Visible = true;
        lblCustomer.Text = vr.company_name;
        lblSalesOrder.Text = vr.SalesOrderNumber;

        //Sale Data
        sale = rdc.ordhed_sales.Where(w => w.unique_id == vr.orderid_sales).SingleOrDefault();
        if (sale != null)
        {
            lblHeaderCustomerPO.Text = sale.orderreference;

        }

        if (vr.AccountManagerID != 0)
        {//NO longer saving AccountManagerID.  If it exists (i.e. old GCAT) show that
            lblHeaderAccountManager.Text = null;
            lblHeaderAccountManager.Text = ciq.AccountManagers.Where(w => w.AccountManagerID == vr.AccountManagerID).Select(s => s.FullName).SingleOrDefault();
        }

        if (string.IsNullOrEmpty(lblHeaderAccountManager.Text))
        {//Else show the Rz Integrated Account Manager            
            lblHeaderAccountManager.Text = vr.agent_name;
        }

        //Line Data
        line = rdc.orddet_lines.Where(w => w.unique_id == vr.orddet_line_uid).SingleOrDefault();
        if (line != null)
        {
            lblCustomerPart.Text = line.internal_customer;
        }

        if (vr.Quantity1 > 0)
        {
            lblQuantity1.Text = vr.Quantity1.ToString();
        }
        else
            lblQuantity1.Text = "See Summary Notes";

        //Summary Notes
        lblHeaderSummaryNotes.Text = vr.summaryNotes;
        //txtHeaderSummaryNotes.Text = vr.summaryNotes;

        LoadSectionImagery("Header", false);
        //LoadImage(imgHeader1, vr.Image_Top);
        //LoadImage(imgHeader2, vr.Image_Bottom);

        cp = ciq.CustomerParts.Where(s => s.MPNID == vr.MPNID).SingleOrDefault();
    }


    protected void LoadRequiredTests()
    {
        if (vr != null)
        {
            GetAssociatedTests();
            //Standardized Tests
            lblStandardizedTests.Text = gcat.GetStandardTests(vr);
            //Customer Requirement
            lblCustomer_RequirementA.Text = gcat.GetCustomerRequirements(vr);
            //GCAT Tests Performed
            lblTestsPerformed.Text = gcat.GetTestsPerformed(vr);
            //Additional Tests
            lblTest_TypeJ.Text = vr.Test_TypeJ;
        }
    }


    protected void GetDDlValue(string dbValue, DropDownList ddl)
    {
        if (ddl.Items.FindByText(dbValue) != null)
            ddl.SelectedValue = dbValue;
        else
        {
            ddl.Items.Add(new ListItem(dbValue, dbValue));
            ddl.SelectedValue = dbValue;
        }
        ddl.Visible = true;
    }
    protected void GetCBXValue(string dbValue, CheckBox cbx)
    {
        switch (dbValue)
        {
            case "De-Capsulation":
                cbx.Checked = true;
                break;
            case "Dimensional":
                cbx.Checked = true;
                break;
            case "IDEA-1010 Detailed Visual":
                cbx.Checked = true;
                break;
            case "Heated Solvents":
                cbx.Checked = true;
                break;
            case "Marking Permanency":
                cbx.Checked = true;
                break;
            case "Sentry V-I Signature":
                cbx.Checked = true;
                break;
            case "Solderability":
                cbx.Checked = true;
                break;
            case "X-Ray":
                cbx.Checked = true;
                break;
            case "XRF Analysis":
                cbx.Checked = true;
                break;
            default:
                cbx.Checked = false;
                break;

        }
    }


    protected void LoadComponentIdentity()
    {


        if (mp != null)
        {
            tabMarkingPerm.Visible = true;
            lblComponentIdentityRohs_compliant.Text = gcat.GetResult(mp.RoHS_Compliant, true, false);

            lblComponentIdentityCondition.Text = SetComponentIdentityCondition();


            //Memory Device
            lblComponentIdentityMemoryDevice.Text = gcat.GetResult(mp.MemoryDevice, true, false);


            //Component Type
            lblComponentIdentityType.Text = SetComponentIdentityType();

            //MSD / MSL
            lblComponentIdentityMoistureLevel.Text = mp.MoistureLevel ?? "0";

            //Traceability
            lblComponentIdentityTraceability.Text = gcat.GetResult(vr.Traceability, true, false);

            //Characteristics
            lblComponentIdentityPackageType.Text = mp.PackageType;

            lblComponentIdentityLeadPinBGAPitch.Text = mp.Lead_PinPitch;

            lblComponentIdentityLeadPinBGACount.Text = mp.Lead_PinCount;

            lblComponentIdentityLength.Text = mp.Length;

            lblComponentIdentityWidth.Text = mp.Width;

            lblComponentIdentityThickness.Text = mp.Thickness;



        }
        else
            tabMarkingPerm.Visible = false;


    }

    private string SetComponentIdentityCondition()
    {
        string s = "";
        List<string> conditionList = new List<string>();
        if (vr.New == true)
            conditionList.Add("New");
        if (vr.Used == true)
            conditionList.Add("Used");
        if (vr.Refurbished == true)
            conditionList.Add("Refurbished");
        if (conditionList.Count > 0)
            s = string.Join(", ", conditionList);
        return s;

    }

    private string SetComponentIdentityType()
    {
        string s = "";
        List<string> typeList = new List<string>();
        if (mp.SurfaceMount == true)
            typeList.Add("Surface Mount");
        if (mp.ThroughHole == true)
            typeList.Add("Through Hole");
        if (mp.BGA == true)
            typeList.Add("BGA");
        if (typeList.Count > 0)
            s = string.Join(", ", typeList);
        return s;

    }

    protected void LoadVisualComponent()
    {
        if (vr != null)
        {
            lblVisualComponentSamplyQTY.Text = vr.VisualComponentSampleQty.ToString();
            //External Visual
            lblVisualComponentWidthActual.Text = (vr.Width_Actual.ToString() ?? "N/A") + vr.UnitWidth;
            //txtVisualComponentWidthUnit.Text = vr.UnitWidth;
            lblVisualComponentPassWidthComment.Text = vr.CommentsWidth;
            lblVisualComponentPassWidth.Text = gcat.GetResult(vr.Pass_W);

            lblVisualComponentLengthActual.Text = (vr.Length_Actual.ToString() ?? "N/A") + vr.UnitLength;
            //txtVisualComponentLengthUnit.Text = vr.UnitLength;
            lblVisualComponentPassLengthComment.Text = vr.CommentsLength;
            lblVisualComponentPassLength.Text = gcat.GetResult(vr.Pass_L);

            lblVisualComponentThicknessActual.Text = (vr.Thickness_Actual.ToString() ?? "N/A") + vr.UnitThickness;
            //txtVisualComponentThicknessUnit.Text = vr.UnitThickness;
            lblVisualComponentPassThicknessComment.Text = vr.CommentsThickness;
            lblVisualComponentPassThickness.Text = gcat.GetResult(vr.Pass_T);

            //Package Connection Type
            LoadPackageConnectionType();

            //Package Type  
            lblVisualComponentPackageConnectionType.Text = vr.ComponentType;
            //ddlVisualComponentPackageConnectionType.SelectedValue = vr.ComponentType;

            //Lead / Pin Castellation
            lblVisualComponentCastellationResult.Text = gcat.GetResult(vr.Lead_Count) + " (" + vr.CommentLeadCount + ")";
            //txtVisualComponentCastellationComment.Text = vr.CommentLeadCount;


            //DateCodes
            lblComponentIdentityDateCodes.Text = gcat.GetDateCodes(vr);
            //txtComponentIdentityDateCodes.Text = GetDateCodesAndQTY() ?? "";

            //Imagery
            //LoadImage(imgVisualComponent1, vr.VCA_Image1);
            //LoadImage(imgVisualComponent2, vr.VCA_Image2);
            //LoadImage(imgVisualComponent3, vr.VCA_Image3);
            LoadSectionImagery("VisualComponent", false);
        }




    }


    protected void LoadPackageConnectionType()
    {
        //ddlVisualComponentPackageConnectionType.SelectedValue = vr.ComponentType;


        //ddlVisualComponentCastellationResult.SelectedValue = vr.Lead_Count ?? "0";
        //txtVisualComponentCastellationComment.Text = vr.CommentLeadCount;


        lblVisualComponentOxidationResult.Text = gcat.GetResult(vr.Oxidation_Discoloration);
        lblVisualComponentOxidationComment.Text = vr.CommentO2Discoloration;

        lblVisualComponentLeadPadConditionResult.Text = gcat.GetResult(vr.Lead_PadCondition);
        lblVisualComponentLeadPadConditionComment.Text = vr.CommentLead_Pad;

        lblVisualComponentExposedBaseMetalResult.Text = gcat.GetResult(vr.SignsExposedBaseMetal);
        lblVisualComponentExposedBaseMetalComment.Text = vr.CommentExpBaseMetal;

        lblVisualComponentCoplanarityResult.Text = gcat.GetResult(vr.LeadCoplanarity);
        lblVisualComponentCoplanarityComment.Text = vr.CommentLeadCoplanarity;

        lblVisualComponentSolderResult.Text = gcat.GetResult(vr.SolderBalls_Spheres);
        lblVisualComponentSolderComment.Text = vr.CommentSolderBalls;

        lblVisualComponentConcentricityResult.Text = gcat.GetResult(vr.Uniform_Concentricity);
        lblVisualComponentConcentricityComment.Text = vr.CommentUniformity;

        lblVisualComponentDamagedSpheresResult.Text = gcat.GetResult(vr.SolderBalls_Spheres);
        lblVisualComponentDamagedSpheresComment.Text = vr.CommentDamagedSpheres;

    }

    protected void LoadVisualSurface()
    {
        if (vr != null)
        {
            lblVisualSurfaceSampleQTY.Text = vr.VisualSurfaceSampleQty.ToString();


            lblVisualSurfaceLogo.Text = gcat.GetResult(vr.LogosPartnumbersPass);
            lblVisualSurfaceLogoComment.Text = vr.LogosPartnumbersComment;

            lblVisualSurfaceTop.Text = gcat.GetResult(vr.Top_Pass);
            lblVisualSurfaceTopComment.Text = vr.Comments_Top;

            lblVisualSurfaceBot.Text = gcat.GetResult(vr.Bottom_Pass);
            lblVisualSurfaceBotComment.Text = vr.Comments_Bottom;

            //Imagery
            //LoadImage(imgVisualSurface1, vr.VSA_Image1);
            //LoadImage(imgVisualSurface2, vr.VSA_Image2);
            //LoadImage(imgVisualSurface3, vr.VSA_Image3);
            LoadSectionImagery("VisualSurface", false);
        }
    }

    protected void LoadMarkingPerm()
    {
        //mperm = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (mperm != null)
        {
            tabMarkingPerm.Visible = true;
            aMarkingPerm.Visible = true;

            lblMarkingPermSampleQTY.Text = mperm.MarkingSample_Qty.ToString();

            lblMarkingPermAcetone.Text = gcat.GetResult(mperm.AcetoneTestPass);
            lblMarkingPermAcetoneComment.Text = mperm.Comments_Acetone;

            lblMarkingPermAlcohol.Text = gcat.GetResult(mperm.Alcohol_MineralSpiritsTestPass);
            lblMarkingPermAlcoholComment.Text = mperm.Comments_Alcohol;

            lblMarkingPermScrape.Text = gcat.GetResult(mperm.ScrapeTestPass);
            lblMarkingPermScrapeComment.Text = mperm.Comments_ScrapeTest;

            lblMarkingPermSecondary.Text = gcat.GetResult(mperm.SecondaryCoatingTestPass);
            lblMarkingPermSecondaryComment.Text = mperm.Comments_SecondaryCoating;

            lblMarkingPermPartMarking.Text = gcat.GetResult(mperm.PartNumber_MarkingTestPass);
            lblMarkingPermPartMarkingComment.Text = mperm.Comments_PartNumber;

            lblMarkingPermCase.Text = gcat.GetResult(mperm.CaseMarking_LogoTestPass);
            lblMarkingPermCaseComment.Text = mperm.Comments_CaseMarking;

            //Imagery
            LoadSectionImagery("MarkingPerm", false);


        }
        else
        {
            tabMarkingPerm.Visible = false;
            aMarkingPerm.Visible = false;
        }

    }

    protected void LoadXray()
    {
        //xray = ciq.XRays.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (xray != null)
        {
            tabXray.Visible = true;
            aXray.Visible = true;

            lblXraySampleQTY.Text = xray.XRaySampleQty.ToString();

            lblXrayDiePresent.Text = gcat.GetResult(xray.DiePresentPass);
            lblXrayDiePresentComment.Text = xray.DiePresentComments;

            lblXrayBondWires.Text = gcat.GetResult(xray.BondWiresPresentPass);
            lblXrayBondWiresComment.Text = xray.BondWiresPresentComment;

            lblXrayDamagedBond.Text = gcat.GetResult(xray.Damaged_MissingWiresPass);
            lblXrayDamagedBondComment.Text = xray.Damaged_MissingWiresComment;

            lblXrayDieLead.Text = gcat.GetResult(xray.DieLeadUniformityPass);
            lblXrayDieLeadComment.Text = xray.DieLeadUniformityComment;

            lblXrayDieFrame.Text = gcat.GetResult(xray.DieFRameSize_LocationPass);
            lblXrayDieFrameComment.Text = xray.DieFrameSize_LocationComment;

            //Imagery
            LoadSectionImagery("Xray", false);


        }
        else
        {
            tabXray.Visible = false;
            aXray.Visible = false;
        }

    }

    private void LoadBasicElectrical()
    {
        //be = ciq.BasicElectricals.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (be != null)
        {
            lblBasicElectricalSampleQTY.Text = be.SampleQty.ToString();

            lblBasicElectricalTest1.Text = gcat.GetResult(be.Test1);
            lblBasicElectricalCondition1.Text = be.Cond1;
            //Overall Result
            lblBasicElectricalResult.Text = be.Actual1;
            lblBasicElectricalNotes.Text = be.notes;

            //Imagery
            //LoadImage(imgBasicElectrical1, be.Image);
            LoadSectionImagery("BasicElectrical", false);
        }
        else
        {
            tabBasicElectrical.Visible = false;
            aBasicElectrical.Visible = false;
        }

    }

    protected void LoadSolderability()
    {
        //sol = ciq.Solderabilities.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (sol != null)
        {
            tabSolderability.Visible = true;
            aSolderability.Visible = true;

            lblSolderSampleQTY.Text = sol.SolderabilitySampleQty.ToString();

            lblSolderSPRequired.Text = gcat.GetResult(sol.SolderabilityServiceProviderRequired, true);
            lblSolderSPRequiredComment.Text = sol.SolderabilitySPRequiredComments;

            lblSolderPackageMountType.Text = sol.PackageMountTypeSolderability;

            lblSolderRohs.Text = gcat.GetResult(sol.RoHS, true);
            lblSolderRohsComment.Text = sol.RoHSComments;

            lblSolderWetting.Text = gcat.GetResult(sol.SolderWettingAcceptable);
            lblSolderWettingComment.Text = sol.SolderWettingComments;

            lblSolderVoid.Text = gcat.GetResult(sol.VoidFree);
            lblSolderVoidComment.Text = sol.VoidFreeComments;

            lblSolderCont.Text = gcat.GetResult(sol.ContaminantFree);
            lblSolderContComment.Text = sol.ContaminantFreeComments;

            //Imagery
            LoadSectionImagery("Solder", false);
        }
        else
        {
            tabSolderability.Visible = false;
            aSolderability.Visible = false;
        }

    }
    protected void LoadXRF()
    {
        //xrf = ciq.XRFs.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (xrf != null)
        {
            tabXRF.Visible = true;
            aXRF.Visible = true;

            lblXRFSampleQTY.Text = xrf.XRFSampleQty.ToString();

            lblXRFPackageType.Text = xrf.PackageMountTypeXRF;

            lblXRFSPRequired.Text = gcat.GetResult(xrf.XRFServiceProviderRequired, true);
            lblXRFSPRequiredComment.Text = xrf.XRFSPRequiredComments;

            lblXRFLeadMaterialsMatch.Text = gcat.GetResult(xrf.LeadMaterialsMatch);
            lblXRFLeadMaterialsMatchComment.Text = xrf.LeadMaterialsMatchComments;

            lblXRFForeignMaterials.Text = gcat.GetResult(xrf.ForeignMaterials);
            lblXRFForeignMaterialsComment.Text = xrf.ForeignMaterialsComments;


            //Imagery
            LoadSectionImagery("XRF", false);
        }
        else
        {
            aXRF.Visible = false;
            tabXRF.Visible = false;
        }

    }
    protected void LoadEprom()
    {
        // ep = ciq.EPROMs.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (ep != null)
        {
            tabEPROM.Visible = true;
            aEPROM.Visible = true;

            lblEpromSampleQTY.Text = ep.EPROMSampleQty.ToString();

            lblEpromPackageMount.Text = ep.PackageMountTypeEPROM;

            lblEpromSPRequired.Text = gcat.GetResult(ep.EPROMServiceProviderRequired, true);
            lblEpromSPRequiredComment.Text = ep.EPROMSPRequiredComments;

            lblEpromChecksum.Text = gcat.GetResult(ep.ChecksumAnalysis);
            lblEpromChecksumComment.Text = ep.ChecksumComment;

            lblEpromProgrammingRequired.Text = gcat.GetResult(ep.ProgrammingRequired, true);
            if (ep.ProgrammingRequired == 1)
            {
                lblEpromVerify.Text = gcat.GetResult(ep.VerifyProgramLoad);
                lblEpromVerifyComment.Text = ep.ProgramLoadComment;
                divEpromVerify.Visible = true;
            }
            else
                divEpromVerify.Visible = false;
        }
        else
        {
            aEPROM.Visible = false;
            tabEPROM.Visible = false;
        }

    }

    protected void LoadSentry()
    {
        //sv = ciq.SentryVIs.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (sv != null)
        {
            tabSentry.Visible = true;
            aSentry.Visible = true;
            lblSentrySampleQTY.Text = sv.SentryVISampleQty.ToString();

            lblSentrySPRequired.Text = gcat.GetResult(sv.ServiceProviderRequiredSentryVI, true);
            lblSentrySPRequiredComment.Text = sv.SentryVISPRequiredComments;

            lblSentryPackageMountType.Text = sv.PackageMountTypeSentryVI;


            lblSentrySignaturesMatch.Text = gcat.GetResult(sv.SignaturesMatch);
            lblSentrySignaturesMatchComment.Text = sv.SignatureMatchComments;

            lblSentryOpenShorts.Text = gcat.GetResult(sv.Open_Shorts_Detection);
            lblSentryOpenShortsComment.Text = sv.OpenShortsComments;

            lblSentryPassedAllSignatureAnalysis.Text = gcat.GetResult(sv.SignatureAnalysisPass);
            lblSentryPassedAllSignatureAnalysisComment.Text = sv.SignatureAnalysisComments;

            //lblSentrySignatureReportRequired.Text = gcat.GetResult(sv.SignatureReportRequired, true);
            //lblSentrySignatureReportRequiredComment.Text = sv.SignatureReportComments;

            lblSentryOverallResult.Text = gcat.GetResult(sv.SentryOverAllResult);

            //Imagery
            LoadSectionImagery("Sentry", false);
        }
        else
        {
            tabSentry.Visible = false;
            aSentry.Visible = false;
        }

    }
    protected void LoadExtendedTesting()
    {
        et = ciq.ExtendedTestings.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (et != null)
        {
            tabExtendedTesting.Visible = true;
            aExtendedTesting.Visible = true;

            lblExtendedTestingSampleQTY.Text = et.SampleQty.ToString();

            //Category
            lblExtCategory.Text = et.ExtCategory;
            //Additional Testing
            //lblExtCategoryOther.Text = "None";
            //if (et.ExtCategory.ToLower() == "other")            
            //    lblExtCategoryOther.Text = et.ExtCategoryOther;                



            //Result
            lblExtTestingResult.Text = gcat.GetResult(et.Actual1);

            //lblExtTest1.Text = et.Test1;
            lblExtCondition1.Text = et.Cond1;
            //lblExtResult1.Text = et.Res1;
            lblExtActual1.Text = et.Actual1;
            lblExtNotes.Text = et.notes;

            //Imagery
            LoadSectionImagery("Ext", false);
        }

        else
        {
            aExtendedTesting.Visible = false;
            tabExtendedTesting.Visible = false;
        }

    }


    protected void LoadMoistureBaking()
    {
        //mb = ciq.MoistureBakings.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (mb != null)
        {
            txtMoistureBakingSampleQTY.Text = mb.total_quantity.ToString();
            //txtMoistureBakingStrategicSalesWorkOrderNumber.Text = mb.strategic_sales_work_order_number;
            txtMoistureBakingProcess.Text = mb.process;
            txtMoistureBakingDuration.Text = mb.bake_duration;
            txtMoistureBakingTemp.Text = mb.bake_temperature;
            txtMoistureBakingMSL.Text = mb.msl;
            ddlMoistureBakingResult.SelectedValue = mb.result;
            txtMoistureBakingComment.Text = mb.result_comment;

            tabMoistureBaking.Visible = true;
            aMoistureBaking.Visible = true;

            //Imagery
            LoadSectionImagery("MoistureBaking", false);
        }

        else
        {
            tabMoistureBaking.Visible = false;
            aMoistureBaking.Visible = false;
        }

    }

    private void LoadSectionImagery(string section, bool showDescription = true)
    {
        //Get a list of the Images for this section.
        int imageID = GCATID;
        if ((vr.demo_source_id ?? 0) > 0)
            imageID = (int)vr.demo_source_id;
        List<insp_images> images = ql.LoadInspectionImagery("GCAT", imageID, section);
        //Load the asp.Panel that contains the imagery section
        Panel p = GetSectionImageryPanel(section);
        if (images.Count == 0)
        {
            p.Visible = false;
            return;
        }

        //Identify the control names
        string controlIDPrefix = ql.GetInspectionImageryControlPrefix("GCAT", section);

        if (string.IsNullOrEmpty(controlIDPrefix))
            return;

        foreach (insp_images i in images)
        {
            string controlID = controlIDPrefix + i.insp_section_id;

            InspectionImageManagerControl c = (InspectionImageManagerControl)Page.Master.FindControl("MainContent").FindControl(controlID);
            //InspectionImageManagerControl c = ImageManagerControlList.Where(w => w.ID == controlID).FirstOrDefault();

            if (c != null)
            {
                c.showDescription = showDescription;
                c.init(i);
                p.Visible = true;
            }
        }


    }

    private Panel GetSectionImageryPanel(string section)
    {
        Panel ret = null;
        switch (section.ToLower())
        {
            case "header":
                {
                    ret = pnlHeaderImagery;
                    break;
                }
            case "visualcomponent":
                {
                    ret = pnlVisualCompImagery;
                    break;
                }
            case "visualsurface":
                {
                    ret = pnlVisualSurfaceImagery;
                    break;
                }
            case "markingperm":
                {
                    ret = pnlMarkingPermImagery;
                    break;
                }
            case "xray":
                {
                    ret = pnlXrayImagery;
                    break;
                }
            case "basicelectrical":
                {
                    ret = pnlBasicElectricalImagery;
                    break;
                }
            case "solder":
                {
                    ret = pnlSolderabilityImagery;
                    break;
                }
            case "sentry":
                {
                    ret = pnlSentryImagery;
                    break;
                }
            case "ext":
                {
                    ret = pnlExtImagery;
                    break;
                }
            case "moisturebaking":
                {
                    ret = pnlMoistureBakingImagery;
                    break;
                }
            case "heated":
                {
                    ret = pnlHeatedImagery;
                    break;
                }
            case "decap":
                {
                    ret = pnlDecapImagery;
                    break;
                }
            case "xrf":
                {
                    ret = pnlXrfImagery;
                    break;
                }
        }

        return ret;
    }

    private void SetImageControlProperties()
    {

        foreach (InspectionImageManagerControl c in ImageManagerControlList)
        {
            c.InspectionType = "GCAT";
            c.InspectionID = GCATID;
            c.EnableControls = false;
            c.SectionID = ql.GetSectionIDfromClientID(c.ClientID);

        }
    }

    private void LoadImageManagerControlList()
    {
        ImageManagerControlList = new List<InspectionImageManagerControl>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, ImageManagerControlList);
    }

    protected void LoadHeatedSolvents()
    {
        //hs = ciq.HeatedSolvents.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (hs != null)
        {
            tabHeatedSolvents.Visible = true;
            aHeatedSolvents.Visible = true;

            lblHeatedSolventsSampleQTY.Text = hs.HeatedSolventSampleQty.ToString();

            lblHeatedMeth.Text = gcat.GetResult(hs.Methyl_PyrrolidinonePass);
            lblHeatedMethComment.Text = hs.Methyl_PyrrolidinoneComments;

            lblHeatedDyn.Text = gcat.GetResult(hs.DynasolvePass);
            lblHeatedDynComment.Text = hs.DynasolveComments;

            lblHeatedSecondary.Text = gcat.GetResult(hs.SecondaryCoatingPass);
            lblHeatedSecondaryComment.Text = hs.SecondaryCoatingComments;

            //Imagery            
            LoadSectionImagery("Heated", false);
        }
        else
        {
            tabHeatedSolvents.Visible = false;
            aHeatedSolvents.Visible = false;
        }

    }

    protected void LoadDecap()
    {
        //decap = ciq.Decapsulations.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (decap != null)
        {
            tabDecap.Visible = true;
            aDecap.Visible = true;

            lblDecapSampleQTY.Text = decap.DeCap_SampleQty.ToString();

            lblDecapMfgLogo.Text = gcat.GetResult(decap.MFGLogoPass);
            lblDecapMfgLogoComment.Text = decap.MFGLogoComments;

            lblDecapCasing.Text = gcat.GetResult(decap.LogoMatchPass);
            lblDecapCasingComment.Text = decap.LogoMatchComments;

            lblDcapPartDie.Text = gcat.GetResult(decap.DiePNMatchPass);
            lblDcapPartDieComment.Text = decap.DiePNMatchComments;

            lblDecapDieBond.Text = gcat.GetResult(decap.DieBondWiresPass);
            lblDecapDieBondComment.Text = decap.DieBondWiresComments;

            //Imagery
            LoadSectionImagery("Decap", false);
        }
        else
        {
            aDecap.Visible = false;
            tabDecap.Visible = false;
        }

    }

    protected void LoadSummary()
    {
        //sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (sum == null)
        {
            ImportSummaryAnswers();
            SaveSummary();
        }
        //External Visual
        //lblSumExternalVisualLogosParts.Text = sum.ExtVisLogosPartsResult;       
        lblSumExternalVisualLogosParts.Text = sum.ExtVisLogosPartsResult;
        lblSumExternalVisualLogosPartsComment.Text = sum.ExtVisLogosPartsComment;
        lblSumExternalVisualDim.Text = sum.ExtVisDimensionalResult;
        lblSumExternalVisualDimComment.Text = sum.ExtVisDimensionalComment;
        lblSumExternalVisualLeadPkg.Text = sum.ExtVisLeadPkgResult;
        lblSumExternalVisualLeadPkgComment.Text = sum.ExtVisLeadPkgComment;
        lblSumExternalVisualAlteration.Text = sum.ExtVisSurfaceAltResult;
        lblSumExternalVisualAlterationComment.Text = sum.ExtVisSurfaceAltComment;
        //MarkingPermenancy
        if (mperm != null)
        {
            lblSumMarkingAcetone.Text = sum.MarkingAcetoneResult;
            lblSumMarkingAcetoneComment.Text = sum.MarkingAcetoneComment;
            lblSumMarkingScrape.Text = sum.MarkingScrapeResult;
            lblSumMarkingScrapeComment.Text = sum.MarkingScrapeComment;
            lblSumMarkingAlteration.Text = sum.MarkingAlterationResult;
            lblSumMarkingAlterationComment.Text = sum.MarkingAlterationComment;
        }
        else
            divSumMarking.Visible = false;


        //Xray
        if (xray != null)
        {
            lblSumXrayUniform.Text = sum.XrayDieWireResult;
            lblSumXrayUniformComment.Text = sum.XrayDieWireComment;
        }
        else
            divSumXray.Visible = false;

        //EPROM
        if (ep != null)
        {
            lblSumEPROMChecksum.Text = sum.EPROMChecksumResult;
            lblSumEPROMChecksumComment.Text = sum.EPROMCheckSumComment;
            lblSumEPROMValidation.Text = sum.EPROMProgramValidationResult;
            lblSumEPROMValidationComment.Text = sum.EPROMProgramValidationComment;
        }
        else
            divSumEPROM.Visible = false;

        //XRF
        if (xrf != null)
        {
            lblSumXRFLeadsMatch.Text = sum.XRFLeadMatsResult;
            lblSumXRFLeadsMatchComment.Text = sum.XRFLeadMatsComment;
            lblSumXRFForeignMat.Text = sum.XRFForeignResult;
            lblSumXRFForeignMatComment.Text = sum.XRFForeignComment;
        }
        else
            divSumXRF.Visible = false;

        //Solderability
        if (sol != null)
        {
            lblSumSolderPassSolderTesting.Text = sum.SolPassedResult;
            lblSumSolderPassSolderTestingComment.Text = sum.SolPassedComment;
            lblSumSolderWetting.Text = sum.SolWettingResult;
            lblSumSolderWettingComment.Text = sum.SolWettingComment;
        }
        else
            divSumSolderability.Visible = false;


        //Basic Electrical
        if (be != null)
        {
            lblSumBasicElectrical.Text = sum.BasicElectricalResult;
            lblSumBasicElectricalComment.Text = sum.BasicElectricalComment;
        }
        else
            divSumBasicElec.Visible = false;

        //Sentry VI
        if (sv != null)
        {
            lblSumElectricalVISignature.Text = sum.SentryVIResult;
            lblSumElectricalVISignatureComment.Text = sum.SentryVIComment;
        }
        else
            divSumSentry.Visible = false;


        //Extended Testing
        if (et != null)
        {
            lblSumExtendedTesting.Text = sum.ExtendedTestingResult;
            lblSumExtendedTestingComment.Text = sum.ExtendedTestingComment;
        }
        else
            divSumExtendedTesting.Visible = false;

        //Moisture / Baking
        if (mb != null)
        {
            lblSumMoistureBaking.Text = sum.MoistureBakingResult;
            lblSumMoistureBakingComment.Text = sum.MoistureBakingComment;

        }
        else
            divSumDecap.Visible = false;


        //Heated Solvents
        if (hs != null)
        {
            lblSumHeatedSolventsMeth.Text = sum.HeatedMethResult;
            lblSumHeatedSolventsMethComment.Text = sum.HeatedMethComment;
            lblSumHeatedSolventsDyna.Text = sum.HeatedDynaResult;
            lblSumHeatedSolventsDynaComment.Text = sum.HeatedDynaComment;
            lblSumHeatedSolventsSecondary.Text = sum.HeatedSecondaryResult;
            lblSumHeatedSolventsSecondaryComment.Text = sum.HeatedSecondaryComment;
        }
        else
            divSumHeatedSolvents.Visible = false;

        //Decap
        if (decap != null)
        {
            lblSumDecapWireBondIntegrity.Text = sum.DecapWireBondResult;
            lblSumDecapWireBondIntegrityComment.Text = sum.DecapWireBondComment;
            lblSumDecapDieMarking.Text = sum.DecapDieMarkingResult;
            lblSumDecapDieMarkingComment.Text = sum.DecapDieMarkingComment;
        }
        else
            divSumDecap.Visible = false;



    }

    private void ImportSummaryAnswers()
    {
        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (vr != null)
        {

            lblSumExternalVisualLogosParts.Text = GetSummaryResultInt16(lblSumExternalVisualLogosParts);
            lblSumExternalVisualDim.Text = GetSummaryResultInt16(lblSumExternalVisualDim);
            lblSumExternalVisualLeadPkg.Text = GetSummaryResultInt16(lblSumExternalVisualLeadPkg);
            lblSumExternalVisualAlteration.Text = GetSummaryResultInt16(lblSumExternalVisualAlteration);

            //Marking Perm
            if (mperm != null)
            {
                lblSumMarkingAcetone.Text = GetSummaryResultInt16(lblSumMarkingAcetone);
                lblSumMarkingScrape.Text = GetSummaryResultInt16(lblSumMarkingScrape);
                lblSumMarkingAlteration.Text = GetSummaryResultInt16(lblSumMarkingAlteration);
            }

            //Xray
            if (xray != null)
            {
                lblSumXrayUniform.Text = GetSummaryResultInt16(lblSumXrayUniform);
            }

            //Heated Solvents
            if (hs != null)
            {
                lblSumHeatedSolventsMeth.Text = GetSummaryResultInt16(lblSumHeatedSolventsMeth);
                lblSumHeatedSolventsDyna.Text = GetSummaryResultInt16(lblSumHeatedSolventsDyna);
                lblSumHeatedSolventsSecondary.Text = GetSummaryResultInt16(lblSumHeatedSolventsSecondary);
            }

            //Decap
            if (decap != null)
            {
                lblSumDecapWireBondIntegrity.Text = GetSummaryResultInt16(lblSumDecapWireBondIntegrity);
                lblSumDecapDieMarking.Text = GetSummaryResultInt16(lblSumDecapDieMarking);
            }

            //XRF
            if (xrf != null)
            {
                lblSumXRFLeadsMatch.Text = GetSummaryResultInt16(lblSumXRFLeadsMatch);
                lblSumXRFForeignMat.Text = GetSummaryResultInt16(lblSumXRFForeignMat);
            }
            //Solderability
            if (sol != null)
            {
                lblSumSolderPassSolderTesting.Text = GetSummaryResultInt16(lblSumSolderPassSolderTesting);
                lblSumSolderWetting.Text = GetSummaryResultInt16(lblSumSolderWetting);

            }

            //Basic Electrical
            if (be != null)
            {
                lblSumBasicElectrical.Text = GetSummaryResultInt16(lblSumBasicElectrical);
            }

            //Sentry VI
            if (sv != null)
            {
                lblSumElectricalVISignature.Text = GetSummaryResultInt16(lblSumElectricalVISignature);
            }

            //Eprom
            if (ep != null)
            {
                lblSumEPROMChecksum.Text = GetSummaryResultInt16(lblSumEPROMChecksum);
                if (ep.ProgrammingRequired == 1)
                    lblSumEPROMValidation.Text = GetSummaryResultInt16(lblSumEPROMValidation);
                else
                {
                    lblSumEPROMValidation.Visible = false;
                    lblSumEPROMValidationComment.Visible = false;
                }
            }
            //Extended Testing
            if (et != null)
            {
                lblSumExtendedTesting.Text = GetSummaryResultInt16(lblSumExtendedTesting);
            }

            if (mb != null)
            {
                lblSumMoistureBaking.Text = mb.result;
                lblSumMoistureBakingComment.Text = mb.result_comment;
            }
        }
    }

    private string GetSummaryResultInt16(Control l)
    {

        switch (l.ID)
        {//External Visual
            case "lblSumExternalVisualLogosParts":
                {
                    lblSumExternalVisualLogosPartsComment.Text = vr.LogosPartnumbersComment;
                    switch (vr.LogosPartnumbersPass)
                    {
                        case 1:
                            return "Pass";
                        case 2:
                            return "Fail";
                        default:
                            return "N/A";
                    }
                }

            case "lblSumExternalVisualDim":
                {
                    if (vr.Pass_L == true && vr.Pass_W == true && vr.Pass_T == true)
                    {
                        lblSumExternalVisualDimComment.Text = "Dimensional measurements match MFG spec";
                        return "Pass";
                    }
                    else
                    {
                        lblSumExternalVisualDimComment.Text = "Dimensional measurements do not match MFG spec";
                        return "Fail";
                    }
                }
            case "lblSumExternalVisualLeadPkg":
                {
                    switch (vr.ComponentType)
                    {

                        case "BGA":
                            {
                                if (vr.Oxidation_Discoloration == "Y" && vr.SolderBalls_Spheres == "Y" && vr.Uniform_Concentricity == "Y" && vr.Crushed_Damages_Spheres == "Y")
                                {
                                    lblSumExternalVisualLeadPkgComment.Text = "Pads and package condition acceptable";
                                    return "Pass";
                                }
                                else
                                {
                                    lblSumExternalVisualLeadPkgComment.Text = "Pads and package condition are unacceptable";
                                    return "Fail";
                                }
                            }
                        case "Lead":
                            {
                                if (vr.Lead_PadCondition == "Y" && vr.LeadCoplanarity == "Y" && vr.Oxidation_Discoloration == "Y" && vr.SignsExposedBaseMetal == "Y")
                                {
                                    lblSumExternalVisualLeadPkgComment.Text = "Pads and package condition acceptable";
                                    return "Pass";
                                }
                                else
                                {
                                    lblSumExternalVisualLeadPkgComment.Text = "Pads and package condition are unacceptable";
                                    return "Fail";
                                }
                            }
                        case "Leadless":
                            {
                                if (vr.Lead_PadCondition == "Y" && vr.Oxidation_Discoloration == "Y" && vr.SignsExposedBaseMetal == "Y")
                                {
                                    lblSumExternalVisualLeadPkgComment.Text = "Pads and package condition acceptable";
                                    return "Pass";
                                }
                                else
                                {
                                    lblSumExternalVisualLeadPkgComment.Text = "Pads and package condition are unacceptable";
                                    return "Fail";
                                }
                            }
                        default:
                            {
                                return null;
                            }
                    }
                }
            //What to do for "Signs of alteration?" - IF Top or Bottom Fail, then fail, else pass.
            case "lblSumExternalVisualAlteration":
                {
                    if (vr.Top_Pass == 1 && vr.Bottom_Pass == 1)
                    {
                        lblSumExternalVisualAlterationComment.Text = "No signs of surface alteration observed";
                        return "Pass";
                    }
                    else if (vr.Top_Pass == 2 || vr.Bottom_Pass == 2)
                    {
                        lblSumExternalVisualAlterationComment.Text = "Signs of surface alteration observed";
                        return "Fail";
                    }
                    else
                    {
                        lblSumExternalVisualAlterationComment.Text = "Results inconclusinve";
                        return "Inconclusive";

                    }
                }

            //Marking Permenancy
            case "lblSumMarkingAcetone":
                switch (mperm.AcetoneTestPass)
                {
                    case 1:
                        {
                            lblSumMarkingAcetoneComment.Text = "Passed acetone test";
                            return "Pass";
                        }

                    case 2:
                        {
                            lblSumMarkingAcetoneComment.Text = "Failed acetone test";
                            return "Fail";
                        }
                    default:
                        {
                            lblSumMarkingAcetoneComment.Text = "Acetone test inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumMarkingScrape":
                switch (mperm.ScrapeTestPass)
                {
                    case 1:
                        {
                            lblSumMarkingScrapeComment.Text = "Passed scrape test";
                            return "Pass";
                        }

                    case 2:
                        {
                            lblSumMarkingScrapeComment.Text = "Failed scrape test";
                            return "Fail";
                        }
                    default:
                        {
                            lblSumMarkingScrapeComment.Text = "Scrape test inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumMarkingAlteration":
                {
                    if (mperm.ScrapeTestPass == 1 && mperm.AcetoneTestPass == 1)
                    {
                        lblSumMarkingAlterationComment.Text = "No signs of marking alteration observed";
                        return "Pass";
                    }
                    else if (mperm.ScrapeTestPass == 2 || mperm.AcetoneTestPass == 2)
                    {
                        lblSumMarkingAlterationComment.Text = "Signs of marking alteration observed";
                        return "Fail";
                    }
                    else
                    {
                        lblSumMarkingAlterationComment.Text = "Results inconclusinve";
                        return "Inconclusive";
                    }
                }
            //X-Ray Analysis
            case "lblSumXrayUniform":
                {
                    if (xray.DiePresentPass == 1 && xray.BondWiresPresentPass == 1)
                    {
                        lblSumXrayUniformComment.Text = "Die and bond wires are uniform";
                        return "Pass";
                    }
                    else if (xray.DiePresentPass == 2 && xray.BondWiresPresentPass == 2)
                    {
                        lblSumXrayUniformComment.Text = "Die and bond wires are not uniform";
                        return "Fail";
                    }
                    else
                    {
                        lblSumXrayUniformComment.Text = "Inconclusive";
                        return "Inconclusive";
                    }
                }

            //Heated Solvents Testing
            case "lblSumHeatedSolventsMeth":
                switch (hs.Methyl_PyrrolidinonePass)
                {
                    case 1:
                        {
                            lblSumHeatedSolventsMethComment.Text = "No Signs of secondary coating";
                            return "Pass";
                        }

                    case 2:
                        {
                            lblSumHeatedSolventsMethComment.Text = "Signs of secondary coating observed";
                            return "Fail";
                        }
                    default:
                        {
                            lblSumHeatedSolventsMethComment.Text = "Secondary coating results inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumHeatedSolventsDyna":
                switch (hs.DynasolvePass)
                {
                    case 1:
                        {
                            lblSumHeatedSolventsDynaComment.Text = "No Signs of secondary coating";
                            return "Pass";
                        }

                    case 2:
                        {
                            lblSumHeatedSolventsDynaComment.Text = "Signs of secondary coating observed";
                            return "Fail";
                        }
                    default:
                        {
                            lblSumHeatedSolventsDynaComment.Text = "Secondary coating results inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumHeatedSolventsSecondary":
                switch (hs.SecondaryCoatingPass)
                {
                    case 1:
                        {
                            lblSumHeatedSolventsSecondaryComment.Text = "No Signs of secondary coating";
                            return "Pass";
                        }

                    case 2:
                        {
                            lblSumHeatedSolventsSecondaryComment.Text = "Signs of secondary coating observed";
                            return "Fail";
                        }
                    default:
                        {
                            lblSumHeatedSolventsSecondaryComment.Text = "Secondary coating results inconclusive";
                            return "Inconclusive";
                        }

                }
            //De-Cap Analysis
            case "lblSumDecapWireBondIntegrity":
                {
                    lblSumDecapWireBondIntegrityComment.Text = decap.DieBondWiresComments;
                    switch (decap.DieBondWiresPass)
                    {
                        case 1:
                            {
                                lblSumDecapWireBondIntegrityComment.Text = "Die and bond wires correct in size, orientation and placement";
                                return "Pass";
                            }
                        case 2:
                            {
                                lblSumDecapWireBondIntegrityComment.Text = "Die and bond wires incorrect in size, orientation or placement";
                                return "Fail";
                            }
                        default:
                            {
                                lblSumDecapWireBondIntegrityComment.Text = "Die and bond wire results inconclusive";
                                return "N/A";
                            }
                    }
                }
            case "lblSumDecapDieMarking":
                {
                    if (decap.MFGLogoPass == 1 && decap.LogoMatchPass == 1 && decap.DiePNMatchPass == 1)
                    {
                        lblSumDecapDieMarkingComment.Text = "Die, part, and logo match on all samples";
                        return "Pass";
                    }
                    else if (decap.MFGLogoPass == 2 || decap.LogoMatchPass == 2 || decap.DiePNMatchPass == 2)
                    {
                        lblSumDecapDieMarkingComment.Text = "Die, part, or logo do not match on all samples";
                        return "Fail";
                    }
                    else
                    {
                        lblSumDecapDieMarkingComment.Text = "Inconclusive";
                        return "Inconclusive";
                    }
                }
         
            //XRF Analysis
            case "lblSumXRFLeadsMatch":
                {
                    lblSumXRFLeadsMatchComment.Text = xrf.LeadMaterialsMatchComments;
                    switch (xrf.LeadMaterialsMatch)
                    {
                        case 1:
                            {
                                lblSumXRFLeadsMatchComment.Text = "Lead materials match MFG specification";
                                return "Pass";
                            }

                        case 2:
                            {
                                lblSumXRFLeadsMatchComment.Text = "Lead materials do not match MFG specification";
                                return "Fail";
                            }

                        default:
                            {
                                lblSumXRFLeadsMatchComment.Text = "Lead materials match inconclusive";
                                return "Inconclusive";
                            }

                    }
                }
            case "lblSumXRFForeignMat":
                {
                    lblSumXRFForeignMatComment.Text = xrf.ForeignMaterialsComments;
                    switch (xrf.ForeignMaterials)
                    {
                        case 1:
                            {
                                lblSumXRFForeignMatComment.Text = "No foreign materials present";
                                return "Pass";
                            }
                        case 2:
                            {
                                lblSumXRFForeignMatComment.Text = "Foreign materials present";
                                return "Fail";
                            }
                        default:
                            {
                                lblSumXRFForeignMatComment.Text = "Inconclusive";
                                return "Inconclusive";
                            }
                    }
                }
            //Solderability Analysis
            case "lblSumSolderPassSolderTesting":
                {
                    //lblSumSolderPassSolderTestingComment.Text = sol.SolderWettingComments;
                    if (sol.RoHS == 1 && sol.VoidFree == 1 && sol.ContaminantFree == 1)
                    {
                        lblSumSolderPassSolderTestingComment.Text = "Solder is RoHS compliant, void-free, and contaminant-free";
                        return "Pass";
                    }
                    else
                    {
                        lblSumSolderPassSolderTestingComment.Text = "Failed solderability testing";
                        return "Fail";
                    }
                  
                }
            case "lblSumSolderWetting":
                {
                    //lblSumSolderWettingComment.Text = sol.SolderWettingComments;
                    switch (sol.SolderWettingAcceptable)
                    {
                        case 1:
                            {
                                lblSumSolderWettingComment.Text = "Passed solder wetting tests";
                                return "Pass";
                            }

                        case 2:
                            {
                                lblSumSolderWettingComment.Text = "Failed solder wetting tests";
                                return "Fail";
                            }

                        default:
                            {
                                lblSumSolderWettingComment.Text = "Solder wetting tests inconclusive";
                                return "Inconclusive";
                            }
                    }
                }
            //Electrical Analysis
            case "lblSumElectricalVISignature":
                {
                    lblSumElectricalVISignatureComment.Text = sv.SignatureAnalysisComments;
                    switch (sv.SignatureAnalysisPass)
                    {
                        case 1:
                            {
                                lblSumElectricalVISignatureComment.Text = "Passed Sentry VI electrical testing";
                                return "Pass";
                            }

                        case 2:
                            {
                                lblSumElectricalVISignatureComment.Text = "Failed Sentry VI electrical testing";
                                return "Fail";
                            }
                        default:
                            {
                                lblSumElectricalVISignatureComment.Text = "Sentry VI electrical testing results inconclusive";
                                return "Inconclusive";
                            }

                    }
                }
            case "lblSumBasicElectrical":
                {
                    lblSumBasicElectricalComment.Text = be.notes;
                    switch (be.Actual1)
                    {
                        case "Y":
                            {
                                lblSumBasicElectricalComment.Text = "Passed basic electrical testing";
                                return "Pass";
                            }
                        case "N":
                            {
                                lblSumBasicElectricalComment.Text = "Failed basic electrical testing";
                                return "Fail";
                            }
                        case "N/A":
                            {
                                lblSumBasicElectricalComment.Text = "Basic electrical testing results inconclusive";
                                return "N/A";
                            }
                        default:
                            {
                                lblSumBasicElectricalComment.Text = "-Choose-";
                                return "-Choose-";
                            }

                    }
                }

            //EPROM Testing
            case "lblSumEPROMChecksum":
                {
                    lblSumEPROMChecksumComment.Text = ep.ChecksumComment;
                    switch (ep.ChecksumAnalysis)
                    {
                        case 1:
                            {
                                lblSumEPROMChecksumComment.Text = "All samples passed checksum testing and were clear of data";
                                return "Pass";
                            }

                        case 2:
                            {
                                lblSumEPROMChecksumComment.Text = "Samples failed checksum / blank-check testing";
                                return "Fail";
                            }
                        default:
                            {
                                lblSumEPROMChecksumComment.Text = "N/A";
                                return "N/A";
                            }

                    }
                }
            case "lblSumEPROMValidation":
                {
                    if (ep.ProgrammingRequired == 1)
                    {
                        lblSumEPROMValidationComment.Text = ep.ProgramLoadComment;
                        switch (ep.VerifyProgramLoad)
                        {
                            case 1:
                                {
                                    lblSumEPROMValidationComment.Text = "Passed programming validation check";
                                    return "Pass";
                                }
                            case 2:
                                {
                                    lblSumEPROMValidationComment.Text = "Failed programming validation check";
                                    return "Fail";
                                }
                            default:
                                {
                                    lblSumEPROMValidationComment.Text = "N/A";
                                    return "N/A";
                                }
                        }
                    }
                    return null;
                }
            case "lblSumExtendedTesting":
                {
                    switch (et.ExtTestingResult)
                    {
                        case "0":
                            {
                                lblSumExtendedTestingComment.Text = "-Choose-";
                                return "-Choose-";
                            }
                        case "Y":
                            {
                                lblSumExtendedTestingComment.Text = "Passed extended testing";
                                return "Pass";
                            }
                        case "N":
                            {
                                lblSumExtendedTestingComment.Text = "Failed extended testing";
                                return "Fail";
                            }
                        default:
                            {
                                lblSumExtendedTestingComment.Text = "N/A";
                                return "N/A";
                            }
                    }
                }




            default:
                return null;
        }
    }

    protected List<Image> GetImageControls()
    {
        List<Image> allImages = new List<Image>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, allImages);
        if (allImages.Count > 0)
            return allImages;
        return null;
    }

    protected List<FileUpload> GetFileUploadControls()
    {
        List<FileUpload> allFileUploads = new List<FileUpload>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, allFileUploads);
        if (allFileUploads.Count > 0)
            return allFileUploads;
        return null;
    }


    protected List<InspectionImageManagerControl> GetFileImageManagerControls()
    {
        List<InspectionImageManagerControl> l = new List<InspectionImageManagerControl>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, l);
        if (l.Count > 0)
            return l;
        return null;
    }

    protected string LoadPhysicalImage(string filepath)
    {
        return "~/public_controls/GetPhysicalImage.aspx?FileName=" + filepath;
    }

    protected void LoadImage(Image img, string imageFilePath = null)
    {
        try
        {
            insp_images dbImage;
            string sectionID = img.ID.Substring(3);
            HtmlAnchor fb = (HtmlAnchor)Page.Master.FindControl("MainContent").FindControl("fancybox" + sectionID);

            if (vr.is_demo ?? false)//For Demos, we need to reuse the images from the database, as new tests, won't have image paths, so we'll lookup based on the source gcat id
                dbImage = smb.insp_images.Where(w => w.insp_id == vr.demo_source_id && w.insp_section_id == sectionID && w.insp_type == "GCAT").SingleOrDefault();//Does the image exist in MyQL?
            else
                dbImage = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID && w.insp_type == "GCAT").SingleOrDefault();//Does the image exist in MyQL?

            if (dbImage == null && !vr.is_imported)//If not found in new database and not already imported from old,from the old
                if (!string.IsNullOrEmpty(imageFilePath))
                {
                    //Check if the physical folder exists
                    if (!File.Exists(imageFilePath))
                        throw new Exception("Physical file not found.");

                    SaveImage(img, imageFilePath);
                    dbImage = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID).SingleOrDefault();
                    if (dbImage != null)
                        tools.HandleResultJS("Image(s) successfully converted", true, Page);
                }
            if (dbImage != null)
            {

                img.ImageUrl = "~/public/web_handlers/ImageHandler.ashx?id=" + dbImage.insp_image_id + "&type=gcat";
                img.Visible = true;
                fb.HRef = img.ImageUrl;
                fb.Visible = true;

            }
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

    }

    private void SaveImage(Image img, string sectionID, FileUpload fu)
    {
        insp_images i;
        //string sectionID = img.ID.Substring(3);
        if (fu == null)
            fu = (FileUpload)this.Page.Master.FindControl("MainContent").FindControl("fuImg" + sectionID);
        if (fu.HasFile)
        {
            i = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID).FirstOrDefault();
            if (i == null)
            {
                i = new insp_images();
                smb.insp_images.Add(i);
            }
            i.img_name = img.ID;
            i.img_type = "jpg";
            i.img_blob = tools.ResizeImage(fu.FileBytes, 1024);
            //i.img_description = "not set";
            i.insp_id = GCATID;
            i.insp_section_id = sectionID;
            i.insp_image_id = Guid.NewGuid().ToString();
            i.img_path = fu.FileName;
            smb.SaveChanges();
        }
    }

    private void SaveImage(Image img, string imageFilePath, insp_images i = null)
    {

        if (i == null)
        {
            i = new insp_images();
            smb.insp_images.Add(i);
        }
        string sectionID = img.ID.Substring(3);
        byte[] imgBytes;
        i.img_name = img.ID;
        i.img_type = "jpg";
        imageFilePath = imageFilePath.Replace("P:", @"\\SM\Shared_Docs\Part Photos");
        imgBytes = File.ReadAllBytes(imageFilePath);
        imgBytes = tools.ResizeImage(imgBytes, 1024);
        i.img_blob = imgBytes;
        //i.img_description = "not set";
        i.insp_type = "GCAT";
        i.insp_id = GCATID;
        i.insp_section_id = sectionID;
        i.insp_image_id = Guid.NewGuid().ToString();
        i.img_path = imageFilePath;
        smb.SaveChanges();
    }

    private void SaveSummary()
    {
        sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (sum == null)
        {
            sum = new Summary_new();
            sum.ValidationReportID = GCATID;
            sum.date_created = DateTime.Now;
            ciq.Summary_news.InsertOnSubmit(sum);
        }
        //External Visual
        sum.ExtVisLogosPartsResult = lblSumExternalVisualLogosParts.Text;
        sum.ExtVisLogosPartsComment = lblSumExternalVisualLogosPartsComment.Text;
        sum.ExtVisDimensionalResult = lblSumExternalVisualDim.Text;
        sum.ExtVisDimensionalComment = lblSumExternalVisualDimComment.Text;
        sum.ExtVisLeadPkgResult = lblSumExternalVisualLeadPkg.Text;
        sum.ExtVisLeadPkgComment = lblSumExternalVisualLeadPkgComment.Text;
        sum.ExtVisSurfaceAltResult = lblSumExternalVisualAlteration.Text;
        sum.ExtVisSurfaceAltComment = lblSumExternalVisualAlterationComment.Text;
        //MarkingPermenancy        
        sum.MarkingAcetoneResult = lblSumMarkingAcetone.Text;
        sum.MarkingAcetoneComment = lblSumMarkingAcetoneComment.Text;
        sum.MarkingScrapeResult = lblSumMarkingScrape.Text;
        sum.MarkingScrapeComment = lblSumMarkingScrapeComment.Text;
        sum.MarkingAlterationResult = lblSumMarkingAlteration.Text;
        sum.MarkingAlterationComment = lblSumMarkingAlterationComment.Text;
        //Xray
        sum.XrayDieWireResult = lblSumXrayUniform.Text;
        sum.XrayDieWireComment = lblSumXrayUniformComment.Text;
        //Heated Solvents
        sum.HeatedMethResult = lblSumHeatedSolventsMeth.Text;
        sum.HeatedMethComment = lblHeatedMethComment.Text;
        sum.HeatedDynaResult = lblSumHeatedSolventsDyna.Text;
        sum.HeatedDynaComment = lblSumHeatedSolventsDynaComment.Text;
        sum.HeatedSecondaryResult = lblSumHeatedSolventsSecondary.Text;
        sum.HeatedSecondaryComment = lblSumHeatedSolventsSecondaryComment.Text;
        //Decap
        sum.DecapWireBondResult = lblSumDecapWireBondIntegrity.Text;
        sum.DecapWireBondComment = lblSumDecapWireBondIntegrityComment.Text;
        sum.DecapDieMarkingResult = lblSumDecapDieMarking.Text;
        sum.DecapDieMarkingComment = lblSumDecapDieMarkingComment.Text;
        //XRF
        sum.XRFLeadMatsResult = lblSumXRFLeadsMatch.Text;
        sum.XRFLeadMatsComment = lblSumXRFLeadsMatchComment.Text;
        sum.XRFForeignResult = lblSumXRFForeignMat.Text;
        sum.XRFForeignComment = lblSumXRFForeignMatComment.Text;
        //Solderability
        sum.SolPassedResult = lblSumSolderPassSolderTesting.Text;
        sum.SolPassedComment = lblSumSolderPassSolderTestingComment.Text;
        sum.SolWettingResult = lblSumSolderWetting.Text;
        sum.SolWettingComment = lblSumSolderWettingComment.Text;
        //Basic Electrical
        sum.BasicElectricalResult = lblSumBasicElectrical.Text;
        sum.BasicElectricalComment = lblSumBasicElectricalComment.Text;
        //Sentry VI
        sum.SentryVIResult = lblSumElectricalVISignature.Text;
        sum.SentryVIComment = lblSumElectricalVISignatureComment.Text;
        //Extended Testing
        sum.ExtendedTestingResult = lblSumExtendedTesting.Text;
        sum.ExtendedTestingComment = lblSumExtendedTestingComment.Text;
        //Moisture Baking
        sum.MoistureBakingResult = lblSumMoistureBaking.Text;
        sum.MoistureBakingComment = lblSumMoistureBakingComment.Text;

        //EPROM
        sum.EPROMChecksumResult = lblSumEPROMChecksum.Text;
        sum.EPROMCheckSumComment = lblSumEPROMChecksumComment.Text;
        sum.EPROMProgramValidationResult = lblSumEPROMValidation.Text;
        sum.EPROMProgramValidationComment = lblEpromVerifyComment.Text;


        //may need to make some new columns to hold this data, since the andwers are programatically derived, and may not map to an existing summary field.          
    }

   

    private void GetManufacturerPart(int mpnID = 0)
    {
        //if (mpnID == 0)
        //{
        //    mp = new ManufacturerPart();
        //    mp.date_created = DateTime.Now;
        //    mp.MPN = txtMPNSearch.Text;
        //    mp.Manufacturer = txtHeaderMFG.Text.Trim().ToUpper();
        //    ciq.ManufacturerParts.InsertOnSubmit(mp);
        //    ciq.SubmitChanges();
        //}
        //else
        mp = ciq.ManufacturerParts.Where(w => w.MPNID == vr.MPNID).FirstOrDefault();
        MPN = mp.MPN;
        MFG = mp.Manufacturer;
        MPNID = mp.MPNID;
        hfCastellation.Value = mp.Lead_PinCount;
    }

    protected void ShowMainGCATForm()
    {
        if (mp != null)
        {



            //Show the Main Panel
            pnlGCATMain.Visible = true;
            //Show the HeaderDetails Panel        
            pnlHeaderData.Visible = true;
            //Set Textbox to selected Part Number             
            lblHeaderMPN.Text = MPN;
            lblHeaderMFG.Text = MFG;
            //Update the Component Tab with these Details? // NO, only after save.
            //Bind the Customer DDL
            //BindCustomerDDL();
            //Show Save Button


        }
    }

    

    protected void lbGetDataSheet_Click(object sender, EventArgs e)
    {

        try
        {
            silicon_expert si = new silicon_expert();
            DataSet ds = new DataSet();
            ds = SiliconExpertLogic.GetListPartSearchDataSetXML(MPN);
            if (ds == null)
            {
                tools.HandleResultJS("No datasheets for for " + MPN, true, Page);
                return;
            }
            //dt = ds.Tables["PCNDto"];
            DataTable dt = ds.Tables["Result"];

            //gvDatasheets.DataSource = rdc.companies.Where(w => w.companyname == "KT Portal Test").Select(s => s.companyname);
            gvDatasheets.DataSource = dt;
            gvDatasheets.DataBind();

            upDatasheets.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "openModal('mdlDatasheets');", true);

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    //protected void lbDeleteAll_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
    //        if (vr != null)
    //            if (!vr.is_deleted)
    //            {
    //                vr.is_deleted = true;
    //                ShowStatusDiv("danger", "deleted");
    //            }
    //        DeleteAllImages();
    //        ciq.SubmitChanges();
    //        //Loading all with the Updatepanels was too wierd without redirecting.  This is simple, and does the job, loads are pretty quick too.      
    //        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
    //        ScriptManager.RegisterClientScriptBlock(Page, GetType(), "successKey", "$(document).ready(successAlert('success'));", true);
    //    }
    //    catch
    //    {
    //        ScriptManager.RegisterClientScriptBlock(Page, GetType(), "successKey", "$(document).ready(successAlert('fail'));", true);
    //    }
    //}
    //protected void lbRestore_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
    //        if (vr != null)
    //            if (vr.is_deleted)
    //            {
    //                vr.is_deleted = false;
    //                divGcatStatus.Visible = false;

    //            }

    //        ciq.SubmitChanges();
    //        //Loading all with the Updatepanels was too wierd without redirecting.  This is simple, and does the job, loads are pretty quick too.       
    //        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
    //        ScriptManager.RegisterClientScriptBlock(Page, GetType(), "successKey", "$(document).ready(successAlert('success'));", true);
    //    }
    //    catch
    //    {
    //        ScriptManager.RegisterClientScriptBlock(Page, GetType(), "successKey", "$(document).ready(successAlert('fail'));", true);
    //    }
    //}
    protected void ShowStatusDiv(string alertType, string statusMessage)
    {
        lblGcatStatus.Text = statusMessage;
        switch (alertType.ToLower())
        {
            case "danger":
                {
                    divGcatStatus.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    break;
                }
        }

        switch (statusMessage.ToLower())
        {
            case "deleted":
                {
                    lblGcatStatus.Text = "Status: Deleted";
                    break;
                }
            default:
                {
                    lblGcatStatus.Text = "Status: " + statusMessage;
                    break;
                }
        }
        divGcatStatus.Visible = true;
    }

    protected bool hasPermission(string type)
    {
        bool ret = false;
        string userName = System.Web.Security.Membership.GetUser().UserName;
        switch (type.ToLower())
        {
            case "save":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_admin") || Roles.IsUserInRole(userName, "sm_insp_user"))
                        ret = true;
                    break;
                }
            case "delete":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_admin") || Roles.IsUserInRole(userName, "sm_insp_user"))
                        ret = true;
                    break;
                }
            case "restore":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_admin"))
                        ret = true;
                    break;
                }
            case "useform":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_user"))
                        ret = true;
                    break;
                }

        }
        return ret;
    }

    private void GetAssociatedTests()
    {
        //This can probably be the place where I consolidate all future queries for individual test types into one place.
        if (GCATID == 0)
            return;
        decap = ciq.Decapsulations.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        hs = ciq.HeatedSolvents.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        mperm = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        be = ciq.BasicElectricals.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        sv = ciq.SentryVIs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        sol = ciq.Solderabilities.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        xray = ciq.XRays.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        xrf = ciq.XRFs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        ep = ciq.EPROMs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        et = ciq.ExtendedTestings.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        mb = ciq.MoistureBakings.Where(w => w.validationreportid == GCATID).SingleOrDefault();
        sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();

    }
    protected void lbPDF_Click(object sender, EventArgs e)
    {
        //Calling this so that old reports get updated with the proper values.
        //SaveAll();
        gcat = new SM_GCAT(GCATID);
        gcat.CreatePDF();
    }


}