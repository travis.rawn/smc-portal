﻿<%@ Page Title="GCAT Report" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="gcat_report.aspx.cs" Inherits="secure_members_qc_center_gcat_report" %>

<%@ Register Src="~/assets/controls/InspectionImageManagerControl.ascx" TagPrefix="uc1" TagName="InspectionImageManagerControl" %>
<%@ Register Src="~/assets/controls/InspectionImageGallery.ascx" TagPrefix="uc1" TagName="InspectionImageGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


    <link href="/Content/css/sm_reporting.css" rel="stylesheet" />
    <link href="/Content/css/sm-gcat.css" rel="stylesheet" />
    <script src="/Content/scripts/sm_gcat.js" type="text/javascript"></script>



    <style>
        .nav-pills li{
            float:none;
        }

       .nav-item >.active {
   color: white;
}
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6 class="relative">GCAT Testing Report</h6>
    <input type="hidden" id="_ispostback" value="<%=Page.IsPostBack.ToString()%>" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <%--JS Variables--%>
    <asp:HiddenField ID="hfCastellation" runat="server" />


    <%-- Container--%>
    <div class="report-container">
        <%--Alert Div--%>
        <div class="alert alert-dismissible" role="alert" id="slideAlert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success!</strong>
            <asp:Label ID="lblSuccessAlert" runat="server"></asp:Label>
        </div>
        <%-- End Alert Div--%>

        <%--Status Panel--%>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div id="divGcatStatus" runat="server" visible="false">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <label>
                        <asp:Label ID="lblGcatStatus" runat="server"></asp:Label></label>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>

        <%--pnlGCATBody--%>
        <asp:Panel ID="pnlGCATMain" runat="server" Visible="false">
            <div class="row">
                <div class="col-sm-2">
                    <%-- <nav class="navbar sm-gcat-nav">--%>

                    <%-- <div class="container-fluid">--%>
                    <%-- <ul class="nav nav-pills nav-stacked" id="tabs">--%>
                    <ul class="nav nav-pills flex-column gcat-nav">
                        <li class="nav-item"><a class="nav-link active" href="#Header" data-toggle="pill">Header</a></li>
                        <li class="nav-item"><a class="nav-link" href="#ComponentIdentity" data-toggle="pill">Component</a></li>
                        <li class="nav-item"><a class="nav-link" href="#VisualComponent" data-toggle="pill">Visual Component</a></li>
                        <li class="nav-item"><a class="nav-link" href="#VisualSurface" data-toggle="pill">Visual Surface</a></li>
                        <li class="nav-item"><a class="nav-link" href="#Required" data-toggle="pill">Required Tests</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabMarkingPerm" data-toggle="pill" id="aMarkingPerm" runat="server">Marking</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabXray" data-toggle="pill" id="aXray" runat="server">X-Ray</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabBasicElectrical" data-toggle="pill" id="aBasicElectrical" runat="server">Basic Electrical</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabSentry" data-toggle="pill" id="aSentry" runat="server">Sentry VI</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabSolderability" data-toggle="pill" id="aSolderability" runat="server">Solderability</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabXRF" data-toggle="pill" id="aXRF" runat="server">XRF Analysis</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabEPROM" data-toggle="pill" id="aEPROM" runat="server">EPROM</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabExtendedTesting" data-toggle="pill" id="aExtendedTesting" runat="server">Extended Testing</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabMoistureBaking" data-toggle="pill" id="aMoistureBaking" runat="server">Moisture / Baking</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabHeatedSolvents" data-toggle="pill" id="aHeatedSolvents" runat="server">Heated Solvents</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabDecap" data-toggle="pill" id="aDecap" runat="server">Decap</a></li>
                        <li class="nav-item"><a class="nav-link" href="#MainContent_tabExtendedImagery" data-toggle="pill" id="aExtendedImagery" runat="server">Extended Imagery</a></li>
                        <li class="nav-item"><a class="nav-link" href="#Summary" data-toggle="pill" id="aSummary">Summary</a></li>
                    </ul>
                </div>
                <%--  End Nav Pills--%>
                <div class="col-sm-10">
                    <%--Tab Content--%>
                    <div class="tab-content clearfix">
                        <%-- Header Tab--%>
                        <div class="tab-pane active" id="Header">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="report-section-title">
                                            <%-- <label>Header:</label>--%>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>
                                                        MPN:
                                                    </label>
                                                    <asp:Label ID="lblHeaderMPN" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>
                                                        MFG:
                                                    </label>
                                                    <asp:Label ID="lblHeaderMFG" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>
                                                        Date:
                                                    </label>
                                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>
                                                        ID:
                                                    </label>
                                                    <asp:Label ID="lblReportID" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-1">
                                                </div>
                                            </div>
                                        </div>
                                        <hr />


                                    </div>

                                    <div class="panel-body report-section-data">
                                        <asp:Panel ID="pnlHeaderData" runat="server" Visible="false">

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="gcat-report-test-title">Customer:</label>
                                                    <asp:Label ID="lblCustomer" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="gcat-report-test-title">Sales Order:</label>
                                                    <asp:Label ID="lblSalesOrder" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="gcat-report-test-title">Cust PO:</label>
                                                    <asp:Label ID="lblHeaderCustomerPO" runat="server"></asp:Label>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlSaleInfo" runat="server">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label class="gcat-report-test-title">Rep:</label>
                                                        <asp:Label ID="lblHeaderAccountManager" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="gcat-report-test-title">Cust Part:</label>
                                                        <asp:Label ID="lblCustomerPart" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="gcat-report-test-title">QTY:</label>
                                                        <asp:Label ID="lblQuantity1" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Summary Notes:</label>
                                                    <asp:Label ID="lblHeaderSummaryNotes" runat="server"></asp:Label>
                                                    <%--<asp:TextBox ID="txtHeaderSummaryNotes" runat="server" TextMode="MultiLine" Style="width: 100%; max-width: 100%; height: 50px;" placeholder="Please provide a summary of the test results upon completion" CssClass="form-control"></asp:TextBox>--%>
                                                </div>
                                            </div>

                                            <%-- Imagery--%>
                                            <hr />
                                            <asp:Panel ID="pnlHeaderImagery" runat="server">
                                                <div class="report-section-title">
                                                    <label>Header Imagery</label>
                                                </div>

                                                <div class="row">
                                                    <%--<asp:Panel ID="pnlHeaderImagery" runat="server">--%>
                                                    <div class="col-sm-12 imageManagerGroup">
                                                        <uc1:InspectionImageManagerControl runat="server" ID="imHeader1" />
                                                        <uc1:InspectionImageManagerControl runat="server" ID="imHeader2" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <%-- End Header Tab--%>

                        <%--Component Identity--%>
                        <div class="tab-pane" id="ComponentIdentity">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading report-section-title">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Component Identification:</label>
                                                <asp:LinkButton ID="lbGetDataSheet" runat="server" OnClick="lbGetDataSheet_Click">
                                                     View  Datasheet
                                                </asp:LinkButton>
                                            </div>

                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body  report-section-data">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="gcat-report-test-title">RoHS Compliant:</label>
                                                <asp:Label ID="lblComponentIdentityRohs_compliant" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="gcat-report-test-title">Condition:</label>
                                                <asp:Label ID="lblComponentIdentityCondition" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="gcat-report-test-title">Type:</label>
                                                <asp:Label ID="lblComponentIdentityType" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label class="gcat-report-test-title">Memory Device:</label>
                                                <asp:Label ID="lblComponentIdentityMemoryDevice" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="gcat-report-test-title">Moisture Level:</label>
                                                <asp:Label ID="lblComponentIdentityMoistureLevel" runat="server"></asp:Label>

                                            </div>
                                            <div class="col-sm-3">
                                                <label class="gcat-report-test-title">Traceability:</label>
                                                <asp:Label ID="lblComponentIdentityTraceability" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label class="gcat-report-test-title">Package Type:</label>
                                                <asp:Label ID="lblComponentIdentityPackageType" runat="server"></asp:Label>
                                                <%--<asp:TextBox ID="txtComponentIdentityPackageType" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="gcat-report-test-title">Lead / Pin / BGA Pitch:</label>
                                                <asp:Label ID="lblComponentIdentityLeadPinBGAPitch" runat="server"></asp:Label>
                                                <%--<asp:TextBox ID="txtComponentIdentityLeadPinBGAPitch" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="gcat-report-test-title">Lead / Pin / BGA Count:</label>
                                                <asp:Label ID="lblComponentIdentityLeadPinBGACount" runat="server"></asp:Label>
                                                <%--<asp:TextBox ID="txtComponentIdentityLeadPinBGACount" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label class="gcat-report-test-title">Length</label>
                                                <asp:Label ID="lblComponentIdentityLength" runat="server"></asp:Label>
                                                <%--<asp:TextBox ID="txtComponentIdentityLength" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="gcat-report-test-title">Width:</label>
                                                <asp:Label ID="lblComponentIdentityWidth" runat="server"></asp:Label>
                                                <%--<asp:TextBox ID="txtComponentIdentityWidth" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="gcat-report-test-title">Thickness:</label>
                                                <asp:Label ID="lblComponentIdentityThickness" runat="server"></asp:Label>
                                                <%--<asp:TextBox ID="txtComponentIdentityThickness" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--End Component Identity--%>

                        <%--Visual Component--%>
                        <div class="tab-pane" id="VisualComponent">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading report-section-title">

                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label>External Visual Component Analysis</label>
                                            </div>
                                            <div class="col-sm-3">
                                                Sample QTY:
                                                <asp:Label ID="lblVisualComponentSamplyQTY" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Dimension</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Actual</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Result</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Comments</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        Width:
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="lblVisualComponentWidthActual" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="lblVisualComponentPassWidth" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:Label ID="lblVisualComponentPassWidthComment" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        Length:
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="lblVisualComponentLengthActual" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="lblVisualComponentPassLength" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">

                                                        <asp:Label ID="lblVisualComponentPassLengthComment" runat="server"></asp:Label>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        Thickness:
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="lblVisualComponentThicknessActual" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="lblVisualComponentPassThickness" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:Label ID="lblVisualComponentPassThicknessComment" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="row">
                                                    <hr />
                                                    <div class="col-sm-6">
                                                        <label>Package Connection Type:</label>
                                                        <asp:Label ID="lblVisualComponentPackageConnectionType" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Date Codes:</label>
                                                        <asp:Label ID="lblComponentIdentityDateCodes" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <%--Lead Package Grid--%>
                                                <div class="row" id="divVisualComponentCastellation" runat="server">
                                                    <div class="col-sm-6">
                                                        <label>Lead / Pin / BGA / Castellation Count:</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:Label ID="lblVisualComponentCastellationResult" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label>Property</label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Result</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label>Comments</label>
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentLeadPadCondition" runat="server">
                                                            <div class="col-sm-4">
                                                                Lead and Pad Condition
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentLeadPadConditionResult" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentLeadPadConditionComment" runat="server"></asp:Label>
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentCoplanarity" runat="server">
                                                            <div class="col-sm-4">
                                                                Lead Coplanarity
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentCoplanarityResult" runat="server"></asp:Label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentCoplanarityComment" runat="server"></asp:Label>

                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentOxidation" runat="server">
                                                            <div class="col-sm-4">
                                                                Oxidation / Discoloration
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentOxidationResult" runat="server"></asp:Label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentOxidationComment" runat="server"></asp:Label>

                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentExposedBaseMetal" runat="server">
                                                            <div class="col-sm-4">
                                                                Signs of Exposed base metal
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentExposedBaseMetalResult" runat="server"></asp:Label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentExposedBaseMetalComment" runat="server"></asp:Label>

                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentSolderBalls" runat="server">
                                                            <div class="col-sm-4">
                                                                Solder Balls / Spheres
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentSolderResult" runat="server"></asp:Label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentSolderComment" runat="server"></asp:Label>

                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentConcentricity" runat="server">
                                                            <div class="col-sm-4">
                                                                Uniform / Concentricity
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentConcentricityResult" runat="server"></asp:Label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentConcentricityComment" runat="server"></asp:Label>

                                                            </div>
                                                        </div>

                                                        <div class="row" id="divVisualComponentDamagedSpheres" runat="server">
                                                            <div class="col-sm-4">
                                                                Crushed Damaged Spheres
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lblVisualComponentDamagedSpheresResult" runat="server"></asp:Label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:Label ID="lblVisualComponentDamagedSpheresComment" runat="server"></asp:Label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <%-- Imagery--%>
                                                <hr />
                                                <asp:Panel ID="pnlVisualCompImagery" runat="server">
                                                    <div class="report-section-title">
                                                        <label>Visual Component Imagery</label>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12 imageManagerGroup">
                                                            <uc1:InspectionImageManagerControl runat="server" ID="imVisualComponent1" />
                                                            <uc1:InspectionImageManagerControl runat="server" ID="imVisualComponent2" />
                                                            <uc1:InspectionImageManagerControl runat="server" ID="imVisualComponent3" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--End Visual Component--%>

                        <%-- Visual Surface--%>
                        <div class="tab-pane" id="VisualSurface">

                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading report-section-title">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label>External Visual Surface Analysis (IDEA-1010)</label>
                                            </div>

                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblVisualSurfaceSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Logos / Part Numbers</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblVisualSurfaceLogo" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblVisualSurfaceLogoComment" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Top Surface</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblVisualSurfaceTop" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblVisualSurfaceTopComment" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Bottom Surface</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblVisualSurfaceBot" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblVisualSurfaceBotComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <%-- Imagery--%>
                                        <hr />
                                        <asp:Panel ID="pnlVisualSurfaceImagery" runat="server">
                                            <div class="report-section-title">
                                                <label>Visual Surface Imagery</label>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 imageManagerGroup">
                                                    <uc1:InspectionImageManagerControl runat="server" ID="imVisualSurface1" showDescription="false" />
                                                    <uc1:InspectionImageManagerControl runat="server" ID="imVisualSurface2" />
                                                    <uc1:InspectionImageManagerControl runat="server" ID="imVisualSurface3" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--End Visual Surface--%>

                        <%--Required--%>
                        <div class="tab-pane" id="Required">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading report-section-title">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label>Required Tests</label>
                                            </div>
                                            <div class="col-sm-1">
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body  report-section-data">
                                        <%--Standardized Tests--%>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="gcat-report-test-title">Standardized Test Requirements:</label>
                                                <asp:Label ID="lblStandardizedTests" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <%--Customer Requirement--%>
                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-12">
                                                <label class="gcat-report-test-title">Additional Customer Requirements:</label>
                                                <asp:Label ID="lblCustomer_RequirementA" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <hr />
                                            <%--Test Types--%>
                                            <div class="col-sm-12">
                                                <label class="gcat-report-test-title">Tests Performed:</label><br />
                                                <asp:Label ID="lblTestsPerformed" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-12">
                                                <br />
                                                <label class="gcat-report-test-title">Other Testing Performed:</label>
                                                <asp:Label ID="lblTest_TypeJ" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--End Required--%>

                        <%-- Marking Permenancy --%>
                        <div class="tab-pane" id="tabMarkingPerm" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading report-section-title">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label>Marking Permenancy Analysis</label>

                                            </div>
                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblMarkingPermSampleQTY" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Acetone Test</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblMarkingPermAcetone" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblMarkingPermAcetoneComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Alcohol Mineral Spirits</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblMarkingPermAlcohol" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblMarkingPermAlcoholComment" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Scrape Test</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblMarkingPermScrape" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblMarkingPermScrapeComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="gcat-testing-title">Secondary Coating</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblMarkingPermSecondary" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblMarkingPermSecondaryComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="gcat-testing-title">Part Number / Marking</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblMarkingPermPartMarking" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblMarkingPermPartMarkingComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="gcat-testing-title">Case Marking / Logo</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblMarkingPermCase" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblMarkingPermCaseComment" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlMarkingPermImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Marking Permenancy Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imMarkingPerm1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imMarkingPerm2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imMarkingPerm3" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <%--End Marking Permenancy--%>

                        <%-- Xray --%>
                        <div class="tab-pane" id="tabXray" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading report-section-title">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label>X-Ray Analysis / Die Verification</label>
                                            </div>
                                            <div class="col-sm-3">
                                                Sample QTY:<asp:Label ID="lblXraySampleQTY" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Die Present</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXrayDiePresent" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXrayDiePresentComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Bond Wires Present?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXrayBondWires" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXrayBondWiresComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Damaged / Missing Bond Wires?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXrayDamagedBond" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXrayDamagedBondComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Die Lead Uniformity</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXrayDieLead" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXrayDieLeadComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Die Frame Size / Location Consistent?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXrayDieFrame" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXrayDieFrameComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                    </div>
                                    <%-- Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlXrayImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>X-Ray Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imXray1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imXray2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imXray3" />
                                            </div>
                                        </div>
                                    </asp:Panel>

                                </div>
                            </div>
                        </div>
                        <%--End Xray--%>

                        <%--  Basic Electrical --%>
                        <div class="tab-pane" id="tabBasicElectrical" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>Basic Electrical Testing</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblBasicElectricalSampleQTY" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Test / Application Requirement:</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Temperature:</label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Result:</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblBasicElectricalTest1" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblBasicElectricalCondition1" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:Label ID="lblBasicElectricalResult" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-12">
                                                <label>Result Notes:</label><br />
                                                <asp:Label ID="lblBasicElectricalNotes" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlBasicElectricalImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Basic Electrical Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imBasicElectrical1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imBasicElectrical2" />

                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <%--End Basic Electrical --%>

                        <%--  Sentry --%>
                        <div class="tab-pane" id="tabSentry" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>Sentry V-I Signature Analysis</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblSentrySampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Service provider required to complete analysis?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSentrySPRequired" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblSentrySPRequiredComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Package Mount Type:</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblSentryPackageMountType" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Signatures Match Standard?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSentrySignaturesMatch" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblSentrySignaturesMatchComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Open / shorts detected?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSentryOpenShorts" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblSentryOpenShortsComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Passed all signature analysis?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSentryPassedAllSignatureAnalysis" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblSentryPassedAllSignatureAnalysisComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <%--     <div class="row">                                            
                                            <div class="col-sm-6">
                                                <label>Signature VI Report Required?</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblSentrySignatureReportRequired" runat="server"></asp:Label>
                                            </div>
                                        </div>--%>

                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label>Overall Result</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:Label ID="lblSentryOverallResult" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlSentryImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Sentry VI Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imSentry1" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <%--End Sentry--%>

                        <%--  Solderability --%>
                        <div class="tab-pane" id="tabSolderability" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="report-section-title">
                                                    <label>
                                                        Solderability Analysis</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblSolderSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Service provider required to complete analysis?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSolderSPRequired" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblSolderSPRequiredComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Package Mount Type:</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:Label ID="lblSolderPackageMountType" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label>Wetting Acceptable?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSolderWetting" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblSolderWettingComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label>RoHS?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSolderRohs" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblSolderRohsComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label>Void Free Leads?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSolderVoid" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblSolderVoidComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label>Contaminant Free?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblSolderCont" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblSolderContComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlSolderabilityImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Solderability Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imSolder1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imSolder2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imSolder3" />
                                            </div>
                                        </div>
                                    </asp:Panel>


                                </div>
                            </div>
                        </div>
                        <%--End Solderability--%>

                        <%--  XRF --%>
                        <div class="tab-pane" id="tabXRF" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>XRF Analysis</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblXRFSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Service provider required to complete analysis?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXRFSPRequired" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXRFSPRequiredComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <label>Package Mount Type:</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXRFPackageType" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-4">
                                                <label>Lead Materials Match?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXRFLeadMaterialsMatch" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXRFLeadMaterialsMatchComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-4">
                                                <label>Foreign Materials Detected?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblXRFForeignMaterials" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblXRFForeignMaterialsComment" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                      <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlXrfImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>XRF Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imXRF1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imXRF2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imXRF3" />
                                            </div>
                                        </div>
                                    </asp:Panel>



                                </div>
                            </div>
                        </div>
                        <%--End XRF--%>

                        <%--  Eprom --%>
                        <div class="tab-pane" id="tabEPROM" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>
                                                        EPROM Testing and Programming Analysis
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                Sample QTY: 
                                                    <asp:Label ID="lblEpromSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Service provider required to complete analysis?</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblEpromSPRequired" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblEpromSPRequiredComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-6">
                                                <label>Package Mount Type:</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblEpromPackageMount" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-4">
                                                <label>Checksum Analysis</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblEpromChecksum" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblEpromChecksumComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-4">
                                                <label>Programming Required?</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblEpromProgrammingRequired" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row" id="divEpromVerify" runat="server">

                                            <div class="col-sm-4">
                                                <label>Verify Program Load</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblEpromVerify" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblEpromVerifyComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--End Eprom--%>



                        <%-- Heated Solvents --%>
                        <div class="tab-pane" id="tabHeatedSolvents" runat="server">

                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>
                                                        Heated Solvents Analysis
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblHeatedSolventsSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Methyl Pyrrolidinone</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblHeatedMeth" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblHeatedMethComment" runat="server"></asp:Label>
                                            </div>
                                        </div>



                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label>Dynasolve 750</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblHeatedDyn" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblHeatedDynComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label>Secondary Coating (no evidence)</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblHeatedSecondary" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblHeatedSecondaryComment" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlHeatedImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Heated Solvents Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imHeated1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imHeated2" />
                                            </div>
                                        </div>
                                    </asp:Panel>


                                </div>
                            </div>
                        </div>
                        <%--End Heated Solvents--%>

                        <%--  Decapsulation and Die Analysis --%>
                        <div class="tab-pane" id="tabDecap" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>
                                                        Decapsulation and Die Analysis
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                Sample QTY:
                                                    <asp:Label ID="lblDecapSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>MFG Logo / ID Present</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblDecapMfgLogo" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblDecapMfgLogoComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label>Logo / ID Match Casing</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblDecapCasing" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblDecapCasingComment" runat="server"></asp:Label>
                                            </div>
                                        </div>



                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label>P/N and Die# Match</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblDcapPartDie" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblDcapPartDieComment" runat="server"></asp:Label>
                                            </div>
                                        </div>



                                        <div class="row">
                                            <hr />
                                            <div class="col-sm-4">
                                                <label>Die and Bond Wires</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblDecapDieBond" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblDecapDieBondComment" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlDecapImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Decapsulation and Die Analysis Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imDecap1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imDecap2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imDecap3" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imDecap4" />
                                            </div>
                                        </div>
                                    </asp:Panel>


                                </div>
                            </div>
                        </div>
                        <%--End  Decapsulation and Die Analysis--%>


                        <%--  Extended Testing --%>
                        <div class="tab-pane" id="tabExtendedTesting" runat="server">
                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>
                                                        Extended Testing
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                Sample QTY: 
                                                        <asp:Label ID="lblExtendedTestingSampleQTY" runat="server"></asp:Label>

                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Testing Category:</label>
                                                <asp:Label ID="lblExtCategory" runat="server"></asp:Label>
                                            </div>

                                            <div class="col-sm-6">
                                                <label>Overall Result:</label>
                                                <asp:Label ID="lblExtTestingResult" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <%--   <div class="row">
                                            <div class="col-sm-6">
                                                <label>Additional Testing:</label>
                                                <asp:Label ID="lblExtCategoryOther" runat="server"></asp:Label>
                                            </div>
                                        </div>--%>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Condition Tested:</label>
                                                <asp:Label ID="lblExtCondition1" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Measurement:</label>
                                                <asp:Label ID="lblExtActual1" runat="server"></asp:Label>
                                            </div>
                                        </div>


                                        <%-- <label>Result:</label><asp:Label ID="lblExtResult1" runat="server"></asp:Label><br />--%>

                                        <label>Additional Notes:</label>
                                        <asp:Label ID="lblExtNotes" runat="server"></asp:Label>
                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlExtImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Extended Testing Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imExt1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imExt2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imExt3" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imExt4" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>

                            </div>
                        </div>
                        <%--End Extended Testing--%>


                        <%--  Moisture / Baking--%>
                        <div class="tab-pane" id="tabMoistureBaking" runat="server">

                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>Moisture / Baking</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="control-group" style="display: inline-block;">
                                                    <label>Total Qty:</label>
                                                </div>
                                                <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                    <asp:TextBox ID="txtMoistureBakingSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <%--Row2--%>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                MSL:
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtMoistureBakingMSL" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">
                                                Process:
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtMoistureBakingProcess" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                        </div>
                                        <%--Row3--%>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                Bake Duration:
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtMoistureBakingDuration" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">
                                                Bake Temperature:
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtMoistureBakingTemp" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <%--Row4--%>
                                        <div class="row" id="divMoistureBakingResult" runat="server">
                                            <div class="col-sm-3">
                                                Overall Result
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMoistureBakingResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtMoistureBakingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>
                                    <%--Imagery--%>
                                    <hr />
                                    <asp:Panel ID="pnlMoistureBakingImagery" runat="server">
                                        <div class="report-section-title">
                                            <label>Moisture/Baking Imagery</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imMoistureBaking1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imMoistureBaking2" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imMoistureBaking3" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <%--End Imagery--%>
                                </div>
                            </div>
                        </div>
                        <%--End  Moisture / Baking--%>


                            <%--  Extended Imagery--%>
                     <div class="tab-pane" id="tabExtendedImagery" runat="server">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Extended Imagery</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-body">
                                  <div class="row">
                                      <div class="col.sm-12">
                                          <uc1:InspectionImageGallery runat="server" ID="iig" />
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>



                        <%--  Summary --%>
                        <div class="tab-pane" id="Summary">

                            <div class="panel-group">
                                <div class="panel panel-default ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="report-section-title">
                                                    <label>Summary of Results</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4"></div>

                                        </div>
                                        <hr />
                                    </div>
                                    <div class="panel-body report-section-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Test Performed</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Result</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Comments</label>
                                            </div>
                                        </div>

                                        <%--External Visual Summary--%>
                                        <div id="divSumExternalVisual" runat="server">
                                            External / Visual
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Logos / Part Numbers
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumExternalVisualLogosParts" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumExternalVisualLogosPartsComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Dimensional Requirements
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumExternalVisualDim" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumExternalVisualDimComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Lead / Package Condition
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumExternalVisualLeadPkg" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumExternalVisualLeadPkgComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Check for Signs of Surface Alteration
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumExternalVisualAlteration" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumExternalVisualAlterationComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End External Visual Summary--%>

                                        <%--Marking Permenancy Summary--%>
                                        <div id="divSumMarking" runat="server">
                                            <hr />
                                            <label>Marking Permenancy</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Acetone Test
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumMarkingAcetone" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumMarkingAcetoneComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Scrape Test
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumMarkingScrape" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumMarkingScrapeComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Check for Signs of Marking Alteration
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumMarkingAlteration" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumMarkingAlterationComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Marking Permenancy Summary--%>

                                        <%--Xray Summary--%>
                                        <div id="divSumXray" runat="server">
                                            <hr />
                                            <label>X-Ray Analysis</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Uniform Die / Wire Bonds
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumXrayUniform" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumXrayUniformComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Xray Summary--%>

                                        <%--Basic Electrical Summary--%>
                                        <div id="divSumBasicElec" runat="server">
                                            <hr />
                                            <label>Basic Electrical Analysis</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Basic Electrical Testing
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumBasicElectrical" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumBasicElectricalComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Basic Electrical Summary--%>

                                        <%--Sentry VI Summary--%>
                                        <div id="divSumSentry" runat="server">
                                            <hr />
                                            <label>Sentry VI Electrical Analysis</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Sentry VI Signature
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumElectricalVISignature" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumElectricalVISignatureComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%--End Sentry VI Summary--%>

                                        <%--Solderability Summary--%>
                                        <div id="divSumSolderability" runat="server">
                                            <hr />
                                            <label>Solderability Analysis</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Solder Wetting Acceptable
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumSolderWetting" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumSolderWettingComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Passed Solderability Testing
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumSolderPassSolderTesting" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumSolderPassSolderTestingComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Solderability Summary--%>

                                        <%--XRF Summary--%>
                                        <div id="divSumXRF" runat="server">
                                            <hr />
                                            <label>XRF Analysis</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Lead Materials Match
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumXRFLeadsMatch" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumXRFLeadsMatchComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Evidence of Foreign Material
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumXRFForeignMat" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumXRFForeignMatComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End XRF Summary--%>

                                        <%--EPROM Summary--%>
                                        <div id="divSumEPROM" runat="server">
                                            <hr />
                                            <label>EPROM Testing</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Checksum and Blank Check
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumEPROMChecksum" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumEPROMChecksumComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="divSumEpromVerify" runat="server">
                                                <div class="col-sm-4">
                                                    Validation
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumEPROMValidation" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumEPROMValidationComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End EPROM Summary--%>

                                        <%--Extended Testing Summary--%>
                                        <div id="divSumExtendedTesting" runat="server">
                                            <hr />
                                            <label>Extended Testing</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Extended Testing Result
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumExtendedTesting" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumExtendedTestingComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Extended Testing Summary--%>

                                        <%--Moisture / Baking Summary--%>
                                        <div id="divSumMoistureBaking" runat="server">
                                            <hr />
                                            <label>Moisture / Baking</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Moisture / Baking Result
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumMoistureBaking" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumMoistureBakingComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Moisture / Baking Summary--%>

                                        <%--Heated Solvents Summary--%>
                                        <div id="divSumHeatedSolvents" runat="server">
                                            <hr />
                                            <label>Heated Solvents Testing</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Methyl Pyrrolidinone Results
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumHeatedSolventsMeth" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumHeatedSolventsMethComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Dynasolve 750 Results
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumHeatedSolventsDyna" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumHeatedSolventsDynaComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Secondary Coating Results
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumHeatedSolventsSecondary" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumHeatedSolventsSecondaryComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Heated Solvents Summary--%>

                                        <%--Decap Summary--%>
                                        <div id="divSumDecap" runat="server">
                                            <hr />
                                            <label>De-Cap Analysis</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Wire Bond Integrity
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumDecapWireBondIntegrity" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumDecapWireBondIntegrityComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    Die Marking Verification
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Label ID="lblSumDecapDieMarking" runat="server" Enabled="false"></asp:Label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:Label ID="lblSumDecapDieMarkingComment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- End Decap Summary--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--End Summary--%>
                    </div>
                </div>
                <%-- End Tab Content--%>
            </div>
        </asp:Panel>
        <%-- End pnlGCATMain--%>
    </div>
    <%--End Container--%>

    <%--Save Panel--%>
    <div id="savePanel" style="position: fixed; top: 30%; right: 1em; line-height: 20px; min-width: 35px;">
        <%-- This min width fixes Chardin Overlay, but makes the button shift left--%>
        <div class="row">
            <div data-intro="Return to Quality Center" data-position="left">
                <a href="my_qc.aspx" class="btn btn-smc btn-smc-warning" title="Return to QC Center">
                    <span aria-hidden="true" class="fa fa-reply" style="font-size: 30px; width: 25px;"></span>
                </a>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div data-intro="Download a PDF of this report" data-position="left">
                <asp:LinkButton ID="lbPDF" runat="server" CssClass="btn btn-smc btn-smc-pdf" OnClick="lbPDF_Click" OnClientClick="slideAlert('success', 'Your PDF is being generated and will be downloaded momentarily.');" ToolTip="Download PDF">
                        <span aria-hidden="true" class="far fa-file-pdf" style="font-size:30px; width:25px;"></span>
                </asp:LinkButton>
            </div>

        </div>
    </div>
    <%-- End Save Panel--%>

    <%-- MSL Modal--%>
    <div id="mdlMSL" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <label>Moisture Level Designations</label>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-8">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <label>MSL 6</label>
                                    – Mandatory Bake before use
                                </li>
                                <li class="list-group-item">
                                    <label>MSL 5A</label>
                                    – 24 hours
                                </li>
                                <li class="list-group-item">
                                    <label>MSL</label>
                                    5 – 48 hours
                                </li>
                                <li class="list-group-item">
                                    <label>MSL 4</label>
                                    – 72 hours
                                </li>
                                <li class="list-group-item">
                                    <label>MSL 3</label>
                                    – 168 hours
                                </li>
                                <li class="list-group-item">
                                    <label>MSL 2A</label>
                                    – 4 weeks
                                </li>
                                <li class="list-group-item">
                                    <label>MSL 2</label>
                                    – 1 year
                                </li>
                                <li class="list-group-item">
                                    <label>MSL 1</label>
                                    – Unlimited
                                </li>
                                <li class="list-group-item">
                                    <label>MSL N/A</label>
                                    - No applicable MSL
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <%--End MSL Modal--%>

    <%-- Datasheets Modal--%>
    <asp:UpdatePanel ID="upDatasheets" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div id="mdlDatasheets" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg modal-dialog-scroll">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <label>Datasheets:</label>
                        </div>
                        <div class="modal-body modal-body-scroll">
                            <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
                                <asp:GridView ID="gvDatasheets" runat="server" CssClass="table table-hover table-striped CenterDataView" GridLines="None" AutoGenerateColumns="false">
                                    <EmptyDataTemplate>
                                        <div class="alert alert-warning" style="text-align: center;">
                                            <h5>No PCN Data found for search term.</h5>
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="PartNumber" HeaderText="Part" SortExpression="PartNumber" />
                                        <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" SortExpression="Manufacturer" />
                                        <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" SortExpression="Manufacturer" />
                                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                        <asp:TemplateField SortExpression="Datasheet" HeaderText="Datasheet">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlDatasheet" runat="server" CssClass="btn btn-sm btn-success" NavigateUrl='<%# Eval("Datasheet") %>' Target="_blank"><span style="font-size:20px;" class="fa fa-list-alt"></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle Wrap="True" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- End Datasheets Modal--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

