﻿<%@ Page Title="Order Center" Language="C#" MasterPageFile="~/hs_flex.Master" AutoEventWireup="true" CodeFile="Orders.aspx.cs" Inherits="secure_members_buyers_orders" %>

<%@ MasterType VirtualPath="~/Customer.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        .modal-xl {
            padding-top: 18vh;
            width: 70vw;
            margin: auto;
        }
    </style>

</asp:Content>

<asp:Content ID="LeadContent_Title" ContentPlaceHolderID="TitleContent" runat="Server">   
        <h6 class="relative">Order Center</h6>
</asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <%-- Headers Grid--%> 
            <div class="card">
                <div class="card-body">
                    <%--Header Grid--%>
                    <uc1:sm_datatable runat="server" ID="smdtHeader" />
                </div>              
            </div>
     

    <%--Details Modal--%>
    <div class="modal" tabindex="-1" role="dialog" id="mdlDetails">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label>
                        <asp:Label ID="lblModalTitle" runat="server"></asp:Label>
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <%--Details Grid--%>
                    <uc1:sm_datatable runat="server" ID="smdtDetails" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
  
</asp:Content>

