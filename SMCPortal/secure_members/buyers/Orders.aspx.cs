﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_members_buyers_orders : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    MembershipUser CurrentUser = System.Web.Security.Membership.GetUser();
    SM_Tools tools = new SM_Tools();
    //List<orddet_line> DetailData;   
    string strSelectedPart;
    company CurrentCompany = null;
    List<ordhed_sale> oList = new List<ordhed_sale>();
    List<insp_head> ideaHeaders = new List<insp_head>();




    protected void Page_Load(object sender, EventArgs e)
    {
        smdtHeader.RowDataBound += new GridViewRowEventHandler(gvOrderHeader_RowDataBound);
        smdtDetails.RowDataBound += new GridViewRowEventHandler(gvOrderDetails_RowDataBound);
        tools.ShowNavHelp(Page);
        GetCurrentCompany();
        LoadData();
        LoadHeaderGrid();


    }

    private void LoadData()
    {
        string companyID = CurrentCompany.unique_id;
        using (sm_portalEntities pdc = new sm_portalEntities())
            ideaHeaders = pdc.insp_head.Where(w => w.customer_uid == companyID).ToList();
        using (RzDataContext rdc = new RzDataContext())
            oList = rdc.ordhed_sales.Where(w => w.base_company_uid == companyID
             && (bool)w.isvoid != true
             && !w.ordernumber.ToLower().Contains("void")
              && w.orderdate > new DateTime(2015, 08, 14)
              && !w.orderreference.Contains("CR%")).ToList();


    }

    private void GetCurrentCompany()
    {

        if (SM_Global.isDemoUser)
            CurrentCompany = SM_Global.DemoSalesCompany;
        else if (SM_Global.PreviewProfile != null)
            CurrentCompany = SM_Global.PreviewCompany;
        else if (SM_Global.CurrentCompany != null)
            CurrentCompany = SM_Global.CurrentCompany;
    }


    protected void LoadHeaderGrid()
    {





        if (SM_Global.isDemoUser)
        {
            //For Demo, constring to 2018+
            oList = oList.Where(w => w.orderdate >= new DateTime(2018, 01, 01) && w.ordertotal > 0).ToList();
        }

        var headerQuery = oList.Select(s => new
        {
            Details = "Details",
            Order = s.ordernumber,
            PO = s.orderreference,
            Date = s.orderdate.Value.ToShortDateString(),
            Total = "$" + Tools.Number.MoneyFormat_2_6(s.ordertotal ?? 0),
            Company = s.companyname,
            ID = s.unique_id,
            Reports = "View Report(s)"

        }).ToList();

        smdtHeader.theGridView.DataKeyNames = new string[] { "ID" };
        smdtHeader.sortColumn = 3;
        smdtHeader.sortDirection = "desc";
        smdtHeader.dataSource = headerQuery;
        smdtHeader.emptyDataText = "No order data found for your account.";
        smdtHeader.loadGrid();
    }


    protected List<orddet_line> GetDetailData(string orderID)
    {
        List<orddet_line> lineList = new List<orddet_line>();
        try
        {

            //DetailData = new List<orddet_line>();

            if (CurrentCompany != null)
            {
                //string orderID = smdtHeader.theGridView.SelectedValue.ToString();
                //TheOrder = HeaderData.Where(w => w.unique_id == orderID).FirstOrDefault();
                using (RzDataContext rdc = new RzDataContext())
                {
                    lineList = rdc.orddet_lines.Where(w => w.customer_uid == CurrentCompany.unique_id
               && w.orderid_sales == orderID
               && w.orderdate_sales >= Convert.ToDateTime("8-14-2015")
               && (w.status != "Void" && !w.status.ToLower().Contains("vendor rma")
               && !(w.status.ToLower() == "scrapped" && w.unit_price <= 0))
               && !(w.was_vendrma == true && w.was_rma == false)).ToList();

                }


                var groupedQuery = lineList.GroupBy(a => new { a.internal_customer, a.status }).Select(g => new orddet_line()
                {
                    //is there a quoted_partnumber?  User quoted_partnumber, if not,
                    //is it stock? Use fullpartnumber
                    //Are we missing a quote line?  return "Processing"
                    //Is the quote line uid a gcat id, return "GCAT - LAB ANALYSIS"
                    //All others, return fullpartnumber
                    fullpartnumber = !string.IsNullOrEmpty(g.First().quoted_partnumber) ? g.First().quoted_partnumber.ToUpper() : g.First().stocktype == "Stock" ? g.First().fullpartnumber.ToUpper() : g.First().quote_line_uid.Length == 0 ? "Processing." : g.First().quote_line_uid == "GCAT_QUOTE_UID" ? "GCAT - LAB ANALYSIS" : g.First().fullpartnumber.ToUpper(),
                    unique_id = g.First().unique_id,
                    quantity = g.Sum(q => q.quantity),
                    internal_customer = SM_Global.isDemoUser ? "ABC12346-7" : g.First().stocktype == "Stock" ? g.First().internal_customer.ToUpper() : g.First().quote_line_uid == "GCAT_QUOTE_UID" & g.First().fullpartnumber.Contains("GCAT") && string.IsNullOrEmpty(g.First().internal_customer) ? "GCAT" : g.First().internal_customer.ToUpper() ?? "",
                    customer_name = SM_Global.isDemoUser ? "Sensible Micro Corporation" : g.First().customer_name ?? "",
                    datecode = g.First().quote_line_uid == "GCAT_QUOTE_UID" ? "N/A" : g.First().datecode ?? "",
                    manufacturer = g.First().quote_line_uid == "GCAT_QUOTE_UID" ? g.First().manufacturer.ToUpper() : g.First().manufacturer.ToUpper() ?? "", //GCAT LINES - Show the Lines MFG (Sensibnle Micro), else Quote MFG
                    quote_line_uid = g.First().quote_line_uid ?? "",
                    ordernumber_sales = g.First().ordernumber_sales ?? "",
                    seller_name = g.First().seller_name ?? "",
                    status = g.First().status == "Buy" ? "Inbound" : g.First().status == "Open" ? "In House" : g.First().status ?? "",
                    customer_dock_date = g.First().customer_dock_date <= new DateTime(1903, 01, 01) ? new DateTime(1900, 01, 01) : g.First().customer_dock_date,
                    shipvia_invoice = g.First().shipvia_invoice ?? "",
                    tracking_invoice = SM_Global.isDemoUser ? "1Z66442F0393351652" : string.IsNullOrEmpty(g.First().tracking_invoice) ? "Processing" : g.First().tracking_invoice,
                    unit_price = g.First().unit_price,
                    unit_cost = g.First().unit_cost

                }).OrderByDescending(o => o.customer_dock_date).ToList();
                //Casting the above ot a list so I can remove GCAT with no cost and no price
                lineList = groupedQuery.ToList();
            }


            return lineList;
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            return null;
        }

    }


    protected void lbDetails_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow row = (GridViewRow)lb.NamingContainer;
            int i = Convert.ToInt32(row.RowIndex);
            string orderId = smdtHeader.theGridView.DataKeys[i].Value.ToString();
            var detailsQuery = GetDetailData(orderId).Select(s => new
            {

                PartData = "PartData",
                Part = s.fullpartnumber.Trim().ToUpper(),
                Internal = s.internal_customer.Trim().ToUpper(),
                CustomerDock = s.customer_dock_date.Value.ToShortDateString(),
                DateCode = s.datecode.Trim().ToUpper(),
                Status = s.status.Trim().ToUpper(),
                ShipVia = s.shipvia_invoice.Trim().ToUpper(),
                MFG = s.manufacturer.Trim().ToUpper(),
                QTY = s.quantity.Value,
                Tracking = "Tracking",
                ID = s.unique_id,
            });
            smdtDetails.dataSource = detailsQuery;
            smdtDetails.theGridView.DataKeyNames = new string[] { "ID" };
            smdtDetails.sortColumn = 3;
            smdtDetails.sortDirection = "desc";
            smdtDetails.loadGrid();
            lblModalTitle.Text = "Order Details";
            tools.ShowModal(Page, "mdlDetails");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }



    protected void gvOrderHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //ID / Datakey column = 6
        int count = e.Row.Cells.Count;
        if (count > 1)
            e.Row.Cells[6].CssClass = "hiddencol";



        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Hid Button for incomplete inspecitons
            LinkButton lbDetails = AddLinkButton(e.Row, "details");
            HyperLink hlReports = AddHyperLink(e.Row, "reports");
            string cpo = e.Row.Cells[2].Text;
            bool complete = ideaHeaders.Any(h => h.customer_po == cpo && h.is_complete == true && h.result == "Pass");
            if (!complete)
            {
                hlReports.Text = "In progress";
                hlReports.CssClass = "InactiveHyperlink";
                hlReports.Enabled = false;
            }
            else
                hlReports.Enabled = true;



        }

        if (e.Row.RowType == DataControlRowType.Header)//Header row only
        {

            e.Row.Cells[0].Attributes.Add("data-intro", "Click Details to view line-item details of our orders");
            e.Row.Cells[0].Attributes.Add("data-position", "bottom");
            e.Row.Cells[7].Attributes.Add("data-intro", "Click View Reports to view any quality reports associated with your order");
            e.Row.Cells[7].Attributes.Add("data-position", "bottom");
        }

    }

    protected void gvOrderDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Handle No Tracking number
        List<string> HideTrackingButton = new List<string>() { "Processing", "" };
        string line_uid = "";



        if (e.Row.Cells.Count > 1)
        {
            //10 is the hidden ID column           
            e.Row.Cells[10].CssClass = "hiddencol";

            //Hide the tracking button for non-shipped items          
            if (e.Row.RowType == DataControlRowType.DataRow)
            {  //Hid Button for incomplete inspecitons
                HyperLink hlPartData = AddHyperLink(e.Row, "partdata");
                HyperLink hlTracking = AddHyperLink(e.Row, "tracking");

                //Get the rowIndex from the event, use it to get DataKey from parent row
                int rowIndex = e.Row.RowIndex;
                line_uid = smdtDetails.theGridView.DataKeys[rowIndex].Value.ToString();
                if (string.IsNullOrEmpty(line_uid))
                    return;
                //Line Data
                orddet_line theLine = null;
                using (RzDataContext rdc = new RzDataContext())
                    theLine = rdc.orddet_lines.Where(w => w.unique_id == line_uid).FirstOrDefault();
                if (theLine == null)
                    return;

                //Tracking Data
                string trackingNumber = theLine.tracking_invoice;
                string carrier = theLine.shipvia_invoice;
                if (!HideTrackingButton.Contains(trackingNumber))
                {
                    hlTracking.NavigateUrl = ShippingLogic.Tracking.getTrackShipmentUrl(carrier, trackingNumber);
                    hlTracking.Target = "_blank";
                    hlTracking.Text = trackingNumber;
                }
                else
                    hlTracking.Visible = false;

                //Part Detail Data                
                string partnumber = e.Row.Cells[1].Text;
                hlPartData.NavigateUrl = "/public/search/part_search.aspx?pn=" + partnumber;
                hlTracking.Target = "_blank";



                //Fix null dates to "N/a"
                if (e.Row.Cells[3].Text == "1/1/1900")
                    e.Row.Cells[3].Text = "N/A";
                if (e.Row.Cells[1].Text.ToLower().Contains("gcat"))
                {
                    e.Row.Cells[8].Text = "N/A";//shipvia
                    if (e.Row.Cells[9] != null)
                        e.Row.Cells[9].Text = "N/A";//

                }
            }
        }
    }


    private HyperLink AddHyperLink(GridViewRow row, string linkType)
    {
        HyperLink hl = new HyperLink();
        hl.Target = "_blank";
        int cell = 0;
        switch (linkType)
        {
            case "reports":
                {
                    cell = 7;
                    hl.ID = "lbReports";
                    string customerpo = row.Cells[2].Text;
                    hl.NavigateUrl = "/secure_members/qc_center/my_qc.aspx?cpo=" + customerpo;
                    hl.Text = "View Report(s)";
                    break;
                }
            case "partdata":
                {
                    hl.ID = "lbPartData";
                    cell = 0;
                    hl.Text = "Part Data";
                    break;
                }
            case "tracking":
                {
                    cell = 9;
                    hl.ID = "lbTracking";
                    hl.Text = "Tracking";
                    break;
                }
        }

        //Clear existing values
        row.Cells[cell].Text = string.Empty;
        //Add the Control
        row.Cells[cell].Controls.Add(hl);
        return hl;
    }

    private LinkButton AddLinkButton(GridViewRow row, string linkType)
    {
        LinkButton lb = new LinkButton();
        int cell = 0;

        switch (linkType)
        {
            case "details":
                {
                    lb.ID = "lbDetails";
                    cell = 0;
                    lb.Text = "Details";
                    lb.Click += new EventHandler(lbDetails_Click);
                    break;
                }

        }

        //Clear existing values
        row.Cells[cell].Text = string.Empty;
        //Add the Control
        row.Cells[cell].Controls.Add(lb);
        return lb;
    }
    protected void lbPartData_Click(object sender, EventArgs e)
    {
        var closeLink = (Control)sender;
        GridViewRow row = (GridViewRow)closeLink.NamingContainer;
        strSelectedPart = Tools.Strings.SanitizeInput(row.Cells[1].Text).ToUpper(); // here we are

        Response.Redirect("/public/search/part_search.aspx?pn=" + strSelectedPart, false);
    }
}
