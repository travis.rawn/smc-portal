﻿<%@ Page Title="Forms Center" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="forms.aspx.cs" Inherits="secure_members_Forms_Forms_main" %>

<asp:Content ID="LeadContent" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>FORMS</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="col-sm-1"></div>
    <div class="col-sm-10" data-intro="These are some of our most commonly requested forms." data-position="bottom">
        <div class="list list-group">          
            <a href="http://forms.sensiblemicro.com/view.php?id=11" target="_blank" class="list-group-item">
                <h4 class="list-group-item-heading">Sensible Micro Customer Satisfaction Survey (F-720-001 Rev F)</h4>
                <p class="list-group-item-text">Let us know how we are doing or how we can better serve you</p>
            </a>
            <a href="http://forms.sensiblemicro.com/view.php?id=8866" target="_blank" class="list-group-item">
                <h4 class="list-group-item-heading">Single Declaration of Regulatory Compliance (F-720-011 Rev D)</h4>
                <p class="list-group-item-text">Use this form to authorize shipment of regulated products on a per-order basis</p>
            </a>
            <a href="http://forms.sensiblemicro.com/view.php?id=9560" target="_blank" class="list-group-item">
                <h4 class="list-group-item-heading">Annual Declaration of Regulatory Compliance (F-720-010-Rev D)</h4>
                <p class="list-group-item-text">Use this form to authorize shipment of regulated products on an annual term basis</p>
            </a>
            <a href="http://forms.sensiblemicro.com/view.php?id=11570" target="_blank" class="list-group-item">
                <h4 class="list-group-item-heading">Sensible Micro Credit Application (F‐720‐004‐G)</h4>
                <p class="list-group-item-text">Use this form to submit your credit details</p>
            </a>
        </div>
    </div>
    <div class="col-sm-1"></div>





</asp:Content>

