﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_members_consign_consignment_center : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string strNoAccess = "";
            bool allowed = SM_Security.is_consignment_authorized(out strNoAccess);
            if (!allowed)
                throw new Exception(strNoAccess);

        }
        catch (Exception ex)
        {
            SM_Tools tools = new SM_Tools();
            //tools.HandleResult("fail", ex.Message);
            tools.HandleResult("fail", ex.Message, 0, true, "/secure_members/Default.aspx");

        }


    }
}