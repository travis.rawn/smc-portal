﻿<%@ Page Title="Consignment Activity" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="consignment_activity.aspx.cs" Inherits="secure_members_consign_consignment_activity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .page-control {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6 class="relative">Consignment Activity  </h6>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-3">
            <asp:DropDownList ID="ddlActivityType" runat="server" CssClass="form-control">
                <asp:ListItem Text="All" Value="all" />
                <asp:ListItem Text="Sales" Value="sale" />
                <asp:ListItem Text="Quotes" Value="quote" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-3">
            <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
        </div>
        <div class="col-sm-3">
            <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
        </div>
        <div class="col-sm-3">
            <asp:LinkButton ID="btnSearch" runat="server" CssClass="page-control updateProgress" Text="Refresh" ToolTip="Search" OnClick="btnSearch_Click" />
            <asp:LinkButton ID="btnExport" runat="server" CssClass="page-control" Text="Export" OnClick="btnExport_Click" OnClientClick="slideAlert( 'success', 'Processing, your export will be downloaded momentarily');" ToolTip="Export Consignment" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <hr />
            <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>

    <div class="row">
        <p style="font-weight: bold; font-size: 13px;">
            <em>*Please note, sale activity may not be final and is subject to change.
            </em>
        </p>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

