﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="consignment_center.aspx.cs" Inherits="secure_members_consign_consignment_center" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .portal-card-grid {
            justify-content: space-around;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6 class="relative">Consignment Center</h6>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="portal-card-grid">

        <a href="/secure_members/consign/consignment_activity.aspx" class="portal-card portal-card-grid__item" target="_blank">
            <img src="https://www.sensiblemicro.com/hubfs/chart-line-solid.svg" alt="" class="portal-card__icon">
            <%--<img src="https://www.sensiblemicro.com/hubfs/icon__search__lblue.svg" alt="" class="portal-card__icon">--%>
            <h2 class="portal-card__title">Consignment Activity
            </h2>
            <div class="portal-card__desc">
                <p>View quote and sale data related to your consigned items.</p>
            </div>
        </a>

        <%--   <a href="/secure_members/consign/current_consignment.aspx" class="portal-card portal-card-grid__item" target="_blank">
                            <img src="https://www.sensiblemicro.com/hubfs/pallet-solid.svg" alt="" class="portal-card__icon">
                            <h2 class="portal-card__title">Current Consignment
                            </h2>
                            <div class="portal-card__desc">
                                <p>View and export current consignment inventory levels.</p>
                            </div>
                        </a>--%>

        <a href="/secure_members/consign/consignment_status.aspx" class="portal-card portal-card-grid__item" target="_blank">
            <img src="https://www.sensiblemicro.com/hubfs/pallet-solid.svg" alt="" class="portal-card__icon">
            <h2 class="portal-card__title">Consignment Status
            </h2>
            <div class="portal-card__desc">
                <p>View and export current consignment inventory levels.</p>
            </div>
        </a>

        <%--   <a href="/secure_members/consign/consign_history.aspx" class="portal-card portal-card-grid__item" target="_blank">
                            <img src="https://www.sensiblemicro.com/hubfs/list-alt-regular.svg" alt="" class="portal-card__icon">
                            <h2 class="portal-card__title">Consignment History
                            </h2>
                            <div class="portal-card__desc">
                                <p>Track excecuted consignment payout data.</p>
                            </div>
                        </a>--%>
    </div>




</asp:Content>


