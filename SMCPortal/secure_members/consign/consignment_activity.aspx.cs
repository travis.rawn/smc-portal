﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;


public partial class secure_members_consign_consignment_activity : System.Web.UI.Page
{

    RzDataContext RDC = new RzDataContext();
    RzTools rzt = new RzTools();
    ConsignmentData con = new ConsignmentData();
    SM_Tools tools = new SM_Tools();
    DateTime StartDate;
    DateTime EndDate = DateTime.Today;
    ConsignmentData cData = new ConsignmentData();
    private List<ConsignmentData.ConsignmentActivityData> ConsignmentData
    {
        get
        {
            //List<ConsignmentData.ConsignmentActivityData> result = new List<ConsignmentData.ConsignmentActivityData>();
            if (ViewState["ConsignmentData"] != null)
                return (List<ConsignmentData.ConsignmentActivityData>)ViewState["ConsignmentData"];
            return new List<ConsignmentData.ConsignmentActivityData>();
        }
        set
        {
            ViewState["ConsignmentData"] = value;
        }
    }
    company ConsignmentCompany = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string strNoAccess = "";
            if (!SM_Security.is_consignment_authorized(out strNoAccess))
                throw new Exception(strNoAccess);

            LoadDates();
            SetConsignmentCompany();
            if (!Page.IsPostBack)
                LoadData();
            LoadGrid();
        }
        catch(Exception ex)
        {
            tools.HandleResult("fail", ex.Message, 0, true, "/secure_members/Default.aspx");

        }
      
    }
    private void LoadGrid()
    {


        smdt.dataSource = ConsignmentData.Select(s => new {
            Date = s.Date.ToShortDateString(),
            s.Type,
            s.PartNumber,
            s.MFG,
            s.QTY,
            UnitCost = s.UnitCost.ToString("C"),
            UnitPrice = s.UnitPrice.ToString("C"),
            TotalPrice = s.TotalPrice.ToString("C")



        }).OrderByDescending(o => o.Date);
        smdt.pageLength = 50;
        smdt.loadGrid();
    }
    private void LoadData()
    {
        //If no company is officially associated with the user, then ConsignmentCompany will be null, and the data will fail.  Notify the user, as well as IT team for resolution.
        if(ConsignmentCompany == null)
        {
            SystemLogic.Email.SendMail("consignment_portal@sensiblemicro.com", SystemLogic.Email.EmailGroup.Systems, "Consignmenr Company is Null", Membership.GetUser().UserName + " is trying to access Consignment Activity, yet no company is associated with the account.  Please assign this account to a company in the Portal admin panel.");
            throw new Exception("Cannot display consignment, your portal account is still being provisioned.  Support team has been notified, please try again later.");

        }

        ConsignmentData = cData.GetConsignmentActivity(ConsignmentCompany.unique_id, StartDate, EndDate, ddlActivityType.SelectedValue, "");        
    }
    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = new DateTime(2018, 01, 01);
            dtpEnd.SelectedDate = DateTime.Today;
        }

        StartDate = dtpStart.SelectedDate.Value;
        EndDate = dtpEnd.SelectedDate.Value;

    }
    private void SetConsignmentCompany()
    {
        if (SM_Global.PreviewUser != null)
            ConsignmentCompany = SM_Global.PreviewCompany;
        else if (SM_Global.isDemoUser)
            ConsignmentCompany = SM_Global.DemoConsignmentCompany;
        else
            ConsignmentCompany = SM_Global.CurrentCompany;

    }  
    protected void gvActivity_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            if (e.Row.Cells.Count > 1)
            {
                Label smvLabel = (Label)e.Row.Cells[5].FindControl("lblSMV");
                if (smvLabel.Text != "N/A")
                {
                    smvLabel.Style.Add("font-weight", "bold");
                    smvLabel.Style.Add("color", "red");
                }
            }

        }


    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            tools.HandleResult("success", "Processing, your export will be available momentarily", 7);
            string filename = Tools.Dates.get_datetime_for_file_export("consignment_activity", "today");
            tools.ExportListToCsv(ConsignmentData, filename);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            LoadData();
            LoadGrid();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
}