﻿<%@ Page Title="Consignment Status" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="consignment_status.aspx.cs" Inherits="secure_members_consignment_consignment_status" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .page-control {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="LeadContent" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6 class="relative">Consignment Status</h6>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row" style="margin-bottom: 10px;">      
        <div class="col-sm-3">
            <asp:LinkButton ID="btnRefresh" runat="server" CssClass="page-control updateProgress" Text="Refresh" ToolTip="Refresh Consignment Data" />
            <asp:LinkButton ID="btnExport" runat="server" CssClass="page-control" Text="Export" OnClick="btnExport_Click" OnClientClick="slideAlert( 'success', 'Processing, your export will be downloaded momentarily');" ToolTip="Export Consignment" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <hr />
            <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>
</asp:Content>

