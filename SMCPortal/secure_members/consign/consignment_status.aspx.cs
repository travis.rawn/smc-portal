﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_members_consignment_consignment_status : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    RzDataContext RZDC = new RzDataContext();
    List<string> DemoUsers = new List<string> { "christ", "kevint", "fredt" };
    company ConsignmentCompany;
    private List<stock_level_object> ConsignmentData
    {
        get
        {
            List<stock_level_object> result = null;
            if (ViewState["ConsignmentData"] != null)
                result = (List<stock_level_object>)ViewState["ConsignmentData"];
            return result;
        }
        set
        {
            ViewState["ConsignmentData"] = value;
        }
    }
    [Serializable]
    private class stock_level_object
    {
        public string PartNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Description { get; set; }
        public string DateCode { get; set; }
        public long AvailableQty { get; set; }
        public long ShippedQty { get; set; }
        public long TotalQty { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            string strNoAccess = "";
            if (!SM_Security.is_consignment_authorized(out strNoAccess))
                throw new Exception(strNoAccess);
            
            SetConsignmentCompany();
            if (!Page.IsPostBack)
                LoadData();
            LoadGrid();
        }
        catch (Exception ex)
        {

            //tools.HandleResult("fail", ex.Message);
            tools.HandleResult("fail", ex.Message, 0, true, "/secure_members/Default.aspx");

        }




    }
    private void SetConsignmentCompany()
    {
        if (SM_Global.PreviewUser != null)
            ConsignmentCompany = SM_Global.PreviewCompany;
        else if (SM_Global.isDemoUser)
            ConsignmentCompany = SM_Global.DemoConsignmentCompany;
        else
            ConsignmentCompany = SM_Global.CurrentCompany;

    }
    private void LoadGrid()
    {

        smdt.dataSource = ConsignmentData;
        smdt.pageLength = 50;
        smdt.loadGrid();

    }
    private void LoadData()
    {

        //Stopwatch to track time
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();
        //TimeSpan to check time spanned from stopwatch
        TimeSpan ts = stopWatch.Elapsed;

        Console.WriteLine("Query started ...");

        ConsignmentData = new List<stock_level_object>();
        List<string> partrecordImportIdList = new List<string>();
        List<string> shippedImportIdList = new List<string>();
        List<string> combinedImportIdList = new List<string>();
        using (RzDataContext rdc = new RzDataContext())
        {
            partrecordImportIdList = rdc.partrecords.Where(w => w.stocktype == "consign" && w.base_company_uid == ConsignmentCompany.unique_id && w.importid.Length > 0 && w.importid != null).Select(s => s.importid).Distinct().ToList();
            Console.WriteLine("Retreiving partrecord ImportIdList completed after " + stopWatch.Elapsed.Seconds + " seconds");
            shippedImportIdList = rdc.shipped_stocks.Where(w => w.stocktype == "consign" && w.base_company_uid == ConsignmentCompany.unique_id && w.importid.Length > 0 && w.importid != null).Select(s => s.importid).Distinct().ToList();
            Console.WriteLine("Retreiving shipped ImportIdList completed after " + stopWatch.Elapsed.Seconds + " seconds");


            combinedImportIdList = partrecordImportIdList;
            foreach (string s in shippedImportIdList)
            {
                if (!combinedImportIdList.Contains(s))
                    combinedImportIdList.Add(s);
            }


            Console.WriteLine("Combined ImportIdList completed after " + stopWatch.Elapsed.Seconds + " seconds ...");
            foreach (string i in combinedImportIdList)
            {

                Console.WriteLine("ImportID: " + i);
                List<shipped_stock> shippedList = rdc.shipped_stocks.Where(w => w.stocktype == "consign" && w.importid == i).ToList();
                List<partrecord> stockList = rdc.partrecords.Where(w => w.stocktype == "consign" && w.importid == i).ToList();
                List<stock_level_object> sloShipped = BuildShippedStockObject(shippedList);
                List<stock_level_object> sloPartrecords = BuildPartrecordObject(stockList);
                List<stock_level_object> sloUnion = sloShipped.Union(sloPartrecords).ToList();

                var query = sloUnion.GroupBy(g => g.PartNumber).Select(s => new stock_level_object
                {

                    PartNumber = s.Key,
                    Manufacturer = s.Max(m => m.Manufacturer ?? ""),
                    DateCode = s.Max(m => m.DateCode ?? ""),
                    Description = s.Max(m => m.Description ?? ""),
                    AvailableQty = (long)s.Sum(ss => ss.AvailableQty),
                    ShippedQty = (long)s.Sum(ss => ss.ShippedQty),
                    TotalQty = s.Sum(ss => ss.AvailableQty) + s.Sum(ss => ss.ShippedQty)

                });


                if (query.Any())
                    ConsignmentData.AddRange(query.ToList());


                //var query = (
                //     from parts in stockList
                //     join shipped in shippedList on parts.fullpartnumber equals shipped.fullpartnumber into gj
                //     from sub in gj.DefaultIfEmpty()
                //     select new stock_level_object
                //     {
                //         PartNumber = parts.fullpartnumber,
                //         Manufacturer = parts.manufacturer ?? "",
                //         DateCode = parts.datecode ?? "",
                //         Description = parts.description ?? "",
                //         AvailableQty = (long)parts.quantity - (long)parts.quantityallocated,
                //         ShippedQty = (long)gj.Sum(s => s.quantity ?? 0),
                //         TotalQty = ((long)parts.quantity - (long)parts.quantityallocated) + ((long)gj.Sum(s => s.quantity ?? 0))
                //     });

                //var query = (
                //     from parts in stockList
                //     join shipped in shippedList on parts.fullpartnumber equals shipped.fullpartnumber into gj
                //     from sub in gj.DefaultIfEmpty()
                //     select new stock_level_object
                //     {
                //         PartNumber = parts.fullpartnumber,
                //         Manufacturer = parts.manufacturer ?? "",
                //         DateCode = parts.datecode ?? "",
                //         Description = parts.description ?? "",
                //         AvailableQty = (long)parts.quantity - (long)parts.quantityallocated,
                //         ShippedQty = (long)gj.Sum(s => s.quantity ?? 0),
                //         TotalQty = ((long)parts.quantity - (long)parts.quantityallocated) + ((long)gj.Sum(s => s.quantity ?? 0))
                //     });

                //if (query.Any())
                //    ConsignmentData.AddRange(query.ToList());

                Console.WriteLine("Count of Lines: " + ConsignmentData.Count());

            }

        }

        Console.WriteLine(Environment.NewLine);
        Console.WriteLine("Processing finished in " + stopWatch.Elapsed.Seconds + " seconds");

    }


    private List<stock_level_object> BuildShippedStockObject(List<shipped_stock> sourceList)
    {
        List<stock_level_object> ret = new List<stock_level_object>();
        foreach (shipped_stock v in sourceList)
        {
            stock_level_object slo = new stock_level_object();
            slo.PartNumber = v.fullpartnumber;
            slo.Manufacturer = v.manufacturer ?? "";
            slo.DateCode = v.datecode ?? "";
            slo.Description = v.description ?? "";
            // slo.AvailableQty = (long)v.quantity - (long)v.quantityallocated;
            slo.ShippedQty = (long)v.quantity;
            //slo.TotalQty = ((long)v.quantity - (long)v.quantityallocated) + ((long)gj.Sum(s => s.quantity ?? 0));

            ret.Add(slo);
        }
        return ret;
    }
    private List<stock_level_object> BuildPartrecordObject(List<partrecord> sourceList)
    {
        List<stock_level_object> ret = new List<stock_level_object>();
        foreach (partrecord v in sourceList)
        {
            stock_level_object slo = new stock_level_object();
            slo.PartNumber = v.fullpartnumber;
            slo.Manufacturer = v.manufacturer ?? "";
            slo.DateCode = v.datecode ?? "";
            slo.Description = v.description ?? "";
            slo.AvailableQty = (long)v.quantity - (long)v.quantityallocated;
            //slo.ShippedQty = (long)v.quantity;
            //slo.TotalQty = ((long)v.quantity - (long)v.quantityallocated) + ((long)gj.Sum(s => s.quantity ?? 0));

            ret.Add(slo);
        }
        return ret;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            tools.HandleResult("success", "Processing, your export will be available momentarily", 7);
            string filename = Tools.Dates.get_datetime_for_file_export("consignment_status", "today");
            tools.ExportListToCsv(ConsignmentData, filename);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }



}