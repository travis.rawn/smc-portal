﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_members_reported_part_search : System.Web.UI.Page
{
    RzDataContext RZDC = new RzDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        GetLastUpdate();

    }

    protected void GetLastUpdate()
    {
        lblLastUpdate.Text = "Last updated: ";
        //DateTime fivepm = new DateTime(2016,04,19,17,00,00);
        //DateTime now = fivepm;


        DateTime now = DateTime.Now;        
        DateTime today = now.Date;
        if (now.Hour >= 17)
            lblLastUpdate.Text += today.ToString("M/d/yyyy");
        else
            lblLastUpdate.Text += today.AddDays(-1).ToString("M/d/yyyy");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchRz();
    }

    protected void SearchRz()
    {
        string trimmedpart = null;
        if (txtSearch.Text.Length < 4)
            trimmedpart = txtSearch.Text.Trim();
        else
            trimmedpart = txtSearch.Text.Trim().Substring(0, 4);

        var query = from c in RZDC.erai_imports
                    where c.fullpartnumber.Contains(trimmedpart)
                    select new
                    {
                        PartNumber = c.fullpartnumber,
                        MFG = c.manufacturer,
                        Datecode = c.datecode,
                        Description = c.description
                    };



        if (query.Any())
        {
            GridView1.DataSource = query;
            GridView1.DataBind();
            GridView1.Visible = true;
            lblResults.Text = query.Count() + " results.";
        }
        else
        {
            lblResults.Text = "No results.";
            GridView1.Visible = false;
        }


    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        SearchRz();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }

    protected void SearchCSV()
    {
        string[] allLines = File.ReadAllLines(@"Y:\STOCK\SensibleExcessUpload.csv");



        var query = from line in allLines
                    where line.Contains(txtSearch.Text)
                    let data = line.Split(',')

                    select new

                    {

                        MPN = data[0].ToString(),

                        QTY = data[2].ToString()

                    };
        if (query.Any())
        {
            GridView1.DataSource = query;
            GridView1.DataBind();
            lblResults.Text = query.Count() + " total results.";
        }
        else
            lblResults.Text = "No results.";

    }
}