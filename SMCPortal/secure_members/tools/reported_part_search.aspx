﻿<%@ Page Title="Reported Part Search" Language="C#"  MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="reported_part_search.aspx.cs" Inherits="secure_members_reported_part_search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>REPORTED PART SEARCH</h1>
    </div>
    <div class="alert alert-info">
        <label>Search our database of suspect counterfeit parts reported in the last few years.</label>
        <em>
            <asp:Label ID="lblLastUpdate" runat="server"></asp:Label></em>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="btn btn-primary btn-sm" />
                </div>
            </div>
            <asp:Label ID="lblResults" runat="server"></asp:Label>
        </div>

        <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
            <asp:GridView ID="GridView1" runat="server" CssClass="table table-hover table-striped gvstyling stacktable" GridLines="None" AllowPaging="true" PageSize="10" OnPageIndexChanging="GridView1_PageIndexChanging">
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </asp:Panel>

</asp:Content>

