﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="conflict_mineral_search.aspx.cs" Inherits="secure_members_tools_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>CONFLICT MINERAL SEARCH</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <div class="row" style="padding-bottom: 10px;">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <asp:TextBox ID="txtSearch" runat="server" ToolTip="Search by my PO" placeholder="Search by my PO" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div style="padding-top: 10px;">
                        <div class="row">
                            <div class="col-sm-7">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="btn-smc btn-smc-primary btn-block" />
                            </div>
                            <div class="col-sm-5">
                                <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CssClass="btn-smc btn-smc-danger btn-block" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <div class="row">
                <asp:GridView ID="gvResults" runat="server" CssClass="table table-hover table-striped gvstyling stacktable" GridLines="None"></asp:GridView>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

