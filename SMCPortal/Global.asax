﻿<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        //string test = "farts";
        //throw new System.Web.HttpRequestValidationException();

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        SM_Tools tools = new SM_Tools();
        System.Exception ex = Server.GetLastError();
        Exception baseException = ex.GetBaseException();

        string ipAddress = SensibleDAL.NetworkLogic.GetVisitorIpAddress();

        if (baseException is System.Threading.ThreadAbortException)
            return;

        if (baseException.Message.Contains("Thread was being aborted"))
            SensibleDAL.SystemLogic.Logs.LogEvent(ex, null, false);
        




        if (baseException is System.Web.HttpException)
        {
            //Handle potenitally dangerous URL Requests
            if (ex.Message.Contains("A potentially dangerous Request.Path value was detected from the client"))
                Response.Redirect("/login.aspx", false);
            if (SensibleDAL.NetworkLogic.CheckForKeywordBasedIpBlock(ex, ipAddress))
                SM_Security.RedirectToBlockPage();

        }

        if (baseException is System.Web.HttpRequestValidationException)
        {
            if (HttpContext.Current != null)
                Response.Redirect("/ErrorPages/404.aspx",false);
        }



    }
    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    /// <summary>
    /// for 301 redirects, etc.
    /// </summary>   
    void Application_BeginRequest(object sender, EventArgs e)
    {


        if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("part_details.aspx"))
            Do301Redirect("part_details.aspx", "part_detail.aspx");

        if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("current_stock.aspx"))
            Do301Redirect("current_stock.aspx", "part_detail.aspx");





    }
    /// <summary>
    /// Redirect Hanlder
    /// </summary>
    /// <param name="oldPage"></param> - the old page
    /// <param name=""></param> - the new page
    void Do301Redirect(string oldPage, string NewPage)
    {

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Status = "301 Moved Permanently";
        HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace(oldPage, NewPage));
    }


</script>
