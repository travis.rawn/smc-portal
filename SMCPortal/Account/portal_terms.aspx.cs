﻿using SensibleDAL.ef;
using System;
using System.Linq;
using System.Web.Security;


public partial class Account_portal_terms : System.Web.UI.Page
{
    MembershipUser CurrentUser;
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        GetCurrentUserFromQueryString();
        if (CurrentUser == null)
        {
            //tools.HandleResult(Page,"fail", "Invalid user account.  Returning to login");
            FormsAuthentication.RedirectToLoginPage();
        }

    }

    private void GetCurrentUserFromQueryString()
    {
        //pk
        if (Request.QueryString.Count == 0)
            return;
        string userID = Tools.Strings.SanitizeInput(Request.QueryString["id"].ToString());
        if (string.IsNullOrEmpty(userID))
            return;
        Guid userKey = new Guid(userID);
        CurrentUser = System.Web.Security.Membership.GetUser(userKey);
    }

    protected void btnSubmitTerms_Click(object sender, EventArgs e)
    {
        try
        {
            //Handle if they didn't check the box
            if (cbxAcceptTerms.Checked != true)
            {
                tools.HandleResultJS("You must accept the terms and conditions before Proceeding", true, Page);
                cbxAcceptTerms.ForeColor = System.Drawing.Color.Red;
                return;
            }



            //Instatiate or create the terms.
            //SMCPortalDataContext PDC = new SMCPortalDataContext();
            Term terms = null;
            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                if (!pdc.Terms.Where(w => w.UserId == (Guid)CurrentUser.ProviderUserKey).Any())//IF no previous terms record already exists for user create new terms   
                {
                    //Create new terms record for user.
                    terms = new Term();
                    pdc.Terms.Add(terms);
                }

                else//get existing terms record for user
                    terms = pdc.Terms.Where(w => w.UserId == (Guid)CurrentUser.ProviderUserKey).FirstOrDefault();

                //Set the terms object properties.
                terms.UserId = (Guid)CurrentUser.ProviderUserKey;
                terms.UserName = CurrentUser.UserName;
                terms.date_accepted = DateTime.Now;
                terms.revision_accepted = "Rev 1";
                if (cbxAcceptTerms.Checked)
                {
                    terms.accepted = true;
                    CurrentUser.Comment = "";
                    System.Web.Security.Membership.UpdateUser(CurrentUser);
                }
                else
                {
                    terms.accepted = false;
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                }
                pdc.SaveChanges();
            }
               

            HandleTermsAccepted();

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    private void HandleTermsAccepted()
    {
        //Terms can be required in the middle of a session (i.e. nee Rev) OR upon initial login  
        if (Request.QueryString.Count > 0)
        {
            string url = Tools.Strings.SanitizeInput( Request.QueryString["ReturnUrl"]);
            if (!string.IsNullOrEmpty(url))
                Response.Redirect(url, false);
        }
        else
            Response.Redirect("~/", false);
    }
}