﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="password_reset.aspx.cs" Inherits="Account_password_reset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .hs-error-msgs {
            color: red;
            font-size: 16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BannerContent" runat="server">

    <h1 class="relative white">Password Reset</h1>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <%--start dual column wrapper--%>
    <div id="hs_cos_wrapper_widget_1550858841443" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
        <div id="section1" class="dual-column__wrapper">
            <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__large">
                <!-- start of  of column1 -->
                <div class="col6 content__container">
                    <h3 class="">Password Reset</h3>
                    <div class="supporting-content__container ">

                        <div class="supporting-content   icon-list__container icon-list__checkmark">
                            <p>Use this form to reset your password when:</p>
                            <ul>
                                <li>You have forgotten your password</li>
                                <li>You want to change your password to something new</li>
                                <li>Security Upgrades require periodic password changes</li>
                                <li>It has been more than 30 days since your last successful login</li>
                            </ul>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <!-- end of column1 -->

                <!-- start of column2 -->
                <div class="col6 content__container">
                    <div class="form__container form__container-boxed">
                        <span id="hs_cos_wrapper_widget_1550858841443_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_form" style="" data-hs-cos-general-type="widget" data-hs-cos-type="form">
                            <h3 id="hs_cos_wrapper_form_39750885_title" class="hs_cos_wrapper form-title" data-hs-cos-general-type="widget_field" data-hs-cos-type="text"></h3>

                            <div id="hs_form_target_form_39750885">
                                <div novalidate="" accept-charset="UTF-8" action="https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/1878634/f0d691c8-324f-498a-af54-d0fc32baddd7" enctype="multipart/form-data" id="hsForm_f0d691c8-324f-498a-af54-d0fc32baddd7_2705" method="POST" class="hs-form stacked hs-custom-form hs-form-private hs-form-f0d691c8-324f-498a-af54-d0fc32baddd7_0ac02eab-f558-4fa5-bcce-7cdf5ae9bd3f" data-form-id="f0d691c8-324f-498a-af54-d0fc32baddd7" data-portal-id="1878634" target="target_iframe_f0d691c8-324f-498a-af54-d0fc32baddd7_2705" data-reactid=".hbspt-forms-1">
                                    <div data-reactid=".hbspt-forms-1.1:$0">
                                        <div class="hs-richtext hs-main-font-element" data-reactid=".hbspt-forms-1.1:$0.0">
                                            <p>&nbsp;</p>
                                            <p>Please enter your username or email below, to initiate a password reset.</p>
                                        </div>
                                        <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0.$email">
                                            <label id="label-email-f0d691c8-324f-498a-af54-d0fc32baddd7_2705" class="" placeholder="Enter your User name / email:" for="email-f0d691c8-324f-498a-af54-d0fc32baddd7_2705" data-reactid=".hbspt-forms-1.1:$0.$email.0">
                                                <span data-reactid=".hbspt-forms-1.1:$0.$email.0.0">User name / email:</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$0.$email.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$0.$email.$email">
                                                <%--<input ID="txtUserName"  runat="server" class="hs-input" type="email" name="email" placeholder="" value="" autocomplete="email" data-reactid=".hbspt-forms-1.1:$0.$email.$email.0">--%>
                                                <asp:TextBox ID="txtUserName" runat="server" CssClass="hs-input" placeholder="" />
                                            </div>
                                        </div>

                                        <%--Error Message--%>
                                        <ul runat="server" id="divPageResult" class="no-list hs-error-msgs inputs-list" style="display: block; list-style-type: none;" role="alert" data-reactid=".hbspt-forms-1.1:$0.$email.3" visible="false">
                                            <li data-reactid=".hbspt-forms-1.1:$0.$email.3.$0">
                                                <%--<label class="hs-error-msg" data-reactid=".hbspt-forms-1.1:$0.$email.3.$0.0">Email must be formatted correctly.</label>--%>
                                                <asp:Label ID="lblPageResult" runat="server" CssClass="hs-error-msg"></asp:Label>
                                            </li>

                                        </ul>
                                    </div>
                                    <noscript data-reactid=".hbspt-forms-1.2"></noscript>
                                    <%--Button--%>
                                    <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                        <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                        <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                            <%--<input type="submit" value="Reset My Password" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0">--%>
                                            <asp:LinkButton ID="btnLogin" runat="server" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0" OnClick="btnSUbmit_Click">Reset My Password</asp:LinkButton>
                                        </div>
                                    </div>
                                    <noscript data-reactid=".hbspt-forms-1.6"></noscript>
                                    <input name="hs_context" type="hidden" value="{&quot;rumScriptExecuteTime&quot;:790.3650000225753,&quot;rumServiceResponseTime&quot;:907.2650000452995,&quot;rumFormRenderTime&quot;:2.954999916255474,&quot;rumTotalRenderTime&quot;:910.8899999409914,&quot;rumTotalRequestTime&quot;:114.43500011228025,&quot;pageUrl&quot;:&quot;https://info.sensiblemicro.com/login-0&quot;,&quot;pageTitle&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;source&quot;:&quot;FormsNext-static-3.320&quot;,&quot;timestamp&quot;:1562942570528,&quot;userAgent&quot;:&quot;Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36&quot;,&quot;referrer&quot;:&quot;https://app.hubspot.com/content-detail/1878634/landing-page/8826293573/performance&quot;,&quot;originalEmbedContext&quot;:{&quot;formsBaseUrl&quot;:&quot;/_hcms/forms/&quot;,&quot;portalId&quot;:&quot;1878634&quot;,&quot;formId&quot;:&quot;f0d691c8-324f-498a-af54-d0fc32baddd7&quot;,&quot;formInstanceId&quot;:&quot;2705&quot;,&quot;pageId&quot;:&quot;8826293573&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;inlineMessage&quot;:true,&quot;rawInlineMessage&quot;:&quot;Your password has been reset.&amp;nbsp; You will receive an email with your new temporary password.&amp;nbsp; You will be required to change this password when you next login.&quot;,&quot;hsFormKey&quot;:&quot;f646cf8d0da908191bb9d3ec07e95e99&quot;,&quot;target&quot;:&quot;#hs_form_target_form_39750885&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;formData&quot;:{&quot;cssClass&quot;:&quot;hs-form stacked hs-custom-form&quot;},&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;},&quot;pageId&quot;:&quot;8826293573&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;formInstanceId&quot;:&quot;2705&quot;,&quot;rawInlineMessage&quot;:&quot;Your password has been reset.&amp;nbsp; You will receive an email with your new temporary password.&amp;nbsp; You will be required to change this password when you next login.&quot;,&quot;hsFormKey&quot;:&quot;f646cf8d0da908191bb9d3ec07e95e99&quot;,&quot;formTarget&quot;:&quot;#hs_form_target_form_39750885&quot;,&quot;correlationId&quot;:&quot;8ee0b51c-53ef-417b-8096-d5dbfd18280d&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;,&quot;isHostedOnHubspot&quot;:true}" data-reactid=".hbspt-forms-1.7"><iframe name="target_iframe_f0d691c8-324f-498a-af54-d0fc32baddd7_2705" style="display: none;" data-reactid=".hbspt-forms-1.8"></iframe>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
                <!-- end of column2 -->
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

