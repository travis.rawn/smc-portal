﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="password_change.aspx.cs" Inherits="Account_password_change" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .hs-form-field label {
            font-weight: bold;
        }

        .instruction {
            font-size: 16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BannerContent" runat="Server">
    <h1 class="relative white">Password Change</h1>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <%--start dual column wrapper--%>
    <div id="hs_cos_wrapper_widget_1550858841443" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
        <div id="section1" class="dual-column__wrapper">
            <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__large">
                <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__small">
                    <div class="col6 content__container">
                        <h3 class="">Change Password</h3>
                        <div class="supporting-content__container ">
                            <div class="supporting-content   icon-list__container icon-list__checkmark">
                                <p>
                                    <asp:Label ID="lblResetDirections" runat="server"></asp:Label>&nbsp;
                                </p>
                                <div class="row" style="text-align: center;">
                                </div>
                                <ul>
                                    <li>Must be at least 3 non-alphanumberic characters (#,*, !, etc)</li>
                                    <li>For account security, do not re-use passwords from other sites.</li>
                                    <li>Consider using a password manager for increased account security.</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <!-- end of column1 -->
                    <!-- start of column2 -->

                    <div class="col6 content__container">
                        <div class="form__container form__container-boxed">
                            <span id="hs_cos_wrapper_widget_1550858841443_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_form" style="" data-hs-cos-general-type="widget" data-hs-cos-type="form">


                                <div role="alert" class="row alert alert-dismissable" runat="server" visible="false" id="divPageResult">
                                    <asp:Label ID="lblPageResult" runat="server"></asp:Label>                                   
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                </div>
                                <div id="hs_form_target_form_287856562">
                                    <div novalidate="" accept-charset="UTF-8" action="https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/1878634/a3a35734-9147-4125-beb1-ceb1c7d69f7e" enctype="multipart/form-data" id="hsForm_a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" method="POST" class="hs-form stacked hs-custom-form hs-form-private hs-form-a3a35734-9147-4125-beb1-ceb1c7d69f7e_f99911c8-7487-4c1d-a0e8-4e40572c6a15" data-form-id="a3a35734-9147-4125-beb1-ceb1c7d69f7e" data-portal-id="1878634" target="target_iframe_a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" data-reactid=".hbspt-forms-1">

                                        <div calss="well" id="divNoEmail" runat="server" visible="false">
                                            <p class="instruction">
                                                <em>**If you did not recieve your temporary password email,&nbsp;<asp:LinkButton ID="lbtnSendTempPW" runat="server" OnClick="lbSendTempPW_Click">click here</asp:LinkButton>
                                                    &nbsp;to have it re-sent.</em>
                                            </p>
                                        </div>
                                        <div class="hs_temporary_password hs-temporary_password hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0">
                                            <label id="label-temporary_password-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" class="" placeholder="Enter your Current or Temporary Password" for="temporary_password-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" data-reactid=".hbspt-forms-1.1:$0.0">
                                                <span data-reactid=".hbspt-forms-1.1:$0.0.0">
                                                    <asp:Label ID="lblCurrentOrTempPasswordLabel" runat="server"></asp:Label></span>
                                            </label>

                                            <div class="hs-field-desc" style="display: block;" data-reactid=".hbspt-forms-1.1:$0.1">
                                                <asp:Label ID="lblCurrentOrTempPasswordIntruction" runat="server" Style="display: block;"></asp:Label>
                                            </div>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$0.$temporary_password">
                                                <asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password" placeholder="Current or Temporary Password"></asp:TextBox>
                                                <%-- <input id="temporary_password-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" class="hs-input" type="text" name="temporary_password" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$0.$temporary_password.0" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">--%>
                                            </div>
                                        </div>
                                        <div class="hs_temporary_password_new hs-temporary_password_new hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$1">
                                            <label id="label-temporary_password_new-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" class="" placeholder="Enter your New Password" for="temporary_password_new-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" data-reactid=".hbspt-forms-1.1:$1.0"><span data-reactid=".hbspt-forms-1.1:$1.0.0">New Password</span></label>
                                            <div class="hs-field-desc" style="display: block;" data-reactid=".hbspt-forms-1.1:$1.1">Please enter a new password</div>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$1.$temporary_password_new">
                                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" placeholder="New Password"></asp:TextBox>
                                                <%--<input id="temporary_password_new-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" class="hs-input" type="text" name="temporary_password_new" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$1.$temporary_password_new.0">--%>
                                            </div>
                                        </div>
                                        <div class="hs_temporary_password_repeat hs-temporary_password_repeat hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$2">
                                            <label id="label-temporary_password_repeat-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" class="" placeholder="Enter your Repeat New Password" for="temporary_password_repeat-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" data-reactid=".hbspt-forms-1.1:$2.0"><span data-reactid=".hbspt-forms-1.1:$2.0.0">Repeat New Password</span></label>
                                            <div class="hs-field-desc" style="display: block;" data-reactid=".hbspt-forms-1.1:$2.1">Please repeat the new password</div>
                                            <div class="input password-label" data-reactid=".hbspt-forms-1.1:$2.$temporary_password_repeat">
                                                <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password" placeholder="Confirm New Password"></asp:TextBox>
                                                <%--<input id="temporary_password_repeat-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" class="hs-input" type="text" name="temporary_password_repeat" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$2.$temporary_password_repeat.0">--%>
                                            </div>
                                        </div>
                                        <noscript data-reactid=".hbspt-forms-1.2"></noscript>
                                        <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                            <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                            <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                                <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangePassword" CssClass="hs-button primary large" OnClick="ChangePasswordPushButton_Click" />
                                                <%--<asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="hs-button danger large" OnClick="CancelPushButton_Click" />--%>

                                                <%--   <input type="submit" value="Change Temporary Password" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0">--%>
                                            </div>
                                        </div>
                                        <noscript data-reactid=".hbspt-forms-1.6"></noscript>
                                        <input name="hs_context" type="hidden" value="{&quot;rumScriptExecuteTime&quot;:809.6900000236928,&quot;rumServiceResponseTime&quot;:947.6949998643249,&quot;rumFormRenderTime&quot;:2.0099999383091927,&quot;rumTotalRenderTime&quot;:950.2250000368804,&quot;rumTotalRequestTime&quot;:136.0299999359995,&quot;pageUrl&quot;:&quot;https://info.sensiblemicro.com/login-1&quot;,&quot;pageTitle&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;source&quot;:&quot;FormsNext-static-3.320&quot;,&quot;timestamp&quot;:1562943880534,&quot;userAgent&quot;:&quot;Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36&quot;,&quot;referrer&quot;:&quot;https://app.hubspot.com/content-detail/1878634/landing-page/8843825770/performance&quot;,&quot;originalEmbedContext&quot;:{&quot;formsBaseUrl&quot;:&quot;/_hcms/forms/&quot;,&quot;portalId&quot;:&quot;1878634&quot;,&quot;formId&quot;:&quot;a3a35734-9147-4125-beb1-ceb1c7d69f7e&quot;,&quot;formInstanceId&quot;:&quot;6236&quot;,&quot;pageId&quot;:&quot;8843825770&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;inlineMessage&quot;:true,&quot;rawInlineMessage&quot;:&quot;Thanks for submitting the form.&quot;,&quot;hsFormKey&quot;:&quot;881266ba25171b5d2ad211a779745d74&quot;,&quot;target&quot;:&quot;#hs_form_target_form_287856562&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;formData&quot;:{&quot;cssClass&quot;:&quot;hs-form stacked hs-custom-form&quot;},&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;},&quot;pageId&quot;:&quot;8843825770&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;formInstanceId&quot;:&quot;6236&quot;,&quot;rawInlineMessage&quot;:&quot;Thanks for submitting the form.&quot;,&quot;hsFormKey&quot;:&quot;881266ba25171b5d2ad211a779745d74&quot;,&quot;formTarget&quot;:&quot;#hs_form_target_form_287856562&quot;,&quot;correlationId&quot;:&quot;91b290c3-0efb-4014-8445-f15497a0df2f&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;,&quot;isHostedOnHubspot&quot;:true}" data-reactid=".hbspt-forms-1.7"><iframe name="target_iframe_a3a35734-9147-4125-beb1-ceb1c7d69f7e_6236" style="display: none;" data-reactid=".hbspt-forms-1.8"></iframe>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <!-- end of column2 -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

