﻿using HubspotApis;
using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_register : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    MembershipUser CreatedUser;
    RzDataContext rdc = new RzDataContext();
    company company = null;
    companycontact contact = null;
    bool validUser = false;
    bool showPartSearchRegisterAlert = false;

    string strFirstName;
    string strLastName;
    string strEmail;


    protected void Page_Load(object sender, EventArgs e)
    {
        ((hs_flex)this.Master).ToggleTitleDiv(false);
        //pnlRegistrationComplete.Visible = false;        
        //showPartSearchRegisterAlert = 
        string strShowPartSearcRegisterAlert = Tools.Strings.SanitizeInput(Request.QueryString["r"]);
        if (!string.IsNullOrEmpty(strShowPartSearcRegisterAlert))
        {
            if (strShowPartSearcRegisterAlert == "partsearch_unregistered")
                showPartSearchRegisterAlert = true;
        }

        if (showPartSearchRegisterAlert)
        {
            HandleError("Unregistered users have a daily limit on searches.  Please register for your FREE account or Login to remove this limit.");
        }
    }


    protected void HandleError(string error)
    {
        CustomValidator err = new CustomValidator();
        err.ValidationGroup = "CreateUserWizard1";
        err.IsValid = false;
        err.ErrorMessage = GenerateFriendlyRegistrationText(error);
        Page.Validators.Add(err);
        //Ensure captcha reloads on asyncppostback
        ScriptManager.RegisterStartupScript(this, GetType(), "CaptchaReload", "$.getScript(\"https://www.google.com/recaptcha/api.js\", function () {});", true);
    }

    private string GenerateFriendlyRegistrationText(string error)
    {

        string ret = error;
        //DuplicateEmail  7
        //The email address already exists in the database for the application.

        //DuplicateProviderUserKey    10
        //The provider user key already exists in the database for the application.

        //DuplicateUserName   6
        //The user name already exists in the database for the application.

        //InvalidAnswer   4
        //The password answer is not formatted correctly.

        //InvalidEmail    5
        //The email address is not formatted correctly.

        //InvalidPassword 2
        //The password is not formatted correctly.

        //InvalidProviderUserKey  9
        //The provider user key is of an invalid type or format.

        //InvalidQuestion 3
        //The password question is not formatted correctly.

        //InvalidUserName 1
        //The user name was not found in the database.

        //ProviderError   11
        //The provider returned an error that is not described by other MembershipCreateStatus enumeration values.

        //Success 0
        //The user was successfully created.

        //UserRejected    8
        //The user was not created, for a reason defined by the provider.
        switch (error)
        {
            case "DuplicateEmail":
            case "DuplicateProviderUserKey":
            case "DuplicateUserName":
            case "InvalidUserName":
            case "InvalidEmail":
            case "InvalidPassword":
            case "InvalidAnswer":
            case "InvalidProviderUserKey":
            case "InvalidQuestion":
            case "ProviderError":
            case "UserRejected":
                {
                    ret = "Invalid login. You may already have an account, otherwise please contact your sales representative.";
                    break;
                }



        }


        return ret;
    }

    protected void CreateUserButtonRender(object sender, EventArgs e)
    {
        Page.Form.DefaultButton = (sender as Button).UniqueID;
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (isCreateUserValid())
            HandleCreateUserTasks();

    }

    private bool isCreateUserValid()
    {
        //Confirm Captcha
        if (!tools.ValidateCaptcha(false))
        {
            HandleError("Please click the Captcha below.");
            return false;
        }

        if (!SetAndSanitizeVariables())
            throw new Exception("Invalid Input.");

        //Validate Email Address   
        if (!tools.ValidateEmailAddress(strEmail))
            return false;

        //Reject common public domains:
        if (tools.IsPublicEmailDomain(strEmail))
        {
            HandleError("Please use your corporate email address to register.");
            return false;
        }


        //confirm valid / allowed email addres from Rz
        validUser = SM_Security.IsValidPortalEmail(strEmail);
        if (!validUser)
        {
            //HandleError("Account creation not possible.  Please contact your sales representative.");
            HandleError("Thank you for your interested in the Sensible Micro Portal.   We are currently approving your account for access, which can take up to 48 hours.  If approved, login information will be sent to the email provided.");

            string body = "First Name: " + strFirstName + "<br />";
            body += "Last Name: " + strLastName + "<br />";
            body += "Email: " + strEmail;
            tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Invalid registration attempt " + strEmail, body);
            return false;
        }

        return true;
    }

    private bool SetAndSanitizeVariables()
    {
        strFirstName = Tools.Strings.SanitizeInput(txtFirstName.Text.Trim());
        if (string.IsNullOrEmpty(strFirstName))
            return false;
        strLastName = Tools.Strings.SanitizeInput(txtLastName.Text.Trim());
        if (string.IsNullOrEmpty(strLastName))
            return false;
        strEmail = Tools.Strings.SanitizeInput(txtEmail.Text.Trim());
        if (string.IsNullOrEmpty(strEmail))
            return false;

        return true;
    }

    private void HandleCreateUserTasks()
    {
        try
        {
            //Actually Create the User
            //User Provided Variables


            //Create the User
            MembershipCreateStatus createStatus;
            bool registerSuccess = SM_Security.RegisterNewExternalUser(strFirstName, strLastName, strEmail, out createStatus);
            if (!registerSuccess)
            {
                HandleError(createStatus.ToString());
                return;
            }

            //Get The user so we can confirm settings
            CreatedUser = Membership.GetUser(strEmail);
            if (CreatedUser == null)
            {
                HandleError("User not found for. " + CreatedUser);
                return;
            }

            //if username is an email address
            if (Tools.Email.IsEmailAddress(strEmail))
            {
                //Set Hubspot Reg Properties.
                SetHubspotPortalRegistrationData(strEmail, CreatedUser, strFirstName, strLastName);

            }

            //Delete user if this was an admin test.
            //Membership.DeleteUser(RegisterationUserName);


            //Hide the Registration Controls


            //Show the completion panel
            tools.HandleResult("success", "Registration Succcessful.  Please check your email to gain secure access.", 10000);

            //Hide Update Prograss spinner
            ScriptManager.RegisterStartupScript(this, GetType(), "hideUpdateProgress", "HideUpdateProgress();", true);

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.ToString(), true, Page);
            tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Portal Registration Error", ex.Message + "<br />" + ex.InnerException);
        }
    }



    private void SetHubspotPortalRegistrationData(string registerationUserName, MembershipUser createdUser, string strFirstName, string strLastName)
    {
        //If User Exists in Hubspot, Update it.
        Dictionary<string, string> contactProps = new Dictionary<string, string>();
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Registration: Checking to see if contact " + registerationUserName + " exists in Hubspot.");
        long unixPortalCreationDate = HubspotApi.ConvertDateTimeToUnixTimestampMillis(CreatedUser.CreationDate.Date);
        long unixLastActivityDate = HubspotApi.ConvertDateTimeToUnixTimestampMillis(CreatedUser.LastActivityDate.Date);
        HubspotApi.Contact c = HubspotApi.Contacts.GetContactByEmail(registerationUserName);
        if (c == null)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Registration: No Hubspot contact found for: " + registerationUserName + ".  Attempting to create.");



            //firstname, lastname, portal_registration, portal_last_activity, email address, Source (Portal Created)

            contactProps.Add("firstname", strFirstName);
            contactProps.Add("lastname", strLastName);
            contactProps.Add("portal_registration_date", unixPortalCreationDate.ToString());
            contactProps.Add("last_portal_login", unixLastActivityDate.ToString());
            contactProps.Add("email", registerationUserName);
            c = HubspotApi.Contacts.CreateHubspotContact(contactProps, HubspotApi.Contacts.ContactSource.PortalRegister);
            string message = "<b>Details</b><br />";
            message += "Name: " + strFirstName + " " + strLastName + " <br />";
            message += "Email: " + registerationUserName + " <br />";
            message += "Portal Registration Date " + CreatedUser.CreationDate.Date.ToShortDateString() + " <br />";

            SystemLogic.Email.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "HubSpot contact craeted via Portal Registration: " + registerationUserName, message);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Registration: Successfully created Hubspot contact for: " + registerationUserName + ".  VID: " + c.vid);
        }
        else //Update HS Contact
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Registration: Hubspot contact found for user : " + registerationUserName + ".  Attempting to update.");
            contactProps.Add("portal_registration_date", unixPortalCreationDate.ToString());
            contactProps.Add("last_portal_login", unixLastActivityDate.ToString());
            c.UpdateContact(contactProps, c.vid);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Registration: Successfully updated portal information for Hubspot Contact : " + registerationUserName + ".");
        }
    }
}