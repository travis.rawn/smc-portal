﻿using SensibleDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_password_reset : System.Web.UI.Page
{
    MembershipUser CurrentUser;
    SM_Tools tools = new SM_Tools();
    string UserNameFromTextBox;
    string UserIDFromQueryString;
    string SessionVariable;





    protected void Page_Load(object sender, EventArgs e)
    {
        ((hs_flex)this.Master).ToggleTitleDiv(false);
        try
        {
            //Get the CurrentUser, sheck Membership, Session, and TextBox
            GetCurrentUser();
            //If current USer is still null, then we didn't find in Session, Memberhsip, nor the Textbox.  Notify
            if (CurrentUser == null)
                return;

        }
        catch (Exception ex)
        {
            HandleResult(ex.Message, true);
        }
    }



    private void DoPasswordReset()
    {
        if (CurrentUser == null)
        {
            tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "No User Found for password reset attempt.", "'<b>User Name: </b>" + txtUserName.Text);
            HandleResult("There was a problem resetting the passowrd for '<b>" + txtUserName.Text + "</b>'.  Please register for the portal or contact your account rep to get set up.", true);
            return;
        }

        // WE use ID querystring to identify the user against the password on the next page.       
        if (SM_Security.ResetPassword(CurrentUser))
        {
            HandleResult("Your password has been reset to a a temporary password and has been emailed to you.  Please follow the instructions in that email to proceed.", false);
            return;
        }
        else
        {
            HandleResult("The user account '"+Tools.Strings.SanitizeInput(txtUserName.Text)+"' does not exist, or is currently not authorized for access.  Please contact Sensible Micro if you believe this to be in error.", true);
            return;
        }
            

    }

    private void GetCurrentUser()
    {
        //Check Current User from Membership
        //CurrentUser = SM_Security.LoadCurrentUser();
        if (CurrentUser == null)
            GetCurrentUserFromTextbox();

    }

    private void GetCurrentUserFromTextbox()
    {
        //IF THE CURRENT USER IS NULL, THIS THIS MUST BE A "FORGOT PASSWORD RESET"    
        UserNameFromTextBox = txtUserName.Text.Trim();
        if (string.IsNullOrEmpty(UserNameFromTextBox))
            return;
        CurrentUser = Membership.GetUser(UserNameFromTextBox);
    }

    protected void HandleResult(string message, bool isFail)
    {
        divPageResult.Visible = true;
        lblPageResult.Text = message;
        if (isFail)
            divPageResult.Attributes["class"] = "alert alert-danger";
        else
            divPageResult.Attributes["class"] = "alert alert-success";
        return;
    }


    protected void PasswordRecovery1_UserLookupError(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtUserName.Text))
            HandleResult("Please enter your user name.", true);
        else
            HandleResult("Invalid user name.", true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/login.aspx", false);
    }

    protected void btnSUbmit_Click(object sender, EventArgs e)
    {
        try
        {   //Do the Password Reset
            GetCurrentUser();
            if (CurrentUser == null)
                return;          
            DoPasswordReset();
        }
        catch (Exception ex)
        {
            HandleResult(ex.Message, true);
        }
    }
}