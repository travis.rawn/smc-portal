﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="reCaptcha.aspx.cs" Inherits="Account_reCaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .g-recaptcha {
            margin: 0 auto;
            display: table;
        }


        .captcha-label {
            color: #EE2738;
            margin-bottom:20px;
        }

        .captcha-content {
            max-width: 600px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div class="section-container captcha-content">

        <div class="alert alert-dismissible" role="alert" id="slideAlert" style="display: none;">
            <button type="button" class="close" aria-label="Close" onclick="toggleAlert();"><span aria-hidden="true">&times;</span></button>
            <label id="lblResult" style="font-weight: bold; padding-right: 15px;"></label>
            <label id="lblMessage"></label>
            <br />
        </div>

        <div class="card mx-auto">
            <div class="card-header">
                <div class="hs-error-msgs" style="display: block;" data-reactid=".hbspt-forms-1.4.0">
                    <label class="hs-main-font-element captcha-label" data-reactid=".hbspt-forms-1.4.0.0.0" id="lblError">Whoah there, we just need make sure you're not a robot real quick.  Please click "I'm not a robot" below then click "Submit".</label>

                </div>

            </div>
            <div class="card-body">
                <div class="card-title">
                </div>
                <div class="g-recaptcha" data-sitekey="6Lc1qSUTAAAAAPrvNw45yv-TJN0N8A4CBIQx8k4Y"></div>
                <br />
                <input type="submit" value="Submit" class="hs-button primary btn-block" onclick="ShowUpdateProgress();" />
            </div>
        </div>
      
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    </div>
</asp:Content>

