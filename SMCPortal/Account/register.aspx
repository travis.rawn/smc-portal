﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="Account_register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src='https://www.google.com/recaptcha/api.js'></script>
  <%--  <script type="text/javascript">
        //Ensure captcha reloads on asyncppostback
        function CaptchaReload() {
            Recaptcha.create("6Lc1qSUTAAAAAPrvNw45yv", '#g-recaptcha', {
                theme: 'white',
                callback: grecaptcha.reset()

            });
        }
    </script>--%>
    <script src="https://www.google.com/recaptcha/api.js?render=site-key" nonce="{NONCE}"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BannerContent" runat="Server">
    <h1 class="relative white">Portal Registration</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <%--start dual column wrapper--%>
    <div id="hs_cos_wrapper_widget_1550858841443" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">

        <div id="section1" class="dual-column__wrapper">
            <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__large">
                <!-- start of  of column1 -->
                <div class="col6 content__container">
                    <h3 class="">How to Register</h3>
                    <div class="supporting-content__container supporting-content-bar__container">

                        <div class="supporting-content supporting-content-bar  icon-list__container icon-list__checkmark">
                            <p>To create your account, our team will add your information to the Customer Portal.</p>
                            <p>Simply fill out the form on this page to have your account created. Once your account is setup , you will be able to Log In and will have complete&nbsp;access to the Customer Portal including:</p>                           
                            <ul>
                                <li>Your orders</li>
                                <li>Tracking numbers and status updates</li>
                                <li>Quality reports</li>
                                <li>and more!</li>
                            </ul>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <!-- end of column1 -->


                <!-- start of column2 -->
                <div class="col6 content__container">
                    <div class="form__container form__container-boxed">
                        <span id="hs_cos_wrapper_widget_1550858841443_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_form" style="" data-hs-cos-general-type="widget" data-hs-cos-type="form">
                            <h3 id="hs_cos_wrapper_form_309512953_title" class="hs_cos_wrapper form-title" data-hs-cos-general-type="widget_field" data-hs-cos-type="text"></h3>

                            <div id="hs_form_target_form_309512953">
                                <div novalidate="" accept-charset="UTF-8" action="https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/1878634/5ac13e0b-dac9-4770-9469-fcf6ac762208" enctype="multipart/form-data" id="hsForm_5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" method="POST" class="hs-form stacked hs-custom-form hs-form-private hs-form-5ac13e0b-dac9-4770-9469-fcf6ac762208_5da50d61-6eab-49b3-9f08-d7ae5a944a96" data-form-id="5ac13e0b-dac9-4770-9469-fcf6ac762208" data-portal-id="1878634" target="target_iframe_5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" data-reactid=".hbspt-forms-1">
                                    <div data-reactid=".hbspt-forms-1.1:$0">
                                        <%--      <div class="hs-richtext hs-main-font-element" data-reactid=".hbspt-forms-1.1:$0.0">
                                            <h4>Register Now</h4>
                                        </div>--%>
                                        <div>
                                           <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="CreateUserWizard1" runat="server" DisplayMode="List" CssClass="login-error" />
                                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>

                                        </div>
                                        <div class="hs_firstname hs-firstname hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0.$firstname">
                                            <label id="label-firstname-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" class="" placeholder="Enter your First name" for="firstname-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" data-reactid=".hbspt-forms-1.1:$0.$firstname.0"><span data-reactid=".hbspt-forms-1.1:$0.$firstname.0.0">First name</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$0.$firstname.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$0.$firstname.$firstname">
                                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="hs-input"></asp:TextBox>
                                                <%--<input id="firstname-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" class="hs-input" type="text" name="firstname" value="" placeholder="" autocomplete="given-name" data-reactid=".hbspt-forms-1.1:$0.$firstname.$firstname.0" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;"></div>--%>
                                            </div>
                                        </div>
                                        <div class="hs_lastname hs-lastname hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$1">
                                            <label id="label-lastname-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" class="" placeholder="Enter your Last name" for="lastname-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" data-reactid=".hbspt-forms-1.1:$1.0"><span data-reactid=".hbspt-forms-1.1:$1.0.0">Last name</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$1.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$1.$lastname">
                                                <%--<input id="lastname-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" class="hs-input" type="text" name="lastname" value="" placeholder="" autocomplete="family-name" data-reactid=".hbspt-forms-1.1:$1.$lastname.0">--%>
                                            </div>
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="hs-input"></asp:TextBox>
                                        </div>
                                        <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$2">
                                            <label id="label-email-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" class="" placeholder="Enter your Email" for="email-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" data-reactid=".hbspt-forms-1.1:$2.0"><span data-reactid=".hbspt-forms-1.1:$2.0.0">Email</span><span class="hs-form-required" data-reactid=".hbspt-forms-1.1:$2.0.1">*</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$2.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$2.$email">
                                                <%--<input id="email-5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" class="hs-input" type="email" name="email" required="" placeholder="" value="" autocomplete="email" data-reactid=".hbspt-forms-1.1:$2.$email.0">--%>
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="hs-input"></asp:TextBox>
                                            </div>
                                        </div>
                                        <noscript data-reactid=".hbspt-forms-1.2"></noscript>
                                        <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                            <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                            <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                                <div class="g-recaptcha" data-sitekey="6Lc1qSUTAAAAAPrvNw45yv-TJN0N8A4CBIQx8k4Y"></div>
                                                <%--<input type="submit" value="Register" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0">--%>
                                                <asp:Button ID="btnCreate" runat="server" CommandName="MoveNext" OnPreRender="CreateUserButtonRender" CssClass="hs-button primary large" Text="Register" ValidationGroup="CreateUserWizard1" OnClientClick="doUpdateProgress();" OnClick="btnCreate_Click" />
                                            </div>
                                        </div>
                                        <noscript data-reactid=".hbspt-forms-1.6"></noscript>
                                        <%--<input name="hs_context" type="hidden" value="{&quot;rumScriptExecuteTime&quot;:1466.3200001232326,&quot;rumServiceResponseTime&quot;:1591.4800001773983,&quot;rumFormRenderTime&quot;:1.990000018849969,&quot;rumTotalRenderTime&quot;:1594.2150000482798,&quot;rumTotalRequestTime&quot;:122.11000011302531,&quot;pageUrl&quot;:&quot;https://info.sensiblemicro.com/registration?hs_preview=jDHKwTBD-7823168090&quot;,&quot;pageTitle&quot;:&quot;Register for the Customer Portal&quot;,&quot;source&quot;:&quot;FormsNext-static-3.320&quot;,&quot;timestamp&quot;:1562964556246,&quot;userAgent&quot;:&quot;Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36&quot;,&quot;originalEmbedContext&quot;:{&quot;formsBaseUrl&quot;:&quot;/_hcms/forms/&quot;,&quot;portalId&quot;:&quot;1878634&quot;,&quot;formId&quot;:&quot;5ac13e0b-dac9-4770-9469-fcf6ac762208&quot;,&quot;formInstanceId&quot;:&quot;8098&quot;,&quot;pageId&quot;:&quot;7823168090&quot;,&quot;pageName&quot;:&quot;Register for the Customer Portal&quot;,&quot;inlineMessage&quot;:true,&quot;rawInlineMessage&quot;:&quot;<span><span>Thank you. A&amp;nbsp;</span>representative will be in contact within 24 hours to activate your account.</span>&quot;,&quot;hsFormKey&quot;:&quot;d6074321e5558d6e431e6d3200dc3d75&quot;,&quot;deactivateSmartForm&quot;:true,&quot;target&quot;:&quot;#hs_form_target_form_309512953&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;formData&quot;:{&quot;cssClass&quot;:&quot;hs-form stacked hs-custom-form&quot;},&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;},&quot;pageId&quot;:&quot;7823168090&quot;,&quot;pageName&quot;:&quot;Register for the Customer Portal&quot;,&quot;formInstanceId&quot;:&quot;8098&quot;,&quot;urlParams&quot;:{&quot;hs_preview&quot;:&quot;jDHKwTBD-7823168090&quot;},&quot;rawInlineMessage&quot;:&quot;<span><span>Thank you. A&amp;nbsp;</span>representative will be in contact within 24 hours to activate your account.</span>&quot;,&quot;hsFormKey&quot;:&quot;d6074321e5558d6e431e6d3200dc3d75&quot;,&quot;formTarget&quot;:&quot;#hs_form_target_form_309512953&quot;,&quot;correlationId&quot;:&quot;94c241fe-dbf3-496c-a735-329b098ba526&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;,&quot;isHostedOnHubspot&quot;:true}" data-reactid=".hbspt-forms-1.7">--%><iframe name="target_iframe_5ac13e0b-dac9-4770-9469-fcf6ac762208_8098" style="display: none;" data-reactid=".hbspt-forms-1.8"></iframe>
                                    </div>
                                </div>
                        </span>
                    </div>
                </div>
                <!-- end of column2 -->
            </div>
        </div>
    </div>
    <%--end dual column wrapper--%>
    <script>
        function doUpdateProgress() {
            ShowUpdateProgress();
            var isValid = Page_ClientValidate("CreateUserWizard1"); //parameter is the validation group - 
            if (!isValid) {
                HideUpdateProgress();
            }
        }


    </script>




</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

