﻿using SensibleDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_password_change : System.Web.UI.Page
{
    //MembershipUser CurrentUser;
    SM_Tools tools = new SM_Tools();
    string userIDFromQueryString;
    PasswordResetType ResetType = PasswordResetType.ChangeCurrentPassword;

    public enum PasswordResetType
    {
        ChangeCurrentPassword,
        ChangeTemporaryPassword
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ((hs_flex)this.Master).ToggleTitleDiv(false);
        divNoEmail.Visible = false;
        //WHY I NEED THE QUERYSTING WTIH ID:  At this point, they eithe rknow their password (Reset) or they don't (forgot).

        //Load the Current USer, form Session, or QueryString ID
        //LoadCurrentUser();

        //Because, on a "Forgot Password" scenario, I need a CurrentUser to check their.  I need to get this username from the 1st login form where the user actually typed that name.  From that name, It's more secure to derive the GUID And pass that.
        LoadResetType();

        //Set Label Directions
        SetResetDirectionLabels();

        //If this is a Change Current Password, there MUST be a detectable used in Sessioin or Membership, else, redirect to login to get user to login
        if (ResetType == PasswordResetType.ChangeCurrentPassword)
            if (SM_Global.CurrentUser == null)
                Response.Redirect("/login.aspx", false);
    }

    //private void LoadCurrentUser()
    //{
    //    CurrentUser = SM_Security.LoadCurrentUser();
    //    //if there is a querystring, override teh CurrentUser with that user string
    //    string strProviderKey;
    //    string userId = Tools.Strings.SanitizeInput(Request.QueryString["id"]);
    //    if (!string.IsNullOrEmpty(userId))
    //    {
    //        strProviderKey = userId;
    //        CurrentUser = Membership.GetUser(new Guid(strProviderKey));
    //    }
    //}




    private void LoadResetType()
    {
        //Default value = Temporyary, whichi also includes instructions how to reset that temp in case they need to do so.        
        bool isTemp = false;
        string strIsTemp = Tools.Strings.SanitizeInput(Request.QueryString["temp"]);
        if (!string.IsNullOrEmpty(strIsTemp))
        {
            if (strIsTemp == "1")
                isTemp = true;
        }
        if (isTemp)
        {
            ResetType = PasswordResetType.ChangeTemporaryPassword;
            return;
        }
        else
        {
            ResetType = PasswordResetType.ChangeCurrentPassword;
            return;
        }
    }

    private void SetResetDirectionLabels()
    {
        switch (ResetType)
        {
            case PasswordResetType.ChangeCurrentPassword:
                {
                    //The USer Doesn't Know the PW
                    lblResetDirections.Text = "Use this form to change your current password.";
                    lblCurrentOrTempPasswordLabel.Text = "Current Password";
                    lblCurrentOrTempPasswordIntruction.Text = "Please enter your current password";
                    break;
                }
            case PasswordResetType.ChangeTemporaryPassword:
                {
                    //The USer Doesn't Know the PW
                    lblResetDirections.Text = "A password reset has been initiated, and you should receive an email with instructions soon.  Please refer to this email to proceed to the Sensible Micro Portal.  Thank you.";
                    lblCurrentOrTempPasswordLabel.Text = "Temporary Password";
                    lblCurrentOrTempPasswordIntruction.Text = "Please enter the Temporary password from your email";
                    divNoEmail.Visible = true;
                    break;
                }
        }
    }



    private void GetCurrentUserFromQueryStringID()
    {
        string userID = Tools.Strings.SanitizeInput(Request.QueryString["id"]);

        if (Request.QueryString.Count > 0 && !string.IsNullOrEmpty(userID))
        {
            Guid userGuid = new Guid(userID);
            SM_Global.CurrentUser = Membership.GetUser(userGuid);
        }

    }

    protected void ChangePassword1_ChangedPassword(object sender, EventArgs e)
    {
        Response.Redirect("../secure_members/Default.aspx", false);
    }

    protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
    {


        try
        {
            GetCurrentUserFromQueryStringID();
            ValidateFields();
            divPageResult.Visible = false;
            // Update the password.
            if (SM_Global.CurrentUser != null)
            {
                if (Membership.ValidateUser(SM_Global.CurrentUser.UserName, txtCurrentPassword.Text.Trim()))
                    if (txtNewPassword.Text.Trim() == txtConfirmNewPassword.Text.Trim())
                        if (SM_Global.CurrentUser.ChangePassword(txtCurrentPassword.Text, txtNewPassword.Text.Trim()))
                        {


                            SM_Security.SetTemporaryPasswordStatus(SM_Global.CurrentUser, false);

                            FormsAuthentication.SetAuthCookie(SM_Global.CurrentUser.UserName, false);
                            tools.JS_Modal_Redirect("Password changed.  Click OK to proceed.", "/secure_members/Default.aspx");

                            return;
                        }
                        else
                        {
                            throw new Exception("Password change failed. Please re-enter your values and try again.");
                        }
                    else
                    {
                        throw new Exception("New password and confirmation password do not match.");
                    }
                else
                {
                    throw new Exception("Current password does not match our records.");
                }
            }
            else
            {
                throw new Exception("User not found.");

            }

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", SM_Tools.HandleLoginExceptions(ex));
        }


    }

    private void ValidateFields()
    {
        string currentPassword = Tools.Strings.SanitizeInput(txtCurrentPassword.Text);
        string newPassword = Tools.Strings.SanitizeInput(txtNewPassword.Text);
        string confirmPassword = Tools.Strings.SanitizeInput(txtConfirmNewPassword.Text);

        if (string.IsNullOrEmpty(currentPassword))
            throw new Exception("Please provide your current password.");
        else if (string.IsNullOrEmpty(newPassword))
            throw new Exception("Please provide your new password.");
        else if (string.IsNullOrEmpty(confirmPassword))
            throw new Exception("Please confirm your new password.");
        

    }

    protected void SendTmpPasswordChangeEmail()
    {
        string userName = SM_Global.CurrentUser.UserName;
        string ip = NetworkLogic.GetVisitorIpAddress();
        tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, userName + " has successfully reset their temporary password.", userName + "'s Current IP is: " + ip);
    }

    protected void lbSendTempPW_Click(object sender, EventArgs e)
    {
        SM_Security.ResetPassword(SM_Global.CurrentUser);
        Response.Redirect("~/Account/PasswordReset.aspx", false);

    }

    protected void CancelPushButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("/login.aspx", false);
    }
}