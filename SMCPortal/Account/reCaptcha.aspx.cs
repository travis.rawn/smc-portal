﻿using SensibleDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_reCaptcha : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
   // SMCPortalDataContext PDC = new SMCPortalDataContext();

    bool captchaSuccess = false;
    string ip_address;
    string returnUrl;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            returnUrl = Tools.Strings.SanitizeInput(Request.QueryString["ReturnUrl"]);
            if (returnUrl == null || returnUrl.Length <= 3)
                returnUrl = "/";

            ip_address = NetworkLogic.GetVisitorIpAddress();

            if (Page.IsPostBack)
            {
                if (tools.ValidateCaptcha())
                {
                    tools.AcceptCaptcha(ip_address, returnUrl);
                }

                else
                    tools.HandleResult("warning", "Please try again.");
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }



    }

}