﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_company_search : System.Web.UI.Page
{
    RzDataContext rzdc = new RzDataContext();
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    List<company> companies = new List<company>();
    

 
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            tools.HandleResult("fail", "Please give me something to search for.");
            return;
        }
        
        companies = rzdc.companies.Where(w => w.companyname.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
        gvresults.DataSource = companies;
        gvresults.DataBind();

    }
}