﻿<%@ Page Title="Customer Lifetime Value Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="customer_lifetime_value_report.aspx.cs" Inherits="secure_sm_Reporting_marketing_customer_lifetime_value_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
         .control{
            padding-left:10%;
            padding-right:10%;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div style="text-align: center; font-size: 40px;">
        Customer Lifetime Value
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="card">
        <div class="row">
            <div class="col-sm-3">
                <label>Start: </label>
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
               
            </div>
            <div class="col-sm-3">
                <label>End: </label>
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
                
            </div>
            <div class="col-sm-3">
                <br />
                <asp:LinkButton runat="server" ID="lbRefresh" OnClientClick="ShowUpdateProgress();" CssClass="control"  OnClick="lbRefresh_Click"><i class="fas fa-search" style="font-size: 25px;"></i></asp:LinkButton>
                <asp:LinkButton runat="server" ID="lbExport" Style="cursor: pointer;" OnClick="lbExport_Click"  CssClass="control" ><i class="fas fa-download" style="font-size: 25px;"></i></asp:LinkButton>

            </div>
            <div class="col-sm-2">
                <br />
                <asp:CheckBox ID="cbxExcludeVendors" runat="server" Checked="true" Text="Exclude Vendors" CssClass="form-control" />
            </div>
        </div>
        <div class="row" style="text-align: center">
            <div class="col-sm-3">
                <h5>
                    <label>Customer Lifetime Value:</label><br />
                    <asp:Label ID="lblTotalCustomerValue" runat="server"></asp:Label></h5>
            </div>
            <div class="col-sm-3">
                <h5>
                    <label>Average Purchase Frequency Rate: </label>
                    <br />
                    <asp:Label ID="lblAPFR" runat="server"></asp:Label></h5>
            </div>
            <div class="col-sm-3">
                <h5>
                    <label>Average Customer Life-span (Years): </label>
                    <br />
                    <asp:Label ID="lblACLS" runat="server"></asp:Label></h5>
            </div>
            <div class="col-sm-3">
                <h5>
                    <label>Average GP Amount: </label>
                    <br />
                    <asp:Label ID="lblAverageSaleAmnt" runat="server"></asp:Label></h5>
            </div>
        </div>


    </div>
    <div class="card">

        <div class="row">
            <div class="col-sm-12">
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

