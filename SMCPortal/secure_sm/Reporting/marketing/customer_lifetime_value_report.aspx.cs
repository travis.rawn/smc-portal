﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_marketing_customer_lifetime_value_report : System.Web.UI.Page
{
    public double averagePurchaseFrequencyRate = 0; //APFR
    public double avgCustomerLifeSpan = 0; //ACLS
    public List<ordhed_sale> salesData = new List<ordhed_sale>();

    SM_Tools tools = new SM_Tools();

    [Serializable]
    public class CustomerValueData
    {
        public string customerID { get; set; }
        public string customerName { get; set; }
        public double totalRevenue { get; set; }
        public int countOfSales { get; set; }
        public double avgPurchaseValue { get; set; } //APV        
        public double customerValue { get; set; }//CV

        public double lifetimeValue { get; set; }//LTV

    }


    // using this const you avoid bugs in mispelling the correct key.
    const string cvdNameConst = "cvdList_cnst";

    public List<CustomerValueData> cvdList
    {
        get
        {
            // check if not exist to make new (normally before the post back)
            // and at the same time check that you did not use the same viewstate for other object
            if (!(ViewState[cvdNameConst] is List<CustomerValueData>))
            {
                // need to fix the memory and added to viewstate
                ViewState[cvdNameConst] = new List<CustomerValueData>();
            }
            return (List<CustomerValueData>)ViewState[cvdNameConst];
        }
        set
        {
            ViewState[cvdNameConst] = value;
        }
    }



    private DateTime startDate = new DateTime(2016, 01, 01);
    private DateTime endDate = DateTime.Now;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            LoadDates();
        //if (!Page.IsPostBack)//Will separately load data on button clicks, and use viewstate to avoid re-loading
        //    LoadAll();





    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = startDate;
            dtpEnd.SelectedDate = endDate;
        }
        startDate = dtpStart.SelectedDate.Value;
        endDate = dtpEnd.SelectedDate.Value;
    }

    private void LoadGrid()
    {
        var GridQuery = cvdList.Select(s => new
        {
            Company = s.customerName,
            TotalSales = Math.Round(s.totalRevenue, 2),
            Count = s.countOfSales,
            AvgGP = Math.Round(s.avgPurchaseValue, 2),
            LifeTimeValue = Math.Round(s.lifetimeValue, 2),



        }).OrderBy(o => o.Company);
        smdt.dataSource = GridQuery;
        smdt.loadGrid();
    }

    private double CalculateAverageCustomerLifeSpan()
    {
        //Calculate average customer lifespan: Calculate this number by 
        //averaging out the number of years a customer continues purchasing from your company.
        double ret = 0;
        //for each customer, get number of years between 1st purchase and last, then divide by the count of distinct customers.
        List<string> companyIDs = salesData.Select(s => s.base_company_uid).Distinct().ToList();
        List<company> distinctCompanies = new List<company>();
        List<double> averageYearsAsCustomer = new List<double>();
        using (RzDataContext rdc = new RzDataContext())
        {
            foreach (string s in companyIDs)
            {
                company c = rdc.companies.FirstOrDefault(f => f.unique_id == s);
                if (c != null)
                    distinctCompanies.Add(c);
            }

            //distinctCompanies = rdc.companies.Where(w => companyIDs.Contains(w.unique_id)).ToList();
        }
        foreach (company c in distinctCompanies)
        {
            var customerQuery = salesData.Where(w => w.base_company_uid == c.unique_id);
            DateTime firstPurchase = customerQuery.Min(m => (DateTime)m.orderdate);
            DateTime lastPurchase = customerQuery.Max(m => (DateTime)m.orderdate);
            TimeSpan ts = lastPurchase.Subtract(firstPurchase);
            double totalYears = ts.TotalDays / 365.25;
            int count = customerQuery.Count();
            double averageYears = totalYears / count;
            averageYearsAsCustomer.Add(averageYears);


        }
        ret = averageYearsAsCustomer.Count > 0 ? averageYearsAsCustomer.Average() : 0.0;
        return ret;
    }

    private double CalculatePurhcaseFrequencyRate()
    {
        double ret = 0;
        //Calculate the average purchase frequency rate: Calculate this number by dividing the number 
        //of purchases over the course of the time period by the number of unique customers who made purchases during that time period.
        int numberOfPurchasesOverTimePeriod = salesData.Count;
        int numberOfUniqueCustomers = salesData.Select(s => s.companyname).Distinct().Count();
        ret = numberOfPurchasesOverTimePeriod / numberOfUniqueCustomers;
        return ret;
    }

    private void LoadData()
    {


        bool excludeVendors = cbxExcludeVendors.Checked;

        using (RzDataContext rdc = new RzDataContext())
        {
            salesData = rdc.ordhed_sales.Where(w => w.orderdate.Value.Date >= startDate.Date && w.orderdate < endDate.Date && !w.ordernumber.ToLower().Contains("cr") && (w.isvoid ?? false) != true && (w.isclosed ?? false) == true).ToList();
            if (excludeVendors)
            {
                List<string> CompanyIDsWithSales = salesData.Select(s => s.base_company_uid).Distinct().ToList();

                List<string> customerIDs = rdc.companies.Where(w => CompanyIDsWithSales.Contains(w.unique_id) && !w.companytype.ToLower().Contains("vendor")).Select(s => s.unique_id).ToList();
                salesData = salesData.Where(w => customerIDs.Contains(w.base_company_uid)).ToList();

            }

        }

        cvdList = (from o in salesData
                   group o by o.base_company_uid into g
                   select new CustomerValueData
                   {
                       customerID = g.Key,
                       customerName = g.Max(f => f.companyname),
                       totalRevenue = g.Sum(f => f.gross_profit ?? 0),
                       countOfSales = g.Count(),


                   }).ToList();
        foreach (CustomerValueData cvd in cvdList)
        {
            cvd.avgPurchaseValue = CalculateAveragePurchaseValue(cvd);//APV                
            cvd.customerValue = CalculateCustomerValue(cvd);//CV                
            cvd.lifetimeValue = CalculateLifetimeValue(cvd);//LTV
        }
        //customerName = g.First(f => f.companyname), Cars = g.ToList() }


        double totalCustomerValue = cvdList.Average(a => a.lifetimeValue);
        lblTotalCustomerValue.Text = Math.Round(totalCustomerValue, 2).ToString();

    }

    private double CalculateLifetimeValue(CustomerValueData cvd)
    {
        //Then, calculate LTV by multiplying customer value by the 
        //average customer lifespan. This will give you an estimate of how 
        //much revenue you can reasonably expect an average customer to 
        //generate for your company over the course of their relationship with you.
        double ret = 0;
        ret = cvd.customerValue / avgCustomerLifeSpan;
        return ret;
    }

    private double CalculateCustomerValue(CustomerValueData cvd)
    {
        //Calculate customer value: 
        //Calculate this number by multiplying the average purchase 
        //value by 1, and subtracting the average purchase frequency rate from that number.
        double ret = 0;

        double apv = cvd.avgPurchaseValue * 1;
        ret = apv - averagePurchaseFrequencyRate;

        return ret;

    }

    private double CalculateAveragePurchaseValue(CustomerValueData cvd)
    {
        //Calculate average purchase value: 
        //Calculate this number by dividing your company's total revenue in a time period (usually one year) 
        //by the number of purchases over the course of that same time period.
        double ret = 0;
        ret = cvd.totalRevenue / cvd.countOfSales;
        return ret;
    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        //Export Dataset to CSV
        //LoadData();

        if (cvdList == null || cvdList.Count <= 0)
        {
            tools.HandleResult("fail", "No data to export.");
            return;
        }


        tools.ExportListToCsv(cvdList, "results.csv");
        tools.HandleResult("Success!", "TestDeviceFilter");     
    }

    protected void lbRefresh_Click(object sender, EventArgs e)
    {
        LoadAll();
    }

    private void LoadAll()
    {
        LoadDates();
        LoadData();
        LoadGrid();
        LoadLabels();
    }

    private void LoadLabels()
    {
        averagePurchaseFrequencyRate = CalculatePurhcaseFrequencyRate();
        lblAPFR.Text = Math.Round(averagePurchaseFrequencyRate, 2).ToString();
        avgCustomerLifeSpan = CalculateAverageCustomerLifeSpan();
        lblACLS.Text = Math.Round(avgCustomerLifeSpan, 2).ToString();
        lblAverageSaleAmnt.Text = Math.Round(salesData.Average(a => (double)a.gross_profit)).ToString("C");
    }
}