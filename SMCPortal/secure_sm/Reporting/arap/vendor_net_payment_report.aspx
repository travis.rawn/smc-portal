﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="vendor_net_payment_report.aspx.cs" Inherits="secure_sm_Reporting_arap_vendor_net_payment_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .action-bar {
            position: fixed;
            padding: 10px;
            /*color: #EE2738;*/
            width: 300px;
            margin-left: -150px; /*half the size of width*/
            bottom: 0px;
            left: 50%;
            /*background-color: #d9534f;*/
            /*background-color: #181D1E;*/
            background-color: lightgrey;
            border: 1px solid;
            border-color: darkgray;
            z-index: 800;
            text-align: center;
            border-radius: 6px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <strong>Vendor NET Payment Report</strong>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="well well-sm">
        This report lists net payment terms for lines that have been received by shipping.  Currently only showing net terms that are <strong>Net10 or less</strong> and still have an outstanding balance.  Once full
        payment is in Rz, the PO will be removed from this list automatically.
    </div>

    <asp:HiddenField ID="hfChangedLineIds" runat="server" />
    <uc1:sm_datatable runat="server" ID="smdt" />

    <%-- Floating Action Panel--%>
    <div id="divAction" class="action-bar form" runat="server">
        <asp:LinkButton runat="server" ID="lbSave" class="btn btn-default btn-block updateProgress save-button" OnClick="lbSave_Click">
                  Save <i class="fa fa-floppy-o" aria-hidden="true"></i>
        </asp:LinkButton>
    </div>


    <script>
        //Detect input on row input
        $('.checkboxCell').on('input', function () {
            //Hiddenfield may already have array objects
            let changedIds = [];

            //retrieve array
            var existingIds = $("#MainContent_hfChangedLineIds").val();
            if (existingIds) {
                changedIds = JSON.parse(existingIds);
                //changedIds.push( );
            }
            
            //Get Row Changes
            var id = $(this).closest("tr").find('td:eq(9)').text();
            if (id) {
                if (!changedIds.includes(id))
                    changedIds.push(id);
            }
            //Set the hidden field to current contents of ID Array
            $("#MainContent_hfChangedLineIds").val(JSON.stringify(changedIds));

            //alert($("#MainContent_hfChangedLineIds").val());
        });
    </script>


</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

