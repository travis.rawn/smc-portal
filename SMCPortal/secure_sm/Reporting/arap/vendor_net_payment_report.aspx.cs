﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_arap_vendor_net_payment_report : System.Web.UI.Page
{
    [Serializable]
    class VendorPaymentData
    {
        public string VendorName { get; set; }
        public string PONumber { get; set; }
        public DateTime PODate { get; set; }
        public DateTime RcvDate { get; set; }
        public DateTime PmtDue { get; set; }
        public double TotalCost { get; set; }
        public double Quantity { get; set; }
        public string Terms { get; set; }
        public double RemDays { get; set; }
        public string LineUID { get; set; }
        public bool PaymentApproved { get; set; }
        public DateTime ReceiveDate { get; set; }

    }

    SM_Tools tools = new SM_Tools();
    private List<VendorPaymentData> VendorPaymentDataList = new List<VendorPaymentData>();
    //{
    //    get
    //    {
    //        if (ViewState["VendorPaymentDataList"] == null)
    //            ViewState["VendorPaymentDataList"] = new List<VendorPaymentData>();
    //        return (List<VendorPaymentData>)ViewState["VendorPaymentDataList"];
    //    }
    //    set
    //    {
    //        // the setter of the property always updates the ViewState
    //        ViewState["VendorPaymentDataList"] = value;
    //    }
    //}

    DateTime startDate = new DateTime(2021, 06, 01);
    List<line_process> lpList = new List<line_process>();
    SM_Tools t = new SM_Tools();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!AllowedUser())
        {
            t.HandleResult("warning", "Sorry, your account is not authorized to access this tool. ", 100000);
        }
        smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
        LoadData();
        LoadGrid();
    }

    private bool AllowedUser()
    {
        List<string> allowedUsers = new List<String>() { "kevint", "christ", "joem", "michelel", "fredt", "lindat", "jenc", "seanm" };
        string currentUserName = Membership.GetUser().UserName;
        if (allowedUsers.Contains(currentUserName))
            return true;
        return false;

    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //column 9 = line id   
        if (e.Row.Cells.Count >= 10)
        {
            //Hide InspID Header
            e.Row.Cells[9].CssClass = "hiddencol";
            //e.Row.Cells[10].CssClass = "hiddencol";
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            TableCell actionCell = e.Row.Cells[0];
            //string actionCellText = actionCell.Text;
            bool vendorPaymentApproved = false;
            //TextBox tb = new TextBox();
            CheckBox cb = (CheckBox)actionCell.Controls[0];
            cb.Enabled = true;
            vendorPaymentApproved = cb.Checked;
            //tb.Enabled = false;
            cb.ID = "rowCheckBox";
            cb.CssClass = "checkboxCell";
            cb.Checked = vendorPaymentApproved;
            //cb.CheckedChanged += new EventHandler(cb_CheckedChanged);
            //cb.AutoPostBack = true;
            actionCell.Controls.Add(cb);

            string strDaysToPay = e.Row.Cells[8].Text;
            int intDaysToPay = 0;
            if (!int.TryParse(strDaysToPay, out intDaysToPay))
                throw new Exception(strDaysToPay + " is an invalid integer.");

            if (intDaysToPay == 0)
                e.Row.BackColor = Color.Orange;
            else if (intDaysToPay < 0)
                e.Row.BackColor = Color.Red;

            if (vendorPaymentApproved)
                e.Row.BackColor = Color.Green;
        }
    }

    protected void cb_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox cb = (CheckBox)sender;
        GridViewRow gvRow = (GridViewRow)cb.NamingContainer;
        string lineUID = gvRow.Cells[9].Text;
        if (cb.Checked)
        {
            ApproveLinePayment(lineUID);
        }
        else
        {
            ClearPaymentApproval(lineUID);
        }
        LoadData();
        LoadGrid();
    }

    private void ApproveLinePayment(string lineUID)
    {
        line_process lp = null;
        using (RzDataContext rdc = new RzDataContext())
        {
            string rzUserID = Profile.RzUserID;
            if (string.IsNullOrEmpty(rzUserID))
                throw new Exception("No Rz user linked to your portal user account.  Please contact IT.");
            n_user rzUser = rdc.n_users.Where(w => w.unique_id == rzUserID).FirstOrDefault();
            if (rzUser == null)
                throw new Exception("No Rz user linked to your portal user account.  Please contact IT.");

            lp = rdc.line_processes.Where(w => w.orddet_line_uid == lineUID).FirstOrDefault();
            if (lp == null)
            {
                lp = new line_process();
                lp.date_created = DateTime.Now;
                lp.unique_id = Guid.NewGuid().ToString();
                rdc.line_processes.InsertOnSubmit(lp);
            }
            lp.type = SM_Enums.LineProcessType.VendorPaymentApproved.ToString();
            lp.orddet_line_uid = lineUID;
            lp.vendor_payment_approved = true;
            lp.vendor_payment_approved_by = rzUser.name;
            lp.vendor_payment_approved_date = DateTime.Now;
            rdc.SubmitChanges();
        }
    }

    private void ClearPaymentApproval(string lineUID)
    {
        line_process lp = null;
        using (RzDataContext rdc = new RzDataContext())
        {
            lp = rdc.line_processes.Where(w => w.orddet_line_uid == lineUID).FirstOrDefault();
            if (lp != null)
            {
                rdc.line_processes.DeleteOnSubmit(lp);
                rdc.SubmitChanges();
            }
        }

    }

    private void LoadData()
    {
        VendorPaymentDataList.Clear();
        List<string> validQcStatus = new List<string>() { SM_Enums.QcStatus.In_House.ToString(), SM_Enums.QcStatus.Final_Inspection.ToString(), SM_Enums.QcStatus.Shipped.ToString() };
        using (RzDataContext rdc = new RzDataContext())
        {
            //First, get a list of all unpaid PO's that have net terms.  
            var poQuery = rdc.ordhed_purchases.Where(w => w.date_created.Value.Date >= startDate.Date && w.terms.ToLower().Contains("net") && w.outstandingamount > 0).Distinct().ToList();
            List<string> poIds = poQuery.Select(s => s.unique_id).ToList();
            //Then from this get a list of lines for them.
            var lineQuery = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= startDate.Date && poIds.Contains(w.orderid_purchase) && validQcStatus.Contains(w.qc_status)).ToList();
            //var query = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= startDate.Date && validQcStatus.Contains( w.qc_status) && w.ordernumber_purchase.Trim().Length > 0);
            //var poQuery = rdc.ordhed_purchases.Where(w => query.Select(s => s.orderid_purchase).Distinct().ToList().Contains(w.unique_id) && w.terms.ToLower().Contains("net")).Distinct().ToList();
            List<string> lineUIDs = lineQuery.Select(s => s.unique_id).Distinct().ToList();
            lpList = rdc.line_processes.Where(w => lineUIDs.Contains(w.orddet_line_uid) && w.type == SM_Enums.LineProcessType.VendorPaymentApproved.ToString()).ToList();
            //Need foreach, else was bumping into 2100 limit for LINQ
            //foreach (string s in lineUIDs)
            //{
            //    line_process lp = rdc.line_processes.Where(w => w.orddet_line_uid == s).FirstOrDefault();
            //    if (lp == null)
            //        continue;
            //    if (!lpList.Contains(lp))
            //        lpList.Add(lp);
            //}

            foreach (var v in lineQuery)
            {
                ordhed_purchase p = poQuery.Where(w => w.unique_id == v.orderid_purchase && w.outstandingamount > 0).FirstOrDefault();
                if (p == null)
                    continue;
                string strNetDays = p.terms.ToUpper().Replace("NET", "").Trim();
                int intNetDays = 0;
                int.TryParse(strNetDays, out intNetDays);
                if (intNetDays <= 0)
                    continue;
                //if (intNetDays > 10)
                //    continue;
                DateTime receivedDate = v.receive_date_actual.Value;
                DateTime paymentDueDate = receivedDate.AddDays(intNetDays);

                //(EndDate - StartDate).TotalDays
                bool vendorPaymentApproved = false;
                line_process lp = lpList.Where(w => w.orddet_line_uid == v.unique_id).FirstOrDefault();
                if (lp != null)
                    if (lp.vendor_payment_approved ?? false)
                        vendorPaymentApproved = true;
                VendorPaymentData vpd = new VendorPaymentData();
                vpd.VendorName = v.vendor_name;
                vpd.PONumber = v.ordernumber_purchase;
                vpd.PODate = p.orderdate.Value;
                vpd.RcvDate = v.receive_date_actual.Value;
                vpd.TotalCost = v.unit_cost.Value * v.quantity.Value;
                vpd.Quantity = v.quantity.Value;
                vpd.Terms = p.terms;
                vpd.PmtDue = paymentDueDate;
                vpd.RemDays = (paymentDueDate - DateTime.Today).TotalDays;
                vpd.LineUID = v.unique_id;
                vpd.PaymentApproved = vendorPaymentApproved;
                VendorPaymentDataList.Add(vpd);

            }
        }




    }

    private void LoadGrid()
    {

        smdt.pageLength = 25;
        smdt.dataSource = VendorPaymentDataList.Select(s => new
        {
            Pay = s.PaymentApproved,
            s.VendorName,
            s.PONumber,
            RcvDate = s.RcvDate.ToShortDateString(),
            s.TotalCost,
            s.Quantity,
            s.Terms,
            DueDate = s.PmtDue.ToShortDateString(),
            RemDays = Math.Round(s.RemDays),
            s.LineUID,
            //s.PaymentApproved

        }).OrderByDescending(o => o.Pay).ThenBy(o => o.RcvDate);


        smdt.loadGrid();


    }



    protected void lbSave_Click(object sender, EventArgs e)
    {
        List<string> changedLineIdList = t.GetListFromHiddenField(hfChangedLineIds);
        if (changedLineIdList == null || changedLineIdList.Count == 0)
            return;

        foreach (GridViewRow row in smdt.theGridView.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox cb = (CheckBox)row.Cells[0].Controls[0];
                if (cb == null)
                    throw new Exception("Unable to find CheckBox for selected row(s).");
                string lineUID = row.Cells[9].Text;
                if (string.IsNullOrEmpty(lineUID))
                    throw new Exception("Unable to find line UID selected row(s).");
                if (!changedLineIdList.Contains(lineUID))
                    continue;

                if (cb.Checked)
                    ApproveLinePayment(lineUID);
                else
                    ClearPaymentApproval(lineUID);

                changedLineIdList.Remove(lineUID);
                if (changedLineIdList.Count == 0)
                    break;
            }
        }
        //LoadData();
        //LoadGrid();
        //string currentUrl = Request.RawUrl;
        //currentUrl += "?success=1";
        //Response.Redirect(currentUrl);
        tools.RedirectToSuccess(Request.RawUrl);
    }


}



