﻿<%@ Page Title="Reporting Dashboard" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="reporting_dashboard.aspx.cs" Inherits="secure_sm_Reports_reports_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">

    <style>
        .sm-pills > .nav-pills > li > a {
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            color: #000000;
        }

        .sm-pills > .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            color: #fff;
            background-color: #041c2c;
        }

        sm-pills > .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:focus {
            background: #000000;
            color: #fff;
        }

        sm-pills > .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            background: #0a4972;
            color: #fff;
        }

        .sm-list * {
            margin: 5px 0px 0px 0px;
            padding: 0;
            color: #333;
        }

        .sm-list div {
            margin: 20px;
        }

        .sm-list ul {
            list-style-type: none;
            /*min-width: 500px;
            max-width: 900px;*/
        }

        .sm-list li img {
            float: left;
            margin: 0 15px 0 0;
        }

        .sm-list li span {
            float: left;
            margin: 0 15px 0 0;
            font-size: 30px;
        }

        h4 {
            font: 20px/1.5 Roboto;
        }

        .sm-list li p {
            font: 200 12px/1.5 Georgia, Times New Roman, serif;
        }

        .sm-list li {
            padding: 10px;
            overflow: auto;
        }

            .sm-list li:hover {
                background: #f5f5f5;
                cursor: pointer;
            }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Reporting Dashboard</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="sm-pills">
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#sales">Sales Reports</a></li>
            <li><a data-toggle="pill" href="#marketing">Marketing Reports</a></li>
            <li><a data-toggle="pill" href="#quality">Quality Reports</a></li>
            <li><a data-toggle="pill" href="#customer">Customer</a></li>
            <li><a data-toggle="pill" href="#arap">AR / AP</a></li>
            <li><a data-toggle="pill" href="#dashoards">Dashboards</a></li>
            <li><a data-toggle="pill" href="#ssrs">SSRS Reports</a></li>
            <li><a data-toggle="pill" href="#archive">Archive</a></li>
        </ul>
    </div>

    <%--Sales--%>
    <div class="tab-content">
        <div id="sales" class="tab-pane fade in active">
            <div class="sm-list">
                <ul>
                   


                    <li>
                        <a href="sales/sourcing_monitor_report.aspx" target="_blank">
                            <label>Sourcing Monitor Report</label>
                            <p>View the validation progress of Quotes and Sales that are in progress.</p>
                        </a>
                    </li>

                    <li>
                        <a href="sales/sales_charts.aspx" target="_blank">
                            <label>Sales Charts</label>
                            <p>Here you can view various charts for key sales metrics.  This is a prototype of charting capabilities.</p>
                        </a>
                    </li>
                     <li>
                        <a href="sales/sales_theory_report.aspx" target="_blank">
                            <label>Sales Theory Data</label>
                            <p>See the values that feed the Sales Theory data.</p>
                        </a>
                    </li>
                    
                    <li>
                        <a href="sales/engagement_activity.aspx" target="_blank">
                            <label>Engagement Activity Report</label>
                            <p>Report of engagement signals from Hubspot (Emails, calls, notes)</p>
                        </a>
                    </li>

                    <li>
                        <a href="sales/consignment_sales_report.aspx" target="_blank">
                            <label>Consignment Sales</label>
                            <p>View consignment sales by consignment partner.</p>
                        </a>
                    </li>





                    <li>
                        <a href="sales/stock_sales_report.aspx" target="_blank">
                            <label>Stock Sales Report</label>
                            <p>View the stock PO and Sales activity between a date range.</p>
                        </a>
                    </li>

                    <li>
                        <a href="sales/part_activity_report.aspx" target="_blank">
                            <label>Part Activity Report</label>
                            <p>See data about parts that have been entered into batches, including overall count, customers, target quanitites, etc. within a date range</p>
                        </a>
                    </li>



                    <li>
                        <a href="sales/account_stagnation.aspx" target="_blank">
                            <label>Account Stagnation Report</label>
                            <p>See customers who have no business since a certain date, including total invoiced amount</p>
                        </a>
                    </li>

                    <li>
                        <a href="sales/source_tbd_report.aspx" target="_blank">
                            <label>TBD Lines Report</label>
                            <p>Get a List of current "Source TBD Lines"</p>
                        </a>
                    </li>





                </ul>
            </div>

        </div>
        <%--Marketing--%>
        <div id="marketing" class="tab-pane fade">
            <div class="sm-list">
                <ul>
                    <li>
                        <a href="marketing/customer_lifetime_value_report.aspx" target="_blank">
                            <label>Customer Lifetime Value Report</label>
                            <p>This will give you an estimate of how much gross profit you can reasonably expect an average customer to generate for your company over the course of their relationship with you.</p>
                        </a>
                    </li>

                </ul>

            </div>
        </div>
        <%--Quality--%>
        <div id="quality" class="tab-pane fade">
            <div class="sm-list">
                <ul>
                    <li>
                        <a href="quality/report_time_inspection_complete.aspx" target="_blank">
                            <label>Inspection Complete Timing</label>
                            <p>View the average time it takes for the inspection team to complete inspections.  We expect this time to decrease as a result of improved validation procedures, and increased visibility into order holds for both sales and managemente staff.</p>
                        </a>
                    </li>
                     <li>
                        <a href="quality/nonconformance_rma_report.aspx" target="_blank">
                            <label>Nonconformance / RMA Report</label>
                            <p>Track noncormance issues and performance.  This is migrated from the old SSRS report.</p>
                        </a>
                    </li>


                    

                </ul>
            </div>
        </div>
        <%--Customer--%>
        <div id="customer" class="tab-pane fade in">
            <div class="sm-list">
                <ul>

                    <li>
                        <a href="customer/company_risk_report.aspx" target="_blank">

                            <label>Customer Risk Report</label>
                            <p>Get Risk report based on customer quote and sale activity.</p>
                        </a>
                    </li>
                    <li>
                        <a href="customer/customer_acquisition_report.aspx" target="_blank">
                            <label>Customer Acquisition Report</label>
                            <p>Get stats of company creation, including first quote, first sale, and agent data.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <%--AR / AP--%>
        <div id="arap" class="tab-pane fade in">
            <div class="sm-list">
                <ul>

                    <li>
                        <a href="arap/vendor_net_payment_report.aspx" target="_blank">

                            <label>Vendor Net Payment Report</label>
                            <p>Lists upcoming vendor payments that are due, based on their net terms.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <%--Dashboards--%>
        <div id="dashoards" class="tab-pane fade in">
            <div class="sm-list">
                <ul>
                    <li>
                        <a href="dashboards/sales_dashboard.aspx" target="_blank">
                            <label>Sales Dashboard</label>
                            <p>Dashboard for Sales KPI monitoring</p>
                        </a>
                    </li>
                    <li>
                        <a href="dashboards/inspection_dashboard.aspx" target="_blank">
                            <label>Inspection Dashboard</label>
                            <p>Dashboard for Inspection KPI monitoring</p>
                        </a>
                    </li>
                    <li>
                        <a href="dashboards/validation_dashboard.aspx" target="_blank">
                            <label>Validation Dashboard</label>
                            <p>Dashboard for Validation</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <%--SSRS--%>
        <div id="ssrs" class="tab-pane fade in">
            <div class="sm-list">
                <ul>

                    <li>
                        <a href="http://sm1/Reports_SQLEXPRESS/Pages/Folder.aspx?ItemPath=%2fRz4+Reports&ViewMode=List" target="_blank">
                            <label>SSRS Reporting Dashboard</label>
                            <p>View legacy SQL Reports</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <%--Archive--%>
        <div id="archive" class="tab-pane fade in">
            <div class="sm-list">
                <ul>
                    
                     <li>
                        <a href="archive/stock_consignment_sales_report.aspx" target="_blank">
                            <label>Stock & Consignment Sales Report</label>
                            <p>View the sales activity between a date range.  Filter by stocktype and company name.</p>
                        </a>
                    </li>
                    <li>
                        <a href="archive/bom_analysis_tool.aspx" target="_blank">
                            <label>BOM Analysis Tool</label>
                            <p>Scrub a BOM for lifecuycle and risk data. (See also part activity and customer risk report)</p>
                        </a>
                    </li>

                    <li>
                        <a href="archive/calls_to_sales_report.aspx" target="_blank">
                            <label>Calls to Sales Report</label>
                            <p>See  call volume compared to actual profit numbers.</p>
                        </a>
                    </li>
                    <li>
                        <a href="archive/no_calls_report.aspx" target="_blank">
                            <label>No Calls Report</label>
                            <p>See a report of Agents with no calls in the last 30 minutes.  Ties in with an optional email alert</p>
                        </a>
                    </li>
                    <li>
                        <a href="archive/rz_hubspot_company_activity_contact_report.aspx" target="_blank">
                            <label>Rz & Hubspot Customer Activity</label>
                            <p>This will give you company-level information from both Rz and Hubspot involving sales and engagement activity.</p>
                        </a>
                    </li>
                   
                    <li>
                        <a href="archive/inspection_priority_report.aspx" target="_blank">
                            <label>Inspection Priority Report</label>
                            <p>View active, un-shipped lines, and evaluate their inspection priority.</p>
                        </a>
                    </li>

                    <li>
                        <a href="archive/turn_back_metrics.aspx" target="_blank">
                            <label>Turn Back Metrics</label>
                            <p>View the status of quality turn backs.</p>
                        </a>
                    </li>



                    <li>
                        <a href="archive/report_order_timing.aspx" target="_blank">
                            <label>Order Timing Report</label>
                            <p>This report shows the average time between 3 order progress indicators:  Sale to Purchase, Purchase to Invoice, Sale to Invoice.  We expect to see these numbers improve as we refine our validation process.</p>
                        </a>
                    </li>

                    <li>
                        <a href="archive/aging_sales.aspx" target="_blank">
                            <label>Aging Sales</label>
                            <p>View sales data related to lines that have not been completed since the set date. (i.e. orderdate was in the past, and they are still in an unresolved status)</p>
                        </a>
                    </li>

                    <li>
                        <a href="archive/report_avg_time_to_validation_complete.aspx" target="_blank">
                            <label>Avg Time Until Validation Complete</label>
                            <p>View the average time it takes for the validation team to complete the validation process.  We expect this number to decrease as a result of improved validation procedures.</p>
                        </a>
                    </li>


                    <li>
                        <a href="archive/report_avg_time_on_hold.aspx" target="_blank">
                            <label>Average Time On Hold</label>
                            <p>View the average times orders sit on hold, and the reasons for the hold.  We expect the average time on hold to decrease as a result of improved validation procedures.</p>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

