﻿<%@ page title="" language="C#" masterpagefile="~/hs_flex.master" autoeventwireup="true" codefile="nonconformance_rma_report.aspx.cs" inherits="secure_sm_Reporting_quality_nonconformance_rma_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .section {
            margin-bottom: 15px;
            /*border-color: black;*/
            /*border-style: dashed;*/
        }

        .inner {
            /*height: 300px;*/
            /*border-color: black;*/
            /*border-style: solid;*/
        }

        .stat_label {
            font-weight: bolder;
        }

        .wrapper {
            margin: 30px;
        }

        p {
            font-weight: bold;
            font-style: italic;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div class="wrapper">
        <div class="row">
            <div class="col-sm-12 outer">
                <h4>Nonconformance  / RMA Report</h4>
            </div>
        </div>
        <div class="row section">
            <div class="col-sm-2">
                <b>Start Date:</b>
                <uc1:datetime_flatpicker runat="server" id="dtpStart" />
            </div>
            <div class="col-sm-2">
                <b>End Date:</b>
                <uc1:datetime_flatpicker runat="server" id="dtpEnd" />
            </div>
            <div class="col-sm-2">
                <div style="padding-top: 20px;">
                    <Asp:LinkButton ID="lbRefresh" runat="server" Font-Size="Larger">Refresh</Asp:LinkButton>
                </div>
            </div>
            <div class="col-sm-3">
                <b>Order Accuracy (Lines): </b>
                <asp:Label ID="lblOrderAccuracyLines" runat="server">XXX</asp:Label>
            </div>
            <div class="col-sm-3">
                <b>Order Accuracy (QTY): </b>
                <asp:Label ID="lblOrderAccuracyQTY" runat="server">XXX</asp:Label>
            </div>
            <hr />
        </div>

        <div class="row section">
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-sm-4 inner">
                        <p>Summaries:</p>
                        Total Nonconforming Lines: 
                        <asp:Label ID="lblTotalNonconformingLines" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total Nonconforming QTY: 
                        <asp:Label ID="lblTotalNonconformingQty" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total CRMA Lines: 
                        <asp:Label ID="lblTotalCRMALines" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total CRMA QTY: 
                        <asp:Label ID="lblTotalCRMAQTY" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total VRMA Lines: 
                        <asp:Label ID="lblTotalVRMALines" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total VRMA QTY: 
                        <asp:Label ID="lblTotalVRMAQTY" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total Shipped Lines: 
                        <asp:Label ID="lblTotalShippedLines" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total Shipped QTY: 
                        <asp:Label ID="lblTotalShippedQTY" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total Short Ships: 
                        <asp:Label ID="lblTotalShortShips" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Total Non-RMA Clerical: 
                        <asp:Label ID="lblTotalNonRMAClerical" runat="server" CssClass="stat_label">XXX</asp:Label>
                    </div>
                    <div class="col-sm-4 inner">
                        <p>Customer RMA Stats</p>
                        % Customer RMA Lines:  
                        <asp:Label ID="lblPctCustomerRMALines_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        % Customer RMA QTY:  
                        <asp:Label ID="lblPctCustomerRMAQTY_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Functional Issues:  
                        <asp:Label ID="lblFunctionalIssues_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Syspect Counterfeit:  
                        <asp:Label ID="lblSuspectCounterfeit_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Damaged Product:  
                        <asp:Label ID="lblDamagedProduct_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Clerical - Vendor:  
                        <asp:Label ID="lblClericalVendor_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Clerical - SMC 
                        <asp:Label ID="lblClericalSMC_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Clerical - Customer:  
                        <asp:Label ID="lblClericalCustomer_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Packaging Issues:  
                        <asp:Label ID="lblPackagingIssues_CRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />

                    </div>
                    <div class="col-sm-4 inner">
                        <p>Vendor RMA Stats</p>
                        % Vendor RMA Lines:  
                        <asp:Label ID="lblPctCustomerRMALines_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        % Vendor RMA QTY:  
                        <asp:Label ID="lblPctCustomerRMAQTY_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Functional Issues:  
                        <asp:Label ID="lblFunctionalIssues_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Syspect Counterfeit:  
                        <asp:Label ID="lblSuspectCounterfeit_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Damaged Product:  
                        <asp:Label ID="lblDamagedProduct_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Clerical - Vendor:  
                        <asp:Label ID="lblClericalVendor_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Clerical - SMC 
                        <asp:Label ID="lblClericalSMC_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Clerical - Customer:  
                        <asp:Label ID="lblClericalCustomer_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                        Packaging Issues:  
                        <asp:Label ID="lblPackagingIssues_VRMA" runat="server" CssClass="stat_label">XXX</asp:Label><br />
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-6 inner">
                        <p>Chart: CRMA By Category</p>
                        <uc1:sm_chartjs runat="server" id="chtCRMA_Category" />
                    </div>
                    <div class="col-sm-6 inner">
                        <p>Chart: VRMA By Category</p>
                        <uc1:sm_chartjs runat="server" id="chtVRMA_Category" />
                    </div>
                </div>
            </div>

        </div>
        <div class="row section">
            <div class="col-sm-12">
                <p>Nonconforming Line Details:</p>
                <uc1:sm_datatable runat="server" id="smdt" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

