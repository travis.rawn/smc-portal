﻿using SensibleDAL.dbml;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_quality_nonconformance_rma_report : System.Web.UI.Page
{

    List<int> ncIDList = new List<int>();
    List<NonCon_Head> ncHeadList = new List<NonCon_Head>();
    List<NonCon_RMA> ncRMAList = new List<NonCon_RMA>();
    List<NonCon_Customer> ncCustomerList = new List<NonCon_Customer>();
    List<NonCon_Details> ncDetailsList = new List<NonCon_Details>();
    List<NonCon_Disposition> ncDispositionList = new List<NonCon_Disposition>();
    List<NonCon_Payment> ncPaymentList = new List<NonCon_Payment>();
    List<orddet_line> allLinesInRange = new List<orddet_line>();
    List<orddet_line> shippedLinesInRange = new List<orddet_line>();
    long allLinesCount = 0;// linesList.Count();
    long allLinesQTY = 0; // linesList.Sum(s => s.quantity).Value;
    long shippedLinesCount = 0;// linesList.Count();
    long shippedLinesQTY = 0; // linesList.Sum(s => s.quantity).Value;

    int crmaFunctionalCount = 0;
    int crmaSuspectCount = 0;
    int crmaClericalCount = 0;
    int crmaDamagedCount = 0;
    int crmaPackagingCount = 0;
    int crmaClericalVendorCount = 0;
    int crmaClericalSMCCount = 0;
    int crmaClericalCustomerCount = 0;


    int vrmaFunctionalCount = 0;
    int vrmaSuspectCount = 0;   
    int vrmaDamagedCount = 0;
    int vrmaPackagingCount = 0;
    int vrmaClericalVendorCount = 0;
    int vrmaClericalSMCCount = 0;
    int vrmaClericalCustomerCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();

        using (sm_nonconEntities ne = new sm_nonconEntities())
            LoadNonConData(ne);

        using (RzDataContext rdc = new RzDataContext())
            LoadRzData(rdc);

        //Load the Details Grid
        LoadGrid();
        //Load the stats labels
        LoadStats();
        //Load the Charts
        LoadCharts();


    }

    private void LoadStats()
    {
        //Summaries
        //This is actually just a count of NonCon_Heads, per the old report.  While not all noncon's are lines, some are partrecords, still, the old report while calln git "lines" was only counting Headers. 
        lblTotalNonconformingLines.Text = ncHeadList.Count.ToString();
        lblTotalNonconformingQty.Text = ncHeadList.Sum(s => s.QTY).ToString();
        lblTotalCRMALines.Text = ncRMAList.Count(w => w.isCRMA.Value == true).ToString();
        //Per SSRS if CRMA, and no CRMA QTY, then using NonConQty, else 0
        lblTotalCRMAQTY.Text = ncRMAList.Where(w => w.isCRMA.Value == true).Sum(s => s.CRMAQTY).ToString();
        lblTotalVRMALines.Text = ncRMAList.Count(w => w.isVRMA.Value == true).ToString();
        lblTotalVRMAQTY.Text = ncRMAList.Where(w => w.isVRMA.Value == true).Sum(s => s.VRMAQTY).ToString();

        //Shipped Lines  
        lblTotalShippedLines.Text = shippedLinesCount.ToString();
        lblTotalShippedQTY.Text = shippedLinesQTY.ToString();
        int totalShortShips = ncHeadList.Where(w => w.isShortShip ?? false == true).Count();
        lblTotalShortShips.Text = totalShortShips.ToString();
        //Non CRMA lines, non VRMA that are flagged as Clerical = true
        int totalNonRmaClerical = ncRMAList.Where(w => w.isClerical == true && (w.isVRMA == false && w.isCRMA == false)).Count();
        lblTotalNonRMAClerical.Text = totalNonRmaClerical.ToString();

        //Customer RMA Stats        
        List<NonCon_RMA> cRMAList = ncRMAList.Where(w => w.isCRMA ?? false == true).ToList();
        decimal crmaLines = cRMAList.Count();
        decimal crmaQTY = cRMAList.Sum(s => s.CRMAQTY.Value);
        decimal pctCustomerRmaLines = 0;
        if (crmaLines > 0)
            pctCustomerRmaLines = (crmaLines / allLinesCount);
        //pctCustomerRmaLines = Math.Round(pctCustomerRmaLines, 4);
        lblPctCustomerRMALines_CRMA.Text = pctCustomerRmaLines.ToString("P");
        decimal pctCustomerRmaQTY = 0;
        if (crmaQTY > 0)
            pctCustomerRmaQTY = crmaQTY / allLinesQTY;
        //pctCustomerRmaQTY = Math.Round(pctCustomerRmaQTY, 4);   
        lblPctCustomerRMAQTY_CRMA.Text = pctCustomerRmaQTY.ToString("P");

        crmaFunctionalCount = cRMAList.Count(c => c.isFunctionalIssue ?? false == true);
        crmaSuspectCount = cRMAList.Count(c => c.isSuspectCounterfeit ?? false == true);
        crmaDamagedCount = cRMAList.Count(c => c.isDamaged ?? false == true);
        crmaClericalVendorCount = cRMAList.Count(c => c.clVendor ?? false == true);
        crmaClericalSMCCount = cRMAList.Count(c => c.clSMC ?? false == true);
        crmaClericalCustomerCount = cRMAList.Count(c => c.clCustomer ?? false == true);
        crmaPackagingCount = cRMAList.Count(c => c.dmPackage ?? false == true);

        lblFunctionalIssues_CRMA.Text = crmaFunctionalCount.ToString();
        lblSuspectCounterfeit_CRMA.Text = crmaSuspectCount.ToString();
        lblDamagedProduct_CRMA.Text = crmaDamagedCount.ToString();
        lblClericalVendor_CRMA.Text = crmaClericalVendorCount.ToString();
        lblClericalSMC_CRMA.Text = crmaClericalSMCCount.ToString();
        lblClericalCustomer_CRMA.Text = crmaClericalCustomerCount.ToString();
        lblPackagingIssues_CRMA.Text = crmaPackagingCount.ToString();


        //Vendor RMA Stats
        List<NonCon_RMA> vRMAList = ncRMAList.Where(w => w.isVRMA ?? false == true).ToList();
        decimal vrmaLines = vRMAList.Count();
        decimal vrmaQTY = vRMAList.Sum(s => s.VRMAQTY.Value);
        decimal pctVendorRmaLines = 0;
        if (vrmaLines > 0)
            pctVendorRmaLines = (vrmaLines / allLinesCount);
        //pctVendorRmaLines = Math.Round(pctVendorRmaLines, 4);
        lblPctCustomerRMALines_VRMA.Text = pctVendorRmaLines.ToString("P");
        decimal pctVendorRmaQTY = 0;
        if (vrmaQTY > 0)
            pctVendorRmaQTY = vrmaQTY / allLinesQTY;
        //pctVendorRmaQTY = Math.Round(pctVendorRmaQTY, 4);
        lblPctCustomerRMAQTY_VRMA.Text = pctVendorRmaQTY.ToString("P");

        vrmaFunctionalCount = vRMAList.Count(c => c.isFunctionalIssue ?? false == true);
        vrmaSuspectCount = vRMAList.Count(c => c.isSuspectCounterfeit ?? false == true);
        vrmaDamagedCount = vRMAList.Count(c => c.isDamaged ?? false == true);
        vrmaClericalVendorCount = vRMAList.Count(c => c.clVendor ?? false == true);
        vrmaClericalSMCCount = vRMAList.Count(c => c.clSMC ?? false == true);
        vrmaClericalCustomerCount = vRMAList.Count(c => c.clCustomer ?? false == true);
        vrmaPackagingCount = vRMAList.Count(c => c.dmPackage ?? false == true);

        
        lblFunctionalIssues_VRMA.Text = vrmaFunctionalCount.ToString();
        lblSuspectCounterfeit_VRMA.Text = vrmaSuspectCount.ToString();
        lblDamagedProduct_VRMA.Text = vrmaDamagedCount.ToString();
        lblClericalVendor_VRMA.Text = vrmaClericalVendorCount.ToString();
        lblClericalSMC_VRMA.Text = vrmaClericalSMCCount.ToString();
        lblClericalCustomer_VRMA.Text = vrmaClericalCustomerCount.ToString();
        lblPackagingIssues_VRMA.Text = vrmaPackagingCount.ToString();


        //Order Accurace Stats
        //CRMALineCount / All Line Count
        decimal orderAccuracyLines = Math.Round(100 - (crmaLines / allLinesCount), 4);
        lblOrderAccuracyLines.Text = orderAccuracyLines.ToString();
        decimal orderAccuracyQty = Math.Round(100 - (crmaQTY / allLinesQTY), 4);
        lblOrderAccuracyQTY.Text = orderAccuracyQty.ToString();

    }

    private void LoadCharts()
    {
        //throw new NotImplementedException();
        //crmaChart.
        //Customer RMAs by Category
        SM_Charts.ChartJS crmaChart = new SM_Charts.ChartJS();
        crmaChart.chartType = SM_Enums.ChartType.pie.ToString();
        crmaChart.canvasID = "cvCrma";
        crmaChart.controlPrefix = chtCRMA_Category.ClientID;
        crmaChart.displayXaxisLabel = false;
        crmaChart.displayYaxisLabel = false;        
        //crmaChart.titleText = "Customer RMA by Category";

        //The Data
        Dictionary<string, int> dictCrmaData = new Dictionary<string, int>();
        dictCrmaData.Add("Functional", crmaFunctionalCount);
        dictCrmaData.Add("Suspect Counterfeit", crmaSuspectCount);
        dictCrmaData.Add("Clerical", crmaClericalCustomerCount + crmaClericalSMCCount + crmaClericalVendorCount);
        dictCrmaData.Add("Damaged", crmaDamagedCount);
        dictCrmaData.Add("Packaging", crmaPackagingCount);

        //Build the dataset
        SM_Charts.chartJsDataSet crmaDS = new SM_Charts.chartJsDataSet();
        crmaDS.chartType = SM_Enums.ChartType.pie.ToString();
        List<string>crmaDSData = dictCrmaData.OrderBy(o => o.Key.ToString()).Select(s => s.Value.ToString()).ToList();
        crmaDS.data = crmaDSData;
        crmaDS.backGroundColorList = GetChartColors();
        crmaChart.dataSetList = new List<SM_Charts.chartJsDataSet>() { crmaDS };   
        List<string> crmaLabels =dictCrmaData.OrderBy(o => o.Key.ToString()).Select(s => s.Key.ToString()).ToList();
        crmaChart.labels = crmaLabels;
        chtCRMA_Category.LoadChart(crmaChart);


        //Vendor RMAs by Category"
        SM_Charts.ChartJS vrmaChart = new SM_Charts.ChartJS();
        vrmaChart.chartType = SM_Enums.ChartType.pie.ToString();        
        vrmaChart.controlPrefix = chtVRMA_Category.ClientID;
        vrmaChart.canvasID = "cvVrma";
        vrmaChart.displayXaxisLabel = false;
        vrmaChart.displayYaxisLabel = false;
        //vrmaChart.titleText = "Vendor RMA by Category";

        //The Data
        Dictionary<string, int> dictVrmaData = new Dictionary<string, int>();
        dictVrmaData.Add("Functional", vrmaFunctionalCount);
        dictVrmaData.Add("Suspect Counterfeit", vrmaSuspectCount);
        dictVrmaData.Add("Clerical", vrmaClericalCustomerCount + vrmaClericalSMCCount + vrmaClericalVendorCount);
        dictVrmaData.Add("Damaged", vrmaDamagedCount);
        dictVrmaData.Add("Packaging", vrmaPackagingCount);

        //Build the dataset
        SM_Charts.chartJsDataSet vrmaDS = new SM_Charts.chartJsDataSet();
        vrmaDS.chartType = SM_Enums.ChartType.pie.ToString();
        List<string> vrmaDSData = dictVrmaData.OrderBy(o => o.Key.ToString()).Select(s => s.Value.ToString()).ToList();
        vrmaDS.data = vrmaDSData;
        vrmaDS.backGroundColorList = GetChartColors();

        vrmaChart.dataSetList = new List<SM_Charts.chartJsDataSet>() { vrmaDS };
        List<string> vrmaLabels = dictCrmaData.OrderBy(o => o.Key.ToString()).Select(s => s.Key.ToString()).ToList();
        vrmaChart.labels = vrmaLabels;
        chtVRMA_Category.LoadChart(vrmaChart);


        //Add the charts to the list
        List<SM_Charts.ChartJS> chartList = new List<SM_Charts.ChartJS>();
        chartList.Add(crmaChart);
        chartList.Add(vrmaChart);

        //Actuall load       
        SM_Charts smc = new SM_Charts();
        smc.LoadAllCharts(Page, chartList);

    }

    private List<string> GetChartColors()
    {
      return new  List<string>() {
          SM_Tools.Colors.smOrangeRgba,
          SM_Tools.Colors.smGreenRgba,
          SM_Tools.Colors.smLightBlueRgba,
          SM_Tools.Colors.smRedRgba,
          SM_Tools.Colors.smNavyRgba
      };
    }

    class NonConDetailObject
    {
        //NonConID
        //PartNumber
        //StockType
        //NonConDate
        //MFG
        //VendorPO
        //VendorNAme
        //SONumber
        //CustomerName
        public int NonConID { get; set; }
        public string PartNumber { get; set; }
        public string StockType { get; set; }
        public DateTime NonConDate { get; set; }
        public string MFG { get; set; }
        public string VendorPO { get; set; }
        public string VendorName { get; set; }
        public string SONumber { get; set; }
        public string CustomerName { get; set; }
    }

    private void LoadGrid()
    {
        //this is the data that came from SSRS RMADATA Dataset
        List<NonConDetailObject> ncDetailList = new List<NonConDetailObject>();
        //Loop through each Nocon Head, each head SHOULD have a 1-1 link with a line item and.
        foreach (NonCon_Head h in ncHeadList)
        {
            NonConDetailObject ndo = new NonConDetailObject();
            ndo.NonConID = h.NonConID;
            ndo.PartNumber = h.partNumber;    
            ndo.StockType = h.stocktype;
            orddet_line l = allLinesInRange.Where(w => w.nonconid == h.NonConID).FirstOrDefault();
            if (l != null)
                ndo.StockType = l.stocktype;
            ndo.NonConDate = h.NonConDate.Value;
            ndo.MFG = h.MFG;
            ndo.VendorPO = h.vendorPO;
            ndo.VendorName = h.vendorName;
            NonCon_Customer nc = ncCustomerList.Where(w => w.NonConID == h.NonConID).FirstOrDefault();
            ndo.SONumber = "N/A";
            ndo.CustomerName = "N/A";
            if (nc != null)
            {
                ndo.SONumber = nc.SONumber;
                ndo.CustomerName = nc.customerName;
            }
            ncDetailList.Add(ndo);

        }

        smdt.dataSource = ncDetailList;
        smdt.loadGrid();




    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Today.AddMonths(-6);
            dtpEnd.SelectedDate = DateTime.Today;
        }


    }

    private void LoadNonConData(sm_nonconEntities ne)
    {
        //NonConHeads
        ncHeadList = ne.NonCon_Head.Where(w =>
        DbFunctions.TruncateTime(w.NonConDate.Value) >= dtpStart.SelectedDate
        && DbFunctions.TruncateTime(w.NonConDate.Value) <= dtpEnd.SelectedDate
        && (w.deleted ?? false) != true
        && (w.partNumber ?? "").Length > 0
        ).ToList();

        //NonConHead IDs
        ncIDList = ncHeadList.Select(s => s.NonConID).Distinct().ToList();
        //NonCon RMA
        ncRMAList = ne.NonCon_RMA.Where(w => ncIDList.Contains(w.NonConID)).ToList();
        //NonCon Customer
        ncCustomerList = ne.NonCon_Customer.Where(w => ncIDList.Contains(w.NonConID)).ToList();
        //NonCon Details
        ncDetailsList = ne.NonCon_Details.Where(w => ncIDList.Contains(w.NonConID)).ToList();
        //NonCon Disposition
        ncDispositionList = ne.NonCon_Disposition.Where(w => ncIDList.Contains(w.NonConID)).ToList();
        //NonCon Payments
        ncPaymentList = ne.NonCon_Payment.Where(w => ncIDList.Contains(w.NonConID)).ToList();


    }

    private void LoadRzData(RzDataContext rdc)
    {
        //Rz Line Items.       
        //We clearly don't want to compare to all lines from forever, but not all lines will be created withing the same date range that a corresponding
        //Noncon has been created.  Therefore, using date_Creates to filter lines to a reasonable comparison to number of noncons created relative to new lines
        //created.
        allLinesInRange = rdc.orddet_lines.Where(w => w.status.ToLower() != "void"
        && w.date_created.Value.Date >= dtpStart.SelectedDate
        && w.date_created.Value.Date <= dtpEnd.SelectedDate
       ).ToList();

        allLinesCount = allLinesInRange.Count();
        allLinesQTY = allLinesInRange.Sum(s => s.quantity.Value);


        //Shipped Lines
        shippedLinesInRange = rdc.orddet_lines.Where(w => w.ship_date_actual.Value.Date >= dtpStart.SelectedDate && w.ship_date_actual.Value.Date <= dtpEnd.SelectedDate).ToList();
        shippedLinesCount = allLinesInRange.Count();
        shippedLinesQTY = allLinesInRange.Sum(s => s.quantity.Value);
    }
}