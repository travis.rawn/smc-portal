﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;
using System.Data.Entity;

public partial class secure_sm_Reporting_quality_report_time_inspection_complete : System.Web.UI.Page
{

    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    List<n_user> agentList = new List<n_user>();
    protected List<insp_head> inspectionsList = new List<insp_head>();

    //protected List<insp_head> inspectionsList
    //{
    //    get
    //    {
    //        if (ViewState["inspectionsList"] == null)
    //            ViewState["inspectionsList"] = new List<insp_head>();
    //        return (List<insp_head>)ViewState["inspectionsList"];
    //    }
    //    set
    //    {
    //        ViewState["inspectionsList"] = value;
    //    }
    //}

    List<InspectionTimings> timings = new List<InspectionTimings>();
    public class InspectionTimings
    {
        public string ID { get; set; }
        public DateTime Date { get; set; }
        public string Inspector { get; set; }
        public bool Complete { get; set; }
        public string CompBy { get; set; }
        public DateTime? CompDate { get; set; }
        public string Result { get; set; }
        public string Status { get; set; }
        //public string timeStri { get; set; }
        public TimeSpan totalTime { get; set; }

    }

    public class ChartData
    {
        public string label { get; set; }
        public string value { get; set; }
        public string color { get; set; }
        public string highlight { get; set; }
    }

    protected List<string> inspectorList
    {
        get
        {
            if (ViewState["inspectorList"] == null)
                ViewState["inspectorList"] = new List<string>();
            return (List<string>)ViewState["inspectorList"];
        }
        set
        {
            ViewState["inspectorList"] = value;
        }
    }

    //DateTime startDate;
    //DateTime endDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        //rzap.SelectedIndexChanged += new EventHandler(RzAgentPicker_SelectedIndexChanged);
        if (!Page.IsPostBack)        
            LoadDatePickers();
            LoadInspections();  
        
            LoadAgentPicker();

        //}

    }

    private void LoadInspections()
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
            inspectionsList = pdc.insp_head.Where(w => (DbFunctions.TruncateTime(w.date_created) >= dtpStart.SelectedDate && DbFunctions.TruncateTime(w.date_created) <= dtpEnd.SelectedDate) && (w.is_deleted == null || w.is_deleted != true)  && (w.fullpartnumber.Length > 0 && w.fullpartnumber != null)).ToList();
    }

    private void bindgvInspectionTiming()
    {
        try
        {

            agentList = rzap.GetSelectedUsers();
            List<string> selectedUserIds = agentList.Select(s => s.unique_id).Distinct().ToList();
            if (selectedUserIds.Count == 0)
                throw new Exception("Invalid Selected user ID");

            GetInspectors();
            if (inspectorList.Count > 0)
            {

                List<InspectionTimings> timingsList = GetInspectionTimings();
                if (timingsList.Count <= 0)
                {
                    ClearData();
                    return;
                }

                divCharts.Style["display"] = "block";
                //gvInspectionTiming.DataSource = timingsList;
                //gvInspectionTiming.DataBind();
                //gvInspectionTiming.DataKeyNames = new[] { "insp_id" };
                gvInspectionTiming.dataSource = timingsList;
                gvInspectionTiming.loadGrid(true);
                //gvInspectionTiming.theGridView.HeaderRow.TableSection = TableRowSection.TableHeader;









                lblCount.Visible = true;
                lblAvg.Visible = true;
                lblTotalTime.Visible = true;


                int count = timings.Count;
                string totalTime = "";
                string averageTime = "";
                GetTotalTimeFromTimeSpan(out totalTime, out averageTime);

                //decimal totalTime = timings.Sum(s => Convert.ToDecimal(s.totalTime ?? "0"));

                lblCount.Text = "Count: " + count;
                lblTotalTime.Text = "Total: " + totalTime;
                lblAvg.Text = "Average: " + averageTime;
                //ret = timings.OrderByDescending(o => o.Date).ToList();
                //ret = ret.OrderBy(o => o.orderid_sales).ThenByDescending(d => d.date_created).ToList();



                LoadPieCharts(timingsList);
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void ClearData()
    {

        gvInspectionTiming.dataSource = null;
        gvInspectionTiming.loadGrid(true);
        lblCount.Visible = false;
        lblAvg.Visible = false;
        lblTotalTime.Visible = false;
        divCharts.Style["display"] = "none";

    }

    public List<InspectionTimings> GetInspectionTimings()
    {

        DateTime startDate = dtpStart.SelectedDate.Value;
        DateTime endDate = dtpEnd.SelectedDate.Value;
        timings = new List<InspectionTimings>();

       

        foreach (insp_head h in inspectionsList)
        {
            InspectionTimings it = new InspectionTimings();
            it.ID = h.insp_id.ToString();
            it.Date = (DateTime)h.date_created;
            it.Inspector = h.inspector;
            it.Complete = h.is_insp_complete ?? false;
            //resolution.holdReason = hold.hold_reason ?? "Not found";
            it.CompBy = h.completed_by ?? "N/A";

            if (h.completed_date != null)
                it.CompDate = h.completed_date;
            else
                it.CompDate = null;
            it.totalTime = GenerateTimeSpan(h);
            it.Status = h.status;
            it.Result = h.result;
            timings.Add(it);

        }

        return timings.OrderByDescending(o => o.Date).ToList();
    }

    private void GetTotalTimeFromTimeSpan(out string totalTime, out string averageTime)
    {
        int hours = 0;
        int minutes = 0;
        double seconds = 0;
        totalTime = "";
        averageTime = "";
        List<TimeSpan> tsList = new List<TimeSpan>();
        foreach (InspectionTimings tim in timings)
        {
            //seconds += tim.totalTime.Seconds;
            //minutes += tim.totalTime.Minutes;
            //hours += tim.totalTime.Hours;
            tsList.Add(tim.totalTime);
        }
        //Total Time
        double totalTimeSpan = tsList.Sum(sum => sum.TotalMilliseconds);
        TimeSpan total = TimeSpan.FromMilliseconds(totalTimeSpan);
        totalTime = total.Days + ":" + total.Hours + ":" + total.Minutes + ":" + total.Seconds;

        //Average Time
        double avgTimeSpan = tsList.Average(timeSpan => timeSpan.TotalMilliseconds);
        //Now using this value you could arrive at the new TimeSpan using
        TimeSpan avg = TimeSpan.FromMilliseconds(avgTimeSpan);
        averageTime = avg.Days + ":" + avg.Hours + ":" + avg.Minutes + ":" + avg.Seconds;

    }

    private TimeSpan GenerateTimeSpan(insp_head h)
    {
        TimeSpan ts = new TimeSpan();
        //Check for weekends to subtract
        DateTime startDate = h.date_created.Value;
        DateTime endDate = DateTime.Today;
        if (h.completed_date != null)
            endDate = h.completed_date.Value;

        int weekendsToSubtract = Tools.Dates.GetCountOfWeekendDaysFromRangeofWeekDays(startDate, endDate);
        //if both dates are on same day, return simple timespan
        TimeSpan weekendInterval = TimeSpan.FromDays(weekendsToSubtract);
        ts = ((h.completed_date ?? DateTime.Now) - (DateTime)h.date_created);//.TotalMinutes;.ToString("#.##");

        //Subtract evenings, 12 hours for each day i.e. 7pm - 7am, no inspections being worked
        //if the start and end date are the same date, timespan.Days will = 0, so we'll intiate the variable at 1, then add days to that
        int totalDays = ts.Days;
        int eveningHoursToSubtract = 12 * totalDays;
        TimeSpan eveningInterval = TimeSpan.FromHours(eveningHoursToSubtract);

        ts = ts.Subtract(weekendInterval);
        ts = ts.Subtract(eveningInterval);

        return ts;
    }

    protected void lbClear_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl, false);
    }
    protected void lbSearch_Click(object sender, EventArgs e)
    {
        bindgvInspectionTiming();
    }

    private void LoadDatePickers()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.placeholderText = "Start Date";
            dtpEnd.placeholderText = "End Date";
            dtpStart.SelectedDate = DateTime.Today.AddMonths(-3);
            dtpEnd.SelectedDate = DateTime.Now;
        }
    }

    private void LoadAgentPicker()
    {
        List<string> inspectionTeam = new List<string>() { "WareHouse" };

        //Filer the picker to only inspectors within the selected date range.
        //using (sm_portalEntities pdc = new sm_portalEntities())
        //{
            List<string> inspectorsInRange = inspectionsList.Where(w => !w.inspector.ToLower().Contains("kevin")
            ).Select(s => s.inspector).Distinct().ToList();
            List<n_user> userList = new List<n_user>();
            using (RzDataContext rdc = new RzDataContext())
                userList = rdc.n_users.Where(w => inspectorsInRange.Contains(w.name)).ToList();

            rzap.LoadPicker(userList);
            rzap.SelectedUserID = "all";
        //}

    }

    private void RzAgentPicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        //bindgvInspectionTiming();
    }

    private void GetInspectors()
    {

        inspectorList.Clear();


        List<string> selectedUserNames = agentList.Select(s => s.name).Distinct().ToList();
        inspectorList.AddRange(selectedUserNames);


    }

    private void LoadPieCharts(List<InspectionTimings> timingsList)
    {

        LoadTotalInspectionsPerAgentPie(timingsList);
        LoadAvgInspectionTimePerAgent(timingsList);
        LoaAvgInspectionCompletionsPerAgentPerDay(timingsList);


    }
    private void LoadTotalInspectionsPerAgentPie(List<InspectionTimings> timingsList)
    {
        List<ChartData> chartDataset = new List<ChartData>();
        if (timingsList.Count > 0)
        {
            foreach (string ins in inspectorList.Where(w => w != "0" && w.ToLower() != "warehouse"))
            {
                ChartData cData = new ChartData();
                decimal agentInspectionCount = timingsList.Count(c => c.Inspector == ins);
                string inspectorName = ins;
                cData.label = ins;
                string val = agentInspectionCount.ToString("#.##");
                if (string.IsNullOrEmpty(val))
                    cData.value = "0";
                else
                    cData.value = agentInspectionCount.ToString("#.##");
                chartDataset.Add(cData);
            }
        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        hfTotalInspectionsPerAgentPie.Value = js.Serialize(chartDataset);
    }
    private void LoadAvgInspectionTimePerAgent(List<InspectionTimings> timingsList)
    {
        List<ChartData> chartDataset = new List<ChartData>();
        if (timingsList.Count > 0)
        {
            foreach (string inspectorName in inspectorList.Where(w => w != "0" && w.ToLower() != "warehouse"))
            {

                ChartData cData = new ChartData();
                decimal agentInspectionCount = timingsList.Count(c => c.Inspector == inspectorName);
                decimal totalminutes = timingsList.Where(c => c.Inspector == inspectorName).Sum(s => Convert.ToDecimal(s.totalTime.Minutes));
                cData.label = inspectorName;
                if (agentInspectionCount > 0)
                    cData.value = ((totalminutes / 60) / agentInspectionCount).ToString("#.##");
                else cData.value = "0";
                chartDataset.Add(cData);
            }
        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        hfAvgInspectionTimePerAgent.Value = js.Serialize(chartDataset);
    }
    private void LoaAvgInspectionCompletionsPerAgentPerDay(List<InspectionTimings> timingsList)
    {
        List<ChartData> chartDataset = new List<ChartData>();
        if (timingsList.Count > 0)
        {
            foreach (string inspectorName in inspectorList.Where(w => w != "0" && w.ToLower() != "warehouse"))
            {   //new ChardDara object            
                ChartData cData = new ChartData();
                //Variables for chart data calculation
                decimal agentCompletionCount = timingsList.Count(c => c.Inspector == inspectorName && c.Complete == true);
                decimal totalminutes = timingsList.Where(c => c.Inspector == inspectorName).Sum(s => Convert.ToDecimal(s.totalTime.Minutes));
                //decimal totalDays = (totalminutes / 60) / 24;
                TimeSpan tsTotalDays = dtpEnd.SelectedDate.Value.Date - dtpStart.SelectedDate.Value.Date;
                decimal totalDays = tsTotalDays.Days;


                //set the chart properties
                cData.label = inspectorName;
                if (agentCompletionCount > 0)
                    cData.value = (agentCompletionCount / totalDays).ToString("#.##");
                else cData.value = "0";
                //Add the object to ChartDataset
                chartDataset.Add(cData);
            }
        }
        //Serialize it
        JavaScriptSerializer js = new JavaScriptSerializer();
        //In Javascript, consume with 
        hfAvgInspectionCompletionsPerAgentPerDay.Value = js.Serialize(chartDataset);
    }


}
