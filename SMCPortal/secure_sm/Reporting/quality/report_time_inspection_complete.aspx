﻿<%@ Page Title="Average Time Inspection Complete" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="report_time_inspection_complete.aspx.cs" Inherits="secure_sm_Reporting_quality_report_time_inspection_complete" %>

<%@ Register Src="~/assets/controls/RzAgentPicker.ascx" TagPrefix="uc1" TagName="RzAgentPicker" %>
<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        /*.searchControl {
            padding: 3px;
            max-width: 150px;
        }

        .datetimePicker {
            margin: 0px;
        }*/

        .form-inline .form-control {
            width: 100%;
        }

        .agentPicker {
            width: auto !important;
        }

        .form-inline .form-group {
            display: inline-block;
        }

        .card {
            margin: 30px;
        }

        .chart {
            position: relative;
            height: 32vh;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div id="pageTitle">
        <h1>Inspection Completion Timing</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FullWidthContent" runat="Server">

    <div class="panel panel-default card">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-2">
                    <label>
                        <asp:Label ID="lblCount" runat="server"></asp:Label></label>
                </div>
                <div class="col-sm-2">
                    <label>
                        <asp:Label ID="lblTotalTime" runat="server"></asp:Label></label>
                </div>
                <div class="col-sm-2">
                    <label>
                        <asp:Label ID="lblAvg" runat="server"></asp:Label></label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-inline" style="padding-bottom: 5px;">
            <div class="form-group">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />

                <uc1:RzAgentPicker runat="server" ID="rzap" />

                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()" OnClick="lbSearch_Click">
                             <span class="fa fa-search" style="font-size:18px;"></span>
                </asp:LinkButton>
                <asp:LinkButton ID="lbClear" runat="server" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()" OnClick="lbClear_Click">
                             <span class="fa fa-times" style="font-size:18px;"></span>
                </asp:LinkButton>
            </div>
        </div>
        <div class="row" id="divCharts" runat="server" style="display: none;">
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <label for="pieTotalInspectionsPerAgent">
                            Total Inspections Created<br />


                        </label>

                        <div class="chart">
                            <canvas id="pieTotalInspectionsPerAgent"></canvas>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <label for="pieAvgInspectionTimePerAgent">
                            Average Completion Time (Hours)<br />


                        </label>

                        <div class="chart">
                            <canvas id="pieAvgInspectionTimePerAgent"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <label for="pieAvgInspectionTimePerAgentPerDay">
                            Average Completions Per Day<br />

                        </label>
                        <div class="chart">
                            <canvas id="pieAvgInspectionTimePerAgentPerDay"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>
            <a href="#gridCollapse" data-toggle="collapse">Details ...</a>
        </p>



        <div id="gridCollapse" class="collapse">
            <%-- <asp:GridView ID="gvInspectionTiming" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None" EmptyDataText="No Data Found" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="insp_id" HeaderText="insp_id" />
                        <asp:BoundField DataField="date_created" HeaderText="Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="inspector" HeaderText="Inspector" />
                        <asp:BoundField DataField="is_insp_complete" HeaderText="Complete?" />
                        <asp:BoundField DataField="completed_by" HeaderText="Completed By" />
                        <asp:BoundField DataField="completed_date" HeaderText="Complete Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="status" HeaderText="Status" />
                        <asp:BoundField DataField="result" HeaderText="Result" />
                        <asp:BoundField DataField="totalMinutes" HeaderText="Minutes" />
                    </Columns>
                </asp:GridView>--%>
            <uc1:sm_datatable runat="server" ID="gvInspectionTiming" />
        </div>

    </div>
    </div>
    <asp:HiddenField ID="hfTotalInspectionsPerAgentPie" runat="server" />
    <asp:HiddenField ID="hfAvgInspectionTimePerAgent" runat="server" />
    <asp:HiddenField ID="hfAvgInspectionCompletionsPerAgentPerDay" runat="server" />

    <script>
        $('#gridCollapse').on('shown.bs.collapse', function () {
            HideUpdateProgress();
        })
        $('#gridCollapse').on('hidden.bs.collapse', function () {
            HideUpdateProgress();
        })
    </script>
    <script>

        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100);
        };


        window.onload = function () {
            LoadTotalInspectionsPerAgent();
            LoadAvgInspectionTimePerAgent();
            LoadAvgInspectionCompletionsPerAgentPerDay();

        };

        function LoadTotalInspectionsPerAgent() {
            if ($("#<% =hfTotalInspectionsPerAgentPie.ClientID%>").val() != null && $("#<% =hfTotalInspectionsPerAgentPie.ClientID%>").val() != '') {

                var hfObj = $("#<% =hfTotalInspectionsPerAgentPie.ClientID%>").val();
                var parsedObj = $.parseJSON(hfObj);
                var arrLabels = [], arrData = [], arrColors = [];

                Object.keys(parsedObj).forEach(function (key) {
                    //get the value of label
                    var label = parsedObj[key]["label"];
                    //push the label string in the array
                    arrLabels.push(label);
                    var value = parsedObj[key]["value"];
                    arrData.push(value);
                    //Grabbing colors dynamically on this 1st loop.  
                    arrColors.push(dynamicColors())
                    //var color = parsedObj[key]["color"];
                    //arrColors.push(color)
                });

                var ctx = document.getElementById("pieTotalInspectionsPerAgent").getContext('2d');
                var config = {
                    type: 'pie',
                    data: {
                        labels: arrLabels,
                        datasets: [{
                            backgroundColor: arrColors,
                            data: arrData
                        }]
                    }
                };
                var myChart = new Chart(ctx, config);
            }

        };



        function LoadAvgInspectionTimePerAgent() {
            if ($("#<% =hfAvgInspectionTimePerAgent.ClientID%>").val() != null && $("#<% =hfAvgInspectionTimePerAgent.ClientID%>").val() != '') {

                var hfObj = $("#<% =hfAvgInspectionTimePerAgent.ClientID%>").val();
                var parsedObj = $.parseJSON(hfObj);
                var arrLabels = [], arrData = [], arrColors = [];

                Object.keys(parsedObj).forEach(function (key) {
                    //get the value of label
                    var label = parsedObj[key]["label"];
                    //push the label string in the array
                    arrLabels.push(label);
                    var value = parsedObj[key]["value"];
                    arrData.push(value);
                    //Grabbing colors dynamically on this 1st loop.  
                    arrColors.push(dynamicColors())
                    //var color = parsedObj[key]["color"];
                    //arrColors.push(color)
                });

                var ctx = document.getElementById("pieAvgInspectionTimePerAgent").getContext('2d');
                var config = {
                    type: 'pie',
                    data: {
                        labels: arrLabels,
                        datasets: [{
                            backgroundColor: arrColors,
                            data: arrData
                        }]
                    }
                };
                var myChart = new Chart(ctx, config);
            }

        };


        function LoadAvgInspectionCompletionsPerAgentPerDay() {
            if ($("#<% =hfAvgInspectionTimePerAgent.ClientID%>").val() != null && $("#<% =hfAvgInspectionTimePerAgent.ClientID%>").val() != '') {
                //THE DATA
                //Get JSON dataset from hidden field
                var hfObj = $("#<% =hfAvgInspectionCompletionsPerAgentPerDay.ClientID%>").val();
                //parse the value into an array on objects
                var parsedObj = $.parseJSON(hfObj);
                //instantiate variables for chart properties
                var arrLabels = [], arrData = [], arrColors = [];

                //loop through the dataset to get the values per keey (i.e. agent, company, etc.)
                Object.keys(parsedObj).forEach(function (key) {
                    //get the value, push into string[], repeat per property
                    var label = parsedObj[key]["label"];
                    arrLabels.push(label);
                    var value = parsedObj[key]["value"];
                    arrData.push(value);
                    //Grabbing colors dynamically  
                    arrColors.push(dynamicColors())
                });

                //THE CHART
                //set chart variable by canvas id
                var ctx = document.getElementById("pieAvgInspectionTimePerAgentPerDay").getContext('2d');
                //setup the config options
                var config = {
                    type: 'pie',
                    data: {
                        labels: arrLabels,
                        datasets: [{
                            backgroundColor: arrColors,
                            data: arrData
                        }]
                    }
                };
                //render the chart
                var myChart = new Chart(ctx, config);
            }
        };






        //var dynamicColors = function () {
        //    var r = Math.floor(Math.random() * 255);
        //    var g = Math.floor(Math.random() * 255);
        //    var b = Math.floor(Math.random() * 255);
        //    return "rgb(" + r + "," + g + "," + b + ")";
        //}







    </script>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

