﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_management_turn_back_metrics : System.Web.UI.Page
{


    RzDataContext rdc = new RzDataContext();

    private class TurnBackObj
    {
        public string tb_agent { get; set; }
        public string tb_inspector { get; set; } //Person who created teh turn back 
        public DateTime date_created { get; set; }
        public int MyProperty { get; set; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }

    private void LoadData()
    {

        var query = rdc.qc_turn_backs.Where(w => w.inspection_agent != "Kevin Till").Select(s => new
        {
            TurnBackAgent = s.turn_back_agent,
            InspectionAgent = s.inspection_agent,
            Date = s.turn_back_date.Value.Date,
            Resolution = s.turn_back_resolution_date.Value.Date,
            Description = s.turn_back_notes,
            ResolvedDate = s.turn_back_resolution_date.Value.Date,
            ResolveTime = "N/A"
        }).OrderByDescending(o => o.Date);



        //if (s.ResolvedDate != null)
        //{
        //    TimeSpan span = s.ResolvedDate - s.Date;
        //    span.TotalMinutes.ToString();
        //}

        //foreach (var v in query)
        //{


        //    v.Resolution
        //}


        smdt.dataSource = query;
        smdt.loadGrid(true);
    }
}