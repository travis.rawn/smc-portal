﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_sales_aging_sales : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();
    RzDataContext rdc = new RzDataContext();
    SM_Charts smr = new SM_Charts();
    List<orddet_line> AgingLines;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            CompleteLoad();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void CompleteLoad()
    {
        LoadDates();
        LoadAgentPicker();
        LoadAgingSalesData();
        if (AgingLines.Count > 0)
        {
            LoadAgingSalesGrid();

        }


    }

    private void LoadAgingSalesData()
    {
        smdt.dataSource = null;
        smdt.loadGrid();
        lblCount.Text = "0";
        lblGP.Text = "$0.00";

        AgingLines = rdc.orddet_lines.Where(w => w.customer_dock_date.Value >= dtpStart.SelectedDate.Value && w.orderid_sales.Length > 0 && !SalesLogic.CompletedStatus.Contains(w.status) && !w.ordernumber_sales.ToLower().Contains("cr")).ToList();
        List<n_user> agentList = rzap.GetSelectedUsers();
        List<string> agentIds = agentList.Select(s => s.unique_id).ToList();

        if (agentIds.Count > 0)
        {


            {
               
                    AgingLines = AgingLines.Where(w => agentIds.Contains(w.seller_uid)).ToList();

            }
        }
        else if (Roles.IsUserInRole(SM_Enums.PortalRole.sm_internal.ToString()))
            AgingLines = AgingLines.Where(w => w.seller_uid == Profile.RzUserID).ToList();
        else


        if (cbxHideFutureDockDates.Checked)
            AgingLines = AgingLines.Where(w => w.customer_dock_date < DateTime.Now).ToList();

    }

    private void LoadAgentPicker()
    {
        try
        {
            List<string> ManagementRoles = new List<string>() { SM_Enums.PortalRole.admin.ToString(), SM_Enums.PortalRole.sm_internal_executive.ToString(), SM_Enums.PortalRole.portal_admin.ToString() };
            var userRoles = Roles.GetRolesForUser();

            if (!userRoles.Any(a => ManagementRoles.Contains(a)))
            {
                rzap.Visible = false;
                lblAgentName.Visible = true;
                lblAgentName.Text = Profile.FirstName + " " + Profile.LastName;
            }
            else
            {
                rzap.Visible = true;
                lblAgentName.Visible = false;
                List<string> includedTeams = new List<string>() { "Sales", "Distributor Sales" };  
                rzap.LoadPicker(includedTeams);
                rzap.SelectedUserID = "all";
               
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }


    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Now.AddDays(-60);
        }
    }


    private void LoadAgingSalesGrid()
    {

        AgingLines = AgingLines.OrderByDescending(o => o.gross_profit).ToList();
        lblCount.Text = AgingLines.Count + " lines";
        lblGP.Text = AgingLines.Sum(s => s.gross_profit).Value.ToString("C");
        var query = AgingLines.Select(s => new
        {
            Sale = s.ordernumber_sales,
            SaleDate = s.date_created.Value.ToString("MM/dd/yyyy"),
            Customer = s.customer_name,
            Vendor = s.vendor_name,
            Dock = s.customer_dock_date.Value.ToString("MM/dd/yyyy"),
            Status = s.status,
            QTY = s.quantity,
            GP = s.gross_profit.Value.ToString("C"),
            Agent = s.seller_name
        }
        );
        smdt.dataSource = query;
        smdt.loadGrid();




    }

}
