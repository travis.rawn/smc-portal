﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_calls_to_sales_report : System.Web.UI.Page
{
    List<n_user> agentList = new List<n_user>();
    List<ordhed_invoice> invoiceList = new List<ordhed_invoice>();
    List<hubspot_engagement> engList = new List<hubspot_engagement>();
    List<AgentEngagementObject> aeoList = new List<AgentEngagementObject>();
    DateTime startDate;
    DateTime endDate;
    RzDataContext rdc = new RzDataContext();
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();



    [Serializable]
    protected class AgentEngagementObject
    {
        public string agentName { get; set; }
        public double totalGP { get; set; }
        public long totalCalls { get; set; }
        public long connectedCalls { get; set; }
        public decimal callValue { get; set; }
        public double monthlyGoal { get; set; }
        public decimal CallsNeededPerDay { get; set; }
        public decimal CallsNeededPerMonth { get; set; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        LoadAgents();
        LoadDates();
        LoadData();
        //LoadCharts();
    }

    private void LoadAgents()
    {
        //agentList = rdc.n_users.Where(w => w.name == "Jon Kazle").ToList();
        agentList = rzt.GetUsersForTeams(new List<string>() { "sales" });
        List<string> excludedAgents = new List<string>() { "Chris Torrioni", "Roberto Hernandez", "Larry McDonald" };
        agentList = agentList.Where(w => !excludedAgents.Contains(w.name)).ToList();
        if (!Page.IsPostBack)
            rzap.LoadPicker(agentList);


    }

    private void LoadCharts()
    {

        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();


        //Total Calls DataSet
        SM_Charts.chartJsDataSet cdsTotalCalls = new SM_Charts.chartJsDataSet();
        cdsTotalCalls.data = aeoList.Select(s => s.totalCalls.ToString()).ToList();
        cdsTotalCalls.dataSetLabel = "Total Calls";
        cdsTotalCalls.fillColors = false;
        //cdsTotalCalls.borderColor = new List<string>() { "#000000" };
        datasetList.Add(cdsTotalCalls);

        //GP DataSet
        SM_Charts.chartJsDataSet cdsGP = new SM_Charts.chartJsDataSet();
        cdsGP.data = aeoList.Select(s => s.totalGP.ToString()).ToList();
        cdsGP.chartType = SM_Enums.ChartType.line.ToString();
        cdsGP.dataSetLabel = "Total GP";
        cdsGP.fillColors = false;
        //cdsGP.borderColor = new List<string>() { "#000000" };
        datasetList.Add(cdsGP);


        //The Chart
        SM_Charts.ChartJS chtAgentEngagementChart = new SM_Charts.ChartJS();
        chtAgentEngagementChart.chartType = SM_Enums.ChartType.bar.ToString();
        chtAgentEngagementChart.stacked = true;
        chtAgentEngagementChart.labels = aeoList.Select(s => s.agentName).ToList();
        chtAgentEngagementChart.dataSetList = datasetList;
        chtAgentEngagementChart.displayXaxisLabel = true;
        chtAgentEngagementChart.displayYaxisLabel = true;
        chtAgentEngagementChart.xAxisLabelAutoskip = false;
        chtAgentEngagementChart.controlPrefix = sm_chartjs.ClientID; //required if using sm_chartjs
        chtAgentEngagementChart.canvasID = "cvEngagementActivity";
        chtAgentEngagementChart.titleText = "Chart: Total Calls";
        chtAgentEngagementChart.informationalText = "Call activity vs total GP";
        chtAgentEngagementChart.canvasHeight = 90;
        //Load all the chart data
        SM_Charts smr = new SM_Charts();
        List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();
        //Load the html elements of the control (labels, etc)
        sm_chartjs.LoadChart(chtAgentEngagementChart);
        //Add chart to master chart list.  
        ChartList.Add(chtAgentEngagementChart);
        //Actuall load
        smr.LoadAllCharts(Page, ChartList);

    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = new DateTime(2020, 01, 01);
            dtpEnd.SelectedDate = DateTime.Today;
        }

        startDate = dtpStart.SelectedDate.Value;
        endDate = dtpEnd.SelectedDate.Value;

    }

    private void LoadData()
    {



        List<string> agentIds = agentList.Select(s => s.unique_id).ToList();
        if (rzap.SelectedUserID.ToLower() != "choose" && rzap.SelectedUserID.ToLower() != "all")
        {
            agentIds.Clear();
            agentIds.Add(rzap.SelectedUserID);
        }

        //agentList.Select(s => s.unique_id).Distinct().ToList();
        invoiceList = rdc.ordhed_invoices.Where(w => (w.date_created.Value.Date >= startDate && w.date_created.Value.Date <= endDate) && agentIds.Contains(w.base_mc_user_uid)).ToList();
        engList = rdc.hubspot_engagements.Where(w => (w.date_created.Value.Date >= startDate && w.date_created.Value.Date <= endDate) && w.type == "call").ToList();


        foreach (string s in agentIds)
        {
            n_user u = agentList.Where(w => w.unique_id == s).FirstOrDefault();
            List<hubspot_engagement> agentEngList = engList.Where(w => w.base_mc_user_uid == u.unique_id).ToList();
            List<ordhed_invoice> agentInvoiceList = invoiceList.Where(w => w.base_mc_user_uid == u.unique_id).ToList();
            AgentEngagementObject aeo = new AgentEngagementObject();
            aeo.agentName = u.name;
            double totalGP = 0;
            totalGP = agentInvoiceList.Where(w => w.base_mc_user_uid == u.unique_id).Sum(ss => ss.gross_profit) ?? 0;
            aeo.totalGP = totalGP;
            long totalCalls = 0;
            totalCalls = agentEngList.Count();
            aeo.totalCalls = totalCalls;
            long connectedCalls = 0;
            connectedCalls = agentEngList.Where(w => w.call_disposition == "Connected").Count();
            aeo.connectedCalls = connectedCalls;
            aeo.CallsNeededPerMonth = 0;
            aeo.CallsNeededPerDay = 0;
            if (totalCalls <= 0)
                aeo.callValue = 0;
            else
            {
                aeo.callValue = (decimal)(totalGP / totalCalls);
                if (aeo.callValue > 0)
                {
                    aeo.CallsNeededPerMonth = (decimal)((double)u.monthly_booking_goal / (double)aeo.callValue);
                    if (aeo.CallsNeededPerMonth > 0)
                    {
                        //Round if value
                        aeo.CallsNeededPerMonth = Math.Round(aeo.CallsNeededPerMonth, 0, MidpointRounding.AwayFromZero);
                        aeo.CallsNeededPerDay = Math.Round(aeo.CallsNeededPerMonth / 20, 0, MidpointRounding.AwayFromZero);

                    }

                }

                aeoList.Add(aeo);
                aeo.monthlyGoal = u.monthly_booking_goal ?? 0;


            }
            ViewState["aeoList"] = aeoList;

            var gridQuery = aeoList.Select(ss => new
            {
                Agent = ss.agentName,
                TotalCalls = ss.totalCalls,
                TotalGP = ss.totalGP.ToString("C"),
                TotalConnected = ss.connectedCalls,
                Callvalue = Math.Round(ss.callValue, 2, MidpointRounding.AwayFromZero).ToString("C"),
                MonthlyGoal = ss.monthlyGoal.ToString("C"),
                ss.CallsNeededPerDay,
                ss.CallsNeededPerMonth




            });

            smdt.dataSource = gridQuery.OrderBy(o => o.Agent);
            smdt.loadGrid();
        }
    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        aeoList = (List<AgentEngagementObject>)ViewState["aeoList"];
        tools.ExportListToCsv(aeoList, "export.csv");
    }
}