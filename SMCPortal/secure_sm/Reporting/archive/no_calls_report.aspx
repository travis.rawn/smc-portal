﻿<%@ Page Title="No Calls Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="no_calls_report.aspx.cs" Inherits="no_calls_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <meta http-equiv="refresh" content="60">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" Runat="Server">
       <div id="pageTitle">
        <h1>No Calls Report</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="card">
        <asp:LinkButton ID="lbRefresh" runat="server" OnClick="lbRefresh_Click" CssClass="btn-smc btn-smc-primary">Refresh</asp:LinkButton>
        <asp:LinkButton ID="lbTestEmail" runat="server" OnClick="lbTestEmail_Click" CssClass="btn-smc btn-smc-primary">Test Email</asp:LinkButton>
    </div>
    <uc1:sm_datatable runat="server" ID="smdtNoCalls" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" Runat="Server">
</asp:Content>

