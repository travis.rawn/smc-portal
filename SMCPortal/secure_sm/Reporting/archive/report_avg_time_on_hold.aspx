﻿<%@ Page Title="Average Time On Hold" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="report_avg_time_on_hold.aspx.cs" Inherits="secure_sm_Reporting_quality_report_avg_time_on_hold" %>

<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div id="pageTitle">
        <h1>Average Time On Hold</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-default card">
        <div class="panel-heading">

            <div class="form-inline">
                <label>
                    <asp:Label ID="lblCount" runat="server"></asp:Label></label>
                | 
            <label>
                <asp:Label ID="lblTotalTimeOnHold" runat="server"></asp:Label></label>
                |
            <label>
                <asp:Label ID="lblAvg" runat="server"></asp:Label></label>
            </div>
            <div class="form-inline">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
                <asp:DropDownList ID="ddlHoldType" runat="server" CssClass="form-control">
                    <asp:ListItem Text="All" Value="all"></asp:ListItem>
                    <asp:ListItem Text="Validation Hold" Value="validation"></asp:ListItem>
                    <asp:ListItem Text="Inspection Hold" Value="inspection"></asp:ListItem>
                     <asp:ListItem Text="Resolved" Value="resolved"></asp:ListItem>
                    <asp:ListItem Text="Un-resolved" Value="unresolved"></asp:ListItem>
                </asp:DropDownList>
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()">
                             <span class="fa fa-search" style="font-size:18px;"></span>
                </asp:LinkButton>
                <asp:LinkButton ID="lbClear" runat="server" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()" OnClick="lbClear_Click">
                             <span class="fa fa-times" style="font-size:18px;"></span>
                </asp:LinkButton>
            </div>
        </div>
        <div class="panel-body">
            <div class="card card-info">
                <div class="row">
                    <div>
                        <div class="col-sm-8">
                            <strong>Information:</strong>
                            <p>
                                This breaks down our average time on hold by month.  Use the dropdown to filter by specific hold types.
                            </p>
                            <em>We went live with validation tracking in mid-march 2017.
                            </em>
                        </div>
                        <div class="col-sm-4">
                            <ul>
                                <li><strong>Validtaion Hold</strong> - <em>Placed on hold due to some infomration or clerical issue.</em> </li>
                                <li><strong>Inspection Hold</strong> -<em>Placed on hold due to inspection / quality issue.</em> </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <canvas id="cvAverageTimeOnHold" height="90"></canvas>
            </div>
            <a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
            <div id="divDetails" style="display: none;">
                <asp:GridView ID="gvAverageTimeOnHold" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None" EmptyDataText="No Data Found." AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="holdType" HeaderText="Type"/>
                        <asp:BoundField DataField="companyName" HeaderText="Company" HeaderStyle-Width="20%" />
                        <asp:BoundField DataField="orderNumber" HeaderText="SO#" />
                        <asp:BoundField DataField="hold_date" HeaderText="Hold Date" DataFormatString="{0:d}" />
                         <asp:BoundField DataField="lastStage" HeaderText="Stage" />
                        <asp:BoundField DataField="Resolved" HeaderText="Resolved" />
                         <asp:BoundField DataField="ResDate" HeaderText="ResDate" />
                        <asp:BoundField DataField="totalMinutes" HeaderText="Minutes" />
                        <asp:BoundField DataField="holdReason" HeaderText="Hold Reason" HeaderStyle-Width="38%" />
                    </Columns>
                </asp:GridView>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

