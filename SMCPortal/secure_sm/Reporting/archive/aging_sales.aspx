﻿<%@ Page Title="Aging Sales Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="aging_sales.aspx.cs" Inherits="secure_sm_Reporting_sales_aging_sales" %>

<%@ Register Src="~/assets/controls/RzAgentPicker.ascx" TagPrefix="uc1" TagName="RzAgentPicker" %>
<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>





<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h3>Aging Sales</h3>
    <div class="well well-sm">
        The following sales orders are incomplete, and not voided.
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="panel panel-default card">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3">
                    <label>Agent:</label>
                </div>
                <div class="col-xs-2">
                    <label>Sale Date:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <uc1:RzAgentPicker class="control-label" runat="server" ID="rzap" Visible="false"/>
                    <h4>
                        <asp:Label runat="server" ID="lblAgentName" Visible="false"></asp:Label>
                    </h4>
                    
                </div>
                <div class="col-xs-2">
                    
                    <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
                </div>
                <div class="col-xs-2">
                    <div class="checkbox">
                        <label>
                            <asp:CheckBox runat="server" ID="cbxHideFutureDockDates" Checked="true" AutoPostBack="true" TextAlign="Left" OnClick="ShowUpdateProgress();" />Hide Future Docks</label>
                    </div>
                </div>
                <div class="col-xs-2">
                </div>
                <div class="col-xs-2">
                </div>
                <div class="col-xs-1">
                    <button class="btn-smc btn-smc-primary" title="Search">
                        <i class="fa fa-search" style="font-size:25px;" onclick="ShowUpdateProgress();"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div ID=pnlclass="panel panel-default card">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <div id="divAgingSales">
                        <div class="row">
                            <div class="col-sm-3">
                                <h3>Count:
                                    <asp:Label ID="lblCount" runat="server" Style="margin-right: 15px;"></asp:Label>
                                </h3>
                            </div>
                            <div class="col-sm-3">

                                <h3>GP:
                                    <asp:Label ID="lblGP" runat="server" Style="margin-right: 15px;"></asp:Label>
                                </h3>
                            </div>
                            <div class="col-sm-3">
                            </div>
                        </div>
                        <uc1:sm_datatable runat="server" ID="smdt" />
                    </div>
                </div>
            </div>


        </div>
    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

