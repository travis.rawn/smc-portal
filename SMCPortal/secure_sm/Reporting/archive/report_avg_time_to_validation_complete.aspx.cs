﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_quality_report_avg_time_to_validation_complete : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    RzDataContext rdc = new RzDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        GetAllValidationTrackingMinMax();
    }

    private void GetAllValidationTrackingMinMax()
    {

        //Tricky - since ordernumbers change, I need to join orddet with ordhed_sales and get the latter's ordernumber
        //HOWEVER, need to call the join AFTER the group, and use FirstOrDefault() on the foreing key link
        //that way, I can reference s as well, else I'd only be able to reference "g" 
        var query = (from q in rzt.Validation_tracking_AverageTimetoValidationComplete()
                     group q by q.orderid_sales into g
                     join s in rdc.ordhed_sales on g.FirstOrDefault().orderid_sales equals s.unique_id
                     where !s.ordernumber.ToLower().Contains("cr") && s.isvoid != true
                     select new
                     {
                         //orderid_sales = g.Key,
                         Inspector = g.Max(z => z.agentname),
                         SO = s.ordernumber,
                         Company = g.Max(z => z.companyname),
                         Stage = g.Max(z => z.new_stage),
                         Min = g.Min(z => z.date_created),
                         End = g.Max(z => z.new_stage) != "ValidationComplete" ? DateTime.Now : g.Max(z => z.date_created),
                         Hours = (g.Max(z => z.date_created) == g.Min(z => z.date_created) && g.Max(z => z.new_stage == "ValidationComplete"))
                         ? "0" //If the min max dates are the same, and validation is complete, then "0" hours to complete
                         : g.Max(z => z.new_stage) != "ValidationComplete"
                         ? ((DateTime.Now - (DateTime)g.Min(z => z.date_created)).TotalMinutes / 60).ToString("#.##") //If the min max dates are not the same, and validation is NOT complete, then clock is still running, so compae to datetime.now 
                         : (((DateTime)g.Max(z => z.date_created) - (DateTime)g.Min(z => z.date_created)).TotalMinutes / 60).ToString("#.##")
                     }).OrderByDescending(o => o.Hours);
        int count = query.Count();
        string strHours = query.Select(s => s.Hours).FirstOrDefault() ?? "0";
        
        decimal totalTime = 0;
        if (strHours != "0")
            totalTime =  Convert.ToDecimal(strHours);
        decimal avg = totalTime / count;

        lblCount.Text = "Count: " + count.ToString() + " results.";
        lblAvg.Text = "Average Days: " + (avg / 24).ToString("#.##") + " Days";
        gvAverageTimeValidationComplete.DataSource = query;
        gvAverageTimeValidationComplete.DataBind();
        gvAverageTimeValidationComplete.HeaderRow.TableSection = TableRowSection.TableHeader;

        var query2 = query.GroupBy(i => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear((DateTime)i.End, CalendarWeekRule.FirstDay, DayOfWeek.Monday));

        var x = from F in query
                group F by new
                {
                    //Date = F.End.Value,
                    Year = F.End.Value.Year,                   
                    Month = F.End.Value.Month

                } into FGroup
                orderby FGroup.Key.Year, FGroup.Key.Month
                select new
                {
                    //Date = FGroup.Key.Date,
                    Year = FGroup.Key.Year,                   
                    Month = FGroup.Key.Month,
                    Count = FGroup.Count()
                };


        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();


        SM_Charts.chartJsDataSet cDataSet3 = new SM_Charts.chartJsDataSet();
        cDataSet3.data = x.Select(s => s.Count.ToString()).ToList();
        cDataSet3.chartType = SM_Enums.ChartType.line.ToString();
        cDataSet3.dataSetLabel = "Avg Hours";
        cDataSet3.fillColors = false;
        datasetList.Add(cDataSet3);






        //Setup the Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = SM_Enums.ChartType.bar.ToString();
        Chart1.labels = x.Select(s => CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(s.Month) + " "+ s.Year.ToString()).ToList();
        Chart1.dataSetList = datasetList;
        Chart1.canvasID = "cvAvgValidationTime";
        //Chart1.toolTipMode = "index";
        //Chart1.tooltipIntersect = true;
        SM_Charts smr = new SM_Charts();
        smr.LoadAllCharts(Page, new List<SM_Charts.ChartJS>() { Chart1 });

    }
}