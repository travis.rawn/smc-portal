﻿<%@ Page Title="Average Time Validation Complete" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="report_avg_time_to_validation_complete.aspx.cs" Inherits="secure_sm_Reporting_quality_report_avg_time_to_validation_complete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Average Time to Validation Complete</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-default card">
        <div class="panel-heading">
            <label>
                <asp:Label ID="lblCount" runat="server"></asp:Label></label>
            <label>
                <asp:Label ID="lblAvg" runat="server"></asp:Label></label>

        </div>
        <div class="panel-body card">
            <div class="card">
                <canvas id="cvAvgValidationTime" height="50"></canvas>      
               <a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
                 <div id="divDetails" style="display: none;">
                    <asp:GridView ID="gvAverageTimeValidationComplete" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None" EmptyDataText="No Data Found."></asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

