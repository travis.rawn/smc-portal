﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_sales_stock_consignment_sales_report : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    DateTime StartDate;
    DateTime EndDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                LoadDatePickers();
                ddlStockType.SelectedValue = "all";
            }
            else
                GetGridData_bak();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void LoadDatePickers()
    {
        //dtpStart.placeholderText = "Start Date";
        //dtpEnd.placeholderText = "End Date";
        //dtpStart.SelectedDate = new DateTime(2017, 3, 1);
        //dtpEnd.SelectedDate = DateTime.Now;
    }

    //private void GetGridData()
    //{
    //    //        --Use this list to find shipped stock that has inventory link uid matching any lines form the po stock buys
    //    //select ordernumber, unique_id, is_Stock, agentname from ordhed_purchase where unique_id in (
    //    //select--purchase_line_uid, 
    //    //the_ordhed_purchase_uid From shipped_stock where stocktype = 'stock' and len(isnull(the_ordhed_purchase_uid, '')) > 0
    //    // --where original_unique_id
    //    // ) and is_stock = 1

    //    smdt.Clear();
    //    StartDate = dtpStart.SelectedDate.Value;
    //    EndDate = dtpEnd.SelectedDate.Value;

    //    List<ordhed_purchase> stockPOList = new List<ordhed_purchase>();
    //    List<shipped_stock> shippedStockList = new List<shipped_stock>();
    //    List<orddet_line> stockLinesList = new List<orddet_line>();
    //    //When po lines are put away, the inventory line gets stamped wtih "the_ordhed_purchase_id" and stocktype is stock
    //    using (RzDataContext rdc = new RzDataContext())
    //    {
    //        //Gather up all the Stock Purchase Ids from Shipped Stock (excludes unsold stock obvs)
    //        List<String> listStockTypes = new List<String>() { "stock", "consign" };
    //        shippedStockList = rdc.shipped_stocks.Where(w => w.the_ordhed_purchase_uid.Length > 0 && listStockTypes.Contains(w.stocktype.ToLower())).ToList();
    //        List<string> shippedStockPurchaseIdList = new List<string>();
    //        shippedStockPurchaseIdList = shippedStockList.Where(w => (w.the_ordhed_purchase_uid ?? "").Length > 0).Select(s => s.the_ordhed_purchase_uid).Distinct().ToList();
    //        //Now get all ordhed_purchase that match these IDs and are is_stock = true             
    //        stockPOList = rdc.ordhed_purchases.Where(w => w.is_stock ?? false == true && shippedStockPurchaseIdList.Contains(w.unique_id)).Distinct().ToList();

    //        //shipped stock inventory ID list
    //        List<string> shippedStockInventoryIdList = new List<string>();
    //        //shippedStockInventoryIdList = shippedStockList.Select(s => s.)
    //        //stockLinesList = rdc.orddet_lines.Where(w => w.inventory_link_uid)
    //    }

    //    double totalStockPOCost = stockPOList.Sum(s => s.costamount ?? 0);
    //    //double totalStockSales = shippedStockList.Sum(s => s.)


    //    //Data We Need
    //    //Total Spend Per PO
    //    //Total Profit


    //}
    private void GetGridData_bak()
    {
        //smdt.Clear();
        //StartDate = dtpStart.SelectedDate.Value;
        //EndDate = dtpEnd.SelectedDate.Value;

        //RzDataContext RDC = new RzDataContext();
        //RzTools rzt = new RzTools();
        //List<string> stockTypes = new List<string>() { SM_Enums.StockType.stock.ToString().ToLower(), SM_Enums.StockType.consign.ToString().ToLower() };
        //List<orddet_line> stockSalesLines = RDC.orddet_lines.Where(w => w.total_price > 0 && w.orderid_invoice.Length > 0 && (w.orderdate_sales >= StartDate && w.orderdate_sales <= EndDate) && GetSelectedStockTypes().Contains(w.stocktype.ToLower()) && !LineData.GetInvalid_orddet_Status(true, true).Contains(w.status)).ToList();

        //if (!string.IsNullOrEmpty(rzca.CompanyID))
        //    stockSalesLines = stockSalesLines.Where(w => w.vendor_uid == rzca.CompanyID).ToList();

        ////In order to get the source PO number, I need to refer to the stock / consign / shipped stock item to find the purchase order id.

        //List<string> inventoryIds = stockSalesLines.Where(w => (w.inventory_link_uid ?? "").Length > 0).Select(s => s.inventory_link_uid).Distinct().ToList();
        //List<string> inStockPOIds = new List<string>();
        ////List<string> consignmentPOIDs = new List<string>();
        //List<shipped_stock> shippedStockList = new List<shipped_stock>();

        //List<string> shippedStockPoIds = new List<string>();
        //inStockPOIds = RDC.partrecords.Where(w => inventoryIds.Contains(w.unique_id)).Where(w => (w.buy_purchase_id ?? "").Length > 0).Select(s => s.buy_purchase_id).ToList();
        ////consignmentPOIDs = RDC.partrecords.Where(w => inventoryIds.Contains(w.unique_id)).Where(w => (w.buy_purchase_id ?? "").Length > 0).Select(s => s.buy_purchase_id).ToList();
        //shippedStockPoIds = RDC.shipped_stocks.Where(w => inventoryIds.Contains(w.original_unique_id)).Where(w => (w.buy_purchase_id ?? "").Length > 0).Select(s => s.buy_purchase_id).ToList();
        //List<string> allInventoryPurchaseIds = inStockPOIds.Union(shippedStockPoIds).ToList();

        //List<ordhed_purchase> stockPOList = RDC.ordhed_purchases.Where(w => allInventoryPurchaseIds.Contains(w.unique_id)).ToList();


        //if (stockSalesLines.Count > 0)
        //{
        //    var query = stockSalesLines.Select(s => new
        //    {
        //        PartNumber = s.fullpartnumber.ToUpper(),
        //        Manufacturer = s.manufacturer,
        //        QTY = s.quantity,
        //        Date = s.orderdate_invoice.Value.ToString("MM/dd/yyyy"),
        //        Agent = s.seller_name,
        //        Type = s.stocktype,
        //        Vendor = s.vendor_name,
        //        PO = GetPONumber(s, stockPOList) ?? "?",
        //        Invoice = s.ordernumber_invoice,
        //        Total = s.total_price.Value.ToString("C")
        //    });




        //    smdt.dataSource = query;
        //    //gvStockConsignSales.DataBind();
        //    //gvStockConsignSales.HeaderRow.TableSection = TableRowSection.TableHeader;
        //    smdt.loadGrid();
        //    //Labels
        //    lblTotalCount.Text = "Results: " + stockSalesLines.Count().ToString();
        //    lblTotalSales.Text = "Total: " + stockSalesLines.Sum(s => s.total_price).Value.ToString("C");
        //    pnlResults.Visible = true;
        //}
        //else
        //    pnlResults.Visible = false;

    }

    private string GetPONumber(orddet_line stockSaleLine, List<ordhed_purchase> stockPOList)
    {
        ordhed_purchase p = stockPOList.Where(w => w.unique_id == stockSaleLine.orderid_purchase).FirstOrDefault();
        if (p != null)
            return p.ordernumber ?? "";
        return null;
    }

    private List<string> GetSelectedStockTypes()
    {
        List<string> ret = new List<string>();
        switch (ddlStockType.SelectedValue)
        {
            case "stock":
                {
                    ret.Add("stock");
                    break;
                }
            case "consign":
                {
                    ret.Add("consign");
                    break;
                }
            default:
                {
                    ret.Add("stock");
                    ret.Add("consign");
                    break;
                }
        }
        return ret;
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        GetGridData_bak();
    }

    protected void lbClear_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl, false);
    }

}