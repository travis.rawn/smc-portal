﻿<%@ Page Title="Order Timing Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="report_order_timing.aspx.cs" Inherits="secure_sm_Reports_report_time_sale_to_invoice" %>

<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Order Timing Report</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="panel panel-default">
        <div class="panel panel-heading">
             <div class="row">
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="lbSearch">
                    <div class="col-sm-12">
                        <div class="row" style="padding-bottom: 8px;">
                            <div class="col-sm-6">
                                <label>Current Range</label>
                                <div class="form-inline">
                                    <label class="sr-only" for="inlineFormInput">Start</label>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpCurrentStart" />
                                    <label class="sr-only" for="inlineFormInput">End</label>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpCurrentEnd" />
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <label>Previous Range</label>
                                <div class="form-inline">
                                    <label class="sr-only" for="inlineFormInput">Start</label>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpPrevStart" />
                                    <label class="sr-only" for="inlineFormInput">End</label>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpPrevEnd" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-inline">
                                    <asp:DropDownList ID="ddlReportType" runat="server" CssClass="form-control" Style="max-width: 200px;">
                                        <asp:ListItem Text="Sale -> Invoice" Value="sToi"></asp:ListItem>
                                        <asp:ListItem Text="Sale -> Purchase" Value="sTop"></asp:ListItem>
                                        <asp:ListItem Text="Purchase -> Invoice" Value="pToi"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinkButton ID="lbSearch" runat="server" OnClick="lbSearch_Click" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress();"> 
                            <span class="fa fa-search"></span>
                                    </asp:LinkButton>
                                    <label>Current Report:</label><asp:Label ID="lblCurrentType" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="panel panel-body">
           
            <div class="row">               
                <div class="col-sm-6">
                    <label>Current Results</label>
                    Total Sales:
                    <asp:Label ID="lblCurrentSaleCount" runat="server"></asp:Label>
                    Total Days:
                    <asp:Label ID="lblCurrentTotalDays" runat="server"></asp:Label>
                    Avg Days
                    <asp:Label ID="lblCurrentAvgDays" runat="server"></asp:Label>
                    <asp:GridView ID="gvCurrent" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None" EmptyDataText="No Data Found."></asp:GridView>
                </div>
                <div class="col-sm-6">
                    <label>Previous Results</label>
                    Count:
                    <asp:Label ID="lblPreviousSaleCount" runat="server"></asp:Label>
                    Total Days:
                    <asp:Label ID="lblPreviousTotalDays" runat="server"></asp:Label>
                    Avg Days
                    <asp:Label ID="lblPreviousAvgDays" runat="server"></asp:Label>
                    <asp:GridView ID="gvPrevious" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None" EmptyDataText="No Data Found."></asp:GridView>

                </div>
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

