﻿<%@ Page Title="Stock Sales Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="stock_consignment_sales_report.aspx.cs" Inherits="secure_sm_Reporting_sales_stock_consignment_sales_report" %>

<%@ Register Src="~/assets/controls/rz_company_autocomplete.ascx" TagPrefix="uc1" TagName="rz_company_autocomplete" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Stock & Consignment Sales</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="lbSearch">
        <div class="row">
            <div class="col-sm-2">
                <asp:DropDownList ID="ddlStockType" runat="server" CssClass="form-control">
                    <asp:ListItem Text="All" Value="all"></asp:ListItem>
                    <asp:ListItem Text="Stock" Value="stock"></asp:ListItem>
                    <asp:ListItem Text="Consign" Value="consign"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-4">
                <uc1:rz_company_autocomplete runat="server" ID="rzca" CssClass="form-control" />
            </div>

            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>

            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()">
                             <span class="fa fa-search" style="font-size:18px;"></span>
                </asp:LinkButton>
            </div>
            <div class="col-sm-3">
                <asp:LinkButton ID="lbClear" runat="server" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()" OnClick="lbClear_Click">
                             <span class="fa fa-times" style="font-size:18px;"></span>
                </asp:LinkButton>
            </div>
        </div>

    </asp:Panel>
    <asp:Panel ID="pnlResults" runat="server" Visible="false">
        <div class="panel">
            <div class="panel panel-heading">
                <div class="row">
                    <div class="col-sm-12">
                    </div>
                </div>
                <div class="row" style="padding-top: 10px;">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <h4>
                            <asp:Label ID="lblTotalCount" runat="server"></asp:Label>
                        </h4>
                    </div>
                    <div class="col-sm-3">
                        <h4>
                            <asp:Label ID="lblTotalSales" runat="server"></asp:Label>
                        </h4>
                    </div>
                    <div class="col-sm-3"></div>
                </div>


            </div>
            <div class="panel panel-body">
                <%-- <asp:GridView ID="gvStockConsignSales" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None" EmptyDataText="No Data Found."></asp:GridView>--%>
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

