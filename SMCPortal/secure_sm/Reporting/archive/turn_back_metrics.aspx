﻿<%@ Page Title="Turn Back Metrics Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="turn_back_metrics.aspx.cs" Inherits="secure_sm_Reporting_quality_turn_back_metrics" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
     <div style="text-align: center; font-size: 40px;">
        Turn Back Metrics
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="card">
        <uc1:sm_datatable runat="server" ID="smdtTurnBackMetrics" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" Runat="Server">
</asp:Content>

