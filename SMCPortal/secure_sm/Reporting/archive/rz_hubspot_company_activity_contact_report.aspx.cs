﻿using SensibleDAL;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_marketing_rz_hubspot_company_activity_contact_report : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();

    protected DataTable DTResults
    {
        get
        {
            if (ViewState["DTResults"] != null)
                return (DataTable)(ViewState["DTResults"]);
            else
                return null;
        }
        set
        {
            ViewState["DTResults"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)//Only fire after user clicks search, i.e. causes a postback.
        {
            smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
            //Data Loading NEEDS to be after the eventhandler add, i.e. not in a _click event handler, but on Page load.
            LoadData();
        }
        else
        {
            LoadDates();
        }

    }

    private void LoadDates()
    {
        dtp.SelectedDate = DateTime.Now.AddDays(-30);
    }

    private void LoadData()
    {
        DateTime startDate;
        DTResults = MarketingLogic.LoadRzHubspotCompanyActivity(dtp.SelectedDate.Value);
        var query = DTResults.AsEnumerable().Select(s => new
        {
            RzCompany = s.Field<string>("RzCompanyName"),
            RzSales = s.Field<string>("RzTotalSales"),// != "Unknown" ? Convert.ToDouble(s.Field<string>("RzTotalSales")).ToString("C") : "Unknown", 
            RzIndustry = s.Field<string>("RzIndustrySegment"),
            AnnualRevenue = s.Field<string>("HubspotAnnualRevenue"),//!= "Unknown" ? Convert.ToDouble(s.Field<string>("HubspotAnnualRevenue")).ToString("C") : "Unknown",
            RzAgent = s.Field<string>("RzAgentName"),
            HubspotOwner = s.Field<string>("HubspotOwnerName"),
            HubspotLast = s.Field<string>("HubspotLastContactDate"),
            LastBatch = s.Field<string>("RzLastBatchDate"),
            LastQuote = s.Field<string>("RzLastQuoteDate"),
            LastInvoice = s.Field<string>("RzLastInvoiceDate"),
            Website = s.Field<string>("companyWebSite"),
            HubspotUrl = s.Field<string>("HubspotCompanyUrl")
        });
        smdt.dataSource = query;
        smdt.loadGrid();
    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {



        //THis is the ROW, not the GV, and think this get set before Columns are present, so can't uyse them.
        GridView gv = smdt.theGridView;
        //Hide the header cell
        if (gv.HeaderRow != null)
            gv.HeaderRow.Cells[11].CssClass = "hiddencol";

        GridViewRow row = e.Row;
        
        if (row.RowType == DataControlRowType.DataRow)
        {
            string companyName = row.Cells[0].Text;
            string hsUrl = row.Cells[11].Text;
            if (hsUrl != "No HS Link")
            {
                //row.Cells[0].Text = "";
                HyperLink hlCompany = new HyperLink();
                hlCompany.NavigateUrl = hsUrl;
                hlCompany.Target = "_blank";
                hlCompany.Text = companyName;
                row.Cells[0].Controls.Add(hlCompany);
            }

            string webSite = row.Cells[10].Text;
            if (webSite != "Unknown")
            {
                //row.Cells[0].Text = "";
                HyperLink hlRzUrl = new HyperLink();
                hlRzUrl.NavigateUrl = "http://"+webSite;
                hlRzUrl.Target = "_blank";
                hlRzUrl.Text = webSite;
                row.Cells[10].Controls.Add(hlRzUrl);
            }
            //Hide the td cell
            row.Cells[11].CssClass = "hiddencol";

        }

        //foreach (GridViewRow r in smdt.theGridView.Rows)
        //{
        //    string companyName = e.Row.Cells[0].Text;
        //    string companyUrl = e.Row.Cells[11].Text;

        //    string url = string.Format(@"<a  href='https://app.hubspot.com/contacts/1878634/company/{0}' target='_blank'>{1}</a>", companyUrl, companyName);

        //    string decodedCompanyLink = HttpUtility.HtmlDecode(url);
        //    e.Row.Cells[0].Text = decodedCompanyLink;
        //}


    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        tools.ExportDataTableToCsv(DTResults, "rz_hubspot_customer_activity_report.csv");
    }
}