﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="rz_hubspot_company_activity_contact_report.aspx.cs" Inherits="secure_sm_Reporting_marketing_rz_hubspot_company_activity_contact_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel panel-heading">
                    <h3>Rz & Hubspot Customer Activity Report</h3>
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-sm-2">
                                <uc1:datetime_flatpicker runat="server" ID="dtp" />                                
                            </div>
                            <div class="col-sm-3">
                                <asp:Button ID="btnSearch" runat="server" Text="View Report" OnClientClick="ShowUpdateProgress();" />
                            </div>



                        </div>
                    </div>
                </div>
                <div class="panel panel-body">
                    <uc1:sm_datatable runat="server" ID="smdt" />
                </div>
                <div class="panel panel-footer">
                    <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" CssClass="hs-button primary" Text="Export" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

