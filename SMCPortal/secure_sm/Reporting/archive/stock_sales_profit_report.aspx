﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="stock_sales_profit_report.aspx.cs" Inherits="secure_sm_Reporting_sales_stock_sales_profit_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
        .totals {
            font-size: 20px;
            font-weight: bolder;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="panel">
        <div class="panel panel-heading">
            <h3>Stock Sales Profit Report</h3>
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="totals" style="color: red">
                            PO Cost:
                        <asp:Label ID="lblTotalCost" runat="server"></asp:Label></label>
                    </div>
                    <div class="col-sm-3">
                        <label class="totals" style="color: blue">
                            Gross Profit:
                        <asp:Label ID="lblTotalGP" runat="server"></asp:Label></label>
                    </div>
                    <div class="col-sm-3">
                        <label class="totals" style="color: green">
                            Totals Sales:
                        <asp:Label ID="lblTotalRevenue" runat="server"></asp:Label></label>
                    </div>
                    <div class="col-sm-3">
                        <label class="totals" style="color: forestgreen">
                            GP Margin:
                          <asp:Label ID="lblGPMargin" runat="server"></asp:Label></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddlStockVendors" runat="server" OnSelectedIndexChanged="ddlStockVendors_SelectedIndexChanged" AutoPostBack="true" onchange="ShowUpdateProgress();"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <uc1:sm_datatable runat="server" ID="smdt" />
                </div>
            </div>
        </div>
        <div class="panel panel-footer">
            <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" CssClass="hs-button primary" Text="Export"/>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

