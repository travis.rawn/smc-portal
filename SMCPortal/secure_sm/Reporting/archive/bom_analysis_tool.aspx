﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="bom_analysis_tool.aspx.cs" Inherits="bom_analysis_tool" %>

<%@ Register Src="~/assets/controls/quote_form.ascx" TagPrefix="uc1" TagName="quote_form" %>
<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/sm_webimage.ascx" TagPrefix="uc1" TagName="sm_webimage" %>






<asp:Content ID="HeadContent1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--Part Search CSS:--%>
    <%--Search Results--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1563907836536/module_9497943201_Sensible_Micro_-_Early_2019_Custom_Modules_bz-portal-search-results.min.css?parameter=1 /">
    <%--Related Results--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1557339400809/module_9502989582_Sensible_Micro_-_Early_2019_Custom_Modules_bz-portal-search-results-related.min.css?parameter=1 /">
    <%--Certifications--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1564517078299/module_9561873698_Sensible_Micro_-_Early_2019_Custom_Modules_bz-portal-certs.min.css?parameter=1 /">
    <style>
        .panel-title {
            margin: 0px;
            padding-bottom: 10px;
        }

        .register-title {
            text-align: center;
        }

        .results-title {
            font-size: 18px;
            color: rgb(238, 38, 55);
        }

        .card-header {
            padding: 0px;
        }

        .card-body {
            padding: 0px;
        }

        .portal-title {
            font-size: 2.8em;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div style="padding-left: 5%; padding-right: 5%;">

        <asp:HiddenField ID="hfIP" runat="server" />
        <asp:HiddenField ID="hfUrl" runat="server" />
        <%--    <div class="section-container__wide padding-top__large padding-bottom__medium section-header__container">--%>
        <div class="align__center">
            <h1 class="main-title relative" style="font-size: 2.5em;">BOM Risk Summary Analysis
            </h1>
            <asp:Panel ID="pnlPortalSearch" runat="server">
                <div class="row" style="text-align: left;">
                    <div class="col-sm-6">
                        <asp:FileUpload ID="fuCsvList" runat="server" CssClass="hs-button" />

                    </div>

                    <div class="col-sm-6">
                        <asp:Button ID="btnSampleData" runat="server" Text="Load Sample Data" OnClick="btnSampleData_Click" CssClass="hs-button primary pull-right" OnClientClick="ShowUpdateProgress();" />
                    </div>
                </div>
                <div class="row" style="text-align: left;">
                    <div class="col-sm-12">
                        <asp:Button ID="btnImportList" runat="server" Text="Run Analysis" OnClick="btnImportList_Click" CssClass="hs-button primary" OnClientClick="ShowUpdateProgress();" />
                    </div>
                </div>

            </asp:Panel>



            <div class="search-results-container">
                <!-- unmodified search result html -->
                <asp:Panel ID="pnlResults" runat="server" Visible="false">
                    <div class="card" style="margin-top: 15px; text-align: left;">
                        <div class="card-header">
                            <asp:LinkButton ID="lbExportList" runat="server" Text="Export Results" OnClick="lbExportList_Click" />
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <uc1:sm_datatable runat="server" ID="smdt" />
                        </div>
                    </div>
                </asp:Panel>
                <!-- end unmodified search result html -->
            </div>
        </div>
        <%--</div>--%>
        <%--End Results Panel--%>
    </div>


    <script>
        var part = "";
        $(function () {
            if (document.location.search.length) {
                $("#resultsPanel").show()

            } else {
                $("#resultsPanel").hide()
            }
            var email = $("#MainContent1_quote_form_hfCustEmail");
            part = '<%= Part %>';
            $("#MainContent1_quote_form_hfPartNumber").val(part);


        });


        function SetPartNumber(button) {

            //Reset the Values on the Quote Form
            ResetQuoteForm();

            //Get the curretn Row Index of the results grid
            var rowIdx = button.parentNode.parentNode.sectionRowIndex;

            //Use Row Index to find the HiddenField, and get that value wwhich was set on RowDataBound
            part = $("#MainContent1_gvResults_hfRowPartNumber_" + rowIdx).val()
            //$('#divQuoteModalLabel').text("Get Quote For: " + part);            
            $("#QuotePartNumber").text("Get Quote For: " + part);
            //Set the Part Number Hidden Field on the Quote Form
            var hfQuoteFormPnID = $("[id$=_hfPartNumber]").attr("id");
            $("#" + hfQuoteFormPnID).val(part);
            //$("#QuotePartNumber").text(part);
            $('#divQuoteModal').modal('show');
            return false;
        }

        function doSearch() {
            ShowUpdateProgress();

            var part = $("#MainContent1_txtSearch").val();
            if (part) {
                //trackQuotes(event, "part_searched", "part_search", part, email);
                var pathname = window.location.pathname;
                window.location.href = pathname + '?pn=' + part;
            }
            else {
                slideAlert("danger", "Please provide search text.", 7000);
            }
            HideUpdateProgress();
            return false;
        }



        function clearSearch() {
            //$("#txtSearch").val("");
            ShowUpdateProgress();
            var pathname = window.location.pathname;
            window.location.href = pathname;
            return false;
        }
    </script>

    <script>
        function doRegister() {
            ShowUpdateProgress();
            window.location.replace('/login.aspx?src=partSearch?ReturnUrl=' + window.location.href);
            return false;
        }

    </script>

    <script>
        $(document).ready(function () {
            $("#MainContent1_gvResults").tablesorter();
        });
    </script>

</asp:Content>



<%--Certifications Panel--%>
<asp:Content ID="cpCertifications" ContentPlaceHolderID="SubMain" runat="Server">






    <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
        <div class="row-fluid ">
            <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
                <div id="hs_cos_wrapper_module_1557509487380601" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">



                    <section class=" image-row__wrapper padding-bottom__large">

                        <%--  <div class="background__grey">--%>
                        <div class="background__white">
                            <div class="section-container__extreme-narrow padding-top__large padding-bottom__medium section-header__container">
                                <div class="main-content align__center">
                                    <h2 class="relative portal-title">Certified Quality by Accredited Organizations
                                    </h2>
                                    <div class="large-paragraph">
                                        <p>Our customers are building the critical infrastructure that our society relies on, so having a certified due diligence approach to quality sourcing and inspection is a must.</p>
                                    </div>
                                </div>
                            </div>

                            <!-- certs -->

                            <div class="section-container__narrow image-row__container cert-container">




                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=200&amp;name=IDEA-QMS-9090-seal.jpeg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=100&amp;name=IDEA-QMS-9090-seal.jpeg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=200&amp;name=IDEA-QMS-9090-seal.jpeg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=300&amp;name=IDEA-QMS-9090-seal.jpeg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=400&amp;name=IDEA-QMS-9090-seal.jpeg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=500&amp;name=IDEA-QMS-9090-seal.jpeg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=600&amp;name=IDEA-QMS-9090-seal.jpeg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=200&amp;name=CCAP-LOGO.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=100&amp;name=CCAP-LOGO.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=200&amp;name=CCAP-LOGO.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=300&amp;name=CCAP-LOGO.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=400&amp;name=CCAP-LOGO.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=500&amp;name=CCAP-LOGO.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=600&amp;name=CCAP-LOGO.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=200&amp;name=NSF-9001.jpeg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=100&amp;name=NSF-9001.jpeg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=200&amp;name=NSF-9001.jpeg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=300&amp;name=NSF-9001.jpeg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=400&amp;name=NSF-9001.jpeg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=500&amp;name=NSF-9001.jpeg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=600&amp;name=NSF-9001.jpeg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=200&amp;name=AS9120.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=100&amp;name=AS9120.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=200&amp;name=AS9120.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=300&amp;name=AS9120.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=400&amp;name=AS9120.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=500&amp;name=AS9120.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=600&amp;name=AS9120.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=200&amp;name=NQA_ESDS20.20.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=100&amp;name=NQA_ESDS20.20.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=200&amp;name=NQA_ESDS20.20.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=300&amp;name=NQA_ESDS20.20.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=400&amp;name=NQA_ESDS20.20.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=500&amp;name=NQA_ESDS20.20.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=600&amp;name=NQA_ESDS20.20.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=200&amp;name=NEWEraiMember_Logo.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=100&amp;name=NEWEraiMember_Logo.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=200&amp;name=NEWEraiMember_Logo.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=300&amp;name=NEWEraiMember_Logo.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=400&amp;name=NEWEraiMember_Logo.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=500&amp;name=NEWEraiMember_Logo.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=600&amp;name=NEWEraiMember_Logo.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>




                            </div>

                            <!-- END certs -->

                        </div>

                    </section>
                </div>

            </div>
            <!--end widget-span -->
        </div>
        <!--end row-->
    </div>



    <div class="row-fluid-wrapper row-depth-1 row-number-5 ">
        <div class="row-fluid ">
            <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
                <div id="hs_cos_wrapper_module_1557320680122665" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">

                    <div class="background__blue offer-section">

                        <div class="section-container__narrow padding-top__medium padding-bottom__medium section-header__container">
                            <div class="main-content align__center">
                                <h2 class="relative">Expand Your Results
                                </h2>
                                <div class="paragraph-large">
                                    <p>There’s even more in our customer portal. Get additional resources and info about your search queries when you log in.</p>
                                    <p>Inside you’ll find conflict mineral alerts, lifecycle information, PCNs and much more ― 100% free of charge.</p>
                                    <p><a href="/login.aspx" target="_blank" class="hs-button primary">Sign In</a>          <a href="/Account/register.aspx" target="_blank" class="hs-button primary">New Account</a></p>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!--end widget-span -->
        </div>
        <!--end row-->
    </div>
</asp:Content>
<%--End Certifications Panel--%>




