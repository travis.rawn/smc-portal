﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_quality_report_avg_time_on_hold : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    RzDataContext rdc = new RzDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            LoadDatePickers();
        BindgvAverageTimeOnHold();

    }
    private void BindgvAverageTimeOnHold()
    {
        List<HoldClosures> trackingList = GetResolutions().OrderBy(o => o.hold_date).ToList();

        switch (ddlHoldType.SelectedValue)
        {
            case "validation":
                {
                    trackingList = trackingList.Where(w => w.initialStage == "ValidationHold").ToList();
                    break;
                }
            case "inspection":
                {
                    trackingList = trackingList.Where(w => w.initialStage == "InspectionHold").ToList();
                    break;
                }
            case "resolved":
                {
                    trackingList = trackingList.Where(w => w.is_resolved == true).ToList();
                    break;
                }
            case "unresolved":
                {
                    trackingList = trackingList.Where(w => w.is_resolved == false).ToList();
                    break;
                }

        }
        if (trackingList.Count <= 0)
            return;

        int count = trackingList.Count;
        decimal totalMinutes = trackingList.Sum(s => Convert.ToDecimal(s.totalMinutes));
        decimal totalHours = totalMinutes / 60;
        decimal avgMinutes = totalMinutes / count;
        decimal avgHours = totalHours / count;
        lblCount.Text = "Count: " + count;
        lblTotalTimeOnHold.Text = "Total: " + totalMinutes.ToString("#.##") + " minutes";
        //lblAvg.Text = "Average: " + avgMinutes.ToString("#.##") + " minutes";
        lblAvg.Text = "Average  Hours: " + avgHours.ToString("#.##");


        var bindQuery = trackingList.Select(s => new
        {
            companyName = s.companyName,
            orderNumber = s.ordernumber,
            hold_date = s.hold_date,
            Resolved = s.is_resolved,
            ResDate = s.is_resolved == true ? s.resolution_date.ToString("MM/dd/yyyy") : "N/A",
            totalMinutes = s.totalMinutes.ToString("#.##"),
            holdReason = s.holdReason,
            orderid_Sales = s.orderid_sales,
            lastStage = s.currentStage,
            holdType = s.holdType
        }).OrderByDescending(o => o.hold_date).ToList();
        gvAverageTimeOnHold.DataSource = bindQuery;
        gvAverageTimeOnHold.DataBind();

        gvAverageTimeOnHold.DataKeyNames = new[] { "orderid_sales" };
        gvAverageTimeOnHold.HeaderRow.TableSection = TableRowSection.TableHeader;

        //if(ddlHoldType.SelectedValue.ToLower() != "validation" && ddlHoldType.SelectedValue.ToLower() != "inspection")
        LoadChart(trackingList);
    }

    public class HoldClosures
    {
        public string orderid_sales { get; set; }
        public string companyName { get; set; }
        public string ordernumber { get; set; }
        public string holdType { get; set; }
        public string initialStage { get; set; }
        public string currentStage { get; set; }
        public DateTime hold_date { get; set; }
        public DateTime resolution_date { get; set; }
        public bool is_resolved { get; set; }
        public double totalMinutes { get; set; }
        public string holdReason { get; set; }

    }


    private List<HoldClosures> GetResolutions()
    {
        List<HoldClosures> resolutions = new List<HoldClosures>();
        //Get All validation Trackings within date range into memory for fast lookup.
        //List<validation_tracking> allTracking = rzt.GetAllValidationTracking(dtpStart.SelectedDate, dtpEnd.SelectedDate);


        //Set the stages that constiture a "hold"
        List<string> holdStages = new List<string>() { "InspectionHold", "ValidationHold", "CustomerHold" };

        //Get a List of all holds in date range
        List<validation_tracking> holdsList = rdc.validation_trackings.Where(w => w.date_created >= dtpStart.SelectedDate.Value && w.date_created <= dtpEnd.SelectedDate.Value && holdStages.Contains(w.new_stage) && w.new_stage.Length > 0).ToList();

        //Get a list of SalesID's that have holds
        List<string> holdSalesIds = holdsList.Select(s => s.orderid_sales).Distinct().ToList();

        //Initial Holds - the earlies Hold type validation per orderid_sales
        List<validation_tracking> initialHolds = holdsList
           .Where(w => holdSalesIds.Contains(w.orderid_sales))
           .GroupBy(l => l.orderid_sales)
  .Select(g => g.OrderBy(l => l.date_created).First())
  .ToList();

        //Current Trackings - the most recent validation_tracking object per orderid_sales from initial Holds
        List<validation_tracking> currentTrackings = rdc.validation_trackings
            .Where(w => holdSalesIds.Contains(w.orderid_sales) && w.new_stage.Length > 0)
            .GroupBy(l => l.orderid_sales)
   .Select(g => g.OrderByDescending(l => l.date_created).First())
   .ToList();


        ////Loop through each of the initial holds, match to it's most recent, then compare times, stages, etc.  Create Resolutions with the data.
        foreach (validation_tracking initialHoldStage in initialHolds)
        {
            //Check for not voided sale
            //Make sure it's for a sale that actually exists:
            ordhed_sale s = rdc.ordhed_sales.Where(w => w.unique_id == initialHoldStage.orderid_sales).FirstOrDefault();
            if (s != null)
                if (s.isvoid != true)
                //if (!rdc.ordhed_sales.Where(w => w.unique_id == initialHoldStage.orderid_sales && w.isvoid == true).Any())
                {
                    HoldClosures resolution = new HoldClosures();
                    resolution.holdType = initialHoldStage.new_stage;
                    resolution.initialStage = initialHoldStage.new_stage;
                    resolution.orderid_sales = initialHoldStage.orderid_sales;
                    resolution.hold_date = (DateTime)initialHoldStage.date_created;
                    resolution.holdReason = initialHoldStage.hold_reason;


                    //validation_tracking InitialValidationTracking = rdc.validation_trackings.Where(w => w.orderid_sales == t.orderid_sales).OrderBy(o => o.date_created).FirstOrDefault();
                    validation_tracking CurrentValidationTracking = currentTrackings.Where(w => w.orderid_sales == initialHoldStage.orderid_sales).FirstOrDefault();
                    resolution.currentStage = CurrentValidationTracking.new_stage;
                    resolution.companyName = CurrentValidationTracking.companyname;

                    //Get the sales order from the detected sale, in case all prior validations happenend on the old SO#
                    resolution.ordernumber = s.ordernumber;




                    if (CurrentValidationTracking != null)
                    {
                        if (CurrentValidationTracking.new_stage.ToLower().Contains("complete"))
                        {
                            resolution.resolution_date = (DateTime)CurrentValidationTracking.date_created;
                            //resolution.totalMinutes = (resolution.resolution_date - (DateTime)initialHoldStage.date_created).TotalMinutes;
                            resolution.is_resolved = true;
                        }
                        else //if(CurrentValidationTracking.unique_id == initialHoldStage.unique_id)//There has only been 1 validation, set date to today
                        {
                            resolution.resolution_date = DateTime.Now;
                            resolution.is_resolved = false;
                        }
                        resolution.totalMinutes = (resolution.resolution_date - (DateTime)initialHoldStage.date_created).TotalMinutes;
                        resolutions.Add(resolution);

                    }

                }


        }

        return resolutions;
    }


    protected void lbClear_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl, false);
    }
    protected void lbSearch_Click(object sender, EventArgs e)
    {
        BindgvAverageTimeOnHold();
    }

    private void LoadDatePickers()
    {
        dtpStart.placeholderText = "Start Date";
        dtpEnd.placeholderText = "End Date";
        dtpStart.SelectedDate = new DateTime(2017, 3, 1);
        dtpEnd.SelectedDate = DateTime.Now;
    }



    private void LoadChart(List<HoldClosures> resolutions)
    {
        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();
        var varChartData = from r in resolutions
                           where r.hold_date >= dtpStart.SelectedDate.Value && r.hold_date <= dtpEnd.SelectedDate.Value
                           select new
                           {
                               holdDate = r.hold_date,
                               holdType = r.initialStage,
                               totalMinutes = r.totalMinutes,
                               resolved = r.is_resolved
                           };

        var varChartLabels = from r in varChartData
                             group r by new { month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(r.holdDate.Month), year = r.holdDate.Year } into d
                             select new { dt = string.Format("{0}/{1}", d.Key.month, d.Key.year), AVG = d.Average(g => Convert.ToDouble(g.totalMinutes)) };




        var avgTimeValidationHold = from r in varChartData
                                    where r.holdType == "ValidationHold"
                                    group r by new { month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(r.holdDate.Month), year = r.holdDate.Year } into d
                                    select new { dt = string.Format("{0}/{1}", d.Key.month, d.Key.year), AVG = d.Average(g => Convert.ToDouble(g.totalMinutes)) };

        var avgTimeInspectionHold = from r in varChartData
                                    where r.holdType == "InspectionHold"
                                    group r by new { month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(r.holdDate.Month), year = r.holdDate.Year } into d
                                    select new { dt = string.Format("{0}/{1}", d.Key.month, d.Key.year), AVG = d.Average(g => Convert.ToDouble(g.totalMinutes)) };




        //var avgTimeValidationHold = from r in resolutions
        //                            where r.hold_date >= dtpStart.SelectedDate && r.hold_date <= dtpEnd.SelectedDate && r.holdStage == "ValidationHold"
        //                            group r by new { month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(r.hold_date.Month), year = r.hold_date.Year } into d
        //                            select new { dt = string.Format("{0}/{1}", d.Key.month, d.Key.year), AVG = d.Average(g => Convert.ToDouble(g.totalMinutes)) };

        //var avgTimeInspectionHold = from r in resolutions
        //                            where r.hold_date >= dtpStart.SelectedDate && r.hold_date <= dtpEnd.SelectedDate && r.holdStage == "InspectionHold"
        //                            group r by new { month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(r.hold_date.Month), year = r.hold_date.Year } into d
        //                            select new { dt = string.Format("{0}/{1}", d.Key.month, d.Key.year), AVG = d.Average(g => Convert.ToDouble(g.totalMinutes)) };



        //Setup the avgHoldTimePerMonth DataSet        
        //if (ddlHoldType.SelectedValue == "all" || ddlHoldType.SelectedValue == "validation")
        //{
        SM_Charts.chartJsDataSet cDataSet1 = new SM_Charts.chartJsDataSet();
        cDataSet1.data = avgTimeValidationHold.Select(s => ((s.AVG) / 60).ToString("#.##")).ToList();
        cDataSet1.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSet1.dataSetLabel = "Avg Minutes on Validation Hold";
        cDataSet1.fillColors = true;
        cDataSet1.backGroundColor = SM_Tools.Colors.smGreenRgba;
        datasetList.Add(cDataSet1);
        //}
        //Setup the avgHoldTimePerMonth DataSet
        //if (ddlHoldType.SelectedValue == "all" || ddlHoldType.SelectedValue == "inspection")
        //{
        SM_Charts.chartJsDataSet cDataSet2 = new SM_Charts.chartJsDataSet();
        cDataSet2.data = avgTimeInspectionHold.Select(s => ((s.AVG) / 60).ToString("#.##")).ToList();
        cDataSet2.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSet2.dataSetLabel = "Avg Minutes on Inspection Hold";
        cDataSet2.fillColors = true;
        cDataSet2.backGroundColor = SM_Tools.Colors.smOrangeRgba;

        datasetList.Add(cDataSet2);
        // }

        //The Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.stacked = true;
        Chart1.chartType = SM_Enums.ChartType.bar.ToString();
        Chart1.labels = varChartLabels.Select(s => s.dt).ToList();
        Chart1.dataSetList = datasetList;
        Chart1.displayXaxisLabel = true;
        Chart1.displayYaxisLabel = true;        
        Chart1.canvasID = "cvAverageTimeOnHold";


        SM_Charts smr = new SM_Charts();
        smr.LoadAllCharts(Page, new List<SM_Charts.ChartJS>() { Chart1 });
    }

}
