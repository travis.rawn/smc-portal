﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class no_calls_report : System.Web.UI.Page
{
    List<HubspotLogic.HubspotCalls.LastHubspotCallUserObject> usersWithNoCalls = new List<HubspotLogic.HubspotCalls.LastHubspotCallUserObject>();
    SM_Tools tools = new SM_Tools();

    protected void Page_Load(object sender, EventArgs e)
    {
        smdtNoCalls.RowDataBound += new GridViewRowEventHandler(smdtNoCalls_RowDataBound);

        if (!Page.IsPostBack)
            RefreshData();
    }

    private void smdtNoCalls_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        foreach (GridViewRow r in smdtNoCalls.theGridView.Rows)
        {
            int i = Convert.ToInt32(r.Cells[2].Text);
            if (i >= 30)
                r.ForeColor = System.Drawing.Color.OrangeRed;
        }

    }

    protected void lbRefresh_Click(object sender, EventArgs e)
    {
        RefreshData();
    }

    private void RefreshData()
    {
        using (RzDataContext rdc = new RzDataContext())
            usersWithNoCalls = HubspotLogic.HubspotCalls.GetLastCallUserObjects(rdc);

        if (usersWithNoCalls.Count > 0)
            HandleNoCallUserTasks();
    }

    private void SendNotificationEmail()
    {
        try
        {
            using (RzDataContext rdc = new RzDataContext())
                HubspotLogic.HubspotCalls.DoNoCallAlertEmail(rdc);
            tools.HandleResult("success", "Successfully sent test email.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", "Failed to send test email. " + ex.Message);
        }


    }

    private void HandleNoCallUserTasks()
    {
        //Bind data to grid
        var gridQuery = usersWithNoCalls.Select(s => new
        {
            Agent = s.userName,
            LastCall = s.lastCall,
            MinutesSince = (int)s.timeSinceLastCall
        }).OrderByDescending(o => o.MinutesSince).ToList();

        smdtNoCalls.sortColumn = 2;
        smdtNoCalls.sortDirection = "desc";
        smdtNoCalls.dataSource = gridQuery;
        smdtNoCalls.loadGrid();

        //Send Notification Email to Joey
        //SendNotificationEmail();
    }





    protected void lbTestEmail_Click(object sender, EventArgs e)
    {
        SendNotificationEmail();
    }
}



