﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_stock_sales_profit_report : System.Web.UI.Page
{

    private List<string> listSelectedVendorUID = new List<string>(); //Making this a list so we can do single or all companies.

    //Viewstate Dictionary to hold all stock vendor names and ids
    protected Dictionary<string, string> DictStockVendors
    {
        get
        {
            if (ViewState["DictStockVendors"] != null)
                return (Dictionary<string, string>)(ViewState["DictStockVendors"]);
            else
                return new Dictionary<string, string>();
        }
        set
        {
            ViewState["DictStockVendors"] = value;
        }
    }

    protected DataTable DTResults
    {
        get
        {
            if (ViewState["DTResults"] != null)
                return (DataTable)(ViewState["DTResults"]);
            else
                return null;
        }
        set
        {
            ViewState["DTResults"] = value;
        }
    }




    //Create a list of distinct importIds from both partrecord and shipped_stock tables
    List<string> combinedImportIds = new List<string>();
    SM_Tools tools = new SM_Tools();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadStockVendorImportDictionary();
            LoadStockVendorsDDL();
        }

    }

    private void LoadStockVendorImportDictionary()
    {
        //Instantiate the dict
        DictStockVendors = new Dictionary<string, string>();
        using (RzDataContext rdc = new RzDataContext())
        {
            //Get a list of all importids in partercords
            List<string> distinctPartrecordImportIDs = new List<string>();
            distinctPartrecordImportIDs = rdc.partrecords.Where(w => w.stocktype == "stock" && (w.base_company_uid ?? "").Length > 0 && (w.companyname ?? "").Length > 0).Select(s => s.importid).Distinct().ToList();
            //get similar list of all importids in shipped_stock, as some may no longer be present in inventory
            List<string> distinctShipped_StockdImportIDs = new List<string>();
            distinctShipped_StockdImportIDs = rdc.shipped_stocks.Where(w => w.stocktype == "stock" && (w.base_company_uid ?? "").Length > 0 && (w.companyname ?? "").Length > 0).Select(s => s.importid).Distinct().ToList();

            //Gather all import ids by checking both partrecord and shipped_stock
            foreach (string pr in distinctPartrecordImportIDs)
                if (!combinedImportIds.Contains(pr))
                    combinedImportIds.Add(pr);
            foreach (string ss in distinctShipped_StockdImportIDs)
                if (!combinedImportIds.Contains(ss))
                    combinedImportIds.Add(ss);

            //Loop through the distinct importIds, and get company data from either partrecord, or shipped_stock
            foreach (string i in combinedImportIds)
            {
                string stockVendorName = "";
                string stockVendorUid = "";
                partrecord p = rdc.partrecords.Where(w => w.importid == i).FirstOrDefault();
                if (p != null)
                {
                    stockVendorName = p.companyname;
                    stockVendorUid = p.base_company_uid;
                }
                else
                {
                    shipped_stock ss = rdc.shipped_stocks.Where(w => w.importid == i).FirstOrDefault();
                    if (ss != null)
                    {
                        stockVendorName = ss.companyname;
                        stockVendorUid = ss.base_company_uid;
                    }

                }

                //Fill the Dictionary
                if (!string.IsNullOrEmpty(stockVendorName) && !string.IsNullOrEmpty(stockVendorUid))
                {
                    if (!DictStockVendors.ContainsKey(stockVendorUid))
                    {
                        DictStockVendors.Add(stockVendorUid, stockVendorName);
                    }
                }
            }
        }
    }

    private void LoadStockVendorsDDL()
    {
        //Clear list
        ddlStockVendors.Items.Clear();
        ddlStockVendors.Items.Add(new ListItem("-Choose-", "-1"));
        ddlStockVendors.Items.Add(new ListItem("-All Stock Vendors-", "0"));
        ddlStockVendors.AppendDataBoundItems = true;
        ddlStockVendors.DataSource = DictStockVendors.Distinct().OrderBy(o => o.Value);
        ddlStockVendors.DataTextField = "Value";
        ddlStockVendors.DataValueField = "Key";
        ddlStockVendors.DataBind();
    }


    protected void ddlStockVendors_SelectedIndexChanged(object sender, EventArgs e)
    {
        DoSearch();
    }

    private void DoSearch()
    {
        try
        {
            smdt.Clear();
            using (RzDataContext rdc = new RzDataContext())
            {

                listSelectedVendorUID.Clear();
                string selectedVendorUID = ddlStockVendors.SelectedValue;
                if (selectedVendorUID == "-1")//No company Chosen
                    return;
                else if (selectedVendorUID == "0")//All Companies, add all values from ddl
                {
                    //Set the stock dict now, if vendor is selected, will filter later.
                    listSelectedVendorUID = DictStockVendors.Select(s => s.Key).Distinct().ToList();
                }
                else
                {
                    //Set to the selected company id
                    listSelectedVendorUID.Add(selectedVendorUID);
                }

                DataTable dt = GetStockSalesDataTable(rdc);
                var query = dt.AsEnumerable().Select(s => new
                {
                    StockVendor = s.Field<string>("StockVendorName"),
                    ImportID = s.Field<string>("ImportID"),
                    Revenue = s.Field<double>("TotalSales").ToString("C"),
                    POCost = s.Field<double>("POCost").ToString("C"),
                    PONumber = s.Field<string>("stockPONumber"),
                    GP = s.Field<double>("TotalGP").ToString("C"),
                    Margin = Math.Round(s.Field<double>("TotalMargin")).ToString("P")

                });

                smdt.dataSource = query;
                smdt.loadGrid();
                LoadTotals(dt);
            }

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }


    private DataTable GetStockSalesDataTable(RzDataContext rdc)
    {



        DTResults = new DataTable();
        DTResults.Columns.Add("StockVendorName", typeof(string));
        DTResults.Columns.Add("StockVendorID", typeof(string));
        DTResults.Columns.Add("ImportID", typeof(string));
        DTResults.Columns.Add("TotalSales", typeof(double));
        DTResults.Columns.Add("POCost", typeof(double));
        DTResults.Columns.Add("TotalGP", typeof(double));
        DTResults.Columns.Add("TotalMargin", typeof(double));
        DTResults.Columns.Add("stockPONumber", typeof(string));
        DTResults.Columns.Add("stockPOId", typeof(string));

        List<Tuple<string, string>> idictIportIdsForVendor = new List<Tuple<string, string>>();

        //var partRecordQuery = rdc.partrecords.Where(w => listSelectedVendorUID.Contains(w.base_company_uid) && (w.importid.Length > 0 && w.importid != null) && w.importid.Contains("stock")).Select(s => s.importid);
        //var shippedStockQuery = rdc.shipped_stocks.Where(w => listSelectedVendorUID.Contains(w.base_company_uid) && (w.importid.Length > 0 && w.importid != null) && w.importid.Contains("stock")).Select(s => s.importid);
        var partRecordQuery = rdc.partrecords.Where(w => listSelectedVendorUID.Contains(w.base_company_uid) && (w.importid.Length > 0 && w.importid != null) && w.importid.Contains("stock")).Select(s => new Tuple<string, string>(s.base_company_uid, s.importid)).Distinct().ToList();//.Select(s => s.importid);
        var shippedStockQuery = rdc.shipped_stocks.Where(w => listSelectedVendorUID.Contains(w.base_company_uid) && (w.importid.Length > 0 && w.importid != null) && w.importid.Contains("stock")).Select(s => new Tuple<string, string>(s.base_company_uid, s.importid)).Distinct().ToList();//.Select(s => s.importid);
        List<Tuple<string, string>> allCompanyAndImportIds = partRecordQuery;
        foreach (Tuple<string, string> tup in shippedStockQuery)
        {
            if (!allCompanyAndImportIds.Contains(tup))
                allCompanyAndImportIds.Add(tup);
        }

        foreach (Tuple<string, string> tup in allCompanyAndImportIds)
        {
            //Tuple contents
            string stockVendorId = tup.Item1;
            string importID = tup.Item2;

            //Totals Variables
            double totalRevenue = 0;
            double totalGP = 0;
            double poCost = 0;
            double margin = 0;
            string stockPOId = "";
            string stockPONumber = "Not Found";
            string stockVendorName = "company not found";


            //stockVendorId = DictStockVendors.Where(w => w.Key == importID).Select(s => s.Value).FirstOrDefault();
            company c = rdc.companies.Where(w => w.unique_id == stockVendorId).FirstOrDefault();
            if (c != null)
                stockVendorName = c.companyname;

            List<orddet_line> lines = rdc.orddet_lines.Where(w => stockVendorId == w.vendor_uid && (w.orderid_sales ?? "").Length > 0 && importID == w.importid && w.status.ToLower() != "void" && w.stocktype == "stock" && (w.inventory_link_uid ?? "").Length > 0).ToList();

            //Total Revenue
            totalRevenue = lines.Sum(s => s.total_price ?? 0);

            //PO Cost - as derived by PO's related to teh import id           
            string ordhed_purchase_uid = rdc.partrecords.Where(w => w.importid == importID).Select(s => s.buy_purchase_id).FirstOrDefault();
            if (!string.IsNullOrEmpty(ordhed_purchase_uid))
            {
                ordhed_purchase p = rdc.ordhed_purchases.Where(s => s.unique_id == ordhed_purchase_uid).FirstOrDefault();
                poCost = p.ordertotal ?? 0;
                stockPOId = p.unique_id;
                stockPONumber = p.ordernumber;

            }


            //Total GP
            totalGP = totalRevenue - poCost;

            //Margin Margin = Gross Profit / Revenue
            //if (totalGP > 0)
            if (totalRevenue == 0)
                margin = 0;
            else
                margin = totalGP / totalRevenue;

            //Add to Datatable
            DTResults.Rows.Add(stockVendorName, stockVendorId, importID, totalRevenue, poCost, totalGP, margin, stockPONumber, stockVendorId);


        }
        return DTResults;
    }

    private void LoadTotals(DataTable dt)
    {

        double totalCost = 0;
        double totalRevenue = 0;
        double totalGP = 0;
        double totalGPMargin = 0;

        //Total Cost can be duplicated, sometimes our guys take a lot tied to a single PO and split that import.
        //In this case we don't want to count that cost twice.  So, need to get po cost for each company.
        //totalCost = dt.AsEnumerable().Sum(x => x.Field<double>("POCost"));
        using (RzDataContext rdc = new RzDataContext())
            foreach (string s in listSelectedVendorUID)
            {
                totalCost += GetPOCostForCompany(rdc, s);
            }



        totalRevenue = dt.AsEnumerable().Sum(x => x.Field<double>("TotalSales"));
        //totalGP = dt.AsEnumerable().Sum(x => x.Field<double>("TotalGP"));       
        totalGP = totalRevenue - totalCost;
        totalGPMargin = totalGP / totalRevenue;

        lblTotalCost.Text = totalCost.ToString("C");
        lblTotalRevenue.Text = totalRevenue.ToString("C");
        lblTotalGP.Text = totalGP.ToString("C");
        lblGPMargin.Text = totalGPMargin.ToString("P");
    }


    private double GetPOCostForCompany(RzDataContext rdc, string companyID)
    {
        double ret = 0;

        List<string> listPurchaseIds = new List<string>();
        listPurchaseIds = rdc.partrecords.Where(w => (w.importid ?? "").Length > 0 && w.base_company_uid == companyID).Select(s => s.buy_purchase_id).Distinct().ToList();
        if (listPurchaseIds.Count > 0)
        {
            foreach (string s in listPurchaseIds)
            {
                ordhed_purchase p = rdc.ordhed_purchases.Where(w => w.unique_id == s).FirstOrDefault();
                if (p != null)
                    ret += (double)p.ordertotal;
            }

        }
        return ret;
    }



    protected void btnExport_Click(object sender, EventArgs e)
    {
        tools.ExportDataTableToCsv(DTResults, "stock_sales_profit.csv");
    }
}