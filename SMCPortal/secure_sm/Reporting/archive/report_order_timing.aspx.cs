﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reports_report_time_sale_to_invoice : System.Web.UI.Page
{
    DataTable DtCurrentRange = new DataTable();
    DataTable DtPrevRange = new DataTable();
    RzDataContext rdc = new RzDataContext();
    SM_Tools tools = new SM_Tools();
    List<string> StockTypes = new List<string>() { SM_Enums.StockType.consign.ToString(), SM_Enums.StockType.stock.ToString(), SM_Enums.StockType.buy.ToString() };



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                LoadDatePickers();
                ddlReportType.SelectedValue = "sToi";
            }
            GetGridData(ddlReportType.SelectedValue);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void GetGridData(string type)
    {
        List<orddet_line> CurrentLineList = new List<orddet_line>();
        List<orddet_line> PreviousLineList = new List<orddet_line>();
        double CurrentDaysToInvoice = 0;
        double PreviousDaysToInvoice = 0;

        gvCurrent.DataSource = null;
        gvPrevious.DataSource = null;
        gvPrevious.DataBind();
        gvCurrent.DataBind();



        switch (type)
        {

            case "sToi":
                {

                    CurrentLineList = rdc.orddet_lines.Where(w => w.orderid_sales.Length > 0 && w.seller_name != "John Sullivan" && w.orderid_invoice.Length > 0 && w.status.ToLower() != "void" && !w.ordernumber_sales.Contains("CR") && StockTypes.Contains(w.stocktype) && (w.orderdate_sales >= dtpCurrentStart.SelectedDate.Value && w.orderdate_sales <= dtpCurrentEnd.SelectedDate.Value)).ToList();
                    PreviousLineList = rdc.orddet_lines.Where(w => w.orderid_sales.Length > 0 && w.seller_name != "John Sullivan" && w.orderid_invoice.Length > 0 && w.status.ToLower() != "void" && !w.ordernumber_sales.Contains("CR") && StockTypes.Contains(w.stocktype) && (w.orderdate_sales >= dtpPrevStart.SelectedDate.Value && w.orderdate_sales <= dtpPrevEnd.SelectedDate.Value)).ToList();

                    gvCurrent.DataSource = CurrentLineList.Select(s => new
                    {
                        Customer = s.customer_name,
                        SONum = s.ordernumber_sales,
                        SODate = s.orderdate_sales.Value.ToString("MM/dd/yyyy"),
                        InvDate = s.orderdate_invoice.Value.ToString("MM/dd/yyyy"),
                        DaysToInvoice = Math.Round((decimal)((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_sales).TotalDays, 2)
                    }).ToList();



                    gvPrevious.DataSource = PreviousLineList.Select(s => new
                    {
                        Customer = s.customer_name,
                        SONum = s.ordernumber_sales,
                        SODate = s.orderdate_sales.Value.ToString("MM/dd/yyyy"),
                        InvDate = s.orderdate_invoice.Value.ToString("MM/dd/yyyy"),
                        DaysToInvoice = Math.Round((decimal)((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_sales).TotalDays, 2)
                    }).ToList();

                    CurrentDaysToInvoice = CurrentLineList.Sum(s => ((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_sales).TotalDays);
                    PreviousDaysToInvoice = PreviousLineList.Sum(s => ((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_sales).TotalDays);

                    break;
                }
            case "sTop":
                {
                    CurrentLineList = rdc.orddet_lines.Where(w => (w.orderid_sales.Length > 0 && w.orderid_purchase.Length > 0) && w.seller_name != "John Sullivan" && w.status.ToLower() != "void" && StockTypes.Contains(w.stocktype) && (w.orderdate_sales >= dtpCurrentStart.SelectedDate.Value && w.orderdate_sales <= dtpCurrentEnd.SelectedDate.Value)).ToList();
                    PreviousLineList = rdc.orddet_lines.Where(w => (w.orderid_sales.Length > 0 && w.orderid_purchase.Length > 0) && w.seller_name != "John Sullivan" && w.status.ToLower() != "void" && StockTypes.Contains(w.stocktype) && (w.orderdate_sales >= dtpPrevStart.SelectedDate.Value && w.orderdate_sales <= dtpPrevEnd.SelectedDate.Value)).ToList();

                    gvCurrent.DataSource = CurrentLineList.Select(s => new
                    {
                        Customer = s.customer_name,
                        SONum = s.ordernumber_sales,
                        SODate = s.orderdate_sales.Value.ToString("MM/dd/yyyy"),
                        PODate = s.orderdate_purchase.Value.ToString("MM/dd/yyyy"),
                        DaysToPO = Math.Round((decimal)((DateTime)s.orderdate_purchase - (DateTime)s.orderdate_sales).TotalDays, 2)
                    }).ToList();


                    gvPrevious.DataSource = PreviousLineList.Select(s => new
                    {
                        Customer = s.customer_name,
                        SONum = s.ordernumber_sales,
                        SODate = s.orderdate_sales.Value.ToString("MM/dd/yyyy"),
                        PODate = s.orderdate_purchase.Value.ToString("MM/dd/yyyy"),
                        DaysToPO = Math.Round((decimal)((DateTime)s.orderdate_purchase - (DateTime)s.orderdate_sales).TotalDays, 2)
                    }).ToList();

                    CurrentDaysToInvoice = CurrentLineList.Sum(s => ((DateTime)s.orderdate_purchase - (DateTime)s.orderdate_sales).TotalDays);
                    PreviousDaysToInvoice = PreviousLineList.Sum(s => ((DateTime)s.orderdate_purchase - (DateTime)s.orderdate_sales).TotalDays);

                    break;
                }
            case "pToi":
                {
                    CurrentLineList = rdc.orddet_lines.Where(w => (w.orderid_invoice.Length > 0 && w.orderid_purchase.Length > 0) && w.seller_name != "John Sullivan" && w.status.ToLower() != "void" && StockTypes.Contains(w.stocktype) && (w.orderdate_sales >= dtpCurrentStart.SelectedDate.Value && w.orderdate_sales <= dtpCurrentEnd.SelectedDate.Value)).ToList();
                    PreviousLineList = rdc.orddet_lines.Where(w => (w.orderid_invoice.Length > 0 && w.orderid_purchase.Length > 0) && w.seller_name != "John Sullivan" && w.status.ToLower() != "void" && StockTypes.Contains(w.stocktype) && (w.orderdate_sales >= dtpPrevStart.SelectedDate.Value && w.orderdate_sales <= dtpPrevEnd.SelectedDate.Value)).ToList();

                    gvCurrent.DataSource = CurrentLineList.Select(s => new
                    {
                        Customer = s.customer_name,
                        PONum = s.ordernumber_purchase,
                        PODate = s.orderdate_purchase.Value.ToString("MM/dd/yyyy"),
                        InvDate = s.orderdate_invoice.Value.ToString("MM/dd/yyyy"),
                        DaysToInv = Math.Round((decimal)((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_purchase).TotalDays, 2)
                    }).ToList();


                    gvPrevious.DataSource = PreviousLineList.Select(s => new
                    {
                        Customer = s.customer_name,
                        PONum = s.ordernumber_purchase,
                        PODate = s.orderdate_purchase.Value.ToString("MM/dd/yyyy"),
                        InvDate = s.orderdate_invoice.Value.ToString("MM/dd/yyyy"),
                        DaysToInv = Math.Round((decimal)((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_purchase).TotalDays, 2)
                    }).ToList();


                    CurrentDaysToInvoice = CurrentLineList.Sum(s => ((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_purchase).TotalDays);
                    PreviousDaysToInvoice = PreviousLineList.Sum(s => ((DateTime)s.orderdate_invoice - (DateTime)s.orderdate_purchase).TotalDays);
                    break;
                }


        }


        gvPrevious.DataBind();
        gvCurrent.DataBind();
        gvPrevious.HeaderRow.TableSection = TableRowSection.TableHeader;
        gvCurrent.HeaderRow.TableSection = TableRowSection.TableHeader;





        SetLabels("current", CurrentLineList, CurrentDaysToInvoice);
        SetLabels("previous", PreviousLineList, PreviousDaysToInvoice);
    }


    private void SetLabels(string rangeType, List<orddet_line> LineList, double daysToInvoice)
    {


        lblCurrentType.Text = ddlReportType.SelectedItem.Text;


        Label lblSaleCount = null;
        Label lblTotalDays = null;
        Label lblAverageDays = null;
        switch (rangeType)
        {
            case "current":
                {
                    lblSaleCount = lblCurrentSaleCount;
                    lblTotalDays = lblCurrentTotalDays;
                    lblAverageDays = lblCurrentAvgDays;
                    break;
                }
            case "previous":
                {
                    lblSaleCount = lblPreviousSaleCount;
                    lblTotalDays = lblPreviousTotalDays;
                    lblAverageDays = lblPreviousAvgDays;
                    break;
                }

        }

        lblSaleCount.Text = LineList.Count().ToString();
        lblTotalDays.Text = daysToInvoice.ToString("##.##");
        lblAverageDays.Text = (daysToInvoice / LineList.Count()).ToString("##.##");
    }

    private void LoadDatePickers()
    {
        dtpCurrentStart.placeholderText = "Current Start Date";
        dtpCurrentEnd.placeholderText = "Current End Date";
        dtpPrevStart.placeholderText = "Previous Start Date";
        dtpPrevEnd.placeholderText = "Previous End Date";

        dtpCurrentStart.SelectedDate = new DateTime(2017, 2, 17);
        dtpCurrentEnd.SelectedDate = DateTime.Now;
        dtpPrevStart.SelectedDate = new DateTime(2016, 1, 1);
        dtpPrevEnd.SelectedDate = new DateTime(2017, 1, 1);
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        GetGridData(ddlReportType.SelectedValue);
    }
}