﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_quality_inspection_priority : System.Web.UI.Page
{
    RzDataContext rdc = new RzDataContext();
    List<ShippingLogic.priorityObject> priorityList = new List<ShippingLogic.priorityObject>();
    RzTools rzt = new RzTools();
    int refreshIntervalSeconds = 60;
    int priorityThreshold = -10; //only show lines that have this base priority. 









    protected void Page_Load(object sender, EventArgs e)
    {
        setRefreshRate();
        setPriorityThreshold();
        smdtPriority.RowDataBound += new GridViewRowEventHandler(smdtPriority_RowDataBound);
        SetupDataTable();
        GetData();

    }

    private void setPriorityThreshold()
    {
        string strPriority = Tools.Strings.SanitizeInput(Request.QueryString["priority"]);
        if (!string.IsNullOrEmpty(strPriority))
        {
            int n;
            bool isNumeric = int.TryParse(strPriority, out n);
            if (isNumeric)
                priorityThreshold = n;
        }
    }

    private void setRefreshRate()
    {
        StringBuilder sb = new StringBuilder();
        int refresh = refreshIntervalSeconds * 10000;
        string strRefresh = Request.QueryString["refresh"];
        if (!string.IsNullOrEmpty(strRefresh))
        {
            int n;
            bool isNumeric = int.TryParse(strRefresh, out n);
            if (isNumeric)
                refresh = n * 10000;
        }
        sb.Append(" setTimeout(function(){ \n");
        sb.Append(" window.location.reload(1); \n");
        sb.Append(" }, " + refresh + "); \n");

        string refreshScript = sb.ToString();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "autoRefresh", refreshScript, true);
    }

    private void SetupDataTable()
    {
        smdtPriority.sortDirection = "desc";
        smdtPriority.sortColumn = 12;
        smdtPriority.pageLength = 25;
        

    }

    protected void smdtPriority_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //Colors:  System.Drawing.ColorTranslator.FromHtml("#123456");
        //Danger: #EE273A
        //Warning: #F67D28
        //Alert: #199B89
        //SortedBy: #82E36D





        GridView gv = smdtPriority.theGridView;
        foreach (GridViewRow row in gv.Rows)
        {
            int priority = 0;
            priority = Convert.ToInt32(row.Cells[13].Text);
            if (priority > 10)
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#EE273A");
            else if (priority > 5)
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F67D28");
            else if (priority > 0)
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#199B89");

        }
    }

    private void GetData()
    {
        DateTime startDate = new DateTime(2020, 1, 1);       
        priorityList = SensibleDAL.ShippingLogic.GetInboundPriorityLines(rdc,startDate);
        if (priorityList.Count > 0)
        {
            var query = priorityList.Select(s => new
            {

                PartNumber = s.partNumber,
                QTY = s.qty,
                Agent = s.salesAgent,
                Customer = s.customerName,
                Vendor = s.vendorName,
                SO = s.ordernumberSales,
                PO = s.ordernumberPurchase,
                cDock = s.dockDate.Date.ToShortDateString(),
                Received = s.recievedDate.Date.ToShortDateString(),
                GP = s.grossProfit,
                POTerms = s.poTerms,
                QCSatatus = s.qcStatus,
                LineStatus = s.lineStatus,
                Priority = s.linePriority > 0 ? (100+s.linePriority) : (s.datePriority + s.gpPriority),
                PriValues = "Days: " + s.datePriority + " | " + "GP: " + s.gpPriority,
                SODate = s.orderDateSales.Date.ToShortDateString(),


            });
            //smdtPriority.dataSource = query.Where(w => w.Priority > priorityThreshold).OrderBy(o => o.Agent);
            smdtPriority.dataSource = query.OrderByDescending(o => o.Priority).ThenByDescending(t => t.cDock);
            smdtPriority.loadGrid(true);
        }
    }








}