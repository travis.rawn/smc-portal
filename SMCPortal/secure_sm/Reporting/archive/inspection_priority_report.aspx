﻿<%@ Page Title="Shipping Priority Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="inspection_priority_report.aspx.cs" Inherits="secure_sm_Reporting_quality_inspection_priority" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
        table.dataTable .sorting_1 {
            background-color: #82E36D !important;
        }
    </style>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Shipping Priority</h1>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-info">
        <div class="panel-heading ">
            <label>Priority Rules:</label>
            Higher Priority Number = Higher Priority
            <p><strong>1</strong> point per day until Customer Dock || <strong>1</strong> point for every day of the PO Term Period || <strong>1</strong> point per 10K of GP </p>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <uc1:sm_datatable runat="server" ID="smdtPriority" />




  

</asp:Content>

