﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using SensibleDAL;
//using SpreadsheetLight;
using System.Drawing;
using System.Web.Security;
using System.Web;
using System.Threading.Tasks;
using SpreadsheetLight;

public partial class bom_analysis_tool : Page
{


    SM_Tools tools = new SM_Tools();
    List<string> PartsList; //This is the main object we pass to Part Search.  Part Search now handles lists.
    public string Part;
    string Mfg;
    //string Des;
    string UserName;
    company Comp;

    protected List<SiliconExpertLogic.PartRiskSummaryObject> listPrso
    {
        get
        {

            return (List<SiliconExpertLogic.PartRiskSummaryObject>)Cache["listPrso"];


        }
        set
        {
            Cache["listPrso"] = value;
        }
    }



    protected List<DataSet> SiDataSetList
    {
        get
        {
            return (List<DataSet>)Cache["SiDataSetList"];
        }
        set
        {
            Cache["SiDataSetList"] = value;
        }
    }

    //CSVHelper properties
    public class PartListSearchInput
    {
        [Index(0)]
        public string partnumber { get; set; }
        [Index(1)]
        public string manufacturer { get; set; }
      
    }

    //Export Object
    [Serializable]
    public class PartListSearchResult
    {
        public string ComID { get; set; }
        public string PartNumber { get; set; }
        public string Updated { get; set; }
        public string LTBDate { get; set; }
        public string LifeCycleUrl { get; set; }
        public string Stage { get; set; }
        public string RiskGrade { get; set; }
        public string RiskPct { get; set; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
    }





    private void ExportRiskSummarySheet(List<SiliconExpertLogic.PartRiskSummaryObject> listPrso)
    {


        SLDocument sl = new SLDocument();
        sl.AddWorksheet("Part Summary Risk Data");

        if (listPrso.Count == 0)
            return;
        DataTable dt = new DataTable();

        dt.Columns.Add("PartNumber");
        dt.Columns.Add("Manufacturer");
        dt.Columns.Add("ECCN");
        dt.Columns.Add("EstYearsEOL");
        dt.Columns.Add("EstEOLDate");
        dt.Columns.Add("LifecycleStage");
        dt.Columns.Add("LifecycleRiskGrade");
        dt.Columns.Add("OverallRisk");
        dt.Columns.Add("ObsNotice");
        dt.Columns.Add("NRND");
        dt.Columns.Add("CounterfietRiskGrade");


        foreach (SiliconExpertLogic.PartRiskSummaryObject prso in listPrso)
        {
            DataRow row = dt.NewRow();
            row["PartNumber"] = prso.partNumber;
            row["Manufacturer"] = prso.manufacturer;
            row["ECCN"] = prso.eCCN;
            row["EstYearsEOL"] = prso.estimatedYearsToEOL;
            row["EstEOLDate"] = prso.estimatedEOLDate;
            row["LifecycleStage"] = prso.partLifecycleStage;
            row["LifecycleRiskGrade"] = prso.lifeCycleRiskGrade;
            row["OverallRisk"] = prso.overallRisk;
            row["ObsNotice"] = prso.hasObsolescenceNotice;
            row["NRND"] = prso.hasNRNDNotice;
            row["CounterfietRiskGrade"] = prso.seGrade;
            dt.Rows.Add(row);
        }
        sl.ImportDataTable("A1", dt, true);

        string fileName = "RiskSummary.xlsx";


        //Download
        Response.Clear();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
        sl.SaveAs(Response.OutputStream);
        Response.End();
    }

    private void LoadDataGrid()
    {


        //var query = dt.AsEnumerable().Select(s => new
        //{
        //    Part = s.Field<string>("PartNumber"),
        //    MFG = s.Field<string>("Manufacturer"),
        //    EOLYears = s.Field<string>("PartNumber"),
        //    Lifecycle = s.Field<string>("PartNumber"),
        //    Risk = s.Field<string>("PartNumber"),
        //    Obs = s.Field<string>("PartNumber"),
        //    NRND = s.Field<string>("PartNumber"),
        //    Image = s.Field<string>("PartNumber"),
        //    ComID = s.Field<string>("PartNumber"),

        //}).ToList();

        var query = listPrso.Select(s => new
        {
            Part = s.partNumber,
            MFG = s.manufacturer,
            EOLYears = string.IsNullOrEmpty(s.estimatedYearsToEOL) ? "Uconfirmed" : s.estimatedYearsToEOL, //+ "(" + s.estimatedEOLDate + ")",
            Lifecycle = string.IsNullOrEmpty(s.partLifecycleStage) ? "Uconfirmed" : s.partLifecycleStage + "Risk: " + s.lifeCycleRiskGrade,
            Risk = s.overallRisk ?? "Uconfirmed",
            Obs = s.hasObsolescenceNotice,
            NRND = s.hasNRNDNotice,
            Image = s.ProductImageSmall,
            ComID = s.comID,
            Grade = s.seGrade
        }).ToList();
        smdt.theGridView.DataKeyNames = new string[] { "ComID" };
        smdt.dataSource = query;
        smdt.loadGrid();
        pnlResults.Visible = true;

    }



    //private List<PartListSearchInput> ImportCsvHelper(StreamReader reader)
    //{
    //    List<PartListSearchInput> ret = new List<PartListSearchInput>();
    //    var csvReader = new CsvReader(reader);
    //    ////Don't look for headers
    //    //csvReader.Configuration.HeaderValidated = null;
    //    //Check check for field names
    //    csvReader.Configuration.MissingFieldFound = null;

    //    //Expect no header
    //    csvReader.Configuration.HasHeaderRecord = false;

    //    ret.AddRange(csvReader.GetRecords<PartListSearchInput>());

    //    return ret;

    //}


    //Control Events

    protected void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {




        //Hide ComID COlumn
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            // Retrieve the key value for the current row. Here it is an int.
            string comID = smdt.theGridView.DataKeys[e.Row.RowIndex].Values[0].ToString();

            //Get the prso object form list
            SiliconExpertLogic.PartRiskSummaryObject prso = listPrso.Where(w => w.comID == comID).FirstOrDefault();
            if (prso == null)
                throw new Exception("Error. prso object not found in prsoList.");

            //if OBS , Cell = Orange
            string strHasObs = e.Row.Cells[5].Text;
            if (!string.IsNullOrEmpty(strHasObs))
            {
                if (strHasObs.ToLower() == "yes")
                    e.Row.Cells[5].BackColor = Color.FromName("#FF9633");
            }
            //If NRND = Cell = Purple
            string strIsNRND = e.Row.Cells[6].Text;
            if (!string.IsNullOrEmpty(strIsNRND))
            {
                if (strIsNRND.ToLower() == "yes")
                    e.Row.Cells[6].BackColor = Color.FromName("#9633FF");
            }
            //Highlight row Yellow if EOL Years <= 2
            string strEolYears = e.Row.Cells[2].Text;
            decimal decEolYears;
            bool eolYearsIsDecimal = decimal.TryParse(strEolYears, out decEolYears);
            if (eolYearsIsDecimal)
            {
                if (decEolYears <= 2)
                    e.Row.BackColor = Color.FromName("#F8FF33");
            }
            //Product Image           
            if (!string.IsNullOrEmpty(prso.ProductImageSmall) && !string.IsNullOrEmpty(prso.ProductImageLarge))
            {

                //load the control - necessary when dynamically adding .ascx controls to page/ object (i.e. Gridview Row) controls.
                assets_controls_sm_webimage wi = (assets_controls_sm_webimage)LoadControl("~/assets/controls/sm_webimage.ascx");
                //add the control where needed               
                wi.init(prso.ProductImageSmall, prso.ProductImageLarge);
                e.Row.Cells[7].Controls.Add(wi);
            }



        }
    }

    protected async void btnImportList_Click(object sender, EventArgs e)
    {
        try
        {
            
            //if (!isAllowed())
            //    throw new Exception("This action is currently only available to admin users.");

            //FileUpload fu = fuCsvList;
            //if (!fu.HasFile)
            //    throw new Exception("No file detected.");


            ////Confirm it's csv, if not alert and return
            //List<string> allowedFileExtensions = new List<string>() { "csv", "txt" };
            //string strExt = Path.GetExtension(fu.FileName).Replace(".", "");
            //if (!allowedFileExtensions.Contains(strExt.ToLower()))
            //    tools.HandleResult("fail", "Please submit only txt or csv files.");

            ////Create a memorystream of the file data
            //StreamReader reader = new StreamReader(fu.FileContent);

            ////Read the contents of the file
            //List<PartListSearchInput> psList = ImportCsvHelper(reader);
            ////Get the part numbers into a list for search
            //List<Tuple<string, string>> listPnMfg = psList.Select(t => new Tuple<string,string>(t.partnumber, t.manufacturer)).ToList();
            ////Pass the Tuple to SE For list DataSet Results (Free)
            //List<DataSet> dsSEResults = await SiliconExpertLogic.GetPnMfgDataSetJSON(listPnMfg, false);
            ////From these results, get the unique ComIds
            //List<string> listComIds = GetDistinctComIds(dsSEResults, listPnMfg);           
            ////Perform Detail Search with the distinct ComID list         
            //List<DataSet> dsDetailList = await SiliconExpertLogic.GetDataSetJSON(listComIds, SiliconExpertLogic.SearchType.Detail);
            //listPrso = SiliconExpertLogic.GeneratePartRiskSummary(dsDetailList);
            //if (listPrso != null)
            //    LoadDataGrid();

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            string error = ex.Message;
        }

    }

    private List<string> GetDistinctComIds(List<DataSet> dsSEResults, List<Tuple<string, string>> listPnMfg)
    {
        List<string> ret = new List<string>();
        foreach (DataSet ds in dsSEResults)
        {
            if (ds.Tables["PartDto"] != null)
                foreach (DataRow row in ds.Tables["PartDto"].Rows)
                {
                    string rowPn = row.Field<string>("PartNumber");
                    string rowMfg = row.Field<string>("Manufacturer").Trim().ToUpper();
                    string rowComID = row.Field<string>("ComID");

                    string strDictMfg = listPnMfg.Where(w => w.Item1.ToUpper() == rowPn.ToUpper()).Select(s => s.Item2.Trim().ToUpper()).FirstOrDefault();
                    if (string.IsNullOrEmpty(strDictMfg))
                        continue;
                    if (strDictMfg == rowMfg)
                    {
                        ret.Add(rowComID);
                    }
                }
        }

        return ret;
    }

    private bool isAllowed()
    {
        MembershipUser mu = Membership.GetUser();
        if (!tools.IsInAnyRole(mu.UserName, new List<string>() { SM_Enums.PortalRole.admin.ToString(), SM_Enums.PortalRole.portal_admin.ToString() }))
        {

            return false;
        }
        return true;
    }

    protected void lbExportList_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv(listPrso, "results.csv");
    }

    protected async void btnSampleData_Click(object sender, EventArgs e)
    {
        try
        {
            //List<Tuple<string, string>> listPnMfg = new List<Tuple<string, string>>();
            ////sample that has multiple mfg: PN:SMCJ15A,MFG:AVX
            //Tuple<string, string> sampleParts = new Tuple<string, string>("SMCJ15A","AVX");
            //listPnMfg.Add(sampleParts);
            ////Pass the Tuple to SE For list DataSet Results (Free)
            //List<DataSet> dsSEResults = await SiliconExpertLogic.GetPnMfgDataSetJSON(listPnMfg, false);
            ////From these results, get the unique ComIds
            //List<string> listComIds = GetDistinctComIds(dsSEResults, listPnMfg);
            ////Perform Detail Search with the distinct ComID list         
            //List<DataSet> dsDetailList = await SiliconExpertLogic.GetDataSetJSON(listComIds, SiliconExpertLogic.SearchType.Detail);
            //listPrso = SiliconExpertLogic.GeneratePartRiskSummary(dsDetailList);
            //if (listPrso != null)
            //    LoadDataGrid();
        }
        catch (Exception ex)
        {
            //tools.HandleResult("fail", ex.Message);
            string error = ex.Message;

        }
    }
    
}