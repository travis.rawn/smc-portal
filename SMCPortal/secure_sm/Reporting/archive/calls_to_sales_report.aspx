﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="calls_to_sales_report.aspx.cs" Inherits="secure_sm_Reporting_sales_calls_to_sales_report" %>










<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>Calls to Sales Report    </h2>

    <uc1:sm_chartjs runat="server" ID="sm_chartjs" />

    <div class="row">
        <div class="col-sm-3">
            <uc1:RzAgentPicker runat="server" ID="rzap" />
        </div>
        <div class="col-sm-3">
            <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
        </div>
        <div class="col-sm-3">
            <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
        </div>
        <div class="col-sm-3">
            <asp:LinkButton ID="lbRefresh" runat="server" OnClientClick="ShowUpdateProgress();">Refresh</asp:LinkButton>
        </div>
    </div>
    <uc1:sm_datatable runat="server" ID="smdt" />
    <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">Export to CSV</asp:LinkButton>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

