﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_quality_turn_back_metrics : System.Web.UI.Page
{
    class tbObject
    {

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public double ResMinutes { get; set; }
        public string Notes { get; set; }
        public string InspID { get; set; }
        public DateTime ResDate { get; set; }

    }
    RzDataContext rdc = new RzDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }

    private void LoadData()
    {


        List<tbObject> objectList = new List<tbObject>();
        List<qc_turn_back> tbList = (from t in rdc.qc_turn_backs select t).ToList();
        foreach (qc_turn_back t in tbList)
        {
            tbObject to = new tbObject();
            //get resolution time
            TimeSpan span = (DateTime)t.turn_back_resolution_date - (DateTime)t.turn_back_date;
            double totalMinutes = span.TotalMinutes;

            to.Name = t.inspection_agent;
            to.InspID = t.insp_id;
            to.Date = (DateTime)t.turn_back_date;
            to.ResDate = (DateTime)t.turn_back_resolution_date;
            to.ResMinutes = Math.Ceiling(totalMinutes);
            to.Notes = t.turn_back_notes;




            objectList.Add(to);

        }

        var query = objectList.Select(s => new {

            s.Name,
            s.InspID,
            Date = s.Date.ToShortDateString(),
            ResDate = s.ResDate.ToShortDateString(),
            s.ResMinutes,
            s.Notes




        });

        smdtTurnBackMetrics.dataSource = query.OrderBy(o => o.ResMinutes);
        smdtTurnBackMetrics.loadGrid();
    }
   
}