﻿<%@ Page Title="Sales Theory Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="sales_theory_report.aspx.cs" Inherits="secure_sm_Reporting_management_sales_theory_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/RzAgentPicker.ascx" TagPrefix="uc1" TagName="RzAgentPicker" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>





<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
        .refreshIcon {
            font-size: 14px;
            margin-left: 10px;
        }


        .tileContainer {
            display: flex;
            height: 100%;
            flex-wrap: wrap;
        }

        .tileContainerTitle {
            font-size: 16px;
        }

        .column {
            flex: 1;
            display: flex;
            flex-direction: column;
        }

        .tile {
            padding: 5px;
            flex: 1;
            color: white;
            text-align: center;
            border: 5px solid;
            margin-top: 5px;
            justify-content: center;
        }

            .tile.total {
                flex: 2;
            }

        .pointSquare-label {
            font-size: 20px;
        }

        .pointSquare-icon {
            font-size: 50px;
        }


        .pointSquare-value {
            font-size: 40px;
        }

        .pointSquare-total-label {
            font-size: 60px;
        }

        .pointSquare-total-icon {
            font-size: 80px;
        }

        .pointSquare-total-value {
            font-size: 70px;
        }



        .graphics-section {
            min-height: 320px;
        }
    </style>


    <%-- <meta http-equiv="refresh" content="120">--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div style="text-align: center; font-size: 40px;">
        Sales Theory Numbers
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div id="controls">
            <div class="card">
                <div class="row">
                    <div class="col-sm-12">
                        <label>
                            <asp:Label runat="server" ID="lblWeekRange"></asp:Label></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                       
                        <uc1:datetime_flatpicker runat="server" ID="dtp" />
                        <input id="cbxSelectedDay" type="checkbox" checked />
                        <label for="cbxSelectedDay">Selected Date Only</label>
                        <p></p>
                    </div>
                    <div class="col-sm-3">
                        <uc1:RzAgentPicker runat="server" ID="rzap" />
                    </div>
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-3 pull-right">
                        <asp:LinkButton runat="server" ID="lbRefresh" OnClientClick="ShowUpdateProgress();" Style="cursor: pointer;"><label>Refresh Data ...</label><i class="fas fa-sync-alt refreshIcon"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="graphicSection" class="graphics-section">
            <div class="card">
                <asp:Label ID="lblGraphicSelectedAgent" runat="server" CssClass="tileContainerTitle"></asp:Label>
                <div class="tileContainer">

                    <div class="column">
                        <div class="tile" style="background: darkmagenta;">
                            <label class="pointSquare-label">Conversation Points:</label><br />
                            <i class="fas fa-comments pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblTotalConvoPoints" runat="server" CssClass="pointSquare-value" Text="0"></asp:Label>

                        </div>

                        <div class="tile" style="background: black;">
                            <label class="pointSquare-label">SalesGoal Points:</label><br />
                            <i class="fas fa-dollar-sign pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblTotalSalesGoalPoints" runat="server" CssClass="pointSquare-value" Text="0"></asp:Label>

                        </div>
                    </div>

                    <div class="column">
                        <%--  <div class="tile" style="background: red;">
                            <label class="pointSquare-label">PhoneCall Points:</label><br />
                            <i class="fas fa-phone pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblTotalPhonePoints" runat="server" CssClass="pointSquare-value" Text="0"></asp:Label>
                        </div>--%>
                        <div class="tile" style="background: red;">
                            <%--<label class="pointSquare-label"></label><br />--%>
                            <asp:Label runat="server" ID="lblBonusSquareTitle" CssClass="pointSquare-label"></asp:Label><br />
                            <i ID="icoBonusIcon" runat="server" ></i>
                            <br />
                            <asp:Label ID="lblTotalBonusPoints" runat="server" CssClass="pointSquare-value" Text="0"></asp:Label>
                        </div>
                        <div class="tile" style="background: green;">
                            <label class="pointSquare-label">RFQ Points:</label><br />
                            <i class="fas fa-list-ul  pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblTotalRFQPoints" runat="server" CssClass="pointSquare-value" Text="0"></asp:Label>

                        </div>
                    </div>


                    <div class="column">
                        <div class="tile total" style="background: blue">
                            <label class="pointSquare-total-label">Total:</label><br />
                            <i class="fas fa-star  pointSquare-total-icon"></i>
                            <br />
                            <asp:Label ID="lblTotalGoalPoints" runat="server" CssClass="pointSquare-total-value" Text="0"></asp:Label>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div id="dataSection" runat="server">

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <label>
                            Daily Points:
                        </label>
                        <uc1:sm_datatable runat="server" ID="smdtDaily" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <label>
                            Weekly Points:
                        </label>
                        <uc1:sm_datatable runat="server" ID="smdtWeekly" />
                    </div>
                </div>

            </div>


            <%--Sales Theory data.--%>
            <a href="#collapse" onclick="toggleElement('collapse'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
            <%--Collapsible--%>
            <div id="collapse" style="display: none">
                <%--Phone Calls--%>
                <%--   <div id="divPhoneCalls">
                    <div class="card">
                        <label>Phone Call Points</label>
                        <uc1:sm_datatable runat="server" ID="smdtPhoneCallPoints" />
                    </div>
                </div>--%>
                <div id="divBonusPoints">
                    <div class="card">
                        <asp:Label runat="server" ID="lblBonusPointsGridTitle"></asp:Label>
                        <uc1:sm_datatable runat="server" ID="smdtBonusPoints" />
                    </div>
                </div>
                <%--Conversations--%>
                <div id="divConversationPoints">
                    <div class="card">
                        <label>Weekly Conversation Points</label>
                        <uc1:sm_datatable runat="server" ID="smdtConversationPoints" />
                    </div>
                </div>
                <%--RFQ POints--%>
                <div id="divRfqPoints">
                    <div class="card">
                        <label>RFQ Points</label>
                        <uc1:sm_datatable runat="server" ID="smdtRfqPoints" />
                    </div>
                </div>
                <%--Sales Goal--%>
                <div id="divSalesGoalPoints">
                    <div class="card">
                        <label>Sales Goal Points</label>
                        <uc1:sm_datatable runat="server" ID="smdtSalesGoalPoints" />
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>



        //Golbal Variables
        var dailyDt = $("#MainContent_smdtDaily_gvMain");
        var weeklyDt = $("#MainContent_smdtWeekly_gvMain");
        var bonusDt = $("#MainContent_smdtBonusPoints_gvMain");
        var convoDt = $('#MainContent_smdtConversationPoints_gvMain');
        var rfqDt = $('#MainContent_smdtRfqPoints_gvMain');
        var salesGoalDt = $('#MainContent_smdtSalesGoalPoints_gvMain');

        var bonusPoints = 0;
        var convoPoints = 0;
        var rfqPoints = 0;
        var salesPoints = 0;
        var totalPoints = 0;

        var strAgentName = '';




        //Event Handlers

        $(function() {
            FilterDataTables();
        });

        //$('.datetimepicker').on('dp.change', function (e) {
        //    FilterDataTables();
        //});      

         $("#MainContent_dtp_txtDate").change(function () {
            FilterDataTables();
        });

        
        $("#MainContent_rzap_ddlAgentPicker").change(function () {
            FilterDataTables();
        });

        $("#cbxSelectedDay").change(function () {
            FilterDataTables();
        });

        $(".dayPicker").find('li').each(function () {
            var li = $(this);
            li.click(function () {
                FilterDataTables();
                table = SearchDataTableForArray(dailyDt, arrSearch)// Daily 
            });
        });


        function FilterDataTables() {

            //The Array of search terms
            var arrSearch = [];

            //The Date            
            FilterDate(arrSearch);

            //Agent   
            strAgentName = '';
            var strAgentID = $('#MainContent_rzap_ddlAgentPicker').val();
            if (strAgentID != 'all' && strAgentID != 'choose')
                strAgentName = $('#MainContent_rzap_ddlAgentPicker  option:selected').text();
            arrSearch.push(strAgentName);

            //Set the Label
            if (strAgentName != '')
                $('#MainContent_lblGraphicSelectedAgent').text('Total: ' + strAgentName);
            else
                $('#MainContent_lblGraphicSelectedAgent').text('Total: All Users');


            //Filter the tables            
            table = SearchDataTableForString(weeklyDt, strAgentName)// Weekly 
            table = SearchDataTableForArray(dailyDt, arrSearch)// Daily 
            table = SearchDataTableForArray(bonusDt, arrSearch)
            table = SearchDataTableForArray(convoDt, arrSearch)
            table = SearchDataTableForArray(rfqDt, arrSearch)
            table = SearchDataTableForArray(salesGoalDt, arrSearch)
            CalculatePoints();
        }

        function CalculatePoints() {

            //Phone Points
            //phonePoints = GetPhonePoints();
            bonusPoints = GetBonusPoints();
            $("#MainContent_lblTotalBonusPoints").text(bonusPoints);
            //Convo Points           
            convoPoints = GetConvoPoints();
            $("#MainContent_lblTotalConvoPoints").text(convoPoints);
            //RFQ Points
            rfqPoints = GetRFQPoints();
            $("#MainContent_lblTotalRFQPoints").text(rfqPoints);
            //SalesGoal Points
            salesPoints = GetSalesGoalPoints();
            $("#MainContent_lblTotalSalesGoalPoints").text(salesPoints);
            //Total Points
            totalPoints = GetTotalPoints();
            $("#MainContent_lblTotalGoalPoints").text(totalPoints);


        }
        function GetTotalPoints() {
            return bonusPoints + convoPoints + rfqPoints + salesPoints;

        }

        function GetRFQPoints() {
            var ret = 0;
            dailyDt.DataTable().rows({ filter: 'applied' }).every(function (rowIdx, tableLoop, rowLoop) {
                var data = this.data();
                var rfqValue = GetValueFromConcatString(data[3]);
                var intTest = eval(rfqValue);
                if (intTest >= 0)
                    ret += intTest;
            });
            return ret;
        }

        function GetBonusPoints() {
            var ret = 0;
            dailyDt.DataTable().rows({ filter: 'applied' }).every(function (rowIdx, tableLoop, rowLoop) {
                var data = this.data();
                var bonusVal = GetValueFromConcatString(data[2]);
                var intTest = eval(bonusVal);
                if (intTest >= 0)
                    ret += intTest;
            });

            return ret;

        }


        function GetConvoPoints() {
            var ret = 0;
            dailyDt.DataTable().rows({ filter: 'applied' }).every(function (rowIdx, tableLoop, rowLoop) {
                var data = this.data();
                var convoValue = GetValueFromConcatString(data[5]);
                var intTest = eval(convoValue);
                if (intTest >= 0)
                    ret += intTest;
            });

            return ret;
        }

        function GetSalesGoalPoints() {
            var ret = 0;
            dailyDt.DataTable().rows({ filter: 'applied' }).every(function (rowIdx, tableLoop, rowLoop) {
                var data = this.data();
                var salesValue = data[4]
                var intTest = eval(salesValue);
                if (intTest >= 0)
                    ret += intTest;
            });
            return ret;
        }


        function GetValueFromConcatString(v) {
            v = v.substr(0, v.indexOf(' '));
            return v;
        }


        function removeItemFromArray(removeItem, arr) {
            //grep - method removes items from an array as necessary so that all remaining items pass a provided test.   
            arr = jQuery.grep(arr, function (value) {
                return value != removeItem;
            });
            return arr;
        }




        function FilterDate(arrSearch) {
            var filterDetails = $("#cbxSelectedDay").is(':checked');
            if (filterDetails) {
                var selectedDate = $("#MainContent_dtp_txtDate").val();
                var cleanedDate = selectedDate.replace(/\b0/g, ''); //Remove the "0" if any from the day so it can match the grid.
                var year = '/' + (new Date()).getFullYear();

                cleanedDate = cleanedDate.replace(year, '');
                arrSearch.push(cleanedDate);
            }
        }


        function SearchDataTableForArray(dataTable, arrSearch) {
            //Break the array into space-separated strings.
            if ($.fn.DataTable.isDataTable(dataTable)) {
                var strSearch = arrSearch.join(' ');
                dataTable.DataTable().search(strSearch).draw();
            }

        }

        function SearchDataTableForString(dataTable, string) {
            //Break the array into space-separated strings.
            if ($.fn.DataTable.isDataTable(dataTable)) {
                dataTable.DataTable().search(string).draw();
            };
        }


    </script>
    <input type="hidden" id="_ispostback" value="<%=Page.IsPostBack.ToString()%>" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

