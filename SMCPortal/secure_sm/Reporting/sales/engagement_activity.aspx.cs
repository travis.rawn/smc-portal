﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HubspotApis;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Sales_Screens_engagement_activity : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    //RzDataContext rdc = new RzDataContext();
    DateTime lastRzEngagementDate = DateTime.MinValue;
    List<hubspot_engagement> rzEngagementList; //Data from Rz database
    //List<hubspot_engagement> reportData; //Dataset to use for filling grid / ui
    List<HubspotApi.Owner> ownerList = new List<HubspotApi.Owner>();
    List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();
    List<n_user> agentList = new List<n_user>();
    int limit = 0;
    long offSet = 0;


  

    private class CallDetailsObject
    {
        internal string chartColor;
        public string ownerName { get; set; }
        public Dictionary<int, int> callsPerHour { get; set; } //Hour, count
        public Dictionary<int, int> minTimeBetweenCallsPerHour { get; set; }
        public Dictionary<int, int> maxTimeBetweenCallsPerHour { get; set; }
        public Dictionary<int, int> callDurationPerHour { get; internal set; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            dtpSince.SelectedDate = DateTime.Today;
            dtpEnd.SelectedDate = DateTime.Today;
            LoadAgents();
        }

        LoadData();
    }
    //Data Retrieval / Manipulation
    private void LoadAgents()
    {
        List<string> sList = new List<string>() { "sales" };
        rzap.LoadPicker(sList);
        rzap.SelectedUserID = "all";
        agentList = rzap.GetSelectedUsers();
    }
    private void LoadData()
    {
        GetData();
        BindGrid();
        LoadUI();
        LoadChart();

    }

    



    private List<hubspot_engagement> FilterEngagementListByType()
    {
        List<hubspot_engagement> ret = new List<hubspot_engagement>();
        string byType = ddlType.SelectedValue.ToLower();

        foreach (hubspot_engagement eng in rzEngagementList)
        {

            //EngagementObject eo = new EngagementObject();  
            eng.body = eng.body ?? "";

            eng.toNumber = eng.type == "CALL" ? eng.toNumber : "N/A";

            eng.duration = (eng.duration / 1000);//Convert miliseconds to seconds           

            if (byType != "all")
            {
                if (eng.type.ToLower() == byType)
                    if (!ret.Contains(eng))
                        ret.Add(eng);
            }
            else
            {
                if (!ret.Contains(eng))
                    ret.Add(eng);
            }


        }
        return ret;
    }
    private List<hubspot_engagement> FilterEngagementListBySingleAgent()
    {
        //"" = "All"

        List<string> selectedUserIds = agentList.Select(s => s.unique_id).ToList();
        List<string> selectedUserNames = agentList.Select(s => s.name).ToList();
        if (selectedUserIds.Count == 0)
            throw new Exception("No valid users Id's found.");
        if (selectedUserIds.Count > 1)
            return rzEngagementList;
        string selectedOwnerName = selectedUserNames[0];
        //Get list of owners and use that for name-matching, rather than separate API lookup per record

        switch (selectedOwnerName)
        {
            case "Adam Lang":
                selectedOwnerName = "Adam Langenbacher";
                break;
        }

        long selectedOwnerHubspotID = ownerList.Where(w => (w.firstName + " " + w.lastName) == selectedOwnerName).Select(s => s.ownerId).FirstOrDefault();
        //Filter downt he main owner list as it's used later.  Def makes chart data easier to manage

        ownerList = ownerList.Where(w => w.ownerId == selectedOwnerHubspotID).ToList();
        return rzEngagementList.Where(w => w.ownerID == selectedOwnerHubspotID.ToString()).OrderByDescending(o => o.hs_date_created).ToList();
    }
    //Controls
    protected void lbSearch_Click(object sender, EventArgs e)
    {
        //RefreshHubspotData(); //This allows a refresh of data on every search
        //LoadData();
    }
    protected void lbPopulateDB_Click(object sender, EventArgs e)
    {
        //RefreshHubspotData();
        //LoadData();
    }
    protected void lbRefreshRecent_Click(object sender, EventArgs e)
    {
        //RefreshHubspotData();
        // LoadData();
    }
    protected void lbRefreshData_Click(object sender, EventArgs e)
    {
        using (RzDataContext rdc = new RzDataContext())
            offSet = rdc.hubspot_engagements.OrderByDescending(o => o.date_created).First().hubspotID;
        //RefreshHubspotData(offSet);
        //LoadData();
    }


    //UI Operations 
    private void BindGrid()
    {
        var query = rzEngagementList.Select(s => new
        {
            Type = s.type,
            Subject = s.subject,
            Duration = s.duration,
            Phone = s.toNumber,
            //Date = s.hs_date_created.Value.ToShortDateString(),
            Date = s.hs_date_created.Value,
            //Time = s.hs_date_created.Value.ToString("hh:mm"),
            User = s.ownerName

        }).ToList();
        smdt.dataSource = query;
        smdt.loadGrid(true);



        var summaryQuery = rzEngagementList.GroupBy(g => g.ownerName).Select(s => new
        {

            Agent = s.Key,
            Calls = s.Count(w => w.type.ToLower() == "call"),
            Dur = s.Sum(w => w.duration),
            TalkTime = GetMinutesAndSecondsFromSeconds(s.Sum(w => w.duration) ?? 0),
            Emails = s.Count(w => w.type.ToLower() == "email"),
            Notes = s.Count(w => w.type.ToLower() == "notes"),
            Tasks = s.Count(w => w.type.ToLower() == "tasks"),
        });

        smdt_sum.dataSource = summaryQuery;
        Tuple<string, string> visibleTalkTimeColumnData = new Tuple<string, string>("orderData", "2");
        Tuple<string, string> visibleTalkTimeColumnTarget = new Tuple<string, string>("targets", "3");
        smdt_sum.jqColDefs.Add(visibleTalkTimeColumnData);
        smdt_sum.jqColDefs.Add(visibleTalkTimeColumnTarget);

        //These need to be their own objects
        //New Target, the hidded column
        Tuple<string, string> hiddenTalkTimeColumnTargets = new Tuple<string, string>("targets", "2");
        smdt_sum.jqColDefs.Add(hiddenTalkTimeColumnTargets);
        //Hide the column
        Tuple<string, string> hiddenTalkTimeColumnVisible = new Tuple<string, string>("visible", "false");
        smdt_sum.jqColDefs.Add(hiddenTalkTimeColumnVisible);
        //Make it not searchable
        Tuple<string, string> hiddenTalkTimeColumnSearchable = new Tuple<string, string>("searchable", "false");
        smdt_sum.jqColDefs.Add(hiddenTalkTimeColumnSearchable);


        smdt_sum.loadGrid(true);
    }
    private void GetData()
    {
        DateTime start = dtpSince.SelectedDate.Value;
        DateTime end = dtpEnd.SelectedDate.Value;

        List<string> skippedAgentEmails = new List<string>() { "smar@sensiblemicro.com", "tkreider@sensiblemicro.com", "ktill@sensiblemicro.com", "joemar@sensiblemicro.com", "pbravo@sensiblemicro.com", "ctorrioni@sensiblemicro.com", "lmcdonald@sensiblemicro.com", "adevivo@sensiblemicro.com" };
        using (RzDataContext rdc = new RzDataContext())
            rzEngagementList = HubspotLogic.EngagementActivity.GetEngagementList(rdc, start, end, null, null,  skippedAgentEmails);

        //Get the OwnerList
        ownerList = HubspotApi.Owners.GetOwners(rzEngagementList.Select(s => Convert.ToInt64(s.ownerID)).Distinct().ToList());

        //Filter by type
        rzEngagementList = FilterEngagementListByType();

        //Filter SelectedIserIDdata if agent chosen
        if (agentList.Count == 1)
            FilterEngagementListBySingleAgent();


    }



    private void LoadUI()
    {
       

        //Labels  
        if (rzEngagementList != null)
        {
            lblResultCount.Text = rzEngagementList.Count.ToString();
            lbltotalCalls.Text = rzEngagementList.Count(w => w.type == "CALL").ToString() + "  (" + GetMinutesAndSecondsFromSeconds(rzEngagementList.Select(s => s.duration ?? 0).Sum()) + ")";
            lbltotalNotes.Text = rzEngagementList.Count(w => w.type == "NOTE").ToString();
            lbltotalTasks.Text = rzEngagementList.Count(w => w.type == "TASK").ToString();
            lblTotalEmails.Text = rzEngagementList.Count(w => w.type == "EMAIL").ToString();
        }

    }
    private void LoadChart()
    {

        if (rzEngagementList != null)
        {
            List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();
            List<HubspotLogic.EngagementActivity.EngagementActivityObject> eaoList = new List<HubspotLogic.EngagementActivity.EngagementActivityObject>();

            //Datasets - Note I am usign a foreach loop from the owners in ownerList, since some agents won't be present for every list.
            //This way I have a consistent number of items to chart, and the labels and values will match up.
            foreach (HubspotApi.Owner o in ownerList)
            {
                HubspotLogic.EngagementActivity.EngagementActivityObject eao = new HubspotLogic.EngagementActivity.EngagementActivityObject();
                eao.ownerName = o.firstName + " " + o.lastName;
                eao.ownerID = o.ownerId;
                eao.callDuration = rzEngagementList.Where(w => w.ownerID == o.ownerId.ToString() && w.type.ToLower() == "call").Select(s => (s.duration / 60) ?? 0).Sum();
                eao.callCount = rzEngagementList.Count(w => w.ownerID == o.ownerId.ToString() && w.type.ToLower() == "call");                
                eaoList.Add(eao);
            }
            //Sorting this now, which will keep all values in line with the agents.          
            //Check the DropDown for proper sort
            eaoList = GetChartSort(eaoList);

            //Call Duration
            SM_Charts.chartJsDataSet cdsCallDuration = new SM_Charts.chartJsDataSet();
            cdsCallDuration.data = eaoList.Select(s => s.callDuration.ToString()).ToList();
            cdsCallDuration.chartType = SM_Enums.ChartType.line.ToString();
            cdsCallDuration.dataSetLabel = "Call Duration";
            cdsCallDuration.borderColor = SM_Tools.Colors.smNavyRgba;
            cdsCallDuration.fillColors = false;
            //cdsCallDuration.borderColor = new List<string>() { "#000000" };
            datasetList.Add(cdsCallDuration);

            //Call Count
            SM_Charts.chartJsDataSet cdsCalls = new SM_Charts.chartJsDataSet();
            cdsCalls.data = eaoList.Select(s => s.callCount.ToString()).ToList();
            cdsCalls.chartType = SM_Enums.ChartType.bar.ToString();
            cdsCalls.dataSetLabel = "Calls";            
            cdsCalls.fillColors = true;
            cdsCalls.backGroundColor = SM_Tools.Colors.smOrangeRgba;
            datasetList.Add(cdsCalls);

            //The Chart
            SM_Charts.ChartJS chtOverallEngagements = new SM_Charts.ChartJS();
            chtOverallEngagements.chartType = SM_Enums.ChartType.bar.ToString();
            chtOverallEngagements.stacked = true;
            chtOverallEngagements.labels = eaoList.Select(s => s.ownerName).ToList();
            chtOverallEngagements.dataSetList = datasetList;
            chtOverallEngagements.displayXaxisLabel = true;
            chtOverallEngagements.displayYaxisLabel = true;
            chtOverallEngagements.xAxisLabelAutoskip = false;
            chtOverallEngagements.controlPrefix = chtAllEngagement.ClientID; //required if using sm_chartjs
            chtOverallEngagements.canvasID = "cvEngagementActivity";
            chtOverallEngagements.titleText = "Chart: Overall Engagement Activity(Calls, Emails, Notes, Tasks)";
            chtOverallEngagements.informationalText = "Combined Engagement Activity from Hubspot, Rz, and the local phone server.";
            chtOverallEngagements.canvasHeight = 90;


            //Load the html elements of the control (labels, etc)
            chtAllEngagement.LoadChart(chtOverallEngagements);
            //Add chart to master chart list.
            ChartList.Add(chtOverallEngagements);



            List<SM_Charts.chartJsDataSet> callTimeDataSetList = new List<SM_Charts.chartJsDataSet>();
            //List<int> hourList = reportData.Where(w => w.type.ToLower() == "call").OrderBy(o => o.hs_date_created.Value.Hour).Select(s => s.hs_date_created.Value.Hour).Distinct().ToList();
            List<int> hourList = rzEngagementList.OrderBy(o => o.hs_date_created.Value.Hour).Select(s => s.hs_date_created.Value.Hour).Distinct().ToList();
            callTimeDataSetList.Add(GetTotalDurationPerHourDataset(hourList));

            //Each Owner is a dataset, label = owner, data - count / hour    
            List<CallDetailsObject> cdoList = new List<CallDetailsObject>();
            Random rnd = new Random();//            

            foreach (HubspotApi.Owner o in ownerList)
            {
                CallDetailsObject cdo = new CallDetailsObject();
                cdo.ownerName = o.firstName + " " + o.lastName;
                cdo.chartColor = SM_Tools.Colors.GetRandomColor(rnd);
                cdoList.Add(cdo);
                cdo.callsPerHour = calculateCallsPerHour(o, hourList);
            }
            foreach (CallDetailsObject cdo in cdoList)
                callTimeDataSetList.Add(GetCallsPerHourOwnerDataSet(cdo));

            //This is a single dataset, not separated by agent, as it's a line.  To separate by agent, would need  a combination Stacked Bar and Miltibar dataaset, or different charts.  Different charts would be hard to compare in stacked bars.  I think even with normalized colors.
            List<string> CallsPerHourLabelList = convertHourIntToString(hourList);
            SM_Charts.ChartJS ChartCallsPerHour = new SM_Charts.ChartJS();
            ChartCallsPerHour.chartType = SM_Enums.ChartType.bar.ToString();
            ChartCallsPerHour.stacked = true;
            ChartCallsPerHour.labels = CallsPerHourLabelList;
            ChartCallsPerHour.dataSetList = callTimeDataSetList;
            ChartCallsPerHour.displayXaxisLabel = true;
            ChartCallsPerHour.displayYaxisLabel = true;
            ChartCallsPerHour.xAxisLabelAutoskip = false;
            ChartCallsPerHour.controlPrefix = chtPhoneEngagement.ClientID;
            ChartCallsPerHour.canvasID = "cvCallsPerHour";
            ChartCallsPerHour.titleText = "Chart: Calls Per Hour Per Agent";
            ChartCallsPerHour.canvasHeight = 90;



            //Load the html elements of the control (labels, etc)
            chtPhoneEngagement.LoadChart(ChartCallsPerHour);
            //Add chart to master chart list.
            ChartList.Add(ChartCallsPerHour);

            //Load all the chart data
            SM_Charts smr = new SM_Charts();
            smr.LoadAllCharts(Page, ChartList);
        }
    }

    private List<string> convertHourIntToString(List<int> labelList)
    {
        List<string> ret = new List<string>();
        foreach (int i in labelList)
        {
            switch (i)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    ret.Add(i.ToString());
                    break;
                case 12:
                    ret.Add("Noon");
                    break;
                case 13:
                    ret.Add("1");
                    break;
                case 14:
                    ret.Add("2");
                    break;
                case 15:
                    ret.Add("3");
                    break;
                case 16:
                    ret.Add("4");
                    break;
                case 17:
                    ret.Add("5");
                    break;
                case 18:
                    ret.Add("6");
                    break;
                case 19:
                    ret.Add("7");
                    break;


            }
        }

        return ret;
    }

    private Dictionary<int, int> calculateCallsPerHour(HubspotApi.Owner o, List<int> hourList)
    {
        Dictionary<int, int> ret = new Dictionary<int, int>();
        foreach (int i in hourList)
        {
            var count = rzEngagementList.Where(w => w.ownerID == o.ownerId.ToString() && w.type.ToLower() == "call" && w.hs_date_created.Value.Hour == i).Count();
            ret.Add(i, count);
        }
        return ret;

    }

    private SM_Charts.chartJsDataSet GetCallsPerHourOwnerDataSet(CallDetailsObject cdo)
    {
        SM_Charts.chartJsDataSet cdsCallsPerHour = new SM_Charts.chartJsDataSet();
        cdsCallsPerHour.data = cdo.callsPerHour.Select(s => s.Value.ToString()).ToList();
        cdsCallsPerHour.chartType = SM_Enums.ChartType.bar.ToString();
        cdsCallsPerHour.dataSetLabel = cdo.ownerName;
        cdsCallsPerHour.backGroundColor =  cdo.chartColor;
        cdsCallsPerHour.fillColors = true;
        return cdsCallsPerHour;
    }

    private SM_Charts.chartJsDataSet GetTotalDurationPerHourDataset(List<int> hourList)
    {
        Dictionary<int, int> dict = new Dictionary<int, int>();
        foreach (int i in hourList)
        {
            var duration = rzEngagementList.Where(w => w.type.ToLower() == "call" && w.hs_date_created.Value.Hour == i).Sum(s => (s.duration) == null ? 0 : s.duration / 60) ?? 0;
            dict.Add(i, duration);
        }

        //callDurationPerHour = reportData.Where(w => w.type == "call").GroupBy(g => g.hs_date_created.Value.Hour).OrderBy(o => o.Key).ToDictionary(d => d.Key, d => d.Sum(ss => ss.duration ?? 0));

        SM_Charts.chartJsDataSet cdsCallDuration = new SM_Charts.chartJsDataSet();
        cdsCallDuration.data = dict.Select(s => s.Value.ToString()).ToList();
        cdsCallDuration.chartType = SM_Enums.ChartType.line.ToString();
        cdsCallDuration.borderColor = SM_Tools.Colors.smNavyRgba;
        cdsCallDuration.dataSetLabel = "Call Duration";
        cdsCallDuration.fillColors = false;
        //cdsCallDuration.borderColor = new List<string>() { "#000000" };
        return cdsCallDuration;
    }




    private List<HubspotLogic.EngagementActivity.EngagementActivityObject> GetChartSort(List<HubspotLogic.EngagementActivity.EngagementActivityObject> eaoList)
    {
        switch (ddlChartSort.SelectedValue.ToLower())
        {
            case "owner":
                return eaoList.OrderBy(o => o.ownerName).ToList();
            case "duration":
                return eaoList.OrderByDescending(o => o.callDuration).ToList();
            case "calls":
                return eaoList.OrderByDescending(o => o.callCount).ToList();
            case "conversation":
                return eaoList.OrderByDescending(o => o.convoCount).ToList();
            case "notes":
                return eaoList.OrderByDescending(o => o.notesCount).ToList();
            case "emails":
                return eaoList.OrderByDescending(o => o.emailCount).ToList();
            case "tasks":
                return eaoList.OrderByDescending(o => o.tasksCount).ToList();
            default:
                return eaoList.OrderByDescending(o => o.ownerName).ToList();
        }
    }

    //Logic
    private string GetMinutesAndSecondsFromSeconds(int secs)
    {
        TimeSpan t = TimeSpan.FromSeconds(secs);
        string answer = string.Format("{0:D2}h:{1:D2}m:{2:D2}s:{3:D3}ms",
                t.Hours,
                t.Minutes,
                t.Seconds,
                t.Milliseconds);
        string time = t.Hours + ":" + t.Minutes + ":" + t.Seconds;



        return time;
    }


}