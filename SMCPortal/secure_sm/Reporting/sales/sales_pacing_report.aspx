﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="sales_pacing_report.aspx.cs" Inherits="secure_sm_Reporting_sales_sales_pacing_report" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .smdtContainer {
            margin: 0px 60px 0px 60px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>Sales Pacing Report</h2>
    <div class="well">
        <p>For companies with invoices in the past, this report will compare their quott activity to a date range in the future.  Underperforming companies are highlighted YELLOW, and overperforming companies are GREEN.</p>
    </div>
    <div style="text-align: center">


        <div class="row">
            <div class="col-sm-6">
                <h5>Previous Period</h5>
            </div>
            <div class="col-sm-6">
                <h5>Current Period</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpPrevStart" />
            </div>
            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpPrevEnd" />
            </div>
            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpCurrStart" />
            </div>
            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpCurrEnd" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <asp:LinkButton runat="server" ID="lbSearch" OnClick="lbSearch_Click" OnClientClick="ShowUpdateProgress();" CssClass="form-control">Search</asp:LinkButton>
            </div>
            <div class="col-sm-6">
                <asp:LinkButton runat="server" ID="lbExport" Style="cursor: pointer;" OnClick="lbExport_Click" CssClass="form-control" OnClientClick="ShowUpdateProgress();">Export</asp:LinkButton>

            </div>

        </div>

    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">

    <div class="smdtContainer">
        <div class="row">
            <div class="col-sm-12">
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

