﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Reporting_sales_part_activity_report : System.Web.UI.Page
{

    //string PartNumber = "";
    DateTime StartDate;
    DateTime EndDate;
    int MinCount = 3;
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();

    List<string> userIdList = new List<string>();
    //For Each Customer, load their reqs / sales from the part

    //List<PartActivityResult> paResultList = new List<PartActivityResult>();


    //I'll fill this during LoadData
    List<string> companyIDList
    {
        get
        {
            if (ViewState["companyIDList"] != null)
                return (List<string>)(ViewState["companyIDList"]);
            else
                return null;
        }
        set
        {
            ViewState["companyIDList"] = value;
        }
    }

    List<PartActivityResult> paResultList
    {
        get
        {
            if (ViewState["paResultList"] != null)
                return (List<PartActivityResult>)(ViewState["paResultList"]);
            else
            {
                return null;
            }

        }
        set
        {
            ViewState["paResultList"] = value;
        }
    }

    List<PartActivityDetail> paDetailList
    {
        get
        {
            if (ViewState["paDetailList"] != null)
                return (List<PartActivityDetail>)(ViewState["paDetailList"]);
            else
                return null;


        }
        set
        {
            ViewState["paDetailList"] = value;
        }
    }



    [Serializable]
    class PartActivityResult
    {

        public string PartNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public long Quantity { get; set; }
        public string ParentOrderID { get; set; }//group by this //the dealheaderid, formalQuoteID, or Ordhed_Sales_id 
        public string ParentOrderNumber { get; set; }
        public string OrderType { get; set; }
        public string CompanyName { get; set; }
        public string CompanyID { get; set; }


    }
    [Serializable]
    class PartActivityDetail
    {
        public string parentOrderID { get; set; }//group by this //the dealheaderid, formalQuoteID, or Ordhed_Sales_id
        public string detailType { get; set; }
        public string groupedItem { get; set; } //I.e. company, MFG, etc.
        public string partNumber { get; set; }
        public string companyID { get; set; }
        public string companyName { get; set; }
        public string agentID { get; set; }
        public string agentName { get; set; }
        public long totalQuantity { get; set; }
        public DateTime dateCreated { get; set; }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        smdt_excess.RowDataBound += new GridViewRowEventHandler(smdt_excess_RowDataBound);
        LoadControls();
        //if (!Page.IsPostBack)
        ViewState["paResultList"] = LoadResultData();
        LoadResultsGrid();


    }

    protected void smdt_excess_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    CheckBox cbx = new CheckBox();
        //    TableCell cbxCell = new TableCell();
        //    cbxCell.Controls.Add(cbx);
        //    e.Row.Cells[4].Text = "";
        //    e.Row.Cells[4].Controls.Add(cbxCell);
        //}

    }

    private void LoadControls()
    {
        if (!Page.IsPostBack)
        {
            StartDate = DateTime.Today.AddMonths(-3);
            EndDate = DateTime.Today;
            txtMinCount.Text = 3.ToString();
            //ddlReqSale.SelectedValue = "reqs";
        }
        else
        {
            StartDate = dtpStart.SelectedDate.Value;
            EndDate = dtpEnd.SelectedDate.Value;
            int minCount = 0;
            if (Int32.TryParse(txtMinCount.Text.Trim(), out minCount))
                MinCount = minCount;
            else
                HandlResult(txtMinCount.Text.Trim() + " is not a valid integer.");
        }

        dtpStart.SelectedDate = StartDate;
        dtpEnd.SelectedDate = EndDate;


    }

    private List<PartActivityResult> LoadResultData()
    {
        string detailType = ddlReqSale.SelectedValue;
        List<PartActivityResult> ret = new List<PartActivityResult>();
        //containers to hold the data before binding to grid
        List<orddet_quote> quoteDetails = new List<orddet_quote>();
        List<orddet_line> salesDetails = new List<orddet_line>();

        using (RzDataContext rdc = new RzDataContext())
        {
            if (detailType == "reqs" || detailType == "all")
            {
                quoteDetails = rdc.orddet_quotes.Where(w => w.date_created >= StartDate && w.date_created <= EndDate && w.fullpartnumber.Length > 0 && w.base_company_uid.Length > 0 && w.base_mc_user_uid.Length > 0 && w.target_quantity > 0).ToList();
                if (ddlAgentType.SelectedValue != "all")
                    userIdList = GetIserIDsByType(rdc);
                if (userIdList.Count > 0)
                    quoteDetails = quoteDetails.Where(w => userIdList.Contains(w.base_mc_user_uid)).ToList();
            }

            if (detailType == "sales" || detailType == "all")
            {
                salesDetails = rdc.orddet_lines.Where(w => w.date_created >= StartDate && w.date_created <= EndDate && w.fullpartnumber.Length > 0
                && w.orderid_sales.Length > 0 && w.orderid_sales != null
           && w.customer_uid.Length > 0 && w.seller_uid.Length > 0 && w.quantity > 0 && w.status.ToLower() == "shipped" && w.stocktype.ToLower() != "service").ToList();

                if (ddlAgentType.SelectedValue != "all")
                    userIdList = GetIserIDsByType(rdc);
                if (userIdList.Count > 0)
                    salesDetails = salesDetails.Where(w => userIdList.Contains(w.seller_uid)).ToList();
            }

        }




        if (quoteDetails.Count > 0)
        {
            List<PartActivityResult> paQuoteList = quoteDetails.Select(s => new PartActivityResult
            {
                ParentOrderID = s.base_dealheader_uid,
                OrderType = "Quote",
                ParentOrderNumber = "Batch: " + s.date_created.Value.ToShortDateString(),
                PartNumber = s.fullpartnumber.ToUpper().Trim(),
                DateCreated = s.date_created.Value.Date,
                Quantity = s.quantity ?? 0,
                CompanyName = s.companyname,
                CompanyID = s.base_company_uid,
            }).ToList();
            if (paQuoteList.Count > 0)
                ret.AddRange(paQuoteList);
        }
        if (salesDetails.Count > 0)
        {
            List<PartActivityResult> paSalesList = salesDetails.Select(s => new PartActivityResult
            {
                ParentOrderID = s.orderid_sales,
                OrderType = "Sale",
                ParentOrderNumber = s.ordernumber_sales,
                PartNumber = s.fullpartnumber.ToUpper().Trim(),
                DateCreated = s.date_created.Value.Date,
                Quantity = s.quantity ?? 0,
                CompanyName = s.customer_name,
                CompanyID = s.customer_uid,
            }).ToList();
            if (paSalesList.Count > 0)
                ret.AddRange(paSalesList);
        }
        return ret;

    }

    private void LoadResultsGrid()
    {

        if (paResultList.Count <= 0)
        {
            gvResults.DataSource = null;

        }
        else
        {

            ////AddCompanyIDsToGlobalList();
            //List<PartActivityResult> groupedList = ret.GroupBy(x => new { x.PartNumber, x.ParentOrderID })
            //             .Select(g => new PartActivityResult
            //             {
            //                 PartNumber = g.Key.PartNumber,
            //                 TotalQuantity = g.Sum(s => s.TotalQuantity),





            //                 ParentOrderID = g.Key.ParentOrderID,
            //                 OrderType = string.Join(",", g.Select(ss => ss.OrderType).Distinct().ToList()),


            //                 MostRecentDate = g.Max(m => m.MostRecentDate),
            //                 ResultCount = g.Count()
            //             }).ToList();

            //if (MinCount > 0)
            //{
            //    ret = groupedList.Where(w => w.ResultCount >= MinCount).ToList();
            //}





            var query = paResultList.GroupBy(g => g.PartNumber).Select(s => new
            {
                PartNumber = s.Key,
                Quantity = s.Sum(ss => ss.Quantity),
                CompanyCount = s.Select(ss => ss.CompanyID).Distinct().Count(),
                SaleCount = s.GroupBy(g => g.ParentOrderID).Where(w => w.Max(m => m.OrderType == "Sale")).Count(),
                QuoteCount = s.GroupBy(g => g.ParentOrderID).Where(w => w.Max(m => m.OrderType == "Quote")).Count(),

            }).Where(w => w.QuoteCount >= MinCount);



            gvResults.DataSource = query.OrderByDescending(o => o.Quantity);
        }
        gvResults.DataBind();
        tools.LoadGridDataDataTablesNetDefault(Page, gvResults);
    }



    private void LoadActivityDetailList(string partNumber)
    {
        //List<PartActivityDetail> padList = new List<PartActivityDetail>();


        //List<PartActivityDetail> padQuoteList = new List<PartActivityDetail>();
        //List<orddet_quote> partQuotesList = new List<orddet_quote>();
        //if (ddlReqSale.SelectedValue == "reqs" || ddlReqSale.SelectedValue == "all")
        //    partQuotesList = paResultList.Where(w => w.fullpartnumber.Trim().ToUpper() == partNumber.Trim().ToUpper()).ToList();

        //List<PartActivityDetail> padSaleList = new List<PartActivityDetail>();
        //List<orddet_line> partSalesList = new List<orddet_line>();
        //if (ddlReqSale.SelectedValue == "sales" || ddlReqSale.SelectedValue == "all")
        //    partSalesList = salesDetails.Where(w => w.fullpartnumber.Trim().ToUpper() == partNumber.Trim().ToUpper()).ToList();




        ////If quote results, add them to the padList
        //if (partQuotesList.Count > 0)
        //{
        //    //var query = partQuotesList.GroupBy(g => g.base_company_uid).Select(s => new PartActivityDetail
        //    var query = partQuotesList.Select(s => new PartActivityDetail

        //    {

        //        partNumber = s.fullpartnumber,
        //        detailType = "Req",
        //        companyID = s.base_company_uid,
        //        companyName = s.companyname,
        //        agentID = s.base_mc_user_uid,
        //        agentName = s.agentname,
        //        totalQuantity = (long)s.quantity,
        //        parentOrderID = s.base_dealheader_uid,
        //        dateCreated = s.date_created.Value.Date

        //    });

        //    padList.AddRange(query.ToList());
        //}

        ////If sale results, add them to the padList
        //if (partSalesList.Count > 0)
        //{
        //    var query = partSalesList.Select(s => new PartActivityDetail
        //    {
        //        partNumber = s.fullpartnumber,
        //        detailType = "Sale",
        //        companyID = s.customer_uid,
        //        companyName = s.customer_name,
        //        agentID = s.seller_uid,
        //        agentName = s.seller_name,
        //        totalQuantity = (long)s.quantity,
        //        parentOrderID = s.orderid_sales,
        //        dateCreated = s.date_created.Value.Date

        //    });

        //    padList.AddRange(query.ToList());
        //}




        //SaleCount = s.GroupBy(g => g.ParentOrderID).Where(w => w.Max(m => m.OrderType == "Sale")).Count(),
        //        QuoteCount = s.GroupBy(g => g.ParentOrderID).Where(w => w.Max(m => m.OrderType == "Quote")).Count(),
        //company, date, quantity, type
        var query = paResultList.Where(w =>w.PartNumber.Trim().ToUpper() == partNumber.Trim().ToUpper());



        var finalQuery = query.GroupBy(g => g.ParentOrderID).Select(s => new
        {
            Date = s.Max(m => m.DateCreated).ToShortDateString(),
            Company = s.Max(m => m.CompanyName),
            QTY = s.Sum(ss => ss.Quantity),
            Type = s.Max(m => m.OrderType),
            Order = s.Max(m => m.ParentOrderNumber)


        });



        //var finalQuery = padList.GroupBy(g => g.parentOrderID).Select(s => new
        //{

        //    Type = s.Max(m => m.detailType),
        //    Date = s.Max(m => m.dateCreated).ToShortDateString(),
        //    Company = s.Max(m => m.companyName),
        //    Agent = s.Max(m => m.agentName),
        //    TotalQty = s.Sum(m => m.totalQuantity)

        //});


        lblTotalQuotes.Text = finalQuery.Where(w => w.Type == "Quote").Count().ToString();
        lblTotalSales.Text = finalQuery.Where(w => w.Type == "Sale").Count().ToString();
        lblTotal.Text = finalQuery.Count().ToString();



        smdtDetails.dataSource = finalQuery;
        smdtDetails.loadGrid();
        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#divModal').modal('show');", true);
        //ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#divModal').modal('show');", true);


    }

    private List<string> GetIserIDsByType(RzDataContext rdc)
    {
        string selectedTeam = ddlAgentType.SelectedValue;
        if (selectedTeam.Trim().ToLower() == "disty")
            selectedTeam = "Distributor Sales";

        List<string> teamNames = new List<string>() { selectedTeam };
        List<n_user> uList = rzt.GetUsersForTeams(teamNames);
        return uList.Select(s => s.unique_id).Distinct().ToList();
    }


    protected void lbRefresh_Click(object sender, EventArgs e)
    {

    }

    protected void lbDetails_Click(object sender, EventArgs e)
    {

        LinkButton lbDetails = sender as LinkButton;
        GridViewRow row = lbDetails.NamingContainer as GridViewRow;
        string partNumber = gvResults.DataKeys[row.RowIndex].Values[0].ToString();
        if (string.IsNullOrEmpty(partNumber))
            throw new Exception("No part number detected from click.");
        //This needs to update Viewstate as it's an explicit user click, params may have changed.
        LoadResultData();
        LoadActivityDetailList(partNumber);
        lblActivityDetailLabel.Text = partNumber.Trim().ToUpper();



    }

    private void HandlResult(string msg)
    {
        tools.HandleResult("fail", msg);
    }


    public class ExcessScrubResult
    {
        public string partNumber { get; set; }
        public string vendor_uid { get; set; }
        public string vendor_names { get; set; }
        public string csutomer_names { get; set; }
        public long total_qty { get; set; }
    }

    protected void lbExcessScrub_Click(object sender, EventArgs e)
    {
        try
        {

            DateTime startTime = DateTime.Now;
            //For all parts in grid
            //Check Excess lists for matching parts
            //List Summed QTY for each matching excess Vendor
            if (paResultList == null || paResultList.Count <= 0)
                return;
            List<ExcessScrubResult> esrList = new List<ExcessScrubResult>();
            //Get a list of unique part numbers from the datasource
            List<string> uniquePartsList = paResultList.Select(s => s.PartNumber.Trim().ToUpper()).Distinct().ToList();

            using (RzDataContext rdc = new RzDataContext())
            {
                List<orddet_quote> quoteList = rdc.orddet_quotes.Where(w => uniquePartsList.Contains(w.fullpartnumber.Trim().ToUpper())).ToList();
                foreach (String s in uniquePartsList)
                {
                    ExcessScrubResult esr = new ExcessScrubResult();
                    List<partrecord> excessList = rdc.partrecords.Where(w => w.stocktype.ToLower() == SM_Enums.StockType.excess.ToString() && w.fullpartnumber == s).ToList();
                    if (excessList == null || excessList.Count <= 0)
                        continue;
                    esr.partNumber = s;
                    esr.total_qty = excessList.Sum(ss => ss.quantity ?? 0 - ss.quantityallocated ?? 0);
                    List<string> unique_vendors = excessList.Select(ss => ss.companyname).Distinct().ToList();
                    string vendors_Concat = string.Join(",", unique_vendors);
                    esr.vendor_names = vendors_Concat;

                    List<string> unique_customers = quoteList.Where(w => w.fullpartnumber.Trim().ToUpper() == s.Trim().ToUpper()).Select(ss => ss.companyname).Distinct().ToList();
                    string customers_Concat = string.Join(",", unique_customers);
                    esr.csutomer_names = customers_Concat;

                    esrList.Add(esr);
                }
            }

            if (esrList == null || esrList.Count <= 0)
                throw new Exception("No Excess Scrub results found.");

            smdt_excess.dataSource = esrList.Select(s => new { PartNumber = s.partNumber, Customers = s.csutomer_names, Vendors = s.vendor_names, Qty = s.total_qty, Quote = "Get Quote" });
            smdt_excess.loadGrid();
            DateTime endTime = DateTime.Now;
            double totalSeconds = (endTime - startTime).TotalSeconds;
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#divEsrModal').modal('show');", true);
            //tools.HandleResult("success", "Success: " + esrList.Count() + " parts identified on excess lists. Duration: " + totalSeconds + " seconds.", 0);

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }




    }
}