﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using SensibleDAL;



public partial class secure_sm_Reporting_management_sales_theory_report : System.Web.UI.Page
{


    public class newCompanyObj
    {
        public string companyName { get; set; }
        public string agentName { get; set; }
        public string agentId { get; set; }
        public double gpSinceAquisition { get; set; }
        public string firstOrderNumber { get; set; }
        public string firstOrderDate { get; set; }
    }



    SM_Tools tools = new SM_Tools();
    RzDataContext rdc = new RzDataContext();
    RzTools rzt = new RzTools();
    SalesTheory st;

    //Start of Week
    public DateTime startOfWeek { get; set; }//based on current week of selected date
    //End of Week
    public DateTime endOfWeek { get; set; }
    //Currently Selected Day
    public DateTime selectedDay { get; set; }
    //Master Engagement List Dataset
    public List<hubspot_engagement> engagementList = new List<hubspot_engagement>();
    //List of agents to include
    public List<n_user> agentList = new List<n_user>();
    //Helper List for the Ids
    public List<string> SelectedAgentIds = new List<string>();



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            dataSection.Visible = false;
            //smdtPhoneCallPoints.RowDataBound += new GridViewRowEventHandler(smdtPhoneCallPoints_RowDataBound);
            smdtBonusPoints.RowDataBound += new GridViewRowEventHandler(smdtPhoneCallPoints_RowDataBound);
            smdtConversationPoints.RowDataBound += new GridViewRowEventHandler(smdtConversationPoints_RowDataBound);
            LoadDates();
            if (!Page.IsPostBack)
            {
                LoadAgentPicker();
            }
            LoadAgents();
            if (agentList.Count <= 0)
                return;
            engagementList = HubspotLogic.EngagementActivity.GetEngagementList(rdc,startOfWeek, endOfWeek);

            if (engagementList.Count == 0)
                return;
            st = new SalesTheory();
            st.Load(engagementList, agentList, startOfWeek, endOfWeek, SM_Enums.BonusCategoryName.PhoneCallCount);

            BindAllGrids();
            dataSection.Visible = true;
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            dataSection.Visible = false;
        }
    }

    private void smdtConversationPoints_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridViewRow row = e.Row;
            TableCell cell = row.Cells[7];
            string url = cell.Text.Trim();
            cell.Text = url;
            if (url != "N/A")
            {
                HyperLink hl = new HyperLink();
                hl.Target = "_blank";
                hl.Text = "Link";
                hl.NavigateUrl = url.Trim();
                cell.Controls.Add(hl);
            }
            TableCell bodyCell = row.Cells[4];
            string notesText = HttpUtility.HtmlDecode(bodyCell.Text);
            notesText = HttpUtility.HtmlDecode(Regex.Replace(notesText, "<[^>]*(>|$)", string.Empty)).Trim();
            if (string.IsNullOrEmpty(notesText))
                bodyCell.Text = "No Notes";
            else
                bodyCell.Text = "<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + notesText + "\"><i style=\"font-size:20px;\" class=\"far fa-comment-alt\" ></i></a>";
        }
    }

    private void smdtPhoneCallPoints_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridViewRow row = e.Row;

            TableCell cell = row.Cells[7];
            string url = cell.Text.Trim();
            cell.Text = url;
            if (url != "N/A")
            {
                HyperLink hl = new HyperLink();
                hl.Target = "_blank";
                hl.Text = "Link";
                hl.NavigateUrl = url.Trim();
                cell.Controls.Add(hl);
            }



            TableCell bodyCell = row.Cells[4];
            string notesText = HttpUtility.HtmlDecode(bodyCell.Text);
            notesText = HttpUtility.HtmlDecode(Regex.Replace(notesText, "<[^>]*(>|$)", string.Empty)).Trim();
            if (string.IsNullOrEmpty(notesText))
                bodyCell.Text = "No Notes";
            else
                bodyCell.Text = "<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + notesText + "\"><i style=\"font-size:20px;\" class=\"far fa-comment-alt\" ></i></a>";


        }
    }








    //private List<ordhed_sale> GetSalesPointsQuery()
    //{
    //    List<ordhed_sale> sourceData = rdc.ordhed_sales.Where(w => ((DateTime)w.date_created >= startOfWeek && (DateTime)w.date_created < endOfWeek) && ((bool)w.isvoid != true) && SelectedAgentIds.Contains(w.base_mc_user_uid)).ToList();
    //    return sourceData;
    //}

    private void BindAllGrids()
    {
        //Same DataSet for both Grids, client side ui will filter Daily grid
        //var query = st.weeklySalesTheoryObjects.Select(s => new { Agent = s.agentName,PhoneCalls = s.phonecallPoints + " (" + s.phonecallCount + ")", RFQs = s.rfqPoints + " (" + s.rfqCount + ")", Sales = s.salesGoalPoints, Conversations = s.conversationPoints + " (" + s.conversationCount + ")", Total = s.currentPoints });
        var query = st.weeklySalesTheoryObjects.Select(s => new { Agent = s.agentName, BonusPoints = s.bonusPoints + " (" + s.bonusCount + ")", RFQs = s.rfqPoints + " (" + s.rfqCount + ")", Sales = s.salesGoalPoints, Conversations = s.conversationPoints + " (" + s.conversationCount + ")", Total = s.currentPoints });

        //LoadGrid(query, smdtDaily);
        LoadGrid(query.OrderByDescending(o => o.Total), smdtWeekly);

        //Weekly Agent Objects
        //LoadGrid(weeklySalesTheoryObjects.Select(s => new { Agent = s.agentName, PhoneCalls = s.phonecallPoints + " (" + s.phonecallCount + ")", RFQs = s.rfqPoints + " (" + s.rfqCount + ")", Sales = s.salesGoalPoints + " (" + s.salesGoalCount + ")", Conversations = s.conversationPoints + " (" + s.conversationCount + ")", Total = s.currentPoints }), smdtWeekly);

        //Daily Agent Objects 
        LoadDailyAgentObjectGrid();
        //LoadGrid(weeklySalesTheoryObjects.Select(s => new { Agent = s.agentName, PhoneCalls = s.phonecallPoints + " (" + s.phonecallCount + ")", RFQs = s.rfqPoints + " (" + s.rfqCount + ")", Sales = s.salesGoalPoints + " (" + s.salesGoalCount + ")", Conversations = s.conversationPoints + " (" + s.conversationCount + ")", Total = s.currentPoints }), smdtDaily);

        //Weekly Agent Objects
        //LoadGrid(weeklySalesTheoryObjects.Select(s => new { Agent = s.agentName, PhoneCalls = s.phonecallPoints + " (" + s.phonecallCount + ")", RFQs = s.rfqPoints + " (" + s.rfqCount + ")", Sales = s.salesGoalPoints + " (" + s.salesGoalCount + ")", Conversations = s.conversationPoints + " (" + s.conversationCount + ")", Total = s.currentPoints }), smdtWeekly);

        //if (SalesTheory.phoneCallQuery.Count > 0)

        //    //PhoneCall Points       
        //    if (SalesTheory.phoneCallQuery.Count() > 0)
        //        //LoadGrid(phoneCallQuery.Where(w => ((DateTime)w.hs_date_created >= selectedDay)).Select(s => new { Agent = s.ownerName, Date = s.hs_date_created.Value.ToShortTimeString(), Number = s.toNumber, Status = s.status, Duration = s.duration == null ? 0 : (s.duration / 1000), Result = s.call_disposition, Rec = s.recording_url ?? "N/A" }), smdtPhoneCallPoints);
        //        LoadGrid(SalesTheory.phoneCallQuery.Select(s => new { Agent = s.ownerName, Date = s.hs_date_created.Value.ToShortDateString(), Time = s.hs_date_created.Value.ToShortTimeString(), Number = s.toNumber, Body = s.body ?? "", Duration = s.duration == null ? 0 : (s.duration / 1000), Result = s.call_disposition, Rec = s.recording_url ?? "N/A" }), smdtPhoneCallPoints);

        //    else
        //        LoadGrid(null, smdtPhoneCallPoints);


        //Bonus Points
        if (SalesTheory.bonusQuery.Count > 0)

            //PhoneCall Points       
            if (SalesTheory.bonusQuery.Count() > 0)
                //LoadGrid(phoneCallQuery.Where(w => ((DateTime)w.hs_date_created >= selectedDay)).Select(s => new { Agent = s.ownerName, Date = s.hs_date_created.Value.ToShortTimeString(), Number = s.toNumber, Status = s.status, Duration = s.duration == null ? 0 : (s.duration / 1000), Result = s.call_disposition, Rec = s.recording_url ?? "N/A" }), smdtPhoneCallPoints);
                LoadGrid(SalesTheory.bonusQuery.Select(s => new { Agent = s.ownerName, Date = s.hs_date_created.Value.ToShortDateString(), Time = s.hs_date_created.Value.ToShortTimeString(), Number = s.toNumber, Body = s.body ?? "", Duration = s.duration == null ? 0 : (s.duration / 1000), Result = s.call_disposition, Rec = s.recording_url ?? "N/A" }), smdtBonusPoints);

            else
                LoadGrid(null, smdtBonusPoints);



        //Conversation Points
        if (SalesTheory.conversationQuery.Count() > 0)
            LoadGrid(SalesTheory.conversationQuery.Select(s => new { Agent = s.ownerName, Date = s.hs_date_created.Value.ToShortDateString(), Time = s.hs_date_created.Value.ToShortTimeString(), Number = s.toNumber, Body = s.body ?? "", Duration = s.duration == null ? 0 : (s.duration / 1000), Result = s.call_disposition, Rec = s.recording_url ?? "N/A" }).ToList(), smdtConversationPoints);
        else
            LoadGrid(null, smdtConversationPoints);

        //RFQ Points
        if (SalesTheory.rfqQuery.Count() > 0)
            LoadGrid(SalesTheory.rfqQuery.Select(s => new { Agent = s.agentname, Date = s.date_created.Value.ToShortDateString(), Customer = s.customer_name, Parts = s.internal_parts }), smdtRfqPoints);
        else
            LoadGrid(null, smdtRfqPoints);

        //Sales Goal Points  
        if (SalesTheory.salesGoalPointsQuery.Count() > 0)
            //LoadGrid(SalesTheory.salesGoalPointsQuery.Select(s => new { Agent = s.agentname, Customer = s.companyname, Date = s.orderdate.Value.ToShortDateString(), Total = s.ordertotal }), smdtSalesGoalPoints);
        LoadGrid(SalesTheory.salesGoalPointsQuery.Select(s => new { Agent = s.seller_name, Customer = s.customer_name, Date = s.orderdate_sales.Value.ToShortDateString(), Total = s.gross_profit }), smdtSalesGoalPoints);

        else
            LoadGrid(null, smdtSalesGoalPoints);

        //New Customers       
        //LoadGrid(newCustomerData.Select(s => new { Agent = s.agentName, Company = s.companyName, GPSince = s.gpSinceAquisition, Order = s.firstOrderNumber, Date = s.firstOrderDate }), smdtNewCustomers);

    }
    class dailyAgentObject
    {
        public DateTime Date { get; set; }
        public string Agent { get; set; }
        //public double PhoneCallPoints { get; set; }
        //public double PhoneCallCount { get; set; }
        public double BonusPoints { get; set; }
        public double BonusCount { get; set; }
        public string BonusTitle { get; set; }
        public string BonusIconClass { get; set; }

        public double RFQPoints { get; set; }
        public int RFQCount { get; set; }
        public double Sales { get; set; }
        public int ConversationPoints { get; set; }
        public int ConversationCount { get; set; }
        public double Total { get; set; }


    }

   

    private void LoadDailyAgentObjectGrid()
    {
        List<dailyAgentObject> dList = new List<dailyAgentObject>();

        foreach (KeyValuePair<DateTime, List<SalesTheory.SalesTheoryObject>> kvp in st.dailySalesTheoryObjects)
        {

            foreach (SalesTheory.SalesTheoryObject sto in kvp.Value)
            {
                dailyAgentObject d = new dailyAgentObject();
                d.Date = kvp.Key;
                d.Agent = sto.agentName;
                d.BonusPoints = sto.bonusPoints;
                d.BonusCount = sto.bonusCount;
                d.RFQPoints = sto.rfqPoints;
                d.RFQCount = sto.rfqCount;
                d.BonusTitle = sto.bonusTitle;
                d.BonusIconClass = sto.bonusIconClass;
                d.Sales = sto.salesGoalPoints;
                d.ConversationPoints = sto.conversationPoints;
                d.ConversationCount = sto.conversationCount;
                d.Total = sto.bonusPoints + sto.rfqPoints + sto.salesGoalPoints + sto.conversationPoints;
                dList.Add(d);
            }
        }

        SetBonusUIProperties(dList);


        string strCurrentYear = "/" + DateTime.Now.Year.ToString();
        var query = dList.Select(s => new
        {

            Date = (s.Date.ToShortDateString()).Replace(strCurrentYear, ""),
            s.Agent,
            Bonus = s.BonusPoints + " (" + s.BonusCount + ")",
            RFQs = s.RFQPoints + " (" + s.RFQCount + ")",
            s.Sales,
            Conversations = s.ConversationPoints + " (" + s.ConversationCount + ")",
            s.Total

        }).OrderByDescending(o => o.Total);
        LoadGrid(query, smdtDaily);

    }

    private void SetBonusUIProperties(List<dailyAgentObject> dList)
    {
        //Since these will be the same for every sto in the current implementaion, just need first of default icon-class and title

        string BonusTitle = dList.Select(s => s.BonusTitle).FirstOrDefault();
        string BonusIconClass = dList.Select(s => s.BonusIconClass).FirstOrDefault();

        lblBonusSquareTitle.Text = BonusTitle;
        icoBonusIcon.Attributes.Add("class", BonusIconClass);

    }

    private void LoadAgentPicker()
    {


        List<string> disallowedUserEmails = new List<string>() { "ctorrioni@sensiblemicro.com", "lmcdonald@sensiblemicro.com" };
        List<string> teamList = new List<string>() { "sales", "distributor sales" };
        agentList = rzt.GetUsersForTeams(teamList);
        agentList = agentList.Where(w => !disallowedUserEmails.Contains(w.email_address.ToLower())).ToList();
        rzap.LoadPicker(agentList);
        SelectedAgentIds = agentList.Select(s => s.unique_id).ToList();
        ViewState["SelectedAgentIds"] = SelectedAgentIds;

        if (isAdminUser())
        {
            lblGraphicSelectedAgent.Text = "All Users";
            rzap.SelectedUserID = "all";
        }
        else //default it to the logged in agent's name.
        {
            string rzId = Profile.RzUserID;
            if (string.IsNullOrEmpty(rzId))
                throw new Exception("Invalid Profile ID");
            rzap.SelectedUserID = rzId;
            lblGraphicSelectedAgent.Text = HttpContext.Current.User.Identity.Name;
        }
    }

    private bool isAdminUser()
    {
        return tools.IsInAnyRole(Membership.GetUser().UserName, new List<string>() { "admin", "sm_internal_exec" });
    }

    private void LoadAgents()
    {
        SelectedAgentIds = (List<string>)ViewState["SelectedAgentIds"];
        agentList = rdc.n_users.Where(w => SelectedAgentIds.Contains(w.unique_id)).ToList();
    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
            dtp.SelectedDate = DateTime.Today;

        startOfWeek = Tools.Dates.GetFirstDateOfWeek(dtp.SelectedDate.Value).AddDays(1);
        selectedDay = dtp.SelectedDate.Value;
        endOfWeek = startOfWeek.AddDays(5);

        //Section Labels
        //lblSelectedDay.Text = selectedDay.ToShortDateString();
        lblWeekRange.Text = startOfWeek.ToShortDateString() + " - " + endOfWeek.ToShortDateString();

    }

    private void LoadGrid(IEnumerable<object> query, sm_datatable smdt)
    {
        smdt.jqColDefs = new List<Tuple<string, string>>();
        Tuple<string, string> responsivePriority1stCol = new Tuple<string, string>("responsivePriority", " 1, targets: 0");
        Tuple<string, string> responsivePriorityLastCol = new Tuple<string, string>("responsivePriority", " 2, targets: -1");
        smdt.jqColDefs.Add(responsivePriority1stCol);
        smdt.jqColDefs.Add(responsivePriorityLastCol);
        smdt.pageLength = 25;

        smdt.theGridView.DataSource = null;
        smdt.theGridView.DataBind();
        smdt.dataSource = query;
        smdt.loadGrid(true);
    }

}