﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using System.Text;
using System.Web.UI.HtmlControls;

public partial class secure_sm_Reports_sales_charts : System.Web.UI.Page
{

    RzDataContext rdc = new RzDataContext();
    SM_Charts smr = new SM_Charts();
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();
    SM_Enums e = new SM_Enums();

    //List<SM_Charts.ChartScript> csList = new List<SM_Charts.ChartScript>();
    List<orddet_quote> QuoteLinesInDateRange = new List<orddet_quote>();
    List<orddet_line> LinesInDateRange = new List<orddet_line>();


    List<SalesLogic.OrderQueryResult> queryQuotedRz = new List<SalesLogic.OrderQueryResult>();
    List<SalesLogic.OrderQueryResult> bookedQuery = new List<SalesLogic.OrderQueryResult>();
    List<SalesLogic.OrderQueryResult> invoicedQuery = new List<SalesLogic.OrderQueryResult>();

    List<string> notIncludedAgents = new List<string>() { "CFT", "Recognin Technologies", "House", "Jen Canizio", "Vendor", "Dead Accounts (Do Not Call)", "E-Sales", "Adam Lang (Soft Phone)", "Chris Torrioni(SalesPhone)" };
    List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();

    List<n_user> includedAgents = new List<n_user>();
    //List<string> includedAgentNames = new List<string>();
    //List<string> includedAgentIDs = new List<string>();
    public List<string> includedAgentNames { get { return ViewState["includedAgentNames"] as List<string>; } set { ViewState["includedAgentNames"] = value; } }
    public List<string> includedAgentIDs { get { return ViewState["includedAgentIDs"] as List<string>; } set { ViewState["includedAgentIDs"] = value; } }
    string noDataText = " No Data";


    private class OrderDataObject
    {

        public List<string> InvoiceIDs { get; set; }
        public string SellerID { get; set; }
        public string Name { get; set; }
        public double Total { get; set; }
        public double GP { get; set; }
        public double NP { get; set; }
        public OrderDataObject(List<string> invoiceIDs, string sellerID, string name, double total, double gp, double np)
        {
            InvoiceIDs = invoiceIDs;
            SellerID = sellerID;
            Name = name;
            Total = total;
            GP = gp;
            NP = np;
        }
        //Other properties, methods, events...
    }




    string runningResults;
    decimal pctReqToQuote;
    protected void Page_Load(object sender, EventArgs e)
    {
        bool debugLog = Membership.GetUser().UserName.ToLower() == "kevint";
        string debugMessage = "Debug log: </br>";
        try
        {
            pnlCharts.Visible = false;

            if (!Page.IsPostBack)
            {
                LoadDates();

                //return;
            }
            LoadIncludedAgents();
            DateTime startTime = DateTime.Now;

            DateTime dtLinesStart = DateTime.Now;
            LoadData();
            debugMessage += "Lines loaded in " + Tools.Dates.GetTimeSpan(dtLinesStart).TotalSeconds + " seconds. <br />";
            DateTime dtAgentsStart = DateTime.Now;

            debugMessage += "Lines loaded in " + Tools.Dates.GetTimeSpan(dtAgentsStart).TotalSeconds + " seconds. <br />";
            DateTime dtLoadLines = DateTime.Now;




            if (includedAgentIDs.Count <= 0)
                return;
            //Load Included agents, can't be serialized into Viewstate unfortunately.
            includedAgents = rdc.n_users.Where(w => includedAgentIDs.Contains(w.unique_id)).ToList();

            //Sales Breakdown
            DateTime salesBreakDownStart = DateTime.Now;
            string salesBreakdownDebugLog = "";
            LoadSalesBreakdown(out salesBreakdownDebugLog);
            salesBreakdownDebugLog += " Sales Breakdown completed in " + Tools.Dates.GetTimeSpan(salesBreakDownStart).TotalSeconds + " seconds. <br />";
            debugMessage += salesBreakdownDebugLog;

            //Projected GP
            DateTime dtProjectedStart = DateTime.Now;
            LoadProjectedGP();
            debugMessage += "Projected GP completed in " + Tools.Dates.GetTimeSpan(dtProjectedStart).TotalSeconds + " seconds.<br />";

            //Book to Bill
            // LoadBookToBill();

            //Deductions
            DateTime dtDeductionsStart = DateTime.Now;
            LoadDeductions();
            debugMessage += "Deductions loaded in " + Tools.Dates.GetTimeSpan(dtDeductionsStart).TotalSeconds + " seconds.<br />";

            //Load Charts
            DateTime dtLoadChartsStart = DateTime.Now;
            smr.LoadAllCharts(Page, ChartList);
            debugMessage += "Charts loaded in " + Tools.Dates.GetTimeSpan(dtLoadChartsStart).TotalSeconds + " seconds.<br />";
            pnlCharts.Visible = true;

            TimeSpan tsTotal = Tools.Dates.GetTimeSpan(startTime);
            //string successMessage = "All queries completed in " + tsTotal.TotalSeconds;
            //if (debugLog)
            //    successMessage += "<br /><br />" + debugMessage;
            //tools.HandleResult("success", successMessage, 0);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void LoadData()
    {
        QuoteLinesInDateRange = rdc.orddet_quotes.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate.Value && w.date_created.Value.Date <= dtpEnd.SelectedDate.Value).ToList();
        //ViewState["QuoteLines"] = QuoteLinesInDateRange;

        LinesInDateRange = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate.Value && w.date_created.Value.Date <= dtpEnd.SelectedDate.Value).ToList();
        //ViewState["OrderLines"] = LinesInDateRange;

    }

    protected void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //<a href="your link here"> <i class="fa fa-dribbble fa-4x"></i></a>
            TableCell linkCell = e.Row.Cells[11];
            HtmlGenericControl anchor = new HtmlGenericControl();


            if (!string.IsNullOrEmpty(linkCell.Text) && (linkCell.Text ?? "") != "&nbsp;")
            {
                HyperLink hl = new HyperLink();
                hl.Text = "Track";
                hl.Target = "_Blank";
                hl.NavigateUrl = linkCell.Text;
                linkCell.Text = "";
                linkCell.Controls.Add(hl);
            }
        }

    }



    private void LoadDates()
    {
        //default to MTD

        Tuple<DateTime, DateTime> monthRange = Tools.Dates.GetQuarterDateRange(DateTime.Today);
        dtpStart.SelectedDate = monthRange.Item1;
        dtpEnd.SelectedDate = monthRange.Item2;
        //dtpStart.SelectedDate = new DateTime(2020, 01, 01);
        //dtpEnd.SelectedDate = new DateTime(2020, 12, 31);
    }

    private void LoadIncludedAgents()
    {
        List<string> agentIDList_lines = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate.Value.Date && w.date_created.Value.Date <= dtpEnd.SelectedDate.Value.Date && w.seller_uid.Length > 0).Select(s => s.seller_uid).Distinct().ToList();
        List<string> agentIDList_quotes = rdc.orddet_quotes.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate.Value.Date && w.date_created.Value.Date <= dtpEnd.SelectedDate.Value.Date && w.base_mc_user_uid.Length > 0).Select(s => s.base_mc_user_uid).Distinct().ToList();
        List<string> split_agentIDList_lines = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate.Value.Date && w.date_created.Value.Date <= dtpEnd.SelectedDate.Value.Date && w.seller_uid.Length > 0).Select(s => s.seller_uid).Distinct().ToList();
        List<string> split_agentIDList_quotes = rdc.orddet_quotes.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate.Value.Date && w.date_created.Value.Date <= dtpEnd.SelectedDate.Value.Date && w.base_mc_user_uid.Length > 0).Select(s => s.base_mc_user_uid).Distinct().ToList();


        List<string> agentIdList = agentIDList_lines;
        agentIdList.AddRange(agentIDList_quotes.Except(agentIDList_lines).ToList());
        agentIdList.AddRange(split_agentIDList_lines.Except(agentIDList_lines).ToList());
        agentIdList.AddRange(split_agentIDList_quotes.Except(agentIDList_lines).ToList());
        includedAgents = rdc.n_users.Where(w => agentIdList.Contains(w.unique_id)).ToList();
        includedAgents.Add(rdc.n_users.Where(w => w.name == "Willie Geter").FirstOrDefault());

        rzap.LoadPicker(includedAgents);
        if (!Page.IsPostBack)
            rzap.SelectedUserID = "all";
        List<string> ManagementRoles = new List<string>() { SM_Enums.PortalRole.admin.ToString(), SM_Enums.PortalRole.sm_internal_executive.ToString(), SM_Enums.PortalRole.portal_admin.ToString() };
        if (tools.IsInAnyRole(Membership.GetUser().UserName, ManagementRoles))
            rzap.Visible = true;

        else if (Roles.IsUserInRole(SM_Enums.PortalRole.sm_internal.ToString()))
        {
            rzap.Visible = false;
            includedAgents = rdc.n_users.Where(w => w.unique_id == Profile.RzUserID).ToList();
        }
        else
            Response.Redirect("/", false);
        //If we're using agent picker, and a specific agent is picked, set includedAgents to that.
        if (rzap.Visible)
            if (rzap.SelectedUserID != "all")
                includedAgents =  includedAgents.Where(w => w.unique_id == rzap.SelectedUserID).ToList();
        includedAgentNames = includedAgents.Select(s => s.name).ToList();
        includedAgentIDs = includedAgents.Select(s => s.unique_id).ToList();



    }
    //private void LoadIncludedAgents()
    //{
    //    //List<string> Teams = rzt.GetTeams().Where(w => w.name.Length > 0).Select(s => s.name).ToList();
    //    //linesInDateRange = new List<orddet_line>();
    //    //List<orddet_quote> quotesList = new List<orddet_quote>();


    //    //List<string> agentIDList_lines = LinesInDateRange.Select(s => s.seller_uid).Distinct().ToList();
    //    //List<string> agentIDList_quotes = QuoteLinesInDateRange.Select(s => s.base_mc_user_uid).Distinct().ToList();
    //    List<string> agentIDList_lines = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate && w.date_created.Value.Date <= dtpEnd.SelectedDate).Select(s => s.seller_uid).Distinct().ToList();
    //    List<string> splitAgentIDList_lines = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= dtpStart.SelectedDate && w.date_created.Value.Date <= dtpEnd.SelectedDate).Select(s => s.split_commission_agent_uid).Distinct().ToList();
    //    List<string> agentIDList_quotes = rdc.orddet_quotes.Where(w => w.date_created.Value.Date <= dtpStart.SelectedDate && w.date_created.Value.Date <= dtpEnd.SelectedDate).Select(s => s.base_mc_user_uid).Distinct().ToList();
    //    List<string> splitAgentIDList_quotes = rdc.orddet_quotes.Where(w => w.date_created.Value.Date <= dtpStart.SelectedDate && w.date_created.Value.Date <= dtpEnd.SelectedDate).Select(s => s.split_commission_agent_uid).Distinct().ToList();

    //    includedAgentIDs.AddRange(agentIDList_lines);
    //    includedAgentIDs.AddRange(agentIDList_lines.Except(splitAgentIDList_lines));
    //    includedAgentIDs.AddRange(agentIDList_lines.Except(agentIDList_quotes));
    //    includedAgentIDs.AddRange(agentIDList_lines.Except(splitAgentIDList_quotes));
    //    foreach (string s in agentIDList_quotes)
    //        if (!includedAgentIDs.Contains(s))
    //            includedAgentIDs.Add(s);


    //    includedAgents = rdc.n_users.Where(w => includedAgentIDs.Contains(w.unique_id)).Distinct().ToList();
    //    includedAgentNames = includedAgents.Select(s => s.name).Distinct().ToList();

    //    //List<string> teamList = new List<string>() { "Sales", "Distributor Sales" };
    //    rzap.LoadPicker(includedAgents);
    //    rzap.SelectedUserID = "all";
    //    //List<string> ManagementRoles = new List<string>() { SM_Enums.PortalRole.admin.ToString(), SM_Enums.PortalRole.sm_internal_executive.ToString(), SM_Enums.PortalRole.portal_admin.ToString() };
    //    //if (tools.IsInAnyRole(Membership.GetUser().UserName, ManagementRoles))
    //    //{
    //    //    List<n_user> agentList = rzap.GetSelectedUsers();
    //    //    List<string> selectedUserIds = agentList.Select(s => s.unique_id).Distinct().ToList();
    //    //    rzap.Visible = true;
    //    //    includedAgents = rdc.n_users.Where(w => selectedUserIds.Contains(w.unique_id) && !notIncludedAgents.Contains(w.name.Trim())
    //    //    ).ToList();

    //    //}
    //    //else if (Roles.IsUserInRole(SM_Enums.PortalRole.sm_internal.ToString()))
    //    //{
    //    //    rzap.Visible = false;
    //    //    includedAgents = rdc.n_users.Where(w => w.unique_id == Profile.RzUserID).ToList();
    //    //}
    //    //else
    //    //    Response.Redirect("/", true);

    //    //includedAgentNames = includedAgents.Select(s => s.name).ToList();
    //    //includedAgentIDs = includedAgents.Select(s => s.unique_id).ToList();
    //}

    private void LoadSalesBreakdown(out string debugLog)
    {

        debugLog = "";

        List<string> badStatus = LineData.GetInvalid_orddet_Status(true, true).ToList();
        lblSbDateRange.Text = dtpStart.SelectedDate.Value.Date.ToString("MM/dd/yyyy") + " - " + dtpEnd.SelectedDate.Value.Date.ToString("MM/dd/yyyy");


        //Quoted Query
        using (RzDataContext rdc = new RzDataContext())
        {
            DateTime queryQuotedStart = DateTime.Now;
            queryQuotedRz = SalesLogic.GetQuotedOrders(rdc, dtpStart.SelectedDate.Value.Date, dtpEnd.SelectedDate.Value.Date, badStatus, includedAgents).ToList();
            debugLog += "Quote Query completed in " + Tools.Dates.GetTimeSpan(queryQuotedStart, null).TotalSeconds + " seconds.<br />";

            var queryQuoted = (from q in queryQuotedRz
                               select new
                               {
                                   OppStage = q.Status,
                                   OrderNumber = q.Order,
                                   Agent = q.AgentName,
                                   Total = q.Total,
                                   GP = q.GP,
                                   Open = q.OpenGP
                               }).OrderByDescending(o => o.Total).ToList();

            var NoQuoteUsers = from u in rdc.n_users
                               where !queryQuoted.Select(s => s.Agent).Distinct().ToList().Contains(u.name)
                               && includedAgentIDs.Contains(u.unique_id)
                               select new
                               {
                                   OppStage = "",
                                   OrderNumber = "",
                                   Agent = u.name,
                                   Total = Convert.ToDouble(0),
                                   GP = Convert.ToDouble(0),
                                   Open = Convert.ToDouble(0)
                               };
            //Concat is better thatn Union, as I found Union was removing what it thought was duplicates
            var finalQuoted = queryQuoted.Concat(NoQuoteUsers).GroupBy(g => g.Agent).Select(s => new
            {
                OppStage = s.Max(t => t.OppStage),
                OrderNumber = s.Max(t => t.OrderNumber),
                Agent = s.Key,
                Total = s.Sum(t => t.Total),
                GP = s.Sum(t => t.GP),
                Open = s.Sum(t => t.Open)

            });

            var gridFormatQuoted = finalQuoted.Select(s => new
            {
                Agent = s.Agent,
                Total = string.Format("{0:C2}", s.Total),
                GP = string.Format("{0:C2}", s.GP),
                OpenGP = string.Format("{0:C2}", s.Open),
            });



            lblsbQuotedTotal.Text = finalQuoted.Sum(s => s.Total).ToString("C");
            lblsbQuotedGP.Text = finalQuoted.Sum(s => s.GP).ToString("C");
            lblsbQuotedOpen.Text = finalQuoted.Sum(s => s.Open).ToString("C");


            gvSbQuoted.DataSource = gridFormatQuoted.ToList();
            gvSbQuoted.DataBind();
            if (gridFormatQuoted.Any())
                gvSbQuoted.HeaderRow.TableSection = TableRowSection.TableHeader;


            //Booked Query          
            DateTime queryBookedStart = DateTime.Now;
            bookedQuery = SalesLogic.GetBookedOrders(rdc, dtpStart.SelectedDate.Value.Date, dtpEnd.SelectedDate.Value.Date, badStatus, includedAgents).ToList();
            debugLog += "Booked Query completed in " + Tools.Dates.GetTimeSpan(queryBookedStart, null).TotalSeconds + " seconds.<br />";
            var noBooked = (from u in rdc.n_users
                            where !bookedQuery.Select(s => s.AgentName).Distinct().ToList().Contains(u.name)
                            && includedAgentIDs.Contains(u.unique_id)
                            select new SalesLogic.OrderQueryResult
                            {
                                Order = "",
                                Status = "",
                                AgentName = u.name,
                                Total = Convert.ToDouble(0),
                                GP = Convert.ToDouble(0),
                                NP = Convert.ToDouble(0)
                            });



            var bookedConcat = bookedQuery.Concat(noBooked);


            var finalBooked = bookedConcat.GroupBy(g => g.AgentName).Select(s => new
            {

                Name = s.Key,
                Total = s.Sum(m => m.Total),
                GP = s.Sum(m => m.GP),
                NP = s.Sum(m => m.NP),

            }).OrderByDescending(o => o.NP);


            var gridFormatBooked = finalBooked.Select(s => new
            {
                Name = s.Name,
                Total = string.Format("{0:C2}", s.Total),
                GP = string.Format("{0:C2}", s.GP),
                NP = string.Format("{0:C2}", s.NP),
            });

            lblsbBookedTotal.Text = finalBooked.Sum(s => s.Total).ToString("C");
            lblsbBookedGP.Text = finalBooked.Sum(s => s.GP).ToString("C");
            lblsbBookedNP.Text = finalBooked.Sum(s => s.NP).ToString("C");


            gvSbBooked.DataSource = gridFormatBooked.ToList();
            gvSbBooked.DataBind();
            if (gridFormatBooked.Any())
                gvSbBooked.HeaderRow.TableSection = TableRowSection.TableHeader;

            //User OrderID from this query to get line details for teh BookedDetailGrid
            DateTime queryBookedDetailStart = DateTime.Now;
            LoadBreakdownDetailGrid(bookedQuery.Select(s => s.orderid_sales).ToList(), "booked");
            debugLog += "Booked Breakdown detail grid completed in " + Tools.Dates.GetTimeSpan(queryBookedDetailStart, null).TotalSeconds + " seconds.<br />";

            //Invoiced
            DateTime queryInvoicedStart = DateTime.Now;
            invoicedQuery = SalesLogic.GetInvoicedOrders(rdc, dtpStart.SelectedDate.Value.Date, dtpEnd.SelectedDate.Value.Date, badStatus, includedAgents).ToList();
            debugLog += "Invoiced Query completed in " + Tools.Dates.GetTimeSpan(queryInvoicedStart, null).TotalSeconds + " seconds.<br />";


            //Users with No Invoices need to be included and Zeroed out.

            List<string> invoicedUserIDs = invoicedQuery.Select(s => s.AgentID).Distinct().ToList();
            List<string> nonInvoicedUseIds = includedAgentIDs.Except(invoicedUserIDs).ToList();
            DateTime queryNonInvoicedStart = DateTime.Now;
            var notInvoiced = (from u in rdc.n_users
                               where nonInvoicedUseIds.Contains(u.unique_id)
                               select new SalesLogic.OrderQueryResult
                               {
                                   Order = "",
                                   orderid_invoice = "",
                                   AgentID = u.unique_id,
                                   AgentName = u.name,
                                   Total = Convert.ToDouble(0),
                                   GP = Convert.ToDouble(0),
                                   NP = Convert.ToDouble(0)
                               });
            debugLog += "Non-Invoiced Query completed in " + Tools.Dates.GetTimeSpan(queryNonInvoicedStart, null).TotalSeconds + " seconds.<br />";

            //Concatenate
            DateTime queryInvoicedConcatStart = DateTime.Now;
            var concatInvoiced = invoicedQuery.Concat(notInvoiced).GroupBy(g => g.AgentName).Select(s => new
            {
                SONum = s.Select(ss => ss.Order).FirstOrDefault().ToString(),
                SellerID = s.Select(ss => ss.AgentID).FirstOrDefault().ToString(),
                InvoiceID = s.Select(ss => ss.orderid_invoice).FirstOrDefault().ToString(),
                Name = s.Key.ToString(),
                Total = s.Sum(m => m.Total),
                GP = s.Sum(m => m.GP),
                NP = s.Sum(m => m.NP)

            }).OrderByDescending(o => o.NP);
            debugLog += "Invoiced Concatenation Query completed in " + Tools.Dates.GetTimeSpan(queryInvoicedConcatStart, null).TotalSeconds + " seconds.<br />";

            //Had to create a customer Object because LINQ Anonymous (var) results are read-only.
            List<OrderDataObject> finalInvoiced = new List<OrderDataObject>();

            //Build the OrderDataObjects Lis
            DateTime queryOrderDataObjectStart = DateTime.Now;
            foreach (var v in concatInvoiced)
            {
                List<string> invoiceIDs = invoicedQuery.Where(w => w.AgentID == v.SellerID).Select(s => s.orderid_invoice).Distinct().ToList();
                OrderDataObject o = new OrderDataObject(invoiceIDs, v.SellerID, v.Name, v.Total, v.GP, v.NP);
                finalInvoiced.Add(o);
            }
            debugLog += "Invoiced Concatenation Query completed in " + Tools.Dates.GetTimeSpan(queryOrderDataObjectStart, null).TotalSeconds + " seconds.<br />";
            //Calculate the invoice charges
            foreach (OrderDataObject o in finalInvoiced)
            {
                //o.Total = o.Total - rzt.GetInvoieceCredits(o.InvoiceIDs);
                o.Total = o.Total + rzt.GetInvoiceCharges(o.InvoiceIDs);
                o.NP = o.NP - rzt.GetInvoiceCredits(o.InvoiceIDs);
            }


            //Labels
            lblsbInvoicedTotal.Text = string.Format("{0:C2}", finalInvoiced.Sum(s => s.Total));
            lblsbInvoicedGP.Text = string.Format("{0:C2}", finalInvoiced.Sum(s => s.GP));
            lblsbInvoicedNP.Text = string.Format("{0:C2}", finalInvoiced.Sum(s => s.NP));


            //Grid fomatting
            var gridFormatInvoiced = finalInvoiced.Select(s => new
            {
                Name = s.Name,
                Total = string.Format("{0:C2}", s.Total),
                GP = string.Format("{0:C2}", s.GP),
                NP = string.Format("{0:C2}", s.NP),
            });

            gvSbInvoiced.DataSource = gridFormatInvoiced.ToList();
            gvSbInvoiced.DataBind();
            if (gridFormatInvoiced.Any())
                gvSbInvoiced.HeaderRow.TableSection = TableRowSection.TableHeader;


            //User OrderID from this query to get line details for teh BookedDetailGrid
            DateTime querySalesBreakdownDetailGrid = DateTime.Now;
            LoadBreakdownDetailGrid(invoicedQuery.Select(s => s.orderid_invoice).ToList(), "billed");
            debugLog += "Billed Breakdown detail grid completed in " + Tools.Dates.GetTimeSpan(querySalesBreakdownDetailGrid, null).TotalSeconds + " seconds.<br />";

        }
    }

    private void LoadBreakdownDetailGrid(List<string> orderIds, string type)
    {

        sm_datatable theGrid = null;
        List<orddet_line> detailList = new List<orddet_line>();
        //Max 2100 parameters
        //List to hold the sub 2100 parameterized results.
        List<orddet_line> tempList = new List<orddet_line>();
        int take = 2000;
        int skip = 0;



        switch (type)
        {
            case "booked":
                {
                    theGrid = smdt_booked;
                    while (true)
                    {
                        tempList = LinesInDateRange.Where(w => orderIds.Skip(skip).Take(take).Contains(w.orderid_sales)).ToList();
                        if (tempList.Count <= 0)
                            break;
                        detailList.AddRange(tempList);
                        skip += take;
                    }


                    break;
                }
            case "billed":
                {
                    theGrid = smdt_billed;
                    while (true)
                    {
                        tempList = LinesInDateRange.Where(w => orderIds.Skip(skip).Take(take).Contains(w.orderid_invoice)).ToList();
                        if (tempList.Count <= 0)
                            break;
                        detailList.AddRange(tempList);
                        skip += take;
                    }

                    break;
                }
        }



        var query = detailList.Select(s => new
        {
            Part = s.fullpartnumber,
            Agent = s.seller_name,
            Customer = s.customer_name,
            Vendor = s.vendor_name,
            QTY = s.quantity,
            TotalPrice = s.total_price,
            GP = s.gross_profit,
            Status = s.status,
            Type = type
        });
        theGrid.dataSource = query;
        theGrid.loadGrid();



    }

    private void LoadProjectedGP()
    {
        lblPGPDate.Text = dtpStart.SelectedDate.Value.ToString("MM/dd/yyyy") + " - " + dtpEnd.SelectedDate.Value.ToString("MM/dd/yyyy");
        //Get The Data
        List<string> badStatus = LineData.GetInvalid_orddet_Status(true, true).ToList();
        badStatus.Add("Shipped");
        var projGPLines = (rdc.orddet_lines.Where(w => (w.customer_dock_date >= dtpStart.SelectedDate.Value.Date && w.customer_dock_date <= dtpEnd.SelectedDate.Value.Date)
        && includedAgentIDs.Contains(w.seller_uid)
        && !badStatus.Contains(w.status)
        && (w.vendor_name.Length > 0
        && w.orderid_sales.Length > 0
        && (w.orderid_invoice.Length <= 0 || w.orderid_invoice == null)
        )).ToList()
            .OrderBy(o => o.orderdate_sales)
            .Select(s => new
            {
                Customer = s.customer_name,
                Agent = s.seller_name,
                OrderDate = s.orderdate_sales,
                CDock = s.customer_dock_date,
                PartNumber = s.fullpartnumber,
                QTY = s.quantity,
                TotalPrice = s.total_price,
                GP = s.gross_profit,
                SONum = s.ordernumber_sales,
                Status = s.status,
                InvoiceID = s.orderid_invoice,
                VendorName = s.vendor_name,
                icon_index = s.icon_index,
                TrackingPurchase = s.tracking_purchase,
                ShipVia = s.shipvia_purchase,
                isInvoiced = string.IsNullOrEmpty(s.orderid_invoice) ? "Not Invoiced" : "Invoiced"
            })).ToList();

        //Load the GridView
        var gridQuery = projGPLines.Select(s => new
        {
            Agent = s.Agent,
            SONum = s.SONum,
            Customer = s.Customer,
            Vendor = s.VendorName,
            CDock = s.CDock.Value.ToString("MM/dd/yyyy"),
            OrderDate = s.OrderDate.Value.ToString("MM/dd/yyyy"),
            PartNumber = s.PartNumber,
            QTY = s.QTY,
            TotalPrice = s.TotalPrice,
            GP = s.GP,
            Status = s.Status,
            Tracking = s.TrackingPurchase

        });
        //Bind Details Grid
        //gvProjGP.DataSource = gridQuery.OrderByDescending(o => Convert.ToDateTime(o.OrderDate)).ToList();
        //gvProjGP.DataBind();
        //if (gridQuery.Any())
        //{
        //    gvProjGP.HeaderRow.TableSection = TableRowSection.TableHeader;
        //}
        smdtProjGP.dataSource = gridQuery.OrderByDescending(o => Convert.ToDateTime(o.OrderDate)).ToList();
        smdtProjGP.loadGrid();

        var groupedStatus = projGPLines.GroupBy(g => g.Status).Select(g => new
        {
            Status = g.Key,
            GP = g.Sum(s => s.GP).ToString(),
        });

        var groupedAgents = projGPLines.GroupBy(g => g.Agent).Select(g => new
        {
            Agent = g.Key,
            Status = g.Select(s => s.Status),
            GP = g.Sum(s => s.GP).ToString(),
        });


        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();


        decimal projectedSales = (decimal)projGPLines.Where(w => w.VendorName != "Source TBD").Sum(s => s.TotalPrice);
        decimal projectedGP = (decimal)projGPLines.Where(w => w.VendorName != "Source TBD").Sum(s => s.GP);
        decimal invoicedGP = (decimal)projGPLines.Where(w => w.VendorName != "Source TBD" && w.InvoiceID.Length > 0).Sum(s => s.GP);
        decimal outstandingGP = (decimal)projGPLines.Where(w => w.VendorName != "Source TBD" && w.InvoiceID.Length <= 0).Sum(s => s.GP);

        lblProjGP.Text = projectedGP.ToString("C");
        lblProjSales.Text = projectedSales.ToString("C");
        //lblInvGP.Text = invoicedGP.ToString("C");
        //lblOutGP.Text = outstandingGP.ToString("C");

        //Status and Count Dataset
        string userlabel = "All Users";

        //if (rzap.GetSelectedUserIds()[0].Length > 10)
        //    userlabel = rzap.GetSelectedUserName();
        userlabel = rzap.SelectedUserName;
        SM_Charts.chartJsDataSet cDataSet1 = new SM_Charts.chartJsDataSet();
        cDataSet1.labelFormat = "currency";
        cDataSet1.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSet1.data = groupedStatus.OrderByDescending(o => Convert.ToDouble(o.GP)).Select(s => s.GP.ToString()).ToList();

        //Add the Datasets to the List
        datasetList.Add(cDataSet1);
        //datasetList.Add(cDataSet2);
        //datasetList.Add(cDataSet3);
        //datasetList.Add(cDataSet4);
        //datasetList.Add(cDataSet5);
        if (projGPLines.Count > 0)
        {
            //Setup the Chart
            SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
            Chart1.chartType = SM_Enums.ChartType.bar.ToString();
            Chart1.labels = groupedStatus.Select(s => s.Status.ToString()).Distinct().ToList();
            //Chart1.options = "scales:{xAxes:[{stacked:true}],yAxes:[{stacked:true}]}}});";
            Chart1.stacked = true;
            Chart1.dataSetList = datasetList;
            Chart1.canvasID = "ProjectedGP";
            Chart1.maintainnAspectRatio = false;
            Chart1.showLegend = false;
            ChartList.Add(Chart1);
        }

    }




    private void LoadSalesPipelineChart()
    {

        //Instantate a list of objects to hold the dataset(s)
        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();

        //Build the DataSet
        SM_Charts.chartJsDataSet cDataSet1 = new SM_Charts.chartJsDataSet();
        List<ordhed_quote> quotesList = rdc.ordhed_quotes.Where(w => w.date_created >= dtpStart.SelectedDate.Value)
            .Where(w => w.opportunity_stage.Length > 0 && includedAgentIDs.Contains(w.base_mc_user_uid))
            .GroupBy(g => g.opportunity_stage).ToList()
            .Select(g => new ordhed_quote { opportunity_stage = g.Key, ordernumber = g.Count().ToString() }).ToList();
        if (quotesList.Count <= 0)
        {
            lblPipelineStageNoData.Text = noDataText;
            return;
        }

        cDataSet1.data = quotesList.OrderByDescending(o => o.ordernumber).Select(s => s.ordernumber.ToString()).ToList();
        //Add the Datasets to the List
        datasetList.Add(cDataSet1);

        //Setup the Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = "pie";
        Chart1.labels = quotesList.OrderByDescending(o => o.ordernumber).Select(s => s.opportunity_stage).ToList();
        Chart1.dataSetList = datasetList;
        Chart1.canvasID = "cvSalesPipelineStage";
        ChartList.Add(Chart1);
    }

    private void LoadLineStatus()
    {
        List<string> statuses = new List<string>() { "buy", "open", "hold", "shipped" };
        //Get The Data
        var lines = rdc.orddet_lines.Where(w => w.date_created >= dtpStart.SelectedDate.Value
        && includedAgentIDs.Contains(w.seller_uid)
        && statuses.Contains(w.status.ToLower()))
            .GroupBy(g => g.status).ToList()
            .Select(g => new
            {
                status = g.Key,
                gross_profit = g.Sum(y => y.gross_profit)
            }).ToList();
        //Instantate a list of objects to hold the dataset(s)
        if (lines.Count <= 0)
        {
            lblLineStatusNoData.Text = noDataText;
            return;
        }


        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();

        //Build the DataSet
        SM_Charts.chartJsDataSet cDataSet1 = new SM_Charts.chartJsDataSet();
        cDataSet1.labelFormat = "currency";
        cDataSet1.data = lines.OrderByDescending(o => o.gross_profit).Select(s => s.gross_profit.ToString()).ToList();

        //Add the Datasets to the List
        datasetList.Add(cDataSet1);

        //Setup the Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = "pie";
        Chart1.labels = lines.OrderByDescending(o => o.gross_profit).Select(s => s.status.ToString()).ToList();
        Chart1.dataSetList = datasetList;
        Chart1.canvasID = "cvLineStatus";
        ChartList.Add(Chart1);
        //pnlLineStatus.Visible = true;

    }

    private void LoadPctReqToQuoteAgentBar()
    {

        //GET the data
        if (QuoteLinesInDateRange == null)
        {
            lblpctReqToQuoteNoData.Text = noDataText;
            return;
        }


        var dataList = QuoteLinesInDateRange.GroupBy(g => g.agentname)
             .Select(g => new
             {
                 agent = g.Key,
                 totalReqs = g.Count(c => !string.IsNullOrEmpty(c.base_ordhed_uid)).ToString(),
                 totalQuotes = g.Count(c => string.IsNullOrEmpty(c.base_ordhed_uid)).ToString(),
                 reqValue = QuoteLinesInDateRange.Where(w => w.agentname == g.Key && !string.IsNullOrEmpty(w.base_ordhed_uid)).Sum(s => s.totalprice).ToString(),
                 quoteValue = QuoteLinesInDateRange.Where(w => w.agentname == g.Key && string.IsNullOrEmpty(w.base_ordhed_uid)).Sum(s => s.totalprice).ToString()
             }).ToList();

        var filteredData = dataList.Select(s => new
        {
            agent = s.agent,
            totalReqs = s.totalReqs,
            totalQuotes = s.totalQuotes,
            reqValue = s.reqValue,
            quoteValue = s.quoteValue,
            percent = s.totalReqs == "0" ? "0" : s.totalQuotes == "0" ? "0" : ((Convert.ToDecimal(s.totalQuotes) / Convert.ToDecimal(s.totalReqs)) * 100).ToString("#.##")
        }).OrderByDescending(o => Convert.ToDecimal(o.percent));


        //Instantate a list of objects to hold the dataset(s)
        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();
        if (filteredData.Count() <= 0)
            return;
        //Build the DataSet
        SM_Charts.chartJsDataSet cDataSet1 = new SM_Charts.chartJsDataSet();
        cDataSet1.dataSetLabel = "% Req To Quote";
        //cDataSet1.isToolTipAfterBody = true;
        //cDataSet1.ToolTipAfterBodyFormat = "percentage";
        cDataSet1.chartType = SM_Enums.ChartType.line.ToString();
        cDataSet1.fillColors = false;
        cDataSet1.data = filteredData.Select(s => string.IsNullOrEmpty(s.percent) ? "0" : s.percent.ToString()).ToList();

        datasetList.Add(cDataSet1);


        //Build the DataSet
        SM_Charts.chartJsDataSet cDataSet2 = new SM_Charts.chartJsDataSet();

        cDataSet2.data = filteredData.Select(s => s.totalQuotes.ToString()).ToList();
        cDataSet2.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSet2.dataSetLabel = "Total Quotes";
        cDataSet2.fillColors = false;
        datasetList.Add(cDataSet2);

        //Build the DataSet
        SM_Charts.chartJsDataSet cDataSet3 = new SM_Charts.chartJsDataSet();
        cDataSet3.data = filteredData.Select(s => s.totalReqs.ToString()).ToList();
        cDataSet3.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSet3.dataSetLabel = "Total Reqs";
        cDataSet3.fillColors = false;
        datasetList.Add(cDataSet3);

        ////Build the DataSet
        //SM_Charts.chartJsDataSet cDataSet4 = new SM_Charts.chartJsDataSet();
        //cDataSet4.label = "Req Value";
        ////cDataSet3.data = quoteList.GroupBy(g => g.agentname).OrderByDescending(o => o.Count(c => string.IsNullOrEmpty(c.base_ordhed_uid))).Select(s => s.Count().ToString()).ToList();
        //cDataSet4.data = filteredData.Select(s => s.reqValue.ToString()).ToList();
        //cDataSet4.chartType = SM_Enums.ChartType.bar.ToString();
        //cDataSet4.fillColors = false;
        ////Add the Datasets to the List
        //datasetList.Add(cDataSet4);

        ////Build the DataSet
        //SM_Charts.chartJsDataSet cDataSet5 = new SM_Charts.chartJsDataSet();
        //cDataSet5.label = "Quote Value";
        ////cDataSet3.data = quoteList.GroupBy(g => g.agentname).OrderByDescending(o => o.Count(c => string.IsNullOrEmpty(c.base_ordhed_uid))).Select(s => s.Count().ToString()).ToList();
        //cDataSet5.data = filteredData.Select(s => s.quoteValue.ToString()).ToList();
        //cDataSet5.chartType = SM_Enums.ChartType.bar.ToString();
        //cDataSet5.fillColors = false;
        ////Add the Datasets to the List
        //datasetList.Add(cDataSet5);


        //Setup the Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = SM_Enums.ChartType.bar.ToString();
        Chart1.stacked = true;
        Chart1.labels = filteredData.Select(s => s.agent).ToList();
        Chart1.dataSetList = datasetList;
        Chart1.canvasID = "PctReqToQuoteAgentBar";
        Chart1.toolTipMode = "index";
        Chart1.tooltipIntersect = true;
        ChartList.Add(Chart1);

    }

    class BookBillobject
    {
        public string Type { get; set; } //"booked" vs "billed"
        public string Agent { get; set; }
        public string AgentID { get; set; }
        public DateTime Date { get; set; }
        public double Total { get; set; }
        public double NP { get; set; }
        public double GP { get; set; }

    }

    List<BookBillobject> bbList = new List<BookBillobject>();

    class BookBillGridObject
    {
        public string Agent { get; set; }
        public double BookedTotal { get; set; }
        public double BookedNP { get; set; }
        public double BookedGP { get; set; }
        public double BilledTotal { get; set; }
        public double BilledNP { get; set; }
        public double BilledGP { get; set; }
        public string Type { get; set; }

    }
    List<BookBillGridObject> bbgList = new List<BookBillGridObject>();



    private void LoadBookToBill()
    {


        //Ratio = Booked / Billed  

        smdtBookBill.dataSource = null;
        smdtBookBill.loadGrid();
        //Get The Data


        //Billed
        var billedQuery = (from l in LinesInDateRange
                           where l.orderid_sales.Length > 0
                           && l.orderdate_sales >= dtpStart.SelectedDate.Value && l.orderdate_sales <= dtpEnd.SelectedDate.Value
                           && includedAgents.Select(s => s.unique_id).ToList().Contains(l.seller_uid)
                           && !LineData.GetInvalid_orddet_Status(true, true).Contains(l.status)
                           && !l.ordernumber_sales.Contains("CR")
                           select new BookBillobject
                           {
                               Agent = l.seller_name,
                               AgentID = l.seller_uid,
                               Date = (DateTime)l.orderdate_invoice,
                               Total = (double)l.total_price,
                               NP = (double)l.net_profit,
                               GP = (double)l.gross_profit,
                               Type = "billed"
                           }).GroupBy(g => g.AgentID).Select(s => new BookBillobject
                           {
                               AgentID = s.Key,
                               Agent = s.Select(f => f.Agent).FirstOrDefault(),
                               Total = s.Sum(f => f.Total),
                               NP = s.Sum(f => f.NP),
                               GP = s.Sum(f => f.GP),
                               Type = s.Max(f => f.Type)
                           }).OrderBy(o => o.Agent).ToList();

        foreach (BookBillobject b in billedQuery)
            bbList.Add(b);



        //Booked
        var bookedQuery = (from l in LinesInDateRange
                           where l.orderid_invoice.Length > 0
                           && l.orderdate_invoice >= dtpStart.SelectedDate.Value && l.orderdate_invoice <= dtpEnd.SelectedDate.Value
                           && includedAgents.Select(s => s.unique_id).ToList().Contains(l.seller_uid)
                           && l.orderdate_sales <= dtpEnd.SelectedDate.Value
         && !LineData.GetInvalid_orddet_Status(true, true).Contains(l.status)
         && !l.ordernumber_sales.Contains("CR")
                           select new BookBillobject
                           {
                               Agent = l.seller_name,
                               AgentID = l.seller_uid,
                               Date = (DateTime)l.orderdate_sales,
                               Total = (double)l.total_price,
                               NP = (double)l.net_profit,
                               GP = (double)l.gross_profit,
                               Type = "booked"
                           })
                           .GroupBy(g => g.AgentID).Select(s => new BookBillobject
                           {
                               AgentID = s.Key,
                               Agent = s.Select(f => f.Agent).FirstOrDefault(),
                               Total = s.Sum(f => f.Total),
                               NP = s.Sum(f => f.NP),
                               GP = s.Sum(f => f.GP),
                               Type = s.Max(f => f.Type)
                           }).OrderBy(o => o.Agent).ToList();

        foreach (BookBillobject b in bookedQuery)
            bbList.Add(b);




        //Agents with Data
        List<string> agentsIdsWithBBData = bbList.Select(s => s.AgentID).Distinct().ToList();
        ////Agents without Data
        //List<string> agentsWithOutBBData = includedAgentIDs.Where(w => !agentsIdsWithBBData.Contains(w) && includedAgentNames.Contains(w)).Distinct().ToList();

        //List<string> allAgents = new List<string>();
        //allAgents.AddRange(agentsWithBBData);
        //allAgents.AddRange(agentsWithOutBBData);
        foreach (string s in agentsIdsWithBBData)
        {
            BookBillGridObject bgo = new BookBillGridObject();
            bgo.Agent = bbList.Where(w => w.AgentID == s).Select(ss => ss.Agent).Distinct().FirstOrDefault();
            bgo.BookedTotal = bbList.Where(w => w.Type == "booked" && w.AgentID == s).Sum(ss => ss.Total);
            bgo.BookedNP = bbList.Where(w => w.Type == "booked" && w.AgentID == s).Sum(ss => ss.NP);
            bgo.BookedGP = bbList.Where(w => w.Type == "booked" && w.AgentID == s).Sum(ss => ss.GP);
            bgo.BilledTotal = bbList.Where(w => w.Type == "billed" && w.AgentID == s).Sum(ss => ss.Total);
            bgo.BilledNP = bbList.Where(w => w.Type == "billed" && w.AgentID == s).Sum(ss => ss.NP);
            bgo.BilledGP = bbList.Where(w => w.Type == "billed" && w.AgentID == s).Sum(ss => ss.GP);
            bbgList.Add(bgo);
        }




        //Bind the Details Grid
        //gvBookToBill.DataSource = bgo;
        //gvBookToBill.DataBind();
        //if (finalQuery.Any())
        //    gvBookToBill.HeaderRow.TableSection = TableRowSection.TableHeader;
        smdtBookBill.dataSource = bbgList;
        smdtBookBill.loadGrid();


        //Labels
        double billedTotal = bbgList.Sum(s => s.BilledTotal);
        double billedNP = bbgList.Sum(s => s.BilledNP);
        double billedGP = bbgList.Sum(s => s.BilledGP);
        double bookedTotal = bbgList.Sum(s => s.BookedTotal);
        double bookedNP = bbgList.Sum(s => s.BookedNP);
        double bookedGP = bbgList.Sum(s => s.BookedGP);
        lblB2BDateRange.Text = dtpStart.SelectedDate.Value.ToString("MM/dd/yyyy") + " " + dtpEnd.SelectedDate.Value.ToString("MM/dd/yyyy");
        lblB2BRatio.Text = (bookedTotal / billedTotal).ToString("#.##") + "%";


        //Booked labels
        lblBookedTot.Text = (bookedTotal).ToString("C");
        lblBookedGP.Text = (bookedGP).ToString("C");
        lblBookedNP.Text = (bookedNP).ToString("C");

        //Billed labels
        lblBilledTot.Text = (billedTotal).ToString("C");
        lblBilledGP.Text = (billedGP).ToString("C");
        lblBilledNP.Text = (billedNP).ToString("C");

        //IF No Results, set label
        if (bbList.Count <= 0)
        {

            lblB2BNoData.Text = noDataText;
            lblB2BRatio.Text = noDataText;
        }




        //Setup the Chart

        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();

        //Build the DataSets
        //Booked Dataset
        SM_Charts.chartJsDataSet cDataSetBooked = new SM_Charts.chartJsDataSet();
        cDataSetBooked.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSetBooked.dataSetLabel = "Booked";
        cDataSetBooked.fillColors = false;
        cDataSetBooked.labelFormat = "currency";
        cDataSetBooked.data = bbgList.OrderByDescending(o => o.BilledTotal + o.BookedTotal).Select(s => s.BookedTotal.ToString()).ToList();

        datasetList.Add(cDataSetBooked);

        //Billed Dataset
        SM_Charts.chartJsDataSet cDataSetBilled = new SM_Charts.chartJsDataSet();
        cDataSetBilled.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSetBilled.dataSetLabel = "Billed";
        cDataSetBilled.fillColors = false;
        cDataSetBilled.labelFormat = "currency";
        cDataSetBilled.data = bbgList.OrderByDescending(o => o.BilledTotal + o.BookedTotal).Select(s => s.BilledTotal.ToString()).ToList();
        //Add the Datasets to the List
        datasetList.Add(cDataSetBilled);

        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = "bar";
        Chart1.labels = bbgList.OrderByDescending(o => o.BilledTotal + o.BookedTotal).Select(s => s.Agent.ToString()).Distinct().ToList();
        Chart1.dataSetList = datasetList;
        Chart1.canvasID = "cvB2B";
        Chart1.displayXaxisLabel = true;
        Chart1.showLegend = false;
        Chart1.stacked = true;
        ChartList.Add(Chart1);


    }





    private void LoadDeductions()
    {
        //Get The Data
        //List<string> agentIDs = includedAgents.Select(s => s.unique_id).Distinct().ToList();
        //Line Data

        //var lineQuery = linesInDateRange.Where(w => agentIDs.Contains(w.seller_uid) && w.ordernumber_sales.Length > 0 && w.date_created >= dtpStart.SelectedDate && w.date_created <= dtpEnd.SelectedDate).ToList();
        List<string> lineIds = LinesInDateRange.Select(s => s.unique_id).Distinct().ToList();

        //Deduction Data
        List<LineDeduction> deductionQuery = LineDeduction.GetLineDeductionsForAgents(lineIds);
        //if(deductionQuery.Count <= 0)
        //{
        //    lblDeductionsNoData.Text = noDataText;
        //    return;
        //}

        var joinQuery = from d in deductionQuery
                        join l in LinesInDateRange on d.lineID equals l.unique_id
                        select new
                        {
                            Agent = l.seller_name,
                            DedAmount = d.DeductionAmount,
                            GP = l.gross_profit,
                            SO = l.ordernumber_sales,
                            Customer = l.customer_name
                        };


        var groupQuery = joinQuery.GroupBy(g => g.Agent).Select(s => new
        {
            Agent = s.Key,
            DedAmount = s.Sum(ss => ss.DedAmount),
            LineGP = s.Sum(ss => ss.GP),
            Customer = s.Max(ss => ss.Customer),
            SO = s.Max(ss => ss.SO)
        });

        var pctQuery = groupQuery.Select(s => new
        {
            Agent = s.Agent,
            DedAmount = s.DedAmount,
            GP = s.LineGP ?? 0,
            Customer = s.Customer,
            SO = s.SO,
            Pct = (s.DedAmount / s.LineGP)
        }).OrderByDescending(o => o.Pct).ToList();

        //Modal Grid
        var gridQuery = joinQuery.GroupBy(g => g.SO).Select(s => new
        {
            Agent = s.Max(ss => ss.Agent),
            Customer = s.Max(ss => ss.Customer),
            SO = s.Key,
            DedAmount = s.Sum(ss => ss.DedAmount),
            GP = s.Sum(ss => ss.GP ?? 0),
            Pct = 0.0 //needs to be a double
        });

        gridQuery = gridQuery.Select(s => new
        {
            Agent = s.Agent,
            Customer = s.Customer,
            SO = s.SO,
            DedAmount = s.DedAmount,
            GP = s.GP,
            Pct = (s.DedAmount == 0 ? 0 : s.GP == 0 ? 0 : (s.DedAmount / s.GP) * 100)
        });

        //Gridview
        //gvDeductions.DataSource = gridQuery.OrderByDescending(s => s.Pct);
        //gvDeductions.DataBind();
        smdtDeductions.dataSource = gridQuery.OrderByDescending(s => s.Pct).ToList();
        smdtDeductions.loadGrid();

        //Chart
        //Labels List
        List<string> labelList = pctQuery.Select(s => s.Agent).ToList();

        //Dataset calcs
        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();
        //Pct
        List<double> dedList = new List<double>();
        //  dedList = pctQuery.Select(s => (s.Pct > 0 ? (s.Pct * 100) : 0) ?? 0).ToList();  //if the Pct is not > 0, return 0
        dedList = pctQuery.Select(s => (s.Pct * 100) ?? 0).ToList();

        List<double> gpList = new List<double>();
        foreach (double d in dedList)

            //gpList.Add(d > 0 ? (100 - d) : 0); //if the deduction is not > 0, return 0
            gpList.Add(100 - d);



        datasetList.Add(GetDeductionsDataSets("GP", "percentage", SM_Tools.Colors.smGreenRgba, gpList.Select(s => Math.Round(s, 2)).ToList()));
        datasetList.Add(GetDeductionsDataSets("Dollar Amount: ", "currency", SM_Tools.Colors.smOrangeRgba, pctQuery.Select(s => s.GP).ToList(), true, true));
        datasetList.Add(GetDeductionsDataSets("Deductions", "percentage", SM_Tools.Colors.smNavyRgba, dedList.Select(s => Math.Round(s, 2)).ToList()));
        datasetList.Add(GetDeductionsDataSets("Dollar Amount: ", "currency", SM_Tools.Colors.smRedRgba, pctQuery.Select(s => s.DedAmount).ToList(), true, true));
        if (deductionQuery.Count > 0)
        {
            //Setup the Chart
            SM_Charts.ChartJS chartDeductions = new SM_Charts.ChartJS();
            //chartDeductions.type = "bar";        
            chartDeductions.chartType = SM_Enums.ChartType.bar.ToString();
            chartDeductions.labels = labelList;
            chartDeductions.showLegend = false;
            chartDeductions.dataSetList = datasetList;
            chartDeductions.canvasID = "cvDeductionsBar";
            chartDeductions.displayXaxisLabel = true;
            chartDeductions.afterLabelFunctionString = GetAfterDeductionLabelFunctionString("deductions");
            chartDeductions.stacked = true;
            ChartList.Add(chartDeductions);
        }





    }

    private string GetDeductionAfterBodyFunctionString(string v)
    {
        string ret = "";
        StringBuilder sb = new StringBuilder();
        switch (v)
        {
            case "deductions":
                {
                    sb.Append("\n");
                    sb.Append("var label = 'Dollars: ';");//The after Body Lable Name
                    sb.Append("\n");
                    sb.Append("var dataIndex = 0;");
                    sb.Append("\n");
                    sb.Append("if(tooltipItem[0].datasetIndex === 0){ dataIndex = 1;}");       //If it's the PCT dataset, show the corresponding dollar value dataset (1)
                    sb.Append("\n");
                    sb.Append("else { dataIndex = 3;}"); //else it's the GP Percent, and show the GP dollart value dataset (3)
                    sb.Append("\n");
                    sb.Append("return label + formatNumberString(data.datasets[dataIndex].data[tooltipItem[0].index], 'currency');");
                    sb.Append("\n");
                    break;
                }

        }

        ret = sb.ToString();
        return ret;
    }
    private string GetAfterDeductionLabelFunctionString(string v)
    {
        string ret = "";
        StringBuilder sb = new StringBuilder();
        switch (v)
        {
            case "deductions":
                {
                    sb.Append("\n");
                    sb.Append("var label = 'Amount: ';");//The after Body Lable Name
                    sb.Append("\n");
                    sb.Append("var dataIndex = 0;");
                    sb.Append("\n");
                    sb.Append("if(tooltipItem.datasetIndex === 0){ dataIndex = 1;}");       //If it's the PCT dataset, show the corresponding dollar value dataset (1)
                    sb.Append("\n");
                    sb.Append("else { dataIndex = 3;}"); //else it's the GP Percent, and show the GP dollart value dataset (3)
                    sb.Append("\n");
                    sb.Append("return label + formatNumberString(data.datasets[dataIndex].data[tooltipItem.index], 'currency');");
                    sb.Append("\n");
                    break;
                }

        }

        ret = sb.ToString();
        return ret;
    }

    private SM_Charts.chartJsDataSet GetDeductionsDataSets(string labelString, string labelFormat, string backGroundColor, List<double> dataList, bool hidden = false, bool iisToolTipAfterBody = false)
    {
        SM_Charts.chartJsDataSet ret = new SM_Charts.chartJsDataSet();
        ret.dataSetLabel = labelString;
        ret.labelFormat = labelFormat;
        ret.ToolTipAfterBodyFormat = labelFormat;
        ret.tooltipAfterLabelFormat = labelFormat;
        ret.backGroundColor = backGroundColor;
        ret.hidden = hidden;
        ret.isToolTipAfterBody = iisToolTipAfterBody;
        ret.maintainAspectRatio = false; //This allows the canvas style to dictate height.           
        ret.data = dataList.Select(s => s.ToString()).ToList();
        return ret;


    }

    private string GetDeductionsFooterFunctionStringString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("var footerData = 0;");
        sb.Append("\n");
        sb.Append("var label = '';");
        sb.Append("\n");
        sb.Append("var index = 0;");
        sb.Append("\n");
        sb.Append("tooltipItem.forEach(function(tooltipItem) {");
        sb.Append("\n");
        sb.Append("if(tooltipItem.datasetIndex == 0)");
        sb.Append("\n");
        sb.Append("index = 1;");
        sb.Append("\n");
        sb.Append("if(tooltipItem.datasetIndex == 3)");
        sb.Append("\n");
        sb.Append("index = 4;");
        sb.Append("\n");
        sb.Append("if(data.datasets.length <= index)");
        sb.Append("\n");
        sb.Append("return '';");
        sb.Append("\n");
        //sb.Append("sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];");
        sb.Append("label = '';");
        sb.Append("\n");
        //sb.Append("footerData = data.datasets[index].data[tooltipItem.index];");
        //sb.Append("\n");
        //sb.Append("label = data.datasets[index].label;");
        //sb.Append("\n");    

        ////This is specific to deductions for now, need to refactor
        sb.Append("if (index === 0)");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        sb.Append(" label = 'GP Dollars';");
        sb.Append("\n");
        sb.Append("footerData = data.datasets[1].data[tooltipItem.index];");
        sb.Append("\n");
        sb.Append(" }");
        sb.Append("\n");
        sb.Append("else if (index === 1)");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        sb.Append("label = 'Deduction Dollars';");
        sb.Append("\n");
        sb.Append("footerData = data.datasets[3].data[tooltipItem.index];");
        sb.Append("\n");
        sb.Append(" }");
        sb.Append("\n");



        sb.Append("\n");
        sb.Append("return label + footerData;");
        sb.Append("\n");
        sb.Append("});");
        return sb.ToString();
    }

    private void LoadPctReqToQuotePie()
    {
        decimal totalReqs = 0;
        decimal quotedReqs = 0;
        //GET the data
        var dataList = QuoteLinesInDateRange.GroupBy(g => (g.base_ordhed_uid == null || g.base_ordhed_uid == "")).ToList()
           .Select(g => new
           {
               type = g.Key == false ? "Quote" : "Req",
               gp = g.Sum(s => s.lineprofit),
               count = g.Count()
           }).ToList();
        if (QuoteLinesInDateRange.Count <= 0)
        {
            lblpctReqToQuoteNoData.Text = noDataText;
            return;
        }

        //Instantate a list of objects to hold the dataset(s)
        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();

        //Build the DataSet
        SM_Charts.chartJsDataSet cDataSet1 = new SM_Charts.chartJsDataSet();
        cDataSet1.data = dataList.OrderByDescending(o => o.gp).Select(s => s.count == 0 ? "0" : s.count.ToString()).ToList();
        datasetList.Add(cDataSet1);

        //AfterLabel Dataset
        SM_Charts.chartJsDataSet cDataSet2 = new SM_Charts.chartJsDataSet();
        cDataSet2.data = dataList.OrderByDescending(o => o.gp).Select(s => s.gp == 0 ? "0" : s.gp.ToString()).ToList();
        cDataSet2.dataSetLabel = "Gross Profit";
        cDataSet2.hidden = true;
        cDataSet2.isTooltipAfterLabel = true;
        cDataSet2.tooltipAfterLabelFormat = "currency";
        datasetList.Add(cDataSet2);

        //Setup the Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = "pie";
        Chart1.labels = dataList.OrderByDescending(o => o.gp).Select(s => s.type).ToList();
        Chart1.dataSetList = datasetList;
        Chart1.canvasID = "PctReqsQuotePie";
        ChartList.Add(Chart1);

        totalReqs = QuoteLinesInDateRange.Count();
        quotedReqs = QuoteLinesInDateRange.Count(w => !string.IsNullOrEmpty(w.base_ordhed_uid));
        pctReqToQuote = totalReqs == 0 ? 0 : (quotedReqs / totalReqs);
        pctReqToQuote = pctReqToQuote * 100;
        lblpctReqToQuote.Text = ": " + pctReqToQuote.ToString("#.##") + "%";
    }


    protected void lbExport_Click(object sender, EventArgs e)
    {
        string debugLog = "";

        LoadSalesBreakdown(out debugLog);
        tools.ExportListToCsv((List<SalesLogic.OrderQueryResult>)queryQuotedRz, "Quotes");
        tools.ExportListToCsv((List<SalesLogic.OrderQueryResult>)bookedQuery, "Booked");
        tools.ExportListToCsv((List<SalesLogic.OrderQueryResult>)invoicedQuery, "Billed");


    }
}
