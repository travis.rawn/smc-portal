﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="targeted_stock_buy_report.aspx.cs" Inherits="secure_sm_Reporting_sales_targeted_stock_buy_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" Runat="Server">
        <div id="pageTitle">
        <h1>Target Stock Buy Report</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" Runat="Server">
    
    
    
   

    <div class="row">
        <div class="col-sm-6">
            <p>All Stock Lines: <asp:Label ID="lblAllLinesValue" runat="server" /></p>
        </div>
        <div class="col-sm-6">
            <p>Stock Purchase Cost: <asp:Label ID="lblStockPurchaseValue" runat="server" /></p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <p>Stock Sales: <asp:Label ID="lblStockSaleValue" runat="server" /></p>
        </div>
        <div class="col-sm-6">
             <p>Profit: <asp:Label ID="lblStockProfit" runat="server" /></p>
        </div>
    </div>

    <uc1:sm_datatable runat="server" id="smdtAll" />
    
    <uc1:sm_datatable runat="server" id="smdtPurchases" />
    
    <uc1:sm_datatable runat="server" id="smdtSales" />


</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" Runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" Runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" Runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" Runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" Runat="Server">
</asp:Content>

