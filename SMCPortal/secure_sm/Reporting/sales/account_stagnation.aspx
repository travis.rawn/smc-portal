﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="account_stagnation.aspx.cs" Inherits="secure_sm_Reporting_sales_account_stagnation" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
    <div id="pageTitle">
        <h1>Account Stagnation</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-2">
            Date:
            <br />
            
            <uc1:datetime_flatpicker runat="server" ID="dtpLastSaleDate" />
        </div>
        <div class="col-sm-2">
            Minimum Sales:
            <br />
            <asp:TextBox ID="txtMinSales" runat="server" Text="10000"></asp:TextBox>

        </div>
        <div class="col-sm-1">
            <br />
            <asp:LinkButton ID="lbSearch" runat="server" OnClick="lbSearch_Click" Text="Search" CssClass="btn btn-sm btn-primary"></asp:LinkButton>
        </div>
        <div class="col-sm-1">
            <br />
            <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click" Text="Export" CssClass="btn btn-sm btn-primary"></asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

