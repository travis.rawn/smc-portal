﻿using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_sourcing_monitor_report : System.Web.UI.Page
{
    private List<SourcingLogic.SourcingObject> sourcingList;   
    DateTime startDate = new DateTime(2021, 01, 01);
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    //bool userIsSalesAdmin = false;
    //bool userIsManagement = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            rzap.SelectedIndexChanged += new EventHandler(rzap_SelectedIndexChanged);
            smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
            smdt_holds.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
            smdt_validationcomplete.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);

            smdt.showToggleButton = true;
            smdt_holds.showToggleButton = true;
            smdt_validationcomplete.showToggleButton = true;
            ClearDataTables();
            //LoadUserType();
            if (!Page.IsPostBack)
                LoadAgentPicker();

            sourcingList = SourcingLogic.GetSourcingObjects(startDate, DateTime.Today);
            LoadGrids();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

        

    }

    private void ClearDataTables()
    {
        smdt.dataSource = null;
        smdt.loadGrid();
        smdt_holds.dataSource = null;
        smdt_holds.loadGrid();
        smdt_validationcomplete.dataSource = null;
        smdt_validationcomplete.loadGrid();
    }

    //private void LoadUserType()
    //{
    //    userIsSalesAdmin = isSalesAdmin();
    //    userIsManagement = isSalesManagement();
    //}

    private void LoadAgentPicker()
    {
        List<string> teamList = new List<string>() { "sales", "sales admin" };
        List<n_user> agentList = rzt.GetUsersForTeams(teamList);
        rzap.LoadPicker(agentList);
        rzap.SelectedUserID = "all";
        //if (userIsManagement || userIsSalesAdmin)
        //    rzap.SelectedUserID = "all";
        //else //default it to the logged in agent's name.
        //{
        //    string rzId = Profile.RzUserID;
        //    if (string.IsNullOrEmpty(rzId))
        //        throw new Exception("Invalid Profile ID");
        //    rzap.SelectedUserID = rzId;
        //}

    }

    private void LoadGrids()
    {
        var query = sourcingList.Select(s => new
        {
            Sale = s.ordernumber,
            Type = s.orderType,
            Agent = s.agent,
            Customer = s.customer,
            Date = s.orderdate.ToShortDateString(),
            Total = s.ordertotal,
            Status = s.sourcing_status,
            PO = s.orderreference,
            Stage = s.validation_stage,
            Alert = string.IsNullOrEmpty(s.comment) ? "" : s.comment,
            lastValidationTrackingReason = s.lastValidationTrackingReason ?? ""

        });


        ////Admins only care about Quotes Ready to turn in.
        //if (!userIsManagement)
        //    if (userIsSalesAdmin)
        //        query = query.Where(w => w.Type == "Quote" && w.Status == "ReadyToValidate");
        //if (!userIsManagement && !userIsSalesAdmin)
        //    rzap.Enabled = false;

        


        //Show all agents if "choose" selected
        if (rzap.SelectedUserID != "all")
            query = query.Where(w => rzap.SelectedUserName == w.Agent);       

        List<string> validationStages = new List<string>() { "Validation", "PreValidation", "Quoting" };
        List<string> inProgressStatus = new List<string>() {"ReadyToValidate", "ReSourced" };
        //var inProgQuery = query.Where(w => inProgressStatus.Contains(w.Status) && validationStages.Contains(w.Stage)).OrderBy(o => o.Date).ThenByDescending(o => o.Total);
        var inProgQuery = query.Where(w => inProgressStatus.Contains(w.Status) || (w.Type == "Sale" && (validationStages.Contains(w.Stage)))  ).OrderBy(o => o.Date).ThenByDescending(o => o.Total);
        if (inProgQuery.Any())
        {
            smdt.dataSource = inProgQuery;
            smdt.pageLength = 50;
            smdt.sortColumn = 4;
            smdt.sortDirection = "desc";
            smdt.loadGrid();
        }




        //On Hold 
        List<string> holdStages = new List<string>() { "InspectionHold", "ValidationHold", "CustomerHold" };

        var holdQuery = query.Where(w => holdStages.Contains(w.Stage)).OrderBy(o => o.Date).ThenByDescending(o => o.Total);
        if (holdQuery.Any())
        {
            smdt_holds.dataSource = holdQuery;
            smdt_holds.pageLength = 50;
            smdt_holds.sortColumn = 4;
            smdt_holds.sortDirection = "desc";
            smdt_holds.loadGrid();
        }

        //ValidationComplete     

        var validationCompleteQuery = query.Where(w => w.Stage == "ValidationComplete").OrderBy(o => o.Date).ThenByDescending(o => o.Total);
        if (validationCompleteQuery.Any())
        {
            smdt_validationcomplete.dataSource = validationCompleteQuery;
            smdt_validationcomplete.pageLength = 50;
            smdt_validationcomplete.sortColumn = 4;
            smdt_validationcomplete.sortDirection = "desc";
            smdt_validationcomplete.loadGrid();
        }

    }

    private bool isSalesManagement()
    {
        if (string.IsNullOrEmpty(Profile.RzUserID))
            return false;

        n_user u = null;
        using (RzDataContext rdc = new RzDataContext())
            u = rdc.n_users.Where(w => w.unique_id == Profile.RzUserID).FirstOrDefault();
        if (u == null)
            return false;

        List<string> mgmtTeamNames = new List<string>() { "Sales Management", "SuperAdmin" };
        List<n_team> userTeams = rzt.GetTeamsForUser(u);
        List<string> userTeamNames = userTeams.Select(s => s.name).Distinct().ToList();

        return mgmtTeamNames.Intersect(userTeamNames).Any();
    }

    private bool isSalesAdmin()
    {
        if (Membership.GetUser().UserName.ToLower() == "tabithak")
            return true;
        if (string.IsNullOrEmpty(Profile.RzUserID))
            return false;

        n_user u = null;
        using (RzDataContext rdc = new RzDataContext())
            u = rdc.n_users.Where(w => w.unique_id == Profile.RzUserID).FirstOrDefault();
        if (u == null)
            return false;

        List<string> adminTeamNames = new List<string>() { "Sales Admin" };
        List<n_team> userTeams = rzt.GetTeamsForUser(u);
        List<string> userTeamNames = userTeams.Select(s => s.name).Distinct().ToList();

        return adminTeamNames.Intersect(userTeamNames).Any();

    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Hide the validation hold reason row.

        if (e.Row.Cells.Count >= 9)
            e.Row.Cells[9].Visible = false;


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Internal Comment ToolTip           
            string internal_comment = e.Row.Cells[8].Text;
            if (internal_comment == "&nbsp;")
                internal_comment = null;
            if (string.IsNullOrEmpty(internal_comment))
            {
                e.Row.Cells[9].Text = "N/A";

            }
            else
            {
                assets_controls_sm_tooltip tt = (assets_controls_sm_tooltip)Page.LoadControl("~/assets/controls/sm_tooltip.ascx");
                tt.dataPlacement = "left";
                tt.toolTipBody = e.Row.Cells[8].Text;
                tt.toolTipTitle = "⚠";
                tt.CssClass = "smdt-tooltip";
                tt.LoadTooltip();
                e.Row.Cells[8].Text = "";
                e.Row.Cells[8].Controls.Add(tt);
            }
            
        }
    }

    private void rzap_SelectedIndexChanged(object sender, EventArgs e)
    {

    }




}