﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="engagement_performance.aspx.cs" Inherits="secure_sm_Reporting_sales_engagement_performance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="pageTitle">
        <h1>Engagement Performance (Alpha)</h1>
    </div>


    <div class="row">
        <div class="col-sm-2">
            
            <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
        </div>
        <div class="col-sm-2">
            
            <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
        </div>
        <div class="col-sm-2" style="text-align: center;">
            <asp:LinkButton ID="lbSearch" runat="server" CssClass="form-control btn-primary" OnClientClick="ShowUpdateProgress();">Refresh</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-sm-4">
               

            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
            </div>

        </div>
        <div class="col-sm-12">
            <uc1:sm_datatable runat="server" ID="smdt_summary" />
        </div>

    </div>





</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div style="margin: 10px;">

        <div class="row">
            <div class="col-sm-12">
                <a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
                <div id="divDetails" style="display: none;">
                    <uc1:sm_datatable runat="server" ID="smdt_details" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

