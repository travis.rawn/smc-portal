﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="source_tbd_report.aspx.cs" Inherits="secure_sm_Reporting_sales_source_tbd_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
        .label-total {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Source TBD Report</h1>
    </div>
    <p>The following lines are on open sales orders, and have no vendor to source parts from. </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="label-total">
        <h5>
            Total Potential Sales:
            <asp:Label ID="lblTotalTBD" runat="server"></asp:Label>
        </h5>
        
    </div>


    <div class="row">
        <div class="col-sm-12">
            <asp:LinkButton ID="lbSearch" runat="server" OnClientClick="ShowUpdateProgress();">Refresh</asp:LinkButton>            | 
            <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">Export</asp:LinkButton>
            |
            <asp:LinkButton ID="lbEmail" runat="server" OnClick="lbEmail_Click">Send Email</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

