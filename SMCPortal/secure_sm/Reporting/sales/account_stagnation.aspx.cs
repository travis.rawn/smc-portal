﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_account_stagnation : System.Web.UI.Page
{
    [Serializable]
    class StagnantAccount
    {
        public string Company { get; set; }
        public string Agent { get; set; }
        public string CompanyType { get; set; }
        public string LastInvoice { get; set; }
        public DateTime LastDate { get; set; }
        public double TotalInvoiced { get; set; }

    }
    SM_Tools tools = new SM_Tools();
    public double MinimumSalesThreshold { get; set; }
    public DateTime LastSaleDate { get; set; }



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LoadVariables();


            if (!Page.IsPostBack)
                LoadData();


            LoadGrid();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void LoadVariables()
    {
        //Get valid Minimum Sales Threshold
        double minSalesThresh = 0;
        if (!Double.TryParse(txtMinSales.Text ?? "", out minSalesThresh))
            throw new Exception(txtMinSales.Text + " is not a valid dollar amount.");

        MinimumSalesThreshold = minSalesThresh;

        //GEt Valid Order Since Date
        if (!Page.IsPostBack)
            dtpLastSaleDate.SelectedDate = DateTime.Today.AddMonths(-6); // Default
        DateTime lastSaleDate = dtpLastSaleDate.SelectedDate.Value;
        if (!DateTime.TryParse(dtpLastSaleDate.SelectedDate.Value.ToString(), out lastSaleDate))
            throw new Exception(dtpLastSaleDate.SelectedDate.Value.ToString() + " is not a valid date.");


        LastSaleDate = lastSaleDate;
    }

    private void LoadGrid()
    {
        if (ViewState["StagnaneAccountData"] == null)
            LoadData();
        List<StagnantAccount> sList = (List<StagnantAccount>)ViewState["StagnaneAccountData"];

        smdt.dataSource = sList.Select(s => new
        {
            s.Company,
            s.Agent,
            //s.CompanyType,
            LastDate = s.LastDate.ToShortDateString(),
            TotalInvoiced = s.TotalInvoiced.ToString("C")
        });
        smdt.dateTimeFormat = "'l'";
        smdt.loadGrid();

    }

    private void LoadData()
    {
        //DateTime DateThresh = new DateTime(2020, 01, 01);
        //double SaleThresh = 10000;
        using (RzDataContext rdc = new RzDataContext())
        {


            var query = rdc.ordhed_invoices
            .Where(w => (w.isvoid ?? false) != true)
            .Join(rdc.companies, i => i.base_company_uid, c => c.unique_id, (i, c) => new
            {
                InvoiceID = i.unique_id,
                InvoiceNumber = i.ordernumber,
                Invoicevoided = i.isvoid ?? false,
                TotalInvoiced = i.ordertotal,
                Agent = i.agentname,
                OrderDate = i.orderdate.Value.Date,
                CompanyName = c.companyname,
                CompanyID = c.unique_id,
                CompanyType = c.companytype

            })
            .Where(w => !w.CompanyType.ToLower().Contains("vendor") && w.CompanyType != "E-Sales" && !w.CompanyType.ToLower().Contains("dead"))
            .GroupBy(g => g.CompanyID)
            .Where(w => w.Max(m => m.OrderDate) < LastSaleDate)
            .Select(s => new StagnantAccount
            {
                Company = s.Max(m => m.CompanyName),
                Agent = s.Max(m => m.Agent),
                CompanyType = s.Max(m => m.CompanyType),
                LastInvoice = s.Max(m => m.InvoiceNumber),
                LastDate = s.Max(m => m.OrderDate),
                TotalInvoiced = Math.Round(s.Sum(m => m.TotalInvoiced ?? 0), 2, MidpointRounding.AwayFromZero)
            })
            .Where(w => w.TotalInvoiced >= MinimumSalesThreshold);

            ViewState["StagnaneAccountData"] = query.ToList();
        }
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        LoadData();
        LoadGrid();
    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        LoadGrid();
        tools.ExportListToCsv((List<StagnantAccount>)ViewState["StagnaneAccountData"], "account_stagnation.csv");
    }
}