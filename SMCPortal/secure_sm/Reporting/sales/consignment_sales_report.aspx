﻿<%@ Page Title="Consignment Sales Report" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="consignment_sales_report.aspx.cs" Inherits="secure_sm_Reporting_sales_consignment_sales_report" %>

<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="lbSearch">
        <div class="panel panel-default card">
            <div class="panel-body">

                <div class="row">

                    <div class="col-sm-3">
                        <label>Consginment Partner:</label><br />
                        <asp:DropDownList class="form-control" runat="server" ID="ddlCompany" Visible="true">
                            <asp:ListItem Text="All"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <label>Start Date:</label>
                        <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
                    </div>
                    <div class="col-sm-3">
                        <label>End Date:</label>
                        <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
                    </div>
                    <div class="col-sm-3">
                        <div class="checkbox">
                            <label>
                                <asp:CheckBox runat="server" ID="cbxOnlyPaidInFull" Checked="true" AutoPostBack="false" TextAlign="Left" />
                                Only Fully Paid   <a href="#" style="color: black;" data-toggle="tooltip" title="Consignment lines that belong to invoices that been 'Fully Paid' within the date range specified."><span class="glyphicon glyphicon-question-sign"></span></a>
                            </label>
                            <asp:LinkButton ID="lbSearch" runat="server" OnClick="lbSearch_Click" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress();">
                          <i class="fa fa-search" style="font-size:20px;"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </asp:Panel>
    <div class="panel panel-default card">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <h4>Count:
                                    <asp:Label ID="lblCount" runat="server" Style="margin-right: 15px;"></asp:Label>
                    </h4>
                </div>
                <div class="col-sm-3">

                    <h4>Total:
                                    <asp:Label ID="lblTotal" runat="server" Style="margin-right: 15px;"></asp:Label>
                    </h4>
                </div>
                <div class="col-sm-3">
                    <h4>Pay:
                        <asp:Label ID="lblTotalPay" runat="server" Style="margin-right: 15px; color: red;"></asp:Label>
                    </h4>
                </div>
                <div class="col-sm-3">
                    <h4>Keep:
                        <asp:Label ID="lblTotalKeep" runat="server" Style="margin-right: 15px; color: forestgreen;"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div style="overflow-x: auto;">
                        <a href="#" onclick="PrintPage();" class="btn-smc btn-smc-primary">Print</a>
                        <asp:Button ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" CssClass="btn-smc btn-smc-primary"/>
                        <asp:Panel ID="pnlPrintGridview" runat="server">
                            <asp:GridView ID="gvConsginmentSales" runat="server" CssClass="table table-hover table-striped tablesorter" GridLines="None" EmptyDataText="No consignment sales found for selected company." OnRowDataBound="gvConsginmentSales_RowDataBound"></asp:GridView>
                        </asp:Panel>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>

        function PrintPage() {
            var startDate = $("#MainContent_dtpStart_txtDate").val();
            var endDate = $("#MainContent_dtpEnd_txtDate").val();
            var company = $("#MainContent_ddlCompany option:selected").text();
            var total = $("#MainContent_lblTotal").text();
            var pay = $("#MainContent_lblTotalPay").text();
            var keep = $("#MainContent_lblTotalKeep").text();

            var Title = "<center><h2>Consignment Sales Report for '"+company+"'</h2> <h3>From: "+startDate +" To "+endDate + "</h3></center>";
            var Footer = "<center>Total: " + total + "  <span style=\"color:red\">Pay: " + pay + "</span> <span style=\"color:green\">Keep: " + keep + "</span></center>";
            var printContent = $("#MainContent_pnlPrintGridview");

            var printWindow = window.open("All Records", "Print Panel", 'left=50000,top=50000,width=0,height=0');

            printWindow.document.write(Title + printContent[0].innerHTML + Footer);

            printWindow.document.close();

            printWindow.focus();

            printWindow.print();
            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

