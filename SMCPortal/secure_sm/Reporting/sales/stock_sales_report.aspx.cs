﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_stock_sales_report : System.Web.UI.Page
{
    [Serializable]
    public class stock_report_object
    {
        public string partNumber { get; set; }
        public string mfg { get; set; }
        public string status { get; set; }
        public string partrecordID { get; set; }
        public string shippedStockID { get; set; }
        public string orddet_lineID { get; set; }
        public double unit_price { get; set; }
        public double total_price { get; set; }
        public double unit_cost { get; set; }
        public double total_cost { get; set; }
        public long qty { get; set; }
        public double gp { get; set; }
        public string inventory_link_uid { get; set; }
        public string customerName { get; set; }
        public string vendorName { get; set; }
        public string poNumber { get; set; }
        public string poDate { get; set; }
        public string poID { get; set; }
        public string soNumber { get; set; }
        public string soID { get; set; }
        public string soDate { get; set; }
        public string salesAgent { get; set; }
        public string purchaseAgent { get; set; }
        public double quoteCount { get; set; }
        public double lastQuotedUnitPrice { get; set; }
        public string type { get; set; }
    }


    public List<stock_report_object> sroAllStock { get { return ViewState["sroAllStock"] as List<stock_report_object>; } set { ViewState["sroAllStock"] = value; } }
    public List<stock_report_object> sroSoldStock { get { return ViewState["sroSoldStock"] as List<stock_report_object>; } set { ViewState["sroSoldStock"] = value; } }
    public List<stock_report_object> sroUnsoldStock { get { return ViewState["sroUnsoldStock"] as List<stock_report_object>; } set { ViewState["sroUnsoldStock"] = value; } }

    List<ordhed_purchase> stockPOList;
    List<string> stockPOIDList;
    List<orddet_line> allPOStockLinesList;
    List<string> allStockPartNumbersList;
    List<orddet_line> soldStockLinesList;
    List<partrecord> unSoldPartrecordsList;
    //List<stock_report_object> sroList;
    List<n_user> userList;
    SM_Tools tools = new SM_Tools();
    TimeSpan loadTime = new TimeSpan();
    bool includeLotBuys = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            DateTime ReportStart = DateTime.Now;
            includeLotBuys = smcbIncludeLots.isChecked;

            LoadDates();
            using (RzDataContext rdc = new RzDataContext())
            {

                stockPOList = rdc.ordhed_purchases.Where(w => w.is_stock == true && w.isvoid.Value != true && (w.date_created.Value.Date >= dtpStart.SelectedDate && w.date_created.Value.Date <= dtpEnd.SelectedDate)).ToList();
                LoadUsers(rdc);
                if (rzap.SelectedUserID != "all")
                    stockPOList = stockPOList.Where(w => w.base_mc_user_uid == rzap.SelectedUserID).ToList();
                stockPOIDList = stockPOList.Select(s => s.unique_id).Distinct().ToList();
                LoadStockPos();
                LoadSoldPoStock_line(rdc);
                LoadUnSoldPoStock(rdc);
            }


            LoadReport();

            DateTime ReportEnd = DateTime.Now;
            loadTime = ReportEnd - ReportStart;
            tools.HandleResult("Success", "Processing time: " + loadTime.Minutes + ":" + loadTime.Seconds);
        }
        catch (Exception ex)
        {
            string message = "Processing time: " + loadTime.Minutes + ":" + loadTime.Seconds + "<br /><br />";
            message += ex.Message;
            tools.HandleResult("fail", message);
        }


    }

    private void LoadStockPos()
    {
        smdtAllPoStock.dataSource = stockPOList.Select(s => new { s.companyname, Date = s.orderdate.Value.ToShortDateString(), s.ordernumber, Total = s.ordertotal.Value.ToString("C"), s.agentname, s.is_stock });
        smdtAllPoStock.loadGrid();
    }

    private void LoadUsers(RzDataContext rdc)
    {
        if (Page.IsPostBack)
            return;
        List<string> userNameList = stockPOList.Select(s => s.agentname).Distinct().ToList();
        userList = rdc.n_users.Where(w => userNameList.Contains(w.name)).ToList();
        rzap.DefaultSelectionName = "Joe Mar";
        rzap.LoadPicker(userList);
    }

    private void LoadDates()
    {
        if (Page.IsPostBack)
            return;
        dtpStart.SelectedDate = new DateTime(2020, 01, 01);
        dtpEnd.SelectedDate = DateTime.Today;
    }

    //private void LoadAllPoStock(RzDataContext rdc)
    //{
    //    //Load All lines related to Stock POs
    //    //allPOStockLinesList = rdc.orddet_lines.Where(w => stockPOIDList.Contains(w.orderid_purchase)).ToList();
    //    //Load partnumbers list
    //    //allStockPartNumbersList = allPOStockLinesList.Where(w => w.fullpartnumber.Length > 0).Select(s => s.fullpartnumber.Trim().ToUpper()).Distinct().ToList();
    //    //Create the all stock list.
    //    sroAllStock = allPOStockLinesList.Select(s => new stock_report_object { partNumber = s.fullpartnumber, qty = (long)s.quantity, mfg = s.manufacturer, vendorName = s.vendor_name, poNumber = s.ordernumber_purchase, unit_cost = s.unit_cost.Value, total_cost = s.total_cost.Value, purchaseAgent = s.buyer_name }).ToList();
    //    //Format for Grid
    //    smdtAllPoStock.dataSource = sroAllStock.Select(s => new { Part = s.partNumber, QTY = s.qty, MFG = s.mfg, Vendor = s.vendorName, PO = s.poNumber, Buyer = s.purchaseAgent, UnitCost = s.unit_cost.ToString("C") });

    //    //Add 
    //    smdtAllPoStock.pageLength = 50;
    //    smdtAllPoStock.loadGrid();
    //}

    private void LoadSoldPoStock_old(RzDataContext rdc)
    {
        //Lines labeled as stock, created within date range, but the poID is NOT the sto PO.  Also using a partnumber lookup to ensure these relate to stock parts, not partnumbers that don't exist in the stock list.  

        //Load Sold orddet_lines
        soldStockLinesList = rdc.orddet_lines.Where(w => w.date_created.Value >= dtpStart.SelectedDate & allStockPartNumbersList.Contains(w.fullpartnumber.Trim().ToUpper()) && w.stocktype.ToLower() == "stock" && w.orderid_sales.Length > 0).ToList();
        //Create the sro Sold List
        sroSoldStock = soldStockLinesList.Select(s => new stock_report_object { partNumber = s.fullpartnumber, qty = (long)s.quantity, mfg = s.manufacturer, status = s.status, vendorName = s.vendor_name, poNumber = s.ordernumber_purchase, customerName = s.customer_name, soNumber = s.ordernumber_sales, unit_price = s.unit_price.Value, total_price = s.total_price.Value, unit_cost = s.unit_cost.Value, total_cost = s.total_cost.Value, gp = s.gross_profit.Value, purchaseAgent = s.buyer_name }).ToList();


        //Format sro object for Grid
        smdtSoldStock.dataSource = sroSoldStock.Select(s => new { Part = s.partNumber, QTY = s.qty, MFG = s.mfg, Customer = s.customerName, SO = s.soNumber, Status = s.status, Vendor = s.vendorName, UnitPrice = s.unit_price.ToString("C"), UnitCost = s.unit_cost.ToString("C") });
        smdtSoldStock.pageLength = 50;
        smdtSoldStock.loadGrid();
    }
    private void LoadSoldPoStock_line(RzDataContext rdc)
    {


        if (sroAllStock == null)
            sroSoldStock = new List<stock_report_object>();
        List<orddet_line> lineList = rdc.orddet_lines.Where(w => w.stocktype == "Stock" && w.status == "shipped" && (w.date_created.Value >= dtpStart.SelectedDate && w.date_created.Value <= dtpEnd.SelectedDate)).ToList();

        //Get a list of all shopped stock matching buy_purchase_id to our stock list
        List<string> po_ids = stockPOList.Select(s => s.unique_id).Distinct().ToList();
        //List<shipped_stock> ssList = rdc.shipped_stocks.Where(w => po_ids.Contains(w.buy_purchase_id) && (w.date_created.Value >= dtpStart.SelectedDate && w.date_created.Value <= dtpEnd.SelectedDate)).ToList();
        List<shipped_stock> ssList = rdc.shipped_stocks.Where(w => (w.date_created.Value >= dtpStart.SelectedDate && w.date_created.Value <= dtpEnd.SelectedDate)).ToList();


        //Counter for debugging
        int i = 0;
        foreach (orddet_line l in lineList)
        {

            stock_report_object sro = new stock_report_object();



            //Line Information
            // orddet_line l = lineList.Where(w => w.inventory_link_uid == ss.unique_id && (w.orderid_sales ?? "").Length > 0).FirstOrDefault();
            sro.partNumber = l.fullpartnumber;
            sro.customerName = l.customer_name ?? "Unknown";
            sro.unit_cost = l.unit_cost ?? 0;
            sro.unit_price = l.unit_price ?? 0;
            sro.gp = l.gross_profit.Value;
            sro.lastQuotedUnitPrice = 0;
            sro.mfg = l.manufacturer ?? "Unknown";
            sro.orddet_lineID = l.unique_id ?? "Unknown";
            sro.partrecordID = "Not Set";
            sro.qty = l.quantity.Value;
            sro.quoteCount = 0;
            sro.salesAgent = l.seller_name ?? "Unknown";
            sro.inventory_link_uid = l.inventory_link_uid;
            sro.soDate = l.orderdate_sales.Value.Date.ToShortDateString() ?? "Unknown";
            sro.soID = l.orderid_sales ?? "Unknown"; ;
            sro.soNumber = l.ordernumber_sales ?? "Unknown";
            sro.status = l.status ?? "Unknown"; ;
            sro.total_cost = l.total_cost.Value;
            sro.total_price = l.total_price.Value;
            sro.unit_cost = l.unit_cost.Value;
            sro.unit_price = l.unit_price.Value;
            if (!string.IsNullOrEmpty(l.inventory_link_uid))
            {
                //ShippedStock info - for PO Linkage

                shipped_stock ss = ssList.Where(w => w.unique_id == l.inventory_link_uid).FirstOrDefault();
                if (ss == null)
                {
                    string searchKluge = (l.fullpartnumber + l.quantity).Trim().ToUpper();
                    ss = ssList.Where(w => (w.fullpartnumber + w.quantity).Trim().ToUpper() == searchKluge).FirstOrDefault();
                }


                if (ss != null)
                {
                    sro.shippedStockID = ss.unique_id;
                    //PO Information
                    ordhed_purchase po = stockPOList.Where(w => w.unique_id == ss.buy_purchase_id).FirstOrDefault();
                    if (po != null)
                    {
                        sro.poID = po.unique_id ?? "Unknown";
                        sro.poNumber = po.ordernumber ?? "Unknown";
                        sro.purchaseAgent = po.agentname ?? "Unknown";
                        sro.vendorName = po.companyname ?? "Unknown";
                        sro.poDate = po.orderdate.Value.ToShortDateString();
                        //Add to list
                        sro.type = "POLinked";

                    }
                    else
                        sro.type = "NoPO";

                    //Anything with a ShippedStock, and or LinkedPO gets added always
                    //sroSoldStock.Add(sro);
                }
                else
                {
                    sro.type = "Lot";
                    //We only included the rest if includeLotBuys is checked.
                    //if (includeLotBuys)                       
                    //sroSoldStock.Add(sro);
                }


            }


            if (sro.type == "POLinked")
                sroSoldStock.Add(sro);
            else if (includeLotBuys)
                sroSoldStock.Add(sro);

            ////Add to list
            //sroSoldStock.Add(sro);
            i++;
        }

        //Format sro object for Grid
        smdtSoldStock.dataSource = sroSoldStock.Select(s => new { Part = s.partNumber, QTY = s.qty, MFG = s.mfg, Customer = s.customerName, SO = s.soNumber, Status = s.status, Vendor = s.vendorName, UnitPrice = s.unit_price.ToString("C"), UnitCost = s.unit_cost.ToString("C"), GP = s.gp.ToString("C"), PO = s.poNumber, PODate = s.poDate, Type = s.type });
        smdtSoldStock.pageLength = 10;
        smdtSoldStock.loadGrid();




    }
    private void LoadSoldPoStock_SS(RzDataContext rdc)
    {
        //This is shipped stock-centric, not finding direct linkage to line items all the time, therefore, for sales value, first need to query lines
        //--Sold(Shipped) Stock
        //--select fullpartnumber, companyname, quantity, (select is_stock from ordhed_purchase where unique_id = buy_purchase_id) [Is_Stock] From shipped_stock where buy_purchase_id in (select distinct unique_id from ordhed_purchase where is_stock = 1) order by companyname desc
        //select ss.fullpartnumber, l.orderdate_sales, l.unit_price, l.unit_cost, l.quantity, l.ordernumber_sales, l.ordernumber_purchase, l.customer_name[Customer], po.companyname[Vendor], ss.quantity[Shipped QTY]
        //From shipped_stock ss
        //inner join ordhed_purchase po on ss.buy_purchase_id = po.unique_id
        //inner join orddet_line l on l.inventory_link_uid = ss.unique_id
        //where ss.buy_purchase_id in (select distinct unique_id from ordhed_purchase where is_stock = 1) and LEN(orderid_sales) > 0 
        //--and LEN(ordernumber_purchase) > 0

        if (sroAllStock == null)
            sroSoldStock = new List<stock_report_object>();
        //Get a list of all shopped stock matching buy_purchase_id to our stock list
        List<string> po_ids = stockPOList.Select(s => s.unique_id).Distinct().ToList();
        var ssList = rdc.shipped_stocks.Where(w => po_ids.Contains(w.buy_purchase_id)).ToList();
        List<string> ss_ids = ssList.Select(s => s.unique_id).Distinct().ToList();
        var lineList = rdc.orddet_lines.Where(w => ss_ids.Contains(w.inventory_link_uid)).ToList();

        //Counter for debugging
        int i = 0;

        foreach (shipped_stock ss in ssList)
        {
            stock_report_object sro = new stock_report_object();

            //shipped_Stock_info
            sro.partNumber = ss.fullpartnumber ?? "Unknown";

            //PO Information
            ordhed_purchase po = stockPOList.Where(w => w.unique_id == ss.buy_purchase_id).FirstOrDefault();
            if (po != null)
            {
                sro.poID = po.unique_id ?? "Unknown";
                sro.poNumber = po.ordernumber ?? "Unknown";
                sro.purchaseAgent = po.agentname ?? "Unknown";
                sro.vendorName = po.companyname ?? "Unknown";
            }


            //Line Information
            orddet_line l = lineList.Where(w => w.inventory_link_uid == ss.unique_id && (w.orderid_sales ?? "").Length > 0).FirstOrDefault();
            if (l != null)
            {
                sro.customerName = l.customer_name ?? "Unknown";
                sro.unit_cost = l.unit_cost ?? 0;
                sro.unit_price = l.unit_price ?? 0;
                sro.gp = l.gross_profit.Value;
                sro.lastQuotedUnitPrice = 0;
                sro.mfg = l.manufacturer ?? "Unknown";
                sro.orddet_lineID = l.unique_id ?? "Unknown";
                sro.partrecordID = "Not Set";
                sro.qty = l.quantity.Value;
                sro.quoteCount = 0;
                sro.salesAgent = l.seller_name ?? "Unknown";
                sro.shippedStockID = ss.unique_id;
                sro.soDate = l.orderdate_sales.Value.Date.ToShortDateString() ?? "Unknown";
                sro.soID = l.orderid_sales ?? "Unknown"; ;
                sro.soNumber = l.ordernumber_sales ?? "Unknown";
                sro.status = l.status ?? "Unknown"; ;
                sro.total_cost = l.total_cost.Value;
                sro.total_price = l.total_price.Value;
                sro.unit_cost = l.unit_cost.Value;
                sro.unit_price = l.unit_price.Value;

            }

            //Add to list
            sroSoldStock.Add(sro);
            i++;

        }


        //Format sro object for Grid
        smdtSoldStock.dataSource = sroSoldStock.Select(s => new { Part = s.partNumber, QTY = s.qty, MFG = s.mfg, Customer = s.customerName, SO = s.soNumber, Status = s.status, Vendor = s.vendorName, UnitPrice = s.unit_price.ToString("C"), UnitCost = s.unit_cost.ToString("C"), GP = s.gp.ToString("C"), STatus = s.status });
        smdtSoldStock.pageLength = 50;
        smdtSoldStock.loadGrid();

    }




    private void LoadUnSoldPoStock(RzDataContext rdc)
    {


        //Load Un sold Partrecords
        unSoldPartrecordsList = rdc.partrecords.Where(w => w.stocktype == "Stock" && w.fullpartnumber.Length > 0 && w.quantity > 0).ToList();
        //Create the sro Unsold List
        sroUnsoldStock = unSoldPartrecordsList.Select(s => new stock_report_object { partNumber = s.fullpartnumber, qty = (long)s.quantity, mfg = s.manufacturer, vendorName = s.companyname, poID = s.buy_purchase_id, unit_cost = s.cost.Value, purchaseAgent = s.agentname }).ToList();
        if (!includeLotBuys)
            sroUnsoldStock = sroUnsoldStock.Where(w => (w.poID).Length > 0).ToList();

        //List<string> unsoldPOIds = sroUnsoldStock.Select(s => s.poID).Distinct().ToList();
        //List<ordhed_purchase> unsoldPos = rdc.ordhed_purchases.
        foreach (stock_report_object sro in sroUnsoldStock)
        {

            ordhed_purchase po = stockPOList.Where(w => sro.poID == w.unique_id).FirstOrDefault();
            if (po == null)
                po = rdc.ordhed_purchases.Where(w => w.unique_id == sro.poID).FirstOrDefault();
            if (po != null)
            {
                sro.poNumber = po.ordernumber;
                sro.poDate = po.orderdate.Value.ToShortDateString();
            }
                
            else
                sro.poNumber = "LotBuy";
        }

        ////Apply Extra Logic
        //sroUnsoldStock = sroApplyExtraLogic(rdc, sroUnsoldStock);


        //Format sro object for Grid
        
        var query = sroUnsoldStock.Select(s => new { Part = s.partNumber, MFG = s.mfg, Vendor = s.vendorName, PO = s.poNumber, poDate = s.poDate, QTY = s.qty, UnitCost = s.unit_cost.ToString("C"), LastQuotePrice = s.lastQuotedUnitPrice, TotQuotes = s.quoteCount });
        int count = query.Count();
        smdtUnsoldStock.dataSource = query;
        smdtUnsoldStock.pageLength = 50;
        smdtUnsoldStock.loadGrid();
    }

    private List<stock_report_object> sroApplyExtraLogic(RzDataContext rdc, List<stock_report_object> sroList)
    {
        //Instantiate ret as a duplicate copy of source list.
        List<stock_report_object> ret = sroList.ToList();
        List<string> uniqueSroParts = new List<string>();
        uniqueSroParts = ret.Select(s => s.partNumber.Trim().ToUpper()).Distinct().ToList();
        //Search t hte lsit of lines for any matchine quotes.
        //At time of coding, there were 73K Quotes
        int skip = 0;
        bool cont = true;
        while (cont)
        {
            int take = 2000;

            List<orddet_quote> matchedQuotesList = new List<orddet_quote>();
            List<string> partialList = uniqueSroParts.Skip(skip).Take(take).ToList();
            if (partialList.Count <= 0 || partialList == null)
            {
                cont = false;
                continue;
            }


            matchedQuotesList = rdc.orddet_quotes.Where(w => (w.fullpartnumber ?? "").Trim().ToUpper().Length > 3 && partialList.Contains(w.fullpartnumber.Trim().ToUpper())).ToList();
            ////Get matched parts in memory for iteration
            //List<string> matchedSroPartsList = quotesList.Select(s => s.fullpartnumber.Trim().ToUpper()).Distinct().ToList();
            int i = 0;
            foreach (stock_report_object sro in ret)
            {
                i++;
                sro.lastQuotedUnitPrice = 0;
                sro.quoteCount = 0;
                List<orddet_quote> matchedSroList = new List<orddet_quote>();
                matchedSroList = matchedQuotesList.Where(s => s.fullpartnumber.Trim().ToUpper() == sro.partNumber.Trim().ToUpper()).ToList();
                if (matchedSroList.Count > 0)
                {
                    orddet_quote oldest_quote = matchedSroList.OrderBy(oo => oo.date_created.Value).FirstOrDefault();
                    orddet_quote newest_quote = matchedSroList.OrderBy(oo => oo.date_created.Value).LastOrDefault();
                    double max_quote_value = matchedSroList.OrderByDescending(o => (o.unitprice ?? 0)).First().unitprice ?? 0;
                    double min_quote_value = matchedSroList.OrderBy(o => (o.unitprice ?? 0)).First().unitprice ?? 0;
                    double totalQuotes = matchedSroList.Count();


                    //set SRO Values
                    sro.lastQuotedUnitPrice = newest_quote.unitprice ?? 0;
                    sro.quoteCount = totalQuotes;


                }


            }


            skip += take;
        }





        return ret;
    }



    private void LoadReport()
    {

        double total_cost = sroSoldStock.Sum(s => s.total_cost);
        double total_price = sroSoldStock.Sum(s => s.total_price);
        // double sold_total_cost = sroSoldStock.Sum(s => s.unit_cost);
        double gross_profit = sroSoldStock.Sum(s => s.gp);
        //   (gross_profit / total_price) / total_price == X%
        double gross_margin = ((gross_profit - total_cost) / gross_profit);// * 100;
                                                                           ////Overall
                                                                           //lblTotalSales.Text = "Total Sales: " + sold_total_price.ToString("C");
                                                                           //lblTotalPOCost.Text = "Total Cost: " + total_stock_cost.ToString("C");
                                                                           //lblTotalGP.Text = "Total GP: " + gross_profit.ToString("C");

        //All Stock Lines Section       
        lblAllPOCount.Text = stockPOList.Count().ToString();
        lblAllPOCost.Text = stockPOList.Sum(s => s.ordertotal.Value).ToString("C");

        //Stock Sales Section
        lblSoldTotalSales.Text = total_price.ToString("C");
        lblSoldTotalCost.Text = total_cost.ToString("C");
        lblSoldTotalGp.Text = gross_profit.ToString("C");
        lblSoldGpMargin.Text = gross_margin.ToString("P"); // The "P" handles the *100 conversion.

        //Unsold Stock Section
        lblUnsoldLineCount.Text = sroUnsoldStock.Count().ToString();
        lblUnsoldCost.Text = sroUnsoldStock.Sum(s => (s.unit_cost * s.qty)).ToString("C");
    }

    protected void exportStockPOLines_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv((List<stock_report_object>)sroAllStock, "All_Stock_Purchase_Lines");
    }

    protected void lbExportSoldStock_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv((List<stock_report_object>)sroSoldStock, "Sold_Stock_Purchase_Lines");
    }

    protected void lbExportUnSoldStock_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv((List<stock_report_object>)sroUnsoldStock, "Unsold_Stock_Lines");
    }
}