﻿<%@ Page Title="Engagement Activity Report" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="engagement_activity.aspx.cs" Inherits="secure_sm_Sales_Screens_engagement_activity" %>


<%@ Register Src="~/assets/controls/RzAgentPicker.ascx" TagPrefix="uc1" TagName="RzAgentPicker" %>
<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/sm_chartjs.ascx" TagPrefix="uc1" TagName="sm_chartjs" %>
<%@ Register Src="~/assets/controls/sm_tooltip.ascx" TagPrefix="uc1" TagName="sm_tooltip" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>







<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Engagement Activity</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-default card">
        <div class="panel-heading">

            <div class="form-inline">
                <label>Total Calls:</label><asp:Label ID="lbltotalCalls" runat="server"></asp:Label>
                | 
           <label>Emails:</label><asp:Label ID="lblTotalEmails" runat="server"></asp:Label>
                |
           <label>Notes:</label><asp:Label ID="lbltotalNotes" runat="server"></asp:Label>
                | 
           <label>Tasks:</label><asp:Label ID="lbltotalTasks" runat="server"></asp:Label>
                |
           <label>Results:</label><asp:Label ID="lblResultCount" runat="server"></asp:Label>
                <div class="pull-right">




                    <asp:CheckBox ID="cbxRefreshHsData" runat="server" Checked="true" Text="Refresh" Visible="false" />
                    <uc1:sm_tooltip runat="server" ID="smtt" CssClass="form-control" Visible="false" />
                </div>

            </div>
        </div>
        <hr />
        <div class="form-inline">
            <label>Start </label>
            <uc1:datetime_flatpicker runat="server" ID="dtpSince" />

            <label>End </label>
            <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />

            <label>Type </label>
            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control">
                <asp:ListItem Text="All" Selected="true" Value="all"></asp:ListItem>
                <asp:ListItem Text="Emails" Value="email"></asp:ListItem>
                <asp:ListItem Text="Calls" Value="call"></asp:ListItem>
                <asp:ListItem Text="Conversations" Value="conversation"></asp:ListItem>
                <asp:ListItem Text="Tasks" Value="task"></asp:ListItem>
                <asp:ListItem Text="Notes" Value="note"></asp:ListItem>
            </asp:DropDownList>
            <label>Agent(s) </label>
            <uc1:RzAgentPicker runat="server" ID="rzap" />
            <br />
            <label>Sort </label>
            <asp:DropDownList ID="ddlChartSort" runat="server" CssClass="form-control">
                <asp:ListItem Text="Calls" Value="calls" Selected="true"></asp:ListItem>
                <asp:ListItem Text="Owner" Value="owner"></asp:ListItem>
                <asp:ListItem Text="Duration" Value="duration"></asp:ListItem>
                <asp:ListItem Text="Conversations" Value="conversation"></asp:ListItem>
                <asp:ListItem Text="Emails" Value="emails"></asp:ListItem>
                <asp:ListItem Text="Tasks" Value="tasks"></asp:ListItem>
                <asp:ListItem Text="Notes" Value="notes"></asp:ListItem>
            </asp:DropDownList>
            <asp:LinkButton ID="lbSearch" runat="server" OnClick="lbSearch_Click" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()">
                             <span class="fa fa-search" style="font-size:18px;"></span>
            </asp:LinkButton>
            <asp:LinkButton ID="lbRefreshRecent" runat="server" OnClick="lbRefreshRecent_Click" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress()" Visible="false">
                             <span class="fa fa-sync" style="font-size:18px;"></span>
            </asp:LinkButton>


            <div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                    <asp:LinkButton ID="lbPopulateDB" runat="server" OnClick="lbPopulateDB_Click" CssClass="btn btn-smc btn-smc-danger" OnClientClick="ShowUpdateProgress();" Visible="false">Populate</asp:LinkButton>
                    <asp:LinkButton ID="lbRefreshData" runat="server" OnClick="lbRefreshData_Click" CssClass="btn btn-smc btn-smc-success" OnClientClick="ShowUpdateProgress();" Visible="false">Refresh</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <div class="card">
            <uc1:sm_chartjs runat="server" ID="chtAllEngagement" />
        </div>

        <div class="card">
            <uc1:sm_chartjs runat="server" ID="chtPhoneEngagement" />
        </div>

        <div class="card">
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-sm-6"><a href="#divSummary" onclick="toggleElement('divSummary'); return false;" class="btn-smc btn-smc-primary btn-block">Summary ...</a></div>
                <div class="col-sm-6"><a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a></div>
            </div>
            <div id="divSummary" style="display: none; margin-bottom: 10px;">
                <uc1:sm_datatable runat="server" ID="smdt_sum" />
            </div>
            <div id="divDetails" style="display: none;">
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>

    </div>
    


</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

