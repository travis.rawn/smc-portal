﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_targeted_stock_buy_report : System.Web.UI.Page
{

    List<orddet_line> allLines = new List<orddet_line>();
    List<orddet_line> purchaseLines = new List<orddet_line>();
    List<orddet_line> soldLines = new List<orddet_line>();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Load Data
        LoadData();
    }

    private void LoadData()
    {
        //1st identify all PO's that were created with no orderid_sales and by Joe, Tabitha or Phil.
        //2nd get Inventory unique_ids of these parts
        //3rd identify all sales lines (ones that had a orderid_sales, this the inverse of the above)
        DateTime start = new DateTime(2020, 10, 01);
        List<string> listBuyerNames = new List<string>() { "Joe Mar", "Phil Scott", "Tabitha Kreider", "" };


        //where date_created >= '10-1-2020'
        //and buyer_name in ('Joe Mar', 'Tabitha Kreider', 'Phil Scott', '')
        //and LEN(isnull(orderid_sales, '')) <= 0
        //and LEN(isnull(fullpartnumber, '')) > 0
        //and status != 'void'
        //and quantity > 10       
        using (RzDataContext rdc = new RzDataContext())
            allLines = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= start && (w.fullpartnumber ?? "").Length > 0 && w.status.ToLower() != "void" && w.quantity > 10 && listBuyerNames.Contains(w.buyer_name) && (w.orderid_purchase ?? "").Length > 0).ToList();

        
       
        purchaseLines = allLines.Where(w => (w.orderid_sales ?? "").Length <= 0).ToList();
        List<string> poInvetoryIDs = purchaseLines.Where(w =>(w.inventory_link_uid ?? "").Length > 0).Select(s => s.inventory_link_uid).Distinct().ToList();
        using (RzDataContext rdc = new RzDataContext())
            soldLines = rdc.orddet_lines.Where(w => poInvetoryIDs.Contains(w.inventory_link_uid)).ToList();

        //smdtAll.dataSource = allLines.Select(s => new
        //{
        //    Part = s.fullpartnumber.Trim().ToUpper(),
        //    MFG = s.manufacturer.Trim().ToUpper(),
        //    Qty = s.quantity,
        //    Vendor = s.vendor_name, 
        //    Buyer = s.buyer_name


        //});        
        //smdtAll.loadGrid();
        //lblAllLinesValue.Text = allLines.Sum(s => s.unit_cost * s.quantity).Value.ToString("C");
        int totalLines = allLines.Count();
        double stockSalesPrice = soldLines.Sum(s => s.total_price).Value;
        double stockPurchaseCost = purchaseLines.Sum(s => s.total_cost).Value;

        lblAllLinesValue.Text = "Total Lines: " + totalLines; 
        lblStockSaleValue.Text = stockSalesPrice.ToString("C");
        lblStockPurchaseValue.Text = stockPurchaseCost.ToString("C");
        lblStockProfit.Text = (stockSalesPrice - stockPurchaseCost).ToString("C");

        smdtSales.dataSource = soldLines.Select(s => new
        {
            Part = s.fullpartnumber.Trim().ToUpper(),
            MFG = s.manufacturer.Trim().ToUpper(),
            Qty = s.quantity,
            Price = s.total_price,
            Sale = s.ordernumber_sales,
            Customer = s.customer_name,
            Vendor = s.vendor_name,
            Buyer = s.buyer_name


        });
        smdtSales.loadGrid();
      

        smdtPurchases.dataSource = purchaseLines.Select(s => new
        {
            Part = s.fullpartnumber.Trim().ToUpper(),
            MFG = s.manufacturer.Trim().ToUpper(),
            Qty = s.quantity,
            Cost = s.total_cost,
            PO = s.ordernumber_purchase,           
            Vendor = s.vendor_name,
            Buyer = s.buyer_name


        });
        smdtPurchases.loadGrid();
       

        

    }
}