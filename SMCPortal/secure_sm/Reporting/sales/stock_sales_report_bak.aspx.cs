﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_stock_sales_report : System.Web.UI.Page
{
    [Serializable]
    public class stock_report_object
    {
        public string partNumber { get; set; }
        public string mfg { get; set; }
        public string status { get; set; }
        public string partrecordID { get; set; }
        public string shippedStockID { get; set; }
        public string orddet_lineID { get; set; }
        public double unit_price { get; set; }
        public double total_price { get; set; }
        public double unit_cost { get; set; }
        public double total_cost { get; set; }
        public long qty { get; set; }
        public double gp { get; set; }
        public string customerName { get; set; }
        public string vendorName { get; set; }
        public string poNumber { get; set; }
        public string poID { get; set; }
        public string soNumber { get; set; }
        public string soID { get; set; }
        public string soDate { get; set; }
        public string salesAgent { get; set; }
        public string purchaseAgent { get; set; }
        public double quoteCount { get; set; }
        public double lastQuotedUnitPrice { get; set; }
    }


    public List<stock_report_object> sroAllStock { get { return ViewState["sroAllStock"] as List<stock_report_object>; } set { ViewState["sroAllStock"] = value; } }
    public List<stock_report_object> sroSoldStock { get { return ViewState["sroSoldStock"] as List<stock_report_object>; } set { ViewState["sroSoldStock"] = value; } }
    public List<stock_report_object> sroUnsoldStock { get { return ViewState["sroUnsoldStock"] as List<stock_report_object>; } set { ViewState["sroUnsoldStock"] = value; } }

    List<ordhed_purchase> stockPOList;
    List<string> stockPOIDList;
    List<orddet_line> allPOStockLinesList;
    List<string> allStockPartNumbersList;
    List<orddet_line> soldStockLinesList;
    List<partrecord> unSoldPartrecordsList;
    //List<stock_report_object> sroList;
    List<n_user> userList;
    SM_Tools tools = new SM_Tools();
    TimeSpan loadTime = new TimeSpan();

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            DateTime ReportStart = DateTime.Now;


            LoadDates();
            using (RzDataContext rdc = new RzDataContext())
            {

                stockPOList = rdc.ordhed_purchases.Where(w => w.is_stock == true && w.isvoid.Value != true && (w.date_created.Value >= dtpStart.SelectedDate && w.date_created.Value <= dtpEnd.SelectedDate)).ToList();
                LoadUsers(rdc);
                if (rzap.SelectedUserID != "choose")
                    stockPOList = stockPOList.Where(w => w.base_mc_user_uid == rzap.SelectedUserID).ToList();
                stockPOIDList = stockPOList.Select(s => s.unique_id).Distinct().ToList();
                LoadAllPoStock(rdc);
                LoadSoldPoStock(rdc);
                LoadUnSoldPoStock(rdc);
            }


            LoadReport();

            DateTime ReportEnd = DateTime.Now;
            loadTime = ReportEnd - ReportStart;
            tools.HandleResult("Success", "Processing time: " + loadTime.Minutes + ":" + loadTime.Seconds);
        }
        catch (Exception ex)
        {
            string message = "Processing time: " + loadTime.Minutes + ":" + loadTime.Seconds + "<br /><br />";
            message += ex.Message;
            tools.HandleResult("fail", message);
        }


    }

    private void LoadUsers(RzDataContext rdc)
    {
        if (Page.IsPostBack)
            return;
        List<string> userNameList = stockPOList.Select(s => s.agentname).Distinct().ToList();
        userList = rdc.n_users.Where(w => userNameList.Contains(w.name)).ToList();
        rzap.DefaultSelectionName = "Joe Mar";
        rzap.LoadPicker(userList);
    }

    private void LoadDates()
    {
        if (Page.IsPostBack)
            return;
        dtpStart.SelectedDate = new DateTime(2020, 01, 01);
        dtpEnd.SelectedDate = DateTime.Today;
    }

    private void LoadAllPoStock(RzDataContext rdc)
    {
        //Load All lines related to Stock POs
        allPOStockLinesList = rdc.orddet_lines.Where(w => stockPOIDList.Contains(w.orderid_purchase)).ToList();
        //Load partnumbers list
        allStockPartNumbersList = allPOStockLinesList.Where(w => w.fullpartnumber.Length > 0).Select(s => s.fullpartnumber.Trim().ToUpper()).Distinct().ToList();
        //Create the all stock list.
        sroAllStock = allPOStockLinesList.Select(s => new stock_report_object { partNumber = s.fullpartnumber, qty = (long)s.quantity, mfg = s.manufacturer, vendorName = s.vendor_name, poNumber = s.ordernumber_purchase, unit_cost = s.unit_cost.Value, total_cost = s.total_cost.Value, purchaseAgent = s.buyer_name }).ToList();
        //Format for Grid
        smdtAllPoStock.dataSource = sroAllStock.Select(s => new { Part = s.partNumber, QTY = s.qty, MFG = s.mfg, Vendor = s.vendorName, PO = s.poNumber, Buyer = s.purchaseAgent, UnitCost = s.unit_cost.ToString("C") });

        //Add 
        smdtAllPoStock.pageLength = 50;
        smdtAllPoStock.loadGrid();
    }

    private void LoadSoldPoStock(RzDataContext rdc)
    {
        //Lines labeled as stock, created within date range, but the poID is NOT the sto PO.  Also using a partnumber lookup to ensure these relate to stock parts, not partnumbers that don't exist in the stock list.  

        //Load Sold orddet_lines
        soldStockLinesList = rdc.orddet_lines.Where(w => w.date_created.Value >= dtpStart.SelectedDate & allStockPartNumbersList.Contains(w.fullpartnumber.Trim().ToUpper()) && w.stocktype.ToLower() == "stock" && w.orderid_sales.Length > 0).ToList();
        //Create the sro Sold List
        sroSoldStock = soldStockLinesList.Select(s => new stock_report_object { partNumber = s.fullpartnumber, qty = (long)s.quantity, mfg = s.manufacturer, status = s.status, vendorName = s.vendor_name, poNumber = s.ordernumber_purchase, customerName = s.customer_name, soNumber = s.ordernumber_sales, unit_price = s.unit_price.Value, total_price = s.total_price.Value, unit_cost = s.unit_cost.Value, total_cost = s.total_cost.Value, gp = s.gross_profit.Value, purchaseAgent = s.buyer_name }).ToList();


        //Format sro object for Grid
        smdtSoldStock.dataSource = sroSoldStock.Select(s => new { Part = s.partNumber, QTY = s.qty, MFG = s.mfg, Customer = s.customerName, SO = s.soNumber, Status = s.status, Vendor = s.vendorName, UnitPrice = s.unit_price.ToString("C"), UnitCost = s.unit_cost.ToString("C") });
        smdtSoldStock.pageLength = 50;
        smdtSoldStock.loadGrid();
    }



    private void LoadUnSoldPoStock(RzDataContext rdc)
    {
        //Load Un sold Partrecords
        unSoldPartrecordsList = rdc.partrecords.Where(w => stockPOIDList.Contains(w.buy_purchase_id)).ToList();
        //Create the sro Unsold List
        sroUnsoldStock = unSoldPartrecordsList.Select(s => new stock_report_object { partNumber = s.fullpartnumber, qty = (long)s.quantity, mfg = s.manufacturer, vendorName = s.companyname, poNumber = s.buy_purchase_ordernumber, unit_cost = s.cost.Value, purchaseAgent = s.agentname }).ToList();

        //Apply Extra Logic
        sroUnsoldStock = sroApplyExtraLogic(sroUnsoldStock);


        //Format sro object for Grid
        smdtUnsoldStock.dataSource = sroUnsoldStock.Select(s => new { Part = s.partNumber, MFG = s.mfg, Vendor = s.vendorName, PO = s.poNumber, QTY = s.qty, UnitCost = s.unit_cost.ToString("C"), LastQuotePrice = s.lastQuotedUnitPrice, TotQuotes = s.quoteCount });
        smdtUnsoldStock.pageLength = 50;
        smdtUnsoldStock.loadGrid();
    }

    private List<stock_report_object> sroApplyExtraLogic(List<stock_report_object> sroList)
    {
        //Instantiate ret as a duplicate copy of source list.
        List<stock_report_object> ret = sroList.ToList();
        List<string> uniqueSroParts = new List<string>();
        uniqueSroParts = ret.Select(s => s.partNumber.Trim().ToUpper()).Distinct().ToList();
        //Search t hte lsit of lines for any matchine quotes.
        //At time of coding, there were 73K Quotes

        List<orddet_quote> matchedQuotesList = new List<orddet_quote>();
        using (RzDataContext rdc = new RzDataContext())
            matchedQuotesList = rdc.orddet_quotes.Where(w => (w.fullpartnumber ?? "").Trim().ToUpper().Length > 3 && uniqueSroParts.Contains(w.fullpartnumber.Trim().ToUpper())).ToList();
        ////Get matched parts in memory for iteration
        //List<string> matchedSroPartsList = quotesList.Select(s => s.fullpartnumber.Trim().ToUpper()).Distinct().ToList();
        int i = 0;
        foreach (stock_report_object sro in ret)
        {
            i++;
            sro.lastQuotedUnitPrice = 0;
            sro.quoteCount = 0;
            List<orddet_quote> matchedSroList = new List<orddet_quote>();
            matchedSroList = matchedQuotesList.Where(s => s.fullpartnumber.Trim().ToUpper() == sro.partNumber.Trim().ToUpper()).ToList();
            if (matchedSroList.Count > 0)
            {
                orddet_quote oldest_quote = matchedSroList.OrderBy(oo => oo.date_created.Value).FirstOrDefault();
                orddet_quote newest_quote = matchedSroList.OrderBy(oo => oo.date_created.Value).LastOrDefault();
                double max_quote_value = matchedSroList.OrderByDescending(o => (o.unitprice ?? 0)).First().unitprice ?? 0;
                double min_quote_value = matchedSroList.OrderBy(o => (o.unitprice ?? 0)).First().unitprice ?? 0;
                double totalQuotes = matchedSroList.Count();


                //set SRO Values
                sro.lastQuotedUnitPrice = newest_quote.unitprice ?? 0;
                sro.quoteCount = totalQuotes;


            }


        }


        return ret;
    }



    private void LoadReport()
    {

        double total_stock_cost = sroAllStock.Sum(s => s.unit_cost);
        double sold_total_price = sroSoldStock.Sum(s => s.unit_price);
        double sold_total_cost = sroSoldStock.Sum(s => s.unit_cost);
        double gross_profit = sroSoldStock.Sum(s => s.gp);
        //   (gross_profit / total_price) / total_price == X%
        double gross_margin = ((gross_profit / sold_total_price) / sold_total_price) * 100;
        //Overall
        lblTotalSales.Text = "Total Sales: " + sold_total_price.ToString("C");
        lblTotalPOCost.Text = "Total Cost: " + total_stock_cost.ToString("C");
        lblTotalGP.Text = "Total GP: " + gross_profit.ToString("C");

        //All Stock Lines Section       
        lblAllPOLineCount.Text = stockPOList.Count().ToString();
        lblAllPOCost.Text = stockPOList.Sum(s => s.ordertotal.Value).ToString("C");

        //Stock Sales Section
        lblSoldTotalSales.Text = sold_total_price.ToString("C");
        lblSoldTotalCost.Text = sold_total_cost.ToString("C");
        lblSoldTotalGp.Text = gross_profit.ToString("C");
        lblSoldGpMargin.Text = gross_margin.ToString("P");

        //Unsold Stock Section
        lblUnsoldLineCount.Text = unSoldPartrecordsList.Count().ToString();
        lblUnsoldCost.Text = unSoldPartrecordsList.Sum(s => s.cost.Value).ToString("C");
    }

    protected void exportStockPOLines_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv((List<stock_report_object>)sroAllStock, "All_Stock_Purchase_Lines");
    }

    protected void lbExportSoldStock_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv((List<stock_report_object>)sroSoldStock, "Sold_Stock_Purchase_Lines");
    }

    protected void lbExportUnSoldStock_Click(object sender, EventArgs e)
    {
        tools.ExportListToCsv((List<stock_report_object>)sroUnsoldStock, "Unsold_Stock_Lines");
    }
}