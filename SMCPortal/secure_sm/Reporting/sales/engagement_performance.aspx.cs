﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_engagement_performance : System.Web.UI.Page
{

    DateTime StartDate;
    DateTime EndDate;
    List<hubspot_engagement> callList;
    List<orddet_line> salesList;
    List<n_user> agentList;

    DataTable linqPadDataTable;
    public class EngagementPerformanceObject
    {
        public string Agent { get; set; }
        public double CallTotal { get; set; }
        public double CallsToday { get; set; }
        public double ConversationTotal { get; set; }
        public double CallsPerDay { get; set; }
        public double ConversationPerDay { get; set; }
        public double ConnectRate { get; set; }
        //public double CallsUntilConversation { get; set; }
        public double SalesTotal { get; set; }
        public double MonthsCount { get; set; }
        public double DaysCount { get; set; }
        public double MonthyGoal { get; set; }
        public double GoalPace { get; set; }
        public double GoalRemaining { get; set; }
        public double DollarsPerConversation { get; set; }
        public double ConversationsUntilGoal { get; set; }
        
    }

    public List<EngagementPerformanceObject> eoList;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();
        //LoadData();
        //LoadGrids();
        linqPadDataTable = LoadLinqPadDataTable();
        smdt_summary.dataSource = linqPadDataTable;
        smdt_summary.loadGrid();
    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Today.AddMonths(-1).Date;
            dtpEnd.SelectedDate = DateTime.Now.Date;
        }
        StartDate = dtpStart.SelectedDate.Value;
        EndDate = dtpEnd.SelectedDate.Value;

    }

    private void LoadGrids()
    {
        smdt_details.dataSource = null;
        if (eoList != null && eoList.Count > 0)
        {
            BindDetailGrid();
            BindSummaryGrid();
        }

        smdt_details.loadGrid();
        smdt_summary.loadGrid();
    }

    private void BindSummaryGrid()
    {
        smdt_summary.pageLength = 25;
        //Handle sorting for columns
        Tuple<string, string> sort1 = new Tuple<string, string>("'type' : 'currency', 'targets' ", "1");
        Tuple<string, string> sort2 = new Tuple<string, string>("'type' : 'currency', 'targets' ", "2");
        Tuple<string, string> sort4 = new Tuple<string, string>("'type' : 'currency', 'targets' ", "4");
        Tuple<string, string> sort6 = new Tuple<string, string>("'type' : 'currency', 'targets' ", "6");
        smdt_summary.jqColDefs.Add(sort1);
        smdt_summary.jqColDefs.Add(sort2);
        smdt_summary.jqColDefs.Add(sort4);
        smdt_summary.jqColDefs.Add(sort6);


        smdt_summary.dataSource = eoList.Select(s => new
        {
            s.Agent,
            SalesGoal = s.MonthyGoal.ToString("C"),
            SalesTotal = s.SalesTotal.ToString("C"),
            ConnectRate = s.ConnectRate, // + " (Today: " + s.CallsToday + ")",
            ConversationValue = s.DollarsPerConversation.ToString("C"),
            AvgDailyonvos = s.ConversationPerDay,
            AddConversations = s.ConversationsUntilGoal,
            Remaining = s.GoalRemaining.ToString("C")

        });
    }

    private void BindDetailGrid()
    {
        smdt_details.dataSource = eoList.OrderBy(o => o.Agent);
        smdt_details.pageLength = 25;
    }

    private void LoadData()
    {

        int totalMonths = 12 * (StartDate.Year - EndDate.Year) + StartDate.Month - EndDate.Month;
        if (totalMonths < 0)
            totalMonths = totalMonths * -1;


        int totalWeekends = totalMonths * 8;
        double totalDays = (EndDate - StartDate).TotalDays;

        totalDays -= totalWeekends;
        List<string> badStatus = new List<string>() { "scrapped", "void" };




        using (RzDataContext rdc = new RzDataContext())
        {
            callList = rdc.hubspot_engagements.Where(w => w.date_created.Value.Date >= StartDate && w.date_created.Value.Date <= EndDate && w.type == "call").ToList();
            salesList = rdc.orddet_lines.Where(w => w.orderdate_sales.Value.Date >= StartDate && w.orderdate_sales.Value.Date <= EndDate && w.vendor_name != "Source TBD" && !badStatus.Contains(w.status.ToLower())).ToList();
            List<string> agentNames = new List<string>() { "", "Tabitha Kreider", "Roberto Hernandez", "Larry McDonald" };

            agentList = rdc.n_users.Where(w => !agentNames.Contains(w.name) && w.main_n_team_uid == "31ce26c99a1644219f7df2ab99cbd63b" && (w.is_inactive ?? false) != true && w.name != "house").ToList();


        }
        if (agentList == null)
            return;

        //Instantiate the eoList
        eoList = new List<EngagementPerformanceObject>();


        foreach (n_user u in agentList)
        {
            int agentMonths = totalMonths;
            double agentDays = totalDays;
            string name = u.name;
            DateTime hireDate = u.date_created.Value.Date;
            if (hireDate > StartDate)
            {
                int deltaMonths = ((hireDate.Year - StartDate.Year) * 12) + hireDate.Month - StartDate.Month;
                int deltaDays = ((hireDate.Year - StartDate.Year) * 12) + hireDate.Month - StartDate.Month;
                agentMonths = totalMonths - deltaMonths;

                agentDays = (hireDate - StartDate).TotalDays;


            }

            List<hubspot_engagement> callsForAgent = callList.Where(w => w.base_mc_user_uid == u.unique_id).ToList();
            List<orddet_line> salesForAgent = salesList.Where(w => w.seller_uid == u.unique_id).ToList();

            //Base metrics
            double totalCalls = callsForAgent.Count();
            double todaysCalls = callsForAgent.Where(w => w.date_created.Value.Date == DateTime.Today.Date).Count();
            double callsPerDay = totalCalls / agentDays;
            int totalConversations = callsForAgent.Where(w => w.call_disposition == "Connected").Count();
            double converstationsPerDay = totalConversations / agentDays;
            double connectRate = totalCalls / totalConversations;
            double totalSaleDollars = salesForAgent.Sum(s => s.gross_profit ?? 0);
            double conversationValue = totalSaleDollars / totalConversations;
            double monthlyGoal = u.monthly_booking_goal ?? 0;
            double currentGoalPace = monthlyGoal * agentMonths;

            //Average calls until next conversation.  Compare this number to the current number of calls.
            double averageCallsPerConversation = totalCalls / totalConversations;
            double callsUntilConversation = 0;
            double remainder = averageCallsPerConversation % todaysCalls;
            if (remainder > 0)
                callsUntilConversation = Math.Round(todaysCalls - remainder, 2);

            //Converstaions until Goal Met
            double remainingToGoal = currentGoalPace - totalSaleDollars;
            double conversationsUntilGoal = 0;
            if (remainingToGoal > 0)
            {
                conversationsUntilGoal = (remainingToGoal / conversationValue) / totalDays;
            }







            EngagementPerformanceObject eo = new EngagementPerformanceObject();
            eo.Agent = u.name;
            eo.CallTotal = totalCalls;
            eo.CallsToday = todaysCalls;
            eo.ConversationTotal = totalConversations;
            eo.CallsPerDay = Math.Round((double)callsPerDay, 2);
            eo.ConversationPerDay = Math.Round(converstationsPerDay, 2);
            eo.ConnectRate = Math.Round(connectRate, 2);
            //eo.CallsUntilConversation = Math.Round(callsUntilConversation, 0);
            eo.DollarsPerConversation = Math.Round(conversationValue, 2);
            eo.SalesTotal = Math.Round(totalSaleDollars, 2);
            eo.ConversationsUntilGoal = Math.Ceiling(Math.Round(conversationsUntilGoal, 2));
            eo.MonthyGoal = monthlyGoal;
            eo.MonthsCount = agentMonths;
            eo.DaysCount = agentDays;
            eo.GoalPace = currentGoalPace;
            eo.GoalRemaining = remainingToGoal;
            eoList.Add(eo);














            //DataRow row = dt.NewRow();
            //row["Agent"] = u.name;
            //row["CallTotal"] = totalCalls;
            //row["ConversationTotal"] = totalConversations;
            //row["CallsPerDay"] = Math.Round((double)callsPerDay, 2);
            //row["ConversationPerDay"] = Math.Round(converstationsPerDay, 2);
            //row["ConnectRate"] = Math.Round(connectRate, 2);
            //row["DollarsPerConversation"] = Math.Round(dollarsPerConversation, 2);
            //row["SalesTotal"] = Math.Round(totalSaleDollars, 2);
            //row["ConversationsUntilGoal"] = Math.Round(conversationsUntilGoal, 2);
            //row["MonthyGoal"] = monthlyGoal;
            //row["MonthsCount"] = agentMonths;
            //row["DaysCount"] = agentDays;
            //row["GoalPace"] = currentGoalPace;
            //row["GoalRemaining"] = remainingToGoal;
            //dt.Rows.Add(row);


        }      


    }


    DataTable LoadLinqPadDataTable()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            //DataTable dt = new DataTable();

            //DateTime startDate = new DateTime(2020, 08, 12);
            //DateTime endDate = DateTime.Today.Date;

            int CurrentQuarter = 1;
            if (StartDate.Month >= 4 && StartDate.Month <= 6)
                CurrentQuarter = 2;
            else if (StartDate.Month >= 7 && StartDate.Month <= 9)
                CurrentQuarter = 3;
            else if (StartDate.Month >= 10 && StartDate.Month <= 12)
                CurrentQuarter = 4;
            else
                CurrentQuarter = 1;
            DateTime CurrentQuarterStartDate = new DateTime(2020, 01, 01);
            DateTime CurrentQuarterEndDate = new DateTime(2020, 12, 31);
            if (CurrentQuarter == 1)
            {
                CurrentQuarterStartDate = new DateTime(2020, 01, 01);
                CurrentQuarterEndDate = new DateTime(2020, 3, 30);
            }

            if (CurrentQuarter == 2)
            {
                CurrentQuarterStartDate = new DateTime(2020, 04, 01);
                CurrentQuarterEndDate = new DateTime(2020, 6, 30);
            }

            if (CurrentQuarter == 3)
            {
                CurrentQuarterStartDate = new DateTime(2020, 07, 01);
                CurrentQuarterEndDate = new DateTime(2020, 09, 30);
            }

            if (CurrentQuarter == 4)
            {
                CurrentQuarterStartDate = new DateTime(2020, 10, 01);
                CurrentQuarterEndDate = new DateTime(2020, 12, 31);
            }


            //Based on the Start Date, determine which QuaRTER
            double CurrentQuarterTotalDays = (CurrentQuarterEndDate.Date - CurrentQuarterStartDate.Date).TotalDays;
            double CurrentQuarterDaysToGoal = (CurrentQuarterEndDate.Date - DateTime.Today.Date).TotalDays;
            double DaysSinceStartOfCurrentQuarter = (DateTime.Today.Date - CurrentQuarterStartDate).TotalDays;
            //double ElapsedDayCount = (endDate.Date - startDate.Date).TotalDays;

            double CompletedWeeksSinceStartOfQuarter = Math.Round(Math.Floor(DaysSinceStartOfCurrentQuarter / 7), 0);
            double ElapsedWeekendDays = CompletedWeeksSinceStartOfQuarter * 2;
            double TotalWeeksInQuarter = (CurrentQuarterTotalDays / 7);
            double TotalWeekendDayCount = (CurrentQuarterTotalDays / TotalWeeksInQuarter) * 2;
            double TotalBusinessDayCount = CurrentQuarterTotalDays - TotalWeekendDayCount;
            double ElapsedBusinessDayCount = Math.Round(DaysSinceStartOfCurrentQuarter - ElapsedWeekendDays, 0);
            double BusinessDaysReminingInQuarter = (TotalBusinessDayCount - ElapsedBusinessDayCount);









            double PreviousQuarter = CurrentQuarter - 1;
            DateTime PreviousQuarterStartDate = new DateTime(2020, 04, 01);
            DateTime PreviousQuarterEndDate = new DateTime(2020, 07, 31);
            double previousDayCount = (PreviousQuarterEndDate.Date - PreviousQuarterStartDate.Date).TotalDays;
            double previousWeekCount = Math.Round(Math.Floor(previousDayCount / 7), 0);
            double PreviousBusinessDayCount = Math.Round(previousDayCount - (previousWeekCount * 2), 0);




            DataTable dtCurrent = new DataTable();
            dtCurrent.Columns.Add("Agent", typeof(string));
            dtCurrent.Columns.Add("ActualBusinessDays", typeof(string));
            dtCurrent.Columns.Add("DaysToGoal", typeof(string));


            dtCurrent.Columns.Add("ActualSales", typeof(string));

            dtCurrent.Columns.Add("SalesGoal", typeof(string));
            dtCurrent.Columns.Add("SalesNeededPerDayToGoal", typeof(string));

            dtCurrent.Columns.Add("CallsPerDayNeeded", typeof(string));
            dtCurrent.Columns.Add("ConvosPerDay", typeof(string));
            dtCurrent.Columns.Add("CallValue", typeof(string));
            dtCurrent.Columns.Add("ConvoValue", typeof(string));

            dtCurrent.Columns.Add("SalesToGoal", typeof(string));
            dtCurrent.Columns.Add("CallsToGoal", typeof(string));
            dtCurrent.Columns.Add("ConvosToGoal", typeof(string));
            DataTable dtPrevious = dtCurrent.Clone();
            DataTable dtCombined = dtCurrent.Clone();
            DataTable dtDaily = dtCurrent.Clone();
            List<string> badStatus = new List<string>() { "scrapped", "void" };



            //modini = "e9197ee25bf84dbfae1b13f38ec582c2"
            List<hubspot_engagement> CurrentEngagements = rdc.hubspot_engagements.Where(w => w.date_created.Value.Date >= StartDate && w.date_created.Value.Date <= EndDate && w.type == "call").ToList();
            List<orddet_line> CurrentSales = rdc.orddet_lines.Where(w => w.orderdate_sales.Value.Date >= StartDate && w.orderdate_sales.Value.Date <= EndDate && w.vendor_name != "Source TBD" && !badStatus.Contains(w.status.ToLower())).ToList();
            List<hubspot_engagement> PreviousQuarterEngagements = rdc.hubspot_engagements.Where(w => w.date_created.Value.Date >= PreviousQuarterStartDate && w.date_created.Value.Date <= PreviousQuarterEndDate && w.type == "call").ToList();
            List<orddet_line> PrevioustQuarterSales = rdc.orddet_lines.Where(w => w.orderdate_sales.Value.Date >= PreviousQuarterStartDate && w.orderdate_sales.Value.Date <= PreviousQuarterEndDate && w.vendor_name != "Source TBD" && !badStatus.Contains(w.status.ToLower())).ToList();

            List<n_user> agentList = rdc.n_users.Where(w => w.main_n_team_uid == "31ce26c99a1644219f7df2ab99cbd63b" && (w.is_inactive ?? false) != true && w.name != "house" && w.monthly_booking_goal > 0).ToList();

            foreach (n_user u in agentList)
            {
                string name = u.name;
                double quarterlyGoal = (u.monthly_booking_goal * 3) ?? 0;

                List<hubspot_engagement> CurrentAgentEngagments = CurrentEngagements.Where(w => w.base_mc_user_uid == u.unique_id).ToList();
                List<hubspot_engagement> PreviousAgentEngagments = PreviousQuarterEngagements.Where(w => w.base_mc_user_uid == u.unique_id).ToList();
                List<orddet_line> CurrentAgentSales = CurrentSales.Where(w => w.seller_uid == u.unique_id).ToList();
                List<orddet_line> PreviousAgentSales = PrevioustQuarterSales.Where(w => w.seller_uid == u.unique_id).ToList();
                //Hire Date, use this to trim the available business days in case new user.  Only need averages for the days actually working.
                DateTime hireDate = u.date_created.Value.Date;
                double PreviousAgentBusinessDayCount = PreviousBusinessDayCount;
                if (hireDate > PreviousQuarterStartDate)
                {
                    double previousAgentDayCount = (PreviousQuarterEndDate.Date - hireDate).TotalDays;
                    double previousAgentWeekCount = Math.Round(Math.Floor(previousDayCount / 7), 0);
                    PreviousAgentBusinessDayCount = Math.Round(previousAgentDayCount - (previousAgentWeekCount * 2), 0);
                }


                //Current
                double cCallsPerDay = CurrentAgentEngagments.Where(w => w.type == "CALL").Count() / ElapsedBusinessDayCount;
                double cConvosPerDay = CurrentAgentEngagments.Where(w => w.call_disposition == "Connected").Count() / ElapsedBusinessDayCount;
                double cBooked = CurrentAgentSales.Sum(s => s.gross_profit) ?? 0;
                double remainingSalesUntilGoal = (quarterlyGoal - cBooked);
                double SalesNeededPerDayToGoal = (quarterlyGoal - cBooked) / BusinessDaysReminingInQuarter;

                //Previous
                double pTotalCalls = PreviousAgentEngagments.Where(w => w.type == "CALL").Count();
                double pCallsPerDay = pTotalCalls / PreviousAgentBusinessDayCount;
                double pTotalConvos = PreviousAgentEngagments.Where(w => w.call_disposition == "Connected").Count();
                double pConvosPerDay = pTotalConvos / PreviousAgentBusinessDayCount;
                double pBooked = PreviousAgentSales.Sum(s => s.gross_profit) ?? 0;
                double pSalesPerDay = pBooked / PreviousAgentBusinessDayCount;
                double previousQuarterDollarsPerConvo = pBooked / pTotalConvos;
                double previousQuarterDollarsPerCall = pBooked / pTotalCalls;
                double previousCallsNeededToGoal = remainingSalesUntilGoal / previousQuarterDollarsPerCall;
                double previousConvosNeededToGoal = remainingSalesUntilGoal / previousQuarterDollarsPerConvo;
                double previousSalesPerDay = pSalesPerDay;


                //Daily
                double todaysCalls = CurrentAgentEngagments.Where(w => w.type == "CALL" && w.date_created.Value.Date == DateTime.Today.Date).Count();
                double todaysConvos = CurrentAgentEngagments.Where(w => w.type == "CALL" & w.call_disposition == "Connected" && w.date_created.Value.Date == DateTime.Today.Date).Count();
                double todayBooked = CurrentAgentSales.Where(w => w.orderdate_sales.Value.Date == DateTime.Today.Date).Sum(s => s.gross_profit) ?? 0;
                double currentCallsNeededToGoal = (remainingSalesUntilGoal / previousQuarterDollarsPerCall);
                double currentConvosNeededToGoal = (remainingSalesUntilGoal / previousQuarterDollarsPerConvo);
                double dailyCallsNeededToGoal = currentCallsNeededToGoal / BusinessDaysReminingInQuarter;
                double dailyConvosNeededToGoal = currentConvosNeededToGoal / BusinessDaysReminingInQuarter;



                //Previous DataTable
                DataRow pRow = dtPrevious.NewRow();
                pRow["Agent"] = u.name;
                pRow["ActualBusinessDays"] = PreviousAgentBusinessDayCount;
                pRow["CallsPerDayNeeded"] = Math.Round(pCallsPerDay, 0);
                pRow["ConvosPerDay"] = Math.Round(pConvosPerDay, 0);
                pRow["ActualSales"] = pBooked.ToString("C");

                pRow["SalesToGoal"] = "N/A";
                //For previous quarter, set this as average sales per day
                pRow["DaysToGoal"] = "0";
                pRow["SalesNeededPerDayToGoal"] = (pSalesPerDay).ToString("C");
                pRow["SalesGoal"] = quarterlyGoal.ToString("C");
                pRow["CallValue"] = previousQuarterDollarsPerCall.ToString("C");
                pRow["ConvoValue"] = previousQuarterDollarsPerConvo.ToString("C");
                pRow["CallsToGoal"] = Math.Round(previousCallsNeededToGoal / PreviousBusinessDayCount, 0).ToString();
                pRow["ConvosToGoal"] = Math.Round(previousConvosNeededToGoal / PreviousBusinessDayCount, 0).ToString();
                dtPrevious.Rows.Add(pRow);

                //Dailiy DataTable
                DataRow dailyRow = dtDaily.NewRow();
                dailyRow["Agent"] = u.name;
                dailyRow["ActualBusinessDays"] = ElapsedBusinessDayCount;
                dailyRow["CallsPerDayNeeded"] = Math.Round(todaysCalls, 0) + "  (" + Math.Round(pCallsPerDay, 0) + ")";
                dailyRow["CallsToGoal"] = Math.Round(dailyCallsNeededToGoal, 0).ToString();
                dailyRow["CallsPerDayNeeded"] = Math.Round(todaysConvos, 0) + "  (" + Math.Round(pConvosPerDay, 0) + ")";
                dailyRow["ConvosToGoal"] = Math.Round(dailyConvosNeededToGoal, 0).ToString();
                dailyRow["ActualSales"] = cBooked.ToString("C") + "  (" + pBooked.ToString("C") + ")";
                dailyRow["SalesToGoal"] = remainingSalesUntilGoal.ToString("C");
                //For previous quarter, set this as average sales per day
                dailyRow["DaysToGoal"] = BusinessDaysReminingInQuarter;
                dailyRow["SalesNeededPerDayToGoal"] = SalesNeededPerDayToGoal.ToString("C");
                dailyRow["SalesGoal"] = quarterlyGoal.ToString("C");
                dailyRow["CallValue"] = previousQuarterDollarsPerCall.ToString("C");
                dailyRow["ConvoValue"] = previousQuarterDollarsPerConvo.ToString("C");
                dtDaily.Rows.Add(dailyRow);


            }

            ////("Current: " + startDate.Date.ToShortDateString() + " - " + endDate.Date.ToShortDateString()).Dump();
            ////dtCurrent.Dump();
            //Environment.NewLine.Dump();
            //("Previous: " + PreviousQuarterStartDate.Date.ToShortDateString() + " - " + PreviousQuarterEndDate.Date.ToShortDateString()).Dump();
            //dtPrevious.Dump();

            //("Today (" + DateTime.Today.ToShortDateString() + ") compared to last Quarter: ").Dump();
            //dtDaily.Dump();


            //return dt;

            return dtDaily;
        }

    }
}