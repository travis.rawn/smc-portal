﻿<%@ Page Title="Sales Charts" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="sales_charts.aspx.cs" Inherits="secure_sm_Reports_sales_charts" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .spacer {
            margin-top: 30px;
        }

        div.labels {
            margin: 0;
            padding: 0;
            padding-bottom: 1.25em;
        }

            div.labels span {
                margin-left: 8%;
                display: block;
                padding-top: .1em;
                padding-right: .25em;
                /*text-align: right;
                float: left;*/
            }

        .dataGrid {
            margin-top: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Sales Charts</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">




    <div class="panel panel-default card">
        <div class="panel-body">
            <div class="form form-inline">
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">  
                    <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
                    <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
                    <uc1:RzAgentPicker runat="server" ID="rzap" />
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress();" />
                </asp:Panel>
            </div>
        </div>
    </div>

    <asp:Panel ID="pnlCharts" runat="server" Visible="false">

        <div class="panel panel-default card" style="display: none">
            <div class="panel-body">
                <div class="col-sm-2 spacer">
                    <h4>Booked: </h4>
                    <div class="labels">
                        <h5>
                            <label>Total:</label><asp:Label ID="lblBookedTot" runat="server"></asp:Label>
                            <label>GP:</label><asp:Label ID="lblBookedGP" runat="server"></asp:Label>
                            <label>Open:</label><asp:Label ID="lblBookedNP" runat="server"></asp:Label>
                        </h5>
                    </div>

                </div>
                <div class="col-sm-8">
                    <div class="panel panel-default card">
                        <div class="panel-body">
                            <div style="text-align: center;">
                                <h3>Book To Bill</h3>
                                <em>
                                    <asp:Label ID="lblB2BNoData" runat="server"></asp:Label></em>
                                <p style="font-size: 10px;">
                                    <em>
                                        <asp:Label ID="lblB2BDateRange" runat="server"></asp:Label></em>
                                </p>
                            </div>
                            <label>
                                Book to Bill Ratio:
                            <asp:Label ID="lblB2BRatio" runat="server"></asp:Label>
                            </label>
                            <div>
                                <canvas id="cvB2B" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 spacer">
                    <h4>Billed:</h4>
                    <div class="labels">
                        <label>Total:</label><asp:Label ID="lblBilledTot" runat="server"></asp:Label>
                        <label>GP:</label><asp:Label ID="lblBilledGP" runat="server"></asp:Label>
                        <label>NP:</label><asp:Label ID="lblBilledNP" runat="server"></asp:Label>

                    </div>


                </div>
                <a href="#fivBookToBill" onclick="toggleElement('divBookToBill'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
                <div id="divBookToBill" style="display: none;">
                    <uc1:sm_datatable runat="server" ID="smdtBookBill" class="dataGrid" />
                    <%--<asp:GridView ID="gvBookToBill" runat="server" CssClass="table table-hover table-striped gvstyling stackable tablesorter" GridLines="None"></asp:GridView>--%>
                </div>

            </div>
        </div>


        <%--Sales Breakdown--%>
        <div class="panel panel-default card">
            <div class="panel-body">
                <div style="text-align: center;">
                    <h3>Sales Breakdown</h3>
                    <p style="font-size: 10px;">
                        <em>
                            <asp:Label ID="lblSbDateRange" runat="server"></asp:Label></em>
                    </p>
                      <p style="font-size: 10px;">
                        <em>
                          <asp:Linkbutton ID="lbExport" Text="Export Details" runat="server" OnClick="lbExport_Click"></asp:Linkbutton>
                    </p>
                </div>
                <div class="col-sm-4">
                    <div class="labels">
                        <h4>Quoted: </h4>
                        <h5>Total:<asp:Label ID="lblsbQuotedTotal" runat="server"></asp:Label><br />
                            GP:<asp:Label ID="lblsbQuotedGP" runat="server"></asp:Label><br />
                            Open:<asp:Label ID="lblsbQuotedOpen" runat="server"></asp:Label>
                        </h5>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="labels">
                        <h4>Booked:</h4>
                        <h5>Total:<asp:Label ID="lblsbBookedTotal" runat="server"></asp:Label><br />
                            GP:<asp:Label ID="lblsbBookedGP" runat="server"></asp:Label><br />
                            NP:<asp:Label ID="lblsbBookedNP" runat="server"></asp:Label>
                        </h5>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="labels">
                        <h4>Invoiced:</h4>
                        <h5>Total:<asp:Label ID="lblsbInvoicedTotal" runat="server"></asp:Label><br />
                            GP:<asp:Label ID="lblsbInvoicedGP" runat="server"></asp:Label><br />
                            NP:<asp:Label ID="lblsbInvoicedNP" runat="server"></asp:Label>
                        </h5>
                    </div>
                </div>
                <a href="#gvSalesBreakdown" onclick="toggleElement('divSalesBreakdown'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
                <div id="divSalesBreakdown" style="display: none;">

                    <div class="col-sm-4">
                        <label>Quoted:</label>
                        <asp:GridView ID="gvSbQuoted" runat="server" RowStyle-HorizontalAlign="Left" CssClass="table table-hover table-striped gvstyling stackable tablesorter" GridLines="None"></asp:GridView>
                    </div>
                    <div class="col-sm-4">
                        <label>Booked:</label>
                        <asp:GridView ID="gvSbBooked" runat="server" CssClass="table table-hover table-striped gvstyling stackable tablesorter" GridLines="None"></asp:GridView>
                    </div>
                    <div class="col-sm-4">
                        <label>Invoiced:</label>
                        <asp:GridView ID="gvSbInvoiced" runat="server" CssClass="table table-hover table-striped gvstyling stackable tablesorter" GridLines="None"></asp:GridView>
                    </div>

                    <div id="divBookedDetail" runat="server" class="col-sm-12">
                        <h4>Booked Detail:</h4>
                        <uc1:sm_datatable runat="server" ID="smdt_booked" class="dataGrid" />
                    </div>
                    <div id="divBilledDetail" runat="server" class="col-sm-12">
                        <h4>Billed Detail:</h4>
                        <uc1:sm_datatable runat="server" ID="smdt_billed" class="dataGrid" />
                    </div>
                </div>
            </div>
        </div>


        <%--Projected Gross Profit--%>
        <div class="panel panel-default card">
            <div class="panel-body" style="text-align: center;">
                <div class="col-sm-4">
                    Projected Sales:
                                <h4>
                                    <asp:Label ID="lblProjSales" runat="server"></asp:Label></h4>
                </div>
                <div class="col-sm-4">
                    <h3>Projected Gross Profit
                             <asp:Label ID="lblProjectedGP" runat="server"></asp:Label>
                    </h3>
                </div>
                <div class="col-sm-4">
                    Projected GP:
                              <h4>
                                  <asp:Label ID="lblProjGP" runat="server"></asp:Label></h4>
                </div>

                <p style="font-size: 10px;">
                    <em>
                        <asp:Label ID="lblPGPDate" runat="server"></asp:Label></em>
                </p>
                <div class="panel panel-default panel-body card">
                    <canvas id="ProjectedGP" height="50"></canvas>
                </div>
                <a href="#gvProjGP" onclick="toggleElement('divProjGP'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
                <div class="col-sm-12">
                    <div id="divProjGP" style="display: none;">
                        <uc1:sm_datatable runat="server" ID="smdtProjGP" class="dataGrid" />
                        <%--<asp:GridView ID="gvProjGP" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None"></asp:GridView>--%>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-default card" style="display: none">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">

                        <label>
                            Total % Req to Quote<asp:Label ID="lblpctReqToQuote" runat="server"></asp:Label>
                        </label>
                        <em>
                            <asp:Label ID="lblpctReqToQuoteNoData" runat="server"></asp:Label></em>

                        <canvas id="PctReqsQuotePie"></canvas>
                    </div>

                    <div class="col-sm-4">
                        <%--   <asp:Panel ID="pnlLineStatus" runat="server" Visible="false">--%>

                        <label>
                            Line Status
                        </label>
                        <em>
                            <asp:Label ID="lblLineStatusNoData" runat="server"></asp:Label>
                        </em>

                        <canvas id="cvLineStatus"></canvas>

                        <%-- </asp:Panel>--%>
                    </div>

                    <div class="col-sm-4">

                        <label>
                            Pipeline Stages
                        </label>
                        <em>
                            <asp:Label ID="lblPipelineStageNoData" runat="server"></asp:Label>
                        </em>
                        <canvas id="cvSalesPipelineStage"></canvas>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default card" style="display: none">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">

                        <label>
                            % Req to Quote by Agent 
                        </label>
                        <em>
                            <asp:Label ID="lblPCtReqsAgentNoData" runat="server"></asp:Label></em>

                        <canvas id="PctReqToQuoteAgentBar"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <%--Deductions to GP %--%>
        <div class="panel panel-default card">
            <div class="panel-body" style="text-align: center;">
                <div class="row">
                    <div class="col-sm-12">

                        <div style="text-align: center;">
                            <h3>% Deductions to GP</h3>
                            <em>
                                <asp:Label ID="lblDeductionsNoData" runat="server"></asp:Label>
                            </em>
                        </div>

                        <%--<canvas id="cvDeductionsBar" height="100"></canvas>--%>
                        <canvas id="cvDeductionsBar" height="75"></canvas>
                    </div>
                    <a href="#gvDeductions" onclick="toggleElement('divDeductions'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
                    <div class="col-sm-12">
                        <div id="divDeductions" style="display: none;">
                            <uc1:sm_datatable runat="server" ID="smdtDeductions" class="dataGrid" />
                            <%--<asp:GridView ID="gvDeductions" runat="server" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None"></asp:GridView>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <%--<script>
        $('#MainContent_gvProjGP').stacktable();
    </script>--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

