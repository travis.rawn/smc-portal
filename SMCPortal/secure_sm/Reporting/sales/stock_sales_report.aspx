﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="stock_sales_report.aspx.cs" Inherits="secure_sm_Reporting_sales_stock_sales_report" %>

<%@ Register Src="~/assets/controls/sm_checkbox.ascx" TagPrefix="uc1" TagName="sm_checkbox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .subtotal {
            font-weight: 600;
        }

        .total {
            font-weight: 600;
            text-align: center''
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

    <h4>Stock Sales Report</h4>
    <div class="well well-sm total">
        <div class="row">
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-2">
                <uc1:RzAgentPicker runat="server" ID="rzap" />
            </div>
            <div class="col-sm-3">
                <asp:TextBox ID="txtInstantSearch" runat="server" PlaceHolder="Type to filter Gridviews"></asp:TextBox>
            </div>
            <div class="col-sm-2">
                <asp:LinkButton ID="lbSearch" runat="server" OnClientClick="ShowUpdateProgress();">Refresh</asp:LinkButton> <br />
                <uc1:sm_checkbox runat="server" ID="smcbIncludeLots" theText="Include Lots?" isChecked="false"/>
            </div> 
        </div>
    </div>
   <%-- <div class="well well-sm total">
        <div class="row">
            <div class="col-sm-4">
                <asp:Label ID="lblTotalSales" runat="server" CssClass="form-control"></asp:Label>
            </div>

            <div class="col-sm-4">
                <asp:Label ID="lblTotalPOCost" runat="server" CssClass="form-control"></asp:Label>
            </div>

            <div class="col-sm-4">
                <asp:Label ID="lblTotalGP" runat="server" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label>
                    <asp:Label ID="lblTotalResults" runat="server"></asp:Label></label>
            </div>
        </div>
    </div>--%>
    <h5>All Stock POs</h5>
    <div class="well well-sm subtotal">
        <div class="row">
            <div class="col-sm-3">
                Stock PO Count: 
                <asp:Label ID="lblAllPOCount" runat="server"></asp:Label>
            </div>
            <div class="col-sm-5">
                Stock PO Cost: 
                <asp:Label ID="lblAllPOCost" runat="server"></asp:Label>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a href="#smdtStockPos" onclick="toggleElement('divAllPoStock'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>

            <asp:LinkButton ID="exportStockPOLines" runat="server" OnClick="exportStockPOLines_Click">Export ...</asp:LinkButton>
        </div>
    </div>
    <div id="divAllPoStock" style="display: none;">
        <uc1:sm_datatable runat="server" ID="smdtAllPoStock" class="dataGrid" />
    </div>
    <br />

    <h5>Sold Stock Lines</h5>
    <div class="well well-sm subtotal">
        <div class="row">
            <div class="col-sm-3">
                Total Sales: 
            <asp:Label ID="lblSoldTotalSales" runat="server"></asp:Label>
            </div>
            <div class="col-sm-3">
                Total Cost: 
            <asp:Label ID="lblSoldTotalCost" runat="server"></asp:Label>
            </div>
            <div class="col-sm-3">
                Gross Profit: 
            <asp:Label ID="lblSoldTotalGp" runat="server"></asp:Label>
            </div>
            <div class="col-sm-3">
                GP Margin: 
            <asp:Label ID="lblSoldGpMargin" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a href="#smdtSoldStock" onclick="toggleElement('divSoldStock'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>

            <asp:LinkButton ID="lbExportSoldStock" runat="server" OnClick="lbExportSoldStock_Click">Export ...</asp:LinkButton>

        </div>
    </div>
    <div id="divSoldStock" style="display: none;">
        <uc1:sm_datatable runat="server" ID="smdtSoldStock" class="dataGrid" />
    </div>
    <hr />
    <h5>Un-Sold PO Lines</h5>
    <div class="well well-sm subtotal">
        <div class="row">
            <div class="col-sm-3">
                Unsold Line Count: 
            <asp:Label ID="lblUnsoldLineCount" runat="server"></asp:Label>
            </div>
            <div class="col-sm-3">
                Total Cost: 
            <asp:Label ID="lblUnsoldCost" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a href="#smdtUnsoldStock" onclick="toggleElement('divUnsoldStock'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
            <asp:LinkButton ID="lbExportUnSoldStock" runat="server" OnClick="lbExportUnSoldStock_Click">Export ...</asp:LinkButton>
        </div>
    </div>
    <div id="divUnsoldStock" style="display: none;">
        <uc1:sm_datatable runat="server" ID="smdtUnsoldStock" class="dataGrid" />
    </div>



    <script>
        allStockTable = $("#MainContent_smdtAllPoStock_gvMain").DataTable();
        soldStockTable = $("#MainContent_smdtSoldStock_gvMain").DataTable();
        unsoldStockTable = $("#MainContent_smdtUnsoldStock_gvMain").DataTable();
        $('#MainContent_txtInstantSearch').keyup(function () {
            allStockTable.search($(this).val()).draw();
            soldStockTable.search($(this).val()).draw();
            unsoldStockTable.search($(this).val()).draw();
        })

        function SearchDataTableForString(dataTable, string) {
            //Break the array into space-separated strings.
            if ($.fn.DataTable.isDataTable(dataTable)) {
                dataTable.DataTable().search(string).draw();
            };
        }
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

