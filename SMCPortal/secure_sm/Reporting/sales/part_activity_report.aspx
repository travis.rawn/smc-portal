﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="part_activity_report.aspx.cs" Inherits="secure_sm_Reporting_sales_part_activity_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .refresh-button {
            margin-top: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h2>Part Activity Report</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
    <asp:Panel ID="pnlSearch" DefaultButton="lbRefresh" runat="server">


        <div class="row">
            <div class="col-sm-3">
                <label>Reqs / Sales</label>
                <asp:DropDownList ID="ddlReqSale" runat="server">
                    <asp:ListItem Text="All" Value="all" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Reqs" Value="reqs"></asp:ListItem>
                    <asp:ListItem Text="Sales" Value="sales"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-3">
                <label>Start Date</label>
              
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-3">
                <label>End Date</label>
             
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-3">
                <label>Minimum Count</label>
                <asp:TextBox ID="txtMinCount" runat="server" placeholder="Minimum Req Count"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label>Agent Type (Sales vs Disty)</label>
                <asp:DropDownList ID="ddlAgentType" runat="server">
                    <asp:ListItem Text="All" Value="all"></asp:ListItem>
                    <asp:ListItem Text="Disty" Value="disty"></asp:ListItem>
                    <asp:ListItem Text="Sales" Value="sales" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-3">
                <br />
                <asp:LinkButton ID="lbExcessScrub" runat="server" OnClientClick="ShowUpdateProgress();" OnClick="lbExcessScrub_Click">Scrub Excess</asp:LinkButton>
            </div>


        </div>

        <div class="row refresh-button">
            <div class="col-sm-12">
                <asp:LinkButton ID="lbRefresh" runat="server" OnClick="lbRefresh_Click" OnClientClick="ShowUpdateProgress();">Refresh</asp:LinkButton>
            </div>
        </div>

        <%--Results Grid--%>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <asp:GridView runat="server" ID="gvResults" AutoGenerateColumns="True" DataKeyNames="PartNumber">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <!-- Trigger the modal with a button -->
                                    <asp:LinkButton runat="server" ID="lbDetails" OnClientClick="ShowUpdateProgress();" OnClick="lbDetails_Click">Details...</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                </div>

            </div>
        </div>

    </asp:Panel>


    <!-- Details Modal -->
    <div class="modal fade" id="divModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Details for:
                            <b>
                                <asp:Label ID="lblActivityDetailLabel" runat="server"></asp:Label></b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4>Quotes:  
                            <asp:Label ID="lblTotalQuotes" runat="server" Text="0"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-sm-4">
                            <h4>
                                <label>Sales: </label>
                                <asp:Label ID="lblTotalSales" runat="server" Text="0"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-sm-4">
                            <h4>
                                <label>Total: </label>
                                <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                            </h4>
                        </div>
                    </div>
                    <%--<h3>Quotes</h3>--%>
                    <uc1:sm_datatable runat="server" ID="smdtDetails" />
                    <%-- <h3>Sales</h3>
                        <uc1:sm_datatable runat="server" ID="smdtSaleDetails" />--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="divEsrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Excess Scrub Results:</h4>
                </div>
                <div class="modal-body">
                  
                     <uc1:sm_datatable runat="server" ID="smdt_excess" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

