﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_sales_pacing_report : System.Web.UI.Page
{

    DateTime startDatePrevious;
    DateTime endDatePrevious;
    DateTime startDateCurrent;
    DateTime endDateCurrent;
    DataTable dt;
    List<ordhed_quote> prevQuotes = new List<ordhed_quote>();
    List<ordhed_sale> prevSales = new List<ordhed_sale>();
    List<ordhed_invoice> prevInvoices = new List<ordhed_invoice>();
    List<ordhed_quote> currentQuotes = new List<ordhed_quote>();
    List<ordhed_sale> currentSales = new List<ordhed_sale>();
    List<ordhed_invoice> currentInvoices = new List<ordhed_invoice>();
    SM_Tools tools = new SM_Tools();
    string companyName;


    protected void Page_Load(object sender, EventArgs e)
    {
        smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
        LoadDates();

    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridViewRow row = e.Row;

            TableCell paceCell = row.Cells[11];
            decimal paceValue = 0;
            string strippedValue = RemoveCurrencySymbols(paceCell.Text);
            bool isDecimal = decimal.TryParse(strippedValue, out paceValue);
            if (!isDecimal)
                return;
            paceValue = Convert.ToDecimal(strippedValue);
            if (paceValue < 0)
                row.BackColor = System.Drawing.Color.LightYellow;
            else if (paceValue > 0)
                row.BackColor = System.Drawing.Color.LightGreen;


        }
    }

    private string RemoveCurrencySymbols(string text)
    {
        bool isNegative = text.Contains("(") || text.Contains(")");
        text = text.Replace("$", "");
        text = text.Replace("(", "");
        text = text.Replace(")", "");
        if (isNegative)
            text = "-" + text;
        return text;
    }

    private void LoadGrid()
    {
        smdt.dataSource = null;
        if (dt == null)
            tools.HandleResult("fail", "DataTable was null.");
        else if (dt.Rows.Count <= 0)
            tools.HandleResult("fail", "DataTable contained zero rows.");
        else
        {
            //DataView dv = dt.AsDataView();
            //dv.Sort = "PaceDelta desc";
        
            smdt.dataSource = dt;
            smdt.sortColumn = 11;
            smdt.sortDirection = "asc";
        }      

       

        smdt.loadGrid();
    }

    private void LoadData()
    {
        using (RzDataContext rdc = new RzDataContext())
        {


            string companyname = "";// "Composite Motors Inc.";//"NPI Technologies";
                                    //Get Separate queries, don't assume the ranges are contingent, may pull unnecessary data
                                    //Exanple, if you wanted to compare 2017 to 2200, you don't care about 2018-19
            var queryPrevious = rdc.ordhed_invoices.Where(w => w.date_created.Value.Date >= startDatePrevious & w.date_created.Value.Date <= endDateCurrent && w.agentname != "Phil Scott");
            if (!string.IsNullOrEmpty(companyname))
                queryPrevious = queryPrevious.Where(w => w.companyname == companyname);

            List<ordhed_invoice> invoicesPrevious = queryPrevious.ToList();
            List<string> invoicedCompanyIdsPrevious = invoicesPrevious.Select(s => s.base_company_uid).Distinct().ToList();


            string totalCompaniesPrevious = "Total Invoiced Companies Previous: " + invoicedCompanyIdsPrevious.Count();
            //totalCompaniesPrevious.Dump();

            List<company> companiesWith10KSalesPrevious = new List<company>();
            foreach (string s in invoicedCompanyIdsPrevious)
            {
                company c = rdc.companies.Where(w => w.unique_id == s).FirstOrDefault();
                if (c == null)
                    continue;
                double totalInvoiced = invoicesPrevious.Where(e => e.base_company_uid == c.unique_id).Sum(ss => ss.ordertotal ?? 0);
                if (totalInvoiced >= 10000)
                    companiesWith10KSalesPrevious.Add(c);
            }

            string total10KCompanies = "Total 10K+ Companies: " + companiesWith10KSalesPrevious.Count();



            prevQuotes = rdc.ordhed_quotes.Where(w => invoicedCompanyIdsPrevious.Contains(w.base_company_uid) && w.date_created.Value.Date >= startDatePrevious && w.date_created.Value.Date <= endDatePrevious).ToList();
            prevSales = rdc.ordhed_sales.Where(w => invoicedCompanyIdsPrevious.Contains(w.base_company_uid) && w.date_created.Value.Date >= startDatePrevious && w.date_created.Value.Date <= endDatePrevious).ToList();
            prevInvoices = rdc.ordhed_invoices.Where(w => invoicedCompanyIdsPrevious.Contains(w.base_company_uid) && w.date_created.Value.Date >= startDatePrevious && w.date_created.Value.Date <= endDatePrevious).ToList();


            currentQuotes = rdc.ordhed_quotes.Where(w => invoicedCompanyIdsPrevious.Contains(w.base_company_uid) && w.date_created.Value.Date >= startDateCurrent && w.date_created.Value.Date <= endDateCurrent).ToList();
            currentSales = rdc.ordhed_sales.Where(w => invoicedCompanyIdsPrevious.Contains(w.base_company_uid) && w.date_created.Value.Date >= startDateCurrent && w.date_created.Value.Date <= endDateCurrent).ToList();
            currentInvoices = rdc.ordhed_invoices.Where(w => invoicedCompanyIdsPrevious.Contains(w.base_company_uid) && w.date_created.Value.Date >= startDateCurrent && w.date_created.Value.Date <= endDateCurrent).ToList();


            LoadDataTable(rdc, companiesWith10KSalesPrevious);

        }
    }

    private void LoadDataTable(RzDataContext rdc, List<company> companiesWith10KSalesPrevious)
    {
        foreach (company c in companiesWith10KSalesPrevious)
        {




            //prevQuotes = rdc.ordhed_quotes.Where(w => c.unique_id == w.base_company_uid && w.date_created.Value.Date >= startDatePrevious && w.date_created.Value.Date <= endDatePrevious).ToList();
            //prevSales = rdc.ordhed_sales.Where(w => c.unique_id == w.base_company_uid && w.date_created.Value.Date >= startDatePrevious && w.date_created.Value.Date <= endDatePrevious).ToList();
            //prevInvoices = rdc.ordhed_invoices.Where(w => c.unique_id == w.base_company_uid && w.date_created.Value.Date >= startDatePrevious && w.date_created.Value.Date <= endDatePrevious).ToList();


            //currentQuotes = rdc.ordhed_quotes.Where(w => c.unique_id == w.base_company_uid && w.date_created.Value.Date >= startDateCurrent && w.date_created.Value.Date <= endDateCurrent).ToList();
            //currentSales = rdc.ordhed_sales.Where(w => c.unique_id == w.base_company_uid && w.date_created.Value.Date >= startDateCurrent && w.date_created.Value.Date <= endDateCurrent).ToList();
            //currentInvoices = rdc.ordhed_invoices.Where(w => c.unique_id == w.base_company_uid && w.date_created.Value.Date >= startDateCurrent && w.date_created.Value.Date <= endDateCurrent).ToList();






            //Previous
            string companyName2019 = c.companyname;
            string previousAgent = c.agentname;
            string previousIndustry = c.industry_segment;
            decimal previousQuoteCount = prevQuotes.Where(w => w.base_company_uid == c.unique_id).Count();
            decimal previousSalesCount = prevSales.Where(w => w.base_company_uid == c.unique_id).Count();
            decimal previousInvoices = prevInvoices.Where(w => w.base_company_uid == c.unique_id).Count();
            decimal previousQuoteAmount = Convert.ToDecimal(prevQuotes.Where(w => w.base_company_uid == c.unique_id).Sum(s => s.ordertotal ?? 0));
            decimal previousSalesAmount = Convert.ToDecimal(prevSales.Where(w => w.base_company_uid == c.unique_id).Sum(s => s.ordertotal ?? 0));
            decimal previousInvoiceAmount = Convert.ToDecimal(prevInvoices.Where(w => w.base_company_uid == c.unique_id).Sum(s => s.ordertotal ?? 0));
            decimal previousQtS = 0;
            decimal previousQtI = 0;
            //Monthly Pace - Totaly yearly sales / elapsed months
            int previousMonths = (endDatePrevious.Year - startDatePrevious.Year) * 12 + endDatePrevious.Month - startDatePrevious.Month;
            decimal previousPace = previousSalesAmount / previousMonths;

            //QtS
            if (previousQuoteAmount > 0)
            {
                previousQtS = (previousSalesAmount / previousQuoteAmount);
                previousQtI = (previousInvoiceAmount / previousQuoteAmount);
            }


            //Current	
            string currenTCompany = c.companyname;
            string currentAgent = c.agentname;
            string currentIndustry = c.industry_segment;
            decimal currentQuoteCount = currentQuotes.Where(w => w.base_company_uid == c.unique_id).Count();
            decimal currentSalesCount = currentSales.Where(w => w.base_company_uid == c.unique_id).Count();
            decimal currentInvoiceCount = currentInvoices.Where(w => w.base_company_uid == c.unique_id).Count();
            decimal currentQuoteAmount = Convert.ToDecimal(currentQuotes.Where(w => w.base_company_uid == c.unique_id).Sum(s => s.ordertotal ?? 0));
            decimal currentSaleAmount = Convert.ToDecimal(currentSales.Where(w => w.base_company_uid == c.unique_id).Sum(s => s.ordertotal ?? 0));
            decimal currentInvoiceAmount = Convert.ToDecimal(currentInvoices.Where(w => w.base_company_uid == c.unique_id).Sum(s => s.ordertotal ?? 0));
            int currentMonths = (endDateCurrent.Year - startDateCurrent.Year) * 12 + endDateCurrent.Month - startDateCurrent.Month;
            decimal currentPace = currentSaleAmount / currentMonths;


            decimal currentQtS = 0;
            decimal currentQtI = 0;
            if (currentQuoteAmount > 0)
            {
                currentQtS = currentSaleAmount / currentQuoteAmount;
                currentQtI = currentInvoiceAmount / currentQuoteAmount;
            }





            //2019 DataTable
            DataRow row = dt.NewRow();
            row["Companyname"] = companyName2019;
            row["Agent"] = currentAgent;
            row["Industry"] = currentIndustry;
            row["PreviousQuoteAmnt"] = Math.Round(previousQuoteAmount, 2).ToString("C");
            row["PreviousSaleAmnt"] = Math.Round(previousSalesAmount, 2).ToString("C");
            row["PreviousPaceAmnt"] = Math.Round(previousPace, 2).ToString("C");
            row["PreviousQtS"] = Math.Round(previousQtS, 2).ToString("P");
            row["CurrentQuoteAmnt"] = Math.Round(currentQuoteAmount, 2).ToString("C");
            row["CurrentSaleAmnt"] = Math.Round(currentSaleAmount, 2).ToString("C");
            row["CurrentPaceAmnt"] = Math.Round(currentPace, 2).ToString("C");
            row["CurrentQtS"] = Math.Round(currentQtS, 2).ToString("P");

            decimal paceDelta = 0;
            if (currentPace > 0)
                paceDelta = currentPace - previousPace;

            row["PaceDelta"] = Math.Round(paceDelta, 2).ToString("C");


            dt.Rows.Add(row);
        }
        ViewState["ResultDt"] = dt;
    }

    private void BuildDataTable()
    {
        dt = new DataTable();
        dt.Columns.Add("Companyname", typeof(string));
        dt.Columns.Add("Agent", typeof(string));
        dt.Columns.Add("Industry", typeof(string));

        dt.Columns.Add("PreviousQuoteAmnt", typeof(string));
        dt.Columns.Add("PreviousSaleAmnt", typeof(string));
        dt.Columns.Add("PreviousPaceAmnt", typeof(string));
        dt.Columns.Add("PreviousQtS", typeof(string));

        dt.Columns.Add("CurrentQuoteAmnt", typeof(string));
        dt.Columns.Add("CurrentSaleAmnt", typeof(string));
        dt.Columns.Add("CurrentPaceAmnt", typeof(string));
        dt.Columns.Add("CurrentQtS", typeof(string));


        dt.Columns.Add("PaceDelta", typeof(string));
    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpPrevStart.SelectedDate = new DateTime(2019, 01, 01);
            dtpPrevEnd.SelectedDate = new DateTime(2019, 12, 31);
            dtpCurrStart.SelectedDate = new DateTime(2020, 01, 01);
            dtpCurrEnd.SelectedDate = DateTime.Today.Date;
        }

        startDatePrevious = dtpPrevStart.SelectedDate.Value;
        endDatePrevious = dtpPrevEnd.SelectedDate.Value;
        startDateCurrent = dtpCurrStart.SelectedDate.Value;
        endDateCurrent = dtpCurrEnd.SelectedDate.Value;


    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        dt = (DataTable)ViewState["ResultDt"];
        if (dt == null || dt.Rows.Count <= 0)
        {
            tools.HandleResult("fail", "No data to export.");
            return;
        }


        tools.ExportDataTableToCsv(dt, "results.csv");
        tools.HandleResult("Success!", "Your export will be downloaded shortly.");
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        DateTime start = DateTime.Now;
        BuildDataTable();
        LoadData();
        LoadGrid();
        DateTime finish = DateTime.Now;
        TimeSpan ts = finish - start;
        string elapsed = ts.Duration().ToString();
        tools.HandleResult("success", "Elapsed: " + elapsed);


    }
}