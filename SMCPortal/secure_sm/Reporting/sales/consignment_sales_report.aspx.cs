﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using System.Data;



public partial class secure_sm_Reporting_sales_consignment_sales_report : System.Web.UI.Page
{
    //SensibleDAL.dbml.RzDataContext rdc = new SensibleDAL.dbml.RzDataContext();
    //RzDataContext rdc = new RzDataContext();
    //RzTools rzt = new RzTools();    
    ConsignmentData cons = new ConsignmentData();
    SM_Tools tools = new SM_Tools();
    List<ConsignmentData.ConsignmentSalesData> cList;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();
        LoadConsignmentCompanies();
        //LoadConsignmentData();


    }

    private void LoadConsignmentCompanies()
    {
        if (!Page.IsPostBack)
        {
            ddlCompany.DataSource = cons.GetConsignmentCompanies().OrderBy(o => o.companyname).Distinct().ToList();
            ddlCompany.AppendDataBoundItems = true;
            ddlCompany.DataTextField = "companyname";
            ddlCompany.DataValueField = "unique_id";
            ddlCompany.DataBind();
        }

    }



    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            //dtpStart.SelectedDate = tools.GetFirstDayOfMonth(DateTime.Today);
            dtpStart.SelectedDate = Tools.Dates.GetFirstDayOfMonth(new DateTime(DateTime.Now.Year, 01, 01));
            //dtpEnd.SelectedDate = tools.GetLastDayOfMonth(DateTime.Today);
            dtpEnd.SelectedDate = DateTime.Now.Date;
        }
    }

    private void LoadConsignmentData()
    {
        cList = cons.GetConsignmentData(dtpStart.SelectedDate.Value, dtpEnd.SelectedDate.Value, ddlCompany.SelectedValue, cbxOnlyPaidInFull.Checked);
        gvConsginmentSales.DataSource = null;

        lblCount.Text = "No Results.";
        lblTotal.Text = 0.ToString("C");
        lblTotalKeep.Text = 0.ToString("C");
        lblTotalPay.Text = 0.ToString("C");





        //if (cbxOnlyPaidInFull.Checked)
        //{
        //    var query = cList.Where(w => !w.InvoiceNumber.ToLower().Contains("cr") && !w.SO.ToLower().Contains("cr")).Select(s => new
        //    {
        //        Part = s.PartNumber,

        //        Agent = s.Agent,
        //        Cust = s.CustomerName,
        //        MFG = s.MFG,

        //        Qty = s.QTY,
        //        Price = GetCurrencyString(s.UnitPrice),
        //        InvTotal = GetCurrencyString(s.TotalPrice),
        //        Split = s.PayKeepPct,
        //        Pay = GetCurrencyString(s.PayoutAmnt),
        //        Keep = GetCurrencyString(s.KeepAmnt),
        //        Account = s.PaymentAccount,
        //        SO = s.SO,
        //        INV = s.InvoiceNumber,
        //        InvDate = s.InvoiceDate.ToString("MM/dd/yyyy"),
        //        Vendor = s.VendorName,

        //    }).ToList();
        //    if (query.Any())
        //    {
        //        gvConsginmentSales.DataSource = query;

        //    }
        //}
        //else
        //{
        var query = cList.Where(w => !w.InvoiceNumber.ToLower().Contains("cr") && !w.SO.ToLower().Contains("cr")).Select(s => new
        {
            Part = s.PartNumber,
            Agent = s.Agent,
            Cust = s.CustomerName,
            MFG = s.MFG,
            Qty = s.QTY,
            Price = GetCurrencyString(s.UnitPrice),
            InvTotal = GetCurrencyString(s.TotalPrice),
            Split = s.PayKeepPct,
            Pay = GetCurrencyString(s.PayoutAmnt),
            Keep = GetCurrencyString(s.KeepAmnt),
            Account = s.PaymentAccount,
            SO = s.SO,
            INV = s.InvoiceNumber,
            InvDate = s.InvoiceDate.ToString("MM/dd/yyyy"),
            Vendor = s.VendorName,

        }).ToList();
        if (query.Any())
        {
            gvConsginmentSales.DataSource = query;
        }
        //}


        gvConsginmentSales.DataBind();
        if (gvConsginmentSales.DataSource != null)//These we will round, but the details will show 6 decimals
        {
            lblTotal.Text = cList.Where(w => w.TotalPrice > 0).Sum(s => s.TotalPrice).ToString("C");
            lblCount.Text = cList.Count.ToString();
            //These we will round, but the details will show 6 decimals
            lblTotalKeep.Text = cList.Where(w => w.KeepAmnt > 0).Sum(s => s.KeepAmnt).ToString("C");
            lblTotalPay.Text = cList.Where(w => w.PayoutAmnt > 0).Sum(s => s.PayoutAmnt).ToString("C");
        }

    }

    private string GetCurrencyString(double d)
    {
        //I could use currency 6, but would that forces 6 places giving me a bung of zeroes
        //string s = d.ToString("C6");
        //string ret = "$" + d.ToString();
        string ret = "$" + Tools.Number.MoneyFormat_2_6(d);

        return ret;
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        try
        {
            LoadConsignmentData();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void gvConsginmentSales_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCell payCell = null;
            TableCell keepCell = null;
            if (!string.IsNullOrEmpty(e.Row.Cells[8].Text))
                payCell = e.Row.Cells[8];
            if (!string.IsNullOrEmpty(e.Row.Cells[9].Text))
                keepCell = e.Row.Cells[9];
            if (payCell != null)
                payCell.ForeColor = System.Drawing.Color.ForestGreen;
            if (keepCell != null)
                keepCell.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        LoadConsignmentData();
        string filename = Tools.Dates.get_datetime_for_file_export("consignment_sales_report", "today");
        tools.ExportListToCsv(cList, filename);

    }
}