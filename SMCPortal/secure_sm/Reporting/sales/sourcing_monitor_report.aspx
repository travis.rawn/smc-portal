﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="sourcing_monitor_report.aspx.cs" Inherits="secure_sm_Reporting_sales_sourcing_monitor_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <meta http-equiv="refresh" content="240">
    <style>
        .smdt-tooltip {
            /*font-size:1vh;*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h3>Sourcing Monitor Report</h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-3">
            <uc1:RzAgentPicker runat="server" ID="rzap" />
        </div>
        <div class="col-sm-3">
            <asp:LinkButton ID="lbRefresh" runat="server">Refresh</asp:LinkButton>
        </div>
    </div>
  <%--  <div class="row">
        <div class="col-sm-12">
            <div class="well well-sm">
                <p>The below shows open quotes and sales that have not yet been completed.</p>
            </div>
        </div>
    </div>--%>
    <div class="card">
        <div class="row">
            <div class="col-sm-12">
                <h4>In Progress:</h4>
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>
    </div>
    <hr />
    <div class="card">
        <div class="row">
            <div class="col-sm-12">
                <h4>Holds:</h4>
                <uc1:sm_datatable runat="server" ID="smdt_holds" />
            </div>
        </div>
    </div>
    <hr />
    <div class="card">
        <div class="row">
            <div class="col-sm-12">
                <h4>Validation Complete:</h4>
                <uc1:sm_datatable runat="server" ID="smdt_validationcomplete" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

