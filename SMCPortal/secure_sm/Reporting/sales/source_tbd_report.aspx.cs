﻿using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_source_tbd_report : System.Web.UI.Page
{
    RzDataContext rdc = new RzDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        //string connString = System.Configuration.ConfigurationManager.ConnectionStrings["AzureConnection"].ConnectionString;
        //rdc = new RzDataContext(connString);
        LoadData();
        BindGrid();
    }


    List<LineLogic.TbdLineObject> TbdLinesList
    {
        get
        {
            // check if not exist to make new (normally before the post back)
            // and at the same time check that you did not use the same viewstate for other object
            if (!(ViewState["TbdLinesList"] is List<LineLogic.TbdLineObject>))
            {
                // need to fix the memory and added to viewstate
                ViewState["TbdLinesList"] = new List<LineLogic.TbdLineObject>();
            }

            return (List<LineLogic.TbdLineObject>)ViewState["TbdLinesList"];
        }

        set
        {
            ViewState["TbdLinesList"] = value;
        }
    }



    private void LoadData()
    {
        List<orddet_line> ret = new List<orddet_line>();
        //Start by getting all new companies within the date Range
        TbdLinesList = LineLogic.GetTBDLines(rdc);




        if (TbdLinesList.Count <= 0)
            lblTotalTBD.Text += "$0";
        else
            lblTotalTBD.Text += TbdLinesList.Sum(s => Convert.ToDouble(s.quantity) * Convert.ToDouble(s.unit_price)).ToString("C");
    }

    private void BindGrid()
    {
        smdt.dataSource = null;
        smdt.loadGrid();

        var query = TbdLinesList.Select(s => new
        {

            Created = s.date_created,
            Part = s.fullpartnumber.Trim().ToUpper(),
            MFG = s.manufacturer.Trim().ToUpper(),
            Sale = s.sales_order.Trim().ToUpper(),
            Customer = s.customer_name,
            Agent = s.seller_name,
            QTY = s.quantity,
            Unit = Convert.ToDouble(s.unit_price).ToString("C"),
            Total = (Convert.ToDouble(s.unit_price) * Convert.ToDouble(s.quantity)).ToString("C"),
            Status = s.status

        }).OrderByDescending(o => o.Created);



        smdt.dataSource = query;
        smdt.loadGrid();
    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        SM_Tools tools = new SM_Tools();
        if (TbdLinesList.Count > 0)
            tools.ExportListToCsv(TbdLinesList, "tbd_lines_export.csv");

    }

    protected void lbEmail_Click(object sender, EventArgs e)
    {
        LineLogic.GenerateSourceTBDEmailReport(rdc);
    }
}