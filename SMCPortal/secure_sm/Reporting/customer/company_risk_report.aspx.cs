﻿using SensibleDAL.dbml;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_sales_company_risk_report : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    DateTime startDate;
    DateTime endDate;
    company SelectedCompany;
    public string SelectedCompanyID { get { return (ViewState["SelectedCompanyID"] ?? "").ToString(); } set { ViewState["SelectedCompanyID"] = value; } }

    List<CompanyRiskData> listCRD = new List<CompanyRiskData>();
    protected class CompanyRiskData
    {
        public long ComID { get; set; }
        public string Part { get; set; }
        public string Internal { get; set; }
        public string Lifecycle { get; set; }
        public string MFG { get; set; }
        public int RiskScore { get; set; }
        public string dataSheet { get; set; }
        public string Details { get; set; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ac.CompanySelected += new EventHandler(OnCompanySelected);
        LoadDates();
    }

    private void BindDataTable()
    {
        gvData.DataSource = null;
        gvData.DataBind();
        gvData.DataSource = listCRD.Select(ss => new { ss.Part, ss.Internal, ss.MFG, ss.Lifecycle, ss.Details, ss.RiskScore }).OrderBy(o => o.Lifecycle);
        gvData.DataBind();

        if (gvData.Rows.Count > 0) //Required for DataTables
        {
            SM_Tools tools = new SM_Tools();
            tools.LoadGridDataDataTablesNetDefault(this.Page, gvData);
            upGrid.Visible = true;
        }
    }

    protected void OnCompanySelected(object sender, EventArgs e)
    {
        try
        {
            LoadRiskData();
            BindDataTable();
            LoadRiskCards();

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void LoadRiskCards()
    {
        int riskThreshold = Convert.ToInt32(ddlRiskThreshold.SelectedValue);

        foreach (CompanyRiskData crd in listCRD.Where(w => w.RiskScore >= riskThreshold).OrderByDescending(o => o.RiskScore))
        {

            //Create risk card Control           
            assets_controls_ctl_risk_card card = (assets_controls_ctl_risk_card)Page.LoadControl("~/assets/controls/ctl_risk_card.ascx");
            card.riskScore = crd.RiskScore;
            card.riskLabel = "Score";
            card.partNumber = crd.Part;
            card.manuFacturer = crd.MFG;
            //  string navigateUrl = "/public/search/part_detail.aspx?pn=" + QueryPart;
            string detailUrl = "/public/search/part_detail.aspx?pn=" + crd.Part;
            string detailAnchor = "<a href='" + detailUrl + "' target=\"_blank\">Details ...</a>";

            if (crd.ComID > 0)
                detailUrl += "?id=" + crd.ComID;
            card.lifeCycle = crd.Lifecycle;
            card.pCn = detailAnchor;
            card.leadTime = detailAnchor;
            card.dataSheet = crd.dataSheet;

            //Create the child flex div
            System.Web.UI.HtmlControls.HtmlGenericControl childRiskCard = new System.Web.UI.HtmlControls.HtmlGenericControl();
            childRiskCard.Attributes.Add("class", "flex-row-item");


            //Addrisk card Control to child div
            childRiskCard.Controls.Add(card);

            //Add child  div to parent div
            divRiskCard.Controls.Add(childRiskCard);
            divRiskCard.Visible = true;
        }





    }

    protected void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hl = new HyperLink();
            hl.Text = "Details";
            hl.Target = "_Blank";



            TableCell linkCell = e.Row.Cells[4];
            string navigateUrl = linkCell.Text;
            linkCell.Text = "";
            string decodedUrl = HttpUtility.UrlDecode(navigateUrl);
            hl.NavigateUrl = decodedUrl;
            linkCell.Controls.Add(hl);





            TextBox tb = new TextBox();
            tb.Attributes.Add("Placeholder", "Enter Qty:");
            tb.Attributes.Add("onKeyup ", "javascript:return TextChanged(this); ");
            e.Row.Cells[6].Controls.Add(tb);

        }
    }

    private void LoadRiskData(List<string> partList, List<orddet_quote> qList = null)
    {
        //This Dataset will have ComIDS
        DataSet summaryDS = SensibleDAL.SiliconExpertLogic.GetListPartSearchDataSetXML(partList);
        silicon_expert se = new silicon_expert();

        foreach (string s in partList)
        {
            CompanyRiskData crd = new CompanyRiskData();

            crd.Part = s;
            crd.Internal = s;
            if (qList != null)
            {
                orddet_quote q = qList.Where(w => w.fullpartnumber.Replace("*", "").Trim().ToUpper() == s).OrderByDescending(o => o.internalpartnumber).ThenBy(b => b.target_manufacturer).FirstOrDefault();

                crd.Internal = q.internalpartnumber;
                if (string.IsNullOrEmpty(crd.Internal))
                    crd.Internal = s;
                crd.MFG = q.target_manufacturer ?? "";
            }


            crd.ComID = 0;
            crd.Lifecycle = "Unconfirmed";
            crd.RiskScore = 1;
            crd.dataSheet = "Unknown";
            //crd.Risk = "Unknown";
            crd.Details = "https://portal.sensiblemicro.com/public/search/part_search.aspx?pn=" + crd.Part;

            //So, partData will have the match to the part number.  for Instance, 88751-1310 is listed as 88751-1310 in partDetail but under partDto it's 887511310
            //Need to grab the matching ID from partData, then query partDto based on that.

            if (summaryDS != null && summaryDS.Tables.Count > 0)
            {
                var siPartData =
                    from myRow in summaryDS.Tables["PartData"].Rows.Cast<DataRow>()
                    where myRow.Field<string>("RequestedPart").ToUpper() == crd.Part
                    select myRow;

                long partDetaildID = siPartData.Select(ss => ss.Field<int>("PartData_Id")).FirstOrDefault();
                //Silicon Expert
                var siResult = from myRow in summaryDS.Tables["PartDto"].Rows.Cast<DataRow>()
                               where myRow.Field<int>("PartList_Id") == partDetaildID
                               select myRow;
                if (siResult.Any())
                {

                    //If no MFG from the quote line, check Silicon Expert MFG, of nothing there, fall back on PN, else "N/A"
                    if (string.IsNullOrEmpty(crd.MFG))
                    {
                        string seMfg = siResult.Select(ss => ss.Field<string>("Manufacturer")).FirstOrDefault();
                        if (!string.IsNullOrEmpty(seMfg))
                            crd.MFG = seMfg.Trim().ToUpper();
                        else
                            crd.MFG = "Unknown";

                    }
                    string dataSheet = siResult.Select(ss => ss.Field<string>("Datasheet")).FirstOrDefault();

                    string dataSheetUrl = "<a href='" + dataSheet + "'  target=\"_blank\">View / Download</a>";
                    if (!string.IsNullOrEmpty(dataSheet))
                        crd.dataSheet = dataSheetUrl;
                    else
                        crd.dataSheet = "Unknown";


                    string lc = siResult.Select(ss => ss.Field<string>("Lifecycle")).FirstOrDefault();
                    //string ri = siResult.Select(s => s.Field<string>("Risk")).FirstOrDefault();
                    string comID = siResult.Select(ss => ss.Field<string>("ComID")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(lc))
                        crd.Lifecycle = lc;

                    long lngComID = 0;
                    if (long.TryParse(comID, out lngComID))
                    {
                        crd.ComID = lngComID;
                        string url = HttpUtility.UrlEncode("~/public/search/part_detail.aspx?pn=" + s + "&id=" + lngComID.ToString());
                        crd.Details = url;
                    }
                }
            }


            crd.RiskScore = CalculareRiskScore(crd);


            if (!listCRD.Contains(crd))
                listCRD.Add(crd);
        }
    }




    private void LoadRiskData()
    {
        string selectedCompanyID = ac.CompanyID;

        using (RzDataContext rdc = new RzDataContext())
        {
            SelectedCompany = rdc.companies.Where(w => w.unique_id == selectedCompanyID).FirstOrDefault();
            SelectedCompanyID = SelectedCompany.unique_id;
            if (string.IsNullOrEmpty(SelectedCompanyID))
                return;
            //Part
            //Internal
            //MFG
            //Lifecycle
            //Risk
            //LinkToPortal

            //Get a list of all quote lines for company
            //List<orddet_quote> quoteList = new List<orddet_quote>();
            List<orddet_quote> qList = new List<orddet_quote>();


            //Note we're omitting short partnumbers as well as missing MFG
            qList = rdc.orddet_quotes.Where(w => (w.date_created.Value.Date >= startDate && w.date_created.Value.Date <= endDate) && w.fullpartnumber.Length > 3 && (w.target_manufacturer ?? "").Length > 0 && w.base_company_uid == SelectedCompany.unique_id).Distinct().ToList();

            if (qList.Count <= 0)
            {
                divRiskCard.Visible = false;
                upGrid.Visible = false;
                throw new Exception("No risk data found for " + ac.CompanyName);
            }



            //Get distinct list of part for Silicon Expert Search
            List<string> distinctParts = qList.Select(s => s.fullpartnumber.Replace("*", "").Trim().ToUpper()).Distinct().ToList();
            LoadRiskData(distinctParts, qList);



        }

    }

    private int CalculareRiskScore(CompanyRiskData crd)
    {
        switch (crd.Lifecycle.ToLower())
        {
            case "obsolete":
                return 5;
            case "nrnd":
            case "ltb":
                return 4;
            case "unconfirmed":
            case "aftermarket":
            case "unknown":
                return 3;
            case "acquired":
                return 2;
            default:
                return 1;
        }


    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = new DateTime(2019, 01, 01);
            dtpEnd.SelectedDate = DateTime.Today;
        }

        startDate = dtpStart.SelectedDate.Value;
        endDate = dtpEnd.SelectedDate.Value;
    }

    protected void lbGetQuotes_Click(object sender, EventArgs e)
    {

    }

    protected void lbGenerateRiskReport_Click(object sender, EventArgs e)
    {

    }

    protected void lbUploadParts_Click(object sender, EventArgs e)
    {
        try
        {


            //Confirm file is csv or xls
            if (!fuUploadParts.HasFile)
                return;

            //Set the path to save the file to
            string savePath = Path.GetTempPath() + Path.GetFileName(fuUploadParts.PostedFile.FileName);
            fuUploadParts.SaveAs(savePath);
            string fileExtension = Path.GetExtension(savePath);
            List<string> allowedExtensions = new List<string>() { ".xls", ".xlsx", ".csv" };
            if (!allowedExtensions.Contains(fileExtension))
                throw new Exception("Uploads must be in one of the following formats:  xls, xlsx, csv");
            //parse the file for the part numbers and qty
            List<string> allLines = new List<string>();
            if (fileExtension == ".csv")
                allLines = File.ReadAllLines(savePath).Cast<string>().ToList();
            else
                allLines = ReadExcelData(savePath);



            var query = from line in allLines
                        let data = line.Split(',')
                        select new
                        {
                            MPN = data[0].ToString(),

                        };
            //add them to the screen
            LoadRiskData(query.OrderBy(o => o.MPN).Where(w => w.MPN.Length > 0).Distinct().Select(s => s.MPN).ToList());
            BindDataTable();
            LoadRiskCards();

            //Delete file when done
            System.IO.File.Delete(savePath);

            //future - allow each import to be a "bom" like thing, with it's own name, and you can add / subtract parts from the bom to monitor or generate a report.
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }


    //Read From XLS / XLSX
    public List<string> ReadExcelData(string filePath)
    {
        List<string> ret = new List<string>();
        using (SLDocument sl = new SLDocument())
        {
            FileStream fs = new FileStream(filePath, FileMode.Open);
            SLDocument sheet = new SLDocument(fs, "Table 1");

            SLWorksheetStatistics stats = sheet.GetWorksheetStatistics();
            for (int j = 1; j < stats.EndRowIndex; j++)
            {
                // Get the first column of the row (SLS is a 1-based index)
                string value = sheet.GetCellValueAsString(j, 0);
                if (!string.IsNullOrEmpty(value))
                    if (!ret.Contains(value))
                        ret.Add(value);

            }
        }
        return ret;
    }

}