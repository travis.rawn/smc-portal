﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="customer_acquisition_report.aspx.cs" Inherits="secure_sm_Reporting_customer_customer_acquisition_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" Runat="Server">
    <div id="pageTitle">
        <h1>Customer Acquisition Report</h1>
    </div>
    <p>Get company statistics for companies created between a date range. </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row">
        <div class="col-sm-3">
            <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
        </div>
         <div class="col-sm-3">
             <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
        </div>
         <div class="col-sm-2">
             <asp:LinkButton ID="lbSearch" runat="server"  OnClientClick="ShowUpdateProgress();">Search</asp:LinkButton>
             <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">Export</asp:LinkButton>
        </div>         
    </div>
    <div class="row">
        <div class="col-sm-12">
             <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>

   
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" Runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" Runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" Runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" Runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" Runat="Server">
</asp:Content>

