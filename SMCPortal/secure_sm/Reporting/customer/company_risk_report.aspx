﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="company_risk_report.aspx.cs" Inherits="secure_sm_Reporting_sales_company_risk_report" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/ctl_risk_card.ascx" TagPrefix="uc1" TagName="ctl_risk_card" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


    <style>
        /*Parent*/
        .flex-row-container {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            /*justify-content: flex-start;*/
            justify-content: space-between;
            align-items: center;
            align-content: stretch;
        }

        /*child*/
        .flex-row-item {
            margin: 5px;
            /*width: 30%;*/
            width: 345px;
            height: 200px;
            /*background-color: #fff4e6;*/
            border: 1px solid #f76707;
            padding: 10px;
        }

        #divRiskCard {
            max-width: 300px;
        }

        .small-text {
            font-size: 14px;
            padding-top: 5px;
            margin-bottom: 0px;            
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:Panel ID="pnlControls" runat="server">


        <div class="row">
            <div class="col-lg-12">
                <div class="well">
                    <p class="small-text">This tool will look at quoted parts for a given customer, between a date range, and get lifecycle data.  You can get risk data from past orders / quotes, or upload a new list.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6  well well-sm">
                <div class="row">
                    <div class="col-sm-6">
                        <asp:FileUpload ID="fuUploadParts" runat="server" CssClass="form-control" />
                    </div>
                    <div class="col-sm-6">
                        <asp:LinkButton ID="lbUploadParts" runat="server" CssClass="btn btn-default btn-block" OnClick="lbUploadParts_Click" OnClientClick="ShowUpdateProgress();">Upload Parts</asp:LinkButton>

                    </div>
                </div>

            </div>
            <div class="col-lg-6  well well-sm">
                <div class="row">
                    <div class="col-sm-7">
                        <p class="small-text">Enter Qty into the below to select parts to quote.</p>
                    </div>

                    <div class="col-sm-5">
                        <asp:LinkButton ID="lbGetQuotes" runat="server" CssClass="btn btn-default btn-block" OnClick="lbGetQuotes_Click" OnClientClick="return DoQuoteTasks();">Quote From Grid</asp:LinkButton>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-sm-2">
                <label>Start Date</label>
                
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-2">
                <label>End Date</label>                
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-1">
                <label>Min Risk</label>
                <asp:DropDownList ID="ddlRiskThreshold" runat="server">
                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-3">
                <label>Company</label>
                <uc1:rz_company_autocomplete runat="server" ID="ac" />
            </div>
        </div>
    </asp:Panel>

    <div class="flex-row-container" runat="server" id="divRiskCard">
    </div>


    <asp:UpdatePanel ID="upGrid" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvData" runat="server" OnRowDataBound="smdt_RowDataBound" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField HeaderText="Part" SortExpression="Part" DataField="Part" />
                    <asp:BoundField HeaderText="Internal" SortExpression="Internal" DataField="Internal" />
                    <asp:BoundField HeaderText="MFG" SortExpression="MFG" DataField="MFG" />
                    <asp:BoundField HeaderText="Lifecycle" SortExpression="Lifecycle" DataField="Lifecycle" />
                    <asp:BoundField HeaderText="Details" SortExpression="Details" DataField="Details" />
                    <asp:BoundField HeaderText="RiskScore" SortExpression="RiskScore" DataField="RiskScore" />
                    <asp:BoundField HeaderText="Qty" SortExpression="Qty" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>



    <script>
        function DoQuoteTasks() {
            alert('This will generate a quote for any rows in the grid that have a qty to be quoted.');
            return false;
        }

        function TextChanged(text) {

            var strText = text.value;
            if (strText === "" || strText === null || strText === "0") {
                $(text).closest('tr').children('td,th').css('background-color', ' #ffffff');
                //return false;
            }
            else {
                // alert('Text Changed!');
                $(text).closest('tr').children('td,th').css('background-color', '#7DCE82');
                // return false;
            }

        }


    </script>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

