﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_customer_customer_acquisition_report : System.Web.UI.Page
{
    [Serializable]
    class CompanyStatistics
    {
        //Company Detail
        public string Created { get; set; }
        public string unique_id { get; set; }

        //Current Agent
        public string CompanyName { get; set; }
        public string CurrentAgent { get; set; }

        //Quotes
        public string FirstQuoteDate { get; set; }
        public string FirstQuoteAgent { get; set; }

        //Sales
        public string FirstSaleDate { get; set; }
        public string FirstSaleAgent { get; set; }
        public string FirstSaleAmount { get; set; }
    }

    //List<CompanyStatistics> CompanyStatisticsList = new List<CompanyStatistics>();

    List<CompanyStatistics> CompanyStatisticsList
    {
        get
        {
            // check if not exist to make new (normally before the post back)
            // and at the same time check that you did not use the same viewstate for other object
            if (!(ViewState["CompanyStatisticsList"] is List<CompanyStatistics>))
            {
                // need to fix the memory and added to viewstate
                ViewState["CompanyStatisticsList"] = new List<CompanyStatistics>();
            }

            return (List<CompanyStatistics>)ViewState["CompanyStatisticsList"];
        }

        set
        {
            ViewState["CompanyStatisticsList"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();
        LoadData();
        BindGrid();
    }

    private void LoadDates()
    {
        if (Page.IsPostBack)
            return;
        dtpStart.SelectedDate = DateTime.Today.AddMonths(-12);
        dtpEnd.SelectedDate = DateTime.Today;
    }

    private void LoadData()
    {
        DateTime start = dtpStart.SelectedDate.Value;
        DateTime end = dtpEnd.SelectedDate.Value;
        List<company> companyList = new List<company>();
        List<ordhed_quote> quoteList = new List<ordhed_quote>();
        List<ordhed_sale> salesList = new List<ordhed_sale>();
        using (RzDataContext rdc = new RzDataContext())
        {
            //Start by getting all new companies within the date Range
            companyList = rdc.companies.Where(w => w.date_created >= start && w.date_created <= end && !w.companytype.ToLower().Contains("vendor")).ToList();
            //Build statistics per company
            CompanyStatisticsList = BuildCompanyStatisticsList(companyList, rdc);
        }

    }

    private void BindGrid()
    {
        smdt.dataSource = null;
        smdt.DataBind();

        if (CompanyStatisticsList.Count <= 0)
            return;


        var query = CompanyStatisticsList.Select(s => new
        {
            s.Created,
            s.CompanyName,
            s.CurrentAgent,
            s.FirstQuoteDate,
            s.FirstQuoteAgent,
            s.FirstSaleDate,
            s.FirstSaleAgent,
            s.FirstSaleAmount
        });

        smdt.dateTimeFormat = "'M/D/YYYY'";
        smdt.dataSource = query.OrderByDescending(o => o.Created);
        smdt.loadGrid();
    }

    private List<CompanyStatistics> BuildCompanyStatisticsList(List<company> companyList, RzDataContext rdc)
    {

        List<CompanyStatistics> ret = new List<CompanyStatistics>();
        List<string> companyIDList = companyList.Select(s => s.unique_id).Distinct().ToList();
        List<ordhed_quote> quotesList = rdc.ordhed_quotes.Where(w => companyIDList.Contains(w.base_company_uid)).ToList();
        List<ordhed_sale> salesList = rdc.ordhed_sales.Where(w => companyIDList.Contains(w.base_company_uid)).ToList();
        foreach (company c in companyList)
        {
            //for all the new companies, build a company stats object
            CompanyStatistics cs = new CompanyStatistics();
            cs.Created = c.date_created.Value.Date.ToShortDateString();
            cs.CompanyName = c.companyname;
            cs.CurrentAgent = c.agentname;
            //List<ordhed_quote> quotes = new List<ordhed_quote>();
            //quotes = rdc.ordhed_quotes.Where(w => w.base_company_uid == c.unique_id).ToList();

            ordhed_quote firstQuote = quotesList.Where(w => w.base_company_uid == c.unique_id).OrderBy(o => o.date_created.Value.Date).FirstOrDefault();
            cs.FirstQuoteAgent = firstQuote == null ? "N/A" : firstQuote.agentname;
            cs.FirstQuoteDate = firstQuote == null ? "N/A" : firstQuote.date_created.Value.ToShortDateString();
            ordhed_sale firstSale = salesList.Where(w => w.base_company_uid == c.unique_id).OrderBy(o => o.date_created.Value.Date).FirstOrDefault();
            cs.FirstSaleAgent = firstSale == null ? "N/A" : firstSale.agentname;
            cs.FirstSaleDate = firstSale == null ? "N/A" : firstSale.date_created.Value.ToShortDateString();
            
            ret.Add(cs);
        }
        return ret;
    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        //LoadData();
        SM_Tools tools = new SM_Tools();
        tools.ExportListToCsv(CompanyStatisticsList, "new_customer_statistics.csv");
    }
}