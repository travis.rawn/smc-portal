﻿using HubspotApis;
using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class secure_sm_Reporting_dashboards_sales_dashboard : System.Web.UI.Page
{

    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();
    List<SalesLogic.OrderQueryResult> saleList = new List<SalesLogic.OrderQueryResult>();
    List<SalesLogic.OrderQueryResult> quoteList = new List<SalesLogic.OrderQueryResult>();
    List<GPDataSet> gpData = new List<GPDataSet>();
    List<WeeklySalesDataSet> weeklySalesData = new List<WeeklySalesDataSet>();
    List<company> newCustomerList = new List<company>();
    List<HubspotApi.Owner> ownerList = new List<HubspotApi.Owner>();
    List<hubspot_engagement> engList = new List<hubspot_engagement>();
    List<n_user> allSalesUsers = new List<n_user>();

    private DateTime startDate;
    private DateTime endDate;

    List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();



    private class GPDataSet
    {
        public string agentID { get; set; }
        public string agentName { get; set; }
        public double GP { get; set; }
        public double agentGoal { get; set; }
        public int countOfMonths { get; set; }
        public string orderType { get; set; }

    }

    private class WeeklySalesDataSet
    {
        public DateTime SaleDate { get; set; }
        public double GP { get; set; }
        public int Year { get; set; }
        public int Week { get; set; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDatePicker();
        LoadAgentPicker();
        if (Page.IsPostBack)
        {
            LoadData();
            LoadCharts();
            LoadGrids();
        }

    }

    private void LoadDatePicker()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Today.Date.AddMonths(-6);
            dtpEnd.SelectedDate = DateTime.Today.Date;
        }

        startDate = dtpStart.SelectedDate.Value;
        endDate = dtpEnd.SelectedDate.Value;

    }

    private void LoadAgentPicker()
    {//List<string> salesTeams = new List<string>() { "Sales", "Distributor Sales" };       
        List<string> salesTeams = new List<string>() { "Sales" };
        allSalesUsers = rzt.GetUsersForTeams(salesTeams);
        if (!Page.IsPostBack)
        {
            rzap.LoadPicker(allSalesUsers);
            rzap.SelectedUserID = "all";
            rzap.Enabled = false;

            //Get list of authorized users, if current user is in that list, Enable the control.
            List<n_user> managementUsers = rzt.GetUsersForTeams(new List<String>() { "Sales Management" });
            List<string> managementUserIDs = managementUsers.Select(s => s.unique_id).Distinct().ToList();           
            string CurrentUserID = Profile.RzUserID;
            if (managementUserIDs.Contains(CurrentUserID))
                rzap.Enabled = true;
            else
                rzap.SelectedUserID = CurrentUserID;


            //Check Rzap if current user is in there, if not, throw error


            //Set list to current user


        }


    }

    private void LoadData()
    {
        bool loadQuoteSalesData = cbLoadGPData.isChecked;
        bool loadNewCustomerData = cbLoadNewCustomerData.isChecked;
        bool loadEngagementData = cbLoadEngagementData.isChecked;


        using (RzDataContext rdc = new RzDataContext())
        {
            //Sales Data
            if (loadQuoteSalesData)
                LoadQuoteSalesData(rdc);


            //New Customer Data
            if (loadNewCustomerData)
                LoadNewCustomerData(rdc);

            //Phone Call Data
            if (loadEngagementData)
                LoadEngagementData(rdc);

        }
    }
    private void LoadCharts()
    {
        LoadGPChart();
        LoadNCChart();
        LoadPCChart();
        LoadWeeklySalesChart();


        SM_Charts smr = new SM_Charts();
        smr.LoadAllCharts(Page, ChartList);

    }


    private void LoadQuoteSalesData(RzDataContext rdc)
    {
        List<string> badStatus = LineData.GetInvalid_orddet_Status(true, true).ToList();
        List<n_user> queryAgents = allSalesUsers;
        if (rzap.SelectedUserID.Length > 10)//GUId's are larger than 10
            queryAgents = queryAgents.Where(w => w.unique_id == rzap.SelectedUserID).ToList();
        saleList = SalesLogic.GetBookedOrders(rdc, startDate, endDate, badStatus, queryAgents).OrderByDescending(o => o.GP).ToList();
        //Sale Data
        foreach (n_user u in queryAgents)
        {
            GPDataSet saleDataSet = new GPDataSet();

            saleDataSet.agentID = u.unique_id;
            saleDataSet.agentName = u.name;
            saleDataSet.GP = 0;
            saleDataSet.agentGoal = 0;
            saleDataSet.orderType = "Sale";
            if (saleList.Where(w => w.AgentID == u.unique_id).Any())
            {
                double grossProfit = saleList.Where(w => w.AgentID == u.unique_id).Sum(s => s.GP);
                saleDataSet.GP = Math.Round(grossProfit, 2);
                saleDataSet.agentGoal = LoadAgentMonthlyBookingGoal(rdc, u);
            }
            gpData.Add(saleDataSet);
        }
        //Quote Data
        //quoteList = SalesLogic.GetQuotedOrders(rdc, startDate, endDate, badStatus, queryAgents).OrderByDescending(o => o.GP).ToList();
        //foreach (n_user u in queryAgents)
        //{
        //    GPDataSet quoteDataSet = new GPDataSet();

        //    quoteDataSet.agentID = u.unique_id;
        //    quoteDataSet.agentName = u.name;
        //    quoteDataSet.GP = 0;
        //    quoteDataSet.agentGoal = 0;
        //    quoteDataSet.orderType = "Quote";
        //    if (saleList.Where(w => w.AgentID == u.unique_id).Any())
        //    {
        //        double grossProfit = quoteList.Where(w => w.AgentID == u.unique_id).Sum(s => s.GP);
        //        quoteDataSet.GP = Math.Round(grossProfit, 2);
        //        quoteDataSet.agentGoal = LoadAgentMonthlyBookingGoal(rdc, u);
        //    }
        //    gpData.Add(quoteDataSet);
        //}


    }

    private double LoadAgentMonthlyBookingGoal(RzDataContext rdc, n_user agent)
    {
        //n_user agent = agentList.Where(w => w.unique_id == agentID).FirstOrDefault();
        //int monthsCount = ((endDate.Year - startDate.Year) * 12) + (endDate.Month - startDate.Month) + 1;
        int monthsCount = Tools.Dates.GetMonthCount(startDate, endDate);
        return monthsCount * agent.monthly_booking_goal ?? 0;


    }

    private void LoadNewCustomerData(RzDataContext rdc)
    {
        List<string> validCompanyTypes = new List<string>() { "c", "p" };
        List<ordhed_sale> recentSales = rdc.ordhed_sales.Where(w => w.date_created.Value.Date >= startDate).ToList();
        List<string> recentSalesCompanyIds = recentSales.Select(s => s.base_company_uid).Distinct().ToList();
        List<company> recentSalesCompanies = rdc.companies.Where(w => recentSalesCompanyIds.Contains(w.unique_id) && validCompanyTypes.Contains(w.companytype)).ToList();


        foreach (company c in recentSalesCompanies)
        {
            bool hasSaleInLastThreeYEars = rdc.ordhed_sales.Where(w => w.base_company_uid == c.unique_id && (w.date_created.Value.Date <= startDate && w.date_created.Value.Date >= startDate.AddYears(-3))).Any();
            if (!hasSaleInLastThreeYEars)
                if (!newCustomerList.Contains(c))
                    newCustomerList.Add(c);
        }

        //Filter to agent
        if (rzap.SelectedUserID != "all")
            newCustomerList = newCustomerList.Where(w => w.base_mc_user_uid == rzap.SelectedUserID).ToList();

    }


    private void LoadEngagementData(RzDataContext rdc)
    {
        //List of skipped emails
        List<string> skippedAgentEmails = new List<string>() { "smar@sensiblemicro.com", "tkreider@sensiblemicro.com", "ktill@sensiblemicro.com", "joemar@sensiblemicro.com", "pbravo@sensiblemicro.com", "ctorrioni@sensiblemicro.com", "lmcdonald@sensiblemicro.com", "adevivo@sensiblemicro.com" };


        // Get the Engagements
        engList = HubspotLogic.EngagementActivity.GetEngagementList(rdc, startDate, endDate, "call", null, skippedAgentEmails);
        //Filter to agent
        if (rzap.SelectedUserID != "all")
            engList = engList.Where(w => w.base_mc_user_uid == rzap.SelectedUserID).ToList();

        //Get the OwnerList
        ownerList = HubspotApi.Owners.GetOwners(engList.Select(s => Convert.ToInt64(s.ownerID)).Distinct().ToList());
        List<string> ownerEmails = ownerList.Select(s => s.email).Distinct().ToList();
        List<n_user> agentList = rdc.n_users.Where(w => ownerEmails.Contains(w.email_address) && !skippedAgentEmails.Contains(w.email_address)).ToList();


        var query = rdc.hubspot_engagements.Where(w => w.hs_date_created.Value.Date >= startDate && w.hs_date_created.Value.Date <= endDate && w.type.ToLower() == "call");
        //Filter unwanted agents
        query = query.Where(w => !skippedAgentEmails.Contains(w.ownerEmail));

        //Filter out body in-memmory for performance
        query = query.Where(w => !(w.body ?? "").Contains("-- Contact Skipped"));
        engList = query.ToList();
    }

   

    private void LoadWeeklySalesChart()
    {
        //cvWeeklySales
        //The Chart
        SM_Charts.ChartJS ctWS = new SM_Charts.ChartJS();
        ctWS.chartType = SM_Enums.ChartType.bar.ToString();
        ctWS.displayXaxisLabel = true;
        ctWS.displayYaxisLabel = true;
        ctWS.controlPrefix = cvWS.ClientID; //required if using sm_chartjs
        ctWS.canvasID = "cvWeeklySales";
        ctWS.titleText = "Weekly GP vs Goal";

        //Datasets
        ctWS.dataSetList = new List<SM_Charts.chartJsDataSet>();





        //Sales GP Dataset
        //Get a list of All Weeks by year from Date Range
        List<Tuple<int, int>> listYearsAndWeeks = new List<Tuple<int, int>>();
        for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
        {

            int year = date.Year;
            int week = Tools.Dates.GetWeekNumberFromDate(date.Date);
            Tuple<int, int> t = Tuple.Create(year, week);
            if (!listYearsAndWeeks.Contains(t))
                listYearsAndWeeks.Add(t);

        }

        //Group the Data by Date at first, we'll use this to get the year and week.
        var query = saleList.GroupBy(g => new { g.OrderDate.Date }, (key, group) => new
        {
            //Inspector = key.inspector,
            Year = key.Date.Year,
            Week = Tools.Dates.GetWeekNumberFromDate(key.Date),
            MonthName = key.Date.Month,
            GP = group.Sum(s => s.GP)

        }); //.OrderBy(o => o.Year).ThenBy(o => o.Week);


        //Using the complete list of Years and Weeks, create GP for each by pulling data from above query
        foreach (Tuple<int, int> tup in listYearsAndWeeks)
        {
            int Year = tup.Item1;
            int Week = tup.Item2;
            double GP = 0;
            if (query.Where(w => w.Year == Year && w.Week == Week).Any())
                GP = query.Where(w => w.Year == Year && w.Week == Week).Sum(ss => ss.GP);
            WeeklySalesDataSet wds = new WeeklySalesDataSet();
            wds.Year = Year;
            wds.Week = Week;
            wds.GP = GP;
            weeklySalesData.Add(wds);
        }

        //Group the Data by year, then week, and get sum of GP.  Order the result by Year and Week.
        var perWeekChartQuery = weeklySalesData.GroupBy(x => new { x.Year, x.Week }, (key, group) => new
        {
            Year = key.Year,
            Week = key.Week,
            GP = group.Sum(s => s.GP)

        }).OrderBy(o => o.Year).ThenBy(o => o.Week);

        List<string> perWeekData = perWeekChartQuery.Select(s => s.GP.ToString()).ToList();

        //Dataset - Per Week GP      
        SM_Charts.chartJsDataSet dsPerWeekGP = new SM_Charts.chartJsDataSet();
        dsPerWeekGP.data = perWeekData;
        dsPerWeekGP.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsPerWeekGP.dataSetLabel = "Gross Profit";
        dsPerWeekGP.useDataLabelsPlugin = true;
        dsPerWeekGP.showDataLabels = false;
        dsPerWeekGP.labelFormat = "currency";

        //Dataset - Cumulative GP      
        //Group the Data by year, then week, and get sum of GP.  Order the result by Year and Week.
        //Then subtract the weekly goal from the achieved GP.  GPData has the agent goal.
        List<string> cumulativeData = new List<string>();
        double cumGP = 0;
        double weeklyGoal = GetWeeklyGoal(); 
        foreach (var v in perWeekChartQuery)
        {
            cumGP += (v.GP - weeklyGoal);
            cumulativeData.Add(cumGP.ToString());           
        }      

       
        SM_Charts.chartJsDataSet dsCumulitaveGP = new SM_Charts.chartJsDataSet();
        dsCumulitaveGP.data = cumulativeData;
        //dsCumulitaveGP.backGroundColor = SM_Tools.Colors.smNavyRgba;
        dsCumulitaveGP.borderColor = SM_Tools.Colors.smNavyRgba;
        dsCumulitaveGP.chartType = SM_Enums.ChartType.line.ToString();
        dsCumulitaveGP.fillColors = false;
        dsCumulitaveGP.borderWidth = 3;
        dsCumulitaveGP.dataSetLabel = "Cumulative GP";
        dsCumulitaveGP.useDataLabelsPlugin = true;
        dsCumulitaveGP.showDataLabels = false;
        dsCumulitaveGP.labelFormat = "currency";
        


        //DataSet - sales Goal
        //For Each Agent in saleList, sum their monthly goal, then divide by 4 to approximate the weekly goal
        double goal = 0;
        List<string> listSalesAgentID = saleList.Select(s => s.AgentID).Distinct().ToList();
        using (RzDataContext rdc = new RzDataContext())
        {
            List<double> userGoal = rdc.n_users.Where(w => listSalesAgentID.Contains(w.unique_id)).Select(s => s.monthly_booking_goal ?? 0).ToList();
            goal = userGoal.Sum() / 4;//Divide by 4 for "weekly"
        }
        List<string> listGoals = new List<string>();
        foreach (string s in perWeekData)
            listGoals.Add(goal.ToString());
        SM_Charts.chartJsDataSet dsSalesGoal = new SM_Charts.chartJsDataSet();
        dsSalesGoal.chartType = SM_Enums.ChartType.line.ToString();
        dsSalesGoal.data = listGoals;
        dsSalesGoal.fillColors = false;
        dsSalesGoal.borderColor = SM_Tools.Colors.smOrangeRgba;
        dsSalesGoal.borderWidth = 3;
        dsSalesGoal.dataSetLabel = "Weekly Goal";
        dsSalesGoal.useDataLabelsPlugin = true;
        dsSalesGoal.showDataLabels = false;
        dsSalesGoal.labelFormat = "currency";


        //This ordering dictates what is on "top" of the chart.  Want the Line on Top
        ctWS.dataSetList.Add(dsSalesGoal);
        ctWS.dataSetList.Add(dsCumulitaveGP);
        ctWS.dataSetList.Add(dsPerWeekGP);
       

        //Chart Labels = dates of objects      
        ctWS.labels = weeklySalesData.Select(s => s.Week.ToString()).ToList();


        //Initialiaze the Chart
        cvWS.LoadChart(ctWS);
        ChartList.Add(ctWS);
    }

    private double GetWeeklyGoal()
    {
        //SO, get the monthly goal SUMmed from GPData, then divide that by 4, that's the weekly goal.  COmpare that to the actual GP.
        int monthsCount = Tools.Dates.GetMonthCount(startDate, endDate);
        double summedGoal = gpData.Sum(s => s.agentGoal);
        double monthlyGoal = summedGoal / monthsCount;
        double weeklyGoal = monthlyGoal / 4;
        return weeklyGoal;
    }

   

    private void LoadGPChart()
    {
        //The Chart
        SM_Charts.ChartJS ctGP = new SM_Charts.ChartJS();
        ctGP.chartType = SM_Enums.ChartType.bar.ToString();
        ctGP.displayXaxisLabel = true;
        ctGP.displayYaxisLabel = true;
        ctGP.controlPrefix = cvGP.ClientID; //required if using sm_chartjs
        ctGP.canvasID = "cvGP";
        ctGP.titleText = "GP Booked vs Goal";

        //Datasets
        ctGP.dataSetList = new List<SM_Charts.chartJsDataSet>();



        //Dataset - Goal
        SM_Charts.chartJsDataSet dsGoal = new SM_Charts.chartJsDataSet();
        dsGoal.chartType = SM_Enums.ChartType.line.ToString();
        dsGoal.data = gpData.OrderByDescending(o => o.GP).Select(s => s.agentGoal.ToString()).ToList();
        dsGoal.fillColors = false;
        dsGoal.borderColor = SM_Tools.Colors.smOrangeRgba;
        dsGoal.borderWidth = 3;
        dsGoal.dataSetLabel = "Goal";
        dsGoal.useDataLabelsPlugin = true;
        dsGoal.showDataLabels = false;
        dsGoal.labelFormat = "currency";

        ctGP.dataSetList.Add(dsGoal);

        //Dataset - GP      
        SM_Charts.chartJsDataSet dsGP = new SM_Charts.chartJsDataSet();
        dsGP.data = gpData.OrderByDescending(o => o.GP).Select(s => s.GP.ToString()).ToList();
        dsGP.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsGP.dataSetLabel = "Weekly GP";
        dsGP.useDataLabelsPlugin = true;
        dsGP.showDataLabels = false;
        dsGP.labelFormat = "currency";
        ctGP.dataSetList.Add(dsGP);

      




        //Chart Labels = dates of objects      
        ctGP.labels = gpData.OrderByDescending(o => o.GP).Select(s => s.agentName.ToString()).ToList();

        //Initialiaze the Chart
        cvGP.LoadChart(ctGP);
        ChartList.Add(ctGP);
    }



    private void LoadNCChart()
    {
        //Query the local new customers list
        Dictionary<string, int> dictAgentNewCompanies = new Dictionary<string, int>();
        foreach (n_user u in allSalesUsers)
        {
            string Agent = u.name;
            int count = newCustomerList.Where(w => w.base_mc_user_uid == u.unique_id).Count();
            dictAgentNewCompanies.Add(Agent, count);
        }

        var sortedDict = dictAgentNewCompanies.OrderByDescending(o => o.Value);

        //var queryAgentsAndCounts = newCustomerList.GroupBy(g => g.agentname).Select(s => new
        //{
        //    Agent = s.Key,
        //    Count = s.Count()
        //}).OrderByDescending(o => o.Count);


        //The Chart
        SM_Charts.ChartJS ctNewCustomers = new SM_Charts.ChartJS();
        ctNewCustomers.chartType = SM_Enums.ChartType.bar.ToString();
        ctNewCustomers.displayXaxisLabel = true;
        ctNewCustomers.displayYaxisLabel = true;
        ctNewCustomers.controlPrefix = cvNC.ClientID;
        ctNewCustomers.canvasID = "cvNC";
        ctNewCustomers.titleText = "New Customers vs. Goal";

        //Datasets
        ctNewCustomers.dataSetList = new List<SM_Charts.chartJsDataSet>();

        //Goal
        //2 new customers per month
        int monthCount = Tools.Dates.GetMonthCount(startDate, endDate);
        string goal = (monthCount * 2).ToString();
        SM_Charts.chartJsDataSet dsGoal = new SM_Charts.chartJsDataSet();
        dsGoal.chartType = SM_Enums.ChartType.line.ToString();
        List<string> newCustomerGoal = new List<string>();
        foreach (var v in sortedDict)
            newCustomerGoal.Add(goal);
        dsGoal.data = newCustomerGoal;
        dsGoal.fillColors = false;
        dsGoal.borderColor = SM_Tools.Colors.smOrangeRgba;
        dsGoal.borderWidth = 3;
        dsGoal.dataSetLabel = "Goal";
        dsGoal.useDataLabelsPlugin = true;
        dsGoal.showDataLabels = false;
        ctNewCustomers.dataSetList.Add(dsGoal);

        //New Customers      
        SM_Charts.chartJsDataSet dsCustomerCount = new SM_Charts.chartJsDataSet();
        dsCustomerCount.data = sortedDict.Select(s => s.Value.ToString()).ToList();
        dsCustomerCount.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsCustomerCount.fillColors = true;
        dsCustomerCount.dataSetLabel = "Customer Count";
        dsCustomerCount.useDataLabelsPlugin = true;
        dsCustomerCount.showDataLabels = false;
        ctNewCustomers.dataSetList.Add(dsCustomerCount);


        //Chart Labels = dates of objects      
        ctNewCustomers.labels = sortedDict.Select(s => s.Key).ToList();

        //Initialiaze the Chart
        cvNC.LoadChart(ctNewCustomers);
        ChartList.Add(ctNewCustomers);




    }

    private void LoadPCChart()
    {

        double totalDays = (endDate - startDate).TotalDays + 1; //Same day will be Zero days, so add one.
        double weekendDays = Tools.Dates.GetCountOfWeekendDaysFromRangeofWeekDays(startDate, endDate);
        totalDays = totalDays - weekendDays;

        //Get Owners


        //Query for call count form the dataset.

        //This way I have a consistent number of items to chart, and the labels and values will match up.
        List<HubspotLogic.EngagementActivity.EngagementActivityObject> eaoList = new List<HubspotLogic.EngagementActivity.EngagementActivityObject>();
        foreach (HubspotApi.Owner o in ownerList)
        {
            HubspotLogic.EngagementActivity.EngagementActivityObject eao = new HubspotLogic.EngagementActivity.EngagementActivityObject();
            eao.ownerName = o.firstName + " " + o.lastName;
            eao.ownerID = o.ownerId;
            eao.callDuration = Math.Round((engList.Where(w => w.ownerID == o.ownerId.ToString() && w.type.ToLower() == "call").Select(s => (s.duration / 60) ?? 0).Sum() / 1000) / totalDays, 2); // Milliseconds
            eao.callCount = Math.Round((engList.Count(w => w.ownerID == o.ownerId.ToString() && w.type.ToLower() == "call")) / totalDays, 2);
            eaoList.Add(eao);
        }

        eaoList = eaoList.OrderByDescending(o => o.callCount).ToList();

        //The Chart
        SM_Charts.ChartJS ctPhoneCalls = new SM_Charts.ChartJS();
        ctPhoneCalls.chartType = SM_Enums.ChartType.bar.ToString();
        ctPhoneCalls.displayXaxisLabel = true;
        ctPhoneCalls.displayYaxisLabel = true;
        ctPhoneCalls.controlPrefix = cvPC.ClientID;
        ctPhoneCalls.canvasID = "cvPC";
        ctPhoneCalls.titleText = "Daily Average Phonecalls vs. Goal";

        //Datasets
        ctPhoneCalls.dataSetList = new List<SM_Charts.chartJsDataSet>();

        //Goal
        string goal = "60";
        SM_Charts.chartJsDataSet dsGoal = new SM_Charts.chartJsDataSet();
        dsGoal.chartType = SM_Enums.ChartType.line.ToString();
        List<string> phoneCallGoal = new List<string>();
        foreach (var v in ownerList)
            phoneCallGoal.Add(goal);
        dsGoal.data = phoneCallGoal;
        dsGoal.fillColors = false;
        dsGoal.borderColor = SM_Tools.Colors.smOrangeRgba;
        dsGoal.borderWidth = 3;
        dsGoal.dataSetLabel = "Goal";
        dsGoal.useDataLabelsPlugin = true;
        dsGoal.showDataLabels = false;
        ctPhoneCalls.dataSetList.Add(dsGoal);



        //Call Duration 
        SM_Charts.chartJsDataSet dsCallDuration = new SM_Charts.chartJsDataSet();
        dsCallDuration.data = eaoList.Select(s => s.callDuration.ToString()).ToList();
        dsCallDuration.chartType = SM_Enums.ChartType.line.ToString();
        dsCallDuration.backGroundColor = SM_Tools.Colors.smNavyRgba;
        dsCallDuration.borderColor = SM_Tools.Colors.smNavyRgba;
        dsCallDuration.fillColors = false;
        dsCallDuration.dataSetLabel = "Average Talk time";
        dsCallDuration.useDataLabelsPlugin = true;
        dsCallDuration.showDataLabels = false;
        ctPhoneCalls.dataSetList.Add(dsCallDuration);

        //Call Count    
        SM_Charts.chartJsDataSet dsPhonecallCount = new SM_Charts.chartJsDataSet();
        //dsPhonecallCount.data = queryAverageCallCount.Select(s => s.AverageCallCount.ToString()).ToList();
        dsPhonecallCount.data = eaoList.Select(s => s.callCount.ToString()).ToList();
        dsPhonecallCount.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsPhonecallCount.fillColors = true;
        dsPhonecallCount.dataSetLabel = "Average Call Count";
        dsPhonecallCount.useDataLabelsPlugin = true;
        dsPhonecallCount.showDataLabels = false;
        ctPhoneCalls.dataSetList.Add(dsPhonecallCount);


        //Chart Labels = dates of objects      
        ctPhoneCalls.labels = eaoList.Select(s => s.ownerName).ToList();

        //Initialiaze the Chart
        cvPC.LoadChart(ctPhoneCalls);
        ChartList.Add(ctPhoneCalls);
    }

    private void LoadGrids()
    {
        LoadGPGrid();
        LoadSalesDataGrid();
        LoadNCGrid();
        LoadPCGrid();
    }

    private void LoadSalesDataGrid()
    {
        smdtWeeklySales.dataSource = weeklySalesData;
        smdtWeeklySales.loadGrid();
    }

    private void LoadGPGrid()
    {
        smdtGP.dataSource = gpData;
        smdtGP.loadGrid();
    }

    private void LoadNCGrid()
    {
        smdtNC.dataSource = newCustomerList.Select(s => new
        {
            Customer = s.companyname,
            Agent = s.agentname,
            Terms = s.termsascustomer,
            Industry = s.industry_segment

        });
        smdtNC.loadGrid();
    }

    private void LoadPCGrid()
    {
        smdtPC.dataSource = engList.Select(s => new
        {

            Date = s.date_created.Value.Date.ToShortDateString(),
            Agent = s.ownerName,
            Type = s.type,
            To = s.toNumber,


        });
        smdtPC.loadGrid();
    }


}