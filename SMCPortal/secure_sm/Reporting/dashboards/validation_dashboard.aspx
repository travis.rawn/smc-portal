﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="validation_dashboard.aspx.cs" Inherits="secure_sm_Reporting_dashboards_validation_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .datatable-widget {
            margin: 15px;
        }

        .ots-labels {
            font-size: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h6>Validation Dashboard</h6>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">

    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-1">
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="updateProgress">Refresh</asp:LinkButton>
            </div>
        </div>
    </div>

    <div class="row chart-widget">

        <div class="col-sm-4 ots-labels">
            <strong>Total Lines:
                <asp:Label ID="lblTotalLines" runat="server"></asp:Label></strong><br />
            <%-- <strong>Projected Late:
                <asp:Label ID="lblProjLateLines" runat="server"></asp:Label></strong><br />
            <strong>Projected On-Time:
                <asp:Label ID="lblProjOnTimeLines" runat="server"></asp:Label></strong><br />--%>

            <%--<hr />--%>
            <strong>Total Shipped Lines:
                <asp:Label ID="lblTotalShippedLines" runat="server"></asp:Label></strong><br />
            <strong>Total Shipped On-Time:
                <asp:Label ID="lblTotalShippedOnTime" runat="server"></asp:Label></strong><br />
            <strong>Total Late:
                <asp:Label ID="lblTotalShippedLate" runat="server"></asp:Label></strong><br />
            <%--            <strong>On-Time Percent (Projected):
                <asp:Label ID="lblProjOnTimePercent" runat="server"></asp:Label></strong><br />--%>
            <strong>On-Time Percent (Shipped):
                <asp:Label ID="lblOnTimeShippedPercent" runat="server"></asp:Label></strong><br />
        </div>
        <hr />


        <div class="col-sm-4">
            <uc1:sm_chartjs runat="server" ID="chtOTDMonthly" />
        </div>
        <div class="col-sm-4">
            <uc1:sm_chartjs runat="server" ID="chtOTD" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <uc1:sm_chartjs runat="server" ID="chtHoldsPerMonth" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="chtValidationHoldReasons" />
        </div>
        <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="chtInspectionHoldReasons" />
        </div>
    </div>
    <hr />


    <%--Details--%>
    <a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
    <div id="divDetails" style="display: none; margin-bottom: 10px;">
        <div class="datatable-widget">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Hold LInes</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <uc1:sm_datatable runat="server" ID="smdtAllHolds" />
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-sm-12">
                    <h4>Late Lines</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <uc1:sm_datatable runat="server" ID="smdtOtdLate" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h4>All Lines</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <uc1:sm_datatable runat="server" ID="smdtOtdAll" />
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

