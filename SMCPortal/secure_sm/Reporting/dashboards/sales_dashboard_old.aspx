﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="sales_dashboard_old.aspx.cs" Inherits="secure_sm_Reporting_management_sales_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .refresh-button {
            font-size: 6vh;
        }

        .report {
            margin: 250px;
        }

        .chart {
            /*border-style: solid;*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
    <h6>Sales Dashboard</h6>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div class="row report">
        <div class="col-sm-12">

            <div class="well well-sm controls">
                <div class="row">
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-3">
                        <strong>Current Period</strong>
                        <div class="row">
                            <div class="col-sm-5">
                                <uc1:datetime_flatpicker runat="server" ID="dtpCurrentStart" />
                            </div>
                            <div class="col-sm-5">

                                <uc1:datetime_flatpicker runat="server" ID="dtpCurrentEnd" />
                            </div>
                        </div>
                    </div>

                    <%-- <div class="col-sm-3">
                        <strong>Previous Period</strong>
                        <div class="row">
                            <div class="col-sm-5">
                                <uc1:datetime_flatpicker runat="server" ID="dtpPreviousStart" />
                            </div>
                            <div class="col-sm-5">
                                <uc1:datetime_flatpicker runat="server" ID="dtpPreviousEnd" />
                             
                            </div>
                        </div>
                    </div>--%>
                    <div class="col-sm-2">
                        <asp:LinkButton ID="lbRefresh" runat="server">Refresh</asp:LinkButton>
                    </div>
                    <div class="col-sm-2">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <label>Current Sales:</label>
                    <asp:Label ID="lblCurrentSales" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label>Gross Profit:</label>
                    <asp:Label ID="lblCurrentGP" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label>Total Margin:</label>
                    <asp:Label ID="lblCurrentMargin" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label>GP Delta:</label>
                    <asp:Label ID="lblGpChangeAmount" runat="server" />
                    <asp:Label ID="lblGpChangePercent" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <label>Prev Sales:</label>
                    <asp:Label ID="lblPrevSales" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label>Prev Profit:</label>
                    <asp:Label ID="lblPrevGP" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label>Prev Margin:</label>
                    <asp:Label ID="lblPrevMargin" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label>Margin Delta:</label>
                    <asp:Label ID="lblMarginChangeAmount" runat="server" />
                    <asp:Label ID="lblMarginChangePecent" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    
                     <div class="chart">
                         <uc1:sm_chartjs runat="server" ID="chrtSales" />
                     </div>
                </div>
                <div class="col-sm-4">                    

                </div>
                <div class="col-sm-4">                    

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h5>Current Sales</h5>
                    <uc1:sm_datatable runat="server" ID="smdtCurrentSales" />
                    <h5>Previous Sales</h5>
                    <uc1:sm_datatable runat="server" ID="smdtPreviousSales" />
                </div>
            </div>
        </div>
    </div>
    <script>      

        //on change, subtract 1 year from corresponding previous fields
        $("#FullWidthContent_dtpCurrentStart_txtDate").change(function () {
            //Get the string representations of current start and end -1 year
            var previousStartDate = GetPreviousYearDate(this)
            //Instantiate the flatpickr and set value 
            const fp = $("#FullWidthContent_dtpPreviousStart_txtDate").flatpickr({ dateFormat: "m/d/Y", });
            fp.setDate(previousStartDate);

        });
        $("#FullWidthContent_dtpCurrentEnd_txtDate").change(function () {
            //Get the string representations of current start and end -1 year
            var previousEndDate = GetPreviousYearDate(this)
            //Instantiate the flatpickr and set value            
            const fp = $("#FullWidthContent_dtpPreviousEnd_txtDate").flatpickr({ dateFormat: "m/d/Y", });
            fp.setDate(previousEndDate);
        });

        function GetPreviousYearDate(currentDate) {

            //Get Current Start date object
            var currentDateStart = new Date(currentDate.value);
            //Subtract one year from current - returns unix time
            var previousDateUnix = currentDateStart.setFullYear(currentDateStart.getFullYear() - 1);
            //Convert Unix time to human readable
            return ConvertUnixTimeToStandardDate(previousDateUnix);
        }

    </script>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

