﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_dashboards_validation_dashboard : System.Web.UI.Page
{
    List<validation_tracking> vtList = new List<validation_tracking>();
    List<orddet_line> allLines = new List<orddet_line>();
    List<OnTimeDeliveryObject> otdList = new List<OnTimeDeliveryObject>();
    //Data queries
    List<ValidationHoldObject> validationHoldList = new List<ValidationHoldObject>();
    List<ValidationHoldObject> inspectionHoldList = new List<ValidationHoldObject>();
    List<line_process> lpList = new List<line_process>();
    List<string> otsLateReasons = new List<string>() { "Vendor Late", "Sensible Late" };

    SM_Tools tools = new SM_Tools();
    DateTime currentQuarterStart;
    DateTime currentQuarterEnd;
    DateTime previousQuarterStart;
    DateTime previousQuarterEnd;




    List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();

    class chartObject
    {
        public int Year { get; set; }
        public int MonthNumber { get; set; }
        public string MonthName { get; set; }
        public int Week { get; set; }
        public int Count { get; set; }
    }
    class ValidationHoldObject : chartObject
    {
        public string HoldType { get; set; }
        public string HoldReason { get; set; }

    }



    class OnTimeDeliveryObject : chartObject
    {
        public string IsOnTime { get; set; }
        public string LineStatus { get; set; }
        public int LineNum { get; set; }
        public string LateReason { get; set; }
        public DateTime ShipDate { get; set; }
        public DateTime ProjectedDock { get; set; }
        public string OtsComments { get; set; }
        public string Customer { get; set; }
        public string SaleNumber { get; set; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();
        LoadData();
        LoadOrderHolds();
        LoadOnTimeDelivery();
        LoadCharts();


    }

    private void LoadDates()
    {

        Tuple<DateTime, DateTime> currentQuarter = Tools.Dates.GetQuarterDateRange(DateTime.Today);
        currentQuarterStart = currentQuarter.Item1;
        currentQuarterEnd = currentQuarter.Item2;

        Tuple<DateTime, DateTime> previousQuarter = Tools.Dates.GetQuarterDateRange(currentQuarterStart.AddDays(-30));
        previousQuarterStart = previousQuarter.Item1;
        previousQuarterEnd = previousQuarter.Item2;


        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = previousQuarterStart;
            dtpEnd.SelectedDate = currentQuarterEnd;
        }
    }

    private void LoadData()
    {



        using (RzDataContext rdc = new RzDataContext())
        {
            //Holds i.e. ValidationTracking
            //List<string> invalidHoldStatus = new List<string>() { "void", "quarantined" };
            List<string> holdStages = new List<string>() { "ValidationHold", "InspectionHold" };
            vtList = rdc.validation_trackings.Where(w => (holdStages.Contains(w.new_stage) || holdStages.Contains(w.previous_stage)) && (w.date_created.Value >= dtpStart.SelectedDate && w.date_created.Value <= dtpEnd.SelectedDate)).ToList();


            //On Time Shipping       
            // List<string> invalidHoldStatus = new List<string>() { "void", "quarantined", "scrapped" };
            List<string> invalidStatus = new List<string>() { "void", "scrapped", "quarantined" };
            allLines = rdc.orddet_lines.Where(w =>
 (w.projected_dock_date.Value.Date >= dtpStart.SelectedDate.Value.Date && w.projected_dock_date.Value.Date <= dtpEnd.SelectedDate.Value.Date)
 && (w.ordernumber_sales ?? "").Length > 0
 && !w.ordernumber_sales.ToLower().Contains("cr")
 && !w.status.ToLower().Contains("rma")
 && !w.quote_line_uid.ToLower().Contains("gcat_quote_uid")
 && !invalidStatus.Contains(w.status.ToLower())
 && w.status == "Shipped"
 ).ToList();


            var qryLinesAndProcesses =
               from line in allLines
               join process in rdc.line_processes on line.unique_id equals process.orddet_line_uid
               //where post.ID == id
               select new { Line = line, Process = process };            

            //string result = "Total Join: " + qryLinesAndProcesses.Count();
            //var processIdList = qryLinesAndProcesses.Select(s => new
            //{
            //    ProcessID = s.Process.unique_id,
            //    LineID = s.Line.unique_id,
            //    ProcessKey = s.Process.orddet_line_uid,
            //    LateReason = s.Process.late_reason ?? "N/A",


            //});            

            lpList = qryLinesAndProcesses.Select(s => s.Process).ToList();



        }
    }



    private void LoadOrderHolds()
    {
        //Holds Per Month
        smdtAllHolds.dataSource = vtList.Select(s => new
        {
            Date = s.date_created.Value.ToShortDateString(),
            SO = s.ordernumber_sales,
            Prev = s.previous_stage,
            New = s.new_stage,
            Company = s.companyname,
            Agent = s.agentname,
            Type = s.new_stage,
            Reason = s.hold_reason

        });
        smdtAllHolds.sortColumn = 0;
        smdtAllHolds.sortDirection = "desc";
        smdtAllHolds.loadGrid();


    }






    private int CalculateHoldCount(List<validation_tracking> trackList, string sOID)
    {
        return trackList.Where(w => w.orderid_sales == sOID).Count();
    }

    private string CalculateTimeOnHold(List<validation_tracking> trackList, string sOID)
    {
        DateTime firstHoldDate = trackList.Where(w => w.orderid_sales == sOID).Min(m => m.date_created.Value);
        //DateTime lastHold = vtListCurrentQuarter.Where(w => w.orderid_sales == sOID).Max(m => m.date_created.Value);
        validation_tracking lastHold = trackList.Where(w => w.orderid_sales == sOID).OrderBy(o => o.date_created).First();
        //if Last Hold current Stage is ValidationComplete, then that's we use that date, if it's anything else, its still pending, so we use today's date.
        DateTime holdEndDate = DateTime.Now;
        if (lastHold.new_stage != "ValidationComplete")
            holdEndDate = trackList.Where(w => w.orderid_sales == sOID).Max(m => m.date_created.Value);
        TimeSpan span = holdEndDate - firstHoldDate;



        double totalDays = span.Days;
        double totalHours = span.Hours;
        double totalMinutes = span.Minutes;
        double totalSeconds = span.Seconds;
        string ret = totalDays + ":" + totalHours + ":" + totalMinutes + ":" + totalSeconds;
        return ret;

    }



    private void LoadOnTimeDelivery()
    {
        otdList = new List<OnTimeDeliveryObject>();


        foreach (orddet_line l in allLines)
        {
            OnTimeDeliveryObject o = new OnTimeDeliveryObject();
            o.Year = l.projected_dock_date.Value.Year;
            o.MonthNumber = l.projected_dock_date.Value.Month;
            o.MonthName = Tools.Dates.GetMonthNameAbbreviated(l.projected_dock_date.Value.Month);
            o.LineStatus = l.status;
            o.Customer = l.customer_name;
            o.ShipDate = l.ship_date_actual.Value;// < new DateTime(1900, 02, 02) ? DateTime.Now.Date : l.ship_date_actual.Value.Date;
            o.ProjectedDock = l.projected_dock_date.Value.Date;
            o.OtsComments = l.internalcomment;
            o.IsOnTime = GetOnTimeStatus(l);
            o.SaleNumber = l.ordernumber_sales.Trim().ToUpper();
            o.LineNum = l.linecode_sales.Value;
            o.LateReason = GetLateReason(l);
            otdList.Add(o);
        }

        //Per Jen 2022, in the Chart, we only want to consider ourselves late if it's Sensible or Vendor late.  Customer late is not our fault, therefor onTime.
        foreach (OnTimeDeliveryObject o in otdList)
        {
            if (o.IsOnTime == "Late" && o.LateReason == "Customer Late")
                o.IsOnTime = "OnTime";
        }


        //Totals
        var totalLineCount = otdList.Count();
        var totalProjOnTimeCount = otdList.Where(w => w.IsOnTime == "OnTime").Count();
        var totaProjLineCount = otdList.Where(w => w.IsOnTime == "Late").Count();
        lblTotalLines.Text = totalLineCount.ToString();
        lblTotalLines.Style.Add("color", SM_Tools.Colors.smNavyRgba);
        //lblProjateLines.Text = totalLateLineCount.ToString();
        //lblProjLateLines.Style.Add("color", SM_Tools.Colors.smOrangeRgba);
        //lblProjOnTimeLines.Text = totalOnTimeCount.ToString();
        //lblProjOnTimeLines.Style.Add("color", SM_Tools.Colors.smGreenRgba);

        //Shipped
        var totalShippedCount = otdList.Where(w => w.LineStatus.ToLower() == "shipped").Count();
        var totalShippedOnTimeCount = otdList.Where(w => w.IsOnTime == "OnTime" && w.LineStatus.ToLower() == "shipped").Count();
        var totalShippedLateCount = otdList.Where(w => w.IsOnTime == "Late" && w.LineStatus.ToLower() == "shipped").Count();
        lblTotalShippedLines.Text = totalShippedCount.ToString();
        lblTotalShippedLines.Style.Add("color", SM_Tools.Colors.smNavyRgba);
        lblTotalShippedLate.Text = totalShippedLateCount.ToString();
        lblTotalShippedLate.Style.Add("color", SM_Tools.Colors.smOrangeRgba);
        lblTotalShippedOnTime.Text = totalShippedOnTimeCount.ToString();
        lblTotalShippedOnTime.Style.Add("color", SM_Tools.Colors.smGreenRgba);

        ////Total Projected On Time = TotalShipped-TotalLate / TotalShipped )
        //decimal projOnTimeTotalPercent = (totalOnTimeCount / (decimal)totalLineCount);
        //lblProjOnTimePercent.Text = projOnTimeTotalPercent.ToString("P");
        //lblProjOnTimePercent.Style.Add("color", SM_Tools.Colors.smOrangeRgba);
        //lblProjOnTimePercent.Style.Add("font-weight", "bolder");
        //lblProjOnTimePercent.Visible = false;


        //Total Shipped On Time = TotalShipped-TotalLate / TotalShipped )        
        decimal onTimeShippedPercent = (totalShippedOnTimeCount / (decimal)totalShippedCount);
        lblOnTimeShippedPercent.Text = onTimeShippedPercent.ToString("P");
        lblOnTimeShippedPercent.Style.Add("color", SM_Tools.Colors.smGreenRgba);
        lblOnTimeShippedPercent.Style.Add("font-weight", "bolder");



        //tools.HandleResult("success", "OTD Report: <br /> All: "+allLineCount+" <br /> On Time: " + onTimeLineCount + " <br /> Late: " + lateLineCount + "");

        smdtOtdAll.dataSource = otdList.Select(s => new
        {
            //s.IsOnTime,
            ProjDock = s.ProjectedDock.ToShortDateString(),
            s.Customer,
            s.SaleNumber,
            s.LineStatus,
            s.LineNum,
            ShipDate = s.ShipDate.ToShortDateString(),
            s.LateReason,
            s.OtsComments
        });
        smdtOtdAll.sortColumn = 0;
        smdtOtdAll.sortDirection = "asc";
        smdtOtdAll.loadGrid();


        smdtOtdLate.dataSource = otdList.Where(w => w.IsOnTime != "OnTime" || w.LateReason == "Customer Late").Select(s => new
        {
            //s.IsOnTime,
            ProjDock = s.ProjectedDock.ToShortDateString(),
            s.Customer,
            s.SaleNumber,
            s.LineStatus,
            s.LineNum,
            ShipDate = s.ShipDate.ToShortDateString(),
            s.LateReason,
            s.OtsComments
        });
        smdtOtdLate.sortColumn = 0;
        smdtOtdLate.sortDirection = "asc";
        smdtOtdLate.loadGrid();

    }

    private string GetLateReason(orddet_line l)
    {
        string ret = "Unknown";
        line_process lp = lpList.Where(w => w.orddet_line_uid == l.unique_id).FirstOrDefault();
        if (lp != null)
            if (!string.IsNullOrEmpty(lp.late_reason))
                ret = lp.late_reason.Trim();
        return ret;
    }

    private string GetOnTimeStatus(orddet_line l)
    {

        string ret = "OnTime";
        //if we don't have a ship date, but today is past the projected dock, then late.
        if (l.ship_date_actual.Value <= new DateTime(1900, 01, 01) || l.projected_dock_date.Value <= new DateTime(1900, 01, 01))
        {
            if (DateTime.Today.Date > l.projected_dock_date.Value.Date)
                ret = "Late";
        }

        else if (l.ship_date_actual.Value <= new DateTime(1901, 01, 01))
        {
            if (DateTime.Today.Date > l.projected_dock_date.Value.Date)
                ret = "Late";
        }
        //default - is projected dock earlier in time than ship_date_actual
        else if (l.projected_dock_date.Value.Date < l.ship_date_actual.Value.Date)
            ret = "Late";
        return ret;

    }


    private void LoadCharts()
    {

        LoadOnTimeDeliveryChart();
        LoadHoldsChart();
        //Load all the chart data
        SM_Charts smr = new SM_Charts();
        smr.LoadAllCharts(Page, ChartList);

    }
    private void LoadHoldsChart()
    {




        validationHoldList = vtList.Where(w => w.new_stage == "ValidationHold" && w.hold_reason.Trim().ToLower() != "n/a").GroupBy(g => new { g.date_created.Value }).Select(s => new ValidationHoldObject
        {
            Year = s.Key.Value.Year,
            Week = Tools.Dates.GetWeekNumberFromDate(s.Key.Value),
            HoldType = "ValidationHold",
            HoldReason = s.Max(ss => ss.hold_reason)
        }).ToList();

        var vhData = validationHoldList.GroupBy(g => new { g.Year, g.Week }).Select(s => new { Year = s.Key.Year, Week = s.Key.Week, Count = s.Count().ToString() }).OrderBy(o => o.Year).ThenBy(o => o.Week).ToList();




        inspectionHoldList = vtList.Where(w => w.new_stage == "InspectionHold" && w.hold_reason.Trim().ToLower() != "n/a").GroupBy(g => new { g.date_created.Value }).Select(s => new ValidationHoldObject
        {
            Year = s.Key.Value.Year,
            Week = Tools.Dates.GetWeekNumberFromDate(s.Key.Value),
            HoldType = "ValidationHold",
            HoldReason = s.Max(ss => ss.hold_reason)
        }).ToList();

        var ihData = inspectionHoldList.GroupBy(g => new { g.Year, g.Week }).Select(s => new { Year = s.Key.Year, Week = s.Key.Week, Count = s.Count().ToString() }).OrderBy(o => o.Year).ThenBy(o => o.Week).ToList();




        //Validation vs Inspections Chart
        SM_Charts.ChartJS ctHolds = new SM_Charts.ChartJS();
        ctHolds.dataSetList = new List<SM_Charts.chartJsDataSet>();
        ctHolds.chartType = SM_Enums.ChartType.bar.ToString();
        ctHolds.stacked = true;
        ctHolds.displayXaxisLabel = true;
        ctHolds.displayYaxisLabel = true;
        ctHolds.controlPrefix = chtHoldsPerMonth.ClientID; //required if using sm_chartjs
        ctHolds.canvasID = "cvHoldsPerMonth";
        ctHolds.titleText = "Holds per Week";
        ctHolds.canvasHeight = 60;

        //Datasets
        //ValidationHolds Dataset
        SM_Charts.chartJsDataSet dsValidationHolds = new SM_Charts.chartJsDataSet();
        dsValidationHolds.data = vhData.Select(s => s.Count).ToList();
        dsValidationHolds.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsValidationHolds.dataSetLabel = "Validation Holds";
        //dsValidationHolds.borderColor = new List<string>() { SM_Tools.Colors.smWhiteRgba };
        dsValidationHolds.useDataLabelsPlugin = true;
        dsValidationHolds.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsValidationHolds.showDataLabels = true;
        ctHolds.dataSetList.Add(dsValidationHolds);

        //InspectionHolds Dataset
        SM_Charts.chartJsDataSet dsInspectionHolds = new SM_Charts.chartJsDataSet();
        dsInspectionHolds.data = ihData.Select(s => s.Count).ToList(); ;
        dsInspectionHolds.backGroundColor = SM_Tools.Colors.smOrangeRgba;
        dsInspectionHolds.dataSetLabel = "Inspection Holds";
        //dsInspectionHolds.borderColor = new List<string>() { SM_Tools.Colors.smWhiteRgba };
        dsInspectionHolds.useDataLabelsPlugin = true;
        dsInspectionHolds.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsInspectionHolds.showDataLabels = true;
        ctHolds.dataSetList.Add(dsInspectionHolds);



        List<string> validationHoldLabels = vhData.Select(s => s.Week.ToString()).ToList();
        List<string> inspectionHoldLabels = ihData.Select(s => s.Week.ToString()).ToList();
        List<string> chartLabelsList = validationHoldLabels.Union(inspectionHoldLabels).Distinct().ToList();
        ctHolds.labels = chartLabelsList;

        //Load the html elements of the control (labels, etc)
        chtHoldsPerMonth.LoadChart(ctHolds);
        ChartList.Add(ctHolds);


        //Chart - Validation Hold Reasons
        //Validation vs Inspections Chart
        SM_Charts.ChartJS ctValidationHoldReasons = new SM_Charts.ChartJS();
        ctValidationHoldReasons.dataSetList = new List<SM_Charts.chartJsDataSet>();
        ctValidationHoldReasons.chartType = SM_Enums.ChartType.bar.ToString();
        ctValidationHoldReasons.displayXaxisLabel = true;
        ctValidationHoldReasons.displayYaxisLabel = true;
        ctValidationHoldReasons.controlPrefix = chtValidationHoldReasons.ClientID; //required if using sm_chartjs
        ctValidationHoldReasons.canvasID = "cvValidationHoldReasons";
        ctValidationHoldReasons.titleText = "Validation Hold Reasons";
        ctValidationHoldReasons.canvasHeight = 120;

        //Datasets
        //Validation HoldReasons Dataset
        var vhReasonData = validationHoldList.GroupBy(g => g.HoldReason).Where(w => w.Count() > 2).Select(s => new { Reason = s.Key, Count = s.Count() }).OrderByDescending(o => o.Count).ToList();

        SM_Charts.chartJsDataSet dsValidationHoldReasons = new SM_Charts.chartJsDataSet();
        dsValidationHoldReasons.data = vhReasonData.Select(s => s.Count.ToString()).ToList();
        dsValidationHoldReasons.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsValidationHoldReasons.dataSetLabel = "Validation Hold Reasons";
        //dsValidationHolds.borderColor = new List<string>() { SM_Tools.Colors.smWhiteRgba };
        dsValidationHoldReasons.useDataLabelsPlugin = true;
        dsValidationHoldReasons.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsValidationHoldReasons.showDataLabels = true;
        ctValidationHoldReasons.dataSetList.Add(dsValidationHoldReasons);
        chtValidationHoldReasons.LoadChart(ctValidationHoldReasons);
        ctValidationHoldReasons.labels = vhReasonData.Select(s => s.Reason.ToString()).ToList();
        ChartList.Add(ctValidationHoldReasons);


        //Chart - Inspection Hold Reasons
        SM_Charts.ChartJS ctInspectionHoldReasons = new SM_Charts.ChartJS();
        ctInspectionHoldReasons.dataSetList = new List<SM_Charts.chartJsDataSet>();
        ctInspectionHoldReasons.chartType = SM_Enums.ChartType.bar.ToString();
        ctInspectionHoldReasons.displayXaxisLabel = true;
        ctInspectionHoldReasons.displayYaxisLabel = true;
        ctInspectionHoldReasons.controlPrefix = chtInspectionHoldReasons.ClientID; //required if using sm_chartjs
        ctInspectionHoldReasons.canvasID = "cvInspectionHoldReasons";
        ctInspectionHoldReasons.titleText = "Inspection Hold Reasons";
        ctInspectionHoldReasons.canvasHeight = 120;

        //Datasets
        //Validation HoldReasons Dataset

        var ihReasonData = inspectionHoldList.GroupBy(g => g.HoldReason).Select(s => new { Reason = s.Key, Count = s.Count() }).OrderByDescending(o => o.Count).ToList();

        SM_Charts.chartJsDataSet dsInspectionHoldReasons = new SM_Charts.chartJsDataSet();
        dsInspectionHoldReasons.data = ihReasonData.Select(s => s.Count.ToString()).ToList();
        dsInspectionHoldReasons.backGroundColor = SM_Tools.Colors.smOrangeRgba;
        dsInspectionHoldReasons.dataSetLabel = "Inspection Hold Reasons";
        //dsValidationHolds.borderColor = new List<string>() { SM_Tools.Colors.smWhiteRgba };
        dsInspectionHoldReasons.useDataLabelsPlugin = true;
        dsInspectionHoldReasons.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsInspectionHoldReasons.showDataLabels = true;
        ctInspectionHoldReasons.dataSetList.Add(dsInspectionHoldReasons);
        chtInspectionHoldReasons.LoadChart(ctInspectionHoldReasons);
        ctInspectionHoldReasons.labels = ihReasonData.Select(s => s.Reason.ToString()).ToList();
        ChartList.Add(ctInspectionHoldReasons);





    }


    private void LoadOnTimeDeliveryChart()
    {
        LoadOtsPieChart();
        LoadOtsMonthlyPercentChart();




    }

    private void LoadOtsMonthlyPercentChart()
    {


        //The Data, percent of OTD by Month / Year
        var otdMontPercentData = otdList.GroupBy(g => new { g.Year, g.MonthNumber }).Select(s => new
        {
            MonthTotal = s.Count(),
            MonthOnTime = s.Where(w => w.IsOnTime == "OnTime").Count(),
            Pct = Math.Round(((decimal)s.Where(w => w.IsOnTime == "OnTime").Count() / s.Count()) * 100, 2),
            s.Key.Year,
            MonthName = s.Max(m => m.MonthName),
            s.Key.MonthNumber
        }).OrderBy(o => o.Year).ThenBy(o => o.MonthNumber).ToList();

        //List of percentages grouped and ordered by year and month.
        var chartData = otdMontPercentData.OrderBy(o => o.Year).ThenBy(o => o.MonthNumber).Select(s => s.Pct.ToString()).ToList();




        //The Chart
        SM_Charts.ChartJS ctOtdMonthly = new SM_Charts.ChartJS();
        ctOtdMonthly.chartType = SM_Enums.ChartType.bar.ToString();
        ctOtdMonthly.displayXaxisLabel = true;
        ctOtdMonthly.displayYaxisLabel = true;
        ctOtdMonthly.controlPrefix = chtOTDMonthly.ClientID; //required if using sm_chartjs
        ctOtdMonthly.canvasID = "cvOtdMonthly";
        ctOtdMonthly.titleText = "Monthly On Time Delivery";
        //ctOtdMonthly.informationalText = "Count Validation Holds and Inspection holds as percent of lines created."
        //This ensures the scale goes from 0 to 100
        ctOtdMonthly.yAxisMin = 0;
        ctOtdMonthly.yAxisMax = 100;



        //Datasets
        ctOtdMonthly.dataSetList = new List<SM_Charts.chartJsDataSet>();

        //On Time Percent Dataset
        SM_Charts.chartJsDataSet dsOnTimePercent = new SM_Charts.chartJsDataSet();
        //dsOnTimePercent.borderColor = new List<string>() { SM_Tools.Colors.smWhiteRgba};
        dsOnTimePercent.data = chartData;
        dsOnTimePercent.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsOnTimePercent.dataSetLabel = "On Time %";
        dsOnTimePercent.useDataLabelsPlugin = true;
        dsOnTimePercent.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsOnTimePercent.showDataLabels = true;
        ctOtdMonthly.dataSetList.Add(dsOnTimePercent);

        //Chart Labels = dates of objects
        List<string> ontimeLabels = otdMontPercentData.Select(s => s.Year + "-" + s.MonthName).ToList();
        List<string> lateLabels = otdMontPercentData.Select(s => s.Year + "-" + s.MonthName).ToList();
        List<string> otdLabelsList = ontimeLabels.Union(lateLabels).Distinct().ToList();
        ctOtdMonthly.labels = otdLabelsList;

        //Initialiaze the Chart
        chtOTDMonthly.LoadChart(ctOtdMonthly);
        ChartList.Add(ctOtdMonthly);

    }

    private void LoadOtsPieChart()
    {
        //Pie / Aggreagate Chart
        SM_Charts.ChartJS ctOtd = new SM_Charts.ChartJS();
        ctOtd.chartType = SM_Enums.ChartType.pie.ToString();
        ctOtd.displayXaxisLabel = false;
        ctOtd.displayYaxisLabel = false;
        ctOtd.controlPrefix = chtOTD.ClientID; //required if using sm_chartjs
        ctOtd.canvasID = "cvOtd";
        ctOtd.titleText = "On Time Delivery Percent";
        ctOtd.dataSetList = new List<SM_Charts.chartJsDataSet>();
        //ctOtd.informationalText = "The measurement of On Time Shipping is based on projected dock date vs actual ship date.  On Time is when the ship date is less than or equal to the projected dock date.";


        //Datasets       
        //For a Pie Chart, the Dataset should reflect the count of On Time vs Late
        SM_Charts.chartJsDataSet allDataSetList = new SM_Charts.chartJsDataSet();
        List<string> dataSetColors = new List<string>() { SM_Tools.Colors.smOrangeRgba, SM_Tools.Colors.smGreenRgba };

        //The Data, percent of OTD by Month / Year
        //.Where(w => otsLateReasons.Contains(w.LateReason))

        var query = otdList.Where(w => w.IsOnTime.ToLower() != "OnTime");



        var otdPercentData = otdList.GroupBy(g => g.IsOnTime).Select(s => new
        {
            Count = s.Count().ToString(),
            IsOnTime = s.Max(m => m.IsOnTime)
        }).OrderBy(o => o.IsOnTime).ToList();

        //List of percentages grouped and ordered by year and month.
        var chartData = otdPercentData.OrderBy(o => o.IsOnTime).Select(s => s.Count.ToString()).ToList();
        allDataSetList.data = chartData;
        allDataSetList.backGroundColorList = dataSetColors;

        //Chart Labels and coloring
        allDataSetList.useDataLabelsPlugin = true;
        allDataSetList.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        allDataSetList.showDataLabels = true;


        ctOtd.dataSetList.Add(allDataSetList);
        List<string> otdLabels = otdList.OrderBy(o => o.IsOnTime).Select(s => s.IsOnTime).Distinct().ToList();
        ctOtd.labels = otdLabels;

        //Initialiaze the Pie Chart
        chtOTD.LoadChart(ctOtd);
        ChartList.Add(ctOtd);

    }
}