﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="sales_dashboard.aspx.cs" Inherits="secure_sm_Reporting_dashboards_sales_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h6>Sales Dashboard</h6>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">

    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-2">
                <uc1:RzAgentPicker runat="server" ID="rzap" />
            </div>
            <div class="col-sm-1">
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="updateProgress">Refresh</asp:LinkButton>
            </div>
        </div>
         <div class="row">
            <div class="col-sm-2">
                <uc1:sm_checkbox runat="server" ID="cbLoadGPData"  isChecked="true"  theText="GP Data"/>
            </div>
            <div class="col-sm-2">
                       <uc1:sm_checkbox runat="server" ID="cbLoadEngagementData" isChecked="true" theText="Phone Data"/>
            </div>
            <div class="col-sm-2">
                      <uc1:sm_checkbox runat="server" ID="cbLoadNewCustomerData" isChecked="true"  theText="New Customer Data"/>
            </div>          
        </div>
        <div class="row">
            <div class="col-sm-12">
                <strong>Related Reports:</strong>
                <br />
                <a href="../sales/engagement_activity.aspx" target="_blank">Engagement Activity Report</a>
                <br />
                <a href="../sales/engagement_performance.aspx" target="_blank">Engagement Performance Report</a>

            </div>
        </div>
    </div>
    <div class="row chart-widget">
        <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="cvGP" />
        </div>
          <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="cvWS" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="cvPC" />
        </div>
      
        <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="cvNC" />
        </div>

    </div>
     <div class="row">
        <div class="col-sm-6">
            <uc1:sm_chartjs runat="server" ID="cvQTS" />
        </div>    
    </div>
    <%--Details--%>
    <a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
    <div id="divDetails" style="display: none; margin-bottom: 10px;">
        <div class="datatable-widget">
            <div class="row">
                <div class="col-sm-12">
                    <strong>Gross Profit Data</strong><br />
                    <uc1:sm_datatable runat="server" ID="smdtGP" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <strong>Weekly Sales Data</strong><br />
                    <uc1:sm_datatable runat="server" ID="smdtWeeklySales" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <strong>New Customer Data</strong><br />
                    <uc1:sm_datatable runat="server" ID="smdtNC" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <strong>Phone Activity Data</strong><br />
                    <uc1:sm_datatable runat="server" ID="smdtPC" />
                </div>
            </div>

        </div>
    </div>



</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

