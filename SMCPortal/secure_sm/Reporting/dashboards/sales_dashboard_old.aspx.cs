﻿using ASP;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class secure_sm_Reporting_management_sales_dashboard : System.Web.UI.Page


{
    List<ordhed_sale> currentSalesList = new List<ordhed_sale>();
    //List<ordhed_sale> previousSalesList = new List<ordhed_sale>();
    protected void Page_Load(object sender, EventArgs e)
    {
        //dtpPreviousStart.Enabled = false;
        //dtpPreviousEnd.Enabled = false;
        if (!Page.IsPostBack)
            LoadDates();
        LoadData();
    }

    private void LoadData()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            currentSalesList = rdc.ordhed_sales.Where(w => w.date_created >= dtpCurrentStart.SelectedDate.Value && w.date_created <= dtpCurrentEnd.SelectedDate.Value && w.isvoid.Value != true).ToList();
            //previousSalesList = rdc.ordhed_sales.Where(w => w.date_created >= dtpCurrentStart.SelectedDate.Value.AddYears(-1) && w.date_created <= dtpCurrentEnd.SelectedDate.Value.AddYears(-1) && w.isvoid.Value != true).ToList();

            LoadLabels();
            LoadTables();
            LoadChart();


        }

    }

    private void LoadChart()
    {
        //chrtSales
        //The Chart
        

        SM_Charts.chartJsDataSet currentSalesDataset = new SM_Charts.chartJsDataSet();

        //SM_Charts.chartJsDataSet previousSalesDataset = new SM_Charts.chartJsDataSet();
        //#EE2738 = sensible red
        //#243746 = sensible blue/black

        //Current Period Dataset
        currentSalesDataset.data = currentSalesList.GroupBy(g => g.orderdate.Value.Month).OrderBy(o => o.Key).Select(s => s.Sum(ss => ss.gross_profit.Value).ToString()).ToList();
       
        //currentSalesDataset.dataSetLabel = GetTooltipYears(dtpCurrentStart.SelectedDate.Value, dtpCurrentEnd.SelectedDate.Value);
        currentSalesDataset.fillColors = false;
        //currentSalesDataset.fixedTooltipPointColor = smNavyColor;
        currentSalesDataset.backGroundColor = SM_Tools.Colors.smGreenRgba;
        //currentSalesDataset.borderColor = new List<string>() { smNavyColor };
        currentSalesDataset.labelFormat = "currency";
        //currentSalesDataset.ToolTipAfterBodyFormat = "currency";
        //currentSalesDataset.tooltipAfterLabelFormat = "currency";
        currentSalesDataset.useDataLabelsPlugin = true;
        currentSalesDataset.showDataLabels = false;

        ////Previous Period Dataset
        //previousSalesDataset.data = previousSalesList.GroupBy(g => g.orderdate.Value.Month).OrderBy(o => o.Key).Select(s => s.Sum(ss => ss.gross_profit.Value).ToString()).ToList();
        //previousSalesDataset.dataSetLabel = GetTooltipYears(dtpPreviousStart.SelectedDate.Value, dtpPreviousEnd.SelectedDate.Value);
        //previousSalesDataset.fillColors = false;
        //previousSalesDataset.fixedTooltipPointColor = smRedColor;
        //previousSalesDataset.backGroundColor = new List<string>() { smRedColor };
        //previousSalesDataset.borderColor = new List<string>() { smRedColor };
        //previousSalesDataset.labelFormat = "currency";
        ////previousSalesDataset.ToolTipAfterBodyFormat = "currency";
        ////previousSalesDataset.tooltipAfterLabelFormat = "currency";
        //previousSalesDataset.useDataLabelsPlugin = true;
        //previousSalesDataset.showDataLabels = false;


        SM_Charts.ChartJS chrtObjectSales = new SM_Charts.ChartJS();
        chrtObjectSales.chartType = SM_Enums.ChartType.bar.ToString();
        chrtObjectSales.yAxisLabelFormat = "currency";
        chrtObjectSales.displayXaxisLabel = true;
        chrtObjectSales.displayYaxisLabel = true;

        chrtObjectSales.controlPrefix = chrtSales.ClientID; //required if using sm_chartjs
        chrtObjectSales.canvasID = "cvMonthlySales";
        chrtObjectSales.titleText = "Chart: Current sales compared to previous period.";
        chrtObjectSales.informationalText = "Gross Profit by Month";
        //chrtObjectSales.canvasHeight = 90;

        //Datasets and Labels
        // Instantiate the dataset list, cannot be null when added to.
        chrtObjectSales.dataSetList = new List<SM_Charts.chartJsDataSet>();
        chrtObjectSales.dataSetList.Add(currentSalesDataset);
        //chrtObjectSales.dataSetList.Add(previousSalesDataset);
        chrtObjectSales.labels = BuildLabels();

        //Load the html elements of the control (labels, etc)
        chrtSales.LoadChart(chrtObjectSales);


        //Load all the chart data
        SM_Charts smr = new SM_Charts();
        smr.LoadAllCharts(Page, new List<SM_Charts.ChartJS>() { chrtObjectSales });
    }

    //private string GetTooltipYears(DateTime date1, DateTime date2)
    //{
    //    List<int> distinctYears = new List<int>();
    //    distinctYears.Add(date1.Year);
    //    distinctYears.Add(date2.Year);
    //    distinctYears = distinctYears.Distinct().ToList();
    //    string ret = "";
    //    if (distinctYears.Count == 1)
    //        ret = distinctYears.First().ToString();
    //    else
    //    {
    //        int year1 = distinctYears.Min();
    //        int year2 = distinctYears.Max();
    //        ret = year1 + "-" + year2;
    //    }
    //    return ret;
    //}

    private List<string> BuildLabels()
    {
        //    Dictionary<int, string> dictMonths = currentSalesList.Select(s => new
        //    {
        //        //monthNumber = s.orderdate.Value.Month,
        //        monthName = s.orderdate.Value.Month.ToString("MMMM")
        //    })
        //        //.Distinct()
        //        .ToDictionary(t => t.monthNumber, t => t.monthName);

        
        List<string> monthNames = currentSalesList.OrderBy(o => o.orderdate.Value.Month).Select(s => s.orderdate.Value.ToString("MMM")).ToList();
        return monthNames;
    }

    private void LoadLabels()
    {
        //Current
        double currentSales = currentSalesList.Sum(s => s.ordertotal.Value);
        double currentGP = currentSalesList.Sum(s => s.gross_profit.Value);
        double currentMargin = CalculateMargin(currentSales, currentGP);
        lblCurrentSales.Text = currentSales.ToString("C");
        lblCurrentGP.Text = currentGP.ToString("C"); ;
        lblCurrentMargin.Text = currentMargin.ToString("P"); ;
        ////Previous
        //double previousSales = previousSalesList.Sum(s => s.ordertotal.Value);
        //double previousGP = previousSalesList.Sum(s => s.gross_profit.Value);
        //double previousMargin = CalculateMargin(previousSales, previousGP);
        //lblPrevSales.Text = previousSales.ToString("C"); ;
        //lblPrevGP.Text = previousGP.ToString("C"); ;
        //lblPrevMargin.Text = previousMargin.ToString("P"); ;

        ////GP Change
        //double gpChangeAmount = (currentGP - previousGP);
        //lblGpChangeAmount.Text = gpChangeAmount.ToString("C");
        //if (gpChangeAmount > 0)
        //{
        //    lblGpChangeAmount.ForeColor = System.Drawing.Color.Green;
        //    lblGpChangePercent.ForeColor = System.Drawing.Color.Green;
        //}            
        //else
        //{
        //    lblGpChangeAmount.ForeColor = System.Drawing.Color.Red;
        //    lblGpChangePercent.ForeColor = System.Drawing.Color.Red;
        //}
           
        
        ////Margin Change
        //double marginPctChange = (currentMargin - previousMargin);
        //lblMarginChangePecent.Text = marginPctChange.ToString("P");


        //if (marginPctChange > 0)
        //{
        //    lblMarginChangePecent.ForeColor = System.Drawing.Color.Green;
        //    lblMarginChangeAmount.ForeColor = System.Drawing.Color.Green;
        //}
           
        //else
        //{
        //    lblMarginChangePecent.ForeColor = System.Drawing.Color.Red;
        //    lblMarginChangeAmount.ForeColor = System.Drawing.Color.Red;
        //}
           
        
    }

    private void LoadTables()
    {
        smdtCurrentSales.dataSource = currentSalesList.Select(s => new { SO = s.ordernumber, Customer = s.companyname, Agent = s.agentname, Total = s.ordertotal, GP = s.gross_profit, Margin = CalculateMargin(s.ordertotal.Value, s.gross_profit.Value) });
        smdtCurrentSales.loadGrid();
        //smdtPreviousSales.dataSource = previousSalesList.Select(s => new { SO = s.ordernumber, Customer = s.companyname, Agent = s.agentname, Total = s.ordertotal, GP = s.gross_profit, Margin = CalculateMargin(s.ordertotal.Value, s.gross_profit.Value) });
        //smdtPreviousSales.loadGrid();
    }

    private double CalculateMargin(double ordertotal, double gross_profit)
    {
        //cals = ((gross_profit /total_sale ) / total_sale) * 100;
        return (gross_profit / ordertotal);
        //throw new NotImplementedException();
    }

    private void LoadDates()
    {
        dtpCurrentStart.SelectedDate = DateTime.Today.AddMonths(-3);
        dtpCurrentEnd.SelectedDate = DateTime.Today;
        //lblPreviousYearStart.InnerHtml = dtpCurrentStart.SelectedDate.Value.AddYears(-1).ToString("MM/dd/yyyy");
        //lblPreviousYearEnd.InnerHtml = dtpCurrentEnd.SelectedDate.Value.AddYears(-1).ToString("MM/dd/yyyy");
        //dtpPreviousStart.SelectedDate = dtpCurrentStart.SelectedDate.Value.AddYears(-1);
        //dtpPreviousEnd.SelectedDate = dtpCurrentEnd.SelectedDate.Value.AddYears(-1);
    }
}