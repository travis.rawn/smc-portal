﻿using SensibleDAL.dbml;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_dashboards_inspection_dashboard : System.Web.UI.Page
{
    List<insp_head> currentRangeInspectionsList = new List<insp_head>();
    //List<insp_head> previousRangeInspectionsList = new List<insp_head>();
    //DateTime currentRangeStart;
    //DateTime currentRangeEnd;
    //DateTime previousRangeStart;
    //DateTime previousRangeEnd;
    //string currentRangeText;
    //string previousRangeText;

    //DateTime startDate = DateTime.Now.AddMonths(-6);
    DateTime startDate = new DateTime(2021, 01, 01);
    DateTime endDate = DateTime.Now.AddMonths(1);

    SM_Tools tools = new SM_Tools();
    List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();
    internal string smNavyColor = SM_Tools.Colors.smNavyRgba;
    internal string smRedColor = SM_Tools.Colors.smRedRgba;
    internal string smGreenColor = SM_Tools.Colors.smGreenRgba;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();
        LoadData();
        LoadBackLog();
        LoadGrids();
        LoadCharts();
        //Load all the chart data
        SM_Charts smr = new SM_Charts();
        smr.LoadAllCharts(Page, ChartList);

    }

    private void LoadDates()
    {
        //Tuple<DateTime, DateTime> currentQuarterRange;
        //Tuple<DateTime, DateTime> previousRange;
        //Current Range is what previous range is based on.
        if (!Page.IsPostBack)
        {
            //Current Range, default 1 year prior to current quarter
            //currentQuarterRange = tools.GetQuarterDateRange(DateTime.Now.Date.AddMonths(-3));
            //currentRangeStart = DateTime.Now.AddYears(-1);//.Item1;
            //currentRangeEnd = DateTime.Now.AddMonths(1);
            //Reflect the above in the datepicker

        }
        else
        {
            //currentQuarterRange = new Tuple<DateTime, DateTime>(dtpStart.SelectedDate.Value, dtpEnd.SelectedDate.Value);
            //currentRangeStart = dtpStart.SelectedDate.Value;
            //currentRangeEnd = dtpEnd.SelectedDate.Value;
            startDate = dtpStart.SelectedDate.Value;
            endDate = dtpEnd.SelectedDate.Value;

        }

        dtpStart.SelectedDate = startDate;
        dtpEnd.SelectedDate = endDate;
        //establish the previous range
        //currentQuarterRange = new Tuple<DateTime, DateTime>(currentRangeStart.AddYears(-1), currentRangeEnd.AddYears(-1));
        //previousRangeStart = currentQuarterRange.Item1;
        // previousRangeEnd = currentQuarterRange.Item2;

        //Get the text representation of the ranges.
        //currentRangeText = GetRangeText(currentRange);
        //previousRangeText = GetRangeText(previousRange);

    }



    private void LoadData()
    {

        List<string> inspectors = new List<string>() { "Chad Prestianni", "Goran Josifov", "Sean Maronpot", "Justin Heinrich" };


        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            currentRangeInspectionsList = pdc.insp_head.Where(w => (w.fullpartnumber ?? "").Length > 0 && (w.is_deleted ?? false != true) && (System.Data.Entity.DbFunctions.TruncateTime(w.date_created) >= startDate.Date && System.Data.Entity.DbFunctions.TruncateTime(w.date_created) <= endDate.Date) && inspectors.Contains(w.inspector)).ToList();
            //previousRangeInspectionsList = pdc.insp_heads.Where(w => (w.fullpartnumber ?? "").Length > 0 && (w.is_deleted ?? false != true) && (w.date_created.Value.Date >= startDate.Date && w.date_created.Value.Date <= endDate.Date) && inspectors.Contains(w.inspector)).ToList();

        }


    }

    private void LoadGrids()
    {
        //Current Quarter
        smdtCurrentRangeInspections.dataSource = currentRangeInspectionsList.Select(s => new
        {
            Date = s.date_created.Value.ToShortDateString(),
            ID = s.insp_id,
            Part = s.fullpartnumber.Trim().ToUpper(),
            Inspector = s.inspector
        });//.OrderByDescending(o => o.Date);
        smdtCurrentRangeInspections.sortColumn = 0;
        smdtCurrentRangeInspections.sortDirection = "desc";
        smdtCurrentRangeInspections.loadGrid();

        ////Previous Quarter
        //smdtPreviousRangeInspections.dataSource = previousRangeInspectionsList.Select(s => new
        //{
        //    Date = s.date_created.Value.ToShortDateString(),
        //    ID = s.insp_id,
        //    Part = s.fullpartnumber.Trim().ToUpper(),
        //    Inspector = s.inspector
        //}).OrderByDescending(o => o.Date);
        //smdtPreviousRangeInspections.sortColumn = 0;
        //smdtPreviousRangeInspections.sortDirection = "desc";
        //smdtPreviousRangeInspections.loadGrid();

        //InspectionRate
        smdtDailyInspectionRate.dataSource = currentRangeInspectionsList.GroupBy(g => new { g.inspector, g.date_created.Value.DayOfWeek, g.date_created.Value.Month, g.date_created.Value.Year, }).Select(s => new
        {
            Inspector = s.Key.inspector,
            s.Key.Year,
            MonthName = Tools.Dates.GetMonthNameAbbreviated(s.Key.Month),// + "-" + s.Key.Year.ToString().Substring(1, 2),
            s.Key.DayOfWeek,
            Count = s.Count()
        });
        smdtDailyInspectionRate.loadGrid();
    }


    private void LoadCharts()
    {
        LoadTeamChart();
        LoadPerAgentChart();
        LoadInspectionRateChart();



    }





    private void LoadInspectionRateChart()
    {


        //Chart Data Query

        //daily average per month, on fixed "Goal" scale;
        SM_Charts.chartJsDataSet dsDailyRatePerAgentPerMonth = new SM_Charts.chartJsDataSet();
        double rateGoal = 12.5;

        var totalDays = 84;
        var query = currentRangeInspectionsList.GroupBy(g => new { g.date_created.Value.Date }, (key, group) => new
            {
                //Inspector = key.inspector,
                Year = key.Date.Year,
                Month = key.Date.Month,
                Day = key.Date.Day,
                Week = Tools.Dates.GetWeekNumberFromDate(key.Date),
                MonthName = key.Date.Month,
                TotalDailyInspection = group.Count(),
                DaysInRange = totalDays,
                InspectionRate = ((decimal)group.Count() / (decimal)totalDays),

            }).OrderBy(o => o.Year).ThenBy(o => o.Month).ThenBy(o => o.Day);


        //By Week
        var chartQuery = query.GroupBy(x => new { x.Year, x.Week }, (key, group) => new
        {
            key.Year,
            key.Week,
            //MonthName = tools.GetMonthNameFromNumber(key.Month),
            AverageRate = Math.Round(group.Average(a => a.TotalDailyInspection), 2),
            //Result = group.ToList() // nested array
        }).OrderBy(o => o.Year).ThenBy(o => o.Week);

        ////DATASETS

        //Average insp/ month
        var data = chartQuery.Select(s => s.AverageRate.ToString()).ToList();
        //Labels reflect Months
        //var labels = chartQuery.Select(s => s.MonthName.ToString() + " '" + s.Year.ToString().Substring(2, 2)).ToList();
        var labels = chartQuery.Select(s => s.Week.ToString()).ToList();
        dsDailyRatePerAgentPerMonth.data = data;
        dsDailyRatePerAgentPerMonth.dataSetLabel = "AVG Daily Rate";
        dsDailyRatePerAgentPerMonth.chartType = SM_Enums.ChartType.line.ToString();
        dsDailyRatePerAgentPerMonth.fillColors = false;
        dsDailyRatePerAgentPerMonth.borderColor = smGreenColor;
        dsDailyRatePerAgentPerMonth.useDataLabelsPlugin = true;
        dsDailyRatePerAgentPerMonth.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsDailyRatePerAgentPerMonth.showDataLabels = false;
        dsDailyRatePerAgentPerMonth.tooltipPointRadius = "3";
        dsDailyRatePerAgentPerMonth.tooltipPointColor = smGreenColor;



        //Target / Goal
        //Fixed Line Daily Inspection Goal, currently 12.5%
        SM_Charts.chartJsDataSet dsDailyRatePerAgentGoal = new SM_Charts.chartJsDataSet();
        dsDailyRatePerAgentGoal.data = new List<string>();
        //foreach dataset column / month, add fixed double for the goal.
        foreach (string s in dsDailyRatePerAgentPerMonth.data)
        {
            dsDailyRatePerAgentGoal.data.Add("12.5");
        }
        dsDailyRatePerAgentGoal.chartType = SM_Enums.ChartType.line.ToString();
        dsDailyRatePerAgentGoal.dataSetLabel = "Target Rate";
        dsDailyRatePerAgentGoal.fillColors = false;
        dsDailyRatePerAgentGoal.backGroundColor = SM_Tools.Colors.smOrangeRgba;
        dsDailyRatePerAgentGoal.borderColor = SM_Tools.Colors.smOrangeRgba;
        dsDailyRatePerAgentGoal.useDataLabelsPlugin = true;
        dsDailyRatePerAgentGoal.showDataLabels = false;
        dsDailyRatePerAgentGoal.tooltipPointRadius = "0";
        dsDailyRatePerAgentGoal.tooltipHitRadius = "0";


        ////Chart Setup
        SM_Charts.ChartJS ctDailyInspectionRatePerAgentPerMonth = new SM_Charts.ChartJS();
        ctDailyInspectionRatePerAgentPerMonth.chartType = SM_Enums.ChartType.line.ToString();
        //ctDailyInspectionRatePerAgentPerMonth.stacked = true;
        ctDailyInspectionRatePerAgentPerMonth.displayXaxisLabel = true;
        ctDailyInspectionRatePerAgentPerMonth.displayYaxisLabel = true;
        ctDailyInspectionRatePerAgentPerMonth.xAxisLabelAutoskip = false;



        ctDailyInspectionRatePerAgentPerMonth.controlPrefix = chtInspectionRate.ClientID; //required if using sm_chartjs
        ctDailyInspectionRatePerAgentPerMonth.canvasID = "cvInspectionRate";
        ctDailyInspectionRatePerAgentPerMonth.titleText = "Average Daily Inspection Rate";
        //ctDailyInspectionRatePerAgentPerMonth.informationalText = "Inspections created per day vs Goal (12.5)";

        //Add Datasets to Chart
        ctDailyInspectionRatePerAgentPerMonth.dataSetList = new List<SM_Charts.chartJsDataSet>();
        ctDailyInspectionRatePerAgentPerMonth.dataSetList.Add(dsDailyRatePerAgentPerMonth);
        ctDailyInspectionRatePerAgentPerMonth.dataSetList.Add(dsDailyRatePerAgentGoal);

        //Labels        
        ctDailyInspectionRatePerAgentPerMonth.labels = labels;

        //Load the html elements of the control (labels, etc)


        chtInspectionRate.LoadChart(ctDailyInspectionRatePerAgentPerMonth);
        ChartList.Add(ctDailyInspectionRatePerAgentPerMonth);



    }

    private void LoadPerAgentChart()
    {
        SM_Charts.chartJsDataSet dsCurrent = new SM_Charts.chartJsDataSet();
        //SM_Charts.chartJsDataSet dsPrevious = new SM_Charts.chartJsDataSet();

        //Current Period Dataset
        //dsCurrent.data = currentRangeInspectionsList.GroupBy(g => g.inspector).OrderBy(o => o.Key).Select(s => s.Sum(ss => ss.insp_id).ToString()).ToList();
        var currentCountPerInspector = currentRangeInspectionsList.GroupBy(g => g.inspector).Select(s => new { Agent = s.Key, Count = s.Count().ToString() }).OrderBy(o => o.Agent);
        dsCurrent.data = currentCountPerInspector.OrderBy(o => o.Agent).Select(s => s.Count.ToString()).ToList();
        dsCurrent.dataSetLabel = "Inspections Created";
        dsCurrent.fillColors = false;
        dsCurrent.backGroundColor = SM_Tools.Colors.smOrangeRgba;
        //dsCurrent.borderColor = new List<string>() { SM_Tools.Colors.smOrangeRgba };
        dsCurrent.useDataLabelsPlugin = true;
        dsCurrent.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsCurrent.showDataLabels = true;

        ////Previous Period Dataset
        //var previousCountPerInspector = previousRangeInspectionsList.GroupBy(g => g.inspector).Select(s => new { Agent = s.Key, Count = s.Count().ToString() });
        //dsPrevious.data = previousCountPerInspector.OrderBy(o => o.Agent).Select(s => s.Count.ToString()).ToList();
        ////dsPrevious.labelOverride = "Inspections Created by Agent";
        //dsPrevious.fillColors = false;
        ////dsPrevious.fixedTooltipPointColor = smRedColor;
        //dsPrevious.backGroundColor = new List<string>() { SM_Tools.Colors.smOrangeRgba };
        //dsPrevious.borderColor = new List<string>() { SM_Tools.Colors.smOrangeRgba };
        //dsPrevious.useDataLabelsPlugin = true;
        //dsPrevious.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        //dsPrevious.showDataLabels = true;


        SM_Charts.ChartJS ctAgentOutput = new SM_Charts.ChartJS();
        ctAgentOutput.chartType = SM_Enums.ChartType.bar.ToString();
        //ctAgentOutput.stacked = true;
        ctAgentOutput.displayXaxisLabel = true;
        ctAgentOutput.displayYaxisLabel = true;

        ctAgentOutput.controlPrefix = chtAgentOutput.ClientID; //required if using sm_chartjs
        ctAgentOutput.canvasID = "cvAgentOutput";
        ctAgentOutput.titleText = "Total Inspections Created (Agent)";
        //ctAgentOutput.informationalText = "Inspections created per agent per Month";


        //Datasets and Labels
        ctAgentOutput.dataSetList = new List<SM_Charts.chartJsDataSet>();
        ctAgentOutput.dataSetList.Add(dsCurrent);
        //ctAgentOutput.dataSetList.Add(dsPrevious);

        List<string> currentInspectors = currentRangeInspectionsList.Where(w => !w.inspector.ToLower().Contains("kevin")).Select(s => s.inspector).Distinct().ToList();
        //List<string> previousInspectors = previousRangeInspectionsList.Where(w => !w.inspector.ToLower().Contains("kevin")).Select(s => s.inspector).Distinct().ToList();
        //List<string> allInspectors = currentInspectors.Union(previousInspectors).ToList();

        //Sort the agents by agent name to ensure they line up with the dataset values
        ctAgentOutput.labels = currentInspectors.OrderBy(o => o).ToList();

        //Load the html elements of the control (labels, etc)
        chtAgentOutput.LoadChart(ctAgentOutput);
        ChartList.Add(ctAgentOutput);
    }

    private void LoadTeamChart()
    {
        //Chart Settings
        SM_Charts.ChartJS ctTeamOutput = new SM_Charts.ChartJS();
        ctTeamOutput.chartType = SM_Enums.ChartType.bar.ToString();
        ctTeamOutput.displayXaxisLabel = true;
        ctTeamOutput.displayYaxisLabel = true;
        //ctTeamOutput.stacked = true;
        ctTeamOutput.controlPrefix = chtTeamOutput.ClientID; //required if using sm_chartjs
        ctTeamOutput.canvasID = "cvTeamOutput";
        ctTeamOutput.titleText = "Total Inspections Created (Team)";// + GetRangeText(currentRangeStart, currentRangeEnd);
        //ctTeamOutput.informationalText = GetRangeText(currentRangeStart, currentRangeEnd);

        //Datasets
        SM_Charts.chartJsDataSet dsCurrent = new SM_Charts.chartJsDataSet();
        SM_Charts.chartJsDataSet dsPrevious = new SM_Charts.chartJsDataSet();
        ctTeamOutput.dataSetList = new List<SM_Charts.chartJsDataSet>();
        //Current Period Dataset       
        var currentRangeInspections = currentRangeInspectionsList.GroupBy(g => new { g.date_created.Value.Date }, (key, group) => new
        {
            key.Date.Year,
            Week = Tools.Dates.GetWeekNumberFromDate(key.Date),
            InspCount = group.Count(),
        });//.OrderBy(o => o.Year).ThenBy(o => o.Week);


        var chartQuery = currentRangeInspections.GroupBy(x => new { x.Year, x.Week }, (key, group) => new
        {
            key.Year,
            key.Week,
            InspCount = group.Sum(s => s.InspCount) 
        }).OrderBy(o => o.Year).ThenBy(o => o.Week);



        dsCurrent.data = chartQuery.Select(s => s.InspCount.ToString()).ToList();
        dsCurrent.chartType = SM_Enums.ChartType.line.ToString();
        dsCurrent.dataSetLabel = "Inspections Created";
        dsCurrent.fillColors = false;
        dsCurrent.backGroundColor = SM_Tools.Colors.smGreenRgba;
        dsCurrent.borderColor = SM_Tools.Colors.smGreenRgba;
        dsCurrent.useDataLabelsPlugin = true;
        //dsCurrent.dataLablesPluginFontColor = SM_Tools.Colors.smWhiteRgba;
        dsCurrent.showDataLabels = false;
        dsCurrent.responsive = true;
        dsCurrent.tooltipPointRadius = "3";
        ctTeamOutput.dataSetList.Add(dsCurrent);

        ctTeamOutput.labels = chartQuery.Select(s => s.Week.ToString()).ToList();


        chtTeamOutput.LoadChart(ctTeamOutput);
        ChartList.Add(ctTeamOutput);
    }

    private string GetRangeText(DateTime rangeStart, DateTime rangeEnd)
    {

        string ret = "";
        //Start
        string startYear = rangeStart.Year.ToString();
        string startMonth = Tools.Dates.GetMonthNameAbbreviated(rangeStart.Month);
        string startWeek = rangeStart.DayOfWeek.ToString();
        //End
        string endYear = rangeEnd.Year.ToString();
        string endMonth = Tools.Dates.GetMonthNameAbbreviated(rangeEnd.Month);
        string endWeek = rangeEnd.DayOfWeek.ToString();
        //Example 2022-Jan        
        ret = startMonth + "-" + startYear + " to " + endMonth + "-" + endYear;
        return ret;
    }

    private void LoadBackLog()
    {
        //Backlog - number of lines in-house that are not inspected, i.e. have no inspection ID
        List<string> validQCStatus = new List<string>() { "in_house" };
        List<string> invalidStatus = new List<string>() { "scrapped", "shipped", "Void", "vendor rma shipped", "quarantined", "RMA Received" };
        List<orddet_line> backlogLines = new List<orddet_line>();
        using (RzDataContext rdc = new RzDataContext())
            backlogLines = rdc.orddet_lines.Where(w => (w.orderid_sales ?? "").Length > 0 && validQCStatus.Contains(w.qc_status) && !invalidStatus.Contains(w.status)).ToList();
        if (backlogLines.Count <= 0)
            return;

        lblBacklogLineCount.Text = "Count: " + backlogLines.Count.ToString();
        lblBackLogGP.Text = "GP: " + backlogLines.Sum(s => s.gross_profit.Value).ToString("C");

        LoadBacklogGrid(backlogLines);




    }

    private void LoadBacklogGrid(List<orddet_line> backlogLines)
    {
        smdtBackLog.dataSource = backlogLines.Select(s => new
        {
            Date = s.date_created.Value.ToShortDateString(),
            Status = s.status,
            Part = s.fullpartnumber,
            QTY = s.quantity,
            Sale = s.ordernumber_sales,
            Agent = s.seller_name
        });
        smdtBackLog.sortColumn = 0;
        smdtBackLog.sortDirection = "desc";
        smdtBackLog.loadGrid();
    }

   
}