﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="inspection_dashboard.aspx.cs" Inherits="secure_sm_Reporting_dashboards_inspection_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
        .chart-widget {
            margin: 15px;
        }

        .backlog{
            padding-left:5px;
            font-style:italic;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h6>Inspection Dashboard</h6>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <%--Report Controls--%>
    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-1">
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="updateProgress">Refresh</asp:LinkButton>
            </div>
            <div class="col-sm-7">
                <%-- Backlog Statistics--%>
                <b>Backloged Lines: </b>
                <span class="backlog">
                    <asp:Label ID="lblBacklogLineCount" runat="server"></asp:Label>
                    - 
                    <asp:Label ID="lblBackLogGP" runat="server"></asp:Label>
                </span>

            </div>

        </div>
    </div>




    <%--Charts--%>
    <div class="row chart-widget">
        <div class="col-sm-4">
            <%-- <b>Output (Total Inspections YoY)</b><br />--%>
            <uc1:sm_chartjs runat="server" ID="chtTeamOutput" CssClass="" />
        </div>
        <div class="col-sm-4">
            <%-- <b>Output (Per agent YoY)</b><br />--%>
            <uc1:sm_chartjs runat="server" ID="chtAgentOutput" />
        </div>
        <div class="col-sm-4">
            <%-- <b>Current Inspection Rate vs. Goal (12.5 / day)</b><br />--%>
            <uc1:sm_chartjs runat="server" ID="chtInspectionRate" />
        </div>
    </div>
    <hr />


    <%--Detail Grids--%>
    <a href="#divDetails" onclick="toggleElement('divDetails'); return false;" class="btn-smc btn-smc-primary btn-block">Details ...</a>
    <div id="divDetails" style="display: none; margin-bottom: 10px;">
        <div class="row">
            <div class="col-sm-12">
                <h4>Output Data</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                Current Range:
            <asp:Label ID="lblCurrentRangePeriod" runat="server"></asp:Label>
                <uc1:sm_datatable runat="server" ID="smdtCurrentRangeInspections" />
            </div>
            <div class="col-sm-6">
                Previous Range:
            <asp:Label ID="lblPreviousRangePeriod" runat="server"></asp:Label>
                <uc1:sm_datatable runat="server" ID="smdtPreviousRangeInspections" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4>BackLog Data</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <uc1:sm_datatable runat="server" ID="smdtBackLog" />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h4>Daily Inspection Rate</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <uc1:sm_datatable runat="server" ID="smdtDailyInspectionRate" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

