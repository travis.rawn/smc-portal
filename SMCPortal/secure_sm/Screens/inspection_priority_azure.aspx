﻿<%@ Page Title="Inspection Priority" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="inspection_priority_azure.aspx.cs" Inherits="secure_sm_Screens_inspection_priority" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">

    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bowlby+One+SC|Bungee+Inline|Bungee+Shade|Eater|Faster+One|Frijole|Holtwood+One+SC|Iceland|Lekton|Luckiest+Guy|Nosifer|Press+Start+2P|Racing+Sans+One|Rubik+Mono+One|Slackey|Stalinist+One|UnifrakturMaguntia" rel="stylesheet">


    <style>
        .GridTitle {
            font-size: 50px;
            text-align: center;
        }

        table.dataTable {
        }

            table.dataTable th {
                font-size: 42px;               
            }

            table.dataTable tbody tr {              
                font-size: 32px;
            }

        /*Hide Scroll Bars*/
        html {
            overflow: scroll;
            overflow-x: hidden;
        }

        ::-webkit-scrollbar {
            width: 0px; /* remove scrollbar space */
            background: transparent; /* optional: just make scrollbar invisible */
        }
    </style>
    <meta http-equiv="refresh" content="60">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cpBody" runat="Server">

    <div class="GridTitle">
        Inspection Priority
    </div>

    <asp:Label ID="lblCount" runat="server"></asp:Label>
    <div class="gridContainer">
        <uc1:sm_datatable runat="server" ID="smdtInspections" />
    </div>


</asp:Content>
