﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class whiteboard : Page
{
    RzTools rzt = new RzTools();
    RzDataContext rdc = new RzDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        string connString = System.Configuration.ConfigurationManager.ConnectionStrings["AzureConnection"].ConnectionString;
        rdc = new RzDataContext(connString);

        getQueryStringVariables();
        LoadData();
    }



    private void getQueryStringVariables()
    {
        //Total Tiles per Page / Screen

        string tiles = Tools.Strings.SanitizeInput(Request.QueryString["tiles"]);
        if (string.IsNullOrEmpty(tiles))
            tiles = "6";
        int testTiles = 0;
        if (!int.TryParse(tiles, out testTiles))
            throw new Exception(tiles + " is not a valid number.");



        //Total Tiles per Page / Screen

        string dur = Tools.Strings.SanitizeInput(Request.QueryString["dur"]);
        if (string.IsNullOrEmpty(dur))
            dur = "7000";
        int intDur = 0;
        if (!int.TryParse(dur, out intDur))
            throw new Exception(dur + " is not a valid duration.");
        if (intDur > 80000)
            intDur = 10000000;
        hfDuration.Value = intDur.ToString();

        //Total Cycles until page refresh 
        string cycleLimit = Tools.Strings.SanitizeInput(Request.QueryString["cycleLimit"]);
        if (string.IsNullOrEmpty(cycleLimit))
            cycleLimit = "3";
        int intCycleLimit = 0;
        if (!int.TryParse(cycleLimit, out intCycleLimit))
            throw new Exception(cycleLimit + " is not a valid number.");
        hfCycleLimit.Value = cycleLimit;




    }

    private void LoadData()

    {
        //List to hold Agent Data for tiles
        List<agentTile> AgentTileList = new List<agentTile>();
        DateTime start = DateTime.Today;
        DateTime end = DateTime.Today.AddDays(1);
        var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        List<string> excludedAgents = new List<string>() { "chris torrioni", "ardian zika" };

        List<string> badStatus = LineData.GetInvalid_orddet_Status();
        List<n_user> salesAgents = rzt.GetUsersForTeams(new List<string>() { "sales", "distributor sales" }).ToList();
        List<n_user> includedAgents = salesAgents.Where(w => w.show_on_sales_screen == true).ToList();
        includedAgents = includedAgents.Where(w => !excludedAgents.Contains(w.name.ToLower())).ToList();
        //Booked, Quoted, Invoiced, DataSets
        //Booked dataset is MTD, so I can query only once, then filter from there.
        List<SalesLogic.OrderQueryResult> BookedList = null;
        List<SalesLogic.OrderQueryResult> QuotedList = null;
        List<SalesLogic.OrderQueryResult> InvoicedList = null;

        BookedList = SalesLogic.GetBookedOrders(rdc, firstDayOfMonth, end, badStatus, includedAgents).ToList();
        QuotedList = SalesLogic.GetQuotedOrders(rdc, start, end, badStatus, includedAgents).ToList();
        InvoicedList = SalesLogic.GetInvoicedOrders(rdc, firstDayOfMonth, end, badStatus, includedAgents).ToList();




        var quotedQuery = QuotedList.GroupBy(g => g.AgentID).Select(s => new { AgentID = s.Key, GPQuoted = s.Sum(ss => ss.GP) });
        var invoicedQuery = InvoicedList.GroupBy(g => g.AgentID).Select(s => new { AgentID = s.Key, GPInvoiced = s.Sum(ss => ss.GP) });




        //Missing Agents
        List<string> missingAgents = (from u in rdc.n_users
                                      where !BookedList.Select(s => s.AgentName).Distinct().ToList().Contains(u.name)
                                      && includedAgents.Select(s => s.unique_id).Distinct().Contains(u.unique_id)
                                      select u.unique_id).ToList();

        //Banner Stuff
        lblBannerTitle.Text = BookedList.Where(w => w.OrderDate >= DateTime.Today).Sum(s => s.GP).ToString("C");
        lblBannerRss.Text = DateTime.Now.ToString("ddd MM/dd/yy - hh:mm tt");



        //Repeater Stuff
        //Daily Orders List for Scroller DataSet
        var dailyBookedOrders = BookedList.Where(w => w.OrderDate >= DateTime.Today).Select(s => new
        {
            AgentID = s.AgentID,
            GP = s.GP
        });


        //Total Booked for each Agent
        var dailyBookedTotals = BookedList.GroupBy(g => g.AgentName).Select(s => new
        {
            AgentID = s.Key,
            RunningTotal = s.Sum(ss => ss.Total)

        });


        //SMCPortalDataContext pdc = new SMCPortalDataContext();

        foreach (string a in BookedList.Select(s => s.AgentID).Distinct().ToList())
        {
            n_user u = rdc.n_users.Where(w => w.unique_id == a).FirstOrDefault();
            agentTile at = new agentTile();
            at.name = GetFirstName(u.name);
            at.quoted = Math.Round(quotedQuery.Where(w => w.AgentID == a).Sum(s => s.GPQuoted), 2);
            at.booked = Math.Round(BookedList.Where(w => w.AgentID == a && w.OrderDate >= DateTime.Today).Sum(s => s.Total), 2);
            at.invoiced = Math.Round(invoicedQuery.Where(w => w.AgentID == a).Sum(s => s.GPInvoiced), 2);
            at.mtd = Math.Round(BookedList.Where(w => w.AgentID == a).Sum(s => s.GP), 2);
            at.dailyBooked = GetDailyRunnignNumbers(dailyBookedOrders.Where(w => w.AgentID == a).Select(s => s.GP).ToList());
            at.totalbooked = Math.Round(dailyBookedOrders.Where(w => w.AgentID == a).Sum(s => s.GP), 2);
            at.imageUrl = LoadAgentWhiteboardImage(rdc, a);
            AgentTileList.Add(at);
        };


        foreach (string s in missingAgents)
        {
            n_user u = rdc.n_users.Where(w => w.unique_id == s).FirstOrDefault();
            agentTile at = new agentTile();
            at.name = GetFirstName(u.name);
            at.quoted = Math.Round(quotedQuery.Where(w => w.AgentID == s).Sum(ss => ss.GPQuoted), 2);
            at.booked = Math.Round(BookedList.Where(w => w.AgentName == s && w.OrderDate >= DateTime.Today).Sum(ss => ss.Total), 2);
            at.invoiced = Math.Round(invoicedQuery.Where(w => w.AgentID == s).Sum(ss => ss.GPInvoiced), 2);
            at.mtd = Math.Round(BookedList.Where(w => w.AgentName == s).Sum(ss => ss.Total), 2);
            at.dailyBooked = "0";
            at.totalbooked = 0;
            at.imageUrl = LoadAgentWhiteboardImage(rdc, s);
            AgentTileList.Add(at);
        }


        BindTilesToRepeater(AgentTileList.OrderByDescending(o => o.totalbooked).ThenByDescending(o => o.mtd).ThenBy(o => o.name).ToList());
    }

    private string GetFirstName(string name)
    {
        string fullName = name;
        var names = fullName.Split(' ');
        string firstName = names[0];
        return firstName;
    }

    private string LoadAgentWhiteboardImage(RzDataContext rdc, string agentID)
    {
        string imageUrl = "/Images/logo_flat_1.png";
        string customUrl = rdc.n_users.Where(w => w.unique_id == agentID).Select(s => s.leaderboard_image_url).FirstOrDefault();
        if (!string.IsNullOrEmpty(customUrl))
            return customUrl;
        return imageUrl;
    }

    private string GetDailyRunnignNumbers(List<double> list)
    {
        string ret = "0";
        if (list.Count > 0)
            ret = string.Join("<br />", list);
        return ret;

    }

    private void BindTilesToRepeater(List<agentTile> agentTileList)
    {
        rpTiles.DataSource = agentTileList;
        rpTiles.DataBind();

    }

    private class agentTile
    {
        public string name { get; set; }
        public string imageUrl { get; set; }
        public double quoted { get; set; }
        public double booked { get; set; }
        public double invoiced { get; set; }
        public double mtd { get; set; }
        public double totalbooked { get; set; }
        public string dailyBooked { get; set; }

    }
}