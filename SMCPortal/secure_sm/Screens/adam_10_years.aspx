﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="adam_10_years.aspx.cs" Inherits="secure_sm_Screens_make_it_rain2020" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <style>
        .title {
            font-size: 66px;
            margin: 50px;
        }

        .sub-title {
            font-size: 50px;
            font-style: italic;
        }

        body {           
            background-color: white;
            text-decoration-color:white;
            /*background-image: url('https://s.yimg.com/uu/api/res/1.2/O8mKboHmPgRbvR6zolm08Q--~B/aD0yMDQ0O3c9MzAwMDtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media.zenfs.com/en/techcrunch_350/9722bd51eebd2639badb69dd494666f6');*/
            height: 1080px;
        }

        .center-content {
            text-align: center;
            margin-top:200px; a
        }

        .center-image {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            max-width: 500px;
            max-height: 500px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .marquee {
            font-size: 56px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">

    <h1 style="text-align: center;"></h1>




    <div>


        <h1>Congrats on 10 years, Adam Langenbacher!!!!</h1>




        <img src="https://info.sensiblemicro.com/hubfs/Adam%20Money!.png" width="350" height="350">
        <img src="https://info.sensiblemicro.com/hubfs/Adam%20Anniv/20180412_193430.jpg" width="350" height="350">
        <img src="https://info.sensiblemicro.com/hubfs/Adam%20Anniv/20191031_163428.jpg" width="350" height="350">
        <img src="https://info.sensiblemicro.com/hubfs/Adam%20Anniv/DSC_0118.jpg" width="350" height="350">


        <h1>
            <marquee class="marquee" behavior="scroll" direction="left">2 Time Salesman of the year!</marquee>
            <marquee class="marquee" behavior="scroll" direction="left">12 Time Presidents Club Member!</marquee>
            <marquee class="marquee" behavior="scroll" direction="left">3 Time Member of the $100K Club</marquee>
        </h1>


    </div>
</asp:Content>

