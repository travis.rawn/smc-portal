﻿using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Screens_inspections_today_chart : System.Web.UI.Page
{
    List<SM_Charts.ChartJS> ChartList = new List<SM_Charts.ChartJS>();
    SM_Tools tools = new SM_Tools();        
    SM_Charts smr = new SM_Charts();
    SM_Quality_Logic smq = new SM_Quality_Logic();
    private class inspectionStatus
    {
        public string inspectorName { get; set; }
        public int inspectionsStarted { get; set; }
        public int inspectionsCompleted { get; set; }
        public DateTime firstInspectionOfTheDay { get; set; }
        public DateTime lastInspectionOfTheDay { get; set; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadAllCharts();
    }

    private void LoadAllCharts()
    {
        LoadBarCompletedInspectionsPerDay();
        smr.LoadAllCharts(Page, ChartList);
    }

    private void LoadBarCompletedInspectionsPerDay()
    {

        //GET the data
        //List<insp_head> headerList = pdc.insp_heads.Where(w => w.date_created >= DateTime.Today).ToList();
        List<insp_head> headerList = smq.GetActiveInspections();
        headerList = headerList.Where(w => w.date_created >= DateTime.Today && (w.fullpartnumber.Length > 0 && w.fullpartnumber != null) && w.is_demo != true).ToList();
        List<inspectionStatus> inspList = new List<inspectionStatus>();

        List<string> lstInspectorNames = headerList.Select(s => s.inspector.Trim()).Distinct().ToList();

        foreach (string s in lstInspectorNames)
        {
            inspectionStatus i = new inspectionStatus();
            i.inspectorName = s;
            i.inspectionsStarted = headerList.Count(c => c.inspector == s);
            i.inspectionsCompleted = headerList.Count(c => c.inspector == s && c.is_insp_complete == true);
            insp_head firstInspection = headerList.OrderBy(o => o.date_created).FirstOrDefault();
            if (firstInspection != null)
                i.firstInspectionOfTheDay = (DateTime)firstInspection.date_created;
            insp_head lastInspection = headerList.OrderByDescending(o => o.date_created).FirstOrDefault();
            if (lastInspection != null)
                i.lastInspectionOfTheDay = (DateTime)lastInspection.date_created;
            inspList.Add(i);
        }
        

        //Instantate a list of objects to hold the dataset(s)
        List<SM_Charts.chartJsDataSet> datasetList = new List<SM_Charts.chartJsDataSet>();
        if (lstInspectorNames.Count() <= 0)
            return;
      
        //Status and Count Dataset       
        SM_Charts.chartJsDataSet cDataSetInspectionsStarted = new SM_Charts.chartJsDataSet();
        cDataSetInspectionsStarted.data = inspList.Select(s => s.inspectionsStarted.ToString()).ToList();
        cDataSetInspectionsStarted.dataSetLabel = "Started";
        cDataSetInspectionsStarted.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSetInspectionsStarted.fillColors = true;
        cDataSetInspectionsStarted.backGroundColor = SM_Tools.Colors.smGreenRgba;        
        datasetList.Add(cDataSetInspectionsStarted);
        //Use the DataLabels plugin to print values on the actual bars
        cDataSetInspectionsStarted.useDataLabelsPlugin = true;
        cDataSetInspectionsStarted.dataLablesPluginFontSize = 90;
        cDataSetInspectionsStarted.dataLablesPluginFontColor =  "#ffffff";
        cDataSetInspectionsStarted.dataLablesPluginAlign = "center";
        cDataSetInspectionsStarted.dataLablesPluginAnchor = "center";

        //Status and Count Dataset       
        SM_Charts.chartJsDataSet cDataSetInspectionsCompleted = new SM_Charts.chartJsDataSet();
        cDataSetInspectionsCompleted.data = inspList.Select(s => s.inspectionsCompleted.ToString()).ToList();
        cDataSetInspectionsCompleted.dataSetLabel = "Completed";
        cDataSetInspectionsCompleted.chartType = SM_Enums.ChartType.bar.ToString();
        cDataSetInspectionsCompleted.fillColors = true;
        cDataSetInspectionsCompleted.backGroundColor = SM_Tools.Colors.smOrangeRgba;
        datasetList.Add(cDataSetInspectionsCompleted);
        //Use the DataLabels plugin to print values on the actual bars
        cDataSetInspectionsCompleted.useDataLabelsPlugin = true;
        cDataSetInspectionsCompleted.dataLablesPluginFontSize = 90;
        cDataSetInspectionsCompleted.dataLablesPluginFontColor = "#ffffff";
        cDataSetInspectionsCompleted.dataLablesPluginAlign = "center";
        cDataSetInspectionsCompleted.dataLablesPluginAnchor = "center";


        //Setup the Chart
        SM_Charts.ChartJS Chart1 = new SM_Charts.ChartJS();
        Chart1.chartType = SM_Enums.ChartType.bar.ToString();
        //Chart1.stacked = true;
        Chart1.labels = inspList.Select(s => s.inspectorName).ToList();        
        Chart1.dataSetList = datasetList;
        Chart1.maintainnAspectRatio = false;
        Chart1.legendLabelFontSize = 40;
        Chart1.legendLabelFontColor = "#ffffff";
        Chart1.xAxisLabelFontColor = "#ffffff";
        Chart1.xAxisLabelFontSize = 30;
        Chart1.yAxisStepSize = 1;
        Chart1.yAxisLabelFontColor = "#ffffff";
        Chart1.yAxisLabelFontSize = 20;
        

        Chart1.displayXaxisLabel = true;
        Chart1.displayYaxisLabel = true;
        Chart1.canvasID = "barDailyInspections";        
        ChartList.Add(Chart1);

    }
}