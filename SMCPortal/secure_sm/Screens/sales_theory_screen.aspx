﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="sales_theory_screen.aspx.cs" Inherits="secure_sm_Screens_sales_theory_screen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <style>
        .tileGrid {
            text-align: center;
            margin-top: 15px;
            margin-left: 5px;
            margin-right: 5px;
        }

        .tileBox {
            width: 365px;
            margin: 5px;
            /*border: white;
            border-style: solid;*/
            display: none;
            /*eliminate the bootstrap column padding*/
            padding-right: 0px;
            padding-left: 0px;
        }


        .agentData {
            height: 500px;
            margin-bottom: 10px;
        }

        .agentProfile {
            font-size: 45px;
        }

            .agentProfile img {
               height: 180px;                
            }

        .agentTotal {
            font-size: 80px;
            margin-top: -60px;
        }

        /*.calls {
            
            background: #EE273A;
        }

            .calls svg {
             
            }*/
         .bonus {
            /*background: gray;*/
            background: #EE273A;
        }

            .calls svg {
                /*icon size*/
            }


        .convos {
            /*background: darkmagenta;*/
            background: #f67e28;
        }

            .bonus svg {
                /*icon size*/
            }


        .rfqs {
            /*background: brown;*/
            background: #199B89;
        }

            .rfqs svg {
                /*icon size*/
            }

        .sales {
            background: #40CE22;
            /*background: blue;*/
        }

            .sales svg {
                /*icon size*/
            }

        .tileData {
            padding: 0px;
            font-size: 70px;
            height: 200px;
        }

            .tileData svg {
                font-size: 60px;
            }

        .infoBar {
            text-align: center;
            margin-top: 10px;
            margin-left: 15px;
            margin-right: 15px;
            /*height: 120px;*/
        }

        .infoBarTile {
            height: 120px;
            /*border: white;
            border-style: solid;*/
        }


        .infoBarTileTitle {
            font-size: 50px;
        }

        .infoBarTileText {
            font-size: 35px;
        }

        .infoBarTileTitle .pace {
            font-size: 60px;
        }

        .pace {
            padding-left: 0px;
            padding-right: 0px;
            height: 120px;
            font-size: 90px;
            /*background-image: url("/Images/pile-of-cash.jpg");*/
            color: whitesmoke;
        }
    </style>

    <meta http-equiv="refresh" content="300">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <asp:HiddenField ID="hfTileIndex" runat="server" />
    <div class="row tileGrid">
        <div class="col-sm-12">
            <asp:Repeater runat="server" ID="rpSalesTheory">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <%--begin tile--%>

                    <div id="divtileBox" runat="server" class="col-sm-2 tileBox">
                        <div class="col-sm-12 agentData" id="divAgentData" runat="server">
                            <div class="agentProfile">
                                <asp:Label ID="lblAgentName" runat="server" Text='<%# Eval("agentName") %>'></asp:Label><br />
                                <asp:Image ID="imgAgentImage" runat="server" CssClass="img img-responsive img-center" ImageUrl='<%# Eval("agentImageUrl") %>' /><br />
                            </div>
                            <div class="agentTotal">
                                <%--<label class="pointSquare-total-label">Total:</label><br />--%>
                                <i class="fas fa-star  pointSquare-total-icon"></i>
                                <%--<i class="fas fa-trophy  pointSquare-total-icon"></i>--%>
                                <br />
                                <asp:Label ID="lblAgentTotal" runat="server" Text='<%# Eval("currentPoints") %>'></asp:Label>

                            </div>
                        </div>
                        <div class="col-sm-6  tileData bonus">
                            <i runat="server" id="icoBonusIcon" class='<%# Eval("bonusIconClass") %>'></i>
                            <br />
                            <asp:Label ID="lblStatBonusCalls" CssClass="value" runat="server" Text='<%# Eval("bonusPoints") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6  tileData convos">
                            <i class="fas fa-comments pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblStatConversations" CssClass="value" runat="server" Text='<%# Eval("conversationPoints") %>'></asp:Label>
                        </div>

                        <div class="col-sm-6  tileData rfqs">
                            <i class="fas fa-list-ul  pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblStatRFQs" CssClass="value" runat="server" Text='<%# Eval("rfqPoints") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6  tileData sales">
                            <i class="fas fa-dollar-sign pointSquare-icon"></i>
                            <br />
                            <asp:Label ID="lblStatSalesGoal" CssClass="value" runat="server" Text='<%# Eval("salesGoalPoints") %>'></asp:Label>
                        </div>
                    </div>
                    <%--end tileBox--%>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="row infoBar">
        <div class="col-sm-2 infoBarTile calls">
            <div class="infoBarTileTitle">
                calls
            </div>
            <div class="infoBarTileText">
                0.5 Points
            </div>

        </div>
        <div class="col-sm-2 infoBarTile convos">

            <div class="infoBarTileTitle">
                convos
            </div>
            <div class="infoBarTileText">
                5 Points
            </div>
        </div>
        <div class="col-sm-2 infoBarTile rfqs">

            <div class="infoBarTileTitle">
                rfqs
            </div>
            <div class="infoBarTileText">
                2.5 Points
            </div>
        </div>
        <div class="col-sm-3 infoBarTile sales">

            <div class="infoBarTileTitle">
                sales goal
            </div>
            <div class="infoBarTileText">
                20 Points
            </div>
        </div>
        <div class="col-sm-3 infoBarTile pace">
            Pace:
                <asp:Label runat="server" ID="lblPace"></asp:Label>
        </div>
    </div>


    <%--Cycle Variables--%>
    <asp:HiddenField runat="server" ID="hfCycleDuration" Value="10000" />

    <script>
        //Global Variables
        var startIndex = 0;//Starting poing for slicing up array
        var takeQty = 5;
        var currentIndex = $("cpBody_hfTileIndex").val();
        var tiles = $('.tileBox');//Array of .tile class objects        
        var tileCount = tiles.length;//coun of tiles in array (agents in array) 
        var duration = $("#cpBody_hfCycleDuration").val(); //how long to show the slide.       
        var cycleLimit = 4//number of cycles before location.reload
        var currentCycle = 0;

        if (currentIndex <= 0)
            startIndex = currentIndex;
        var subset = sliceSubSet();

        //Slice Subnet by startIndex and number to take (number of tiles per screen)
        function sliceSubSet() {
            return tiles.slice(startIndex, startIndex + takeQty);
        }

        //Fade in with Delay before continuing execution.
        function fadeSubsetIn(subset) {
            subset.fadeIn(400).delay(duration);
        }

        //Fade Out, Quick
        function fadeSubsetOut(subset) {
            subset.fadeOut(400);
        }

        //Main Cycle, will run perpetually.
        $(function doCycle() {
            //Initial slice from 0,5 (Default parameters for startIndex and takeQty)
            subset = sliceSubSet();
            //Set the start Index
            startIndex += takeQty;
            //Fade Everything out, else you'll have some tiles briefly visible
            tiles.fadeOut(400);

            //When fadeout (and the delay duration are over)
            tiles.promise().done(function () {
                //Fade new subset in
                fadeSubsetIn(subset);
                //Get the next subset
                subset = sliceSubSet();
                //reset to position zero if the new subset is empty
                if (subset === undefined || subset.length == 0) {
                    startIndex = 0;
                }
                //repeat the cycle
                doCycle();
            });

        });
    </script>


</asp:Content>

