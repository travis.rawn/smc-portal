﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="make_it_rain2020.aspx.cs" Inherits="secure_sm_Screens_make_it_rain2020" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <style>
        .title {
            font-size: 66px;
            margin: 50px;
        }

        .sub-title {
            font-size: 50px;
            font-style: italic;
        }

        body {
            background-image: url('https://s.yimg.com/uu/api/res/1.2/O8mKboHmPgRbvR6zolm08Q--~B/aD0yMDQ0O3c9MzAwMDtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media.zenfs.com/en/techcrunch_350/9722bd51eebd2639badb69dd494666f6');
            height: 1080px;
        }

        .center-content {
            text-align: center;
        }

        .center-image {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            max-width: 500px;
            max-height: 500px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .marquee {
            font-size: 56px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">

    <h1 style="text-align: center;"></h1>




    <div class="center-content">


        <h1 class="title">CHRIS IS MAKING IT RAIN!!!!</h1>

        <p class="sub-title">$10 per every $1000 in GP Booked</p>

        <p class="sub-title">$50 Biggest order of the day (must $2500 GP to Qualify)</p>
        <p class="sub-title">$50 Most orders of the day (must be at least 3 to Qualify)</p>



        <img class="center-image" src="https://info.sensiblemicro.com/hubfs/sensiblemicro/Make%20it%20Rain.png" />


        <h1>
            <marquee class="marquee" behavior="scroll" direction="left">Monday Jon Biggest Order $50, Over $3k Total $80 | Phil Over $1K $10</marquee>
            <marquee class="marquee" behavior="scroll" direction="left">Tuesday Phil Biggest Order $50, Most Orders $50 Over $12k Booked Total $220 | Joe Over $1K $10 | Adam Over 2K $20</marquee>
            <marquee class="marquee" behavior="scroll" direction="left">Wednesday Jon Biggest Order $50, Over $3k Booked Total $80</marquee>
            <marquee class="marquee" behavior="scroll" direction="left">Thursday Adam Biggest Order $50, Most Orders $50 Over $10k Booked Total $200 | Phil Over $1K $10</marquee>

        </h1>

    </div>
</asp:Content>

