﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Screens_screen_cycler : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    string cycle_name;
    //int defaultDuration = 10000;
    List<string> screens_to_cycle = new List<string>();

    public int tick
    {
        get
        {
            return Convert.ToInt32(ViewState["tick"]);
        }
        set
        {
            ViewState["tick"] = value;
        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
            DoScreenCycle();
    }



    private void LoadScreensToCycle()
     {

        //Clear existing list
        screens_to_cycle.Clear();

        //Get Cycle Name from querystring
        cycle_name = Tools.Strings.SanitizeInput(Request.QueryString["name"]);
        if (string.IsNullOrEmpty(cycle_name))
            return;

        switch (cycle_name)
        {
            case "inspection":
            case "shipping":
            case "ship":
            case "shipping_inspection":
                {
                    screens_to_cycle.Add("/secure_sm/Screens/inspections_this_week_chart.aspx");
                    screens_to_cycle.Add("/secure_sm/Screens/inspections_today_chart.aspx");
                    screens_to_cycle.Add("/secure_sm/Screens/shipping_priority.aspx");
                    screens_to_cycle.Add("/secure_sm/Screens/inspection_priority.aspx");

                    break;

                }
            case "sales":
                {
                    //screens_to_cycle.Add("/secure_sm/Screens/adam_10_years.aspx?dur=20");
                    screens_to_cycle.Add("/secure_sm/Screens/hot_mfgs.aspx");
                    screens_to_cycle.Add("/secure_sm/Screens/todays_lines.aspx");
                    screens_to_cycle.Add("/secure_sm/Screens/sales_theory_screen.aspx");
                    screens_to_cycle.Add("/secure_sm/Screens/whiteboard.aspx?dur=10&cycle=1&tiles=6");


                    break;

                }
        }
        if (screens_to_cycle.Count <= 0)//default, this screen always has data to show ... debugging etc.
            screens_to_cycle.Add("/secure_sm/Screens/shipping_priority.aspx");

    }



    private void SetScreenDuration(string screenUrl)
    {   //defualt to 10 seconds if no parameter given.
        int currentDuration = 10000;
        //Check the screen_duration parameter
        string strScreen_dur = Tools.Strings.SanitizeInput(Request.QueryString["dur"]);
        if (!string.IsNullOrEmpty(strScreen_dur))
        {
            //confirm we have an integer before we overwrite the 0 index
            int i = 0;
            if (int.TryParse(strScreen_dur, out i))
                currentDuration = i * 1000;
        }
        //Assign duration to the Timer control's interval
        int customDuration = GetCustomScreenDuration(screenUrl);
        if (customDuration != 0)
            Timer1.Interval = customDuration * 1000;
        else
            Timer1.Interval = currentDuration;
    }

    private void Cycle_Screens()
    {
        //Make sure we still have a string 
        if (tick > screens_to_cycle.Count - 1)//It's a zero-based index, therefore count 1 = index 0
            tick = 0;
        //Get the screen from the screens list.
        string screen = screens_to_cycle[tick];

        //Check for screen custom duration


        //set iFrame URL
        screen_cycler.Attributes["Src"] = screen;
        SetScreenDuration(screen);

    }

    private int GetCustomScreenDuration(string screen)
    {
        //str.LastIndexOfAny(new []{',','|'});
        string url = screen;
        if (url == "/secure_sm/Screens/sales_theory_screen.aspx")
            return 30;
        return 0;
    }

    //private int GetCustomScreenDuration(string screen)
    //{
    //    //str.LastIndexOfAny(new []{',','|'});
    //    string url = screen;
    //    //variable for custom duration
    //    int customDur = 0;
    //    //Overall url length for comparison.
    //    int length = url.Length;//.Dump(" Url Length.");
    //    //Get the index of dur parameter        
    //    int index = url.LastIndexOf("dur=");//.Dump(" dur= Index");
    //    //if -1 doesn't exist
    //    if (index == -1)
    //        return 0;



    //    //Expand index 4 digits after the dur start index
    //    index = index + 4;
    //    //Get remaining characters so we don't pass out of bounds index to Substring 
    //    int remainingCharacters = length - index;
    //    //dur can be any number.  Assume less than 1000, i.e. max 4 chars from index throw error otherwise	
    //    string strCustomDur = url.Substring(index, remainingCharacters);
    //    //strip this to only numbers to eliminate remaining url chars
    //    string strCustomDurStripped = new string(strCustomDur.Where(c => char.IsDigit(c)).ToArray());
    //    //Confirm we still have a valid int
    //    if (int.TryParse(strCustomDurStripped, out customDur))
    //        return customDur * 1000;


    //    return 0;
    //}

    protected void Timer1_Tick(object sender, EventArgs e)
    {
       
        DoScreenCycle();
    }

    private void DoScreenCycle()
    {
        LoadScreensToCycle();
        if (screens_to_cycle.Count <= 0)
        {
            tools.HandleResult("success", "Please set a screen group name (?name=) and refresh duration in seconds (&dur=) if Desired: <br /><b><em>Example:</em></b><br /> <em>https://portal.sensiblemicro.com/secure_sm/Screens/screen_cycler.aspx?name=sales&dur=10</em>");
            return;
        }

        Cycle_Screens();
        tick++;
        //After showing the screen, sleep for the set duration
        //Thread.Sleep(Timer1.Interval);
    }
}