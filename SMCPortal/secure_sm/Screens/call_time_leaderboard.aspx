﻿<%@ Page Title="Call Leaderboard" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="call_time_leaderboard.aspx.cs" Inherits="secure_sm_Sales_Screens_call_time_leaderboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bowlby+One+SC|Bungee+Inline|Bungee+Shade|Eater|Faster+One|Frijole|Holtwood+One+SC|Iceland|Lekton|Luckiest+Guy|Nosifer|Press+Start+2P|Racing+Sans+One|Rubik+Mono+One|Slackey|Stalinist+One|UnifrakturMaguntia" rel="stylesheet">

    <style>
        /*Body*/
        body {
            margin-left: 5px;
            margin-top: 10px;
            display: block;
            height: 100%;
            width: 99%;
        }

        /*Wrapper*/
        .wrapper {
            height: 97vh;
            display: grid;
            grid-template-columns: 50% 50%;
            grid-gap: 10px;
            background-color: #000000;
            color: #444;
        }
        /*Box*/
        .box {
            background-color: #444;
            color: #fff;
            border-radius: 5px;
            padding: 20px;
            font-size: 45px;
        }

        .leaderBox {
        }

        /*Leader*/
        .leaderTitle {
            font-size: 55px;
            margin-bottom: 10px;
            color: goldenrod;
        }

        .leaderImage {
            display: flex;
            /*width: 50vw;*/
            width:100%;
            height: 70vh;
            align-items: center; /* horizontal */
            justify-content: center; /* vertical */
        }

            .leaderImage img {
                margin: auto;
                min-height: 400px;
                width:100%;
            }

        .leaderComment {
            font-size: 42px;
            color: goldenrod;
        }

        .scroller {
            height: 1000px;
            overflow: hidden;
        }
        /*Scroller*/
        .scrollerBox {
        }

            .scrollerBox tr {
                width: 100%;
            }

            .scrollerBox td {
                padding: 12px;
            }

        .divScrollerImage {
            /*width: 200px;
            height: 200px;
            max-height: 200px;*/
            max-width: 200px;
        }

            .divScrollerImage img {
                max-width: 700px;
                max-height: 700px;
                width: 100%;
                height: 100%;
            }

        .scrollerText {
        }

        .scrollerRowTotal {
            color: green;
            font-size: 90px;
        }

        .scrollerRowName {
        }


        #contain {
            height: 100%;
            overflow-y: hidden;
        }
    </style>




    <meta http-equiv="refresh" content="60">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">

    <asp:HiddenField ID="hfLastCheckDate" runat="server" />

    <div class="wrapper">
        <div class="box leaderbox">
            <div class="leaderTitle">
                <asp:Label ID="lblLeaderTitle" runat="server"></asp:Label>
            </div>
            <div class="leaderImage">
                <asp:Image ID="imgLeaderImage" runat="server" CssClass="img-fluid" />
            </div>
            <div class="leaderComment">
                <asp:Label ID="lblLeaderSlogan" runat="server"></asp:Label>
            </div>
        </div>
        <div id="scrollerBox" class="box scrollerBox">
            <div class="scroller" id="scroller">
                <div id="contain">
                    <asp:GridView ID="gvScroller" CssClass="scroller" runat="server" GridLines="None" ShowHeader="False" AutoGenerateColumns="false" OnRowDataBound="gvScroller_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="" ShowHeader="False">
                                <ItemTemplate>
                                    <div class="divScrollerImage">
                                        <asp:Image ID="imgLeaderImage" runat="server" CssClass="img-fluid" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="" SortExpression="Name" ItemStyle-CssClass="scrollerRowName" />
                            <asp:BoundField DataField="Total" HeaderText="" SortExpression="Total" ItemStyle-Font-Bold="true" ItemStyle-CssClass="scrollerRowTotal" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

    </div>
    <script>
        <%--  https://codepen.io/salman31/pen/dYdGLa --%>
        $(document).ready(function () {
            pageScroll();
        })
        //Global variables
        //How often to reset the scroller to top
        var scrollSpeed = 25;
        //The element that contains the Table
        var tblContainer = $("#contain");
        //The Table to be scrolled
        var tbl = $("#cpBody_gvScroller");
        //Keep track of how many times the current scroller has scrolled
        var scrollInterval = 0;
        //How many time to scroll before refreshing data
        var refreshInterval = 5;
        //ScrollTop, how many pixed from the top we have scrolled
        var scrollTop = 0;
         //ScrollHeight, Available scrolling space boundary
        var scrollHeight = 0;
         //ScrollTop, how many pixed from the top we have scrolled
        var tableHeight = 0;

        function pageScroll() {
            //Set Initial ScrollTop
            scrollTop = tblContainer.prop('scrollTop');
            //Check the ScrollInterval vs Refresh Interval to determin refresh of page
            //if (scrollInterval >= refreshInterval)
            //    location.reload();
            //Add one Pixel to scrolltop
            tblContainer.scrollTop(scrollTop + 1);
            //Actually animate the DOM elements
            //Set scrollTop of 1st p element
            $('p:nth-of-type(1)').html('scrollTop : ' + tblContainer.scrollTop);
            //Set the scrollHeight of the 2nd p element
            $('p:nth-of-type(2)').html('scrollHeight : ' + tblContainer.scrollHeight);
            //If the distance from the top is equal or greater than the total distance
            if (reachedEnd(tblContainer))
                resetScroller()
            //Start the scroll at speed "scrollSpeed".  Not sure why it's a var      
            //my_time = setTimeout('pageScroll()', scrollSpeed);
            setTimeout('pageScroll()', scrollSpeed);
        }

        function resetScroller() {
            //Increment the scroll interval
            scrollInterval++;

            //fade out out the table in prep for reset
            tbl.fadeOut("slow", function () {
                //Reset scroll position to top
                tblContainer.scrollTop(0);
                //Fade conent back in
                tbl.fadeIn("slow");

            })
        }

        function reachedEnd() {
            //Distance from current scroll point from the actual top of the content (which is increasingly hidden above the table as we scroll)
            scrollTop = tblContainer.prop('scrollTop');
            //Total Height available to scroll
            scrollHeight = tblContainer.prop('scrollHeight');
            //Height of the actual table content
            tableHeight = tblContainer.height();
            if (scrollTop == (scrollHeight - tableHeight))
                if (tableHeightExceedsScrollHeight())
                    return true;
            //if (tblContainer.prop('scrollTop') == (tblContainer.prop('scrollHeight') - 900))
            //    return true;
            return false;
        }

        function tableHeightExceedsScrollHeight() {
            if (tableHeight > scrollHeight)
                return true;
            return false;
        }

        function GetAllRowsHeight() {
            var totalHeight = 0
            $("#cpBody_gvScroller tr").each(function (i, row) {
                var $row = $(row);
                totalHeight += $row.height();
            });
            return totalHeight;
        }


    </script>
</asp:Content>

