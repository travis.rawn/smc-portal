﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_sm_Sales_Screens_monthly_goals : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    List<n_user> SalesAgentsList = new List<n_user>();
    List<RzTools.SalesGoal> GoalList = new List<RzTools.SalesGoal>();

    //Common Variables
    List<string> orderTypes = new List<string>() { "quote", "sales", "invoice" };
    string month = DateTime.Now.ToString("MMMM");
    DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
    DateTime lastDayOfMonth;
    protected void Page_Load(object sender, EventArgs e)
    {
        lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

        try
        {
            LoadSalesAgents();
            LoadData();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void LoadSalesAgents()
    {
        List<string> disallowedUsers = new List<string>() { "Chris Torrioni", "Ardian Zika" };
        SalesAgentsList = rzt.GetUsersForTeams(new List<string>() { "sales" }).Where(w => !disallowedUsers.Contains(w.name)).ToList();
    }

    private void LoadData()
    {
        if (SalesAgentsList == null)
            throw new Exception("SalesAgentsList is empty");

        string strType = Tools.Strings.SanitizeInput(Request.QueryString["type"]);
        if (!string.IsNullOrEmpty(strType))
        {
            string type = strType.ToLower();
            if (!string.IsNullOrEmpty(type))

                switch (type)
                {
                    case "quoted":
                        LoadQuoted();
                        break;
                    case "booked":
                        LoadBooked();
                        break;
                    case "invoiced":
                        LoadInvoiced();
                        break;
                    case "magic":
                        LoadMagic();
                        break;
                    case "leaderboard":
                        Loadleaderboard();
                        break;
                }
        }
        else
            LoadInvoiced();
        gvMonthlyGoals.DataBind();
    }



    private void LoadMagic()
    {
        GetGoalData("magic");
    }

    private void LoadQuoted()
    {
        GetGoalData("quote");

    }

    private void LoadBooked()
    {
        GetGoalData("sales");
    }

    private void LoadInvoiced()
    {
        GetGoalData("invoice");

    }
    private void Loadleaderboard()
    {
        GetLeaderboardData();
    }

    private void GetLeaderboardData()
    {

        int quoteWeight = 1;
        int bookedWeight = 30;
        int invoicedWeight = 50;

        string goalName = month + " Leaderboard";


        lblGoalLabel.Text = goalName;

        GoalList = (List<RzTools.SalesGoal>)rzt.GetMagicNumberCalculation2(goalName, SalesAgentsList, orderTypes, firstDayOfMonth);

        foreach (RzTools.SalesGoal s in GoalList)
        {
            GetGoalImages(s);
        }

        GetGoalText(GoalList);

        gvLeaderBoard.DataSource = GoalList.OrderByDescending(o => o.AchievedValue).Select(s => new { Agent = s.Agent, Rank = s.AchievedValue.ToString(), ImageUrl = s.ImageURL, Text = s.Text });
        gvLeaderBoard.DataBind();
        gvLeaderBoard.BackImageUrl = "/Images/Dollar_sign_white.jpg";
    }

    private void GetGoalText(List<RzTools.SalesGoal> goalList)
    {
        RewardSystem rs = new RewardSystem();
        List<string> messages = rs.GetMessage();
        List<string> messagesMotivate = rs.GetMessageMotivate();


        Random rand = new Random();
        foreach (RzTools.SalesGoal s in goalList)
        {

            string msg = "";
            int n = rand.Next(0, messages.Count);
            if (s == goalList.OrderByDescending(o => o.AchievedValue).First())
                msg = "It's gooooood to be king baby!";
            else if (s.AchievedValue >= 20)
                msg = messages.Skip(n).Take(1).First();
            else
                msg = messagesMotivate.Skip(n).Take(1).First();

            s.Text = msg;
        }
    }

    private void GetGoalData(string orderType)
    {
        switch (orderType)
        {
            case "magic":
                GetMagicData();
                break;
            case "sales":
            case "invoice":
            case "quote":
            default:
                GetMonthlySalesData(orderType);
                break;
        }

    }

    private void GetMonthlySalesData(string orderType)
    {
        string goalName = GetGoalName(orderType);
        lblGoalLabel.Text = goalName;
        using (RzDataContext rdc = new RzDataContext())
            GoalList = (from o in rdc.ordheds
                        join u in rdc.n_users on o.base_mc_user_uid equals u.unique_id
                        where o.orderdate >= DateTime.Now.AddDays(-30) && o.ordertype == orderType && SalesAgentsList.Select(s => s.unique_id).ToList().Contains(o.base_mc_user_uid)
                        select new RzTools.SalesGoal
                        {
                            Agent = u.name,
                            GoalValue = orderType == "sales" || orderType == "invoice" ? u.monthly_invoiced_goal ?? 0 : u.monthly_quote_goal ?? 0,
                            AchievedValue = o.profitamount ?? 0,
                            OrderType = orderType
                        }).ToList();

        GoalList = (GoalList.GroupBy(g => g.Agent).Select(s => new RzTools.SalesGoal
        {
            Agent = s.Key,
            GoalName = goalName,
            GoalValue = s.Max(m => m.GoalValue),
            AchievedValue = s.Sum(m => m.AchievedValue)
        })).ToList();

        GoalList = GoalList.Where(w => w.AchievedValue > 0).Select(s => new RzTools.SalesGoal
        {
            Agent = s.Agent,
            GoalName = s.GoalName,
            GoalValue = s.GoalValue,
            AchievedValue = s.AchievedValue,
            Percent = Math.Round(s.AchievedValue / s.GoalValue, 2, MidpointRounding.AwayFromZero)
        }).ToList();


        List<RzTools.SalesGoal> missing = SalesAgentsList.Where(w => !GoalList.Select(s => s.Agent).Distinct().ToList().Contains(w.name)).Select(s => new RzTools.SalesGoal
        {
            Agent = s.name,
            GoalName = goalName,
            GoalValue = Convert.ToDouble(0),
            AchievedValue = Convert.ToDouble(0),
            Percent = Convert.ToDouble(0)
        }).ToList();

        foreach (RzTools.SalesGoal s in GoalList)
        {
            GetGoalImages(s);
        }

        gvMonthlyGoals.DataSource = GoalList.OrderByDescending(o => o.Percent).Select(s => new { Agent = s.Agent, GP = s.AchievedValue.ToString("c"), Percent = s.Percent.ToString("P0"), ImageUrl = s.ImageURL });
        gvMonthlyGoals.DataBind();
    }

    private void GetMagicData()
    {
        string goalName = GetGoalName("magic");
        lblGoalLabel.Text = goalName;

        GoalList = (List<RzTools.SalesGoal>)rzt.GetMagicNumberCalculation2(goalName, SalesAgentsList, orderTypes, firstDayOfMonth);

        //Final Processing and Images
        foreach (RzTools.SalesGoal s in GoalList)
        {
            if (s.AchievedValue >= 100)
                s.AchievedValue = 100;
            GetGoalImages(s);
        }

        gvMagic.DataSource = GoalList.OrderByDescending(o => o.AchievedValue).Select(s => new { Agent = s.Agent, Magic = s.AchievedValue.ToString(), ImageUrl = s.ImageURL });
        gvMagic.DataBind();

    }

    private void GetGoalImages(RzTools.SalesGoal s)
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
            s.ImageURL = pdc.Reps.Where(w => w.FullName == s.Agent).Select(ss => ss.HSPath).FirstOrDefault() ?? @"https://sensiblemicro.com/wp-content/uploads/2016/09/Sensible-Micro-Logo_Header.png";
    }

    private string GetGoalName(string orderType)
    {
        switch (orderType)
        {
            case "sales":
                return "Booked Goal";
            case "invoice":
                return "Invoiced Goal";
            case "quote":
                return "Quoted Goal";
            case "magic":
                return "Magic Goal";
            default:
                return "No Goal Name Identified";
        }


    }

    protected void gvMagic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        foreach (GridViewRow row in gvMagic.Rows)
        {
            //Fore / Background color
            //Random Message
            double magicValue = Convert.ToDouble(row.Cells[1].Text);
            if (magicValue >= 80)
            {
                row.Cells[1].ForeColor = System.Drawing.Color.Green;

            }

            else if (magicValue >= 60)
            {
                row.Cells[1].ForeColor = System.Drawing.Color.Yellow;
            }

            else if (magicValue >= 30)
            {
                row.Cells[1].ForeColor = System.Drawing.Color.Orange;
            }


        }
    }

    protected void gvLeaderBoard_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        RewardSystem rs = new RewardSystem();
        Color rowColor = new Color();
        string rgbaColor = "";//rgba(0, 255, 0, 0.3);
        double alpha = .6;

        //Fore / Background color      
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            e.Row.BackColor = System.Drawing.Color.Transparent;
            double rank = Convert.ToDouble(e.Row.Cells[2].Text);
            Label rankLabel = (Label)e.Row.Cells[0].FindControl("lblRank");
            if (e.Row.RowIndex == 0) // Captain
            {
                rankLabel.Text = "LEGENDARY";
                rankLabel.Style.Add("font-weight", "Bold");
                rankLabel.Style.Add("font-size", "96px");
                rankLabel.Style.Add("color", "goldenrod");
                e.Row.BackColor = System.Drawing.Color.Transparent;
                //e.Row.Attributes.Add("style", "background-image: url(/Images/Hundo.jpg)");
            }
            else
            {
                if (rank >= 30)
                {
                    rankLabel.Text = "PLAYER";
                    rowColor = Color.FromArgb(50, Color.Green);
                    rgbaColor = "rgba(" + rowColor.R + "," + rowColor.G + "," + rowColor.B + "," + alpha + ")";
                    e.Row.Style.Add("background", rgbaColor);

                }

                else if (rank >= 20)
                {

                    rankLabel.Text = "HUSTLER";
                    rowColor = Color.FromArgb(50, Color.Yellow);
                    rgbaColor = "rgba(" + rowColor.R + "," + rowColor.G + "," + rowColor.B + "," + alpha + ")";
                    e.Row.Style.Add("background", rgbaColor);
                }

                else
                {
                    rankLabel.Text = "ROOKIE";
                    rowColor = Color.FromArgb(50, Color.Orange);
                    rgbaColor = "rgba(" + rowColor.R + "," + rowColor.G + "," + rowColor.B + "," + alpha + ")";
                    e.Row.Style.Add("background", rgbaColor);
                }
            }
        }





    }

}

