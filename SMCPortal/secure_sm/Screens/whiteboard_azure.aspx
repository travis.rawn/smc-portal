﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="whiteboard_azure.aspx.cs" Inherits="whiteboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bowlby+One+SC|Bungee+Inline|Bungee+Shade|Eater|Faster+One|Frijole|Holtwood+One+SC|Iceland|Lekton|Luckiest+Guy|Nosifer|Press+Start+2P|Racing+Sans+One|Rubik+Mono+One|Slackey|Stalinist+One|UnifrakturMaguntia" rel="stylesheet">

    <style>
      
        .wrapper {
            /*margin: 10px 3px 3px 13px;*/
            margin-left: 20px;
            color: whitesmoke;
            /*width: 1892px;*/
            height: auto;
            width: auto;
        }

        .parent {
            background-color: rgb(27, 28, 31);
            margin: 5px;
            /*display: flex;
            flex-wrap: wrap;*/
            /*flex: 2 0 50px;
            flex-direction: row;*/
            display: flex;
            flex-flow: row wrap;
            /*height: 479px;*/ /* Or whatever */
        }

        .banner {
            margin: 2px;
            background-color: rgb(42, 42, 42);
            color: whitesmoke;
            margin-left: 2px;
            height: 78px;
            width: 100%;
        }

            .banner img span {
                display: table-cell;
                vertical-align: middle;
            }

            .banner img {
                padding: 3px;
            }

        .banner-title {
            display: inline-block;
            font-size: 61px;
        }

        .banner-title-number {
            color: yellowgreen;
            margin-left: 30px;
        }

        .banner-rss {
            margin-left: 15px;
            display: inline-block;
            font-size: 35px;
            padding-right: 20px;
        }

        .banner-image {
            display: inline-block;
            padding-right: 5px;
        }

            .banner-image img {
                max-height: 78px;
            }


        .banner-right-label {
            display: table-cell;
            vertical-align: middle;
        }


        /* (1440x900) WXGA+ Display */
        @media screen and (max-width: 1570px) {
            /* insert styles here */
            .banner-rss {
                font-size: 25px;
                margin-bottom: 15px;
            }

            .banner-image {
                width: 30%;
                margin-top: 5px;
                /*margin-left:15px;*/
            }

            .banner-title {
                font-size: 40px;
                margin-top: 15px;
                margin-left: 15px;
            }
        }

        @media screen and (max-width: 1192px) {
            /* insert styles here */

            .banner-image {
                display: none;
            }

            .tile-data {
                display: none;
            }
        }

        @media screen and (max-width:989px) {
            /* insert styles here */

            .banner-rss {
                font-size: 20px;
                margin-top: 25px;
            }

            .tile-scroller-label {
                display: none;
            }
        }




        .tile {
            background-color: rgb(42, 42, 42);
            /*width: 100%;
            height: 100%;*/
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            margin: 2px;
            padding: 10px;
            /*flex-grow: 0;*/
            width: 33%;
            max-height: 470px;
        }

        .tile-quad {
            flex-basis: calc(50% - 10px);
        }

        .tile-triple {
            flex-basis: calc(30% - 3px);
        }

        .tile-name {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            /*font-size: 56px;*/
            /*height:180px;*/
            font-size: 48px;
            margin-right: 20px;
        }

        .tile-image {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            height: 200px;
        }

            .tile-image img {
                /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
                height: 100%;
                width: auto;
                clear: both;
            }

        .tile-data {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            margin-top: 2px;
            font-size: 28px;
            clear: both;
        }

        .tile-scroller {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            height: 370px;
            width: 100%;
            font-size: 30px;
            padding-right: 5px;
            direction: rtl;
            /*color: darkgreen;*/
            color: greenyellow;
        }

        .tile-data-numbers {
            color: greenyellow;
            margin-left: 15px;
        }

        .scroller-total {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            padding-right: 5px;
            width: 100%;
            color: darkgreen;
            font-size: 70px;
            direction: rtl;
            /*font-weight: bold;*/
            color: greenyellow;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <asp:HiddenField ID="hfTileCount" runat="server" />
    <asp:HiddenField ID="hfDuration" runat="server" />
    <asp:HiddenField ID="hfCycleLimit" runat="server" />



    <div class="wrapper">
        <div class="parent">
            <div class="banner">
                <div class="banner-title">
                    Daily Total:<asp:Label ID="lblBannerTitle" runat="server" CssClass="banner-title-number"></asp:Label>
                </div>
                <div class="pull-right">
                    <div class="banner-rss">
                        <asp:Label ID="lblBannerRss" runat="server" CssClass="banner-right-label" Text="" />
                    </div>
                    <div class="banner-image">
                        <%--           <img src="https://bestanimations.com/Holidays/Halloween/happy-halloween-starry-night-pumpkin-jack-o-lantern-animated-gif-image.gif" class="img img-center img-responsive" />--%>
                        <%--<img src="http://static.skaip.org/img/emoticons/180x180/f6fcff/turkey.gif" class="img img-center img-responsive" />--%>
                        <%-- <img src="/Images/Happy-holidays.jpg" class="img img-center img-responsive" />--%>
                        <img src="/Images/Sensible-Logo.png" class="img img-center img-responsive" />


                    </div>
                </div>
            </div>


            <%--Repeaters--%>
            <%--Child Repeater--%>
            <asp:Repeater runat="server" ID="rpTiles">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <%--begin tile--%>
                    <div class="tile tile-quad" id="tile-<%#Container.ItemIndex %>" style="display: none;">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="tile-name">
                                    <asp:Label ID="lblAgentName" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                </div>
                                <div class="tile-image">
                                    <img src='<%# Eval("imageUrl") %>' class="img img-center img-responsive" />
                                </div>
                                <div class="tile-data">
                                    Quoted Daily 
                                    <asp:Label ID="lblQuoted" runat="server" Text='<%# Eval("quoted") %>' CssClass="tile-data-numbers"></asp:Label>
                                    <br />
                                    Inv MTD
                                    <asp:Label ID="lblInvoiced" runat="server" Text='<%# Eval("invoiced") %>' CssClass="tile-data-numbers"></asp:Label>
                                    <br />
                                    Booked MTD<asp:Label ID="lblMtd" runat="server" Text='<%# Eval("mtd") %>' CssClass="tile-data-numbers"></asp:Label>

                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="tile-scroller">
                                    <asp:Label ID="lblScroller" runat="server" CssClass="tile-scroller-label" Text='<%# Eval("dailyBooked") %>'></asp:Label>
                                </div>
                                <div class="scroller-total">
                                    $<asp:Label ID="lblDailyTotal" runat="server" Text='<%# Eval("totalBooked") %>'></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--end tile--%>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
            <%--End Child Repeater--%>
            <%--End Repeaters--%>
        </div>
    </div>

    <script>

        startIndex = 0;//Starting poing for slicing up array
        tileCountPerPage = $("#cpBody_hfTileCount").val();//how many tiles per page / screen.  Max = 6
        if (tileCountPerPage > 6 || tileCountPerPage.length <= 0 || tileCountPerPage == null)
            tileCountPerPage = 6;
        //If tiles are 4 or oless, apply tile-quad
        var cssClass = "tile tile-quad";
        //else trip
        if (tileCountPerPage >= 5)
            cssClass = "tile tile-trip";


        takeQty = tileCountPerPage;//running current Take Qty as screens transition
        var tile = $('.tile');//Array of .tile class objects
        for (var i = 0; i < tile.length; ++i)
            tile[i].className = cssClass;
        //while (tile.length) {
        //    tile[0].className = cssClass;
        //}
        //tile.each(function () {tile.addClass('newClass'); });
        var totalCount = tile.length;//coun of tiles in array (agents in array) 
        var duration = $("#cpBody_hfDuration").val(); //how long to show the slide.    
        if (duration == null || duration.length <= 0)
            duration = 10;
        duration = duration * 1000;
        var cycleLimit = $("#cpBody_hfCycleLimit").val();//number of cycles before location.reload
        var currentCycle = 1;

        transition(duration);

        function transition(duration) {

            if (currentCycle > cycleLimit)
                tile.hide().promise().done(function () {
                    location.reload();
                });
            else {
                subset = tile.slice(startIndex, takeQty);
                showhideDiv(subset, duration);
                subset.promise().done(function () {
                    startIndex = +startIndex + +takeQty;//increase startIndex to next record in array
                    takeQty = +startIndex + +takeQty;//increase the take qty based on the past entry point (index)
                    if (startIndex > totalCount) {//If the startIndex is higher than count, then there are no records, reset.
                        startIndex = 0;
                        takeQty = tileCountPerPage;
                        currentCycle++;
                    }
                    transition(duration);
                });

            }

        }

        function showhideDiv(suset) {
            subset.fadeIn(400)
                .delay(duration)
                .fadeOut(400);
        }
    </script>

</asp:Content>

