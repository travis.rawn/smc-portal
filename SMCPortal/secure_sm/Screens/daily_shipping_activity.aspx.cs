﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Sales_Screens_daily_shipping_activity : System.Web.UI.Page
{
    RzDataContext rdc = new RzDataContext();
    RzTools rzt = new RzTools();
    protected void Page_Load(object sender, EventArgs e)
    {
        GetData();
    }

    private void GetData()
    {
        LoadShipped();
        LoadInProcess();
    }



    private void LoadShipped()
    {

        //Grid 1 - Things that have shipped today
        var shippedQuery = rdc.orddet_lines.Where(w => w.ship_date_actual.Value.Date >= DateTime.Today && w.status.ToLower().Contains("shipped")).Select(s => new
        {
            Part = s.fullpartnumber,
            QTY = s.quantity,
            Agent = s.seller_name,
            Customer = s.customer_name,
            Sale = s.ordernumber_sales,
            Dock = s.customer_dock_date.Value.Date.ToShortDateString(),
            QCStatus = s.qc_status,
            ShipDate = s.ship_date_actual.Value

        }).OrderBy(o => o.ShipDate).OrderBy(o => o.Sale);

        var shippedGrid = shippedQuery.Select(s => new
        {
            s.Part,
            s.QTY,
            s.Agent,
            s.Customer,
            s.Sale,
            s.Dock,
            s.ShipDate,
            s.QCStatus

        }).OrderByDescending(o => o.ShipDate).OrderBy(o => o.Sale);
        smdtInProcess.sortColumn = 6;
        smdtShipped.pageLength = 25;
        smdtShipped.allowSearch = false;
        smdtShipped.allowPaging = false;
        smdtShipped.showRecordsInfo = false;
        smdtShipped.dataSource = shippedGrid;
        smdtShipped.loadGrid(true);


        GridView gv = smdtShipped.theGridView;
        foreach (GridViewRow row in gv.Rows)
        {
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#82E36D"); //Green
            row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");//White

        }
    }


    private void LoadInProcess()
    {




        DateTime startDate = new DateTime(2016, 1, 1);
        var inProcessQuery = SensibleDAL.ShippingLogic.GetInboundPriorityLines(rdc,startDate);     
        
        
        //The Grid
        var query = inProcessQuery.Select(s => new
        {

            PartNumber = s.partNumber,
            QTY = s.qty,
            Agent = s.salesAgent,

            Customer = s.customerName,
            Vendor = s.vendorName,
            SO = s.ordernumberSales,
            PO = s.ordernumberPurchase,
            cDock = s.dockDate.Date.ToShortDateString(),
            Received = s.recievedDate.Date.ToShortDateString(),
            GP = s.grossProfit,
            //SOTerms = s.saleTerms,
            POTerms = s.poTerms,
            QCSatatus = s.qcStatus,
            //Priority =s.datePriority + s.gpPriority,
            //Priority = s.gpPriority,
            Priority = 1,
            //PriValues = rzt.GetEffectivePriorityValueString(s.cDockPriority, s.gpPriority, s.poTermsPriority),
            //PriValues = "Days: " + s.datePriority + " | " + "GP: " + s.gpPriority,
            SODate = s.orderDateSales.Date.ToShortDateString(),


        });
        smdtInProcess.sortColumn = 12;
        smdtInProcess.sortDirection = "desc";
        smdtInProcess.allowSearch = false;
        smdtInProcess.allowPaging = false;
        smdtInProcess.showRecordsInfo = false;
        smdtInProcess.dataSource = query.OrderByDescending(o => o.Priority).OrderBy(o => o.cDock).OrderBy(o => o.SO).OrderBy(o => o.PO);
        smdtInProcess.loadGrid(true);



    }


}