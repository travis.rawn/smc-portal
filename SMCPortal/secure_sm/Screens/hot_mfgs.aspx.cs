﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Screens_hot_mfgs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
        LoadGrid();
    }

    private DataTable dt { get; set; }

    private void LoadData()
    {
        //Count of distinct part's mfg per customer.  
        //I.e. if 1 customers have same part, count, but don't count multiple instances for same customer.
        dt = new DataTable();
        dt.Columns.Add("CustomerID", typeof(string));
        dt.Columns.Add("CustomerName", typeof(string));
        dt.Columns.Add("MFG", typeof(string));
        dt.Columns.Add("Part", typeof(string));
        dt.Columns.Add("Count", typeof(int));

        List<orddet_quote> quoteList = new List<orddet_quote>();

        using (RzDataContext rdc = new RzDataContext())
        {
            quoteList = rdc.orddet_quotes.Where(w => w.date_created >= DateTime.Now.AddMonths(-1) && (w.fullpartnumber).Length > 0 && w.agentname != "Phil Scott" && w.target_manufacturer.Length > 0 && w.target_manufacturer != "N/A" ).ToList();

            //From quote list, get distinct customer id
            List<string> distinctCustomerId = quoteList.Select(s => s.base_company_uid).Distinct().ToList();

            //Loop through each customer, get count of different parts (i.e. group by part number)
            foreach (String customerID in distinctCustomerId)
            {
                company c = rdc.companies.Where(w => w.unique_id == customerID).FirstOrDefault();
                if (c == null)
                    continue;
                var query = quoteList.Where(w => w.base_company_uid == customerID).GroupBy(g => new { g.fullpartnumber, g.target_manufacturer });
                foreach (var v in query)
                {
                    DataRow row = dt.NewRow();
                    row["CustomerID"] = customerID;
                    row["CustomerName"] = c.companyname;
                    row["Count"] = v.Count();
                    row["Part"] = v.Key.fullpartnumber;
                    row["Mfg"] = v.Key.target_manufacturer;
                    dt.Rows.Add(row);

                }
            }
        }




        //finalQuery.Dump();
    }

    private void LoadGrid()
    {

        //Now we want Company, MFG Per COmpany, and count of occurrences of each MFG
        var companyMfgQuery = dt.AsEnumerable().GroupBy(s => new { CustomerName = s.Field<string>("CustomerName"), MFG = s.Field<string>("Mfg"), Part = s.Field<string>("Part") }).Select(ss => new
        {
            ss.Key.CustomerName,
            ss.Key.MFG,
            ss.Key.Part,
            Count = ss.Sum(k => k.Field<int>("Count"))
        }).OrderByDescending(o => o.MFG);
        //companyMfgQuery.Dump();
        //ok, from this DataTable, count when MFG occurs more than the threshold, return that count, and the MFG name.


        //Ok now we have a count of of MFG per company and grouped by part number.  We no longer care about the "Count", only the number of occurences of a MFG in this list.

        var finalQuery = companyMfgQuery.GroupBy(g => g.MFG).Select(s => new
        {
            MFG = s.Key,
            Count = s.Count()
        }).Where(w => w.Count > 5).OrderByDescending(o => o.Count);




        smdt.dataSource = null;
        //smdt.allowPaging = true;
        smdt.allowPageLengthChange = false;
        smdt.showPageInfo = false;
        smdt.allowSearch = false;
        smdt.sortDirection = "desc";
        //Sort by Count (column 1)
        smdt.sortColumn = 1;
        
        if (finalQuery.Any())
            smdt.dataSource = finalQuery;
        smdt.loadGrid();






    }
}