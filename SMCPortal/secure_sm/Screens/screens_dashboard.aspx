﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="screens_dashboard.aspx.cs" Inherits="secure_sm_Screens_screens_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style>
        .sub-text {
            font-size: 8px;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Screen Content</h1>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-default panel-body card">

        <div class="row">
            <div class="col-sm-3">
                <strong><a href="monthly_goals.aspx?type=leaderboard&speed=25&repeat=true" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Monthly Goals </a></strong>
            </div>
            <div class="col-sm-9">
                View monthly quoted, booked, and invoiced goals.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="whiteboard.aspx?dur=10&cycle=1&tiles=6" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Whiteboard </a></strong>
            </div>
            <div class="col-sm-9">
                Sales Whiteboard. Set the "tiles" parameter to switch betwee 4 and 6 tiles per page.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="inspection_priority.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Inspection Priority </a></strong>
            </div>
            <div class="col-sm-9">
                See today's Inspection activity.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="shipping_priority.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Shipping Priority </a></strong>
            </div>
            <div class="col-sm-9">
                See today's Shipping activity.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="inspections_today_chart.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Today's Inspections Chart </a></strong>
            </div>
            <div class="col-sm-9">
                See today's Inspection activity.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="inspections_this_week_chart.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Weekly Inspections Chart </a></strong>
            </div>
            <div class="col-sm-9">
                See this week's Inspection activity.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="call_time_leaderboard.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Call Time Leaderboard </a></strong>
            </div>
            <div class="col-sm-9">
                See today's call time leaderboard.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong><a href="sales_theory_screen.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Sales Theory Screen </a></strong>
            </div>
            <div class="col-sm-9">
                Tracks 4 KPI's critical for productivity.
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <strong><a href="todays_lines.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Today's Sale lines  </a></strong>
            </div>
            <div class="col-sm-9">
                Including part, mfg, and company.  Sorted by manufacturer.
            </div>
        </div>

         <div class="row">
            <div class="col-sm-3">
                <strong><a href="hot_mfgs.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Hot Manufacturers  </a></strong>
            </div>
            <div class="col-sm-9">
                Top requested manufacturers over the past month.
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <strong><a href="screen_cycler.aspx" target="_blank"><span class="fas fa-database" style="margin-right: 15px;"></span>Screen Cycler </a></strong>
            </div>
            <div class="col-sm-9">
                Cycle screen content on an HD display.<%--the parameters "name" and "dur" to set the group of screens to cycle and the duration between screens. <span class="sub-text"><em>Ex: https://portal.sensiblemicro.com/secure_sm/Screens/screen_cycler.aspx?name=sales&dur=20  </em></span>--%>
            </div>
        </div>





    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

