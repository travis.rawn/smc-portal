﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="hot_mfgs.aspx.cs" Inherits="secure_sm_Screens_hot_mfgs" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">

    <style>

        /*Keep the sort column black*/
        .sorting_2 {
             background-color: rgb(27, 28, 31) !important;
        }

        body, html {
            background-color: rgb(27, 28, 31);
            font-family: Arial;
        }

        .GridTitle {
            font-size: 5.5vh;
            text-align: center;
        }


        table.dataTable {
        }

            table.dataTable th {
                font-size: 4vh;
                background-color: rgb(27, 28, 31) !important; /*black*/
            }

            table.dataTable tbody tr {
                background-color: rgb(27, 28, 31) !important; /*black*/
                font-size: 5vh;
            }



        /*InProcess Grid*/
        #cpBody_smdt_gvMain {
            background-color: rgb(27, 28, 31) !important; /*black*/
        }

            #cpBody_smdt_gvMain .sorting_1 {
                background-color: rgb(27, 28, 31) !important;
            }
        /*Shipped Table*/
        #cpBody_smdt_gvMain {
            /*background-color: #82E36D !important;*/ /*green*/
        }

            #cpBody_smdt_gvMain .sorting_1 {
                /*background-color: #82E36D !important;*/ /*green*/
            }

            #cpBody_smdt_gvMain tbody tr {
                /*background-color: #82E36D !important;*/ /*green*/
            }

                #cpBody_smdt_gvMain tbody tr.odd {
                    /*background-color: #82E36D !important;*/ /*green*/
                }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <div class="GridTitle">
        Hot Manufacturers
    </div>
    <uc1:sm_datatable runat="server" ID="smdt" />

    <script>



        //Global Variables.
        //The table and info variables are global, so I can check to see if any pages present. 
        var table = $('#cpBody_smdt_gvMain').DataTable();
        var info = table.page.info();
        var totalPages = info.pages;

        //Seconds between page changes
        var intervalSeconds = 8;

        //Call on load
        $(cyclePage());
        function cyclePage() {
            if (totalPages < 2) {
                //alert('Only one page!');
                return;
            }

            setInterval(function () {
                //Wrapping this way invoked the fadeOut callback function to ensure fade finishes before updating page.
                $('#cpBody_smdt_gvMain').fadeOut('fast', function () {
                    // this will be called when the element finishes fading out
                    // if selector matches multiple elements it will be called once for each
                    SetCurrentPage();
                });
            }, intervalSeconds * 1000)
        }

        function SetCurrentPage() {
            //Global vars for table and info need to invoke on each loop.
            table = $('#cpBody_smdt_gvMain').DataTable();
            info = table.page.info();

            var currentPageIndex = info.page;
            var nextPage = currentPageIndex + 1;//page is zero based, pages is not.         
            if (nextPage >= totalPages)
                nextPage = 0;
            //Change the page.            
            table.page(nextPage).draw(false);
            //fade back in.
            $('#cpBody_smdt_gvMain').fadeIn('fast');

        };

    </script>
</asp:Content>
