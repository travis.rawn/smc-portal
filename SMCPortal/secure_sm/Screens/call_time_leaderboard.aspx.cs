﻿using HubspotApis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using System.Web.UI;

public partial class secure_sm_Sales_Screens_call_time_leaderboard : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    List<HubspotApi.Owner> ownerList = new List<HubspotApi.Owner>();
    List<hubspot_engagement> rzEngagementList = new List<hubspot_engagement>();
    //DateTime LastUpdateCheck;
    List<hubspot_engagement> eList;
    CallLeader TheLeader;

    //CallLeader TheLeader;
    class CallLeader
    {
        public int totalCalls { get; set; }
        public string leaderName { get; set; }
        public string leaderText { get; set; }
        public string leaderImagePath { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            GetEngagements();
            LoadScrollerBox();
        }
        catch (Exception ex) { }

    }



    private void GetEngagements()
    {

        //Get the Date Range
        DateTime start = DateTime.Today.Date;
        //DateTime start = new DateTime(2018, 06, 01);
        //DateTime start = DateTime.Today.AddDays(1);//Tomorrow, test for null
        DateTime end = start.AddDays(1).Date;

        //Get List of leaderst to include
        //List<string> leaderBoardUsers = rdc.n_users.Where(w => w.show_on_sales_screen ?? false == true).Select(s => s.name).ToList();
        RzTools rzt = new RzTools();
        List<n_user> leaderBoardUsers = rzt.GetUsersForTeams(new List<string>() { "sales", "distributor sales" });
        List<string> leaderBoardUsersStrings = leaderBoardUsers.Select(s => s.name).ToList();
        //Get the Engagements
        using (RzDataContext rdc = new RzDataContext())
            eList = rdc.hubspot_engagements.Where(w => w.hs_date_created.Value.Date >= start
        && w.hs_date_created.Value.Date <= end
        && w.type.ToLower() == "call"
        && leaderBoardUsersStrings.Contains(w.ownerName)
        ).ToList();

    }

    private void LoadScrollerBox()
    {
        gvScroller.DataSource = null;
        gvScroller.DataBind();

        string defaultComment = "It's time to kick butt and chew bubble gum ...  and I'm all outta gum.";
        //string defaultComment = "I Rock, you know it, and you can't touch THIS!!";

        if (eList.Count == 0)
            GetEngagements();
        if (eList.Count == 0)
        {
            tools.HandleResult("fail", "No engagements found.");
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "overlayKey", "$('#standByOverlay').show(); return false;", true);

            return;
        }

        var query = eList.GroupBy(g => g.ownerName).Select(s => new
        {
            Name = s.Key,
            Total = s.Count()
        });




        //Leader Stuff;              
        TheLeader = new CallLeader();
        TheLeader.leaderName = query.OrderByDescending(o => o.Total).Select(s => s.Name).First();
        n_user LeaderUser = null;
        using (RzDataContext rdc = new RzDataContext())
            LeaderUser = rdc.n_users.Where(w => w.name == TheLeader.leaderName).FirstOrDefault();
        TheLeader.totalCalls = query.OrderByDescending(o => o.Total).Select(s => s.Total).First();
        TheLeader.leaderText = LeaderUser.leaderboard_text;
        TheLeader.leaderImagePath = SM_Global.GetLeaderboardImagePath(LeaderUser);
        lblLeaderTitle.Text = TheLeader.leaderName + ": " + TheLeader.totalCalls + " Calls";
        lblLeaderSlogan.Text = TheLeader.leaderText ?? defaultComment;
        imgLeaderImage.ImageUrl = TheLeader.leaderImagePath;



        //Bind the Grid
        gvScroller.DataSource = query.Where(w => w.Name != TheLeader.leaderName).OrderByDescending(o => o.Total);
        gvScroller.DataBind();





    }

    protected void gvScroller_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        using (RzDataContext rdc = new RzDataContext())
            foreach (GridViewRow row in gvScroller.Rows)
            {
                Image i = (Image)row.Cells[0].FindControl("imgLeaderImage");
                if (i != null)
                {
                    string agentName = row.Cells[1].Text;
                    n_user u = rdc.n_users.Where(w => w.name == agentName).FirstOrDefault();
                    i.ImageUrl = SM_Global.GetLeaderboardImagePath(u);
                }

            }
    }




}