﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;



public partial class secure_sm_Screens_sales_theory_screen : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();

    List<n_user> agentList = new List<n_user>();
    List<hubspot_engagement> engagementList = new List<hubspot_engagement>();
    SalesTheory st = new SalesTheory();
    private List<SalesTheory.SalesTheoryObject> aoList = new List<SalesTheory.SalesTheoryObject>();
    static Random rnd = new Random();
    DateTime startOfWeek;
    DateTime endOfWeek;

    //Cycle Variables
    private int duration = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LoadQueryStringVariables();
            LoadAgents();
            LoadDates();
            if (agentList.Count <= 0)
                return;
            using (RzDataContext rdc = new RzDataContext())
                engagementList = HubspotLogic.EngagementActivity.GetEngagementList(rdc, DateTime.Today, endOfWeek);
            if (engagementList.Count == 0)
                return;
            st = new SalesTheory();
            st.Load(engagementList, agentList, startOfWeek, endOfWeek, SM_Enums.BonusCategoryName.PhoneCallCount);
            LoadSalesTheoryList();
            LoadRepeater();
            DoColoration();

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void DoColoration()
    {
        //80 points a day / 8 hours a day = 10 points per hour
        //10 / 2 = 5 pts per 30 minutes, 2.5 pts every 15 minutes.
        // 5 / 30 = .16 points per minute
        //100 pts / day / 8 / 60 = .20
        //Rankings:
        //High = 70-100
        //Med = 30-69
        //0-29

        //begin of day, 0 hours have passed, have everyone HIGH
        //at 30 minute mark, change to points earned in that 30 minuts, with 5 = 100%
        //at 60 minute mark, update again, 10 = 100%
        //etc.

        DateTime dayStart = DateTime.Today.AddHours(8).AddMinutes(30);//8:30AM


        TimeSpan difference = DateTime.Now - dayStart;
        //int hoursSoFar = (int)difference.TotalHours;//Hours so far
        double minutesSoFar = difference.TotalMinutes;//Minutes so far 
        if (DateTime.Now.Hour >= 12)
            minutesSoFar -= 60; ///subtract lunch hour
        double possiblePointsSoFar = .2 * minutesSoFar;

        foreach (RepeaterItem stTile in rpSalesTheory.Items)
        {
            HtmlGenericControl divAgentData = (HtmlGenericControl)stTile.FindControl("divAgentData");
            //divAgentData.Style.Add("background", "green");
            Label lblTotal = (Label)stTile.FindControl("lblAgentTotal");
            double totalPoints = Convert.ToDouble(lblTotal.Text);
            double percentOfPossiblePointsSoFar = totalPoints / possiblePointsSoFar;

            string bgColor = SetBgColor(percentOfPossiblePointsSoFar);
            divAgentData.Style.Add("background", bgColor);

            //What is the current pace of points that will get you 80%
            double pace = Math.Ceiling(possiblePointsSoFar * .8);//80% of the current possible points
            lblPace.Text = pace.ToString();


            //1st image gets flames
            if (stTile.ItemIndex == 0)
            {
                divAgentData.Style.Remove("background");
                divAgentData.Style.Add("background-image", "url(/Images/flames.gif");
                divAgentData.Style.Add("background-size", "cover");

            }
        }


    }

    private string SetBgColor(double percentOfPossiblePointsSoFar)
    {
        if (percentOfPossiblePointsSoFar >= .8)
            //return "green";
            return "#24AE07";
        if (percentOfPossiblePointsSoFar >= .5)
            //return "orange";
            return "#D15C08";
        //return "red";
        return "#CA081B";

    }

    //private Color SetBgColor(double percentOfPossiblePointsSoFar)
    //{
    //    return new Color(2.0f * percentOfPossiblePointsSoFar, 2.0f * (1 - percentOfPossiblePointsSoFar), 0);
    //}

    private void LoadQueryStringVariables()
    {
        //Cycle duration = time between each cycle, in MS
        string dur = Tools.Strings.SanitizeInput(Request.QueryString["dur"]);
        if (!string.IsNullOrEmpty(dur))//not empty
            int.TryParse(dur, out duration);//if it's an int, it will set that value
        if (duration != 0)
            hfCycleDuration.Value = (duration * 1000).ToString();


    }

    private void LoadDates()
    {
        startOfWeek = Tools.Dates.GetFirstDateOfWeek(DateTime.Now.Date).AddDays(1);
        endOfWeek = startOfWeek.AddDays(5);
    }


    private void LoadDummyAgents(int n)
    {
        int i = 1;//1st Tile
        while (i < n)
        {

            SalesTheory.SalesTheoryObject ao = new SalesTheory.SalesTheoryObject();
            ao.agentName = "Name" + i;
            ao.conversationPoints = rnd.Next(1, 16);
            ao.bonusPoints = rnd.Next(1, 16);
            ao.rfqPoints = rnd.Next(1, 16);
            ao.salesGoalPoints = rnd.Next(1, 16);
            ao.agentImageUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Miguel_%C3%81ngel%2C_por_Daniele_da_Volterra_%28detalle%29.jpg/220px-Miguel_%C3%81ngel%2C_por_Daniele_da_Volterra_%28detalle%29.jpg";
            ao.currentPoints = ao.conversationPoints + ao.bonusPoints + ao.rfqPoints + ao.salesGoalPoints;
            aoList.Add(ao);
            i++;
        }

    }

    private void LoadAgents()
    {
        //int i = tileIndex;        
        List<string> disallowedUserEmails = new List<string>() { "ctorrioni@sensiblemicro.com", "lmcdonald@sensiblemicro.com" };
        List<string> teamList = new List<string>() { "sales", "distributor sales" };
        agentList = rzt.GetUsersForTeams(teamList);
        agentList = agentList.Where(w => (w.show_on_sales_screen ?? false == true) && !disallowedUserEmails.Contains(w.email_address.ToLower())).ToList();

    }

    private void LoadSalesTheoryList()
    {
        DateTime today = DateTime.Now.Date;
        if (!st.dailySalesTheoryObjects.ContainsKey(today))
            return;
        List<SalesTheory.SalesTheoryObject> sList = st.dailySalesTheoryObjects[today];
        foreach (SalesTheory.SalesTheoryObject v in sList)
        {

            SalesTheory.SalesTheoryObject ao = new SalesTheory.SalesTheoryObject();
            string firstName = v.agentName.Split(' ')[0];
            ao.agentName = firstName;
            ao.conversationPoints = v.conversationPoints;
            ao.bonusPoints = v.bonusPoints;
            ao.bonusTitle = v.bonusTitle;
            ao.bonusIconClass = v.bonusIconClass;
            ao.rfqPoints = v.rfqPoints;
            ao.salesGoalPoints = v.salesGoalPoints;
            ao.rz_user_uid = v.rz_user_uid;
            n_user salesTheoryUser = null;
            using (RzDataContext rdc = new RzDataContext())
            {
                salesTheoryUser = rdc.n_users.Where(w => w.unique_id == v.rz_user_uid).FirstOrDefault();
            }
            //ao.agentImageUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Miguel_%C3%81ngel%2C_por_Daniele_da_Volterra_%28detalle%29.jpg/220px-Miguel_%C3%81ngel%2C_por_Daniele_da_Volterra_%28detalle%29.jpg";
            ao.agentImageUrl = SM_Global.GetLeaderboardImagePath(salesTheoryUser);

            ao.currentPoints = ao.conversationPoints + ao.bonusPoints + ao.rfqPoints + ao.salesGoalPoints;
            aoList.Add(ao);
        }
    }


    private void LoadRepeater()
    {

        //Get at tileIndex, take numberOftiles
        var query = aoList.OrderByDescending(o => o.currentPoints);
        rpSalesTheory.DataSource = aoList.OrderByDescending(o => o.currentPoints);
        rpSalesTheory.DataBind();
    }
}