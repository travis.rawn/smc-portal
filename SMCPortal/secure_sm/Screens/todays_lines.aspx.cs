﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Screens_todays_lines : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadGrid();
    }

    private void LoadGrid()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            var query = rdc.orddet_lines
                 .Where(w => w.date_created >= DateTime.Today.Date.AddDays(-1)
                 && (w.orderid_sales ?? "").Length > 0
                 && w.status != "void")
                 .GroupBy(g => new { g.manufacturer, g.customer_name })
                 .Select(s => new
                 {


                     Part = s.Max(m => m.fullpartnumber),
                     MFG = s.Key.manufacturer.Substring(0, 20),
                     Company = s.Key.customer_name.Substring(0, 20),
                     //Segment = rdc.companies.Where(w => w.unique_id == s.customer_uid).Select(ss => ss.industry_segment)
                 }
        );



            smdt.dataSource = null;
            smdt.allowPaging = true;
            smdt.allowPageLengthChange = false;
            smdt.showPageInfo = false;
            smdt.allowSearch = false;

            if (query.Any())
                smdt.dataSource = query.OrderBy(o => o.MFG);
            smdt.loadGrid();


        }




    }
}