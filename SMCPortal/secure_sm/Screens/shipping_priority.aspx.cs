﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_sm_Screens_shipping_priority : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();

    class ShipLine
    {
        public string SO { get; set; }
        public string Customer { get; set; }
        public string Part { get; set; }
        public string Dock { get; set; }
        public int InspID { get; set; }
        public string ShipDate { get; set; }
        public bool isAdvanceTerms { get; set; }
        public bool isAdvanceFullyPaid { get; set; }


    }


    protected void Page_Load(object sender, EventArgs e)
    {
        smdtInspections.RowDataBound += new GridViewRowEventHandler(smdtInspections_RowDataBound);
        GetData();
    }

    protected void smdtInspections_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //GridView gv = smdtInspections.theGridView;
        //Hide InspID Header        
        if (e.Row.RowType == DataControlRowType.Header)//Critical, at first there are no rows, and this is null. 
        {
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;
            e.Row.Cells[7].Visible = false;
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Grab the Values before making invisible
            string inspID = e.Row.Cells[5].Text;
            string strAdvTerms = e.Row.Cells[6].Text;
            string strAdvFullyPaid = e.Row.Cells[7].Text;
            bool isAdvTerms = false;
            bool isAdvFullyPaid = false;
            if (!string.IsNullOrEmpty(strAdvTerms))
                isAdvTerms = Convert.ToBoolean(strAdvTerms);
            if (!string.IsNullOrEmpty(strAdvFullyPaid))
                isAdvFullyPaid = Convert.ToBoolean(strAdvFullyPaid);

            //Make Rows Invisisble
            //Advanced Terms
            e.Row.Cells[5].Visible = false;
            //IsAdvancePayment
            e.Row.Cells[6].Visible = false;
            //Adv PaymentFullyPaid
            e.Row.Cells[7].Visible = false;


            //Aoply Colors
            //Is Fully Paid and Due Today = Green
            //Due before today = Orange
            //If Advanced Terms and Not Fully Paid = Red

            e.Row.BackColor = System.Drawing.Color.Black;
            DateTime today = DateTime.Today;
            DateTime dueDate = Convert.ToDateTime(e.Row.Cells[4].Text);
            if (dueDate == today)
                e.Row.BackColor = System.Drawing.Color.Green;
            if (dueDate < today)
                e.Row.BackColor = System.Drawing.Color.Orange;
            if (isAdvTerms)
                if (isAdvFullyPaid)
                {
                    e.Row.BackColor = System.Drawing.Color.Green;
                }
                else
                    e.Row.BackColor = System.Drawing.Color.Red;


        }
    }

    private void GetData()
    {
        LoadLines();
        //Thread.Sleep(60000);
        //Response.Redirect(Page.Request.Url.ToString(), true);
    }



    private void LoadLines()
    {
        try
        {


            List<string> onlyStatus = new List<string>() { "open", "packing", "received from service" };
            using (RzDataContext rdc = new RzDataContext())
            {


                var dataQuery = (from l in rdc.orddet_lines
                                 where l.qc_status != "Inbound"
                                 where l.ordernumber_sales.Length > 0
                                 where !l.fullpartnumber.ToLower().Contains("gcat")
                                 where onlyStatus.Contains(l.status.ToLower())
                                 where (l.insp_id.Length > 0 && l.insp_id != null)
                                 select new
                                 {
                                     SO = l.ordernumber_sales,
                                     Orderid_sales = l.orderid_sales,
                                     Orderid_invoice = l.orderid_invoice,
                                     Customer = l.customer_name,
                                     Part = l.fullpartnumber,
                                     Qty = l.quantity,
                                     Dock = (DateTime)l.customer_dock_date,
                                     InspID = l.insp_id,
                                     Inspector = "",
                                     InspStartDate = "",
                                     ExpectedShipDate = "",
                                     ShipDue = l.ship_date_due

                                 }).OrderBy(o => o.ShipDue);



                var lineInspIds = dataQuery.Where(w => w.InspID.Length > 0).Select(s => s.InspID).ToList();//NEed the list, as next query is different datacontext
                List<insp_head> currentInspections = new List<insp_head>();
                using (sm_portalEntities pdc = new sm_portalEntities())
                    currentInspections = pdc.insp_head.Where(w => lineInspIds.Contains(w.insp_id.ToString())).ToList();




                List<ShipLine> shipList = new List<ShipLine>();
                foreach (var v in dataQuery)
                {
                    ShipLine l = new ShipLine();
                    //Get the related inspection
                    insp_head insp = null;
                    int inspID = Convert.ToInt32(v.InspID);
                    string so = v.SO;
                    insp = currentInspections.Where(w => w.insp_id == inspID).FirstOrDefault();
                    if (insp != null)
                    {
                        bool isComplete = insp.is_insp_complete ?? false;

                        l.SO = v.SO;
                        l.Customer = v.Customer;
                        l.Part = v.Part + " (" + v.Qty + ")";
                        l.Dock = v.Dock.Date.ToShortDateString();
                        l.InspID = 0;
                        l.ShipDate = v.ShipDue.Value.ToShortDateString();
                        l.isAdvanceTerms = false;
                        l.isAdvanceFullyPaid = false;
                        bool isAdvance = false;


                        ordhed_sale s = rdc.ordhed_sales.Where(w => w.unique_id == v.Orderid_sales).FirstOrDefault();
                        if (s != null)
                        {

                            string terms = s.terms;
                            //Are these Advance Terms?
                            isAdvance = IsAdvanceTerms(s);
                            l.isAdvanceTerms = isAdvance;
                        }
                        //Must be an invoice to check fully paid by customer.
                        ordhed_invoice i = rdc.ordhed_invoices.Where(w => w.unique_id == v.Orderid_invoice).FirstOrDefault();
                        if (i != null)
                        {
                            //IF Advanced Terms, is fully paid?
                            if (isAdvance)
                                l.isAdvanceFullyPaid = IsAdvanceFullyPaid(i);
                        }



                        var names = insp.inspector.Split(' ');
                        string firstName = names[0];
                        DateTime startTime = insp.date_created.Value;
                        string strStartTime = startTime.ToShortTimeString();
                        l.InspID = Convert.ToInt32(insp.insp_id);
                        DateTime shipDate = new DateTime(1900, 01, 01);
                        //if (insp.expectedShipDate != null)
                        //    shipDate = (DateTime)insp.expectedShipDate;
                        //l.ShipDate = shipDate.ToShortDateString();
                        if (isComplete)
                            shipList.Add(l);
                    }


                }


                int count = shipList.Count;
                //lblCount.Text = count.ToString();
                var gridQuery = shipList.Select(s => new
                {

                    s.SO,
                    s.Customer,
                    s.Part,
                    s.Dock,
                    s.ShipDate,
                    s.InspID,
                    isAdvanceTerms = s.isAdvanceTerms.ToString(),
                    isAdvanceFullyPaid = s.isAdvanceFullyPaid.ToString(),

                });

                //8-15-2020 DFixed by Lincoln Till - Copy and paste led to realizatioj that cortColumn was non-existent.  Changed to accurate column of 4
                smdtInspections.sortColumn = 4;
                smdtInspections.allowSearch = false;
                smdtInspections.allowSorting = false; // Else we'll get highlights on whatever column is sorted.
                smdtInspections.allowPaging = false;
                smdtInspections.showRecordsInfo = false;
                smdtInspections.theGridView.DataKeyNames = new string[] { "InspID" };

                smdtInspections.dataSource = gridQuery.OrderBy(o => Convert.ToDateTime(o.ShipDate ?? "01/01/1900"));
                smdtInspections.loadGrid();
            }



        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private bool IsAdvanceFullyPaid(ordhed_invoice i)
    {
        bool ret = false;
        ret = CheckAdvanceFullyPaid(i);
        return ret;
    }

    private bool IsAdvanceTerms(ordhed_sale s)
    {
        bool ret = false;
        string terms = s.terms;
        List<string> advPaymentStrings = new List<string>() { "Wire In Advance", "Check in Advance", "IC Source Escrow", "ACH", "Paypal" };
        if (advPaymentStrings.Contains(terms))
            ret = true;
        if (terms.ToLower().Contains("advance"))
            ret = true;
        if (terms.ToLower().Contains("tt"))
            ret = true;
        return ret;
    }

    private bool CheckAdvanceFullyPaid(ordhed_invoice i)
    {
        List<checkpayment> paymentList = new List<checkpayment>();
        using (RzDataContext rdc = new RzDataContext())
            paymentList = rdc.checkpayments.Where(w => w.base_ordhed_uid == i.unique_id).ToList();
        double invoiceTotal = (double)i.ordertotal;
        double paymentTotal = (double)paymentList.Sum(s => s.transamount);
        if (paymentTotal >= invoiceTotal)
            return true;
        return false;
    }
}