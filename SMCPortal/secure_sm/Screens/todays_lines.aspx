﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="todays_lines.aspx.cs" Inherits="secure_sm_Screens_todays_lines" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">

    <style>
        body, html {
            background-color: rgb(27, 28, 31);
            font-family: Arial;
        }

        .GridTitle {
            font-size: 5.5vh;
            text-align: center;
        }


        table.dataTable {
        }

            table.dataTable th {
                font-size: 4vh;
                background-color: rgb(27, 28, 31) !important; /*black*/
            }

            table.dataTable tbody tr {
                background-color: rgb(27, 28, 31) !important; /*black*/
                font-size: 5vh;
            }



        /*InProcess Grid*/
        #cpBody_smdt_gvMain {
            background-color: rgb(27, 28, 31) !important; /*black*/
        }

            #cpBody_smdt_gvMain .sorting_1 {
                background-color: rgb(27, 28, 31) !important;
            }
        /*Shipped Table*/
        #cpBody_smdt_gvMain {
            /*background-color: #82E36D !important;*/ /*green*/
        }

            #cpBody_smdt_gvMain .sorting_1 {
                /*background-color: #82E36D !important;*/ /*green*/
            }

            #cpBody_smdt_gvMain tbody tr {
                /*background-color: #82E36D !important;*/ /*green*/
            }

                #cpBody_smdt_gvMain tbody tr.odd {
                    /*background-color: #82E36D !important;*/ /*green*/
                }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <div class="GridTitle">
        Today's Lines by MFG
    </div>
    <uc1:sm_datatable runat="server" ID="smdt" />

    <script>
        //Seconds between pages


        var intervalSeconds = 8;
        var table = $('#cpBody_smdt_gvMain').DataTable();
        ///Call on load
        //Set which page should show on an interval.
        $(setInterval(function () {
            //Wrapping this way invoked the fadeOut callback function to ensure fade finishes before updating page.
            $('#cpBody_smdt_gvMain').fadeOut('slow', function () {
                // will be called when the element finishes fading out
                // if selector matches multiple elements it will be called once for each
                SetCurrentPage();
            });
        }, intervalSeconds * 1000));




        function SetCurrentPage() {

            var info = table.page.info();
            var totalPages = info.pages;
            var currentPageIndex = info.page;//page is zero based, pages is not.
            var nextPageIndex = currentPageIndex + 1;
            if (nextPageIndex > totalPages - 1)
                nextPage = 0;
            //Change the page.            
            table.page(nextPageIndex).draw('page');
            //fade back in.
            $('#cpBody_smdt_gvMain').fadeIn("fast");

        };




    </script>
</asp:Content>
