﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="screen_cycler.aspx.cs" Inherits="secure_sm_Screens_screen_cycler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">




    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upCycler">
        <ContentTemplate>
            <div style="text-align: center;">             
                <iframe id="screen_cycler" runat="server" scrolling="auto" width="1920" height="1080" frameborder="0"></iframe>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Timer ID="Timer1" runat="server"  OnTick="Timer1_Tick"></asp:Timer>


</asp:Content>

