﻿<%@ Page Title="Ship Activity" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="daily_shipping_activity.aspx.cs" Inherits="secure_sm_Sales_Screens_daily_shipping_activity" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">

    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bowlby+One+SC|Bungee+Inline|Bungee+Shade|Eater|Faster+One|Frijole|Holtwood+One+SC|Iceland|Lekton|Luckiest+Guy|Nosifer|Press+Start+2P|Racing+Sans+One|Rubik+Mono+One|Slackey|Stalinist+One|UnifrakturMaguntia" rel="stylesheet">


    <style>
        .GridTitle {
            font-size: 28px;
            text-align: center;
        }


        table.dataTable {
        }

            table.dataTable th {
                font-size: 22px;
                background-color: rgb(27, 28, 31) !important; /*black*/
            }

            table.dataTable tbody tr {
                background-color: rgb(27, 28, 31) !important; /*black*/
                font-size: 18px;
            }



        /*InProcess Grid*/
        #cpBody_smdtInProcess_gvMain {
            background-color: rgb(27, 28, 31) !important; /*black*/
        }

            #cpBody_smdtInProcess_gvMain .sorting_1 {
                background-color: rgb(27, 28, 31) !important;
            }
        /*Shipped Table*/
        #cpBody_smdtShipped_gvMain {
            background-color: #82E36D !important; /*green*/
        }

            #cpBody_smdtShipped_gvMain .sorting_1 {
                background-color: #82E36D !important; /*green*/
            }

            #cpBody_smdtShipped_gvMain tbody tr {
                background-color: #82E36D !important; /*green*/
            }

                #cpBody_smdtShipped_gvMain tbody tr.odd {
                    background-color: #82E36D !important; /*green*/
                }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cpBody" runat="Server">

    <div class="GridTitle">
        Shipped
    </div>


    <uc1:sm_datatable runat="server" ID="smdtShipped" />

    <div class="GridTitle">
        In Process
    </div>

    <uc1:sm_datatable runat="server" ID="smdtInProcess" />
</asp:Content>
