﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="monthly_goals.aspx.cs" Inherits="secure_sm_Sales_Screens_monthly_goals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">

    <link href="https://fonts.googleapis.com/css?family=Monoton" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Monoton" rel="stylesheet">

    <style>
        .scroller {
            margin: 5px;
            padding:0px;
            display: none;
            vertical-align: middle !important;
            text-align: center !important;
            overflow: hidden;
            position: relative;
            color: black;
            /*height: 500px;*/
        }

            .scroller img {
                max-height: 200px;
                max-width: 200px;
            }

        .scroller-score-text {
            font-family: 'Josefin Sans', sans-serif;
            /*text-shadow: 2px 1px 15px rgba(150, 150, 150, 1);*/
        }

        .scroller td {
            font-weight: bold;
            font-size: 72px;
            display: table-cell;
            vertical-align: middle !important;
        }

        .scroller th {
            font-weight: bold;
            font-size: 72px;
            display: table-cell;
            vertical-align: middle !important;
            text-align: center;
            text-shadow: 3px 2px 2px black;
        }


        .scroller tr {
            /*opacity: 0.5;
                filter: alpha(opacity=50); /* For IE8 and earlier */ */;
        }

        .scroller-title {
            font-weight: bold;
            font-size: 80px;
            vertical-align: middle !important;
            text-align: center;
        }

        .scroller-rank {
            font-family: 'Monoton', cursive;
            font-weight: bold;
            font-size: 60px;
            vertical-align: middle !important;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <div class="scroller-title">
        <asp:Label ID="lblGoalLabel" runat="server"></asp:Label>
    </div>

    <div id="scroller" class="panel panel-default panel-body card scroller">

        <asp:GridView ID="gvMonthlyGoals" runat="server" CssClass="table table-responsive" AutoGenerateColumns="false" GridLines="None" HeaderStyle-CssClass="scroller-title-text">
            <RowStyle HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField SortExpression="orderdate" ShowHeader="true">
                    <ItemTemplate>
                        <h3>
                            <asp:Label runat="server" ID="lblAgent" Text='<%# Eval("Agent") %>' /></h3>
                        <asp:Image runat="server" ID="imAgent" ImageUrl='<%# Eval("ImageURL") %>' CssClass="img-responsive img-circle img-scroller" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="GP" HeaderText="GP" SortExpression="GP" DataFormatString="{0:c2}" ItemStyle-CssClass="scroller-text" />
                <asp:BoundField DataField="Percent" HeaderText="Percent" SortExpression="Percent" DataFormatString="{0:p0}" ItemStyle-CssClass="scroller-text" />
            </Columns>
        </asp:GridView>

        <asp:GridView ID="gvMagic" runat="server" CssClass="table table-responsive" AutoGenerateColumns="false" GridLines="None" HeaderStyle-CssClass="scroller-title-text" OnRowDataBound="gvMagic_RowDataBound">
            <RowStyle HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField SortExpression="orderdate" ShowHeader="true">
                    <ItemTemplate>
                        <h3>
                            <asp:Label runat="server" ID="lblAgent" Text='<%# Eval("Agent") %>' /></h3>
                        <asp:Image runat="server" ID="imAgent" ImageUrl='<%# Eval("ImageURL") %>' CssClass="img-responsive img-circle img-scroller" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Magic" HeaderText="Magic" SortExpression="Magic" />
            </Columns>
        </asp:GridView>

        <asp:GridView ID="gvLeaderBoard" runat="server" CssClass="table table-responsive" AutoGenerateColumns="false" GridLines="None" HeaderStyle-CssClass="scroller-title-text" OnRowDataBound="gvLeaderBoard_RowDataBound">
            <RowStyle HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField SortExpression="Rank" ShowHeader="false">
                    <HeaderStyle Width="20%" />
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblRank" CssClass="scroller-rank" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="orderdate" ShowHeader="true">
                    <HeaderStyle Width="20%" />
                    <ItemTemplate>
                        <h3>
                            <asp:Label runat="server" ID="lblAgent" Text='<%# Eval("Agent") %>' /></h3>
                        <asp:Image runat="server" ID="imAgent" ImageUrl='<%# Eval("ImageURL") %>' CssClass="img-responsive img-circle img-scroller" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Rank" SortExpression="Rank" ItemStyle-CssClass="scroller-score-text ">
                    <HeaderStyle Width="20%" />
                </asp:BoundField>
                <%--   <asp:BoundField DataField="Text" HeaderText="" SortExpression="Text" ItemStyle-CssClass="scroller-text">
                    <HeaderStyle Width="40%" />
                </asp:BoundField>      --%>
            </Columns>
        </asp:GridView>

    </div>





    <script>
        window.onload = function () {
            //http://localhost:50477/secure_sm/Sales/Screens/monthly_goals.aspx?type=quoted&speed=25&autoCycle=false
            var type = getParameterByName("type")
            var url = window.location.href;           
            var autoCycle = getParameterByName("autoCycle", url);
            var autoScroll = getParameterByName("autoScroll", url);
            var scrollSpeed = getParameterByName("speed", url);
            var repeat = getParameterByName("repeat", url);




            if (autoScroll === null)
                autoScroll = "true";


            //If the div has no set height, no scroll.
            $('#scroller').css("height", $(document).height() - $("#cpBody_lblGoalLabel").height() - 65);
            var element = $(".scroller");
            element.fadeTo(100, 100, 'linear');
            if (autoScroll === "true")
                AutoScroll(type);


            function AutoScroll(type) {
                var grid = GetGrid(type);
                //SetColors(grid);
                element.animate({
                    scrollTop: grid.height()
                }, (scrollSpeed * 1000), "linear", function () { loadNewPage(autoCycle, type, repeat) });

            }

        }


        function loadNewPage(autoCycle, type, repeat) {
            if (autoCycle === "true") {
                var nextPage = window.location.href;
                if (type == "quoted")
                    nextPage = nextPage.replace("quoted", "booked")
                else if (type == "booked")
                    nextPage = nextPage.replace("booked", "invoiced")
                else if (type == "invoiced")
                    nextPage = nextPage.replace("invoiced", "magic")
                else if (type == "magic")
                    nextPage = nextPage.replace("magic", "leaderboard")
                else if (type == "leaderboard")
                    nextPage = nextPage.replace("leaderboard", "quoted")
                window.location.replace(nextPage);
            }
            else if (repeat)
                window.location.replace(window.location.href);



        }

        function GetGrid(type) {
            switch (type) {
                case "quoted":
                case "booked":
                case "invoiced":
                    return $("#cpBody_gvMonthlyGoals");
                    break;
                case "magic":
                    return $("#cpBody_gvMagic")
                    break;
                case "leaderboard":
                case "magic":
                    return $("#cpBody_gvLeaderBoard")
                    break;

            }

        }

    </script>
</asp:Content>

