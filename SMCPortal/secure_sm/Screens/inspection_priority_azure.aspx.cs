﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_sm_Screens_inspection_priority : System.Web.UI.Page
{

    RzTools rzt = new RzTools();
    RzDataContext rdc = new RzDataContext();
    class InspectionLine
    {
        public string SO { get; set; }
        public string Customer { get; set; }
        public string Part { get; set; }
        public string Dock { get; set; }
        public int InspID { get; set; }
        public string Inspector { get; set; }
        public string Status { get; set; }
        public int inspPriority { get; set; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string connString = System.Configuration.ConfigurationManager.ConnectionStrings["AzureConnection"].ConnectionString;
        rdc = new RzDataContext(connString);
        smdtInspections.RowDataBound += new GridViewRowEventHandler(smdtInspections_RowDataBound);
        GetData();
    }

    protected void smdtInspections_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header) //Hide InspID Header
            e.Row.Cells[7].CssClass = "hiddencol";

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Hide InspID td
            //e.Row.Cells[7].Visible = false;
            e.Row.Cells[7].CssClass = "hiddencol";
            e.Row.BackColor = System.Drawing.Color.Black;
            int InspID = 0;
            InspID = Convert.ToInt32(smdtInspections.theGridView.DataKeys[e.Row.RowIndex].Values[0]);
            if (InspID > 0)//Color in-progress inspecitons
                e.Row.BackColor = System.Drawing.Color.ForestGreen;

        }
    }

    private void GetData()
    {
        LoadLines();

    }


    private void LoadLines()
    {

        List<SensibleDAL.ShippingLogic.priorityObject> dataQuery = SensibleDAL.ShippingLogic.GetInboundPriorityLines(rdc, DateTime.Today.AddYears(-1).Date)
            .Where(w => w.qcStatus == SM_Enums.QcStatus.In_House.ToString() && w.ordernumberSales.Length > 0).ToList();


        int count = dataQuery.Count;
        //lblCount.Text = count.ToString();

        var lineInspIds = dataQuery.Where(w => (w.idea_insp_id ?? "").Length > 0).Select(s => s.idea_insp_id).ToList();//NEed the list, as next query is different datacontext
        List<insp_head> currentInspections = new List<insp_head>();
        List<string> completedInspectionIDs = new List<string>();
        using (sm_portalEntities pdc = new sm_portalEntities())
        {

            currentInspections = pdc.insp_head.Where(w => lineInspIds.Contains(w.insp_id.ToString())).ToList();
            completedInspectionIDs = pdc.insp_head.Where(w => lineInspIds.Contains(w.insp_id.ToString()) && w.is_insp_complete == true).Select(s => s.insp_id.ToString()).ToList();
        }
        var openInspectionQuery = dataQuery.Where(w => !completedInspectionIDs.Contains(w.idea_insp_id.ToString()));

        smdtInspections.sortColumn = 1;
        smdtInspections.allowSorting = false; // Else we'll get highlights on whatever column is sorted.
        smdtInspections.pageLength = 25;
        smdtInspections.allowSearch = false;
        smdtInspections.allowPaging = false;
        smdtInspections.showRecordsInfo = false;
        smdtInspections.theGridView.DataKeyNames = new string[] { "ID" };


        List<InspectionLine> inspectionList = new List<InspectionLine>();
        foreach (var v in openInspectionQuery)
        {
            InspectionLine l = new InspectionLine();
            insp_head insp = null;

            l.SO = v.ordernumberSales;
            l.Customer = v.customerName;
            l.Part = v.partNumber + " (" + v.qty + ")";
            //l.Dock = v.customerDock.ToShortDateString();//CalculateDockDatePriority(v.ProjDock, v.CustDock, v.InitialDock, v.DateCreated, v.ServiceOrderID);
            l.Dock = v.projectedDock.ToShortDateString();
            l.InspID = 0;
            l.Inspector = "";
            l.Status = v.lineStatus;
            if (!string.IsNullOrEmpty(v.idea_insp_id.ToString()))//Get the related inspection
                insp = currentInspections.Where(w => w.insp_id.ToString() == v.idea_insp_id.ToString()).FirstOrDefault();
            if (insp != null)
            {
                var names = insp.inspector.Split(' ');
                string firstName = names[0];
                DateTime startTime = insp.date_created.Value;
                string strStartTime = startTime.ToShortTimeString();
                l.InspID = Convert.ToInt32(insp.insp_id);
                l.Inspector = GetInspectorInitials(insp.inspector);

            }
            l.inspPriority = v.linePriority;

            inspectionList.Add(l);
        }



        var gridQuery = inspectionList.Select(s => new
        {
            s.SO,
            Customer = s.Customer.Length >= 25 ? s.Customer.Substring(0, 25) : s.Customer,
            s.Part,
            s.Dock,
            ID = s.InspID,
            INSP = s.Inspector,
            Stat = s.Status,
            pri = s.inspPriority
        });


        smdtInspections.sortColumn = 7;
        smdtInspections.sortDirection = "desc";
        smdtInspections.allowSorting = true; // Else we'll get highlights on whatever column is sorted.
        smdtInspections.pageLength = 25;
        smdtInspections.allowSearch = false;
        smdtInspections.allowPaging = false;
        smdtInspections.showRecordsInfo = false;
        smdtInspections.theGridView.DataKeyNames = new string[] { "ID" };
        smdtInspections.dataSource = gridQuery.OrderBy(o => DateTime.Parse(o.Dock));
        smdtInspections.loadGrid();
    }

    private string GetInspectorInitials(string inspector)
    {
        string firstName = inspector.Split(' ')[0];
        string lastName = inspector.Split(' ')[1];
        var firstInitial = firstName[0].ToString();
        var lastInitial = lastName[0].ToString();
        return (firstInitial + lastInitial).ToString();

    }
}