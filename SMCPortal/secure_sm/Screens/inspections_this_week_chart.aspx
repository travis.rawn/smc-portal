﻿<%@ Page Title="Weekly Inspections" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="inspections_this_week_chart.aspx.cs" Inherits="secure_sm_Screens_inspections_this_week_chart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" Runat="Server">   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" Runat="Server">
     <div style="text-align:center; font-size:70px;">
         This Week's Inspections
    </div>
<div><%--assign this class to prevent scrollbar from flickering--%>
     <canvas id="barDailyInspections"></canvas>
</div>
</asp:Content>

