﻿<%@ Page Title="Inventory Updated" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="inventory_updater.aspx.cs" Inherits="secure_sm_Warehouse_inventory_management" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>INVENTORY UPDATER</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">


    <asp:HiddenField ID="hfPartUid" runat="server" />
    
        <div class="row">

            <div class="col-xs-8">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Placeholder="Part Number | Location"></asp:TextBox>
            </div>
            <div class="col-xs-4">
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-sm btn-primary-sm btn-block" OnClick="btnSearch_Click" OnClientClick="ShowUpdateProgress();" />
            </div>


            <div style="padding-bottom: 50px;"></div>
            <div>
                <div class="card">
                    <uc1:sm_datatable runat="server" ID="smdtSearchResults" />
                </div>
            </div>
        </div>
        <div>
            <div class="col-xs-12">
                <asp:Panel ID="pnlPartrecordDetails" runat="server">
                    <asp:FormView ID="fvPartrecordDetails" runat="server" Width="100%">
                        <ItemTemplate>
                            <div style="text-align: center" class="well well-sm">
                                <label>
                                    <h3>
                                        <asp:Label ID="lblPartnumber" runat="server" Text='<%# Eval("partnumber") %>'></asp:Label></h3>
                                </label>
                            </div>
                            <hr />

                            <div class="row">
                                <div class="col-xs-6">
                                    <label>MFG:</label>
                                    <asp:TextBox ID="txtMfg" runat="server" Text='<%# Eval("mfg") %>' ForeColor="Black" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-xs-6">
                                    <label>QTY:</label>
                                    <asp:TextBox ID="txtQty" runat="server" Text='<%# Eval("qty") %>' ForeColor="Black" CssClass="form-control"></asp:TextBox>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>DC:</label>
                                    <asp:TextBox ID="txtDc" runat="server" Text='<%# Eval("dc") %>' ForeColor="Black" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-xs-6">
                                    <label>RoHS:</label>
                                    <asp:TextBox ID="txtRohs" runat="server" Text='<%# Eval("rohs") %>' ForeColor="Black" CssClass="form-control"></asp:TextBox>

                                </div>
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Pkg:</label>
                                    <asp:TextBox ID="txtPkg" runat="server" Text='<%# Eval("pkg") %>' ForeColor="Black" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-xs-6">
                                    <label>Loc:</label>
                                    <asp:TextBox ID="txtLoc" runat="server" Text='<%# Eval("loc") %>' ForeColor="Black" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <hr />
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label>Comment:</label>
                                    <asp:TextBox ID="txtIc" runat="server" Text='<%# Eval("ic") %>' ForeColor="Black" CssClass="form-control" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                    <br />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary-sm btn-block" OnClick="btnUpdate_Click" />
                                </div>
                        </ItemTemplate>
                    </asp:FormView>
                </asp:Panel>
            </div>

        </div>
   

















</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

