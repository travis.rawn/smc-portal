﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Warehouse_inventory_management : System.Web.UI.Page
{
    RzDataContext RZDC = new RzDataContext();
    partrecord CurrentPart;
    SM_Tools tools = new SM_Tools();
    string partUID;
    protected void Page_Load(object sender, EventArgs e)
    {
        partUID = hfPartUid.Value;
        //smdtSearchResults.SelectedIndexChanged += new EventHandler(smdtSearchResults_SelectedIndexChanged);
        smdtSearchResults.SelectedIndexChanged += new EventHandler(smdtSearchResults_SelectedIndexChanged);
        smdtSearchResults.DataBound += new EventHandler(smdtSearchResults_DataBound);
        smdtSearchResults.RowDataBound += new GridViewRowEventHandler(smdtSearchResults_RowDataBound);              
        BindGvSearchResults();

    }

    protected void smdtSearchResults_DataBound(object sender, EventArgs e)
    {

        ////THis is the ROW, not the GV, and think this get set before Columns are present, so can't uyse them.
        //GridView gv = smdtSearchResults.theGridView;
        ////Hide the header cell
        //gv.HeaderRow.Cells[0].CssClass = "hiddencol";
        //foreach (GridViewRow row in gv.Rows)
        //{
        //    if (row.RowType == DataControlRowType.DataRow)
        //    {
        //        //Hide the td cell
        //        row.Cells[0].CssClass = "hiddencol";
        //    }
        //}


    }
    protected void smdtSearchResults_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Add the select button
        LinkButton b = new LinkButton();
        b.ID = "btnSelect";
        b.Click += new EventHandler(b_Click);
        b.Text = "Select";
        e.Row.Cells[1].Controls.Add(b);

    }

    private void b_Click(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;
        GridViewRow row = lb.NamingContainer as GridViewRow;
        string uid = smdtSearchResults.theGridView.DataKeys[row.RowIndex].Values[0].ToString();
        if (!string.IsNullOrEmpty(uid))
        {

            hfPartUid.Value = uid;
            BindfvPartrecordDetails();
        }
            
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
       
        
    }



    protected void BindGvSearchResults()
    {        
        string searchTerm = txtSearch.Text.ToLower();
        if (!string.IsNullOrEmpty(searchTerm))
        {
            var query = from p in RZDC.partrecords
                        where (p.fullpartnumber.Contains(searchTerm) || p.location.Contains(searchTerm)) && (p.stocktype.ToLower() == "stock" || p.stocktype.ToLower() == "consign")
                        select new
                        {
                            uid = p.unique_id,
                            Select = "",
                            Part = p.fullpartnumber,
                            MFG = p.manufacturer,
                            QTY = p.quantity,
                            LOC = p.location

                        };

            if (query.Any())
            {                
                smdtSearchResults.dataSource = query;
                smdtSearchResults.theGridView.DataKeyNames = new string[] { "uid" };
                smdtSearchResults.loadGrid();
                return;
            }
        }
     

    }




    protected void smdtSearchResults_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        smdtSearchResults.Visible = false;
    }

    

    protected void BindfvPartrecordDetails()
    {
        
           
        var query = from p in RZDC.partrecords
                    where p.unique_id == partUID
                    select new
                    {
                        uid = p.unique_id,
                        partnumber = p.fullpartnumber,
                        mfg = p.manufacturer,
                        qty = p.quantity,
                        dc = p.datecode,
                        rohs = p.rohs_info,
                        pkg = p.packaging,
                        loc = p.location,
                        ic = p.internalcomment,

                    };

        if (query.Any())
        {
            pnlPartrecordDetails.Visible = true;
            fvPartrecordDetails.Visible = true;
            fvPartrecordDetails.DataSource = query;
            fvPartrecordDetails.DataKeyNames = new string[] { "uid" };
            fvPartrecordDetails.DataBind();
        }
        else
        {
            pnlPartrecordDetails.Visible = false;
            fvPartrecordDetails.Visible = false;
            fvPartrecordDetails.DataSource = null;
            fvPartrecordDetails.DataBind();
        }


    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        UpdatePartrecord();
    }

    protected void UpdatePartrecord()
    {
        try
        {

            CurrentPart = RZDC.partrecords.Where(p => p.unique_id == partUID).FirstOrDefault();
            if (CurrentPart != null)
            {
                TextBox txtMfg = (TextBox)fvPartrecordDetails.FindControl("txtMfg");
                TextBox txtQty = (TextBox)fvPartrecordDetails.FindControl("txtQTY");
                TextBox txtDc = (TextBox)fvPartrecordDetails.FindControl("txtDc");
                TextBox txtRohs = (TextBox)fvPartrecordDetails.FindControl("txtRohs");
                TextBox txtPkg = (TextBox)fvPartrecordDetails.FindControl("txtPkg");
                TextBox txtLoc = (TextBox)fvPartrecordDetails.FindControl("txtLoc");
                TextBox txtIc = (TextBox)fvPartrecordDetails.FindControl("txtIc");


                CurrentPart.manufacturer = txtMfg.Text.ToUpper();
                CurrentPart.quantity = Convert.ToInt32(txtQty.Text);
                CurrentPart.datecode = txtDc.Text;
                CurrentPart.rohs_info = txtRohs.Text;
                CurrentPart.packaging = txtPkg.Text;
                CurrentPart.location = txtLoc.Text;
                CurrentPart.internalcomment = txtIc.Text;



                RZDC.SubmitChanges();
                //BindGvSearchResults();
                tools.HandleResultJS("Successfully updated partrecord.", true, Page);
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail",ex.Message);
        }
    }
}