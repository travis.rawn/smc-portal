﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="company_search.aspx.cs" Inherits="secure_sm_company_search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">


        <div class="panel">
            <div class="panel panel-heading">
                <h4>This is a mobile-friendly search of Rz Companies.  It is a work in progress. It's intended to let users search Rz to see if a company exists while on the go, and to add them if they are not.</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="btn btn-sm btn-default btn-block" Style="text-align: left!important" />
                    </div>
                    <div class="col-sm-3">
                        <a href="#" class="btn btn-sm btn-success btn-block" data-toggle="modal" title="Clear" style="vertical-align: middle; color: #ffffff; font-weight: bold;" onclick="return false;">Clear</a>

                    </div>
                </div>
                <div class="row">
                    <div class="col-smm-12">
                        <asp:GridView ID="gvresults" runat="server" CssClass="table table-hover table-striped gvstyling" GridLines="None" AutoGenerateColumns="false">
                            <EmptyDataTemplate>
                                <h4>No Companies Found:</h4>
                                <a href="#" class="btn btn-sm btn-success btn-block" data-toggle="modal" title="Add Company" data-target="#divAddCompanyModal" data-trigger="click" style="vertical-align: middle; color: #ffffff; font-weight: bold;">Add Company  </a>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:BoundField DataField="companyname" HeaderText="Company" SortExpression="compayname" />
                                <asp:BoundField DataField="agentname" HeaderText="Agent" SortExpression="compayname" />
                                <asp:BoundField DataField="companytype" HeaderText="Type" SortExpression="compaytype" />
                                <asp:BoundField DataField="industry_segment" HeaderText="Industry" SortExpression="industry_segment" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>


    <div class="modal fade" id="divAddCompanyModal" tabindex="-1" role="dialog" aria-labelledby="divQuoteModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body">

                    <div>
                        <div class="row clearfix">

                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6  column col-sm-offset-0 col-md-offset-2 col-lg-offset-3">

                                <div class="form-horizontal">
                                    <fieldset>

                                        <!-- Form Name -->
                                        <legend>Add Company</legend>

                                        <!--inline TextBoxes -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="textinput">Company Name</label>
                                            <div class="col-md-3">
                                                <input id="txtCompanyName" name="textinput" type="text" placeholder="placeholder" class="form-control input-md" />
                                            </div>
                                            <label class="col-md-3 control-label" for="textinput">Company Type</label>
                                            <div class="col-md-3">
                                                <select id="ddlCompanyType" name="selectbasic" class="form-control">
                                                    <option value="1">Prospect(P)</option>
                                                    <option value="2">Vendor(V)</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="selectbasic">Industry Segment</label>
                                            <div class="col-md-9">
                                                <select id="ddlIndustrySegment" name="selectbasic" class="form-control">
                                                    <option value="1">Industrial</option>
                                                    <option value="2">Commercial</option>
                                                    <option value="3">blah ...</option>
                                                    <option value="3">This will match Rz</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Button (Double) -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="button1id"></label>
                                            <div class="col-md-8">
                                                <button id="button1id" name="button1id" class="btn btn-success">Add</button>
                                                <button id="button2id" name="button2id" class="btn btn-danger">Cancel</button>
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

