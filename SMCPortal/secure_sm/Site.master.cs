﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{
    MembershipUser CurrentUser;   
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    protected void Page_Init(object sender, EventArgs e)
    {
        // The code below helps to protect against XSRF attacks
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                Response.Redirect("~/Default.aspx", false);
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }



    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (SM_Global.CurrentUser != null)
        {
            //CurrentUser = SM_Security.LoadCurrentUser();


            if (SM_Security.HandleUserSecurityValidation())
            {
                if (CurrentUser.IsLockedOut)
                {
                    Response.Redirect("/Default.aspx", false);
                }
                if (SM_Security.HasTempPassword(CurrentUser) && HttpContext.Current.Request.Url.AbsolutePath != "/Account/PasswordChange.aspx")//Comment 1 = user initiated "Forgot Password"
                {
                    Response.Redirect("/Account/PasswordChange.aspx", false);
                }
                if (SM_Security.IsAgendUserLogin(CurrentUser) && HttpContext.Current.Request.Url.AbsolutePath != "/Account/ForgotPassword.aspx")//Comment 2 = 30 day lockout, password needs to be reset to verify emal ownership.
                {
                    Response.Redirect("~/Account/PasswordReset.aspx", false);
                }
                return;
            }
            else
            {
                ShowCompanyData();
            }
        }

    }

  

    private bool CheckIsTempPwStatus()
    {
        bool ret = false;
        if (CurrentUser.Comment == "1") //Handle the old way for now.
            ret = true;
        if (SM_Security.HasTempPassword(CurrentUser))
            return true;
        return ret;
    }

    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Session.Abandon();
    }

    protected void ShowCompanyData()
    {
        string SMCRep = null;
        if (Profile != null)
        {
            SMCRep = Profile.SensibleRep;
            lblCompanyname.Visible = true;
            lblCompanyname.Text = Profile.CompanyName;
            if (!String.IsNullOrEmpty(Profile.FirstName))
            {
                lblFirstName.Visible = true;
                lblFirstName.Text = "Welcome, " + Profile.FirstName;
                if (Roles.IsUserInRole("admin"))
                    lblCompanyname.Text += "(Admin)";
            }
        }
    }




}