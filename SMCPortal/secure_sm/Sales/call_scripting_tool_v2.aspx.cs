﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using SensibleDAL;
using SensibleDAL.dbml;


public partial class secure_sm_Sales_call_scripting_tool_v2 : System.Web.UI.Page
{
    RzDataContext rdc = new RzDataContext();
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();


    protected void Page_Load(object sender, EventArgs e)
    {
        rz_company_autocomplete.CompanySelected += new EventHandler(rz_company_autocomplete_OnCompanySelected);
        rz_company_autocomplete.ShowContacts = "true";
    }

    protected void rz_company_autocomplete_OnCompanySelected(object sender, EventArgs e)
    {
        company c = null;
        StringBuilder sb = new StringBuilder();
        //handle the event 
        if (!string.IsNullOrEmpty(rz_company_autocomplete.CompanyID))
            c = rdc.companies.Where(w => w.unique_id == rz_company_autocomplete.CompanyID).FirstOrDefault();
        if (c != null)
        {
            if (!string.IsNullOrEmpty(c.primaryphone))
                sb.Append("P: " + c.primaryphone + "<br />");
            if (!string.IsNullOrEmpty(c.primaryfax))
                sb.Append("F: " + c.primaryfax + "<br />");
            if (!string.IsNullOrEmpty(c.primarywebaddress))
                sb.Append("Web: " + c.primarywebaddress + "<br />");
            if (!string.IsNullOrEmpty(c.industry_segment))
                sb.Append("Ind: " + c.industry_segment);

            if (!string.IsNullOrEmpty(sb.ToString()))
                lblDetails.Text = sb.ToString();
            //Get Top 5 MFG
            lblTop5Mfg.Text = GetTop5MFG();
            //Get Top 5 reqs
            lblTop5Parts.Text = GetTop5Reqs();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hideupdateprogress", "$(HideUpdateProgress());", true);
            pnlBody.Visible = true;
        }
    }

    private string GetTop5MFG()
    {

        string ret = null;
        var query = from l in rdc.orddet_quotes
                    where l.manufacturer.Length > 0 && l.base_company_uid == rz_company_autocomplete.CompanyID
                    group l by l.manufacturer into grouping
                    select new { Key = grouping.Key, Count = grouping.Count() };

        var topMFG = from mfg in query.OrderByDescending(o => o.Count).Take(5)
                     orderby mfg.Count descending
                     select mfg;

        foreach (var mfg in topMFG)
        {
            ret += "<li>" + (mfg.Key + "(" + mfg.Count + ")") + "</li>";
            if (mfg != topMFG.AsEnumerable().Last())
                ret += Environment.NewLine;
        }
        return ret;

    }

    private string GetTop5Reqs()
    {
        string ret = null;
        var query = from l in rdc.orddet_quotes
                    where l.fullpartnumber.Length > 0 && l.base_company_uid == rz_company_autocomplete.CompanyID
                    group l by l.fullpartnumber into grouping
                    select new { Key = grouping.Key, Count = grouping.Count() };

        var topReqs = from q in query.OrderByDescending(o => o.Count).Take(5)
                      orderby q.Count descending
                      select q;

        foreach (var r in topReqs)
        {
            ret += "<li>" + (r.Key + "(" + r.Count + ")") + "</li>";
            if (r != topReqs.AsEnumerable().Last())
                ret += Environment.NewLine;
        }
        return ret;
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {

        GetSelectedItems();
        GenerateEmail();
    }

    private List<string> GetSelectedItems()
    {
        List<string> ret = new List<string>();
        if (hfLeadTimes.Value == "true")
            ret.Add("LeadTimes");
        if (hfOnlyFranchise.Value == "true")
            ret.Add("OnlyFranchise");
        if (hfObsolescenceIssues.Value == "true")
            ret.Add("ObsolescenceIssues");
        if (hfNotAddingVendorsAVL.Value == "true")
            ret.Add("NotAddingVendorsAVL");
        if (hfCostReduction.Value == "true")
            ret.Add("CostReduction");
        if (hfSlowRightNow.Value == "true")
            ret.Add("SlowRightNow");
        if (hfStockLiquidation.Value == "true")
            ret.Add("StockLiquidation");
        if (hfQualityCounterfeit.Value == "true")
            ret.Add("QualityCounterfeit");

        string results = string.Join(",", ret);
        if (string.IsNullOrEmpty(results))
            results = "No Items Selected";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alert1", "$(slideAlert('success', '" + results + "', '" + 5000 + "'));", true);
        return ret;
    }

    private void GenerateEmail()
    {
        n_user u = new n_user();
        string cleancompanyname = "No company selected";
        string companyname = "No company selected";
        //string companynameGreet;
        string contactname = "No contact selected";
        string solutions;
        string objections;



        MailMessage Msg = new MailMessage();
        //Msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"]);        
        Msg.From = new MailAddress("scriptdetails@sensiblemicro.com");
        string agentemail = Membership.GetUser().Email;
        Msg.To.Add(agentemail);
        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/ScriptNotesEmail.htm"));
        string readFile = reader.ReadToEnd();
        string StrContent = "";
        StrContent = readFile;

        if (!string.IsNullOrEmpty(rz_company_autocomplete.CompanyID))
        {
            //companynameGreet = rz_company_autocomplete.CompanyName;
            companyname = rz_company_autocomplete.CompanyName;
            cleancompanyname = rzt.GetCleanCompanyName(rz_company_autocomplete.CompanyName);
            StrContent = StrContent.Replace("[CompanyGreet]", cleancompanyname);
        }
        else
        {
            //companynameGreet = "your organizaion";
            cleancompanyname = "your organizaion";
            StrContent = StrContent.Replace("[CompanyGreet]", cleancompanyname);
        }


        if (!string.IsNullOrEmpty(rz_company_autocomplete.ContactID))
        {
            contactname = rz_company_autocomplete.ContactName;
        }


        StrContent = StrContent.Replace("[Company]", companyname);
        StrContent = StrContent.Replace("[Contact]", contactname);


        //Customer Facing email text:        
        //Aggregate Solutions
        List<string> OpportunityNames = new List<string>();
        //Get Opportinities based on extender Clientstate.  True = collapsed
        if (hfLeadTimes.Value == "true") // panel is Expanded.         
        {
            OpportunityNames.Add("Lead Times");
            StrContent = StrContent.Replace("[Lead-time issues]", "<li><b>Shortening your leadtimes</b></li>");
        }
        else
        {
            StrContent = StrContent.Replace("[Lead-time issues]", "");
        }

        if (hfObsolescenceIssues.Value == "true") // panel is Expanded.         
        {
            OpportunityNames.Add("Obsolescence Issues");
            StrContent = StrContent.Replace("[Obsolescence issues]", "<li><b>Helping with Obsoletes</b></li>");
        }
        else
        {
            StrContent = StrContent.Replace("[Obsolescence issues]", "");
        }
        if (hfCostReduction.Value == "true") // panel is Expanded.         
        {
            OpportunityNames.Add("Cost Reduction");
            StrContent = StrContent.Replace("[Cost Reduction]", "<li><b>Lowering Costs</b></li>");
        }
        else
        {
            StrContent = StrContent.Replace("[Cost Reduction]", "");
        }

        if (hfStockLiquidation.Value == "true") // panel is Expanded
        {
            OpportunityNames.Add("Stock Liquidation");
            //StrContent = StrContent.Replace("[StrategicMats]", "<li>Stock Liquidation (Excess or Consignment)</li>");
            StrContent = StrContent.Replace("[Stock Liquidation]", "<li><b>Stock Liquidation</b></li>");
        }
        else
        {
            //StrContent = StrContent.Replace("[StrategicMats]", "");
            StrContent = StrContent.Replace("[Stock Liquidation]", "");
        }

        List<string> ObjectionsNames = new List<string>();
        if (hfOnlyFranchise.Value == "true") // panel is Expanded
        {
            ObjectionsNames.Add("Only Franchise");
            StrContent = StrContent.Replace("[OnlyFranchise]", "<li>You mentioned a few concerns, which I certainly can understand, in dealing with a new vendor. Many of our customers prefer to only purchase thru authorized distribution.This offers a layer of protection which I certainly appreciate you looking for. We offer this same type of protection to our customers with our In house test lab and IDEA Std 1010 Inspection process. <a href=\"http://www.sensiblemicro.com/lab-analysis-engineering/\" >Here is some more info on our Lab.</a> </li>");
        }
        else
        {
            StrContent = StrContent.Replace("[OnlyFranchise]", "");
        }
        if (hfNotAddingVendorsAVL.Value == "true") // panel is Expanded
        {
            ObjectionsNames.Add("Not adding to AVL");
        }
        if (hfSlowRightNow.Value == "true") // panel is Expanded
        {
            ObjectionsNames.Add("Slow right now");
            //StrContent = StrContent.Replace("[Business is slow]", "<li><b>Excess Opportunities</b> - Feel free to take a look at <a href=\"http://www.sensiblemicro.com/inventory-management.aspx\" target="_blank">our excess and revenue sharing programs.</a></li>");
            StrContent = StrContent.Replace("[Business is slow]", "<li>You mentioned you guys were a little slow about now, which seems to be a lingering trait in the industry right now. We are seeing many of our customers utilizing our Cost reduction programs or Excess inventory solutions. <a href=\"http://sensiblemicro.com/surplus-management/\" >Here is some information on our Excess and Consignment programs.</a> </li>");
        }
        else
        {
            StrContent = StrContent.Replace("[Business is slow]", "");
        }
        if (hfQualityCounterfeit.Value == "true") // panel is Expanded
        {
            ObjectionsNames.Add("Concerned about Quality / Counterfeit");
            //StrContent = StrContent.Replace("[Quality concerns]", "<li><b>Quality Issues</b> - Quality product and counterfeit mitigation are core tenants to Sensible Micro's procurement strategy.  We perform a rigorous 98 point, IDEA 10.10 compliant inspection on&nbsp;<b><u>Each and every incoming product</b></ui> to ensure our customers only get quality components every time.  We pride ourselves on identifying and quarantining substandard product, effectively eliminating the risk of counterfeit components polluting our customers' supply chain.  Can your other distributors do that?  We invite you to take a virtual tour of our world class <a href=\"http://www.sensiblemicro.com/lab-analysis-engineering/\" > test lab.</a>  While you're there, you can grab a a sample copy of our General Counterfeit Avoidance Test (GCAT) report.</li><br /><br />");
            StrContent = StrContent.Replace("[Quality concerns]", "<li>Quality is something we take very serious here at Sensible Micro. Our team is ISO9001, AS9120, IDEA certified to handle all incoming inspections. All parts go thru a full 72point inspection which includes:<ul><li> Full Visual Inspection</li><li>Digital Imagery</li><li>Marking Permanency</li><li>Dimensional verification (using digital calipers)</li></ul>This is for EVERY part that comes thru the door, no matter where it was purchased from. We also offer advanced inspection capabilities for high reliability or high risk parts. <a href=\"http://www.sensiblemicro.com/lab-analysis-engineering/\" >Click here to learn more.</a>");

        }
        else
        {
            StrContent = StrContent.Replace("[Quality concerns]", "");
        }



        //Concatenate Solutions into variable for use with template placeholder
        solutions = string.Join(", ", OpportunityNames);
        objections = string.Join(", ", ObjectionsNames);

        //Show solutions flagged during call
        if (OpportunityNames.Count > 0)
        {
            StrContent = StrContent.Replace("[Opportunities]", solutions);
            //StrContent = StrContent.Replace("[SolutionsBody]", "<b><u>Solutions:</u></b><br />");
        }
        else
        {
            StrContent = StrContent.Replace("[Opportunities]", "No solution opportunities logged during call.");
            //StrContent = StrContent.Replace("[SolutionsBody]", "");
        }
        //Show objections flagged during call
        if (ObjectionsNames.Count > 0)
        {
            StrContent = StrContent.Replace("[Objections]", objections);
            StrContent = StrContent.Replace("[ObjectionsBody]", "<b><u>Objections:</u></b><br />");

        }
        else
        {
            StrContent = StrContent.Replace("[Objections]", "No objections logged during call.");
            //StrContent = StrContent.Replace("[ObjectionsBody]", "");
        }

        //Introduction paragraph
        StrContent = StrContent.Replace("[Content]", "Hello, <br /><br />Thanks again for taking the time to speak to me today, I appreciate your time.");
        if (OpportunityNames.Count > 0 && ObjectionsNames.Count > 0)
        {
            StrContent = StrContent.Replace("[ContentFill]", "  Judging by our conversation,  I feel we would be an excellent fit to help you out in a few ways:");
        }
        else
        {
            StrContent = StrContent.Replace("[ContentFill]", "");
        }



        //Send Email Code
        Msg.Subject = "Call Details for " + companyname + " - " + DateTime.Now;
        Msg.Body = StrContent.ToString();
        Msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        //smtp.Host = ConfigurationManager.AppSettings["MailServer"];
        //smtp.Port = int.Parse(ConfigurationManager.AppSettings["MailPort"]);
        try
        {
            smtp.Send(Msg);
            //lblSuccess.Text = "Email Sent! Check your inbox";
            tools.HandleResult("success", "Email has been sent to your inbox.");
        }
        catch (Exception ex)
        {
            //if (ex.Message != null)
            //lblError.Visible = true;
            tools.HandleResult("success", ex.Message);
            //lblError.Text = ex.Message.ToString();
        }
    }
}