﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Sales_rfq_entry : System.Web.UI.Page
{
    List<n_user> RzAgentList;
    string rzID;
    //n_user QuotingAgent;
    List<orddet_quote> TodaysReqs;
    companycontact TheContact;
    company TheCompany;
    partrecord ThePartRecord;
    string ThePartNumber;
    SM_Tools tools = new SM_Tools();

    protected void Page_Load(object sender, EventArgs e)
    {
        rzca.ContactSelected += new EventHandler(OnContactSelected);
        if (!Page.IsPostBack)
            LoadAgentPicker();
        LoadTodaysRfqs();
    }

    private void LoadAgentPicker()
    {
        using (RzDataContext rdc = new RzDataContext())
            RzAgentList = rdc.n_users.Where(w => w.is_inactive != true).ToList();
        rzap.autoPostBack = true;
        rzap.LoadPicker(RzAgentList);
        rzap.SelectedUserID = "all";
    }

    private void LoadTodaysRfqs()
    {
        
        string philID = "2f948876da1a47a8b37e954c26e27892";
        if (!string.IsNullOrEmpty(rzap.SelectedUserID) && rzap.SelectedUserID != "all")
            rzID = rzap.SelectedUserID;
        else
        {
            rzID = philID;  
            //rzID = SM_Global.CurrentProfile.RzUserID;
            rzap.SelectedUserID = SM_Global.CurrentProfile.RzUserID;
        }


        //Or MAnually override to phil scott for testing

        if (string.IsNullOrEmpty(rzID))
        {
            tools.HandleResult("fail", "No Rz Agent ID detected for this Portal User.  Please notify IT support.");
            return;
        }

        ViewState["QuotingAgentID"] = rzID;
        using (RzDataContext rdc = new RzDataContext())
        {
            TodaysReqs = rdc.orddet_quotes.Where(w => w.date_created.Value.Date == DateTime.Today.Date && w.base_mc_user_uid == rzID).ToList();
           
        }
            

        pnlTodaysRfqs.Visible = true;
        smdt.emptyDataText = "No quotes today.";
        smdt.dataSource = TodaysReqs.Select(s => new { s.fullpartnumber, s.manufacturer, s.quantity, s.companyname, s.contactname, s.target_price });
        smdt.loadGrid();



    }

    protected void OnContactSelected(object sender, EventArgs e)
    {
        string contactID = rzca.ContactID;
        if (string.IsNullOrEmpty(contactID))
            return;
        //Lookup the company, list in modal, or dropdown.
        List<company> matchedCompanies = new List<company>();
        List<string> companyIdsForContact = new List<string>();
        using (RzDataContext rdc = new RzDataContext())
        {
            //FOr now let's assume we are able to identify a single contact for simplicity, though I am sure we have a few duplicate emails under different companies.
            TheContact = rdc.companycontacts.Where(w => w.unique_id == contactID).FirstOrDefault();
            TheCompany = rdc.companies.Where(w => w.unique_id == TheContact.base_company_uid).FirstOrDefault();
        }
        lblSelectedCompanyName.Text = TheCompany.companyname;

        //Show the detail entry panel
        pnlRfqDetail.Visible = true;

    }



    protected void lbSaveRfq_Click(object sender, EventArgs e)
    {
        
      
        string agentID = ViewState["QuotingAgentID"].ToString();
        //Create a batch
        using (RzDataContext rdc = new RzDataContext())
        {
            n_user u = rdc.n_users.Where(w => w.unique_id == agentID).FirstOrDefault();
            if (u == null)
                return;            

            //Create the dealheader object
            dealheader d = new dealheader();   
            rdc.dealheaders.InsertOnSubmit(d);
            d.date_created = DateTime.Now;
            d.unique_id = Guid.NewGuid().ToString();
            d.base_mc_user_uid = u.unique_id;
            d.agentname = u.name;
            //Get next batch ID
            d.dealheader_name = SensibleDAL.RzLogic.OrderBatch.GetNextBatchNumberLinq().ToString();
            d.contact_uid = TheContact.unique_id;
            d.contact_name = TheContact.contactname;
            //d.companyid = TheCompany.unique_id;
            d.customer_name = TheCompany.companyname;
            d.customer_uid = TheCompany.unique_id;

            //Create the quote object
            orddet_quote q = new orddet_quote();
            rdc.orddet_quotes.InsertOnSubmit(q);
            q.date_created = DateTime.Now;
            q.base_mc_user_uid = u.unique_id;
            q.agentname = u.name;
            q.companyname = TheCompany.companyname;
            q.base_company_uid = TheCompany.unique_id;
            q.contactname = TheContact.contactname;
            q.split_commission_agent_name = TheCompany.split_commission_agent_name;
            q.split_commission_agent_uid = TheCompany.split_commission_agent_uid;
            


            rdc.SubmitChanges();
        }

        //Refresh the grid
        LoadTodaysRfqs();


    }
}