﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_sm_Sales_CallScriptingTool : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            string User = Membership.GetUser().ToString();
            string CompanyUser = "";
            if (User == "kevint")
            { CompanyUser = "%"; }
            else
            {
                CompanyUser = User;
            }
            RzDataContext RZDC = new RzDataContext();
            ddlCompany.Items.Add(new ListItem("--Choose--", "0"));
            ddlCompany.AppendDataBoundItems = true;
            ddlCompany.DataSource = RZDC.SMCPortal_CallScripting_ddlCompany(CompanyUser);
            ddlCompany.DataTextField = "companyname";
            ddlCompany.DataValueField = "unique_id";
            ddlCompany.DataBind();
            ddlCompany.SelectedIndex = 0;


            ddlContact.Items.Add(new ListItem("--Choose Company--", "0"));
            ddlContact.AppendDataBoundItems = true;
            ddlContact.DataSource = RZDC.SMCPortal_CallScripting_ddlContact(ddlCompany.SelectedValue);
            ddlContact.DataTextField = "contactname";
            ddlContact.DataValueField = "Email";
            ddlContact.DataBind();
            ddlContact.Visible = true;
            ddlContact.SelectedIndex = 0;
        }
        ddlContact.DataSource = ddlCompany.SelectedValue;

    }
    protected void btnLead_Click(object sender, EventArgs e)
    {
        //if (cpeOppLead.Collapsed == false)
        //{
        //}
        //else
        //{
        //}
    }



    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        blstProducts.DataSource = "";
        blstProducts.DataBind();

        blstTop5MFG.DataSource = "";
        blstTop5MFG.DataBind();

        RzDataContext RZDC = new RzDataContext();
        ddlContact.Items.Clear();
        ddlContact.Items.Add(new ListItem("--Choose--", "0"));
        ddlContact.AppendDataBoundItems = true;
        ddlContact.DataSource = RZDC.SMCPortal_CallScripting_ddlContact(ddlCompany.SelectedValue);
        ddlContact.DataTextField = "contactname";
        ddlContact.DataValueField = "Email";
        ddlContact.DataBind();
        ddlContact.Visible = true;
        ddlContact.SelectedIndex = 0;

        //Gather Other infomation on the company

        List<string> ProductTypeList = new List<string>();
        List<string> Top5MFGList = new List<string>();
        List<string> RecentPartsList = new List<string>();

        //Gather Product types
        var Products =
            from cd in RZDC.companies
            where (cd.unique_id == ddlCompany.SelectedValue)
            select cd;
        foreach (company company in Products)
        {

            if (company.Products_Cabling == true)
                ProductTypeList.Add("Cabling");
            if (company.Products_CrystalOsc == true)
                ProductTypeList.Add("Crystal Oscillators");
            if (company.Products_Display == true)
                ProductTypeList.Add("Displays");
            if (company.Products_Interconnect == true)
                ProductTypeList.Add("Interconnects");
            if (company.Products_OpticalTransceiver == true)
                ProductTypeList.Add("Optical Transceivers");
            if (company.Products_PowerSupply == true)
                ProductTypeList.Add("Power Supplies");
            if (company.Products_Relay == true)
                ProductTypeList.Add("Relays");
            if (company.Products_SSD == true)
                ProductTypeList.Add("SSD Drives");
        }
        if (ProductTypeList.Count > 0)
        {
            lblProductList.Visible = true;
            blstProducts.DataSource = ProductTypeList;
            blstProducts.DataBind();
        }

        //Top 5 MFG
        var query = from l in RZDC.orddet_quotes
                    where l.manufacturer.Length > 0 && l.base_company_uid == ddlCompany.SelectedValue
                    group l by l.manufacturer into grouping
                    select new { Key = grouping.Key, Count = grouping.Count() };

        var topMFG = from mfg in query.Take(5)
                     orderby mfg.Count descending
                     select mfg;

        foreach (var mfg in topMFG)
        {
            RecentPartsList.Add(mfg.Key + "(x"+ mfg.Count+")");
        }
        if (RecentPartsList.Count > 0)
        {
            lblTop5MFG.Visible = true;
            blstTop5MFG.DataSource = RecentPartsList;
            blstTop5MFG.DataBind();

        }
    }
    protected void ddlContact_SelectedIndexChanged(object sender, EventArgs e)
    {



    }
    protected void btnEmail_Click(object sender, EventArgs e)
    {
        lblSuccess.Text = "";
        n_user u = new n_user();
        string companyname = "No company selected";
        string companynameGreet;
        string contactname = "No contact selected";
        string solutions;
        string objections;



        //Compose Email of containing call results
        if (ddlCompany.SelectedIndex != 0)
        {
            companyname = ddlCompany.SelectedItem.ToString();
        }

        if (ddlCompany.SelectedIndex != 0)
        {
            companynameGreet = ddlCompany.SelectedItem.ToString();
        }
        {
            companynameGreet = "you";
        }
        if (ddlContact.Visible == true)
        {
            if (ddlContact.SelectedItem != null)
            {
                contactname = ddlContact.SelectedItem.ToString();
            }
        }



        MailMessage Msg = new MailMessage();
        //Msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"]);        
        Msg.From = new MailAddress("scriptdetails@sensiblemicro.com");
        string agentemail = Membership.GetUser().Email;
        Msg.To.Add(agentemail);
        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/ScriptNotesEmail.htm"));
        string readFile = reader.ReadToEnd();
        string StrContent = "";
        StrContent = readFile;

        if (ddlCompany.SelectedIndex != 0)
        {
            companynameGreet = ddlCompany.SelectedItem.ToString();
            StrContent = StrContent.Replace("[CompanyGreet]", companynameGreet);
        }
        else
        {
            companynameGreet = "your organizaion";
            StrContent = StrContent.Replace("[CompanyGreet]", companynameGreet);
        }
        StrContent = StrContent.Replace("[Company]", companyname);
        StrContent = StrContent.Replace("[Contact]", contactname);


        ////Customer Facing email text:        
        ////Aggregate Solutions
        //List<string> OpportunityNames = new List<string>();
        ////Get Opportinities based on extender Clientstate.  True = collapsed
        //if (cpeOppLead.ClientState != "true") // panel is Expanded.         
        //{
        //    OpportunityNames.Add("Lead Times");
        //    StrContent = StrContent.Replace("[LeadMats]", "<li><b>Reducing Lead Times</b></li>");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[LeadMats]", "");
        //}

        //if (cpeOppObs.ClientState != "true") // panel is Expanded.         
        //{
        //    OpportunityNames.Add("Obsolescence Issues");
        //    StrContent = StrContent.Replace("[ObsMats]", "<li><b>Obsolescence Support</b></li>");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[ObsMats]", "");
        //}
        //if (cpeOppObs.ClientState != "true") // panel is Expanded.         
        //{
        //    OpportunityNames.Add("Cost Reduction");
        //    StrContent = StrContent.Replace("[CostMats]", "<li><b>Cost Reduction</b></li>");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[CostMats]", "");
        //}

        //if (cpeOppStrategic.ClientState != "true") // panel is Expanded
        //{
        //    OpportunityNames.Add("Strategic Solutions");
        //    StrContent = StrContent.Replace("[StrategicMats]", "<li>Strategic Solutions</li>");
        //    StrContent = StrContent.Replace("[StrategicSolutions]", "<b>Strategic Solutions Forms <u>(Internal Use Only)</u>:</b><li><a href=\"http://forms.sensiblemicro.com/view.php?id=14654\" target=\"_blank\">Display Form</a></li><li><a href=\"http://forms.sensiblemicro.com/view.php?id=14166\" target=\"_blank\">SSD Form</a></li><li><em>Strategic Form coming soon ...</em><br /><li><a href=\"http://forms.sensiblemicro.com/view.php?id=14773\" target=\"_blank\" >Optical Transceivers Form</a></li>");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[StrategicMats]", "");
        //    StrContent = StrContent.Replace("[StrategicSolutions]", "");
        //}

        //List<string> ObjectionsNames = new List<string>();
        //if (cpeObjFranchise.ClientState != "true") // panel is Expanded
        //{
        //    ObjectionsNames.Add("Only Franchise");
        //}
        //if (cpeObjAVL.ClientState != "true") // panel is Expanded
        //{
        //    ObjectionsNames.Add("Not adding to AVL");
        //}
        //if (cpeObjSlow.ClientState != "true") // panel is Expanded
        //{
        //    ObjectionsNames.Add("Slow right now");
        //    StrContent = StrContent.Replace("[SlowMats]", "<li><b>Excess Opportunities</b></li><ul><li>In the event that you are looking to liquidate excess to improve your bottom line, please take a moment to review <a href=\"http://www.sensiblemicro.com/inventory-management.aspx\">our excess and revenue sharing programs.</a></li></ul>");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[SlowMats]", "");
        //}
        //if (cpeObjQuality.ClientState != "true") // panel is Expanded
        //{
        //    ObjectionsNames.Add("Concerned about Quality / Counterfeit");
        //    StrContent = StrContent.Replace("[QualityMats]", "<li><b>Quality Issues</b></li><ul><li>Quality product and counterfeit mitigation are core tenants to Sensible Micro's procurement strategy.  We perform a rigorous 98 point, IDEA 10.10 compliant inspection on&nbsp;<b><u>Each and every incoming product</b></ui> to ensure our customers only get quality components every time.  We pride ourselves on identifying and quarantining substandard product, effectively eliminating the risk of counterfeit components polluting our customers' supply chain.  Can your other distributors do that?  We invite you to take a virtual tour of our world class <a href=\"http://www.sensiblemicro.com/quality/testing-services.aspx\">test lab</a>, as well as to download a sample copy of our General Counterfeit Avoidance Test (GCAT) report  <a href=\"https://portal.sensiblemicro.com/pdf/Sample Lab Report.pdf\">here</a>.</li></ul><br /><br />");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[QualityMats]", "");
        //}



        ////Concatenate Solutions into variable for use with template placeholder
        //solutions = string.Join(", ", OpportunityNames);
        //objections = string.Join(", ", ObjectionsNames);

        ////Show solutions flagged during call
        //if (OpportunityNames.Count > 0)
        //{
        //    StrContent = StrContent.Replace("[Opportunities]", solutions);
        //    //StrContent = StrContent.Replace("[SolutionsBody]", "<b><u>Solutions:</u></b><br />");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[Opportunities]", "No solution opportunities logged during call.");
        //    //StrContent = StrContent.Replace("[SolutionsBody]", "");
        //}
        ////Show objections flagged during call
        //if (ObjectionsNames.Count > 0)
        //{
        //    StrContent = StrContent.Replace("[Objections]", objections);
        //    StrContent = StrContent.Replace("[ObjectionsBody]", "<b><u>Objections:</u></b><br />");

        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[Objections]", "No objections logged during call.");
        //    //StrContent = StrContent.Replace("[ObjectionsBody]", "");
        //}

        ////Introduction paragraph
        //StrContent = StrContent.Replace("[Content]", "Hello, " + Profile.FirstName + " here, <br /><br />Thanks again for taking the time to speak to me today, I know you're busy, so I'll keep it brief.  I believe we would be an excellent fit to assist "+companynameGreet+" in a few key aspects of your business model.");
        //if (OpportunityNames.Count > 0 && ObjectionsNames.Count > 0)
        //{
        //    StrContent = StrContent.Replace("[ContentFill]", "  Based on our chat, I think there's an immediate opportunity to assist " + companynameGreet + "in the following areas:");
        //}
        //else
        //{
        //    StrContent = StrContent.Replace("[ContentFill]", "");
        //}



        //Send Email Code
        Msg.Subject = "Call Details for " + companyname + " - " + DateTime.Now;
        Msg.Body = StrContent.ToString();
        Msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        //smtp.Host = ConfigurationManager.AppSettings["MailServer"];
        //smtp.Port = int.Parse(ConfigurationManager.AppSettings["MailPort"]);
        try
        {
            smtp.Send(Msg);
            lblSuccess.Text = "Email Sent! Check your inbox";
        }
        catch (Exception ex)
        {
            if (ex.Message != null)
                lblError.Visible = true;
            lblError.Text = ex.Message.ToString();
        }
    }
}