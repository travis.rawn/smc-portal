﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="rfq_entry.aspx.cs" Inherits="secure_sm_Sales_rfq_entry" %>

<%@ Register Src="~/assets/controls/rz_contact_autocomplete.ascx" TagPrefix="uc1" TagName="rz_contact_autocomplete" %>
<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/RzAgentPicker.ascx" TagPrefix="uc1" TagName="RzAgentPicker" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlCompanyContact" runat="server">
        <div class="row">
            <div class="4">
                <label>Choose the Rz Agent for this req:</label><br />
                <uc1:RzAgentPicker runat="server" ID="rzap" />
            </div>
            <div class="col-sm-4">
                <div class="form-inline">
                    <div class="form-group mb-2">
                        <label>Search for the Rz Contact by Email:</label><br />
                        <uc1:rz_contact_autocomplete runat="server" ID="rzca" />
                    </div>
                    <%--<asp:LinkButton ID="lbChooseContactEmail" runat="server" OnClick="lbChooseContactEmail_Click">Select</asp:LinkButton>--%>
                </div>
            </div>
            <div class="col-sm-4">
                <asp:Label ID="lblSelectedCompanyName" runat="server">No Company Selected.</asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlRfqDetail" runat="server" Visible="false">
        <div class="form-inline">
            <div class="form-group mb-2">
                <label>Part Number:</label><br />
                <asp:TextBox ID="tbPartNumber" runat="server"></asp:TextBox>
            </div>
            <div class="form-group mb-2">
                <label>QTY:</label><br />
                <asp:TextBox ID="txtQTY" runat="server"></asp:TextBox>
            </div>
            <div class="form-group mb-2">
                <label>MFG:</label><br />
                <asp:TextBox ID="txtMFG" runat="server"></asp:TextBox>
            </div>
             <div class="form-group mb-2">
                <label>Target Price:</label><br />
                <asp:TextBox ID="txtTargetPrice" runat="server"></asp:TextBox>
            </div>
             <div class="form-group mb-2">
                <label>Save Rfq:</label><br />
             <asp:LinkButton ID="lbSaveRfq" runat="server" OnClick="lbSaveRfq_Click">Save</asp:LinkButton>
            </div>
           
        </div>
    </asp:Panel>

    <div class="row">
        <asp:Panel ID="pnlTodaysRfqs" runat="server" Visible="false">
            <label>Today's RFQ's</label><br />
            <uc1:sm_datatable runat="server" ID="smdt" />
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

