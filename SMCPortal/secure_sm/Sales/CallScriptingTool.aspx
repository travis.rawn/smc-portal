﻿<%@ Page Title="Call Scripting Tool" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="CallScriptingTool.aspx.cs" Inherits="secure_sm_Sales_CallScriptingTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="callscripting.css" rel="stylesheet" />
    <script src="CallScripting.js"></script>

    <style type="text/css">
        .auto-style1 {
            width: 171px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
           
            <%--Button Color JS--%>
            <script>
                function setColor(btn, color) {

                    var property = document.getElementById(btn);
                    if (window.getComputedStyle(property).backgroundColor != color) {
                        property.style.backgroundColor = color;
                    }
                    else {
                        property.style.backgroundColor = 'rgb(42, 48, 60)';
                    }
                }
            </script>
            <%--End Button Color JS--%>

            <asp:Label ID="lblError" runat="server" ForeColor="#B20000" Visible="false"></asp:Label>
            <asp:Label ID="lblSuccess" runat="server" Font-Bold="True" ForeColor="#33CC33"></asp:Label>
            <a href="http://sm1/Reports_SQLEXPRESS/Pages/Report.aspx?ItemPath=%2fRz4+Reports%2fSales%2fCustom+Solutions+Product+Types+by+Company" target="_blank">
                <asp:Label ID="lblCSReport" Text="Click here to View Custom Solutions by Company Report" runat="server"></asp:Label></a>
            <table style="width: 100%">
                <tr>
                    <th>
                        <asp:Label ID="lblSelectCompany" runat="server" Text="Select Company:"></asp:Label>
                    </th>
                    <th style="padding-right: 10%;">
                        <asp:Label ID="lblProductList" runat="server" Text="Products used:"></asp:Label>
                    </th>
                    <th style="padding-right: 10%;">
                        <asp:Label ID="lblTop5MFG" runat="server" Text="Top 5 MFG(by Count):"></asp:Label>
                    </th>
                    <%-- <th>
                        <asp:Label ID="lblLineGP" runat="server" Text="Top 5 Lines by GP:"></asp:Label>
                    </th>--%>
                </tr>
                <tr style="vertical-align: top">
                    <td style="text-align: left;">

                        <asp:DropDownList ID="ddlCompany" runat="server" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="true" Width="200px"></asp:DropDownList><br />
                        <asp:DropDownList ID="ddlContact" runat="server" OnSelectedIndexChanged="ddlContact_SelectedIndexChanged" Width="200px"></asp:DropDownList><br />
                    </td>
                    <td style="text-align: left;">
                        <asp:BulletedList Style="line-height: 15px;" ID="blstProducts" runat="server"></asp:BulletedList>
                    </td>
                    <td style="text-align: left;">
                        <asp:BulletedList Style="line-height: 15px;" ID="blstTop5MFG" runat="server"></asp:BulletedList>
                    </td>
                    <%--<td>
                        <asp:BulletedList Style="line-height: 15px;" ID="blstLineGP" runat="server"></asp:BulletedList>
                    </td>--%>
                </tr>
            </table>
            <table style="width: 100%; background-color: lightgray; text-align: center">
                <tr>
                    <td>
                        <span><a href="#" class="tooltipLeft" onclick="return false">Intro Script Example
                                    <span style="text-align: justify">Hey is this Jon?<br />
                                        <br />
                                        Perfect…maybe you can help me out, or point me in the right direction…reason I was calling is I work with a few companies that build a real similar product to yours, typically they are coming to me for help with shortages, obsoletes, lead time issues, and that sort of thing….Being that you build a similar product, I assume you run into the same sort of issues?
                                    </span></a></span>
                    </td>
                    <td>
                        <span><a href="#" class="tooltipRight" onclick="return false">Current News
                                    <span style="text-align: left">
                                        <b>ON Semiconductor</b><br />
                                        <li>Onsemi just announce a LTB on all their TO92 packages. The LTB is 12/31…</li>
                                        <li>NXP 74AHC series 36+ weeks</li>
                                        <li>Aluminum Electrolytic caps up to 28wks (NIC, Panasonic, Nippon Chemicon)</li>
                                        <li>Inductors up to 56wks (Murata, AVX, TDK)</li>
                                    </span></a></span>
                    </td>

                    <td>
                        <span><a href="#" class="tooltipRight" onclick="return false">General Call Tips
                                    <span style="text-align: left">Have real conversations that are impactful, stop making “sales calls”.  Adding value turns heads, sounding like an expert on a topic such as counterfeits will quickly elevate your status from annoying sales person to a trusted business partner.<br />
                                        <br />
                                        <b>Interject thought provoking questions (that also support our business strengths) into your conversation such as:</b><br />
                                        <li><i>"When it comes to counterfeit avoidance, how familiar are you with best practices & inspection protocols?</i><br>
                                            <br>

                                            <li><i>&quot;Who is currently re-certifying the authenticity of your components now?&quot;<br />
                                                <br />
                                                <b>Make bold statements, filled with confidence and back it up with facts:</b><br />
                                                <li><i>I appreciate that you already have your set suppliers and are happy with them. However, I truly believe our company offers something uniquely different. In fact, I don’t know of any distributor in the world who has both the in-house testing capability, technical oversight and product insurance binders that we do. Are you aware of all the types of test equipment we have in-house that certifies the integrity of parts being purchased? </i></li>
                                            </i></li>
                                    </span></a></span>


                    </td>
                </tr>
            </table>

            <asp:Button ID="btnEmail" runat="server" Text="Generate Email" OnClick="btnEmail_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
        <ContentTemplate>


            <div class="ColumnLeft">
                <h3 style="display: inline;"><b>Opportunities:</b></h3>
                <span>
                    <asp:Panel ID="pnlOpportunities" runat="server">
                        <input type="button" id="btnOppLead" class="Scriptbutton" value="Lead Times" onclick="setColor('btnOppLead', 'rgb(25, 139, 7)');" />
                        <asp:Panel ID="pnlOppLead" runat="server">
                            <ul>
                                <li><span><a href="#" class="tooltipLeft" onclick="return false">Immediate Sourcing
                                    <span>One of the values of Sensible Micro is we having the ability to source the components you need <b>via our large breadth of Stock and Excees partnerships</b>.</span></a></span>

                                </li>
                                <li><span><a href="#" class="tooltipLeft" onclick="return false">Sensible Relationships
                                    <span>Many times, we are able to leverage our relationships though <b>factory channels</b> which allows us to get material <b>faster than current lead times dictate.</b></span></a></span></li>
                            </ul>
                        </asp:Panel>
                       
                        <input type="button" id="btnOppObs" class="Scriptbutton" value="Obsolescence Issues" onclick="setColor('btnOppObs', 'rgb(25, 139, 7)');" />
                        <asp:Panel ID="pnlOppObs" runat="server">
                            <ul>
                                <li>

                                    <span><a href="#" class="tooltipLeft" onclick="return false">Identify Manufacturers
                                    <span><b>Which manufacturers do they work usually with?</b><br />
                                        <br />

                                        <b><em>Joey's Corner:</em></b><br />
                                        <i>"Many of our customers have longer lifecycle products  ...  we see this a lot ...  I have a similar customers seeing this with "Atmel, Motorola, Freescale" (See top 5 MFG List above fro MFG's that this company uses)"</i>
                                    </span></a></span>
                                </li>
                                <li>

                                    <span><a href="#" class="tooltipLeft" onclick="return false">Cross Referencing
                                    <span>Through our <b>franchised lines</b>, we have the ability to offer <b>drop-in replacements for obsolete parts.</b><br />
                                        <br />
                                        <b><em>Joey's Corner:</b><br />
                                        "I actually just helped a customer of mine ... they had a Motorola processor that had gone EOL, and they had another 5 years of product lifecycle left.  So we had one of our franchise lines, Tekmos, who specialized in obsolete microcontrollers.  We were able to offer a factory-direct, drop-in replacement which turned out to be a much more cost and time-effective solution.  They were very happy."</em>
                                    </span></a></span>

                                </li>
                            </ul>
                        </asp:Panel>
                     

                        <input type="button" id="btnOppCost" class="Scriptbutton" value="Cost Reduction" onclick="setColor('btnOppCost', 'rgb(25, 139, 7)');" />
                        <asp:Panel ID="pnlOppCost" runat="server">
                            <ul>
                                <li><u><a href="#" class="tooltipLeft" onclick="return false">Scheduled/Blanket ordering
                                    <span>
                                        <strong>Some good cost-saving MFG's</strong><br />
                                        <ul>
                                            <li>Microchip
                                            </li>
                                            <li>Xilinx
                                            </li>
                                            <li>Linear Tech
                                            </li>
                                            <li>Texas Instruments
                                            </li>
                                            <li>Atmel
                                            </li>
                                        </ul>
                                        <br />
                                        <b>Joey's corner:</b><br />
                                        <i>“I actually had one of my real good customers who was doing a LTB (last time buy) on an Atmel chip, I think it was a AT29 series... and we were able to save them about 15% on a LTB and offered them a schedule so they did not have to lay the full amount out as they would if they bought direct from Mfr or thru authorized distribution..."</i>
                                </a>
                                    <li><u><a href="#" class="tooltipLeft" onclick="return false">Authorized lines
                                    <span>Many times on commodity parts (relays, crystals, connectors, capacitors, etc.) we can offer one of our auhthorized lines and show immediate, significant cost-savings against the components you are currently buying.  We can offer full trace and schduling on these as well.<br />
                                        <br />
                                        <b><em>Joey's corner:</b><br />"I was actually just able to help one of my ---insert Company's industry here--- customers with a Citizen crystal oscillator that they had been buying through distribution at $1.24, and we were able to offer our Cardinal brand at $.89.  We provide them around 200K per year, which nets them a huge cost savings, something like $60K / year.</em>
                                    </a>
                            </ul>
                        </asp:Panel>
                   
                        <input type="button" id="btnOppStrategic" class="Scriptbutton" value="Strategic Solutions" onclick="setColor('btnOppStrategic', 'rgb(25, 139, 7)');" />

                        <asp:Panel ID="pnlOppStrategic" runat="server" Width="100%">



                            <table>
                                <%--Begin Display Block--%>
                                <tr>
                                    <td>
                                        <a href="#" class="tooltipLeft">Displays
                                        <span><b>We can offer TFTs, LCDs, OLEDs, TFEL and integrated touch solutions.</b><br />
                                            <br />
                                            <li><b>Size</b> – diagonal size of the display</li>
                                            <li><b>Resolution</b> – this is the pixel count for the vertical and horizontal axes</li>
                                            <li><b>Minimum Brightness</b> – usually measured by nits or cd/m^2, this is important especially for outdoor applications</li>
                                            <li><b>Temperature Range</b> – there should be a specified range for both operating and storage</li>
                                            <li><b>Ask if they just need the display or an integrated solution</b>.  We can offer touch, bonding and coverglass.</li>
                                        </span>
                                        </a>
                                    </td>
                                    <td align="center" class="auto-style1">
                                        <a href="http://forms.sensiblemicro.com/view.php?id=14654" target="_blank" class="AnchorButton">Displays Form</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>-- <a href="#" class="tooltipLeft">Lumineq
                                            <span><b>Lumineq Info & Selling points</b><br />
                                                <br />
                                                <li><b>Rugged</b> - Look for prospects with applications that require no-service, high-reliability displays</li>
                                                <li><b>Repair Costs</b> - Spending a lot of money repairing or maintaining existing displays?</li>
                                                <li><b>Proven</b> - Some existing customers have been using these screens for 10+ years with nearly ZERO failure rate</li>
                                                <li><b>Popular Product</b> - Their "Quarter VGA" (EL 320.240.36-HB series) is one of their more popular</li>
                                                <li><b>Warranty</b> - Most of their lines carry a 10+ year warranty</li>
                                                <li><b>Ultra-Wide Viweing angle</b> - Good when the screen needs to be visible from awkward angles, i.e. portable test equipment</li>
                                                <br />
                                                <br />
                                                <b>Definitions:</b><br />
                                                <li><b>TFEL:</b> - "Thin Film ElectroLuminescence" - Different from OLED and AMOLED which are cheaper, but less reliable in extreme environments / over time</li>
                                                <li><b>TASEL:</b> - "Transparent Electroluminescent Display" - Same as above, but ther remove any transluscent backing, leaving only the glass substrate (casing)</li>

                                            </span></td>
                                    </a>
                                    <td align="center" class="auto-style1">
                                        <a href="http://lumineq.com/en/products" target="_blank" class="AnchorButton">Lumineq Products</a>
                                    </td>
                                </tr>
                                <%--End Display Block--%>
                                <%--Begin SSD Block--%>
                                <tr>
                                    <td>
                                        <a href="#" class="tooltipLeft">SSD
                                        <span><b>Mammoth Memory can cross with all manufacturers such as; Intel, Samsung, Crucial, Innodisk, IBM, PNY, Toshiba, Western Digital, Kingston, etc.</b><br />
                                            <br />
                                            <li><b>High quality flash memory</b> used from companies such as Micron, Toshiba and Intel.</li>
                                            <li>Sandforce controller available on some models.</li>
                                            <li>Available in <b>all standard interfaces</b> such as SATA, mSATA, PATA, PCI-e, etc.</li>
                                            <li>Storage capacities from 4GB up to 1TB.</li>
                                            <li><b>Extreme temperature and extreme conditions</b> drives available.</li>
                                            <li>Security features such as Smart Destruction and Physical Destruction available.</li>
                                            <li><b>Features</b> such as S.M.A.R.T., Trim, NCQ, and Wear Leveling are available.</li>
                                            <li><b>3 year warranty</b></li>
                                        </span>
                                        </a>
                                    </td>
                                    <td align="center" class="auto-style1">
                                        <a href="http://forms.sensiblemicro.com/view.php?id=14166" target="_blank" class="AnchorButton">SSD Form</a>
                                    </td>
                                </tr>
                                <%--End SSD Block--%>
                                <%--Begin Cabling / Interconnect Block--%>
                                <tr>
                                    <td>
                                        <a href="#" class="tooltipLeft">Cabling / Interconnects
                                        <span><b><em>"We rep a line called FiOX, who handles all sorts of Cabling and interconnect devices."</em></b><br />
                                            <li>HDMI</li>
                                            <li>DVI</li>
                                            <li>MONITOR CABLES</li>
                                            <li>USB</li>
                                            <li>AC/DC POWER CABLES</li>
                                            <li>ETHERNET CABLING</li>
                                            <li>SCSI (small computer system interface</li>
                                            <li>AUDIO/VIDEO</li>
                                        </span>
                                        </a>
                                    </td>
                                    <td align="center" class="auto-style1">
                                        <a href="#" onclick="return false" class="AnchorButton">Pending</a>
                                    </td>
                                </tr>
                                <%--End Cabling / Interconnect Block--%>
                                <%--Begin Optical Transcievers--%>
                                <tr>
                                    <td>
                                        <a href="#" class="tooltipLeft">Optical Transcievers
                                        <span><b>FioX</b> can cross with many manufacturers such as; <u>Finisar, Cisco, HP, 3Com, Huawei, Alcatel-Lucent, Juniper</u>, etc..<br />
                                            <br />
                                            <li>Matches the <b>quality</b> of competitors and in most instances the <b>warranty</b> period</li>
                                            <li>Warranty is <b>3 Years from date of shipment and life time technical support.</b></li>
                                            <li>Available in <b>all standard form factors</b> such as XFP, X2, SFP+, XENPAK, SFP, SFF, GBIC, 1X9, CWDM, DWDM and BIDI.</li>
                                            <li>Data Rates ranging from <b>155mb up to 40Gb/s</b></li>
                                            <li>Full range of Distances, Wavelengths, Connectors and Fiber Modes</li>
                                            <li>Certifications: ISO9001, FCCC, CE, Rohs</li>
                                            <li>Single 3.3V Power Supply</li>
                                        </span>
                                        </a>

                                    </td>
                                    <td align="center">
                                        <a href="http://forms.sensiblemicro.com/view.php?id=14773" target="_blank" class="AnchorButton">OT Form</a>
                                    </td>
                                </tr>
                                <%--End  Optical Transcievers--%>
                            </table>




                        </asp:Panel>
                     
                    </asp:Panel>
            </div>
            <div class="ColumnRight">
                <h3 style="display: inline;"><b>Objections: </b></h3>
                <u><a href="#" class="tooltipRight" onclick="return false">(Agree with Style)
                                    <span>
                                        <strong>Agree with Style!</strong><br />
                                        “I totally agree with you, most of my current customers said the same exact thing when I first introduced myself”...
                                    </span></a></u>
                <asp:Panel ID="pnlObjections" runat="server">
                    <input type="button" id="btnObjFranchise" class="Scriptbutton" value="Only Franchise / No Distys" onclick="setColor('btnObjFranchise', 'rgb(25, 139, 7)');" />
                    <asp:Panel ID="pnlObjFranchise" runat="server">
                        <ul>
                            <li><a href="#" class="tooltipRight" onclick="return false">Hybrid Distribution
                                <span>The key difference in dealing with Sensible Micro is we are actually a hybrid distributor, which means <b>we are an authorized source for many lines, but also support outside lines</b> when the customer has a supply chain issue like obsoletes, hard to finds, lead time or pricing situation.<br />
                                </span></a>
                            </li>
                            <li><a href="#" class="tooltipRight" onclick="return false">Advanced Inspection / Anticounterfeit Screening
                                <span>Since we have one of the most <b>advanced inspection labs</b> in the world, this <b>allows us to source</b> from outside excess markets <b>while certifying the integrity of parts sold</b> every time.

                                </span></a>
                            </li>
                            <li><a href="#" class="tooltipRight" onclick="return false">Rebuttal Questions<span><b>Good Questions to ask:</b><br />
                                <br />
                                What are some of the supply chain hiccups you experience from time to time?<br />
                                <ul>
                                    <li>Obsoletes?</li>
                                    <li>Lead times?</li>
                                    <li>Hard to finds?</li>
                                </ul>
                                <br />
                                <b><em>Joey's Corner:</b><br />
                                "Sometimes some of my buyers just get really bad service from a supplier and usually we can help out by sourcing the product for them so they don’t have to deal with all the headaches."</em>

                                                                                                          </span></a>

                            </li>
                        </ul>
                    </asp:Panel>
              
                    <input type="button" id="btnObjAVL" class="Scriptbutton" value="Not adding new vendors to our AVL" onclick="setColor('btnObjAVL', 'rgb(25, 139, 7)');" />
                    <asp:Panel ID="pnlObjAVL" runat="server">
                        <ul>
                            <li><a href="#" class="tooltipRight" onclick="return false">Do more with less<span><b>We bring more points of value than most vendors can offer.</b><br />
                                <br />
                                <b><em>Joey's Corner:</b><br />
                                “Yes, I absolutely see that trend with most of my customers currently. Doing more with less makes your supply chain stronger and more effective.  <b>Sensible takes that a step further by combining our sourcing expertise with our in-house component screening / verification capabilities.</b></em>

                                                                                                         </span></a>
                            </li>
                            <li><a href="#" class="tooltipRight" onclick="return false">One stop shop!<span>This is another reason why Sensible Micro is such a good fit for many of our customers. 
                                <br />
                                <br />
                                We can reduce your vendor base because we support so many different commodities and component services like <b>testing, tape & reeling and tinning, baking, warehousing, etc.</b>. 
                                <br />
                                <br />
                                We really are a one stop shop fro most component sourcing demands.</span></a>
                            </li>
                            <li><a href="#" class="tooltipRight" onclick="return false">Friendly, Effecient, Sensible Customer Service<span><b><em>Joey's Corner:<br /></b>"We both know at the end of the day its all about <b>service, price and delivery</b>.  If I am able to show you more value in all 3 of those areas and make your day easier and more effective, that’s what really counts."</em></span></a>
                            </li>
                        </ul>
                    </asp:Panel>
          
                    <input type="button" id="btnObjSlow" class="Scriptbutton" value="We&#39;re slow right now" onclick="setColor('btnObjSlow', 'rgb(25, 139, 7)');" />

                    <asp:Panel ID="pnlObjSlow" runat="server">
                        <ul>
                            <li><a href="#" class="tooltipRight" onclick="return false" style="color: black">I am sorry to hear that, is that typical for this time of year?</a></B>
                            </li>
                            <li><a href="#" class="tooltipRight" onclick="return false">Excess Inventory Opportunity?<span>You know a few of my other customers have been slow recently too, <b>do you have any excess inventory currently?</b> I know we were purchasing excess parts recently, maybe that can help relieve some added cost?</span></a>
                            </li>
                            <li><a href="#" class="tooltipRight" onclick="return false">Is this a pattern?<span>Is your buying usually cyclical? For example, do you usually do all your buying on a few days per month or quarter?</span></a>
                            </li>
                        </ul>
                    </asp:Panel>
                   <input type="button" id="btnObjQuality" class="Scriptbutton" value="Concerned about Quality / Counterfeit" onclick="setColor('btnObjQuality', 'rgb(25, 139, 7)');" />
                    <asp:Panel ID="pnlObjQuality" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <a href="#" class="tooltipRight" onclick="return false">Educate the customer<span>
                                        <b><em>Joey's Corner:</b><br />
                                        This is a topic that <b><u>is extremely important to our organization</u></b>.  When it comes to understanding counterfeit mitigation, would you say you are a novice or pretty much an expert on the topic?<br />
                                        <br />
                                        The reason I ask is because our company goes above and beyond when it comes to quality and I wasn’t sure if you had experience with some of the certifications out there and advanced inspection protocols?</em>


                                                                                                                </span></a></li>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><a href="#" class="tooltipRight" onclick="return false">Sensible Advanced Anticounterfeit<span>
                                    <b><em>Joey's Corner:</b><br />
                                    Sensible Micro houses one of the <b>most advanced anti-counterfeit labs in the world</b>, which means we have the ability to <b>re-certify the authenticity of components sold every time.</em></b>

                                                                                                                             </span></a></td>
                                <td><a href="https://sites.google.com/a/sensiblemicro.com/sensible-intranet-v2/departments/sales-1/sales-training-2?place=msg%2Fsm_training_sales%2FGlXW7dXdybs%2FcewazVzSsnEJ" target="_blank" class="AnchorButton">Selling the Lab</a></td>
                            </tr>
                            <tr>
                                <td><a href="#" class="tooltipRight" onclick="return false">GCAT Report Sample<span>Our lab report documents the integrity by verifying internal and external visual confirmations of the parts.<br />
                                    <ul>
                                        <li>Heated Solvents
                                        </li>
                                        <li>Decapsulation (De-cap)
                                        </li>
                                        <li>X-ray
                                        </li>
                                        <li>Advanced Microscopy
                                        </li>
                                    </ul>
                                </span></a></td>
                                <td>
                                    <a href="http://www.sensiblemicro.com/userfiles/files/prot/GCAT%20SAMPLE.pdf" target="_blank" class="AnchorButton">Sample GCAT Link</a>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#" class="tooltipRight" onclick="return false">Every order undergoes Re-Certification<span>
                                    We re-certify a component by using <b>X-Ray, Heated Solvents,Chemical Decapsulation and High Powered Microscopy</b>. In fact, we are the only distributor in the world who has a <b>Keyence VHX-2000 microscope</b>.  This scope conducts <b>3D imaging</b> of component body and leads while boasting the highest resolution for inspection.


                                                                                                                                  </span></a></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><a href="#" class="tooltipRight" onclick="return false">Insurance Protection<span>
                                    We can also add your company to our <b>insurance binder, which is $2M in product liability coverage and $3M in Errors / Omissions coverage</b>.  A guarantee is only as good as the protection it provides.

                                                                                                                </span></a></td>
                                <td></td>
                            </tr>
                        </table>

                    </asp:Panel>
                </asp:Panel>

            </div>

            </span>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

