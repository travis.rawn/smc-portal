﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="call_scripting_tool_v2.aspx.cs" Inherits="secure_sm_Sales_call_scripting_tool_v2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Call Scripting Tool v2 (Beta)</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:HiddenField ID="hfLeadTimes" runat="server" />
    <asp:HiddenField ID="hfOnlyFranchise" runat="server" />
    <asp:HiddenField ID="hfObsolescenceIssues" runat="server" />
    <asp:HiddenField ID="hfNotAddingVendorsAVL" runat="server" />
    <asp:HiddenField ID="hfCostReduction" runat="server" />
    <asp:HiddenField ID="hfSlowRightNow" runat="server" />
    <asp:HiddenField ID="hfStockLiquidation" runat="server" />
    <asp:HiddenField ID="hfQualityCounterfeit" runat="server" />

    <style>
        .popover {
            max-width: 600px !important;
            /* Max Width of the popover (depending on the container!) */
        }

        .joeyscorner {
            font-size: 20px;
            padding-right: 8px;
        }

        .inlineLink {
            font-size: small;
            font-weight: bold;
            color: green;
        }

        .buttonBar {
            font-size: 20px;
            margin: 5px;
        }
    </style>

    <div class="panel" id="pnlMain">
        <div class="panel panel-header">
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-3">
                    <h4>Details:</h4>
                </div>
                <div class="col-sm-3">
                    <h4>Top 5 Reqs(Count):</h4>
                </div>
                <div class="col-sm-3">
                    <h4>Top 5 MFG(Count):</h4>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-3">
                   <%-- <uc1:rz_company_autocomplete runat="server" ID="rz_company_autocomplete" />--%>
                    <uc1:rz_company_autocomplete runat="server" ID="rz_company_autocomplete" />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnSendEmail" runat="server" OnClick="btnSendEmail_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();"><span class="fa fa-envelope" style="font-size:15px; padding-right:15px;"></span>Generate Email</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="lblDetails" runat="server"></asp:Label>
                </div>

                <div class="col-sm-3">
                    <asp:Label ID="lblTop5Mfg" runat="server"></asp:Label>
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="lblTop5Parts" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row" style="background-color: whitesmoke">
                <div class="col-sm-4">
                    <h5><a href="#" class="popTip" title="Intro Script Example" data-toggle="popover" data-trigger="hover" data-placement="bottom">Intro Script Example</a></h5>
                    <div class="popTip-content hide">
                        One of the values of Sensible Micro is our the ability to <b>source the components you need</b> from a global, vetted list of trusted suppliers.  The same ones often used by so-called "franchise" outlets. 
                    </div>
                </div>
                <div class="col-sm-4">
                    <h5><a href="#" class="popTip" title="Current News" data-toggle="popover" data-trigger="hover" data-placement="bottom">Current News</a></h5>
                    <div class="popTip-content hide">
                        <ul>
                            <li>AVX and TDK, have made capacity-related announcements
                            </li>
                            <li>Stretched lead times are attributed to the Fairchild/ON Semi consolidation
                            </li>
                            <li>NXP selling their low-end product line
                            </li>
                            <li>International Rectifier/Infineon consolidation
                            </li>
                            <li>Diodes factory fire
                            </li>
                            <li>STMicroelectronics share gains at Apple consuming their available capacity
                            </li>
                            <li>Micron and Cypress have announced end-of-life (EOL) for any NOR product under 128 megabytes
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h5><a href="#" class="popTip" title="Intro Script Example" data-toggle="popover" data-trigger="hover" data-placement="bottom">General Call Tips</a></h5>
                    <div class="popTip-content hide">
                        Have real conversations that are impactful, stop making “sales calls”.  Adding value turns heads, sounding like an expert on a topic such as counterfeits will quickly elevate your status from annoying sales person to a trusted business partner.
                    </div>

                </div>
            </div>
        </div>
        <div class="panel panel-body">
            <asp:Panel ID="pnlBody" runat="server" Visible="false">
            <div class="row">
                <div class="col-sm-5">
                    <h3 style="display: inline;">Opportunities:</h3>
                </div>
                <div class="col-sm-2">
                </div>
                <div class="col-sm-5">
                    <div>
                        <h3 style="display: inline;">Objections:</h3>
                        <a href="#" style="display: inline;" class="popTip" title="Agree with Style" data-toggle="popover" data-trigger="hover" data-placement="bottom">(Agree with Style)</a>
                        <div class="popTip-content hide">
                            “I totally agree with you, most of my current customers said the same exact thing when I first introduced myself”...
                        </div>
                    </div>

                </div>


            </div>

            <div class="col-sm-5">
                <div class="row">
                    <button type="button" id="btnLeadTimes" data-toggle="collapse" data-target="#divLeadTimes" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">LeadTimes</button>
                    <div id="divLeadTimes" class="collapse">
                        <ul>
                            <li><a href="#" class="popTip" title="Immediate Sourcing" data-toggle="popover" data-trigger="hover" data-placement="bottom">Immediate Sourcing</a>
                                <div class="popTip-content hide">
                                    One of the values of Sensible Micro is we having the ability to source the components you need <b>via our large breadth of Stock and Excees partnerships</b>
                                </div>
                            </li>
                            <li>
                                <a href="#" class="popTip" title="Sensible Relationships" data-toggle="popover" data-trigger="hover" data-placement="bottom">Sensible Relationships</a>
                                <div class="popTip-content hide">
                                    Many times, we are able to leverage our relationships though <b>factory channels</b> which allows us to get material <b>faster than current lead times dictate.</b>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <button type="button" id="btnObsolescenceIssues" data-toggle="collapse" data-target="#divObsolescenceIssues" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">Obsolescence Issues</button>

                    <div id="divObsolescenceIssues" class="collapse">
                        <ul>
                            <li>
                                <a href="#" class="popTip" title="Identify Manufacturers" data-toggle="popover" data-trigger="hover" data-placement="bottom">Identify Manufacturers</a>
                                <div class="popTip-content hide">
                                    <b>Which manufacturers do they work usually with?</b><br />
                                    <br />

                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    <i>"Many of our customers have longer lifecycle products  ...  we see this a lot ...  I have a similar customers seeing this with "Atmel, Motorola, Freescale" (See top 5 MFG List above fro MFG's that this company uses)"</i>
                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Cross Referencing" data-toggle="popover" data-trigger="hover" data-placement="bottom">Cross Referencing</a>
                                <div class="popTip-content hide">
                                    Through our <b>franchised lines</b>, we have the ability to offer <b>drop-in replacements for obsolete parts.</b><br />
                                    <br />
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    "I actually just helped a customer of mine ... they had a Motorola processor that had gone EOL, and they had another 5 years of product lifecycle left.  So we had one of our franchise lines, Tekmos, who specialized in obsolete microcontrollers.  We were able to offer a factory-direct, drop-in replacement which turned out to be a much more cost and time-effective solution.  They were very happy."
                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <button type="button" id="btnCostReduction" data-toggle="collapse" data-target="#divCostReduction" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">Cost Reduction</button>

                    <div id="divCostReduction" class="collapse">
                        <ul>
                            <li><a href="#" class="popTip" title="Cost Reduction" data-toggle="popover" data-trigger="hover" data-placement="bottom">Cost Reduction</a>
                                <div class="popTip-content hide">
                                    <strong>Some good cost-saving MFG's</strong><br />
                                    <ul>
                                        <li>Microchip
                                        </li>
                                        <li>Xilinx
                                        </li>
                                        <li>Linear Tech
                                        </li>
                                        <li>Texas Instruments
                                        </li>
                                        <li>Atmel
                                        </li>
                                    </ul>
                                    <br />
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    <i>“I actually had one of my real good customers who was doing a LTB (last time buy) on an Atmel chip, I think it was a AT29 series... and we were able to save them about 15% on a LTB and offered them a schedule so they did not have to lay the full amount out as they would if they bought direct from Mfr or thru authorized distribution..."</i>

                                </div>
                            </li>
                            <li>
                                <a href="#" class="popTip" title="Authorized lines" data-toggle="popover" data-trigger="hover" data-placement="bottom">Authorized lines</a>
                                <div class="popTip-content hide">
                                    Many times on commodity parts (relays, crystals, connectors, capacitors, etc.) we can offer one of our auhthorized lines and show immediate, significant cost-savings against the components you are currently buying.  We can offer full trace and schduling on these as well.<br />
                                    <br />
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    "I was actually just able to help one of my ---insert Company's industry here--- customers with a Citizen crystal oscillator that they had been buying through distribution at $1.24, and we were able to offer our Cardinal brand at $.89.  We provide them around 200K per year, which nets them a huge cost savings, something like $60K / year.
                               
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <button type="button" id="btnStockLiquidation" data-toggle="collapse" data-target="#divStockLiquidation" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">Stock Liquidation</button>

                    <div id="divStockLiquidation" class="collapse">
                        <ul>
                            <li><a href="#" class="popTip" title="<title>" data-toggle="popover" data-trigger="hover" data-placement="bottom">(Excess Agreements?)</a>
                                <div class="popTip-content hide">
                                    (popover body)
                                </div>
                            </li>
                            <li>
                                <a href="#" class="popTip" title="<title>" data-toggle="popover" data-trigger="hover" data-placement="bottom">(Consignment Program?)</a>
                                <div class="popTip-content hide">
                                    (popover body)
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="col-sm-2">
            </div>
            <div class="col-sm-5">
                <div class="row">
                    <button type="button" id="btnOnlyFranchise" data-toggle="collapse" data-target="#divOnlyFranchise" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">Only Franchise / No Distys</button>

                    <div id="divOnlyFranchise" class="collapse">
                        <ul>



                            <li>
                                <a href="#" class="popTip" title="Funny you mention that, one of my best customers mentioned the same thing when I first started calling them ..." data-toggle="popover" data-trigger="hover" data-placement="bottom">Agree with them</a>
                                <div class="popTip-content hide">
                                    <ul>
                                        <li>... then they had a leadtime of 26+ weeks, and thru one of our OEM partnerships we have the part in consignment and were able to get it to them in a few days, and save them from a line down.
                                        </li>
                                        <li>... we eventually got our foot in the door doing some Anti-counterfeit testing for them on an Obsolete IC. They use us for all their testing because of our onsite Inspection Lab.
                                        </li>
                                    </ul>


                                </div>
                            </li>


                            <li><a href="#" class="popTip" title="Questions to Ask:" data-toggle="popover" data-trigger="hover" data-placement="bottom">Questions to Ask</a>
                                <div class="popTip-content hide">
                                    What are some of the supply chain hiccups you experience from time to time?<br />
                                    <ul>
                                        <li>What would you guys do if you run into a 26+ week leadtime and cannot wait?</li>
                                        <li>What would you guys do if you have an obsolete part come up on your BOM?</li>
                                        <li>Do you guys have a lab you use for anti- counterfeit screening?</li>
                                        <li>Obsoletes?</li>
                                        <li>Lead times?</li>
                                        <li>Hard to finds?</li>
                                    </ul>
                                    <br />
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    "Sometimes some of my buyers just get really bad service from a supplier and usually we can help out by sourcing the product for them so they don’t have to deal with all the headaches."
                                </div>
                            </li>


                            <li>
                                <a href="#" class="popTip" title="Hybrid Distribution" data-toggle="popover" data-trigger="hover" data-placement="bottom">Hybrid Distributions</a>
                                <div class="popTip-content hide">
                                    The key difference in dealing with Sensible Micro is we are actually a hybrid distributor, which means <b>we are an authorized source for many lines, but also support outside lines</b> when the customer has a supply chain issue like obsoletes, hard to finds, lead time or pricing situation.<br />

                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Advanced Inspection / Anticounterfeit Screening" data-toggle="popover" data-trigger="hover" data-placement="bottom">Advanced Inspection / Anticounterfeit Screening</a>
                                <div class="popTip-content hide">
                                    Since we have one of the most <b>advanced inspection labs</b> in the world, this <b>allows us to source</b> from outside excess markets <b>while certifying the integrity of parts sold</b> every time.
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <button type="button" id="btnNotAddingVendorsAVL" data-toggle="collapse" data-target="#divNotAddingVendorsAVL" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">Not adding Vendors to AVL</button>

                    <div id="divNotAddingVendorsAVL" class="collapse">
                        <ul>
                            <li>
                                <a href="#" class="popTip" title="Funny you mention that, one of my best customers when I first started calling them mentioned the same thing ..." data-toggle="popover" data-trigger="hover" data-placement="bottom">Agree with them</a>
                                <div class="popTip-content hide">
                                    <ul>
                                        <li>... then, we eventually got our foot in the door doing some Anti-counterfeit testing for them on an Obsolete IC. Now, they use us for all their testing because of our onsite Inspection Lab. From that it developed into sourcing all their Obsoletes and long leadtime parts.
                                        </li>
                                        <li>... but at the end of the day, someone always drops the ball, whether it be on delivery, pricing, shipping, something, and that’s when we would want you to think of Sensible Micro.
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="popTip" title="Questions to Ask:" data-toggle="popover" data-trigger="hover" data-placement="bottom">Questions to Ask</a>
                                <div class="popTip-content hide">
                                    <ul>
                                        <li>What would you guys do if you run into a 26+ week leadtime and cannot wait?
                                        </li>
                                        <li>What would you guys do if you have an obsolete part come up on your BOM and your normal sources can’t locate?
                                        </li>
                                    </ul>
                                </div>
                            </li>


                            <li>
                                <a href="#" class="popTip" title="Do more with less" data-toggle="popover" data-trigger="hover" data-placement="bottom">Do more with less</a>
                                <div class="popTip-content hide">
                                    We bring more points of value than most vendors can offer.
                                     <br />
                                    <br />
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    “Yes, I absolutely see that trend with most of my customers currently. Doing more with less makes your supply chain stronger and more effective.  <b>Sensible takes that a step further by combining our sourcing expertise with our in-house component screening / verification capabilities.</b>

                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="One stop shop!" data-toggle="popover" data-trigger="hover" data-placement="bottom">One stop shop!</a>
                                <div class="popTip-content hide">
                                    This is another reason why Sensible Micro is such a good fit for many of our customers. 
                                <br />
                                    <br />
                                    We can reduce your vendor base because we support so many different commodities and component services like <b>testing, tape & reeling and tinning, baking, warehousing, etc.</b>. 
                                <br />
                                    <br />
                                    We really are a one stop shop fro most component sourcing demands.
                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Friendly, Effecient, Sensible Customer Service" data-toggle="popover" data-trigger="hover" data-placement="bottom">Friendly, Effecient, Sensible Customer Service</a>
                                <div class="popTip-content hide">
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    "We both know at the end of the day its all about <b>service, price and delivery</b>.  If I am able to show you more value in all 3 of those areas and make your day easier and more effective, that’s what really counts."
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <button type="button" id="btnSlowRightNow" data-toggle="collapse" data-target="#divSlowRightNow" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">We're Slow right now</button>

                    <div id="divSlowRightNow" class="collapse">
                        <ul>

                            <li>
                                <a href="#" class="popTip" title="That’s crazy you mention that ..." data-toggle="popover" data-trigger="hover" data-placement="bottom">Agree with them</a>
                                <div class="popTip-content hide">
                                    ... one of my best customers and I were just chatting about how it seems slower now then this time last year…
                                </div>
                            </li>

                            <li>
                                <a href="#" class="popTip" title="Questions to Ask:" data-toggle="popover" data-trigger="hover" data-placement="bottom">Questions to Ask</a>
                                <div class="popTip-content hide">
                                    <ul>
                                        <li>Is that typical for you guys this time of year?
                                        </li>
                                        <li>A few of my other customers when they are slower will purge their obsolete or excess material…do you guys have a plan in place for that? (Excess / Consignment opportunity)
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="popTip" title="Is that typical for this time of year?" data-toggle="popover" data-trigger="hover" data-placement="bottom">I am sorry to hear that, is that typical for this time of year?</a>
                                <div class="popTip-content hide">
                                    See if this is seasonal, etc.
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    "I am sorry to hear that, is that typical for this time of year?"  
                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Excess Inventory Opportunity?" data-toggle="popover" data-trigger="hover" data-placement="bottom">Excess Inventory Opportunity?</a>
                                <div class="popTip-content hide">
                                    You know a few of my other customers have been slow recently too, <b>do you have any excess inventory currently?</b> I know we were purchasing excess parts recently, maybe that can help relieve some added cost?
                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Is this a buying pattern?" data-toggle="popover" data-trigger="hover" data-placement="bottom">Is this a buying pattern?</a>
                                <div class="popTip-content hide">
                                    Is your buying usually cyclical? For example, do you usually do all your buying on a few days per month or quarter?
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <button type="button" id="btnQualityCounterfeit" data-toggle="collapse" data-target="#divQualityCounterfeit" class="btn-smc btn-smc-primary btn-block buttonBar" onclick="SetHiddenDiv(this);">Concerned about Quality / Counterfeit</button>

                    <div id="divQualityCounterfeit" class="collapse">
                        <ul>
                            <li>
                                <a href="#" class="popTip" title="I couldn’t agree more ..." data-toggle="popover" data-trigger="hover" data-placement="bottom">Agree with them</a>
                                <div class="popTip-content hide">
                                    That’s one of the reasons I love working at SMC. Our owner takes quality extremely serious. We are ISO, AS9120 aerospace certified, and have an full on site Anti Counterfeit lab.
                                </div>
                            </li>
                            <li>
                                <a href="#" class="popTip" title="Questions to Ask:" data-toggle="popover" data-trigger="hover" data-placement="bottom">Questions to Ask</a>
                                <div class="popTip-content hide">
                                    <ul>
                                        <li>Have you guys ever received a counterfeit part?
                                        </li>
                                        <li>Do you have a counterfeit mitigation plan in place?
                                        </li>
                                        <li>A lot of my customers will do full site audits…do you guys come to your vendors facilities to do a full audit of their QMS (quality management system)?
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="#" class="popTip" title="Concerned about Quality / Counterfeit" data-toggle="popover" data-trigger="hover" data-placement="bottom">Concerned about Quality / Counterfeit</a>
                                <div class="popTip-content hide">
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    This is a topic that <b><u>is extremely important to our organization</u></b>.  When it comes to understanding counterfeit mitigation, would you say you are a novice or pretty much an expert on the topic?<br />
                                    <br />
                                    The reason I ask is because our company goes above and beyond when it comes to quality and I wasn’t sure if you had experience with some of the certifications out there and advanced inspection protocols?
                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Sensible Advanced Anticounterfeit" data-toggle="popover" data-trigger="hover" data-placement="bottom">Sensible Advanced Anticounterfeit</a>

                                <div class="popTip-content hide">
                                    <b><span class="fa fa-lightbulb-o joeyscorner"></span><em>Joey's Corner:</em></b><br />
                                    Sensible Micro houses one of the <b>most advanced anti-counterfeit labs in the world</b>, which means we have the ability to <b>re-certify the authenticity of components sold every time.</b>
                                </div>
                                <span><em>
                                    <a href="https://sites.google.com/a/sensiblemicro.com/sensible-intranet-v2/departments/sales-1/sales-training-2?place=msg%2Fsm_training_sales%2FGlXW7dXdybs%2FcewazVzSsnEJ" target="_blank" class="inlineLink">Link: Selling the Lab</a>
                                </em></span>
                            </li>
                            <li><%--<a href="#" class="popTip" title="GCAT Sample Report" data-toggle="popover" data-trigger="hover" data-placement="bottom">GCAT Sample Report</a>--%>
                                <span>
                                    <a href="http://www.sensiblemicro.com/userfiles/files/prot/GCAT%20SAMPLE.pdf" target="_blank" class="inlineLink">Link: GCAT Sample Report</a>
                                </span>
                                <div class="popTip-content hide">
                                    Our lab report documents the integrity by verifying internal and external visual confirmations of the parts.<br />
                                    <ul>
                                        <li>Heated Solvents
                                        </li>
                                        <li>Decapsulation (De-cap)
                                        </li>
                                        <li>X-ray
                                        </li>
                                        <li>Advanced Microscopy
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Every order undergoes Re-Certification" data-toggle="popover" data-trigger="hover" data-placement="bottom">Every order undergoes Re-Certification</a>
                                <div class="popTip-content hide">
                                    We re-certify a component by using <b>X-Ray, Heated Solvents,Chemical Decapsulation and High Powered Microscopy</b>. In fact, we are the only distributor in the world who has a <b>Keyence VHX-2000 microscope</b>.  This scope conducts <b>3D imaging</b> of component body and leads while boasting the highest resolution for inspection.


                                </div>
                            </li>
                            <li><a href="#" class="popTip" title="Insurance Protection" data-toggle="popover" data-trigger="hover" data-placement="bottom">Insurance Protection</a>
                                <div class="popTip-content hide">
                                    We can also add your company to our <b>insurance binder, which is $2M in product liability coverage and $3M in Errors / Omissions coverage</b>.  A guarantee is only as good as the protection it provides.
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
                </asp:Panel>
        </div>
    </div>

    <%--  <script>
        //Change Listner for hfCompanyID to trigger contact ddl bind.
        $(document).ready(
        function addListner() {
            var hf = $("#MainContent_rz_company_autocomplete_hfCompanyID");
            hf.on("change", function () { //bind() for older jquery version
                var companyID = $("#MainContent_rz_company_autocomplete_hfCompanyID").val();
                if (companyID.length > 0)
                    alert('hey' + companyID);
            }).triggerHandler('change'); //could be change() or trigger('change')
        });
    </script>--%>

    <script>
        function SetHiddenDiv(control) {
            var targetHfID = control.id.replace("btn", "hf");
            var targetHF = $("#MainContent_" + targetHfID);
            if (targetHF.val() === "true")
                targetHF.val("false");
            else
                targetHF.val("true");

        };

        $('.popTip').popover({
            container: 'body',
            //container: '#pnlMain',           
            html: true,
            content: function () {
                var text = $(this).parent().next(".popTip-content").html();
                if (text === undefined)
                    var text = $(this).next(".popTip-content").html();
                return text;
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

