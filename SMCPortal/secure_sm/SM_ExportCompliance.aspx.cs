﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;

public partial class secure_sm_PartIQ : Page
{

    SM_Tools tools = new SM_Tools();
    string strSearchPart = null;
    int MPNID;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load the initial UI Settings.        
        pnlEditPart.Visible = false;
        //pnlDeatils.Visible = false;


        //Reload the table on each Postback, else will break Datatable if I don't somehow keep in ViewState.
        smdt_Results.SelectedIndexChanged += smdt_Results_SelectedIndexChanged;
        smdt_Results.AddGridSelectButton = true;
        LoadComplianceTable();

        ////Load DetailsView id MPNID not empty.
        //if (!string.IsNullOrEmpty(hfMPNID.Value))
        //{
        //    MPNID = Convert.ToInt32(hfMPNID.Value);
        //    LoadDetailsPanel();
        //}


    }



    private void LoadComplianceTable()
    {
        using (gcatDataContext cdb = new gcatDataContext())
        {
            var query = cdb.ManufacturerParts.Where(w => w.MPN.Trim().Length > 0).ToList();
            var gcQuery = query.Select(s => new
            {
                s.MPN,
                MFG = s.Manufacturer,
                s.ECCN,
                HarmCode = s.HarmonizedCode,
                s.MPNID
            }).ToList();



            if (gcQuery.Any())
            {
                smdt_Results.dataSource = gcQuery;
                smdt_Results.theGridView.DataKeyNames = new string[] { "MPNID" };
                smdt_Results.loadGrid();
                return;
            }
        }
    }



   

    private void LoadDetailsPanel()
    {

        if (!string.IsNullOrEmpty(hfMPNID.Value))
        {
            //Existing Records will fill the textbox with current values.
            MPNID = Convert.ToInt32(hfMPNID.Value);
            using (gcatDataContext gdc = new gcatDataContext())
            {
                ManufacturerPart p = gdc.ManufacturerParts.Where(w => w.MPNID == MPNID).FirstOrDefault();
                if (p == null)
                    return;
                txtPartNumber.Text = p.MPN;
                txtMFG.Text = p.Manufacturer;
                txtECCN.Text = p.ECCN;
                txtHarmCode.Text = p.HarmonizedCode;
            }


        }


        //New Records will fall to here, and txt fields will be blank
        pnlEditPart.Visible = true;
        //pnlDeatils.Visible = false;

    }


    private void SubmitDetails()
    {
        using (gcatDataContext gdc = new gcatDataContext())
        {
            //Set the Properties from the text fields
            DateTime createDate = DateTime.Now;
            string MPN = txtPartNumber.Text.Trim().ToUpper();
            string MFG = txtMFG.Text.Trim().ToUpper();
            string HarmCode = txtHarmCode.Text.ToString().ToUpper();
            string ECCN = txtECCN.Text.ToString().ToUpper();

            ManufacturerPart mp = null;
            if (string.IsNullOrEmpty(hfMPNID.Value))
            {
                mp = new ManufacturerPart();
                gdc.ManufacturerParts.InsertOnSubmit(mp);
            }
            else
            {
                if (!Tools.Number.IsNumeric(hfMPNID.Value))
                    throw new Exception(hfMPNID.Value + " is not a valid MPNID.");
                //We have a numeric MPNID
                MPNID = Convert.ToInt32(hfMPNID.Value);
                mp = gdc.ManufacturerParts.Where(w => w.MPNID == MPNID).FirstOrDefault();  //Update Record
            }

            //Check for Null at this point
            if (mp == null)
                throw new Exception("Failed to retrieve MPN With MPNID " + MPNID);



            mp.MPN = MPN;
            mp.date_created = createDate;
            mp.Manufacturer = MFG;
            mp.ECCN = ECCN;
            mp.HarmonizedCode = HarmCode;
            //Submit the changes
            gdc.SubmitChanges();

            MPNID = mp.MPNID;
            //LoadDetailsPanel();
            LoadComplianceTable();
        }
    }





    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //pnlDeatils.Visible = false;
        LoadDetailsPanel();
    }

    protected void btnCancelAdd_Click(object sender, EventArgs e)
    {
        pnlEditPart.Visible = false;
        // pnlDeatils.Visible = false;
    }


    //protected void lbEditDetails_Click(object sender, EventArgs e)
    //{
    //    //pnlDeatils.Visible = false;
    //    LoadDetailsPanel();
    //}

    protected void submitDetails_Click(object sender, EventArgs e)
    {
        try
        {
            //Submit details
            SubmitDetails();

            //Push Success Alert
            tools.HandleResult("success", "Successfully updated Export Compliance record.");

        }
        catch (Exception ex)
        {
            //Alert on fail
            tools.HandleResult("fail", ex.Message);
        }


    }

    private void smdt_Results_SelectedIndexChanged(object sender, EventArgs e)
    {

        GridViewRow row = smdt_Results.theGridView.SelectedRow;
        string strMPNID = smdt_Results.theGridView.DataKeys[row.RowIndex].Values[0].ToString();
        if (String.IsNullOrEmpty(strMPNID))
            return;
        hfMPNID.Value = strMPNID;
        MPNID = Convert.ToInt32(strMPNID);
        LoadDetailsPanel();
    }


}