﻿using ASP;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class secure_sm_Reporting_quality_po_management : System.Web.UI.Page
{

    List<orddet_line> listInboundLines = new List<orddet_line>();
    List<ordhed_purchase> listOrdhedPurchase = new List<ordhed_purchase>();
    SM_Tools tools = new SM_Tools();

    protected void Page_Load(object sender, EventArgs e)
    {

        smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
        smdt_tracking.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
        if (!Page.IsPostBack)
            LoadInboundLines();
    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        bool confirmed = false;
        string tracking = "";
        string comment = "";

        if (e.Row.Cells.Count > 0 & e.Row.Cells.Count > 11)
        {
            //Hide unique_id col
            e.Row.Cells[12].CssClass = "hiddencol";

        }


        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //1. PONumber            
            //2. PODate
            //3. PartNumber
            //4. Vendor
            //5. Agent
            //6. Terms
            //7. TotalPOAmount
            //8. SO
            //9. ProjDock
            //10. Confirmed
            //11. Tracking
            //12. Comment
            //13. Unique_id

            TableCell partNumberCell = e.Row.Cells[2];
            TableCell confrimedCell = e.Row.Cells[9];
            TableCell trackingCell = e.Row.Cells[10];
            TableCell commentEditCell = e.Row.Cells[11];
            TableCell lineUIDCell = e.Row.Cells[12];
            string lineUid = lineUIDCell.Text;
            orddet_line theLine = listInboundLines.Where(w => w.unique_id == lineUid).FirstOrDefault();

            CheckBox cb1 = (CheckBox)confrimedCell.Controls[0];
            confirmed = Convert.ToBoolean(cb1.Checked);
            tracking = Server.HtmlDecode(trackingCell.Text).Trim();
            comment = Server.HtmlDecode(commentEditCell.Text).Trim();



            //PONumber Detail Modal
            //Edit Button with Modal
            LinkButton lbPoDetailModal = new LinkButton();
            lbPoDetailModal.ID = "lbPODetail";
            lbPoDetailModal.OnClientClick = "return false;";
            string PONUmber = partNumberCell.Text;
            lbPoDetailModal.Text = PONUmber;
            lbPoDetailModal.CssClass = "poDetailCell";
            partNumberCell.Controls.Add(lbPoDetailModal);
            //Set a HiddenField List of Pertinent PO Details
            HiddenField hdPoDetailsList = new HiddenField();
            hdPoDetailsList.ID = "hfPODetails";
            hdPoDetailsList.Value = GetPODetailsList(theLine);
            partNumberCell.Controls.Add(hdPoDetailsList);


            //PO Confirmed           
            string isCOnfirmed = "Yes";
            if (!confirmed)
                isCOnfirmed = "No";
            confrimedCell.Text = isCOnfirmed;



            //Tracking
            HiddenField hfRowTracking = new HiddenField();
            LinkButton lbTracking = new LinkButton();
            lbTracking.ID = "lblTracking";
            lbTracking.OnClientClick = "return false;";
            if (String.IsNullOrEmpty(tracking))
            {
                tracking = "None";
                lbTracking.Text = "None";
            }
            else
            {
                lbTracking.Text = "View";
                lbTracking.Attributes.Add("data-toggle", "tooltip");
                lbTracking.Attributes.Add("data-placement", "left");
                lbTracking.Attributes.Add("Title", tracking);
                //Save real value in HiddenField
                hfRowTracking.ID = "hfRowTracking";
                hfRowTracking.Value = tracking;
                trackingCell.Controls.Add(hfRowTracking);
            }
            trackingCell.Controls.Add(lbTracking);




            //Comments
            TableCell commentCell = e.Row.Cells[11];
            //Add hiddenField to store row's comment for comsumption by Modal text.
            HiddenField hfRowComment = new HiddenField();
            hfRowComment.Value = comment;
            hfRowComment.ID = "hfRowComment";
            commentCell.Controls.Add(hfRowComment);


            //Edit Button with Modal
            LinkButton lbEditModal = new LinkButton();
            if (!string.IsNullOrEmpty(comment))
                lbEditModal.Text = "<i class='far fa-comment-dots'> | Edit";
            else
                lbEditModal.Text = "Edit";
            lbEditModal.CssClass = "editCell";
            //ToolTip
            lbEditModal.Attributes.Add("data-toggle", "tooltip");
            lbEditModal.Attributes.Add("data-placement", "left");
            lbEditModal.Attributes.Add("Title", comment);
            commentCell.Controls.Add(lbEditModal);



            trackingCell.Text.Replace("&nbsp;", "");
            commentCell.Text.Replace("&nbsp;", "");



        }
    }

    private string GetPODetailsList(orddet_line theLine)
    {
        List<string> poDetailValues = new List<string>();
        //PO number
        poDetailValues.Add((theLine.ordernumber_purchase ?? "N/A").Trim());
        //PartNumber
        poDetailValues.Add((theLine.fullpartnumber ?? "N/A").Trim().ToUpper());
        //MFG
        poDetailValues.Add(theLine.manufacturer.Trim().ToUpper());
        //QTY
        poDetailValues.Add(theLine.quantity.Value.ToString());
        //QCSTatus
        poDetailValues.Add((theLine.qc_status ?? "N/A").Trim().ToUpper());
        //DateCode
        poDetailValues.Add(theLine.datecode_purchase.Trim().ToUpper());
        //UnitCost
        poDetailValues.Add(theLine.unit_cost.Value.ToString());
        //Condition
        poDetailValues.Add( (theLine.condition ?? "N/A").Trim());
        //Packaging
        poDetailValues.Add( (theLine.packaging ?? "N/A").Trim());
        //Rohs Vendor
        poDetailValues.Add((theLine.rohs_info_vendor ?? "N/A").Trim());
        //COO
        poDetailValues.Add( (theLine.country_of_origin_vendor ?? "N/A").Trim());
        //ShipVia_Purchase
        poDetailValues.Add(theLine.shipvia_purchase.Trim().ToUpper());
        

        //Line COmment
        poDetailValues.Add((theLine.internalcomment ?? "N/A").Trim());
        return String.Join(",", poDetailValues);

    }
    

    private void LoadInboundLines()
    {
        LoadData();
        LoadGrids();
    }
    protected void SaveLineData(out string result)
    {
        result = "";
        string lineID = Tools.Strings.SanitizeInput(hfLineID.Value);
        string comment = Tools.Strings.SanitizeInput(txtCommentModal.Text);
        string poTracking = Tools.Strings.SanitizeInput(txtTrackingNumberModal.Text);
        string poConfirmed = cbxPOConfirmedModal.isChecked.ToString();

        if (string.IsNullOrEmpty(lineID))
        {
            result = "No Line ID Detected.";
            return;
        }


        using (RzDataContext rdc = new RzDataContext())
        {
            //Grab Line Item
            orddet_line l = rdc.orddet_lines.Where(w => w.unique_id == lineID).FirstOrDefault();
            if (l != null)
            {

                //Save Tracking Number to line.
                if (!string.IsNullOrEmpty(poTracking))
                    l.tracking_purchase = poTracking.Trim().ToUpper();
                else
                    l.tracking_purchase = string.Empty;

                //Grab Purchase Order from Line Item
                if (!string.IsNullOrEmpty(l.orderid_purchase))
                {
                    ordhed_purchase p = rdc.ordhed_purchases.Where(w => w.unique_id == l.orderid_purchase).FirstOrDefault();
                    if (p != null)
                    {
                        p.internalcomment = comment;
                        p.is_confirmed = Convert.ToBoolean(poConfirmed);

                    }
                }
                rdc.SubmitChanges();
            }

        }

        result = "Success";
    }
   
    private void LoadData()
    {
        DateTime oneMonthFromToday = DateTime.Today.AddMonths(1);
        List<string> omitStatus = new List<string>() { "shipped", "void", "packing for service", "scrapped", "vendor rma packing", "vendor_rma_received", "received", "rma received", "received from service" };
        List<string> omitQcStatus = new List<string>() { "final_inspection" };
        lblOmittedStatus.Text = string.Join(", ", omitStatus);
        DateTime minCustomerDockDate = new DateTime(1900, 01, 01);
        using (RzDataContext rdc = new RzDataContext())
        {
            listOrdhedPurchase = rdc.ordhed_purchases.Where(w => (w.isvoid ?? false) != true && (w.isclosed ?? false) != true && w.agentname != "Fred Torrioni").ToList();

            List<string> uniquePOIds = listOrdhedPurchase.Select(s => s.unique_id).Distinct().ToList();

            listInboundLines = rdc.orddet_lines.Where(w => uniquePOIds.Contains(w.orderid_purchase) && w.customer_dock_date.Value <= oneMonthFromToday && !omitStatus.Contains(w.status.ToLower()) && (w.ordernumber_purchase ?? "").Length > 0 && (w.status ?? "").Length > 0 && !omitQcStatus.Contains(w.qc_status.ToLower())).ToList();
            //if (cbNoTracking.isChecked)
            //    listInboundLines = listInboundLines.Where(w => w.tracking_purchase.Length <= 0).ToList();
            //if (cbUnconfirmed.isChecked)            
            //    listOrdhedPurchase = listOrdhedPurchase.Where(w => (w.is_confirmed ?? false) == true).ToList();



        }
    }


    private void LoadGrids()
    {


        //1. PONumber
        //2. PODate
        //3. Vendor
        //4. Agent
        //5. Terms
        //6. TotalPOAmount
        //7. Status
        //8. SO
        //9. ProjDock
        //10. COnfirmed
        //11. Tracking
        //12. Comment
        //13. Unique_id



        var query = listInboundLines.Select(s => new
        {

            PO = s.ordernumber_purchase.Trim().ToUpper(),
            PODate = s.orderdate_purchase <= new DateTime(1900, 01, 01) ? "Not set" : s.orderdate_purchase.Value.ToShortDateString(),
            Part = s.fullpartnumber.Trim().ToUpper(),
            Vendor = s.vendor_name ?? "N/A",
            Agent = s.seller_name.Split(' ')[0],
            Terms = listOrdhedPurchase.Where(w => w.unique_id == s.orderid_purchase).Select(ss => ss.terms).FirstOrDefault() ?? "N/A",
            POLineCOst = s.total_cost.Value.ToString("C"),
            //Status = s.status,
            SO = s.ordernumber_sales.Trim().ToUpper(),
            ProjDock = s.projected_dock_date.Value <= new DateTime(1900, 01, 01) ? "Not set" : s.projected_dock_date.Value.ToShortDateString(),
            Conf = listOrdhedPurchase.Where(w => w.unique_id == s.orderid_purchase).Select(ss => ss.is_confirmed ?? false).FirstOrDefault(),
            Tracking = (s.tracking_purchase ?? "").Trim(),
            Comment = listOrdhedPurchase.Where(w => w.unique_id == s.orderid_purchase).Select(ss => ss.internalcomment ?? "").FirstOrDefault(),
            ID = s.unique_id
        });


        var noTrackQuery = query.Where(w => w.Tracking.Length <= 0);
        var hasTrackQuery = query.Where(w => w.Tracking.Length > 0);


        LoadDatatable(smdt, noTrackQuery.OrderByDescending(o => o.PODate));
        LoadDatatable(smdt_tracking, hasTrackQuery.OrderByDescending(o => o.PODate));









    }

    private void LoadDatatable(assets_controls_sm_datatable_ascx smdt, IEnumerable<object> query)
    {
        smdt.Clear();
        //Set the DataKeys
        smdt.theGridView.DataKeyNames = new string[] { "ID" };
        smdt.sortColumn = 2;
        smdt.sortDirection = "desc";
        smdt.dataSource = query;
        smdt.loadGrid();
    }



    //protected void lbActionSave_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string saveResult = "";
    //        SaveLineData(out saveResult);
    //        tools.RedirectToSuccess(Request.RawUrl, saveResult);

    //    }
    //    catch (Exception ex)
    //    {

    //        tools.HandleResult("fail", ex.Message);
    //    }
    //}

    protected void lbCommentModalSave_Click(object sender, EventArgs e)
    {
        string result = "";
        try
        {

            SaveLineData(out result);
            LoadInboundLines();
            tools.HandleResult("success", result);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }


    }
}