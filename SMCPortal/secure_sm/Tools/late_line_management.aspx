﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="late_line_management.aspx.cs" Inherits="secure_sm_Tools_late_line_management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .frame {
            margin-left: 15px;
            margin-right: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div id="pageTitle">
        <h1>Late Line Management</h1>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div class="frame">

        <div class="row">
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-2">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>
            <div class="col-sm-1">
                <asp:LinkButton ID="lbRefresh" runat="server" OnClick="lbRefresh_Click">Refresh</asp:LinkButton>
            </div>
             <div class="col-sm-1">
               <uc1:sm_checkbox runat="server" ID="cbShowAll"  theText="Show all" isChecked="false"  OnCheckedChanged="cb_CheckedChanged"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>
        <!-- Edit Modal -->
        <div id="mdl" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <b>
                            <asp:Label ID="lblModalTitle" runat="server"></asp:Label></b>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="txtCommentModal">Internal comment / Notes:</label>
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    <small id="noteHelp" class="form-text text-muted">Enter some notes to update the line's Internal Comments.</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="ddlLateReason">Late Reason:</label>
                                    <asp:DropDownList ID="ddlLateReason" runat="server">
                                        <asp:ListItem Selected="True">--Choose--</asp:ListItem>
                                        <asp:ListItem>Customer Late</asp:ListItem>
                                        <asp:ListItem>Vendor Late</asp:ListItem>
                                        <asp:ListItem>Sensible Late</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="dtpProjectedDock">Projected Dock:</label>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpProjectedDock" />
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="lbModalSave" CssClass="btn btn-default" runat="server" OnClick="lbModalSave_Click">Save</asp:LinkButton>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <asp:HiddenField runat="server" ID="hfLineID" />
        <asp:HiddenField runat="server" ID="hfInternalComment" />
        <asp:HiddenField runat="server" ID="hfLateReason" />

    </div>
    <script>

        $(".editCell").on('click', function (event) {
            //Rest Controls to initial state
            $("#FullWidthContent_ddlLateReason").val('--Choose--');
            $("#FullWidthContent_txtNotes").val('');
            $("#FullWidthContent_dtpProjectedDock_txtDate").val('');

            var lineDetails = $(this).closest("tr").find('td:eq(11)  input[type=hidden]').val();
            //Get PO Modal Details based on HF in Edit Column            
            var arrDetails = lineDetails.split(',');

            var lineID = arrDetails[0];
            var partNumber = arrDetails[1];
            var lineCode = arrDetails[2];
            var lineComment = arrDetails[3];
            var lateReason = arrDetails[4];
            var projDockDate = arrDetails[5];


            //Set the label values
            $("#FullWidthContent_lblModalTitle").text(partNumber + " Line: " + lineCode);


            //Set Values for row into HiddenFields for postback access
            $("#FullWidthContent_hfLineID").val(lineID);
            $("#FullWidthContent_hfInternalComment").val(lineComment);
            $("#FullWidthContent_hfLateReason").val(lateReason);
            $("#FullWidthContent_dtpProjectedDock").val(projDockDate);


            //Set existing values into their controls
            if (lineComment)
                $("#FullWidthContent_txtNotes").val(lineComment);
            if (projDockDate)
                $("#FullWidthContent_dtpProjectedDock_txtDate").val(projDockDate);
            if (lateReason && lateReason.length > 0)
                $("#FullWidthContent_ddlLateReason").val(lateReason);           

            //Show the Modal
            $('#mdl').modal('show');
            console.log("Showing Modal");
            return false;
        });
    </script>

</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

