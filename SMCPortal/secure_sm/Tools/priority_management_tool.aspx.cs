﻿using Newtonsoft.Json;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class secure_sm_Tools_priority_management_tool : System.Web.UI.Page
{


    List<string> lineIdList = new List<string>();
    SM_Tools tools = new SM_Tools();
    RzDataContext rdc = new RzDataContext();


    //List<ShippingLogic.priorityObject> priorityList = new List<ShippingLogic.priorityObject>();
    public List<ShippingLogic.priorityObject> priorityList
    {
        get
        {
            // If not on the viewstate then add it
            if (ViewState["priorityList"] == null)
                ViewState["priorityList"] = new List<ShippingLogic.priorityObject>();

            return (List<ShippingLogic.priorityObject>)ViewState["priorityList"];
        }
        set
        {
            ViewState["priorityList"] = value; ;
        }
    }
    

    protected void Page_Load(object sender, EventArgs e)
    {        
        smdt.RowCreated += new GridViewRowEventHandler(smdt_RowCreated);
        smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);

        //Load the Data, will have a priorty value AND bool HasManualPriority = tru if manual detected.
        //bool pullData = !Page.IsPostBack;
        LoadAll();

    }

    private void LoadAll(bool pullData = false)
    {
        if (!Page.IsPostBack)
            LoadDates();
        LoadPriorityList();
        LoadDataTable();
        LoadControls();
    }

    private void LoadControls()
    {
        int currentHighestPriority = priorityList.Max(m => m.linePriority);
        if (ddlQcStatus.SelectedValue != "")
            currentHighestPriority = priorityList.Where(w => w.qcStatus == ddlQcStatus.SelectedValue).Max(m => m.linePriority);
        lblCurrentTopPriority.Text = currentHighestPriority.ToString();
    }

    private void LoadDates()
    {
        if (Page.IsPostBack)
            return;
        dtpStart.SelectedDate = DateTime.Today.AddYears(-1);

        //new DateTime(2021, 03, 01);
        dtpEnd.SelectedDate = DateTime.Today.AddMonths(1);
    }



    private void smdt_RowCreated(object sender, GridViewRowEventArgs e)
    {


    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //15 = line id   
        if (e.Row.Cells.Count > 0 & e.Row.Cells.Count > 14)
        {
            //Set Max Width for columns
            e.Row.Cells[0].CssClass = "maxWidthGridCol";
            //Hide InspID Header
            e.Row.Cells[15].CssClass = "hiddencol";
            //Hide Weight column
            e.Row.Cells[2].CssClass = "hiddencol";
        }



        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCell priorityCell = e.Row.Cells[0];
            int priority = Convert.ToInt32(priorityCell.Text);
            TextBox tb = new TextBox();
            //tb.Enabled = false;
            tb.ID = "rowTextBox";
            tb.CssClass = "prioritycell";
            tb.Text = priority.ToString();
            //This span is a hack to get the Datatable to sort by the value of the input field.
            var span = new HtmlGenericControl("span");
            span.InnerHtml = priority.ToString();
            span.Attributes["style"] = "display:none";
            span.ID = "rowPrioritySpan";
            priorityCell.Text = "";
            priorityCell.Controls.Add(span);
            priorityCell.Controls.Add(tb);

            //Clear Priority
            TableCell actionCell = e.Row.Cells[1];
            HtmlButton hbClear = new HtmlButton();
            hbClear.ServerClick += lbClearPriority_Click;
            hbClear.Attributes.Add("class", "btn btn-sm btn-danger updateProgress");
            hbClear.InnerHtml = "<i class=\"far fa-times-circle\"></i>";
            actionCell.Controls.Add(hbClear);


            //Set background if manually set.
            string lineID = e.Row.Cells[15].Text;
            if (!string.IsNullOrEmpty(lineID))
            {
                ShippingLogic.priorityObject pp = priorityList.Where(w => w.line_uid == lineID).FirstOrDefault();
                if (pp != null)
                    if (pp.hasManualPriority)
                        e.Row.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            }



        }

    }

    protected void lbClearPriority_Click(object sender, EventArgs e)
    {
        int rindex = (((GridViewRow)(((HtmlButton)(sender)).Parent.BindingContainer))).RowIndex;
        string line_uid = smdt.theGridView.DataKeys[rindex].Values[0].ToString();
        lineIdList.Add(line_uid);
        ClearPriority();

        tools.HandleResult("success", "Success, manual priority removed.");

    }

    private void ClearPriority()
    {

        
            List<line_process> lpList = rdc.line_processes.Where(w => lineIdList.Contains(w.orddet_line_uid)).ToList();
            foreach (line_process lp in lpList)
                rdc.line_processes.DeleteOnSubmit(lp);


            rdc.SubmitChanges();
        

        LoadAll();
    }

    protected void lbSetTop_Click(object sender, EventArgs e)
    {


    }

   

    private int GetHighestPriority()
    {
        //LoadPriorityList();
        return priorityList.Max(m => m.linePriority);
    }





    private void LoadDataTable()
    {

        smdt.theGridView.DataSource = null;
        smdt.theGridView.DataBind();

        smdt.jqColDefs = new List<Tuple<string, string>>();



        Tuple<string, string> responsivePriority1stCol = new Tuple<string, string>("responsivePriority", " 1, targets: 2");
        //Tuple<string, string> responsivePriorityLastCol = new Tuple<string, string>("responsivePriority", " 2, targets: 7");   //Sales Order          
        Tuple<string, string> responsivePriorityLastCol = new Tuple<string, string>("responsivePriority", " 2, targets: 8");   //Part Number  
        Tuple<string, string> fixedWidthColumn = new Tuple<string, string>("width", " 30px, targets: 0");   //Part Number  
                                                                                                            //smdt.jqColDefs.Add(responsivePriority1stCol);
                                                                                                            //smdt.jqColDefs.Add(fixedWidthColumn);
        smdt.jqColDefs.Add(responsivePriorityLastCol);
        smdt.pageLength = 100;

        //Set the DataKeys
        smdt.theGridView.DataKeyNames = new string[] { "lineuid" };

        //Bind the Grid
        var query = priorityList.Select(s => new
        {

            Priority = s.linePriority, //> 0 ? (s.linePriority) : (s.datePriority + s.gpPriority),
            Action = "",
            Weight = "GP:" + s.gpPriority + " DP:" + s.datePriority,
            Customer = s.customerName,
            Vendor = s.vendorName,
            PO = s.ordernumberPurchase,
            QcStatus = s.qcStatus,
            Sale = (s.ordernumberSales ?? "No Sale") + " (" + s.lineCount + ")",
            PartNumber = s.partNumber.Trim().ToUpper(),
            Price = s.totalPrice.ToString("C"),
            GP = s.grossProfit.ToString("C"),
            SaleDate = s.orderDateSales.ToShortDateString(),
            CDock = s.customerDock.ToShortDateString(),
            ProjDock = s.projectedDock.ToShortDateString(),
            Agent = s.salesAgent,
            lineuid = s.line_uid



        });



        //Handle Grid Options        
        smdt.sortColumn = 0;//priority rating
        smdt.sortDirection = "desc";
        smdt.dataSource = query;
        smdt.loadGrid();

    }


    private void LoadPriorityList()
    {
        priorityList = ShippingLogic.GetInboundPriorityLines(rdc, dtpStart.SelectedDate.Value, dtpEnd.SelectedDate.Value);

        var lineInspIds = priorityList.Where(w => (w.idea_insp_id ?? "").Length > 0).Select(s => s.idea_insp_id).ToList();//NEed the list, as next query is different datacontext



        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            var completedInspections = pdc.insp_head.Where(w => lineInspIds.Contains(w.insp_id.ToString()) && w.is_insp_complete == true).Select(s => s.insp_id.ToString()).ToList();


            priorityList = priorityList.Where(w => !completedInspections.Contains(w.idea_insp_id.ToString())).ToList();
        }




    }

    protected void lbSavePriority_Click(object sender, EventArgs e)
    {

        try
        {
            string result = "";
            SavePriority(out result);
            tools.RedirectToSuccess(Request.RawUrl, result);  
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }


    }
    protected void SavePriorityTest(out string result)
    {
        result = "";
        List<string> listHfValues = tools.GetListFromHiddenField(hfChangedLineIds);
        if (listHfValues.Count <= 0)
            result = "No Ids changed.";
        else
        {
            int count = listHfValues.Count;
            StringBuilder sb = new StringBuilder();
            sb.Append(count + " ids will be changed: <br />");
            foreach (string s in listHfValues)
                sb.Append(s + " <br />");
            result = sb.ToString();
        }

    }

    

    protected void SavePriority(out string result)
    {
        List<line_process> listChangedLineProcesses = new List<line_process>();


        List<string> listDataKeys = new List<string>();
        DataKeyArray lineIdDataKeys = smdt.theGridView.DataKeys;
        foreach (DataKey dk in lineIdDataKeys)
            listDataKeys.Add(dk.ToString());
        //Values of changed lines from row HiddenField
        List<string> listHfValues = tools.GetListFromHiddenField(hfChangedLineIds);
        if (listHfValues.Count <= 0)
        {
            result = "Nothing to do. No Priorities updated.";
            return;
        }



       
            //haven't found a way to query Gridview by datakey to retrieve corresponding row, so for now looping through.
            foreach (GridViewRow row in smdt.theGridView.Rows)
            {
                int intPriority = 0;
                string lineUid = smdt.theGridView.DataKeys[row.RowIndex].Values[0].ToString();
                if (!listHfValues.Contains(lineUid))
                    continue;
                //6c0345cb698b4f81932001578b58f147
                //Get the Textbox
                TextBox tb = (TextBox)row.FindControl("rowTextBox");
                if (tb != null)
                    intPriority = Convert.ToInt32(tb.Text);

                line_process lp = rdc.line_processes.Where(w => w.orddet_line_uid == lineUid).SingleOrDefault();

                if (lp == null)
                {
                    lp = new line_process();
                    lp.unique_id = Guid.NewGuid().ToString();
                    lp.date_created = DateTime.Now;
                    lp.date_modified = DateTime.Now;
                    rdc.line_processes.InsertOnSubmit(lp);
                }
                lp.type = SM_Enums.LineProcessType.LinePriority.ToString();
                lp.date_modified = DateTime.Now;
                lp.orddet_line_uid = lineUid;
                lp.line_priority = intPriority;
                listChangedLineProcesses.Add(lp);


                HtmlGenericControl spanPriority = (HtmlGenericControl)row.FindControl("rowPrioritySpan");
                spanPriority.InnerHtml = intPriority.ToString();



            }


            rdc.SubmitChanges();

            result = BuildResultMessage(listChangedLineProcesses);
            //Flushg the HiddenField, else, we'll keep saving the same things
            hfChangedLineIds.Value = "";
            //result = "All line Priorities saved.";


        
    }

    private string BuildResultMessage(List<line_process> listChangedLines)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("The following Lines have been manually Piroritized: <br />");
        foreach (line_process lp in listChangedLines)
        {
            ShippingLogic.priorityObject po = priorityList.Where(w => w.line_uid == lp.orddet_line_uid).FirstOrDefault();
            sb.Append(po.partNumber + " | " + po.customerName + " | " + po.vendorName + " | QTY: " + po.qty + " <br />");

        }

        return sb.ToString();
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {

    }

    protected void lbBulkActions_Click(object sender, EventArgs e)
    {

    }


}