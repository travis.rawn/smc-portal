﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="priority_management_tool.aspx.cs" Inherits="secure_sm_Tools_priority_management_tool" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <style>
        .main-body-content {
            padding-left: 30px;
            padding-right: 30px;
        }

        .save-button {
            color: #EE2738;
        }

            .save-button i {
                padding-left: 5px;
            }

        .action-bar {
            position: fixed;
            padding: 10px;
            /*color: #EE2738;*/
            width: 300px;
            margin-left: -150px; /*half the size of width*/
            bottom: 0px;
            left: 50%;
            /*background-color: #d9534f;*/
            /*background-color: #181D1E;*/
            background-color: lightgrey;
            border: 1px solid;
            border-color: darkgray;
            z-index: 800;
            text-align: center;
            border-radius: 6px;
        }

            .action-bar p {
                padding-top: 5px;
            }

        .action-label {
            color: #EE2738;
        }

        /*.maxWidthGridCol {
            max-width: 60px;
            overflow: hidden;
        }*/

        .manual-priority {
            background-color: lightgoldenrodyellow;
        }

        .priority-text {
            font-style: italic;
        }

        .title-text {
            text-align: center;
        }

        #divTitle {
            display: none;
        }
    </style>


    <div class="row">
        <div class="col-sm-12">
            <div class="well well-sm priority-text">
                <div class="title-text">
                    <h5>How ship priority is determined:</h5>
                    <p>
                        The priority is determined by the priority value.  The <strong>higher</strong> the priority number, the <strong>greater</strong> the priority.
                    </p>
                </div>
                <ol>

                    <%--  <li><strong>GP Priority: 5 points for every $10,000 in GP</strong>.   (i.e. $2,246 in GP is 2 x 5 = <strong>10 points</strong>)</li>
                    <li><strong>Date Priority:1 point per days</strong> since customer dock date.   (i.e. Dock was yesterday, that's 1 point, dock is 10 days from now, that's -10 points))</li>--%>
                    <li>
                        <strong><span class="manual-priority">Manual Priority: </span></strong>
                        Management can set any arbitrary number manually, and click the <strong>"Save Priority"</strong> button.  
                        <span class="manual-priority">Manual Priority lines will have a Yellow background.</span>
                        All lines start with a default Priority of 0.
                    </li>
                </ol>

            </div>
        </div>
    </div>


    <div class="well well-sm">
        <div class="row">
            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
            </div>
            <div class="col-sm-3">
                <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
            </div>

            <div class="col-sm-2">
                <asp:DropDownList ID="ddlQcStatus" runat="server" onchange="FilterGridByQcStatus();">
                    <asp:ListItem Text="All" Value=""></asp:ListItem>
                    <asp:ListItem Text="Final_Inspection" Value="Final_Inspection"></asp:ListItem>
                    <asp:ListItem Text="In_House" Value="In_House" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inbound" Value="Inbound"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-2">

                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-sm btn-default btn-block updateProgress" OnClick="lbSearch_Click">
                Search
                </asp:LinkButton>
            </div>
            <div class="col-sm-2">
                <asp:LinkButton runat="server" ID="lbSavePriority" class="btn btn-default btn-block save-button updateProgress" OnClick="lbSavePriority_Click">
                  Save Priority <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </asp:LinkButton>

            </div>



        </div>
    </div>



    <%-- Floating Action Panel--%>
    <div id="divAction" class="action-bar form" runat="server">

        <p class="action-label">
            Current Top Priority: 
            <strong>
                <asp:Label ID="lblCurrentTopPriority" runat="server"></asp:Label>
            </strong>
        </p>


        <asp:LinkButton runat="server" ID="lbActionSave" class="btn btn-default btn-block updateProgress save-button" OnClick="lbSavePriority_Click">
                  Save Priority <i class="fa fa-floppy-o" aria-hidden="true"></i>
        </asp:LinkButton>
    </div>

    <asp:HiddenField ID="hfChangedLineIds" runat="server" />
    <uc1:sm_datatable runat="server" ID="smdt" />



    <script>

        //Globals
        var dataTable = $("#FullWidthContent_smdt_gvMain");
        var ddlQcStatus = $("#FullWidthContent_ddlQcStatus");
        //Functions


        $(window).load(function () {
            //This ensures this runs on page load for auto-filtering
            FilterGridByQcStatus();
        });

        function FilterGridByQcStatus() {
            var string = ddlQcStatus.val();
            var column = 6;
            SearchDataTableForString(dataTable, string, column);
        };

        function SearchDataTableForString(dataTable, string, column) {

            if ($.fn.DataTable.isDataTable(dataTable)) {
                //dataTable.column(column).filter(string).draw();
                dataTable.DataTable().column(column).search(string, true, false).draw();
            };
        }

        //Detect input on row input
        $('.prioritycell').on('input', function () {
            //Hiddenfield may already have array objects
            let changedIds = [];

            //retrieve array
            var existingIds = $("#FullWidthContent_hfChangedLineIds").val();
            if (existingIds) {
                changedIds = JSON.parse(existingIds);
                //changedIds.push( );
            }


            //Get Row Changes
            var id = $(this).closest("tr").find('td:eq(15)').text();
            if (id) {
                if (!changedIds.includes(id))
                    changedIds.push(id);
            }
            //Set the hidden field to current contents of ID Array
            $("#FullWidthContent_hfChangedLineIds").val(JSON.stringify(changedIds));

            //alert($("#MainContent_hfChangedLineIds").val());
        });
    </script>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

