﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="po_management.aspx.cs" Inherits="secure_sm_Reporting_quality_po_management" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/sm_checkbox.ascx" TagPrefix="uc1" TagName="sm_checkbox" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .frame {
            padding-left: 35px;
            padding-right: 35px;
        }

        .action-bar {
            position: fixed;
            padding: 10px;
            /*color: #EE2738;*/
            width: 300px;
            margin-left: -150px; /*half the size of width*/
            bottom: 0px;
            left: 50%;
            /*background-color: #d9534f;*/
            /*background-color: #181D1E;*/
            background-color: lightgrey;
            border: 1px solid;
            border-color: darkgray;
            z-index: 800;
            text-align: center;
            border-radius: 6px;
        }

            .action-bar p {
                padding-top: 5px;
            }

        .action-label {
            color: #EE2738;
        }



        #FullWidthContent_txtCommentModal {
            min-height: 150px;
        }

        .tooltip-inner {
            text-align: left;
        }

        .po-detail{
            font-weight:bolder;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div id="pageTitle">
        <h1>PO Management</h1>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div class="frame">
        <div class="well well-sm">
            <p>Lines with the following status are omitted from this report:</p>
            <em>
                <asp:Label ID="lblOmittedStatus" runat="server"></asp:Label></em>
        </div>
        <div class="well well-sm">
            <div class="row">
                <%-- <div class="col-sm-4">
                    <uc1:sm_checkbox runat="server" ID="cbNoTracking" />
                </div>
                <div class="col-sm-4">
                    <uc1:sm_checkbox runat="server" ID="cbUnconfirmed" />
                </div>--%>
                <div class="col-sm-4">
                    <asp:LinkButton ID="lbRefresh" runat="server" CssClass="updateProgress">Refresh</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4>Lines with no Tracking</h4>
                <uc1:sm_datatable runat="server" ID="smdt" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4>Lines with Tracking</h4>
                <uc1:sm_datatable runat="server" ID="smdt_tracking" />
            </div>
        </div>

        <%-- Floating Action Panel--%>
        <%-- <div id="divAction" class="action-bar form" runat="server">

            <asp:LinkButton runat="server" ID="lbActionSave" class="btn btn-default btn-block updateProgress save-button" OnClick="lbActionSave_Click">
                  Save <i class="fa fa-floppy-o" aria-hidden="true"></i>
            </asp:LinkButton>
        </div>--%>
    </div>



    <!-- Edit Modal -->
    <div id="mdlComments" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <%--<h4 class="modal-title">View / Edit Comment</h4>--%>
                    <b>
                        <asp:Label ID="lblModalTitle" runat="server"></asp:Label></b>
                </div>
                <div class="modal-body">


                    <div class="form-group">
                        <label for="txtCommentModal">PO Comments</label>
                        <asp:TextBox ID="txtCommentModal" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="txtTrackingNumberModal">PO Tracking Number</label>
                        <asp:TextBox ID="txtTrackingNumberModal" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-check">
                        <uc1:sm_checkbox runat="server" ID="cbxPOConfirmedModal" theText="PO Confirmed?" autoPostback="false" />
                        <%-- <label class="form-check-label" for="cbxPOConfirmedModal">PO Confirmed?</label>--%>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="lbCommentModalSave" CssClass="btn btn-default" runat="server" OnClick="lbCommentModalSave_Click">Save</asp:LinkButton>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- PO Details Modal -->
    <div id="mdlPODetails" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <%--<h4 class="modal-title">View / Edit Comment</h4>--%>
                    <b>
                        <asp:Label ID="lblPoDetailsTitle" runat="server"></asp:Label></b>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lblPODetail_PN" class="po-detail">PartNumber: </label>
                                <asp:Label ID="lblPODetail_PN" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_MFG" class="po-detail">MFG: </label>
                                <asp:Label ID="lblPODetail_MFG" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_QTY" class="po-detail">QTY: </label>
                                <asp:Label ID="lblPODetail_QTY" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_QCS" class="po-detail">QC Status: </label>
                                <asp:Label ID="lblPODetail_QCS" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_DC" class="po-detail">DateCode: </label>
                                <asp:Label ID="lblPODetail_DC" runat="server" />
                            </div>

                            <div class="form-group">
                                <label for="lblPODetail_UC" class="po-detail">Unit Cost: </label>
                                <asp:Label ID="lblPODetail_UC" runat="server" />
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lblPODetail_Condition" class="po-detail">Condition: </label>
                                <asp:Label ID="lblPODetail_Condition" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_Packaging" class="po-detail">Packaging: </label>
                                <asp:Label ID="lblPODetail_Packaging" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_RoHS" class="po-detail">RoHS Info: </label>
                                <asp:Label ID="lblPODetail_RoHS" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_COO" class="po-detail">Country of Origin: </label>
                                <asp:Label ID="lblPODetail_COO" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="lblPODetail_ShipVia" class="po-detail">ShipVia Purchase: </label>
                                <asp:Label ID="lblPODetail_ShipVia" runat="server" />
                            </div>

                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="lblPODetail_LineComment" class="po-detail">Comment: </label>
                                <asp:Label ID="lblPODetail_LineComment" runat="server" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <%--<asp:HiddenField runat="server" ID="hfChangedLineIds" />--%>
    <asp:HiddenField runat="server" ID="hfLineID" />
    <asp:HiddenField runat="server" ID="hfComment" />
    <asp:HiddenField runat="server" ID="hfTracking" />
    <asp:HiddenField runat="server" ID="hfPOConfirmed" />

    <script>
        ////Detect input on row input
        //$('.inputCell').on('input', function () {
        //    //Hiddenfield may already have array objects
        //    //alert('farts!');
        //    let changedIds = [];

        //    //retrieve array
        //    var existingIds = $("#FullWidthContent_hfChangedLineIds").val();
        //    if (existingIds) {
        //        changedIds = JSON.parse(existingIds);
        //        //changedIds.push( );
        //    }


        //    //Get Row Changes
        //    var id = $(this).closest("tr").find('td:eq(12)').text();
        //    if (id) {
        //        if (!changedIds.includes(id))
        //            changedIds.push(id);
        //    }
        //    //Set the hidden field to current contents of ID Array
        //    $("#FullWidthContent_hfChangedLineIds").val(JSON.stringify(changedIds));

        //    //alert($("#FullWidthContent_hfChangedLineIds").val());
        //    console.log($("#FullWidthContent_hfChangedLineIds").val());
        //});

        $(".poDetailCell").on('click', function (event) {
            //Get PO Modal Details based on HF in column 2
            var poDetailsString = $(this).closest("tr").find('td:eq(2) input[type=hidden]').val();
            var arrPoDetails = poDetailsString.split(',');
            //Show the Modal

            var PONumber = arrPoDetails[0];
            var PartNumber = arrPoDetails[1];
            var MFG = arrPoDetails[2];
            var Qty = arrPoDetails[3];
            var QcStatus = arrPoDetails[4];
            var DateCode = arrPoDetails[5];
            var UnitCost = arrPoDetails[6];
            var Condition = arrPoDetails[7];
            var Packaging = arrPoDetails[8];
            var RoHS = arrPoDetails[9];
            var COO = arrPoDetails[10];
            var ShipVia = arrPoDetails[11];
            var Comment = arrPoDetails[12];

            //Set the label values
            $("#FullWidthContent_lblPoDetailsTitle").text("Details for PO: " + PONumber);
            $("#FullWidthContent_lblPODetail_PN").text(PartNumber);
            $("#FullWidthContent_lblPODetail_MFG").text(MFG);
            $("#FullWidthContent_lblPODetail_QTY").text(Qty);
            $("#FullWidthContent_lblPODetail_QCS").text(QcStatus);
            $("#FullWidthContent_lblPODetail_DC").text(DateCode);
            $("#FullWidthContent_lblPODetail_UC").text(UnitCost);
            $("#FullWidthContent_lblPODetail_Condition").text(Condition);
            $("#FullWidthContent_lblPODetail_Packaging").text(Packaging);
            $("#FullWidthContent_lblPODetail_RoHS").text(RoHS);
            $("#FullWidthContent_lblPODetail_COO").text(COO);
            $("#FullWidthContent_lblPODetail_ShipVia").text(ShipVia);
            $("#FullWidthContent_lblPODetail_LineComment").text(Comment);

            $('#mdlPODetails').modal('show');
            console.log("Showing PO Modal");
            return false;
        });


        //For the row Edit Button, copy corresponding comment text to the textbox for view edit.
        $(".editCell").on('click', function (event) {
            //Get comment text
            var lineID = $(this).closest("tr").find('td:eq(12)').text();
            var comment = $(this).closest("tr").find('td:eq(11)  input[type=hidden]').val();
            var tracking = $(this).closest("tr").find('td:eq(10) input[type=hidden]').val();
            var confirmed = $(this).closest("tr").find('td:eq(9)').text();
            var poNumber = $(this).closest("tr").find('td:eq(6)').text();
            var vendorName = $(this).closest("tr").find('td:eq(1)').text();
            //Set Values for row into HiddenFields for postback access
            $("#FullWidthContent_hfLineID").val(lineID);
            $("#FullWidthContent_hfComment").val(comment);
            $("#FullWidthContent_hfTracking").val(tracking);
            $("#FullWidthContent_hfPOConfirmed").val(confirmed);

            //Copy values to Edit modal controls
            $("#FullWidthContent_lblModalTitle").text("PO: " + poNumber + "(" + vendorName + ")");
            $("#FullWidthContent_txtCommentModal").val(comment);
            $("#FullWidthContent_txtTrackingNumberModal").val(tracking);
            var checked = confirmed === "Yes";
            $("#FullWidthContent_cbxPOConfirmedModal_cbx").prop("checked", checked)
            //Show the Modal
            $('#mdlComments').modal('show');
            console.log(comment);
            return false;
        });


    </script>

</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

