﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_sm_Tools_late_line_management : System.Web.UI.Page
{
    //List<orddet_line> listLateLines = new List<orddet_line>();
    //List<line_process> listLineProcesses = new List<line_process>();
    List<LateLineObject> listLateLineObjects = new List<LateLineObject>();
    List<ordhed_purchase> listSalesOrders = new List<ordhed_purchase>();
    SM_Tools tools = new SM_Tools();
    DateTime startDate;
    DateTime endDate;

    [Serializable]
    class LateLineObject
    {

        public DateTime Date { get; set; }
        public DateTime ProjDock { get; set; }
        public DateTime ShipDate { get; set; }
        public string Part { get; set; }
        public string Status { get; set; }
        public int LineCode { get; set; }
        public string SO { get; set; }
        public string Customer { get; set; }
        public string Vendor { get; set; }
        public string Agent { get; set; }
        public string ID { get; set; }
        public string Edit { get; set; }
        public string LateReason { get; set; }
        public string InternalComment { get; set; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
        if (!Page.IsPostBack)
            LoadAll();
    }





    private void LoadAll(bool pullData = false)
    {
        LoadDates();
        LoadData();
        LoadGrid();

    }

    private void LoadGrid()
    {
        var gridQuery = listLateLineObjects.Select(lo => new
        {

            Date = lo.Date.ToShortDateString(),
            ProjDock = lo.ProjDock.ToShortDateString(),
            ShipDate = lo.ShipDate.ToShortDateString(),
            lo.Part,
            lo.Status,
            lo.LineCode,
            lo.SO,
            lo.Customer,
            lo.Vendor,
            lo.Agent,
            Late = lo.LateReason,
            lo.Edit,
            lo.ID

        }).OrderBy(o => o.ProjDock);

        smdt.sortColumn = 1;
        smdt.pageLength = 25;
        smdt.sortDirection = "desc";
        smdt.dataSource = gridQuery;
        smdt.loadGrid();
    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            //Per Victoria, start date should default to first of the year.  I pushed back and suggested a rolling 8 months.  
            //dtpStart.SelectedDate = DateTime.Today.AddMonths(-6);
            dtpStart.SelectedDate = DateTime.Today.AddMonths(-8).Date;
            dtpEnd.SelectedDate = DateTime.Today.Date;
        }
        startDate = dtpStart.SelectedDate.Value.Date;
        endDate = dtpEnd.SelectedDate.Value.Date;
    }

    private void LoadData()
    {
        List<orddet_line> listLateLines = new List<orddet_line>();
        List<line_process> listLineProcesses = new List<line_process>();

        using (RzDataContext rdc = new RzDataContext())
        {


            //Get lists of sales IDs  that belong to "CPO-" and "CR" orders, they should not show late
            List<string> customreReturnIDs = new List<string>();
            List<string> cpoSaleIds = new List<string>();
            List<string> NonLateStatus = new List<string>() { "void", "scrapped", "quarantined" };
            customreReturnIDs = rdc.ordhed_sales.Where(w => w.date_created.Value >= startDate && w.date_created.Value.Date <= endDate && w.ordernumber.ToLower().Contains("cr")).Select(s => s.unique_id).ToList();
            cpoSaleIds = rdc.ordhed_sales.Where(w => w.date_created.Value >= startDate && w.date_created.Value.Date <= endDate && w.orderreference.ToLower().Contains("cpo-")).Select(s => s.unique_id).ToList();



            listLateLines = rdc.orddet_lines.Where(w =>
            w.date_created.Value >= startDate
            && w.date_created.Value.Date <= endDate
            && (w.orderid_sales ?? "").Length > 0
            && !cpoSaleIds.Contains(w.orderid_sales)
            && !customreReturnIDs.Contains(w.orderid_sales)
            && !w.status.ToLower().Contains("rma")
            && !w.quote_line_uid.ToLower().Contains("gcat_quote_uid")
            && !NonLateStatus.Contains(w.status.ToLower())
            && !w.fullpartnumber.Trim().ToLower().Contains("gcat")
            && !w.customer_name.Trim().ToLower().Contains("amazon")
            && !w.vendor_name.Trim().ToLower().Contains("tbd")
            && (
            (w.status.ToLower() != "shipped" && DateTime.Today.AddDays(-1) >= w.projected_dock_date.Value.Date) // Not shipped but yesterday is greater that projected dock. 
            ||
            (w.status.ToLower() == "shipped" && w.ship_date_actual.Value.Date > w.projected_dock_date.Value.Date) // Shipped but after projected dock
            )
            ).ToList();


            listLateLines.Count();

            //Get line IDs for line process (late reason)            
            List<string> lateLineUids = listLateLines.Select(s => s.unique_id).Distinct().ToList();
            listLineProcesses = rdc.line_processes.Where(w => lateLineUids.Contains(w.orddet_line_uid) && (w.late_reason ?? "").Length > 0).ToList();
            listLineProcesses.Count();
        }


        foreach (orddet_line l in listLateLines)
        {
            LateLineObject lo = new LateLineObject();
            string lateReason = listLineProcesses.Where(w => w.orddet_line_uid == l.unique_id).Select(s => s.late_reason ?? "").FirstOrDefault();
            if (!cbShowAll.isChecked)
            {
                if (l.status.ToLower() == "shipped" && !string.IsNullOrEmpty(lateReason))
                    continue;
            }

            //Build Details
            lo.Date = l.date_created.Value.Date;
            lo.ProjDock = l.projected_dock_date.Value.Date;
            lo.ShipDate = l.ship_date_actual.Value.Date;
            lo.Part = l.fullpartnumber;
            lo.Status = l.status;
            lo.LineCode = l.linecode_sales.Value;
            lo.SO = l.ordernumber_sales;
            lo.Customer = l.customer_name;
            lo.Vendor = l.vendor_name;
            lo.Agent = l.seller_name;
            lo.InternalComment = l.internalcomment;
            //Late Reason

            lo.LateReason = "";
            if (!string.IsNullOrEmpty(lateReason))
                lo.LateReason = lateReason;
            //Edit Column
            lo.Edit = "";
            //Line ID, hidden
            lo.ID = l.unique_id;
            //Add to list
            listLateLineObjects.Add(lo);
        }


        //If Not Show All, hide lines that are resolved, i.e. Shipped, and has a non N/A reason.
        //if (!cbShowAll.isChecked)
        //    listLateLineObjects = listLateLineObjects.Where(w => w.Status.ToLower() != "shipped" && w.LateReason != "N/A").ToList();

        listLateLineObjects.Count();



    }

    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        TableCell lineIDCell = null;



        if (e.Row.Cells.Count > 1)
        {

            //Hide unique_id column, needs to happen out of datarow            
            lineIDCell = e.Row.Cells[12];
            lineIDCell.CssClass = "hiddencol";
        }


        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            //10. line uid
            //11. Edit Cell          
            //3. Part Number
            //5. linecode sales


            TableCell editCell = e.Row.Cells[11];
            string lineUid = lineIDCell.Text;
            LateLineObject lo = listLateLineObjects.Where(w => w.ID == lineUid).FirstOrDefault();

            //Edit Button with Modal
            LinkButton lbEditLine = new LinkButton();
            lbEditLine.ID = "lbEditLine";
            lbEditLine.OnClientClick = "return false;";
            //string PONUmber = partNumberCell.Text;
            lbEditLine.Text = "Edit";
            lbEditLine.CssClass = "editCell";
            editCell.Controls.Add(lbEditLine);


            //Add hiddenField to store line Details.
            List<string> lineDetails = new List<string>();
            lineDetails.Add(lo.ID);
            lineDetails.Add(lo.Part);
            lineDetails.Add(lo.LineCode.ToString());
            lineDetails.Add(lo.InternalComment);
            lineDetails.Add(lo.LateReason);
            lineDetails.Add(lo.ProjDock.ToShortDateString());

            HiddenField hfLineDetails = new HiddenField();
            hfLineDetails.Value = string.Join(",", lineDetails);
            editCell.Controls.Add(hfLineDetails);

        }
    }

    protected void lbRefresh_Click(object sender, EventArgs e)
    {
        LoadAll();
    }

    protected void lbModalSave_Click(object sender, EventArgs e)
    {
        string result = "";
        try
        {

            SaveLineData(out result);
            LoadAll();
            tools.HandleResult("success", result);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void SaveLineData(out string result)
    {
        result = "";
        string lineID = Tools.Strings.SanitizeInput(hfLineID.Value);
        string lateReason = ddlLateReason.SelectedValue;
        string notes = txtNotes.Text;
        string strProjDock = dtpProjectedDock.SelectedDate.Value.ToShortDateString();
        if (lateReason == "--Choose--")
            lateReason = null;

        if (string.IsNullOrEmpty(lineID))
        {
            result = "No Line ID Detected.";
            return;
        }


        using (RzDataContext rdc = new RzDataContext())
        {
            //Grab Line Item
            orddet_line l = rdc.orddet_lines.Where(w => w.unique_id == lineID).FirstOrDefault();
            if (l != null)
            {

                //if (!string.IsNullOrEmpty(lateReason))
                    SaveLineProcess(rdc, l, lateReason);
                

                if (!string.IsNullOrEmpty(notes))
                    l.internalcomment = notes;

                DateTime dtProjectedDockDate;
                if (DateTime.TryParse(strProjDock, out dtProjectedDockDate))
                {
                    if (dtProjectedDockDate.Date != l.projected_dock_date.Value.Date)
                        l.projected_dock_date = dtProjectedDockDate;
                }

            }
            rdc.SubmitChanges();
        }

        result = "Success";

    }


    private void SaveLineProcess(RzDataContext rdc, orddet_line l, string lateReason)
    {
        line_process lp = rdc.line_processes.Where(w => w.orddet_line_uid == l.unique_id).FirstOrDefault();
        if (lp == null)
        {
            //If there is no late reason, and there's no existing late reason in Line Process, do nothing,
            if (string.IsNullOrEmpty(lateReason))
                return;
            lp = new line_process();
            lp.unique_id = Guid.NewGuid().ToString();
            lp.date_created = DateTime.Now;
            rdc.line_processes.InsertOnSubmit(lp);
        }
        lp.orddet_line_uid = l.unique_id;
        lp.date_modified = DateTime.Now;
        lp.late_reason = lateReason;

    }


    protected void cb_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            LoadAll();
        }
        catch (Exception ex)
        {

        }



    }
}