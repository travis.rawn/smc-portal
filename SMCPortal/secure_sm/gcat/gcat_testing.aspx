﻿<%@ Page Title="GCAT Testing" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="gcat_testing.aspx.cs" Inherits="secure_members_qc_center_gcat_testing" %>

<%@ Register Src="~/assets/controls/InspectionImageManagerControl.ascx" TagPrefix="uc1" TagName="InspectionImageManagerControl" %>
<%@ Register Src="~/assets/controls/InspectionImageGallery.ascx" TagPrefix="uc1" TagName="InspectionImageGallery" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="/Content/css/sm-gcat.css" rel="stylesheet" />
    <script src="/Content/scripts/sm_gcat.js" type="text/javascript"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
    <h1 class="relative">GCAT Testing</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <%--JS Variables--%>
    <asp:HiddenField ID="hfCastellation" runat="server" />

    <%-- Container--%>
    <div class="container">
        <%--Status Panel--%>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div id="divGcatStatus" runat="server" visible="false">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h5>
                        <asp:Label ID="lblGcatStatus" runat="server"></asp:Label></h5>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <%--End Status Panel--%>

        <%--MPN and Header Panel--%>
        <asp:Panel ID="pnlManufacturerPartSearch" runat="server" DefaultButton="lbMPNSearch">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 well">
                    <div style="text-align: center">
                        <h5>To begin, please search for a manufacturer part number (MPN)</h5>
                    </div>
                    <div class="col-sm-6">
                        <h5>Search Existing:</h5>
                        <div class="input-group" style="white-space: nowrap;">
                            <asp:TextBox ID="txtMPNSearch" runat="server" CssClass="form-control" Placeholder="MPN Search"></asp:TextBox>
                            <asp:LinkButton ID="lbMPNSearch" runat="server" OnClick="lbMPNSearch_Click" CssClass="btn-smc btn-smc-primary"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="divCreateManufactuerPart" runat="server">
                            <h5><a href="#" onclick="ToggleShow($('#NewMPN'));">Create New:</a></h5>
                            <div class="input-group" style="white-space: nowrap; display: none;" id="NewMPN">
                                <asp:TextBox ID="txtHeaderMFG" runat="server" CssClass="form-control" Placeholder="Enter Manufacturer"></asp:TextBox>
                                <asp:LinkButton ID="lbCreateManufacturerPart" runat="server" OnClick="lbCreateManufacturerPart_Click" CssClass="btn-smc btn-smc-success"><i class="fa fa-check"></i></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </asp:Panel>
    </div>
    <div class="row">

        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <asp:Panel ID="pnlMPNGrid" runat="server" Visible="false">
                <asp:GridView ID="gvMPNSelection" runat="server" CssClass="table table-hover table-striped CenterDataView" GridLines="None" AutoGenerateColumns="false">
                    <EmptyDataTemplate>
                        <div style="text-align: center;">
                            <h5>No existing MPN records found.  To create a new one, please provide a manufacturer:</h5>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField SortExpression="Source" HeaderText="Source">
                            <ItemTemplate>
                                <asp:Button ID="btnSelectMPN" runat="server" Text="Select" CssClass="btn btn-sm btn-success btn-block" OnClick="btnSelectMPN_Click" />
                            </ItemTemplate>
                            <ItemStyle Wrap="True" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="MPN" HeaderText="MPN" SortExpression="MPN" />
                        <asp:BoundField DataField="Manufacturer" HeaderText="MFG" SortExpression="Manufacturer" />
                        <asp:BoundField DataField="ECCN" HeaderText="ECCN" SortExpression="ECCN" />
                        <asp:BoundField DataField="HarmonizedCode" HeaderText="HarmonizedCode" SortExpression="HarmonizedCode" />
                    </Columns>
                </asp:GridView>

            </asp:Panel>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <%--End MPN and Header Panel--%>
    <%--pnlGCATBody--%>
    <div class="row">
        <asp:Panel ID="pnlGCATMain" runat="server" Visible="false">
            <div class="col-sm-2">
                <nav class="navbar sm-gcat-nav">
                    <%-- <div class="container-fluid">--%>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#GCATNAV" style="background-color: black; float: left; margin-left: 10px;">
                            <span class="icon-bar" style="background-color: white !important;"></span>
                            <span class="icon-bar" style="background-color: white !important;"></span>
                            <span class="icon-bar" style="background-color: white !important;"></span>
                        </button>
                        <a class="navbar-brand" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="GCATNAV">
                        <div id="Tabs" style="padding: 10px; text-align: center;">
                            <%-- Nav Pills --%>
                            <ul class="nav nav-pills nav-stacked" id="tabs">
                                <li class="active"><a href="#Header" data-toggle="tab">Header</a></li>
                                <li><a href="#ComponentIdentity" data-toggle="tab">Component</a></li>
                                <li><a href="#VisualComponent" data-toggle="tab">Visual Component</a></li>
                                <li><a href="#VisualSurface" data-toggle="tab">Visual Surface</a></li>
                                <li><a href="#Required" data-toggle="tab">Required Tests</a></li>
                                <li><a href="#MarkingPerm" data-toggle="tab" id="aMarkingPerm" style="display: none">Marking</a></li>
                                <li><a href="#Xray" data-toggle="tab" id="aXray" style="display: none">X-Ray</a></li>
                                <li><a href="#BasicElectrical" data-toggle="tab" id="aBasicElectrical" style="display: none">Basic Electrical</a></li>
                                <li><a href="#Sentry" data-toggle="tab" id="aSentry" style="display: none">Sentry VI</a></li>
                                <li><a href="#Solderability" data-toggle="tab" id="aSolderability" style="display: none">Solderability</a></li>
                                <li><a href="#XRF" data-toggle="tab" id="aXRF" style="display: none">XRF Analysis</a></li>
                                <li><a href="#EPROM" data-toggle="tab" id="aEPROM" style="display: none">EPROM</a></li>
                                <li><a href="#ExtendedTesting" data-toggle="tab" id="aExtendedTesting" style="display: none">Extended Testing</a></li>
                                <li><a href="#HeatedSolvents" data-toggle="tab" id="aHeatedSolvents" style="display: none">Heated Solvents</a></li>
                                <li><a href="#MoistureBaking" data-toggle="tab" id="aMoistureBaking" style="display: none">Moisture / Baking</a></li>
                                <li><a href="#Decap" data-toggle="tab" id="aDecap" style="display: none">Decap</a></li>
                                <li><a href="#ExtendedImagery" data-toggle="tab" id="aExtendedImagery" style="display: none">Extended Imagery</a></li>
                                <li><a href="#Summary" data-toggle="tab" id="aSummary">Summary</a></li>
                            </ul>
                        </div>
                    </div>
                    <%-- </div>--%>
                </nav>
            </div>
            <%--  End Nav Pills--%>
            <div class="col-sm-10">
                <%--Tab Content--%>
                <div class="tab-content clearfix">
                    <%-- Header Tab--%>
                    <div class="tab-pane active" id="Header">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h5 class="gcat-report-test-title">
                                                <asp:Label ID="lblHeaderMPN" runat="server"></asp:Label></h5>
                                            <asp:LinkButton ID="lbChgangeMPN" runat="server" OnClick="lbChgangeMPN_Click" ToolTip="Change the Manufacturer Part (MPN) for this GCAT">
                                                        <span aria-hidden="true" class="fa fa-wrench" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>
                                                <asp:Label ID="lblHeaderMFG" runat="server"></asp:Label></h5>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>
                                                <asp:Label ID="lblDate" runat="server"></asp:Label></h5>
                                        </div>
                                        <div class="col-sm-2">
                                            <h5>
                                                <asp:Label ID="lblReportID" runat="server"></asp:Label></h5>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveHeader" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveHeader_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <asp:Panel ID="pnlHeaderData" runat="server" Visible="false">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h5>Customer:</h5>
                                                <asp:DropDownList ID="ddlChooseCustomer" runat="server" CssClass="form-control updateProgress" OnSelectedIndexChanged="ddlChooseCustomer_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4">
                                                <h5>Sales Order:</h5>

                                                <asp:DropDownList ID="ddlChooseSale" runat="server" CssClass="form-control updateProgress" OnSelectedIndexChanged="ddlChooseSale_SelectedIndexChanged" AutoPostBack="true" onchange="ShowUpdateProgress();"></asp:DropDownList>

                                            </div>
                                            <div class="col-sm-4">
                                                <h5>Rz Line:</h5>

                                                <asp:DropDownList ID="ddlLineItem" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlLineItem_SelectedIndexChanged" AutoPostBack="true" onchange="ShowUpdateProgress();"></asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="row">

                                            <asp:Panel ID="pnlSaleInfo" runat="server">
                                                <div class="col-sm-4">
                                                    <h5>Cust PO:</h5>
                                                    <asp:Label ID="lblHeaderCustomerPO" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h5>Rep:</h5>
                                                    <asp:Label ID="lblHeaderAccountManager" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h5>Cust Part:</h5>
                                                    <asp:Label ID="lblCustomerPart" runat="server"></asp:Label>
                                                </div>
                                            </asp:Panel>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h5>Quantity:</h5>
                                                <asp:TextBox ID="txtQuantity1" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-8">
                                                <h5>Summary Notes:</h5>
                                                <asp:TextBox ID="txtHeaderSummaryNotes" runat="server" TextMode="MultiLine" Style="width: 100%; max-width: 100%; height: 50px;" placeholder="Please provide a summary of the test results upon completion" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <%--  Header Imagery--%>
                                        <%-- Header Imagery--%>
                                        <h4><span class="label label-default center-block">Header Imagery</span></h4>
                                        <div class="row">
                                            <%--<asp:Panel ID="pnlHeaderImagery" runat="server">--%>
                                            <div class="col-sm-12 imageManagerGroup">
                                                <uc1:InspectionImageManagerControl runat="server" ID="imHeader1" />
                                                <uc1:InspectionImageManagerControl runat="server" ID="imHeader2" />
                                            </div>
                                            <%--  </asp:Panel>--%>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                    </div>
                    <%-- End Header Tab--%>

                    <%--Component Identity--%>
                    <div class="tab-pane" id="ComponentIdentity">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <label>Component Identification (From Datasheet):</label>
                                            <asp:LinkButton ID="lbGetDataSheet" CssClass="btn btn-sm btn-smc btn-smc-datasheet" runat="server" OnClick="lbGetDataSheet_Click" ToolTip="Search Datasheets for this part."><span style="font-size:18px;" class="fa fa-list-alt"></span>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveComponentIdentity" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveComponentIdentity_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h5>RoHS Compliant:</h5>
                                            <asp:DropDownList ID="ddlComponentIdentityRohs_compliant" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="U" Value="U"></asp:ListItem>
                                            </asp:DropDownList>
                                            <h5>Condition:</h5>
                                            <asp:CheckBoxList ID="cblComponentIdentityCondition" runat="server" CssClass="checkbox checkbox-inline">
                                                <asp:ListItem Text="New" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Used" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Refurbished" Value="3"></asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5>Memory Device:</h5>
                                            <asp:DropDownList ID="ddlComponentIdentityMemoryDevice" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="U" Value="U"></asp:ListItem>
                                            </asp:DropDownList>
                                            <h5>Type:</h5>
                                            <asp:CheckBoxList ID="cblComponentIdentityType" runat="server" CssClass="checkbox checkbox-inline">
                                                <asp:ListItem Text="Surface-mount" Value="sm"></asp:ListItem>
                                                <asp:ListItem Text="Through-hole" Value="th"></asp:ListItem>
                                                <asp:ListItem Text="BGA" Value="bga"></asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                        <div class="col-sm-4">
                                            <div>
                                                <h5 style="float: left;">Moisture Level:</h5>
                                                <a href="#" data-toggle="modal" data-target="#mdlMSL" style="clear: both"><i class="fal fa-question-square"></i></a>
                                                <%--<i class="far fa-question-circle" style="line-height: inherit; padding-left: 5px;"></i>--%>
                                            </div>
                                            <asp:DropDownList ID="ddlComponentIdentityMoistureLevel" runat="server" CssClass="form-control" onchange="handleDDL(this);">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="5A" Value="5A"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="2A" Value="2A"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                                                <asp:ListItem Text="Unknown" Value="Unknown"></asp:ListItem>
                                            </asp:DropDownList>


                                            <div id="mdlMSL" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3>Moisture Level Designations</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">

                                                                <div class="col-sm-8">
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">
                                                                            <label>MSL 6</label>
                                                                            – Mandatory Bake before use
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL 5A</label>
                                                                            – 24 hours
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL</label>
                                                                            5 – 48 hours
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL 4</label>
                                                                            – 72 hours
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL 3</label>
                                                                            – 168 hours
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL 2A</label>
                                                                            – 4 weeks
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL 2</label>
                                                                            – 1 year
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL 1</label>
                                                                            – Unlimited
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <label>MSL N/A</label>
                                                                            - No applicable MSL
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h5>Traceability:</h5>
                                            <asp:DropDownList ID="ddlComponentIdentityTraceability" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Y" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h5>Package Type:</h5>
                                            <asp:TextBox ID="txtComponentIdentityPackageType" runat="server" CssClass="form-control"></asp:TextBox>
                                            <h5>Width:</h5>
                                            <asp:TextBox ID="txtComponentIdentityWidth" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5>Lead / Pin / BGA Pitch:</h5>
                                            <asp:TextBox ID="txtComponentIdentityLeadPinBGAPitch" runat="server" CssClass="form-control"></asp:TextBox>
                                            <h5>Length</h5>
                                            <asp:TextBox ID="txtComponentIdentityLength" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5>Lead / Pin / BGA Count:</h5>
                                            <asp:TextBox ID="txtComponentIdentityLeadPinBGACount" runat="server" CssClass="form-control"></asp:TextBox>
                                            <h5>Thickness:</h5>
                                            <asp:TextBox ID="txtComponentIdentityThickness" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End Component Identity--%>

                    <%--Required--%>
                    <div class="tab-pane" id="Required">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Required Tests</label>
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveRequired" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveRequired_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <%--Customer Category--%>
                                    <div class="input-group">
                                        <h5>Customer Categorization:</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlRequiredCatTypeA" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Undefined" Value="Undefined"></asp:ListItem>
                                                    <asp:ListItem Text="Military" Value="Military"></asp:ListItem>
                                                    <asp:ListItem Text="US Government" Value="US Government"></asp:ListItem>
                                                    <asp:ListItem Text="Medical" Value="Medical"></asp:ListItem>
                                                    <asp:ListItem Text="Aerospace" Value="Aerospace"></asp:ListItem>
                                                    <asp:ListItem Text="Commercial" Value="Commercial"></asp:ListItem>
                                                    <asp:ListItem Text="Authorized/Franchised" Value="Authorized/Franchised"></asp:ListItem>
                                                    <asp:ListItem Text="Contract Manufacturer (CEM)" Value="Contract Manufacturer (CEM)"></asp:ListItem>
                                                    <asp:ListItem Text="Original Equipment Manufacturer (OEM)" Value="Original Equipment Manufacturer (OEM)"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlRequiredCatTypeB" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Undefined" Value="Undefined"></asp:ListItem>
                                                    <asp:ListItem Text="Military" Value="Military"></asp:ListItem>
                                                    <asp:ListItem Text="US Government" Value="US Government"></asp:ListItem>
                                                    <asp:ListItem Text="Medical" Value="Medical"></asp:ListItem>
                                                    <asp:ListItem Text="Aerospace" Value="Aerospace"></asp:ListItem>
                                                    <asp:ListItem Text="Commercial" Value="Commercial"></asp:ListItem>
                                                    <asp:ListItem Text="Authorized/Franchised" Value="Authorized/Franchised"></asp:ListItem>
                                                    <asp:ListItem Text="Contract Manufacturer (CEM)" Value="Contract Manufacturer (CEM)"></asp:ListItem>
                                                    <asp:ListItem Text="Original Equipment Manufacturer (OEM)" Value="Original Equipment Manufacturer (OEM)"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlRequiredCatTypeC" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Undefined" Value="Undefined"></asp:ListItem>
                                                    <asp:ListItem Text="Military" Value="Military"></asp:ListItem>
                                                    <asp:ListItem Text="US Government" Value="US Government"></asp:ListItem>
                                                    <asp:ListItem Text="Medical" Value="Medical"></asp:ListItem>
                                                    <asp:ListItem Text="Aerospace" Value="Aerospace"></asp:ListItem>
                                                    <asp:ListItem Text="Commercial" Value="Commercial"></asp:ListItem>
                                                    <asp:ListItem Text="Authorized/Franchised" Value="Authorized/Franchised"></asp:ListItem>
                                                    <asp:ListItem Text="Contract Manufacturer (CEM)" Value="Contract Manufacturer (CEM)"></asp:ListItem>
                                                    <asp:ListItem Text="Original Equipment Manufacturer (OEM)" Value="Original Equipment Manufacturer (OEM)"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <%--Standardized Tests--%>
                                    <%-- <div class="input-group">--%>
                                    <h5>Standardized Tests:</h5>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="checkbox">
                                                <label>
                                                    <asp:CheckBox ID="cbxIsCCAP" Text="CCAP-101" runat="server" />
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="checkbox">
                                                <label>
                                                    <asp:CheckBox ID="cbxIsAS6081" Text="AS 6081" runat="server" />
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-8" style="text-align: left !important">
                                            <div class="checkbox">
                                                <label>
                                                    <em>**IDEA 1010 & SMC GCAT included in all GCAT tests</em>
                                                </label>
                                            </div>

                                        </div>

                                    </div>

                                    <%--Customer Requirement--%>
                                    <h5>Additional Customer Requirements:</h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtCustomer_RequirementA" runat="server" CssClass="form-control" placeholder="Additional Customer Requirements" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>

                                    <%-- Test Type--%>
                                    <div id="divSelectedTests">
                                        <h5>Test Type:</h5>
                                        <%--New--%>
                                        <div class="row">

                                            <%--Marking Permenancy--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeMarkingPerm" Text="Marking Permanency" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>
                                            </div>


                                            <%--Xray--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeXRay" Text="X-Ray" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>

                                            </div>


                                            <%--Basic Elec--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeBasicElectrical" Text="Basic Electrical" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>
                                            </div>


                                            <%--Sentry--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeSentryVI" Text="Sentry V-I Signature" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>

                                            </div>


                                        </div>
                                        <div class="row">


                                            <%--Solderability--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeSolderability" Text="Solderability" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>
                                            </div>


                                            <%--XRF--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeXRF" Text="XRF Analysis" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>

                                            </div>


                                            <%--EPROM--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeEprom" Text="EPROM" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>
                                            </div>


                                            <%--Extended Testing--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeExtendedTesting" Text="Extended Testing" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>
                                            </div>



                                        </div>
                                        <div class="row">


                                            <%--Heated Solvents--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeHeatedSolvents" Text="Heated Solvents" runat="server" onchange="ShowUpdateProgress();" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" />
                                                    </label>
                                                </div>
                                            </div>


                                            <%--Decap--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeDecap" Text="De-Capsulation" runat="server" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" onchange="ShowUpdateProgress();" /><%--cbxShowHideControl(this, 'aDecap');--%>
                                                    </label>
                                                </div>
                                            </div>


                                            <%-- Moisture / Baking--%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeMoistureBaking" Text="Moisture/Baking" runat="server" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" onchange="ShowUpdateProgress();" />
                                                    </label>
                                                </div>
                                            </div>


                                            <%-- Extended Imagery --%>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <asp:CheckBox ID="cbxTestTypeExtendedImagery" Text="Extended Imagery" runat="server" OnClick="confirmTestTypeDelete(this)" OnCheckedChanged="cbxTestType_CheckedChanged" AutoPostBack="true" onchange="ShowUpdateProgress();" />
                                                    </label>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                Other:
                                        <asp:TextBox ID="txtTest_TypeJ" runat="server" placeholder="Additional Test Type" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--     </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                    <%--End Required--%>

                    <%--Visual Component--%>
                    <div class="tab-pane" id="VisualComponent">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>External Visual Component Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtVisualComponentSamplyQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveVisualComponent" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveVisualComponent_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <h5>Dimension</h5>
                                                </div>
                                                <div class="col-sm-2">
                                                    <h5>Actual</h5>
                                                </div>
                                                <div class="col-sm-2">
                                                    <h5>Unit</h5>
                                                </div>
                                                <div class="col-sm-2">
                                                    <h5>Result</h5>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h5>Comments</h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Width:</label>

                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtVisualComponentWidthActual" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtVisualComponentWidthUnit" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlVisualComponentPassWidth" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                        <asp:ListItem Text="-Choose-" Value="-1"></asp:ListItem>
                                                        <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Fail" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtVisualComponentPassWidthComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Length:</label>

                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtVisualComponentLengthActual" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtVisualComponentLengthUnit" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlVisualComponentPassLength" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                        <asp:ListItem Text="-Choose-" Value="-1"></asp:ListItem>
                                                        <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Fail" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtVisualComponentPassLengthComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Thickness:</label>

                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtVisualComponentThicknessActual" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtVisualComponentThicknessUnit" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlVisualComponentPassThickness" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                        <asp:ListItem Text="-Choose-" Value="-1"></asp:ListItem>
                                                        <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Fail" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtVisualComponentPassThicknessComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>


                                            <hr />
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <h5>Package Connection Type:</h5>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlVisualComponentPackageConnectionType" runat="server" CssClass="form-control" onchange="handleVisualComponentPackageConnectionType();">
                                                        <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Lead" Value="Lead"></asp:ListItem>
                                                        <asp:ListItem Text="Leadless" Value="Leadless"></asp:ListItem>
                                                        <asp:ListItem Text="BGA" Value="BGA"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h5>Date Code(s):</h5>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtComponentIdentityDateCodes" runat="server" CssClass="form-control center-block" TextMode="MultiLine" placeholder="Example: 0304, 0612, 0621, 0631"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--Lead Package Grid--%>
                                            <div class="row" id="divVisualComponentCastellation" runat="server">
                                                <div class="col-sm-3">
                                                    <h5>Lead / Pin / BGA / Castellation Count</h5>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlVisualComponentCastellationResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                        <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                        <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtVisualComponentCastellationComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h5>Property</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <h5>Result</h5>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Comments</h5>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentLeadPadCondition" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Lead and Pad Condition</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentLeadPadConditionResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentLeadPadConditionComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentCoplanarity" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Lead Coplanarity</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentCoplanarityResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentCoplanarityComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentOxidation" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Oxidation / Discoloration</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentOxidationResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentOxidationComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentExposedBaseMetal" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Signs of Exposed base metal</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentExposedBaseMetalResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentExposedBaseMetalComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentSolderBalls" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Solder Balls / Spheres</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentSolderResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentSolderComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentConcentricity" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Uniform / Concentricity</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentConcentricityResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentConcentricityComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVisualComponentDamagedSpheres" runat="server">
                                                        <div class="col-sm-4">
                                                            <label>Crushed Damaged Spheres</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddlVisualComponentDamagedSpheresResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtVisualComponentDamagedSpheresComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <%--Imagery--%>
                                            <hr />
                                            <%-- VisualComponent Imagery--%>
                                            <h4><span class="label label-default center-block">Visual Component Imagery</span></h4>
                                            <div class="row">
                                                <div class="col-sm-12 imageManagerGroup">
                                                    <uc1:InspectionImageManagerControl runat="server" ID="imVisualComponent1" />
                                                    <uc1:InspectionImageManagerControl runat="server" ID="imVisualComponent2" />
                                                    <uc1:InspectionImageManagerControl runat="server" ID="imVisualComponent3" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End Visual Component--%>

                    <%-- Visual Surface--%>
                    <div class="tab-pane" id="VisualSurface">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>External Visual Surface Analysis (IDEA-1010)</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtVisualSurfaceSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveVisualSurface" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveVisualSurface_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Logos / Part Numbers</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlVisualSurfaceLogo" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtVisualSurfaceLogo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Top Surface</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlVisualSurfaceTop" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtVisualSurfaceTopComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Bottom Surface</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlVisualSurfaceBot" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtVisualSurfaceBotComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%--Imagery--%>

                                    <h4><span class="label label-default center-block">Visual Surface Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imVisualSurface1" showDescription="false" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imVisualSurface2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imVisualSurface3" />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <%--  </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                    <%--End Visual Surface--%>

                    <%-- Marking Permenancy --%>
                    <div class="tab-pane" id="MarkingPerm">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Marking Permenancy Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtMarkingPermSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveMarkingPerm" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveMarkingPerm_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Acetone Test</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMarkingPermAcetone" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtMarkingPermAcetoneComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Alcohol Mineral Spirits</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMarkingPermAlcohol" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtMarkingPermAlcoholComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Scrape Test</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMarkingPermScrape" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtMarkingPermScrapeComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Secondary Coating</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMarkingPermSecondary" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtMarkingPermSecondaryComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Part Number / Marking</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMarkingPermPartMarking" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtMarkingPermPartMarkingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Case Marking / Logo</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlMarkingPermCase" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtMarkingPermCaseComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <%--Imagery--%>

                                    <hr />

                                    <h4><span class="label label-default center-block">Marking Permenancy Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imMarkingPerm1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imMarkingPerm2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imMarkingPerm3" />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End Marking Permenancy--%>

                    <%-- Xray --%>
                    <div class="tab-pane" id="Xray">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>X-Ray Analysis / Die Verification</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtXraySampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveXray" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveXray_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Die Present</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXrayDiePresent" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXrayDiePresentComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Bond Wires Present?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXrayBondWires" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXrayBondWiresComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Damaged / Missing Bond Wires?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXrayDamagedBond" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXrayDamagedBondComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Die Lead Uniformity</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXrayDieLead" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXrayDieLeadComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Die Frame Size / Location Consistent?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXrayDieFrame" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXrayDieFrameComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Imagery--%>
                                    <hr />

                                    <h4><span class="label label-default center-block">X-Ray Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imXray1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imXray2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imXray3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End Xray--%>

                    <%--  Solderability --%>
                    <div class="tab-pane" id="Solderability">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Solderability Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtSolderSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveSolder" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveSolder_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="checkbox">
                                                <label>
                                                    <asp:CheckBox ID="cbxSolderSPRequired" Text="Service Provider Required to complete analysis:" runat="server" onchange="cbxShowHideControl(this, 'txtSolderSPRequired');" />
                                                </label>
                                            </div>
                                            <asp:TextBox ID="txtSolderSPRequired" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Service Provider Comments"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>Package Mount Type:</p>
                                            <asp:DropDownList ID="ddlSolderPackageMountType" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="BGA" Value="BGA"></asp:ListItem>
                                                <asp:ListItem Text="Leadless Chip Carrier" Value="Leadless Chip Carrier"></asp:ListItem>
                                                <asp:ListItem Text="Plated Through-Hole" Value="Plated Through-Hole"></asp:ListItem>
                                                <asp:ListItem Text="Surface Mount" Value="Surface Mount"></asp:ListItem>
                                                <asp:ListItem Text="Through-Hole" Value="Through-Hole"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Wetting Acceptable?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSolderWetting" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSolderWettingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>RoHS?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSolderRohs" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSolderRohsComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Void Free Leads?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSolderVoid" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSolderVoidComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Contaminant Free?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSolderCont" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSolderContComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>


                                    <%--Imagery--%>

                                    <hr />
                                    <h4><span class="label label-default center-block">Solderability Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imSolder1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imSolder2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imSolder3" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End Solderability--%>

                    <%--  Eprom --%>
                    <div class="tab-pane" id="EPROM">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>EPROM Testing and Programming Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtEpromSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveEprom" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveEprom_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="checkbox">
                                                <label>
                                                    <asp:CheckBox ID="cbxEpromSPRequired" Text="Service Provider Required to complete analysis:" runat="server" onchange="cbxShowHideControl(this, 'txtEpromSPRequired');" />
                                                </label>
                                            </div>
                                            <%-- <p>
                                                        Service Provider Required to complete analysis:
                                            <input type="checkbox" id="cbxEpromSPRequired" runat="server" onchange="cbxShowHideControl(this, 'txtEpromSPRequired');" />
                                                    </p>--%>
                                            <asp:TextBox ID="txtEpromSPRequired" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Service Provider Comments"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>Package Mount Type:</p>
                                            <asp:DropDownList ID="ddlEpromPackageMount" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="BGA" Value="BGA"></asp:ListItem>
                                                <asp:ListItem Text="Leadless Chip Carrier" Value="Leadless Chip Carrier"></asp:ListItem>
                                                <asp:ListItem Text="Plated Through-Hole" Value="Plated Through-Hole"></asp:ListItem>
                                                <asp:ListItem Text="Surface Mount" Value="Surface Mount"></asp:ListItem>
                                                <asp:ListItem Text="Through-Hole" Value="Through-Hole"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Checksum Analysis</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlEpromChecksum" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtEpromChecksumComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="checkbox">
                                                <h5 style="float: left;">Programming Required?</h5>
                                                <span style="float: right">
                                                    <asp:CheckBox ID="cbxEpromProgrammingRequired" runat="server" onchange="ShowHideControls();" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="divEpromVerify" runat="server">
                                        <div class="col-sm-12">
                                            <h5>Verify Program Load</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlEpromVerify" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtEpromVerifyComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End Eprom--%>

                    <%--  Basic Electrical --%>
                    <div class="tab-pane" id="BasicElectrical">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Basic Electrical Testing</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtBasicElectricalSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveBasicElectrical" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveBasicElectrical_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">

                                            <div class="col-sm-3">
                                            </div>
                                            <div class="col-sm-6">
                                            </div>
                                            <div class="col-sm-3">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5>Test / Application Requirement</h5>
                                        </div>
                                        <div class="col-sm-2">
                                            <h5>Temperature</h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5>Result</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtBasicElectricalTest1" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtBasicElectricalCondition1" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlBasicElectricalResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                <asp:ListItem Text="-Choose-" Value="-Choose-"></asp:ListItem>
                                                <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                                <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                                <asp:ListItem Text="Inconclusive" Value="Inconclusive"></asp:ListItem>
                                                <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Result Notes</h5>
                                            <asp:TextBox ID="txtBasicElectricalNotes" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%--Imagery--%>
                                    <hr />
                                    <h4><span class="label label-default center-block">Basic Electrical Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imBasicElectrical1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imBasicElectrical2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imBasicElectrical3" />

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End Basic Electrical --%>

                    <%--  Sentry --%>
                    <div class="tab-pane" id="Sentry">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Sentry V-I Signature Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtSentrySampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveSentry" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveSentry_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="checkbox">
                                                <label>
                                                    <asp:CheckBox ID="cbxSentrySPRequired" Text="Service Provider Required to complete analysis:" runat="server" onchange="cbxShowHideControl(this, 'txtSentrySPRequired');" />
                                                </label>
                                            </div>
                                            <asp:TextBox ID="txtSentrySPRequired" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Service Provider Comments"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>Package Mount Type:</p>
                                            <asp:DropDownList ID="ddlSentryPackageMountType" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="BGA" Value="BGA"></asp:ListItem>
                                                <asp:ListItem Text="Leadless Chip Carrier" Value="Leadless Chip Carrier"></asp:ListItem>
                                                <asp:ListItem Text="Plated Through-Hole" Value="Plated Through-Hole"></asp:ListItem>
                                                <asp:ListItem Text="Surface Mount" Value="Surface Mount"></asp:ListItem>
                                                <asp:ListItem Text="Through-Hole" Value="Through-Hole"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Signatures Match Standard?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSentrySignaturesMatch" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSentrySignaturesMatchComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Open / shorts detected?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSentryOpenShorts" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSentryOpenShortsComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Passed all signature analysis?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSentryPassedAllSignatureAnalysis" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSentryPassedAllSignatureAnalysisComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Overall Result</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSentryOverallResult" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Success" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Failure" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                    </div>


                                    <%--Imagery--%>

                                    <hr />
                                    <h4><span class="label label-default center-block">Sentry VI Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imSentry1" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End Sentry--%>

                    <%--  XRF --%>
                    <div class="tab-pane" id="XRF">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>XRF Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtXRFSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveXRF" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveXRF_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="checkbox">
                                                <label>
                                                    <asp:CheckBox ID="cbxXRFSPRequired" Text="Service Provider Required to complete analysis:" runat="server" onchange="cbxShowHideControl(this, 'txtXRFSPRequired');" />
                                                </label>
                                            </div>
                                            <asp:TextBox ID="txtXRFSPRequired" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Service Provider Comments"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>Package Mount Type:</p>
                                            <asp:DropDownList ID="ddlXRFPackageType" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="BGA" Value="BGA"></asp:ListItem>
                                                <asp:ListItem Text="Leadless Chip Carrier" Value="Leadless Chip Carrier"></asp:ListItem>
                                                <asp:ListItem Text="Plated Through-Hole" Value="Plated Through-Hole"></asp:ListItem>
                                                <asp:ListItem Text="Surface Mount" Value="Surface Mount"></asp:ListItem>
                                                <asp:ListItem Text="Through-Hole" Value="Through-Hole"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Lead Materials Match?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXRFLeadMaterialsMatch" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXRFLeadMaterialsMatchComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Foreign Materials Detected?</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlXRFForeignMaterials" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="2"></asp:ListItem>

                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtXRFForeignMaterialsComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>


                                    <%--Imagery--%>

                                    <hr />
                                    <h4><span class="label label-default center-block">XRF Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imXRF1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imXRF2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imXRF3" />
                                        </div>
                                    </div>




                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End XRF--%>



                    <%-- Heated Solvents --%>
                    <div class="tab-pane" id="HeatedSolvents">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Heated Solvents Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtHeatedSolventsSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveHeatedSolvents" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveHeatedSolvents_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Methyl Pyrrolidinone</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlHeatedMeth" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtHeatedMethComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Dynasolve 750</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlHeatedDyn" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtHeatedDynComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Secondary Coating (no evidence)</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlHeatedSecondary" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtHeatedSecondaryComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <%--Imagery--%>

                                    <hr />
                                    <h4><span class="label label-default center-block">Heated Solvents Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imHeated1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imHeated2" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End Heated Solvents--%>

                    <%--  Decapsulation and Die Analysis --%>
                    <div class="tab-pane" id="Decap">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Decapsulation and Die Analysis</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtDecapSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveDecap" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveDecap_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>MFG Logo / ID Present</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlDecapMfgLogo" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDecapMfgLogoComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Logo / ID Match Casing</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlDecapCasing" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDecapCasingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>P/N and Die# Match</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlDcapPartDie" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Inconclusive" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDcapPartDieComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Die and Bond Wires</h5>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlDecapDieBond" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                    <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Pass" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Fail" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDecapDieBondComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Decap Imagery--%>
                                    <hr />
                                    <h4><span class="label label-default center-block">Solderability Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imDecap1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imDecap2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imDecap3" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imDecap4" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End  Decapsulation and Die Analysis--%>

                    <%--  Extended Testing --%>
                    <div class="tab-pane" id="ExtendedTesting">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Extended Testing</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Sample Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtExtendedTestingSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveExtended" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveExtended_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <h5>Testing Category</h5>
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>Overall Result</h5>
                                        </div>


                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlExtCategory" runat="server" CssClass="form-control" onchange="ddlShowHideText(this, 'txtExtCategoryOther', 'Other');">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Dynamic" Value="Dynamic"></asp:ListItem>
                                                <asp:ListItem Text="Parametric / Dynamic" Value="Parametric / Dynamic"></asp:ListItem>
                                                <asp:ListItem Text="Extended Electrical" Value="Extended Electrical"></asp:ListItem>
                                                <asp:ListItem Text="Service Provider Testing" Value="Service Provider Testing"></asp:ListItem>
                                                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtExtCategoryOther" runat="server" CssClass="form-control" placeholder="Other Testing Category"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlExtTestingResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                <asp:ListItem Text="-Choose-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Pass" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="Fail" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="Inconclusive" Value="I"></asp:ListItem>
                                                <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h5>Condition Tested</h5>

                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-2">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtExtCondition1" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5>Result Notes</h5>
                                            <asp:TextBox ID="txtExtNotes" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%--Imagery--%>

                                    <hr />
                                    <h4><span class="label label-default center-block">Solderability Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imExt1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imExt2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imExt3" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imExt4" />
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>

                    </div>
                    <%--End Extended Testing--%>

                    <%--  Moisture / Baking--%>
                    <div class="tab-pane" id="MoistureBaking">

                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Moisture / Baking</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="control-group" style="display: inline-block;">
                                                <label>Total Qty:</label>
                                            </div>
                                            <div class="control-group" style="display: inline-block; max-width: 75px;">
                                                <asp:TextBox ID="txtMoistureBakingSampleQTY" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lbSaveMoistureBaking" CssClass="btn btn-sm btn-success updateProgress" runat="server" OnClick="lbSaveMoistureBaking_Click">
    <span aria-hidden="true" class="fa fa-save" style="font-size:15px;"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <%--Row1--%>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h5>MSL:</h5>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtMoistureBakingMSL" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>Process:</h5>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtMoistureBakingProcess" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%--Row2--%>
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <h5>Bake Temperature:</h5>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtMoistureBakingTemp" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h5>Bake Duration:</h5>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtMoistureBakingDuration" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%--Row3--%>

                                    <div class="row" id="divMoistureBakingResult" runat="server">
                                        <div class="col-sm-3">
                                            <h5>Overall Result</h5>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlMoistureBakingResult" runat="server" CssClass="form-control" onchange="autofillTextbox(this);">
                                                <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                                <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                                <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtMoistureBakingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>


                                    <%--Imagery--%>
                                    <hr />
                                    <h4><span class="label label-default center-block">Moisture/Baking Imagery</span></h4>
                                    <div class="row">
                                        <div class="col-sm-12 imageManagerGroup">
                                            <uc1:InspectionImageManagerControl runat="server" ID="imMoistureBaking1" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imMoistureBaking2" />
                                            <uc1:InspectionImageManagerControl runat="server" ID="imMoistureBaking3" />
                                        </div>
                                    </div>
                                    <%--End Imagery--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End  Moisture / Baking--%>




                    <%--  Extended Imagery--%>
                    <div class="tab-pane" id="ExtendedImagery">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Extended Imagery</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-body">
                                  <div class="row">
                                      <div class="col.sm-12">
                                          <uc1:InspectionImageGallery runat="server" ID="iig" />
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>






                    <%--  Summary --%>
                    <div class="tab-pane" id="Summary">
                        <div class="panel-group">
                            <div class="panel panel-default ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label>Summary of Results</label>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Test Performed</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Result</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Comments</label>
                                        </div>
                                    </div>

                                    <%--External Visual Summary--%>
                                    <div id="divSumExternalVisual" runat="server">
                                        <h5>External / Visual</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Logos / Part Numbers
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumExternalVisualLogosParts" runat="server" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumExternalVisualLogosPartsComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Dimensional Requirements
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumExternalVisualDim" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumExternalVisualDimComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Lead / Package Condition
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumExternalVisualLeadPkg" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumExternalVisualLeadPkgComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Check for Signs of Surface Alteration
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumExternalVisualAlteration" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumExternalVisualAlterationComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End External Visual Summary--%>

                                    <%--Marking Permenancy Summary--%>
                                    <div id="divSumMarking" runat="server">
                                        <h5>Marking Permenancy</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Acetone Test
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumMarkingAcetone" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumMarkingAcetoneComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Scrape Test
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumMarkingScrape" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumMarkingScrapeComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Check for Signs of Marking Alteration
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumMarkingAlteration" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumMarkingAlterationComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Marking Permenancy Summary--%>

                                    <%--Xray Summary--%>
                                    <div id="divSumXray" runat="server">
                                        <h5>X-Ray Analysis</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Uniform Die / Wire Bonds
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumXrayUniform" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumXrayUniformComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Xray Summary--%>

                                    <%--Basic Electrical Summary--%>
                                    <div id="divSumBasicElec" runat="server">
                                        <h5>Basic Electrical Analysis</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Basic Electrical Testing
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumBasicElectrical" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumBasicElectricalComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Basic Electrical Summary--%>

                                    <%--Sentry VI Summary--%>
                                    <div id="divSumSentry" runat="server">
                                        <h5>Sentry VI Electrical Analysis</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Sentry VI Signature
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumElectricalVISignature" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumElectricalVISignatureComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%--End Sentry VI Summary--%>


                                    <%--Solderability Summary--%>
                                    <div id="divSumSolderability" runat="server">
                                        <h5>Solderability Analysis</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Solder Wetting Acceptable
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumSolderWetting" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumSolderWettingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Passed Solderability Testing
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumSolderPassSolderTesting" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumSolderPassSolderTestingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Solderability Summary--%>


                                    <%--XRF Summary--%>
                                    <div id="divSumXRF" runat="server">
                                        <h5>XRF Analysis</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Lead Materials Match
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumXRFLeadsMatch" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumXRFLeadsMatchComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Evidence of Foreign Material
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumXRFForeignMat" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumXRFForeignMatComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End XRF Summary--%>


                                    <%--EPROM Summary--%>
                                    <div id="divSumEPROM" runat="server">
                                        <h5>EPROM Testing</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Checksum and Blank Check
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumEPROMChecksum" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumEPROMChecksumComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row" id="divSumEpromVerify" runat="server">
                                            <div class="col-sm-4">
                                                Validation
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumEPROMValidation" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumEPROMValidationComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End EPROM Summary--%>


                                    <%--Extended Testing Summary--%>
                                    <div id="divSumExtendedTesting" runat="server">
                                        <h5>Extended Testing</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Extended Testing
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumExtendedTesting" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumExtendedTestingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Extended Testing Summary--%>


                                    <%--Heated Solvents Summary--%>
                                    <div id="divSumHeatedSolvents" runat="server">
                                        <h5>Heated Solvents Testing</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Methyl Pyrrolidinone Results
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumHeatedSolventsMeth" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumHeatedSolventsMethComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Dynasolve 750 Results
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumHeatedSolventsDyna" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumHeatedSolventsDynaComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Secondary Coating Results
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumHeatedSolventsSecondary" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumHeatedSolventsSecondaryComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Heated Solvents Summary--%>


                                    <%--Decap Summary--%>
                                    <div id="divSumDecap" runat="server">
                                        <h5>De-Cap Analysis</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Wire Bond Integrity
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumDecapWireBondIntegrity" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumDecapWireBondIntegrityComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Die Marking Verification
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumDecapDieMarking" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumDecapDieMarkingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Decap Summary--%>



                                    <%--Moisture / Baking Summary--%>
                                    <div id="divMoistureBaking" runat="server">
                                        <h5>Moisture / Baking</h5>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Moisture / Baking Result
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="lblSumMoistureBakingResult" runat="server" Font-Bold="true" CssClass="labelTextBox"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtSumMoistureBakingComment" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- End Moisture / Baking Summary--%>
                                </div>
                            </div>
                        </div>
                        <%--  </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                    <%--End Summary--%>
                </div>
            </div>
            <%-- End Tab Content--%>
        </asp:Panel>
        <%-- End pnlGCATMain--%>
    </div>
    <%--End Container--%>

    <%--Save Panel--%>
    <div id="savePanel" style="position: fixed; top: 30%; right: 1em; line-height: 20px;">
        <div class="row">
            <a href="gcat_search.aspx" class="btn btn-smc btn-smc-warning" title="Return to search">
                <span aria-hidden="true" class="fa fa-reply" style="font-size: 30px; width: 25px;"></span>
            </a>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbPDF" runat="server" CssClass="btn btn-smc btn-smc-pdf" OnClick="lbPDF_Click" OnClientClick="slideAlert('success', 'Your PDF is being generated and will be downloaded momentarily.');" ToolTip="Download PDF">
                        <span aria-hidden="true" class="far fa-file-pdf" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbSaveAll" runat="server" CssClass="btn btn-smc btn-smc-primary" OnClick="lbSaveAll_Click" Visible="false" OnClientClick="ShowUpdateProgress();" ToolTip="Save all GCAT data">
    <span aria-hidden="true" class="fa fa-save" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbComplete" runat="server" CssClass="btn btn-smc btn-smc-success" OnClick="lbComplete_Click" OnClientClick="return ValidateForm();" ToolTip="Complete / Un-complete this GCAT" Visible="false">
     <span aria-hidden="true" class="fas fa-check" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbApprove" runat="server" CssClass="btn btn-smc btn-smc-primary" OnClick="lbApprove_Click" ToolTip="Approve this GCAT" Visible="false">
    <span aria-hidden="true" class="fa fa-thumbs-up" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbSetDemoGCAT" runat="server" CssClass="btn btn-smc btn-smc-info" OnClick="lbSetDemoGCAT_Click" ToolTip="Duplicate this GCAT as a demo." Visible="false">
    <span aria-hidden="true" class="fa fa-copy" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbDeleteAll" runat="server" CssClass="btn btn-smc btn-smc-danger" OnClientClick="return doDelete();" OnClick="lbDeleteAll_Click" Visible="false" ToolTip="Delete / Restore this GCAT">
                <span aria-hidden="true" class="fa fa-times" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
        <div class="row">
            <asp:LinkButton ID="lbRestore" runat="server" CssClass="btn btn-smc btn-smc-primary" OnClick="lbRestore_Click" Visible="false" ToolTip="Restore this GCAT">
    <span aria-hidden="true" class="fa fa-history" style="font-size:30px; width:25px;"></span>
            </asp:LinkButton>
        </div>
    </div>
    <%-- End Save Panel--%>


    <%-- Datasheets Modal--%>
    <%--  <asp:UpdatePanel ID="upDatasheets" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>--%>
    <div id="mdlDatasheets" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-scroll">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3>Datasheets:</h3>
                </div>
                <div class="modal-body modal-body-scroll">
                    <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
                        <asp:GridView ID="gvDatasheets" runat="server" CssClass="table table-hover table-striped CenterDataView" GridLines="None" AutoGenerateColumns="false">
                            <EmptyDataTemplate>
                                <div class="alert alert-warning" style="text-align: center;">
                                    <h5>No PCN Data found for search term.</h5>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField DataField="PartNumber" HeaderText="Part" SortExpression="PartNumber" />
                                <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" SortExpression="Manufacturer" />                                
                                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                <asp:TemplateField SortExpression="Datasheet" HeaderText="Datasheet">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlDatasheet" runat="server" CssClass="btn btn-sm btn-success" NavigateUrl='<%# Eval("Datasheet") %>' Target="_blank"><span style="font-size:20px;" class="fa fa-list-alt"></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="True" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
    <%-- End Datasheets Modal--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

