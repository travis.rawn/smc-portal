﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_members_qc_center_gcat_testing : Page
{
    private int GCATID
    {
        get { return Convert.ToInt32(ViewState["GCATID"] ?? GetGCATID()); }
        set { ViewState["GCATID"] = value; }
    }
    private string MPN
    {
        get { return ViewState["MPN"].ToString(); }
        set { ViewState["MPN"] = value; }
    }

    private string MFG
    {
        get { return ViewState["MFG"].ToString(); }
        set { ViewState["MFG"] = value; }
    }
    private int MPNID
    {
        get { return Convert.ToInt32(ViewState["MPNID"]); }
        set { ViewState["MPNID"] = value; }
    }

    private List<string> SelectedTests
    {
        get { return (List<string>)ViewState["SelectedTests"]; }
        set { ViewState["SelectedTests"] = value; }
    }

    int UploadedImageCount = 0;



    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    gcatDataContext ciq = new gcatDataContext();
    RzDataContext rdc = new RzDataContext();
    sm_binaryEntities smb = new sm_binaryEntities();
    SM_Quality_Logic ql = new SM_Quality_Logic();

    ordhed_sale sale;
    orddet_line line;

    ValidationReport vr;
    ManufacturerPart mp;
    CustomerPart cp;
    MarkingPermanency mperm;
    XRay xray;
    HeatedSolvent hs;
    Decapsulation decap;
    Solderability sol;
    EPROM ep;
    SentryVI sv;
    XRF xrf;
    BasicElectrical be;
    ExtendedTesting et;
    Summary_new sum;
    MoistureBaking mb;
    List<insp_images> listExtendedImages = new List<insp_images>();


    ValidationReport vr2;



    SM_GCAT gcat;

    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreLoad(e);
        string strGCATID = Tools.Strings.SanitizeInput(Request.QueryString["id"]);
        if (!string.IsNullOrEmpty(strGCATID))
            GCATID = Convert.ToInt32(strGCATID);
        if (GCATID == 0)
            return;

        listExtendedImages = ql.LoadInspectionImagery(SM_Enums.InspectionType.gcat.ToString().ToUpper(), GCATID, "ExtendedImagery");
        iig.EnableControls = true;
        iig.ShowDescription = true;
        iig.EnableUpload = true;
        iig.LoadGallery(listExtendedImages, SM_Enums.InspectionType.gcat.ToString().ToUpper(), GCATID, "ExtendedImagery");
    }


    protected void Page_Load(object sender, EventArgs e)
    {


        try
        {
            SaveImagery(); //This way, every postback saves images, and I don't need to call this method from all over the place to support individual test saves.
            init();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            return;
        }

    }

    protected void init()
    {
        if (!check_security())
            throw new Exception("Not a valid GCAT ID for this user.");
        SetImageControlProperties();
        if (!Page.IsPostBack)
        {
            if (GCATID != 0)
            {

                //gcat = new SM_GCAT(GCATID);
                LoadAll();
            }
        }
    }

    private void SetImageControlProperties()
    {

        foreach (InspectionImageManagerControl c in GetImageManagerControlList())
        {
            c.InspectionType = "GCAT";
            c.InspectionID = GCATID;
            c.SectionID = ql.GetSectionIDfromClientID(c.ClientID);
            c.showDescription = true;
        }
    }

    private List<InspectionImageManagerControl> GetImageManagerControlList()
    {
        List<InspectionImageManagerControl> ret = new List<InspectionImageManagerControl>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, ret);
        return ret;
    }
    protected int GetGCATID()
    {
        int gcatid;
        //check querystring

        string strGCATId = Tools.Strings.SanitizeInput(Request.QueryString["id"]);
        if (string.IsNullOrEmpty(strGCATId))//NEw GCAT
        {
            gcatid = 0;
        }
        else
        {
            if (vr != null)
                gcatid = Convert.ToInt32(vr.ValidationReportID);
            else
                return Convert.ToInt32(strGCATId);
        }
        return gcatid;

    }
    protected void BindCustomerDDL()
    {
        ddlChooseCustomer.Items.Clear();
        ddlChooseCustomer.Items.Add(new ListItem("-Choose-", "0"));
        ddlChooseCustomer.AppendDataBoundItems = true;
        ddlChooseCustomer.DataSource = rzt.GetGCATCustomers(new DateTime(2012, 01, 01), DateTime.Today, mp.MPN).OrderByDescending(o => o.Value);
        ddlChooseCustomer.DataValueField = "Key";
        ddlChooseCustomer.DataTextField = "Value";
        ddlChooseCustomer.DataBind();

    }

    public void BindSalesOrderDDL()
    {
        string MPNStripped = Tools.Strings.FilterTrash(MPN);
        if (MPNStripped.Length > 6)
            MPNStripped = MPNStripped.Substring(0, 5);
        ddlChooseSale.Items.Clear();
        ddlChooseSale.Items.Add(new ListItem("-Choose-", "0"));
        ddlChooseSale.AppendDataBoundItems = true;
        var query = rdc.orddet_lines.Where(w => w.customer_uid == ddlChooseCustomer.SelectedValue
        && w.part_number_stripped.Contains(MPNStripped)
        && w.orderid_sales.Length > 0)
        .Select(o => new
        {
            o.orderid_sales,
            o.ordernumber_sales,
            o.orderdate_sales
        }).OrderByDescending(o => o.orderdate_sales).Distinct();

        ddlChooseSale.DataSource = query;

        ddlChooseSale.DataValueField = "orderid_sales";
        ddlChooseSale.DataTextField = "ordernumber_sales";
        ddlChooseSale.DataBind();

    }
    protected bool check_security()
    {
        string userName = System.Web.Security.Membership.GetUser().UserName;
        if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_internal"))
            return true;
        List<int> AllowedGCATIDs = new List<int>();
        AllowedGCATIDs = ciq.ValidationReports.Where(w => w.company_uid == Profile.CompanyID).Select(s => s.ValidationReportID).ToList();
        if (!AllowedGCATIDs.Contains(GCATID))
        {
            throw new Exception("Not Authorized.");
        }
        else
            return true;
    }

    protected void LoadAll()
    {
        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (!Page.IsPostBack)
        {

            if (vr == null)
            {
                tools.HandleResult("fail", "Invalid GCAT ID.");
                return;
            }
            LoadPageControls();
            if (vr.is_deleted != true)
            {
                GetManufacturerPart(vr.MPNID);
                ShowMainGCATForm();
                LoadHeader();
                LoadRequiredTests();
                LoadComponentIdentity();
                LoadVisualComponent();
                LoadVisualSurface();
                LoadMarkingPerm();
                LoadXray();
                LoadHeatedSolvents();
                LoadDecap();
                LoadSolderability();
                LoadEprom();
                LoadBasicElectrical();
                LoadSentry();
                LoadXRF();
                LoadExtendedTesting();
                LoadMoistureBaking();
                //LoadExtendedImagery();
                LoadSummary();
                MarkImported();
                if (!vr.is_imported)
                    MarkImported();
            }
            else
            {
                ShowStatusDiv("danger", "deleted");
                pnlManufacturerPartSearch.Visible = false;
            }
        }
    }

    private void LoadExtendedImagery()
    {
        //    iig.imageList = listExtendedImages;
        //    iig.sectionID = "ExtendedImagery";

        //LoadSectionImagery("ExtendedImagery");
    }

    //private void LoadExtendedImageryControls()
    //{
    //    //Get Count of all Images with SectionIDprefix 'ExtendedImagery'

    //    //Add ImagerManagerControls for each Image
    //    //Controls to add more en mass (i.e. button for add multiple)
    //    //On click new, save new insp_image, even if no blob data


    //    string section = "ExtendedImagery";
    //    if (listExtendedImages.Count == 0)
    //        return;

    //    //loops through the images and get the ones that match the constolPrefix
    //    //int iDIndex = 1;
    //    foreach (insp_images i in listExtendedImages)
    //    {

    //        InspectionImageManagerControl c = (InspectionImageManagerControl)Page.LoadControl("~/assets/controls/InspectionImageManagerControl.ascx");            
    //        c.ID = "im" + i.insp_section_id;
    //        c.ClientIDMode = ClientIDMode.Static;//Required for predicatble control ID, otherwise they come up as  ct100 etc.

    //        c.showDescription = true;
    //        c.InspectionType = SM_Enums.InspectionType.gcat.ToString();
    //        c.SectionID = i.insp_section_id;
    //        //c.Delete += new EventHandler(imc_deleteImage);
    //        //c.init(i);
    //        //This is necessary when adding USER controls dynimcally.  Add them to the page controls container 1st, then to whatever other container.

    //        pnlExtendedImagery.Controls.Add(c);
    //    }
    //    //Actually load the exttended imagery
    //    //LoadSectionImagery("ExtendedImagery");
    //}



    private void MarkImported() //Use this after initial load from old system to new.  When yes, check on image load so subsequently deleted (purposefully) images won't get re-imported
    {
        if (vr == null)
            return;
        vr.is_imported = true;
        ciq.SubmitChanges();
    }

    protected void LoadPageControls()
    {
        if (Roles.IsUserInRole("admin"))
        {
            showPageControls("admin");
            return;
        }
        if (Roles.IsUserInRole("sm_insp_user"))
        {
            showPageControls("sm_insp_user");
            return;
        }
        if (Roles.IsUserInRole("sm_internal_executive"))
        {
            showPageControls("sm_internal_executive");
            return;
        }
    }

    private void showPageControls(string role)
    {
        //save/delete/restore
        //complete
        //approve       
        //demo
        switch (role)
        {
            case "admin":
                {
                    showSaveButton(true);
                    showCompletionButton(true);
                    showApproveButton(true);
                    showDemoButton(true);
                    break;
                }
            case "sm_insp_user":
                {
                    showCompletionButton(true);
                    showSaveButton(true);
                    showDemoButton(true);
                    break;
                }
            case "sm_internal_executive":
                {
                    showApproveButton(true);
                    break;
                }
            default:
                {
                    showCompletionButton(false);
                    showSaveButton(false);
                    showApproveButton(false);
                    showDemoButton(false);
                    break;
                }
        }
    }

    private void showCompletionButton(bool show)
    {
        //Completion Button
        if (show)
        {
            lbComplete.Visible = show;
            if (vr != null) //i.e. New GCAT
            {
                if (vr.is_completed ?? false)
                    lbComplete.CssClass = "btn btn-smc btn-smc-success";
                else
                    lbComplete.CssClass = "btn btn-smc btn-smc-primary";
            }
            else
                lbComplete.Visible = false;

        }

    }
    private void showSaveButton(bool show)
    {
        //Save Restore
        if (show)
        {
            if (vr != null)
            {
                lbSaveAll.Visible = !vr.is_deleted;
                lbRestore.Visible = vr.is_deleted;
                lbDeleteAll.Visible = !vr.is_deleted;

            }
            else
            {
                lbSaveAll.Visible = true;
                lbRestore.Visible = false;
                lbDeleteAll.Visible = false;
            }

        }

    }
    private void showApproveButton(bool show)
    {
        //Approval Button
        if (show)
        {
            lbApprove.Visible = show; //i.e. New GCAT
            if (vr != null)
            {
                if (vr.is_approved ?? false)
                    lbApprove.CssClass = "btn btn-smc btn-smc-success";
                else
                    lbApprove.CssClass = "btn btn-smc btn-smc-primary";
            }
            else
            {
                lbApprove.Visible = false;
            }

        }


    }

    private void showDemoButton(bool show)
    {
        //Demo Button       
        if (show)
            if (vr != null) //i.e. New GCAT
                lbSetDemoGCAT.Visible = vr.is_demo ?? false == true ? false : true; // if demo is null, then false, and if demo is false, then visible is true
                                                                                    //Don't create demos of demos
            else
                lbSetDemoGCAT.Visible = false;

    }

    protected void LoadHeader()
    {
        //Date & ID
        lblReportID.Text = "ID: " + vr.ValidationReportID.ToString();
        lblDate.Text = "Date: " + vr.ReportDate.Value.ToString("d");

        pnlHeaderData.Visible = true;
        //BindCustomerDDL(); //This is handled in ShowMainGCATForm
        ddlChooseCustomer.SelectedValue = vr.company_uid;
        BindSalesOrderDDL();
        ddlChooseSale.SelectedValue = vr.orderid_sales ?? DetermineSalesOrderID() ?? "0";
        BindLineItemDDL();
        ddlLineItem.SelectedValue = vr.orddet_line_uid ?? DetermineOrddetID() ?? "0";

        //Sale Data
        if (!string.IsNullOrEmpty(vr.CustomerPO))
            lblHeaderCustomerPO.Text = vr.CustomerPO;
        else
        {
            sale = rdc.ordhed_sales.Where(w => w.unique_id == vr.orderid_sales).SingleOrDefault();
            if (sale != null)
                lblHeaderCustomerPO.Text = sale.orderreference;

        }


        if (vr.AccountManagerID != 0)
        {//NO longer saving AccountManagerID.  If it exists (i.e. old GCAT) show that
            lblHeaderAccountManager.Text = null;
            lblHeaderAccountManager.Text = ciq.AccountManagers.Where(w => w.AccountManagerID == vr.AccountManagerID).Select(s => s.FullName).SingleOrDefault();
        }

        if (string.IsNullOrEmpty(lblHeaderAccountManager.Text))
        {//Else show the Rz Integrated Account Manager            
            lblHeaderAccountManager.Text = vr.agent_name;
        }

        //Line Data
        line = rdc.orddet_lines.Where(w => w.unique_id == vr.orddet_line_uid).SingleOrDefault();
        if (vr.CPN != null)
            lblCustomerPart.Text = vr.CPN;
        else
        {
            if (line != null)
                lblCustomerPart.Text = line.internal_customer;
        }
        //QTY
        if ((vr.Quantity1 ?? 0) > 0)
            txtQuantity1.Text = vr.Quantity1.ToString();
        else if (line != null)
            txtQuantity1.Text = line.quantity.ToString();
        //else
        //    txtQuantity1.Text = "See Summary Notes";

        //Summary Notes
        txtHeaderSummaryNotes.Text = vr.summaryNotes;


        //LoadImage(imgHeader1, vr.Image_Top);
        //LoadImage(imgHeader2, vr.Image_Bottom);
        LoadSectionImagery("Header");
        cp = ciq.CustomerParts.Where(s => s.MPNID == vr.MPNID).SingleOrDefault();
    }

    //Initally we were only saving the sales order number, bad design.  Need this method to update this, and handle any collissions.
    protected string DetermineSalesOrderID()
    {
        string ret = null;
        List<string> list = rdc.ordhed_sales.Where(w => w.ordernumber == vr.SalesOrderNumber).Select(s => s.unique_id).ToList();
        if (list.Count == 1)
        {
            ret = list[0];
            sale = rdc.ordhed_sales.Where(w => w.unique_id == ret).SingleOrDefault();
            vr.orderid_sales = ret;
            ciq.SubmitChanges();
        }
        return ret;
    }
    protected string DetermineOrddetID()
    {
        string ret = null;
        List<string> list = rdc.orddet_lines.Where(w => w.orderid_sales == vr.orderid_sales && w.fullpartnumber == vr.MPN && w.manufacturer == vr.Manufacturer).Select(s => s.unique_id).ToList();
        if (list.Count == 1)
        {
            ret = list[0];
            line = rdc.orddet_lines.Where(w => w.unique_id == ret).SingleOrDefault();
            vr.orddet_line_uid = ret;
            ciq.SubmitChanges();
        }
        return ret;
    }


    protected void LoadRequiredTests()
    {


        if (vr != null)
        {
            GetAssociatedTests();
            cbxTestTypeDecap.Checked = decap != null;
            cbxTestTypeHeatedSolvents.Checked = hs != null;
            cbxTestTypeMarkingPerm.Checked = mperm != null;
            cbxTestTypeBasicElectrical.Checked = be != null;
            cbxTestTypeSentryVI.Checked = sv != null;
            cbxTestTypeSolderability.Checked = sol != null;
            cbxTestTypeXRay.Checked = xray != null;
            cbxTestTypeXRF.Checked = xrf != null;
            cbxTestTypeEprom.Checked = ep != null;
            cbxTestTypeExtendedTesting.Checked = et != null;
            cbxTestTypeMoistureBaking.Checked = mb != null;

            //Extended Imagery - This list gets populated onLoad so the controls can be available for postback events.

            cbxTestTypeExtendedImagery.Checked = listExtendedImages.Count > 0;

            //CustomerCategory
            if (!string.IsNullOrEmpty(vr.CustomerTypeA))
            {
                GetDDlValue(vr.CustomerTypeA, ddlRequiredCatTypeA);
            }
            else
            {
                ddlRequiredCatTypeA.SelectedValue = "0";
            }
            if (!string.IsNullOrEmpty(vr.CustomerTypeB))
            {
                GetDDlValue(vr.CustomerTypeB, ddlRequiredCatTypeB);
            }
            else
            {
                ddlRequiredCatTypeB.SelectedValue = "0";

            }
            if (!string.IsNullOrEmpty(vr.CustomerTypeC))
            {
                GetDDlValue(vr.CustomerTypeC, ddlRequiredCatTypeC);
            }
            else
            {
                ddlRequiredCatTypeC.SelectedValue = "0";

            }
            //Standardized Tests  
            cbxIsCCAP.Checked = vr.is_CCAP ?? false;
            cbxIsAS6081.Checked = vr.is_AS6081 ?? false;

            txtTest_TypeJ.Text = vr.Test_TypeJ;
            //Customer Requirement
            txtCustomer_RequirementA.Text = vr.Customer_RequirementA ?? "" + ", " + vr.Customer_RequirementB ?? "" + vr.Customer_RequirementC + ", " + vr.Customer_RequirementC ?? "";
        }

    }





    protected void GetDDlValue(string dbValue, DropDownList ddl)
    {

        if (ddl.Items.FindByText(dbValue) != null)
            ddl.SelectedValue = dbValue;
        else
        {
            ddl.Items.Add(new ListItem(dbValue, dbValue));
            ddl.SelectedValue = dbValue;
        }
        ddl.Visible = true;
    }
    protected void GetCBXValue(string dbValue, CheckBox cbx)
    {
        switch (dbValue)
        {
            case "De-Capsulation":
                cbx.Checked = true;
                break;
            case "Dimensional":
                cbx.Checked = true;
                break;
            case "IDEA-1010 Detailed Visual":
                cbx.Checked = true;
                break;
            case "Heated Solvents":
                cbx.Checked = true;
                break;
            case "Marking Permanency":
                cbx.Checked = true;
                break;
            case "Sentry V-I Signature":
                cbx.Checked = true;
                break;
            case "Solderability":
                cbx.Checked = true;
                break;
            case "X-Ray":
                cbx.Checked = true;
                break;
            case "XRF Analysis":
                cbx.Checked = true;
                break;
            default:
                cbx.Checked = false;
                break;

        }
    }


    protected void LoadComponentIdentity()
    {

        //mp = ciq.ManufacturerParts.Where(w => w.MPNID == vr.MPNID).FirstOrDefault();
        if (mp != null)
        {
            //Search DataSheet


            //Rohs Compliant
            ddlComponentIdentityRohs_compliant.SelectedValue = mp.RoHS_Compliant ?? "0";

            //Condition Checkboxes
            if (vr.New == true)
                cblComponentIdentityCondition.Items.FindByText("New").Selected = true;
            if (vr.Used == true)
                cblComponentIdentityCondition.Items.FindByText("Used").Selected = true;
            if (vr.Refurbished == true)
                cblComponentIdentityCondition.Items.FindByText("Refurbished").Selected = true;

            //Memory Device
            ddlComponentIdentityMemoryDevice.SelectedValue = mp.MemoryDevice ?? "0";

            //Component Type
            if (mp.SurfaceMount == true)
                cblComponentIdentityType.Items.FindByValue("sm").Selected = true;
            if (mp.ThroughHole == true)
                cblComponentIdentityType.Items.FindByValue("th").Selected = true;
            if (mp.BGA == true)
                cblComponentIdentityType.Items.FindByValue("bga").Selected = true;

            //MSD / MSL
            //ddlComponentIdentityMSD.SelectedValue = mp.MSD_MSL;
            //if (mp.MSD_MSL == "Y")
            //{
            //    txtComponentIdentityMoistureLevel.Enabled = true;
            ddlComponentIdentityMoistureLevel.SelectedValue = mp.MoistureLevel ?? "0";
            //}
            //else
            //{
            //    txtComponentIdentityMoistureLevel.Enabled = false;
            //}


            //Traceability
            if (vr.Traceability != null)
            {   //1 = Y, 2 = N, 3 = N/A  
                ddlComponentIdentityTraceability.SelectedValue = vr.Traceability.ToString();
            }
            else
                ddlComponentIdentityTraceability.SelectedValue = "0";

            //Characteristics
            txtComponentIdentityPackageType.Text = mp.PackageType;
            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "handleVisualComponentPackageConnectionType", "handleVisualComponentPackageConnectionType()", false);

            txtComponentIdentityLeadPinBGAPitch.Text = mp.Lead_PinPitch;
            txtComponentIdentityLeadPinBGACount.Text = mp.Lead_PinCount;
            txtComponentIdentityLength.Text = mp.Length;
            txtComponentIdentityWidth.Text = mp.Width;
            txtComponentIdentityThickness.Text = mp.Thickness;

        }


    }
    protected void SetDateCodesAndQTY(ManufacturerPart manP)
    {//Consolidate existing database fields by combining all fields into the unlimited field:
        string DateCodes = txtComponentIdentityDateCodes.Text;
        vr.DateCodeUnltd = DateCodes;
        //Clear out existing individuals:
        vr.DateCode1 = null;
        vr.DateCode2 = null;
        vr.DateCode3 = null;
        vr.DateCode4 = null;
        vr.DateCode5 = null;
        vr.DateCode6 = null;



    }
    protected string GetDateCodesAndQTY()
    {
        StringBuilder ret = new StringBuilder();
        if (vr.DateCode1 != null)
            ret.Append(vr.DateCode1 + "(QTY:" + vr.Quantity1 + "),");
        if (vr.DateCode2 != null)
            ret.Append(vr.DateCode2 + "( QTY:" + vr.Quantity2 + "),");
        if (vr.DateCode3 != null)
            ret.Append(vr.DateCode3 + "( QTY:" + vr.Quantity3 + "),");
        if (vr.DateCode4 != null)
            ret.Append(vr.DateCode4 + "( QTY:" + vr.Quantity4 + "),");
        if (vr.DateCode5 != null)
            ret.Append(vr.DateCode5 + "( QTY:" + vr.Quantity5 + "),");
        if (vr.DateCode6 != null)
            ret.Append(vr.DateCode6 + "( QTY:" + vr.Quantity6 + ")");
        //Additional (Unlimited) Date Codes
        if (!string.IsNullOrEmpty(vr.DateCodeUnltd))
        {
            if (!string.IsNullOrEmpty(ret.ToString()))//Were there previous date codes?  If so, these are "Additional"
                ret.Append(" Additional Date Codes: ");
            else
                //ret.Append("Date Codes: ");
                ret.Append(vr.DateCodeUnltd);
        }
        if (!string.IsNullOrEmpty(ret.ToString()))
            return ret.ToString();
        else
            return null;
    }



    protected void LoadVisualComponent()
    {
        if (vr != null)
        {
            txtVisualComponentSamplyQTY.Text = vr.VisualComponentSampleQty.ToString();
            //External Visual
            txtVisualComponentWidthActual.Text = vr.Width_Actual.ToString() ?? "N/A";
            txtVisualComponentWidthUnit.Text = vr.UnitWidth;
            txtVisualComponentPassWidthComment.Text = vr.CommentsWidth;

            txtVisualComponentLengthActual.Text = vr.Length_Actual.ToString() ?? "N/A";
            txtVisualComponentLengthUnit.Text = vr.UnitLength;
            txtVisualComponentPassLengthComment.Text = vr.CommentsLength;

            txtVisualComponentThicknessActual.Text = vr.Thickness_Actual.ToString() ?? "N/A";
            txtVisualComponentThicknessUnit.Text = vr.UnitThickness;
            txtVisualComponentPassThicknessComment.Text = vr.CommentsThickness;

            switch (vr.Pass_W)
            {
                case true:
                    {
                        ddlVisualComponentPassWidth.SelectedValue = "1";
                        break;
                    }
                case false:
                    {
                        ddlVisualComponentPassWidth.SelectedValue = "0";
                        break;
                    }
                default:
                    {
                        ddlVisualComponentPassWidth.SelectedValue = "-1";
                        break;
                    }
            }
            switch (vr.Pass_L)
            {
                case true:
                    {
                        ddlVisualComponentPassLength.SelectedValue = "1";
                        break;
                    }
                case false:
                    {
                        ddlVisualComponentPassLength.SelectedValue = "0";
                        break;
                    }
                default:
                    {
                        ddlVisualComponentPassLength.SelectedValue = "-1";
                        break;
                    }
            }
            switch (vr.Pass_T)
            {
                case true:
                    {
                        ddlVisualComponentPassThickness.SelectedValue = "1";
                        break;
                    }
                case false:
                    {
                        ddlVisualComponentPassThickness.SelectedValue = "0";
                        break;
                    }
                default:
                    {
                        ddlVisualComponentPassThickness.SelectedValue = "-1";
                        break;
                    }
            }

            //Package Connection Type
            LoadPackageConnectionType();

            //Package Type            
            ddlVisualComponentPackageConnectionType.SelectedValue = vr.ComponentType;

            //Lead / Pin Castellation
            ddlVisualComponentCastellationResult.SelectedValue = vr.Lead_Count;
            txtVisualComponentCastellationComment.Text = vr.CommentLeadCount;


            //DateCodes
            txtComponentIdentityDateCodes.Text = GetDateCodesAndQTY() ?? "";

            //Imagery
            //LoadImage(imgVisualComponent1, vr.VCA_Image1);
            //LoadImage(imgVisualComponent2, vr.VCA_Image2);
            //LoadImage(imgVisualComponent3, vr.VCA_Image3);
            LoadSectionImagery("VisualComponent", false);

        }




    }

    protected string ConvertYNtoPassFail(string s)
    {
        switch (s.ToLower())
        {
            case "y":
                {
                    return "Pass";
                }
            case "n":
                {
                    return "Fail";
                }
            default:
                return "N/A";
        }
    }

    protected void LoadPackageConnectionType()
    {
        ddlVisualComponentPackageConnectionType.SelectedValue = vr.ComponentType;


        //ddlVisualComponentCastellationResult.SelectedValue = vr.Lead_Count ?? "0";
        txtVisualComponentCastellationComment.Text = vr.CommentLeadCount;

        ddlVisualComponentOxidationResult.Text = vr.Oxidation_Discoloration ?? "0";
        txtVisualComponentOxidationComment.Text = vr.CommentO2Discoloration;

        ddlVisualComponentLeadPadConditionResult.Text = vr.Lead_PadCondition;
        txtVisualComponentLeadPadConditionComment.Text = vr.CommentLead_Pad;

        ddlVisualComponentExposedBaseMetalResult.SelectedValue = vr.SignsExposedBaseMetal ?? "0";
        txtVisualComponentExposedBaseMetalComment.Text = vr.CommentExpBaseMetal;

        ddlVisualComponentCoplanarityResult.Text = vr.LeadCoplanarity;
        txtVisualComponentCoplanarityComment.Text = vr.CommentLeadCoplanarity;

        ddlVisualComponentSolderResult.SelectedValue = vr.SolderBalls_Spheres ?? "0";
        txtVisualComponentSolderComment.Text = vr.CommentSolderBalls;

        ddlVisualComponentConcentricityResult.SelectedValue = vr.Uniform_Concentricity ?? "0";
        txtVisualComponentConcentricityComment.Text = vr.CommentUniformity;

        ddlVisualComponentDamagedSpheresResult.SelectedValue = vr.SolderBalls_Spheres ?? "0";
        txtVisualComponentDamagedSpheresComment.Text = vr.CommentDamagedSpheres;

    }


    protected void LoadVisualSurface()
    {
        if (vr != null)
        {
            ddlVisualSurfaceLogo.SelectedValue = vr.LogosPartnumbersPass.ToString() ?? "3";
            txtVisualSurfaceLogo.Text = vr.LogosPartnumbersComment;
            txtVisualSurfaceSampleQTY.Text = vr.VisualSurfaceSampleQty.ToString();
            ddlVisualSurfaceTop.SelectedValue = vr.Top_Pass.ToString() ?? "3";
            txtVisualSurfaceTopComment.Text = vr.Comments_Top;
            ddlVisualSurfaceBot.SelectedValue = vr.Bottom_Pass.ToString() ?? "3";
            txtVisualSurfaceBotComment.Text = vr.Comments_Bottom;
            //Imagery
            //LoadImage(imgVisualSurface1, vr.VSA_Image1);
            //LoadImage(imgVisualSurface2, vr.VSA_Image2);
            //LoadImage(imgVisualSurface3, vr.VSA_Image3);
            LoadSectionImagery("VisualSurface");
        }
    }

    protected void LoadMarkingPerm()
    {
        //mperm = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (mperm != null)
        {
            txtMarkingPermSampleQTY.Text = mperm.MarkingSample_Qty.ToString();

            ddlMarkingPermAcetone.SelectedValue = mperm.AcetoneTestPass.ToString() ?? "0";
            txtMarkingPermAcetoneComment.Text = mperm.Comments_Acetone;

            ddlMarkingPermAlcohol.SelectedValue = mperm.Alcohol_MineralSpiritsTestPass.ToString() ?? "0";
            txtMarkingPermAlcoholComment.Text = mperm.Comments_Alcohol;

            ddlMarkingPermScrape.SelectedValue = mperm.ScrapeTestPass.ToString() ?? "0";
            txtMarkingPermScrapeComment.Text = mperm.Comments_ScrapeTest;

            ddlMarkingPermSecondary.Text = mperm.SecondaryCoatingTestPass.ToString() ?? "0";
            txtMarkingPermSecondaryComment.Text = mperm.Comments_SecondaryCoating;

            ddlMarkingPermPartMarking.Text = mperm.PartNumber_MarkingTestPass.ToString() ?? "0";
            txtMarkingPermPartMarkingComment.Text = mperm.Comments_PartNumber;

            ddlMarkingPermCase.Text = mperm.CaseMarking_LogoTestPass.ToString() ?? "0";
            txtMarkingPermCaseComment.Text = mperm.Comments_CaseMarking;

            //Imagery
            //LoadImage(imgMarkingPerm1, mperm.Marking_Image1);
            //LoadImage(imgMarkingPerm2, mperm.Marking_Image2);
            //LoadImage(imgMarkingPerm3, mperm.Marking_Image3);
            LoadSectionImagery("MarkingPerm");

        }
    }

    protected void LoadXray()
    {
        //xray = ciq.XRays.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (xray != null)
        {
            txtXraySampleQTY.Text = xray.XRaySampleQty.ToString();

            ddlXrayDiePresent.SelectedValue = xray.DiePresentPass.ToString() ?? "0";
            txtXrayDiePresentComment.Text = xray.DiePresentComments;

            ddlXrayBondWires.SelectedValue = xray.BondWiresPresentPass.ToString() ?? "0";
            txtXrayBondWiresComment.Text = xray.BondWiresPresentComment;

            ddlXrayDamagedBond.SelectedValue = xray.Damaged_MissingWiresPass.ToString() ?? "0";
            txtXrayDamagedBondComment.Text = xray.Damaged_MissingWiresComment;

            ddlXrayDieLead.SelectedValue = xray.DieLeadUniformityPass.ToString() ?? "0";
            txtXrayDieLeadComment.Text = xray.DieLeadUniformityComment;

            ddlXrayDieFrame.SelectedValue = xray.DieFRameSize_LocationPass.ToString() ?? "0";
            txtXrayDieFrameComment.Text = xray.DieFrameSize_LocationComment;

            //Imagery
            //LoadImage(imgXray1, xray.XRay_Image1);
            //LoadImage(imgXray2, xray.XRay_Image2);
            //LoadImage(imgXray3, xray.XRay_Image3);
            LoadSectionImagery("Xray");

        }
    }

    protected void LoadHeatedSolvents()
    {
        //hs = ciq.HeatedSolvents.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (hs != null)
        {
            txtHeatedSolventsSampleQTY.Text = hs.HeatedSolventSampleQty.ToString();

            ddlHeatedMeth.SelectedValue = hs.Methyl_PyrrolidinonePass.ToString() ?? "0";
            txtHeatedMethComment.Text = hs.Methyl_PyrrolidinoneComments;

            ddlHeatedDyn.SelectedValue = hs.DynasolvePass.ToString() ?? "0";
            txtHeatedDynComment.Text = hs.DynasolveComments;

            ddlHeatedSecondary.SelectedValue = hs.SecondaryCoatingPass.ToString() ?? "0";
            txtHeatedSecondaryComment.Text = hs.SecondaryCoatingComments;

            //Imagery
            //LoadImage(imgHeated1, hs.Heated_SolventsImage1);
            //LoadImage(imgHeated2, hs.Heated_SolventsImage2);
            LoadSectionImagery("Heated");
        }
    }


    protected void LoadDecap()
    {
        //decap = ciq.Decapsulations.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (decap != null)
        {
            txtDecapSampleQTY.Text = decap.DeCap_SampleQty.ToString();

            ddlDecapMfgLogo.SelectedValue = decap.MFGLogoPass.ToString() ?? "0";
            txtDecapMfgLogoComment.Text = decap.MFGLogoComments;

            ddlDecapCasing.SelectedValue = decap.LogoMatchPass.ToString() ?? "0";
            txtDecapCasingComment.Text = decap.LogoMatchComments;

            ddlDcapPartDie.SelectedValue = decap.DiePNMatchPass.ToString() ?? "0";
            txtDcapPartDieComment.Text = decap.DiePNMatchComments;

            ddlDecapDieBond.SelectedValue = decap.DieBondWiresPass.ToString() ?? "0";
            txtDecapDieBondComment.Text = decap.DieBondWiresComments;

            ////Imagery            
            LoadSectionImagery("Decap");

        }
    }

    protected void LoadSolderability()
    {
        //sol = ciq.Solderabilities.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (sol != null)
        {
            txtSolderSampleQTY.Text = sol.SolderabilitySampleQty.ToString();

            cbxSolderSPRequired.Checked = sol.SolderabilityServiceProviderRequired ?? false;
            txtSolderSPRequired.Text = sol.SolderabilitySPRequiredComments;

            ddlSolderPackageMountType.SelectedValue = sol.PackageMountTypeSolderability;

            ddlSolderRohs.SelectedValue = sol.RoHS.ToString() ?? "0";
            txtSolderRohsComment.Text = sol.RoHSComments;

            ddlSolderWetting.SelectedValue = sol.SolderWettingAcceptable.ToString() ?? "0";
            txtSolderWettingComment.Text = sol.SolderWettingComments;

            ddlSolderVoid.SelectedValue = sol.VoidFree.ToString() ?? "0";
            txtSolderVoidComment.Text = sol.VoidFreeComments;

            ddlSolderCont.SelectedValue = sol.ContaminantFree.ToString() ?? "0";
            txtSolderContComment.Text = sol.ContaminantFreeComments;

            //Imagery
            //LoadImage(imgSolder1, sol.SolderabilityImage1);
            //LoadImage(imgSolder2, sol.SolderabilityImage2);
            //LoadImage(imgSolder3, sol.SolderabilityImage3);
            LoadSectionImagery("Solder");
        }
    }

    protected void LoadEprom()
    {
        // ep = ciq.EPROMs.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (ep != null)
        {
            txtEpromSampleQTY.Text = ep.EPROMSampleQty.ToString();

            ddlEpromPackageMount.SelectedValue = ep.PackageMountTypeEPROM;
            cbxEpromSPRequired.Checked = ep.EPROMServiceProviderRequired ?? false;
            txtEpromSPRequired.Text = ep.EPROMSPRequiredComments;

            ddlEpromChecksum.SelectedValue = ep.ChecksumAnalysis.ToString() ?? "0";
            txtEpromChecksumComment.Text = ep.ChecksumComment;

            switch (ep.ProgrammingRequired)
            {
                case 1:
                    {
                        cbxEpromProgrammingRequired.Checked = true;//Pass
                        break;
                    }
                case 2:
                    {
                        cbxEpromProgrammingRequired.Checked = false;//Fail
                        break;
                    }
                default:
                    {
                        cbxEpromProgrammingRequired.Checked = false;
                        break;
                    }
            }

            //ddlEpromProgramming.SelectedValue = ep.ProgrammingRequired.ToString() ?? "0";
            //txtEpromProgrammingComment.Text = ep.ProgrammingRequiredComment;

            ddlEpromVerify.SelectedValue = ep.VerifyProgramLoad.ToString() ?? "0";
            txtEpromVerifyComment.Text = ep.ProgramLoadComment;


        }
    }
    private void LoadBasicElectrical()
    {
        //be = ciq.BasicElectricals.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (be != null)
        {
            txtBasicElectricalSampleQTY.Text = be.SampleQty.ToString();
            txtBasicElectricalTest1.Text = be.Test1;
            txtBasicElectricalCondition1.Text = be.Cond1;
            //Overall Result
            ddlBasicElectricalResult.SelectedValue = be.Actual1;
            txtBasicElectricalNotes.Text = be.notes;

            //Imagery
            // LoadImage(imgBasicElectrical1, be.Image);
            LoadSectionImagery("BasicElectrical");
        }
    }

    protected void LoadSentry()
    {
        //sv = ciq.SentryVIs.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (sv != null)
        {
            txtSentrySampleQTY.Text = sv.SentryVISampleQty.ToString();
            cbxSentrySPRequired.Checked = sv.ServiceProviderRequiredSentryVI ?? false;
            txtSentrySPRequired.Text = sv.SentryVISPRequiredComments;

            ddlSentryPackageMountType.SelectedValue = sv.PackageMountTypeSentryVI;


            ddlSentrySignaturesMatch.SelectedValue = sv.SignaturesMatch.ToString() ?? "0";
            txtSentrySignaturesMatchComment.Text = sv.SignatureMatchComments;

            ddlSentryOpenShorts.SelectedValue = sv.Open_Shorts_Detection.ToString() ?? "0";
            txtSentryOpenShortsComment.Text = sv.OpenShortsComments;

            ddlSentryPassedAllSignatureAnalysis.SelectedValue = sv.SignatureAnalysisPass.ToString() ?? "0";
            txtSentryPassedAllSignatureAnalysisComment.Text = sv.SignatureAnalysisComments;

            //ddlSentrySignatureReportRequired.SelectedValue = sv.SignatureReportRequired.ToString() ?? "0";
            //txtSentrySignatureReportRequiredComment.Text = sv.SignatureReportComments;

            ddlSentryOverallResult.SelectedValue = sv.SentryOverAllResult.ToString() ?? "0";

            //Imagery
            //LoadImage(imgSentry1, sv.SentryICResults);
            LoadSectionImagery("Sentry");
        }
    }

    protected void LoadXRF()
    {
        //xrf = ciq.XRFs.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (xrf != null)
        {
            txtXRFSampleQTY.Text = xrf.XRFSampleQty.ToString();

            ddlXRFPackageType.SelectedValue = xrf.PackageMountTypeXRF ?? "0";

            cbxXRFSPRequired.Checked = xrf.XRFServiceProviderRequired ?? false;
            txtXRFSPRequired.Text = xrf.XRFSPRequiredComments;

            ddlXRFLeadMaterialsMatch.SelectedValue = xrf.LeadMaterialsMatch.ToString() ?? "0";
            txtXRFLeadMaterialsMatchComment.Text = xrf.LeadMaterialsMatchComments;

            ddlXRFForeignMaterials.SelectedValue = xrf.ForeignMaterials.ToString() ?? "0";
            txtXRFForeignMaterialsComment.Text = xrf.ForeignMaterialsComments;

            LoadSectionImagery("XRF", false);
        }
    }

    protected void LoadExtendedTesting()
    {
        et = ciq.ExtendedTestings.Where(w => w.ValidationReportID == GCATID).FirstOrDefault();
        if (et != null)
        {
            txtExtendedTestingSampleQTY.Text = et.SampleQty.ToString();
            //Category
            ddlExtCategory.SelectedValue = et.ExtCategory;
            txtExtCategoryOther.Text = et.ExtCategoryOther;

            //Result
            ddlExtTestingResult.SelectedValue = et.Actual1;

            //txtExtTest1.Text = et.Test1;
            txtExtCondition1.Text = et.Cond1;
            //txtExtResult1.Text = et.Res1;
            //txtExtActual1.Text = et.Actual1;
            txtExtNotes.Text = et.notes;

            //foreach(InspectionImageManagerControl c in tools.GetControlList())
            //Imagery
            //LoadImage(imgExt1, et.ExtImage);
            LoadSectionImagery("Ext");
        }
    }

    protected void LoadMoistureBaking()
    {
        mb = ciq.MoistureBakings.Where(w => w.validationreportid == GCATID).FirstOrDefault();
        if (mb != null)
        {
            txtMoistureBakingSampleQTY.Text = mb.total_quantity.ToString();

            //txtMoistureBakingStrategicSalesWorkOrderNumber.Text = mb.strategic_sales_work_order_number;
            txtMoistureBakingProcess.Text = mb.process;
            txtMoistureBakingDuration.Text = mb.bake_duration;
            txtMoistureBakingMSL.Text = mb.msl;
            txtMoistureBakingTemp.Text = mb.bake_temperature;
            if (!string.IsNullOrEmpty(mb.result))
                ddlMoistureBakingResult.SelectedValue = mb.result;
            if (!string.IsNullOrEmpty(mb.result_comment))
                txtMoistureBakingComment.Text = mb.result_comment;


            LoadSectionImagery("MoistureBaking");
        }
    }


    private void LoadSectionImagery(string section, bool showDescription = true)
    {
        //Get a list of the Images for this section.
        List<insp_images> images = ql.LoadInspectionImagery("GCAT", GCATID, section);
        if (images.Count == 0)
            return;
        //Identify the control names
        string controlIDPrefix = ql.GetInspectionImageryControlPrefix("GCAT", section);

        if (string.IsNullOrEmpty(controlIDPrefix))
            return;

        //loops through the images and get the ones that match the constolPrefix
        //int iDIndex = 1;
        foreach (insp_images i in images)
        {
            string controlID = controlIDPrefix + i.insp_section_id;

            InspectionImageManagerControl c = (InspectionImageManagerControl)Page.Master.FindControl("MainContent").FindControl(controlID);

            if (c == null)
                return;
            c.showDescription = showDescription;

            c.init(i);
        }


    }

    protected void LoadSummary()
    {
        //sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (sum == null)
        {
            ImportSummaryAnswers();
            SaveSummary();
        }
        //External Visual
        //lblSumExternalVisualLogosParts.Text = sum.ExtVisLogosPartsResult;
        lblSumExternalVisualLogosParts.Text = sum.ExtVisLogosPartsResult;
        txtSumExternalVisualLogosPartsComment.Text = sum.ExtVisLogosPartsComment;
        lblSumExternalVisualDim.Text = sum.ExtVisDimensionalResult;
        txtSumExternalVisualDimComment.Text = sum.ExtVisDimensionalComment;
        lblSumExternalVisualLeadPkg.Text = sum.ExtVisLeadPkgResult;
        txtSumExternalVisualLeadPkgComment.Text = sum.ExtVisLeadPkgComment;
        lblSumExternalVisualAlteration.Text = sum.ExtVisSurfaceAltResult;
        txtSumExternalVisualAlterationComment.Text = sum.ExtVisSurfaceAltComment;
        //MarkingPermenancy
        lblSumMarkingAcetone.Text = sum.MarkingAcetoneResult;
        txtSumMarkingAcetoneComment.Text = sum.MarkingAcetoneComment;
        lblSumMarkingScrape.Text = sum.MarkingScrapeResult;
        txtSumMarkingScrapeComment.Text = sum.MarkingScrapeComment;
        lblSumMarkingAlteration.Text = sum.MarkingAlterationResult;
        txtSumMarkingAlterationComment.Text = sum.MarkingAlterationComment;
        //Xray
        lblSumXrayUniform.Text = sum.XrayDieWireResult;
        txtSumXrayUniformComment.Text = sum.XrayDieWireComment;
        //EPROM
        lblSumEPROMChecksum.Text = sum.EPROMChecksumResult;
        txtSumEPROMChecksumComment.Text = sum.EPROMCheckSumComment;
        lblSumEPROMValidation.Text = sum.EPROMProgramValidationResult;
        txtSumEPROMValidationComment.Text = sum.EPROMProgramValidationComment;

        //XRF
        lblSumXRFLeadsMatch.Text = sum.XRFLeadMatsResult;
        txtSumXRFLeadsMatchComment.Text = sum.XRFLeadMatsComment;
        lblSumXRFForeignMat.Text = sum.XRFForeignResult;
        txtSumXRFForeignMatComment.Text = sum.XRFForeignComment;
        //Solderability
        lblSumSolderPassSolderTesting.Text = sum.SolPassedResult;
        txtSumSolderPassSolderTestingComment.Text = sum.SolPassedComment;
        lblSumSolderWetting.Text = sum.SolWettingResult;
        txtSumSolderWettingComment.Text = sum.SolWettingComment;
        //Basic Electrical
        lblSumBasicElectrical.Text = sum.BasicElectricalResult;
        txtSumBasicElectricalComment.Text = sum.BasicElectricalComment;

        //Sentry VI
        lblSumElectricalVISignature.Text = sum.SentryVIResult;
        txtSumElectricalVISignatureComment.Text = sum.SentryVIComment;

        //Extended Testing
        lblSumExtendedTesting.Text = sum.ExtendedTestingResult;
        txtSumExtendedTestingComment.Text = sum.ExtendedTestingComment;

        //Heated Solvents
        lblSumHeatedSolventsMeth.Text = sum.HeatedMethResult;
        txtSumHeatedSolventsMethComment.Text = sum.HeatedMethComment;
        lblSumHeatedSolventsDyna.Text = sum.HeatedDynaResult;
        txtSumHeatedSolventsDynaComment.Text = sum.HeatedDynaComment;
        lblSumHeatedSolventsSecondary.Text = sum.HeatedSecondaryResult;
        txtSumHeatedSolventsSecondaryComment.Text = sum.HeatedSecondaryComment;
        //Decap
        lblSumDecapWireBondIntegrity.Text = sum.DecapWireBondResult;
        txtSumDecapWireBondIntegrityComment.Text = sum.DecapWireBondComment;
        lblSumDecapDieMarking.Text = sum.DecapDieMarkingResult;
        txtSumDecapDieMarkingComment.Text = sum.DecapDieMarkingComment;

        //MoistureBaking
        lblSumMoistureBakingResult.Text = sum.MoistureBakingResult;
        txtSumMoistureBakingComment.Text = sum.MoistureBakingComment;


    }

    private void ImportSummaryAnswers()
    {
        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (vr != null)
        {

            lblSumExternalVisualLogosParts.Text = GetSummaryResultInt16(lblSumExternalVisualLogosParts);
            lblSumExternalVisualDim.Text = GetSummaryResultInt16(lblSumExternalVisualDim);
            lblSumExternalVisualLeadPkg.Text = GetSummaryResultInt16(lblSumExternalVisualLeadPkg);
            lblSumExternalVisualAlteration.Text = GetSummaryResultInt16(lblSumExternalVisualAlteration);

            //Marking Perm
            if (mperm != null)
            {
                lblSumMarkingAcetone.Text = GetSummaryResultInt16(lblSumMarkingAcetone);
                lblSumMarkingScrape.Text = GetSummaryResultInt16(lblSumMarkingScrape);
                lblSumMarkingAlteration.Text = GetSummaryResultInt16(lblSumMarkingAlteration);
            }

            //Xray
            if (xray != null)
            {
                lblSumXrayUniform.Text = GetSummaryResultInt16(lblSumXrayUniform);
            }

            //Heated Solvents
            if (hs != null)
            {
                lblSumHeatedSolventsMeth.Text = GetSummaryResultInt16(lblSumHeatedSolventsMeth);
                lblSumHeatedSolventsDyna.Text = GetSummaryResultInt16(lblSumHeatedSolventsDyna);
                lblSumHeatedSolventsSecondary.Text = GetSummaryResultInt16(lblSumHeatedSolventsSecondary);
            }

            //Decap
            if (decap != null)
            {
                lblSumDecapWireBondIntegrity.Text = GetSummaryResultInt16(lblSumDecapWireBondIntegrity);
                lblSumDecapDieMarking.Text = GetSummaryResultInt16(lblSumDecapDieMarking);
            }

            //XRF
            if (xrf != null)
            {
                lblSumXRFLeadsMatch.Text = GetSummaryResultInt16(lblSumXRFLeadsMatch);
                lblSumXRFForeignMat.Text = GetSummaryResultInt16(lblSumXRFForeignMat);
            }
            //Solderability
            if (sol != null)
            {
                lblSumSolderPassSolderTesting.Text = GetSummaryResultInt16(lblSumSolderPassSolderTesting);
                lblSumSolderWetting.Text = GetSummaryResultInt16(lblSumSolderWetting);

            }

            //Basic Electrical
            if (be != null)
            {
                lblSumBasicElectrical.Text = GetSummaryResultInt16(lblSumBasicElectrical);
            }

            //Sentry VI
            if (sv != null)
            {
                lblSumElectricalVISignature.Text = GetSummaryResultInt16(lblSumElectricalVISignature);
            }

            //Eprom
            if (ep != null)
            {
                lblSumEPROMChecksum.Text = GetSummaryResultInt16(lblSumEPROMChecksum);
                if (ep.ProgrammingRequired == 1)
                    lblSumEPROMValidation.Text = GetSummaryResultInt16(lblSumEPROMValidation);
                else
                {
                    lblSumEPROMValidation.Visible = false;
                    txtSumEPROMValidationComment.Visible = false;
                }
            }
            //Extended Testing
            if (et != null)
            {
                lblSumExtendedTesting.Text = GetSummaryResultInt16(lblSumExtendedTesting);
            }
        }
    }

    private string GetSummaryResultInt16(Control l)
    {

        switch (l.ID)
        {//External Visual
            case "lblSumExternalVisualLogosParts":
                {
                    txtSumExternalVisualLogosPartsComment.Text = vr.LogosPartnumbersComment;
                    switch (vr.LogosPartnumbersPass)
                    {
                        case 1:
                            return "Pass";
                        case 2:
                            return "Fail";
                        default:
                            return "N/A";
                    }
                }

            case "lblSumExternalVisualDim":
                {
                    if (vr.Pass_L == true && vr.Pass_W == true && vr.Pass_T == true)
                    {
                        txtSumExternalVisualDimComment.Text = "Dimensional measurements match MFG spec";
                        return "Pass";
                    }
                    else
                    {
                        txtSumExternalVisualDimComment.Text = "Dimensional measurements do not match MFG spec";
                        return "Fail";
                    }
                }
            case "lblSumExternalVisualLeadPkg":
                {
                    switch (vr.ComponentType)
                    {

                        case "BGA":
                            {
                                if (vr.Oxidation_Discoloration == "Y" && vr.SolderBalls_Spheres == "Y" && vr.Uniform_Concentricity == "Y" && vr.Crushed_Damages_Spheres == "Y")
                                {
                                    txtSumExternalVisualLeadPkgComment.Text = "Pads and package condition acceptable";
                                    return "Pass";
                                }
                                else
                                {
                                    txtSumExternalVisualLeadPkgComment.Text = "Pads and package condition are unacceptable";
                                    return "Fail";
                                }
                            }
                        case "Lead":
                            {
                                if (vr.Lead_PadCondition == "Y" && vr.LeadCoplanarity == "Y" && vr.Oxidation_Discoloration == "Y" && vr.SignsExposedBaseMetal == "Y")
                                {
                                    txtSumExternalVisualLeadPkgComment.Text = "Pads and package condition acceptable";
                                    return "Pass";
                                }
                                else
                                {
                                    txtSumExternalVisualLeadPkgComment.Text = "Pads and package condition are unacceptable";
                                    return "Fail";
                                }
                            }
                        case "Leadless":
                            {
                                if (vr.Lead_PadCondition == "Y" && vr.Oxidation_Discoloration == "Y" && vr.SignsExposedBaseMetal == "Y")
                                {
                                    txtSumExternalVisualLeadPkgComment.Text = "Pads and package condition acceptable";
                                    return "Pass";
                                }
                                else
                                {
                                    txtSumExternalVisualLeadPkgComment.Text = "Pads and package condition are unacceptable";
                                    return "Fail";
                                }
                            }
                        default:
                            {
                                return null;
                            }
                    }
                }
            //What to do for "Signs of alteration?" - IF Top or Bottom Fail, then fail, else pass.
            case "lblSumExternalVisualAlteration":
                {
                    if (vr.Top_Pass == 1 && vr.Bottom_Pass == 1)
                    {
                        txtSumExternalVisualAlterationComment.Text = "No signs of surface alteration observed";
                        return "Pass";
                    }
                    else if (vr.Top_Pass == 2 || vr.Bottom_Pass == 2)
                    {
                        txtSumExternalVisualAlterationComment.Text = "Signs of surface alteration observed";
                        return "Fail";
                    }
                    else
                    {
                        txtSumExternalVisualAlterationComment.Text = "Results inconclusinve";
                        return "Inconclusive";

                    }
                }

            //Marking Permenancy
            case "lblSumMarkingAcetone":
                switch (mperm.AcetoneTestPass)
                {
                    case 1:
                        {
                            txtSumMarkingAcetoneComment.Text = "Passed acetone test";
                            return "Pass";
                        }

                    case 2:
                        {
                            txtSumMarkingAcetoneComment.Text = "Failed acetone test";
                            return "Fail";
                        }
                    default:
                        {
                            txtSumMarkingAcetoneComment.Text = "Acetone test inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumMarkingScrape":
                switch (mperm.ScrapeTestPass)
                {
                    case 1:
                        {
                            txtSumMarkingScrapeComment.Text = "Passed scrape test";
                            return "Pass";
                        }

                    case 2:
                        {
                            txtSumMarkingScrapeComment.Text = "Failed scrape test";
                            return "Fail";
                        }
                    default:
                        {
                            txtSumMarkingScrapeComment.Text = "Scrape test inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumMarkingAlteration":
                {
                    if (mperm.ScrapeTestPass == 1 && mperm.AcetoneTestPass == 1)
                    {
                        txtSumMarkingAlterationComment.Text = "No signs of marking alteration observed";
                        return "Pass";
                    }
                    else if (mperm.ScrapeTestPass == 2 || mperm.AcetoneTestPass == 2)
                    {
                        txtSumMarkingAlterationComment.Text = "Signs of marking alteration observed";
                        return "Fail";
                    }
                    else
                    {
                        txtSumMarkingAlterationComment.Text = "Results inconclusinve";
                        return "Inconclusive";
                    }
                }
            //X-Ray Analysis
            case "lblSumXrayUniform":
                {
                    if (xray.DiePresentPass == 1 && xray.BondWiresPresentPass == 1)
                    {
                        txtSumXrayUniformComment.Text = "Die and bond wires are uniform";
                        return "Pass";
                    }
                    else if (xray.DiePresentPass == 2 && xray.BondWiresPresentPass == 2)
                    {
                        txtSumXrayUniformComment.Text = "Die and bond wires are not uniform";
                        return "Fail";
                    }
                    else
                    {
                        txtSumXrayUniformComment.Text = "Inconclusive";
                        return "Inconclusive";
                    }
                }
            //case "lblSumXraySignatures"://Checking Same Condition as above??
            //    {
            //        if (xray.BondWiresPresentPass == 1 && xray.BondWiresPresentPass == 1)
            //        {
            //            txtSumXraySignaturesComment.Text = "All samples match in signature";
            //            return "Pass";
            //        }
            //        else
            //        {
            //            txtSumXraySignaturesComment.Text = "Samples do not match in signature";
            //            return "Fail";
            //        }
            //    }
            //Heated Solvents Testing
            case "lblSumHeatedSolventsMeth":
                switch (hs.Methyl_PyrrolidinonePass)
                {
                    case 1:
                        {
                            txtSumHeatedSolventsMethComment.Text = "No Signs of secondary coating";
                            return "Pass";
                        }

                    case 2:
                        {
                            txtSumHeatedSolventsMethComment.Text = "Signs of secondary coating observed";
                            return "Fail";
                        }
                    default:
                        {
                            txtSumHeatedSolventsMethComment.Text = "Secondary coating results inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumHeatedSolventsDyna":
                switch (hs.DynasolvePass)
                {
                    case 1:
                        {
                            txtSumHeatedSolventsDynaComment.Text = "No Signs of secondary coating";
                            return "Pass";
                        }

                    case 2:
                        {
                            txtSumHeatedSolventsDynaComment.Text = "Signs of secondary coating observed";
                            return "Fail";
                        }
                    default:
                        {
                            txtSumHeatedSolventsDynaComment.Text = "Secondary coating results inconclusive";
                            return "Inconclusive";
                        }

                }
            case "lblSumHeatedSolventsSecondary":
                switch (hs.SecondaryCoatingPass)
                {
                    case 1:
                        {
                            txtSumHeatedSolventsSecondaryComment.Text = "No Signs of secondary coating";
                            return "Pass";
                        }

                    case 2:
                        {
                            txtSumHeatedSolventsSecondaryComment.Text = "Signs of secondary coating observed";
                            return "Fail";
                        }
                    default:
                        {
                            txtSumHeatedSolventsSecondaryComment.Text = "Secondary coating results inconclusive";
                            return "Inconclusive";
                        }

                }
            //De-Cap Analysis
            case "lblSumDecapWireBondIntegrity":
                {
                    txtSumDecapWireBondIntegrityComment.Text = decap.DieBondWiresComments;
                    switch (decap.DieBondWiresPass)
                    {
                        case 1:
                            {
                                txtSumDecapWireBondIntegrityComment.Text = "Die and bond wires correct in size, orientation and placement";
                                return "Pass";
                            }
                        case 2:
                            {
                                txtSumDecapWireBondIntegrityComment.Text = "Die and bond wires incorrect in size, orientation or placement";
                                return "Fail";
                            }
                        default:
                            {
                                txtSumDecapWireBondIntegrityComment.Text = "Die and bond wire results inconclusive";
                                return "N/A";
                            }
                    }
                }
            case "lblSumDecapDieMarking":
                {
                    if (decap.MFGLogoPass == 1 && decap.LogoMatchPass == 1 && decap.DiePNMatchPass == 1)
                    {
                        txtSumDecapDieMarkingComment.Text = "Die, part, and logo match on all samples";
                        return "Pass";
                    }
                    else if (decap.MFGLogoPass == 2 || decap.LogoMatchPass == 2 || decap.DiePNMatchPass == 2)
                    {
                        txtSumDecapDieMarkingComment.Text = "Die, part, or logo do not match on all samples";
                        return "Fail";
                    }
                    else
                    {
                        txtSumDecapDieMarkingComment.Text = "Inconclusive";
                        return "Inconclusive";
                    }
                }
            //case "lblSUmDecapEOS":
            //    switch ()
            //    {
            //        case 1:
            //            return "Pass";
            //        case 2:
            //            return "Fail";
            //        default:
            //            return "N/A";
            //    }
            //XRF Analysis
            case "lblSumXRFLeadsMatch":
                {
                    txtSumXRFLeadsMatchComment.Text = xrf.LeadMaterialsMatchComments;
                    switch (xrf.LeadMaterialsMatch)
                    {
                        case 1:
                            {
                                txtSumXRFLeadsMatchComment.Text = "Lead materials match MFG specification";
                                return "Pass";
                            }

                        case 2:
                            {
                                txtSumXRFLeadsMatchComment.Text = "Lead materials do not match MFG specification";
                                return "Fail";
                            }

                        default:
                            {
                                txtSumXRFLeadsMatchComment.Text = "Lead materials match inconclusive";
                                return "Inconclusive";
                            }

                    }
                }
            case "lblSumXRFForeignMat":
                {
                    txtSumXRFForeignMatComment.Text = xrf.ForeignMaterialsComments;
                    switch (xrf.ForeignMaterials)
                    {
                        case 1:
                            {
                                txtSumXRFForeignMatComment.Text = "No foreign materials present";
                                return "Pass";
                            }
                        case 2:
                            {
                                txtSumXRFForeignMatComment.Text = "Foreign materials present";
                                return "Fail";
                            }
                        default:
                            {
                                txtSumXRFForeignMatComment.Text = "Inconclusive";
                                return "Inconclusive";
                            }
                    }
                }
            //Solderability Analysis
            case "lblSumSolderPassSolderTesting":
                {
                    //txtSumSolderPassSolderTestingComment.Text = sol.SolderWettingComments;
                    if (sol.RoHS == 1 && sol.VoidFree == 1 && sol.ContaminantFree == 1)
                    {
                        txtSumSolderPassSolderTestingComment.Text = "Solder is RoHS compliant, void-free, and contaminant-free";
                        return "Pass";
                    }
                    else
                    {
                        txtSumSolderPassSolderTestingComment.Text = "Failed solderability testing";
                        return "Fail";
                    }
                    //    switch ()
                    //{
                    //    case 1:
                    //        {

                    //        }

                    //    case 2:
                    //        {
                    //            txtSumSolderPassSolderTestingComment.Text = "Failed solder wetting tests";
                    //            return "Fail";
                    //        }

                    //    default:
                    //        {
                    //            txtSumSolderPassSolderTestingComment.Text = "Solder wetting tests inconclusive";
                    //            return "Inconclusive";
                    //        }

                    //}
                }
            case "lblSumSolderWetting":
                {
                    //txtSumSolderWettingComment.Text = sol.SolderWettingComments;
                    switch (sol.SolderWettingAcceptable)
                    {
                        case 1:
                            {
                                txtSumSolderWettingComment.Text = "Passed solder wetting tests";
                                return "Pass";
                            }

                        case 2:
                            {
                                txtSumSolderWettingComment.Text = "Failed solder wetting tests";
                                return "Fail";
                            }

                        default:
                            {
                                txtSumSolderWettingComment.Text = "Solder wetting tests inconclusive";
                                return "Inconclusive";
                            }
                    }
                }
            //Electrical Analysis
            case "lblSumElectricalVISignature":
                {
                    txtSumElectricalVISignatureComment.Text = sv.SignatureAnalysisComments;
                    switch (sv.SignatureAnalysisPass)
                    {
                        case 1:
                            {
                                txtSumElectricalVISignatureComment.Text = "Passed Sentry VI electrical testing";
                                return "Pass";
                            }

                        case 2:
                            {
                                txtSumElectricalVISignatureComment.Text = "Failed Sentry VI electrical testing";
                                return "Fail";
                            }
                        default:
                            {
                                txtSumElectricalVISignatureComment.Text = "Sentry VI electrical testing results inconclusive";
                                return "Inconclusive";
                            }

                    }
                }
            case "lblSumBasicElectrical":
                {
                    txtSumBasicElectricalComment.Text = et.notes;
                    switch (et.ExtTestingResult)
                    {
                        case "Y":
                            {
                                txtSumBasicElectricalComment.Text = "Passed basic electrical testing";
                                return "Pass";
                            }
                        case "N":
                            {
                                txtSumBasicElectricalComment.Text = "Failed basic electrical testing";
                                return "Fail";
                            }
                        case "N/A":
                            {
                                txtSumBasicElectricalComment.Text = "Basic electrical testing results inconclusive";
                                return "N/A";
                            }
                        default:
                            {
                                txtSumBasicElectricalComment.Text = "-Choose-";
                                return "-Choose-";
                            }

                    }
                }

            //EPROM Testing
            case "lblSumEPROMChecksum":
                {
                    txtSumEPROMChecksumComment.Text = ep.ChecksumComment;
                    switch (ep.ChecksumAnalysis)
                    {
                        case 1:
                            {
                                txtSumEPROMChecksumComment.Text = "All samples passed checksum testing and were clear of data";
                                return "Pass";
                            }

                        case 2:
                            {
                                txtSumEPROMChecksumComment.Text = "Samples failed checksum / blank-check testing";
                                return "Fail";
                            }
                        default:
                            {
                                txtSumEPROMChecksumComment.Text = "N/A";
                                return "N/A";
                            }

                    }
                }
            case "lblSumEPROMValidation":
                {
                    if (ep.ProgrammingRequired == 1)
                    {
                        txtSumEPROMValidationComment.Text = ep.ProgramLoadComment;
                        switch (ep.VerifyProgramLoad)
                        {
                            case 1:
                                {
                                    txtSumEPROMValidationComment.Text = "Passed programming validation check";
                                    return "Pass";
                                }
                            case 2:
                                {
                                    txtSumEPROMValidationComment.Text = "Failed programming validation check";
                                    return "Fail";
                                }
                            default:
                                {
                                    txtSumEPROMValidationComment.Text = "N/A";
                                    return "N/A";
                                }
                        }
                    }
                    return null;
                }
            case "lblSumExtendedTesting":
                {
                    switch (et.ExtTestingResult)
                    {
                        case "0":
                            {
                                txtSumExtendedTestingComment.Text = "-Choose-";
                                return "-Choose-";
                            }
                        case "Y":
                            {
                                txtSumExtendedTestingComment.Text = "Passed extended testing";
                                return "Pass";
                            }
                        case "N":
                            {
                                txtSumExtendedTestingComment.Text = "Failed extended testing";
                                return "Fail";
                            }
                        default:
                            {
                                txtSumExtendedTestingComment.Text = "N/A";
                                return "N/A";
                            }
                    }
                }
            default:
                return null;
        }
    }


    protected void ddlChooseCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSalesOrderDDL();
    }

    protected void ddlChooseSale_SelectedIndexChanged(object sender, EventArgs e)
    {
        sale = rdc.ordhed_sales.Where(w => w.unique_id == ddlChooseSale.SelectedValue).FirstOrDefault();
        if (sale != null)
        {
            lblHeaderCustomerPO.Text = sale.orderreference;
            lblHeaderAccountManager.Text = sale.agentname;
            BindLineItemDDL();
        }

    }

    protected void BindLineItemDDL()
    {

        ddlLineItem.Items.Clear();
        ddlLineItem.Items.Add(new ListItem("-Choose-", "0"));
        ddlLineItem.AppendDataBoundItems = true;
        ddlLineItem.DataSource = rdc.orddet_lines.Where(w => w.orderid_sales == ddlChooseSale.SelectedValue && !w.fullpartnumber.ToLower().Contains("gcat")).Select(s => new KeyValuePair<string, string>(s.unique_id, s.fullpartnumber + " - QTY:" + s.quantity + " - Line#" + s.linecode_sales));
        ddlLineItem.DataValueField = "Key";
        ddlLineItem.DataTextField = "Value";
        ddlLineItem.DataBind();
    }

    protected void ddlLineItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlLineItem.SelectedValue != "0")
            {
                //Fill ManufacturerPartGrid with possible choices:
                line = rdc.orddet_lines.Where(w => w.unique_id == ddlLineItem.SelectedValue).FirstOrDefault();
                if (line != null)
                {
                    lblCustomerPart.Text = line.internal_customer;
                }
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void LoadManufacturerPartGrid()
    {
        gvMPNSelection.DataSource = null;
        gvMPNSelection.DataBind();
        string trimmedPart;
        if (!string.IsNullOrEmpty(txtMPNSearch.Text))
            if (txtMPNSearch.Text.Length >= 5)
                trimmedPart = txtMPNSearch.Text.Substring(0, 5);//Check the first 5 characters.
            else
                trimmedPart = txtMPNSearch.Text;
        else
        {
            tools.HandleResult( "fail", "Please search for a Part Number.");
            return;
        }
        List<ManufacturerPart> MPNList = new List<ManufacturerPart>();
        MPNList = ciq.ManufacturerParts.Where(w => w.MPN.Contains(trimmedPart)).ToList();
        gvMPNSelection.DataKeyNames = new string[] { "MPNID" };
        gvMPNSelection.DataSource = MPNList;
        gvMPNSelection.DataBind();
        gvMPNSelection.Visible = true;

    }

    protected void btnUploadImage_Click(object sender, EventArgs e)
    {
        SaveImagery();

    }

    private void SaveImagery()
    {
        try
        {
            string sectionID;
            foreach (InspectionImageManagerControl c in GetImageManagerControlList())
            {
                if (c.hasFile)
                {
                    c.SaveImage(GCATID, "gcat");
                    UploadedImageCount++;
                }

            }

            if (UploadedImageCount > 0)
                tools.HandleResult( "success", UploadedImageCount + " images uploaded.");

        }
        catch (Exception ex)
        {
            tools.HandleResult( "fail", ex.Message);
        }
    }

    //private void UploadImageLegacy(FileUpload fu, string sectionID)
    //{
    //    Image i = (Image)Page.Master.FindControl("MainContent").FindControl("img" + sectionID);
    //    if (i != null)
    //    {
    //        SaveImage(i, sectionID, fu);
    //        LoadImage(i);
    //    }

    //}


    protected string LoadPhysicalImage(string filepath)
    {
        return "~/customControls/GetPhysicalImage.aspx?FileName=" + filepath;
    }

    protected void lbSaveAll_Click(object sender, EventArgs e)
    {
        try
        {
            //ScriptManager.RegisterClientScriptBlock(Page, GetType(), "ShowUpdateProgress", "$(document).ready(ShowUpdateProgress());", true);
            SaveAll();
            smb.SaveChanges();
            ciq.SubmitChanges();
            LoadSummary();
            //ScriptManager.RegisterClientScriptBlock(Page, GetType(), "HandleUI", "$(document).ready(HandleUI());", true);
            tools.HandleResult( "success", "All sections saved successfully.");

        }
        catch (DbEntityValidationException eve)
        {
            string error = "";
            foreach (var validationErrors in eve.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    error += validationError.PropertyName + ": " + validationError.ErrorMessage + Environment.NewLine;
                    //System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName,validationError.ErrorMessage);                    
                }
            }
            tools.HandleResult( "fail", error);
        }
        catch (Exception ex)
        {
            tools.HandleResult( "fail", ex.Message);
        }
    }

    protected void SaveAll()
    {
        //sm_binary_mysql_Entities smb = new sm_binary_mysql_Entities();
        //SaveManufacturerPart(MPN, MFG);
        SaveHeader();
        //SaveComponent(); MOved Save Header as it need to fire there too to keep MPN stuff in sync with vr.MPN, etc.
        SaveRequired();
        SaveVisualComponent();
        SaveVisualSurface();
        //Optional Tests - Save based on checkbox.
        if (cbxTestTypeMarkingPerm.Checked)
            SaveMarking();
        if (cbxTestTypeXRay.Checked)
            SaveXray();
        if (cbxTestTypeHeatedSolvents.Checked)
            SaveHeatedSolvents();
        if (cbxTestTypeDecap.Checked)
            SaveDecap();
        if (cbxTestTypeSolderability.Checked)
            SaveSolderability();
        if (cbxTestTypeEprom.Checked)
            SaveEprom();
        if (cbxTestTypeBasicElectrical.Checked)
            SaveBasicElectrical();
        if (cbxTestTypeSentryVI.Checked)
            SaveSentryVI();
        if (cbxTestTypeXRF.Checked)
            SaveXRFAnalysis();
        if (cbxTestTypeExtendedTesting.Checked)
            SaveExtendedTesting();
        if (cbxTestTypeExtendedImagery.Checked)
            SaveExtendedImagery();
        if (cbxTestTypeMoistureBaking.Checked)
            SaveMoistureBaking();
        //if (listExtendedImages.Count > 0)
        //    SaveExtendedImagery();
        SaveSummary();
    }

    private void SaveExtendedImagery()
    {
        //iig.InspectionID = GCATID;
        //iig.InspectionType = SM_Enums.InspectionType.gcat.ToString().ToUpper();
        //iig.SectionName = "ExtendedImagery";
        iig.SaveAllImages();
    }

    private void SaveHeader()
    {
        if (GCATID > 0)  //         
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();//Update       
        else
        {
            CreateGCAT();
        }
        //GCATID = vr.ValidationReportID;
        vr.Form = "F-850-009 Rev B";
        vr.company_uid = ddlChooseCustomer.SelectedValue;
        vr.company_name = ddlChooseCustomer.SelectedItem.ToString();
        vr.Manufacturer = lblHeaderMFG.Text;
        vr.CustomerPO = lblHeaderCustomerPO.Text;
        vr.CPN = lblCustomerPart.Text;

        //Date & ID
        lblReportID.Text = "ID: " + vr.ValidationReportID.ToString();
        lblDate.Text = "Date: " + vr.ReportDate.Value.ToString("d");



        //Sale Data
        sale = rdc.ordhed_sales.Where(w => w.unique_id == ddlChooseSale.SelectedValue).SingleOrDefault();
        if (sale != null)
        {
            vr.SalesOrderNumber = sale.ordernumber;
            vr.agent_uid = sale.base_mc_user_uid;
            vr.agent_name = sale.agentname;
        }

        //CustomerPart   
        cp = ciq.CustomerParts.Where(w => w.CPNID == vr.CPNID).SingleOrDefault();
        if (cp == null)
        {
            cp = new CustomerPart();
            cp.CPN = vr.CPN; //from label            
            ciq.CustomerParts.InsertOnSubmit(cp);
            ciq.SubmitChanges();
        }
        vr.CPNID = cp.CPNID;

        //Line Data
        line = rdc.orddet_lines.Where(w => w.unique_id == ddlLineItem.SelectedValue).SingleOrDefault();
        if (line != null)
        {
            //Need to update Manufacturer part too, in case user has changed the  part, need to update the validationreport as well as manufacturerpart
            //Update ValidationReport
            //Wait, No, I need to just push the user back through the selection, and fix it there.
            //vr.MPN = line.fullpartnumber.Trim().ToUpper();
            vr.orddet_line_uid = line.unique_id;
            vr.orderid_sales = line.orderid_sales;
            vr.orderid_purchase = line.orderid_purchase;
            if (cp != null)
            {
                cp.CPN = line.internal_customer ?? null;
                cp.Customer = line.customer_name;
            }


            if (string.IsNullOrEmpty(vr.agent_uid))
            {
                vr.agent_uid = line.seller_uid;
                vr.agent_name = line.seller_name;
            }
        }

        //QTY Sampled:  
        if (!string.IsNullOrEmpty(txtQuantity1.Text))
            vr.Quantity1 = Convert.ToInt32(txtQuantity1.Text);

        //Summary Notes
        if (!string.IsNullOrEmpty(txtHeaderSummaryNotes.Text))
            vr.summaryNotes = txtHeaderSummaryNotes.Text;
        else
            vr.summaryNotes = null;
        //Calling this in here to keep vr stuff in sync with mpn stuff.
        SaveComponent();
    }

    protected void LoadImage(Image img, string imageFilePath = null)
    {
        try
        {
            if (vr == null)
                vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
            insp_images dbImage;
            string sectionID = img.ID.Substring(3);
            HtmlAnchor fb = (HtmlAnchor)Page.Master.FindControl("MainContent").FindControl("fancybox" + sectionID);
            if (vr.is_demo ?? false)//For Demos, we need to reuse the images from the database, as new tests, won't have image paths, so we'll lookup based on the source gcat id
                dbImage = smb.insp_images.Where(w => w.insp_id == vr.demo_source_id && w.insp_section_id == sectionID && w.insp_type == "GCAT").SingleOrDefault();//Does the image exist in MyQL?
            else
                dbImage = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID && w.insp_type == "GCAT").SingleOrDefault();//Does the image exist in MyQL?
            if (dbImage == null && !vr.is_imported)//If not found in new database and not already imported from old,from the old
                if (!string.IsNullOrEmpty(imageFilePath) || string.IsNullOrEmpty(dbImage.img_path_web))
                {
                    SaveImage(img, imageFilePath);
                    dbImage = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID).SingleOrDefault();
                    if (dbImage != null)
                        tools.HandleResult( "fail", "Image(s) successfully converted");
                }
            if (dbImage != null)
            {

                img.ImageUrl = "~/public/web_handlers/ImageHandler.ashx?id=" + dbImage.insp_image_id + "&type=gcat";
                img.Visible = true;
                fb.HRef = img.ImageUrl;
                fb.Visible = true;

            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    //private void SaveImage(Image img, string sectionID, FileUpload fu)
    //{
    //    insp_images i;
    //    //string sectionID = img.ID.Substring(3);
    //    if (fu == null)
    //        fu = (FileUpload)this.Page.Master.FindControl("MainContent").FindControl("fuImg" + sectionID);
    //    if (fu.HasFile)
    //    {
    //        i = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID).FirstOrDefault();
    //        if (i == null)
    //        {
    //            i = new insp_images();
    //            i.date_created = DateTime.Now;
    //            smb.insp_images.Add(i);
    //        }
    //        i.img_name = img.ID;
    //        i.img_type = "jpg";
    //        i.img_blob = tools.ResizeImage(fu.FileBytes, 1024);
    //        //i.img_description = "not set";
    //        i.insp_id = GCATID;
    //        i.insp_section_id = sectionID;
    //        i.insp_image_id = Guid.NewGuid().ToString();
    //        i.img_path = fu.FileName;
    //        string report = "";
    //        i.img_path_web = tools.SaveImageFileToFolder(i, fu.FileBytes, out report);
    //        i.insp_type = "GCAT";
    //        i.date_modified = DateTime.Now;
    //        smb.SaveChanges();
    //        UploadedImageCount++;
    //    }
    //}

    private void SaveImage(Image img, string imageFilePath, insp_images i = null)
    {

        if (i == null)
        {
            i = new insp_images();
            smb.insp_images.Add(i);
        }
        string sectionID = img.ID.Substring(3);
        byte[] imgBytes;
        i.img_name = img.ID;
        i.img_type = "jpg";
        imageFilePath = imageFilePath.Replace("P:", @"\\SM\Shared_Docs\Part Photos");
        imgBytes = File.ReadAllBytes(imageFilePath);
        imgBytes = tools.ResizeImage(imgBytes, 1024);
        i.img_blob = new byte[0];
        //i.img_description = "not set";
        string report = "";
        BinaryLogic bl = new BinaryLogic();
        i.img_path_web = bl.SaveImageFileToFolder(i, imgBytes, out report);
        i.insp_id = GCATID;
        i.insp_section_id = sectionID;
        i.insp_image_id = Guid.NewGuid().ToString();
        i.img_path = imageFilePath;
        i.insp_type = "GCAT";
        smb.SaveChanges();
    }

    protected void DeleteImage(Image img)
    {
        insp_images dbImage;
        string sectionID = img.ID.Substring(3);
        HtmlAnchor fb = (HtmlAnchor)Page.Master.FindControl("MainContent").FindControl("fancybox" + sectionID);
        //delete the image
        dbImage = smb.insp_images.Where(w => w.insp_id == GCATID && w.insp_section_id == sectionID).SingleOrDefault();//Does the image exist in MyQL?
        if (dbImage == null)
            return;
        smb.insp_images.Remove(dbImage);
        smb.SaveChanges();
        fb.Visible = false;
    }


    protected void lbDeleteImage_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lb = sender as LinkButton;
            Image img = (Image)Page.Master.FindControl("MainContent").FindControl("img" + lb.ID.Substring(8));
            DeleteImage(img);
            tools.HandleResult("success", "Image deleted successfully.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void SaveRequired()
    {

        if (vr == null)
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        //Customer Categorization
        if (ddlRequiredCatTypeA.SelectedValue != "0")
            vr.CustomerTypeA = ddlRequiredCatTypeA.SelectedValue;
        else
            vr.CustomerTypeA = null;
        if (ddlRequiredCatTypeB.SelectedValue != "0")
            vr.CustomerTypeB = ddlRequiredCatTypeB.SelectedValue;
        else
            vr.CustomerTypeB = null;
        if (ddlRequiredCatTypeC.SelectedValue != "0")
            vr.CustomerTypeC = ddlRequiredCatTypeC.SelectedValue;
        else
            vr.CustomerTypeC = null;

        //Standardized Tests
        vr.is_CCAP = cbxIsCCAP.Checked;
        vr.is_AS6081 = cbxIsAS6081.Checked;
        //if (ddlStandardTestTypeA.SelectedValue != "0")
        //    vr.Test_StandardA = ddlStandardTestTypeA.SelectedValue;
        //if (ddlStandardTestTypeB.SelectedValue != "0")
        //    vr.Test_StandardB = ddlStandardTestTypeB.SelectedValue;
        //if (ddlStandardTestTypeC.SelectedValue != "0")
        //    vr.Test_StandardC = ddlStandardTestTypeC.SelectedValue;
        //if (ddlStandardTestTypeD.SelectedValue != "0")
        //    vr.Test_StandardD = ddlStandardTestTypeD.SelectedValue;

        //Additional Customer Requirements
        vr.Customer_RequirementA = txtCustomer_RequirementA.Text;
        //Delete B C and D to prevent duplication after concatenating them on load.
        vr.Customer_RequirementB = null;
        vr.Customer_RequirementC = null;
        vr.Customer_RequirementD = null;

        //Test Types
        if (cbxTestTypeDecap.Checked)
            vr.Test_TypeA = "De-Capsulation";
        else
            vr.Test_TypeA = null;
        if (cbxTestTypeHeatedSolvents.Checked)
            vr.Test_TypeD = "Heated Solvents";
        else
            vr.Test_TypeD = null;

        if (cbxTestTypeMarkingPerm.Checked)
            vr.Test_TypeE = "Marking Permanency";
        else
            vr.Test_TypeE = null;

        if (cbxTestTypeSentryVI.Checked)
            vr.Test_TypeF = "Sentry V-I Signature";
        else
            vr.Test_TypeF = null;

        if (cbxTestTypeSolderability.Checked)
            vr.Test_TypeG = "Solderability";
        else
            vr.Test_TypeG = null;

        if (cbxTestTypeXRay.Checked)
            vr.Test_TypeH = "X-Ray";
        else
            vr.Test_TypeH = null;

        if (cbxTestTypeXRF.Checked)
            vr.Test_TypeI = "XRF Analysis";
        else
            vr.Test_TypeI = null;

        //Other tests
        vr.Test_TypeJ = txtTest_TypeJ.Text;

        //throw new NotImplementedException();
    }

    //KT Try using this to pass table (class) as parameter? http://stackoverflow.com/questions/1992443/why-do-i-get-error-must-be-a-reference-type-in-my-c-sharp-generic-method

    private void removeTest(string testtype)
    {
        string SectionName = GetTestSection(testtype);
        switch (SectionName)
        {
            case "decap":
                {
                    if (ciq.Decapsulations.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (decap == null)
                            decap = ciq.Decapsulations.Single(w => w.ValidationReportID == GCATID);
                        ciq.Decapsulations.DeleteOnSubmit(decap);
                        DeleteSectionImages(SectionName);
                    }
                    break;
                }
            case "heatedsolvents":
                {
                    if (ciq.HeatedSolvents.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (hs == null)
                            hs = ciq.HeatedSolvents.Single(w => w.ValidationReportID == GCATID);
                        ciq.HeatedSolvents.DeleteOnSubmit(hs);
                        DeleteSectionImages("heated");
                    }
                    break;
                }
            case "moisturebaking":
                {
                    if (ciq.MoistureBakings.Any(w => w.validationreportid == GCATID))
                    {
                        if (mb == null)
                            mb = ciq.MoistureBakings.Single(w => w.validationreportid == GCATID);
                        ciq.MoistureBakings.DeleteOnSubmit(mb);
                        DeleteSectionImages("moisturebaking");
                    }
                    break;
                }
            case "markingperm":
                {
                    if (ciq.MarkingPermanencies.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (mperm == null)
                            mperm = ciq.MarkingPermanencies.Single(w => w.ValidationReportID == GCATID);
                        ciq.MarkingPermanencies.DeleteOnSubmit(mperm);
                        DeleteSectionImages(SectionName);
                    }
                    break;
                }
            case "solderability":
                {
                    if (ciq.Solderabilities.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (sol == null)
                            sol = ciq.Solderabilities.Single(w => w.ValidationReportID == GCATID);
                        ciq.Solderabilities.DeleteOnSubmit(sol);
                        DeleteSectionImages("solder");
                    }
                    break;
                }
            case "xray":
                {
                    if (ciq.XRays.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (xray == null)
                            xray = ciq.XRays.Single(w => w.ValidationReportID == GCATID);
                        ciq.XRays.DeleteOnSubmit(xray);
                        DeleteSectionImages(SectionName);
                    }
                    break;
                }
            case "xrf":
                {
                    if (ciq.XRFs.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (xrf == null)
                            xrf = ciq.XRFs.Single(w => w.ValidationReportID == GCATID);
                        ciq.XRFs.DeleteOnSubmit(xrf);
                        DeleteSectionImages(SectionName);
                    }
                    break;
                }
            case "basicelectrical":
                {
                    if (ciq.BasicElectricals.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (be == null)
                            be = ciq.BasicElectricals.Single(w => w.ValidationReportID == GCATID);
                        ciq.BasicElectricals.DeleteOnSubmit(be);
                        DeleteSectionImages("basicelectrical");
                    }
                    break;
                }
            case "sentryvi":
                {
                    if (ciq.SentryVIs.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (sv == null)
                            sv = ciq.SentryVIs.Single(w => w.ValidationReportID == GCATID);
                        ciq.SentryVIs.DeleteOnSubmit(sv);
                        DeleteSectionImages("sentry");
                    }
                    break;
                }
            case "eprom":
                {
                    if (ciq.EPROMs.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (ep == null)
                            ep = ciq.EPROMs.Single(w => w.ValidationReportID == GCATID);
                        ciq.EPROMs.DeleteOnSubmit(ep);
                        DeleteSectionImages("eprom");
                    }
                    break;
                }
            case "extendedtesting":
                {
                    if (ciq.ExtendedTestings.Any(w => w.ValidationReportID == GCATID))
                    {
                        if (et == null)
                            et = ciq.ExtendedTestings.Single(w => w.ValidationReportID == GCATID);
                        ciq.ExtendedTestings.DeleteOnSubmit(et);
                        DeleteSectionImages("ext");
                    }
                    break;
                }
        }
        ciq.SubmitChanges();
    }

    protected void DeleteSectionImages(string testSection)
    {
        smb.insp_images.RemoveRange(smb.insp_images.Where(w => w.insp_section_id.Contains(testSection) && w.insp_id == GCATID));
        smb.SaveChanges();
    }

    protected void DeleteAllImages()
    {
        smb.insp_images.RemoveRange(smb.insp_images.Where(w => w.insp_id == GCATID));//Good, this won't delete Demos, since they have a different GCATID from the original
        smb.SaveChanges();
    }

    private string GetTestSection(string testtype)
    {
        return testtype.Substring(11).ToLower();
    }

    private void SaveComponent()
    {
        if (mp == null)
            mp = ciq.ManufacturerParts.Where(w => w.MPNID == MPNID).SingleOrDefault();
        if (vr == null)
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();

        //RoHS Compliant
        mp.RoHS_Compliant = ddlComponentIdentityRohs_compliant.SelectedValue;

        //Memory Device        
        mp.MemoryDevice = ddlComponentIdentityMemoryDevice.SelectedValue;

        //MSD / MSL
        //mp.MSD_MSL = ddlComponentIdentityMSD.SelectedValue;
        //if (ddlComponentIdentityMSD.SelectedValue != "Y")
        //{
        //    txtComponentIdentityMoistureLevel.Text = "";
        //    txtComponentIdentityMoistureLevel.Enabled = false;
        //    mp.MoistureLevel = null;
        //}
        //else
        //{
        mp.MoistureLevel = ddlComponentIdentityMoistureLevel.SelectedValue;
        //txtComponentIdentityMoistureLevel.Enabled = true;
        //}


        //Condition Checkboxes
        if (cblComponentIdentityCondition.Items.FindByText("New").Selected == true)
            vr.New = true;
        else
            vr.New = false;
        if (cblComponentIdentityCondition.Items.FindByText("Used").Selected == true)
            vr.Used = true;
        else
            vr.Used = false;
        if (cblComponentIdentityCondition.Items.FindByText("Refurbished").Selected == true)
            vr.Refurbished = true;
        else
            vr.Refurbished = false;

        //Component Type
        if (cblComponentIdentityType.Items.FindByValue("sm").Selected == true)
            mp.SurfaceMount = true;
        else
            mp.SurfaceMount = false;
        if (cblComponentIdentityType.Items.FindByValue("th").Selected == true)
            mp.ThroughHole = true;
        else
            mp.ThroughHole = false;
        if (cblComponentIdentityType.Items.FindByValue("bga").Selected == true)
            mp.BGA = true;
        else
            mp.BGA = false;

        //ValidationReport Props
        //Traceability      
        vr.Traceability = Convert.ToInt16(ddlComponentIdentityTraceability.SelectedValue);
        vr.MPN = mp.MPN;
        vr.Manufacturer = mp.Manufacturer;


        //Package Type
        mp.PackageType = txtComponentIdentityPackageType.Text;


        //Measurements  
        mp.Lead_PinPitch = txtComponentIdentityLeadPinBGAPitch.Text;
        mp.Lead_PinCount = txtComponentIdentityLeadPinBGACount.Text;
        mp.Length = txtComponentIdentityLength.Text;
        mp.Width = txtComponentIdentityWidth.Text;
        mp.Thickness = txtComponentIdentityThickness.Text;


        //Commit
        //ciq.SubmitChanges();

    }
    private void SaveVisualComponent()
    {
        if (vr == null)
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();

        //Sample Quantity
        if (!string.IsNullOrEmpty(txtVisualComponentSamplyQTY.Text))
            vr.VisualComponentSampleQty = Convert.ToInt32(txtVisualComponentSamplyQTY.Text);
        else
            vr.VisualComponentSampleQty = 0;

        //Observed Measurements
        //Width
        if (!string.IsNullOrEmpty(txtVisualComponentWidthActual.Text))
        {
            vr.Width_Actual = Convert.ToDecimal(txtVisualComponentWidthActual.Text);
            vr.UnitWidth = txtVisualComponentWidthUnit.Text;
            vr.Pass_W = HandleVisualComponentDimensionalddl(ddlVisualComponentPassWidth);
            vr.CommentsWidth = txtVisualComponentPassWidthComment.Text;
        }

        //Length
        if (!string.IsNullOrEmpty(txtVisualComponentLengthActual.Text))
        {
            vr.Length_Actual = Convert.ToDecimal(txtVisualComponentLengthActual.Text);
            vr.UnitLength = txtVisualComponentLengthUnit.Text;
            vr.Pass_L = HandleVisualComponentDimensionalddl(ddlVisualComponentPassLength);
            vr.CommentsLength = txtVisualComponentPassLengthComment.Text;
        }
        //Thickness
        if (!string.IsNullOrEmpty(txtVisualComponentThicknessActual.Text))
        {
            vr.Thickness_Actual = Convert.ToDecimal(txtVisualComponentThicknessActual.Text);
            vr.UnitThickness = txtVisualComponentThicknessUnit.Text;
            vr.Pass_T = HandleVisualComponentDimensionalddl(ddlVisualComponentPassThickness);
            vr.CommentsThickness = txtVisualComponentPassThicknessComment.Text;
        }
        //Visual Component Package Type        
        vr.ComponentType = ddlVisualComponentPackageConnectionType.SelectedValue;

        //Date Codes
        SetDateCodesAndQTY(mp);

        //Component Property Results
        vr.Lead_Count = ddlVisualComponentCastellationResult.SelectedValue;
        vr.CommentLeadCount = txtVisualComponentCastellationComment.Text;

        vr.Lead_PadCondition = ddlVisualComponentLeadPadConditionResult.SelectedValue;
        vr.CommentLead_Pad = txtVisualComponentLeadPadConditionComment.Text;

        vr.LeadCoplanarity = ddlVisualComponentCoplanarityResult.SelectedValue;
        vr.CommentLeadCoplanarity = txtVisualComponentCoplanarityComment.Text;

        vr.Oxidation_Discoloration = ddlVisualComponentOxidationResult.SelectedValue;
        vr.CommentO2Discoloration = txtVisualComponentOxidationComment.Text;

        vr.SignsExposedBaseMetal = ddlVisualComponentExposedBaseMetalResult.SelectedValue;
        vr.CommentExpBaseMetal = txtVisualComponentExposedBaseMetalComment.Text;

        //BGA
        vr.SolderBalls_Spheres = ddlVisualComponentSolderResult.SelectedValue;
        vr.CommentSolderBalls = txtVisualComponentSolderComment.Text;

        vr.Uniform_Concentricity = ddlVisualComponentConcentricityResult.SelectedValue;
        vr.CommentUniformity = txtVisualComponentConcentricityComment.Text;

        vr.Crushed_Damages_Spheres = ddlVisualComponentDamagedSpheresResult.SelectedValue;
        vr.CommentDamagedSpheres = txtVisualComponentDamagedSpheresComment.Text;
        //Maintain Dropdowns after postback
        // ScriptManager.RegisterClientScriptBlock(Page, GetType(), "HandleUI", "$(document).ready(HandleUI());", true);

    }

    private bool? HandleVisualComponentDimensionalddl(DropDownList ddl)
    {
        switch (ddl.SelectedValue)
        {
            case "1":
                {
                    return true;
                }
            case "0":
                {
                    return false;
                }
            default:
                {
                    return null;
                }
        }
    }
    private void SaveVisualSurface()
    {
        if (vr == null)
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();

        if (!string.IsNullOrEmpty(txtVisualSurfaceSampleQTY.Text))
            vr.VisualSurfaceSampleQty = Convert.ToInt32(txtVisualSurfaceSampleQTY.Text);
        else
            vr.VisualSurfaceSampleQty = 0;
        vr.LogosPartnumbersPass = Convert.ToInt16(ddlVisualSurfaceLogo.SelectedValue);
        vr.LogosPartnumbersComment = txtVisualSurfaceLogo.Text;
        vr.Top_Pass = Convert.ToInt16(ddlVisualSurfaceTop.SelectedValue);
        vr.Comments_Top = txtVisualSurfaceTopComment.Text;
        vr.Bottom_Pass = Convert.ToInt16(ddlVisualSurfaceBot.SelectedValue);
        vr.Comments_Bottom = txtVisualSurfaceBotComment.Text;


    }


    private void SaveMarking()
    {
        mperm = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (mperm == null)
        {
            mperm = new MarkingPermanency();
            mperm.ValidationReportID = GCATID;
            ciq.MarkingPermanencies.InsertOnSubmit(mperm);
        }

        if (!string.IsNullOrEmpty(txtMarkingPermSampleQTY.Text))
            mperm.MarkingSample_Qty = Convert.ToInt32(txtMarkingPermSampleQTY.Text);
        else
            mperm.MarkingSample_Qty = 0;

        mperm.AcetoneTestPass = Convert.ToInt16(ddlMarkingPermAcetone.SelectedValue);
        mperm.Comments_Acetone = txtMarkingPermAcetoneComment.Text;

        mperm.Alcohol_MineralSpiritsTestPass = Convert.ToInt16(ddlMarkingPermAlcohol.SelectedValue);
        mperm.Comments_Alcohol = txtMarkingPermAlcoholComment.Text;

        mperm.ScrapeTestPass = Convert.ToInt16(ddlMarkingPermScrape.SelectedValue);
        mperm.Comments_ScrapeTest = txtMarkingPermScrapeComment.Text;

        mperm.SecondaryCoatingTestPass = Convert.ToInt16(ddlMarkingPermSecondary.SelectedValue);
        mperm.Comments_SecondaryCoating = txtMarkingPermSecondaryComment.Text;

        mperm.PartNumber_MarkingTestPass = Convert.ToInt16(ddlMarkingPermPartMarking.SelectedValue);
        mperm.Comments_PartNumber = txtMarkingPermPartMarkingComment.Text;

        mperm.CaseMarking_LogoTestPass = Convert.ToInt16(ddlMarkingPermCase.SelectedValue);
        mperm.Comments_CaseMarking = txtMarkingPermCaseComment.Text;

        ////Imagery
        //if (fuImgMarkingPerm1.HasFile)
        //{
        //    SaveImage(imgMarkingPerm1, fuImgMarkingPerm1);
        //}
        //if (fuImgMarkingPerm2.HasFile)
        //{
        //    SaveImage(imgMarkingPerm2, fuImgMarkingPerm2);
        //}
        //throw new NotImplementedException();

    }
    private void SaveXray()
    {
        xray = ciq.XRays.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (xray == null)
        {
            xray = new XRay();
            xray.ValidationReportID = GCATID;
            ciq.XRays.InsertOnSubmit(xray);
        }
        if (!string.IsNullOrEmpty(txtXraySampleQTY.Text))
            xray.XRaySampleQty = Convert.ToInt32(txtXraySampleQTY.Text);
        else
            xray.XRaySampleQty = 0;

        xray.DiePresentPass = Convert.ToInt16(ddlXrayDiePresent.SelectedValue);
        xray.DiePresentComments = txtXrayDiePresentComment.Text;

        xray.BondWiresPresentPass = Convert.ToInt16(ddlXrayBondWires.SelectedValue);
        xray.BondWiresPresentComment = txtXrayBondWiresComment.Text;

        xray.Damaged_MissingWiresPass = Convert.ToInt16(ddlXrayDamagedBond.SelectedValue);
        xray.Damaged_MissingWiresComment = txtXrayDamagedBondComment.Text;

        xray.DieLeadUniformityPass = Convert.ToInt16(ddlXrayDieLead.SelectedValue);
        xray.DieLeadUniformityComment = txtXrayDieLeadComment.Text;

        xray.DieFRameSize_LocationPass = Convert.ToInt16(ddlXrayDieFrame.SelectedValue);
        xray.DieFrameSize_LocationComment = txtXrayDieFrameComment.Text;

        ////Imagery
        //if (fuImgXray1.HasFile)
        //{
        //    SaveImage(imgXray1, fuImgXray1);
        //}
        //if (fuImgXray2.HasFile)
        //{
        //    SaveImage(imgXray2, fuImgXray2);
        //}
        //if (fuImgXray3.HasFile)
        //{
        //    SaveImage(imgXray3, fuImgXray3);
        //}
        //throw new NotImplementedException();
    }
    private void SaveHeatedSolvents()
    {
        hs = ciq.HeatedSolvents.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (hs == null)
        {
            hs = new HeatedSolvent();
            hs.ValidationReportID = GCATID;
            ciq.HeatedSolvents.InsertOnSubmit(hs);
        }
        if (!string.IsNullOrEmpty(txtHeatedSolventsSampleQTY.Text))
            hs.HeatedSolventSampleQty = Convert.ToInt32(txtHeatedSolventsSampleQTY.Text);
        else hs.HeatedSolventSampleQty = 0;

        hs.Methyl_PyrrolidinonePass = Convert.ToInt16(ddlHeatedMeth.SelectedValue);
        hs.Methyl_PyrrolidinoneComments = txtHeatedMethComment.Text;

        hs.DynasolvePass = Convert.ToInt16(ddlHeatedDyn.SelectedValue);
        hs.DynasolveComments = txtHeatedDynComment.Text;

        hs.SecondaryCoatingPass = Convert.ToInt16(ddlHeatedSecondary.SelectedValue);
        hs.SecondaryCoatingComments = txtHeatedSecondaryComment.Text;

    }
    private void SaveDecap()
    {
        decap = ciq.Decapsulations.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (decap == null)
        {
            decap = new Decapsulation();
            decap.ValidationReportID = GCATID;
            ciq.Decapsulations.InsertOnSubmit(decap);
        }

        if (!string.IsNullOrEmpty(txtDecapSampleQTY.Text))
            decap.DeCap_SampleQty = Convert.ToInt32(txtDecapSampleQTY.Text);
        else
            decap.DeCap_SampleQty = 0;

        //logo, casign, die, bond
        decap.MFGLogoPass = Convert.ToInt16(ddlDecapMfgLogo.SelectedValue);
        decap.MFGLogoComments = txtDecapMfgLogoComment.Text;

        decap.LogoMatchPass = Convert.ToInt16(ddlDecapCasing.SelectedValue);
        decap.LogoMatchComments = txtDecapCasingComment.Text;

        decap.DiePNMatchPass = Convert.ToInt16(ddlDcapPartDie.SelectedValue);
        decap.DiePNMatchComments = txtDcapPartDieComment.Text;

        decap.DieBondWiresPass = Convert.ToInt16(ddlDecapDieBond.SelectedValue);
        decap.DieBondWiresComments = txtDecapDieBondComment.Text;

    }
    private void SaveSolderability()
    {
        sol = ciq.Solderabilities.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (sol == null)
        {
            sol = new Solderability();
            sol.ValidationReportID = GCATID;
            ciq.Solderabilities.InsertOnSubmit(sol);
        }

        if (!string.IsNullOrEmpty(txtSolderSampleQTY.Text))
            sol.SolderabilitySampleQty = Convert.ToInt16(txtSolderSampleQTY.Text);
        else sol.SolderabilitySampleQty = 0;

        sol.SolderabilityServiceProviderRequired = cbxSolderSPRequired.Checked;
        if (cbxSolderSPRequired.Checked)
            sol.SolderabilitySPRequiredComments = txtSolderSPRequired.Text;
        else
            sol.SolderabilitySPRequiredComments = null;

        sol.PackageMountTypeSolderability = ddlSolderPackageMountType.SelectedValue;

        sol.RoHS = Convert.ToInt16(ddlSolderRohs.SelectedValue);
        sol.RoHSComments = txtSolderRohsComment.Text;

        sol.SolderWettingAcceptable = Convert.ToInt16(ddlSolderWetting.SelectedValue);
        sol.SolderWettingComments = txtSolderWettingComment.Text;

        sol.VoidFree = Convert.ToInt16(ddlSolderVoid.SelectedValue);
        sol.VoidFreeComments = txtSolderVoidComment.Text;

        sol.ContaminantFree = Convert.ToInt16(ddlSolderCont.SelectedValue);
        sol.ContaminantFreeComments = txtSolderContComment.Text;


    }
    private void SaveEprom()
    {
        ep = ciq.EPROMs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (ep == null)
        {
            ep = new EPROM();
            ep.ValidationReportID = GCATID;
            ciq.EPROMs.InsertOnSubmit(ep);
        }

        if (!string.IsNullOrEmpty(txtEpromSampleQTY.Text))
            ep.EPROMSampleQty = Convert.ToInt16(txtEpromSampleQTY.Text);
        else
            ep.EPROMSampleQty = 0;

        ep.EPROMServiceProviderRequired = cbxEpromSPRequired.Checked;
        if (cbxEpromSPRequired.Checked)
            ep.EPROMSPRequiredComments = txtEpromSPRequired.Text;
        else
            ep.EPROMSPRequiredComments = null;

        ep.PackageMountTypeEPROM = ddlEpromPackageMount.SelectedValue;

        ep.ChecksumAnalysis = Convert.ToInt16(ddlEpromChecksum.SelectedValue);
        ep.ChecksumComment = txtEpromChecksumComment.Text;

        //ep.ProgrammingRequired = Convert.ToInt16(ddlEpromProgramming.SelectedValue);
        //ep.ProgrammingRequiredComment = txtEpromProgrammingComment.Text;
        switch (cbxEpromProgrammingRequired.Checked)
        {
            case true:
                {
                    ep.ProgrammingRequired = 1;//Required
                    ep.VerifyProgramLoad = Convert.ToInt16(ddlEpromVerify.SelectedValue);
                    ep.ProgramLoadComment = txtEpromVerifyComment.Text;
                    break;
                }
            case false:
                {
                    ep.ProgrammingRequired = 2;//Not Required
                    ep.VerifyProgramLoad = null;
                    ep.ProgramLoadComment = null;
                    break;
                }
        }
        //throw new NotImplementedException();
    }

    private void SaveSentryVI()
    {
        sv = ciq.SentryVIs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (sv != null && !cbxTestTypeSentryVI.Checked)
        {
            ciq.SentryVIs.DeleteOnSubmit(sv);
            return;
        }
        if (sv == null)
        {
            sv = new SentryVI();
            sv.ValidationReportID = GCATID;
            ciq.SentryVIs.InsertOnSubmit(sv);
        }

        if (!string.IsNullOrEmpty(txtSentrySampleQTY.Text))
            sv.SentryVISampleQty = Convert.ToInt16(txtSentrySampleQTY.Text);
        else
            sv.SentryVISampleQty = 0;

        sv.PackageMountTypeSentryVI = ddlSentryPackageMountType.SelectedValue;

        sv.ServiceProviderRequiredSentryVI = cbxSentrySPRequired.Checked;
        if (cbxSentrySPRequired.Checked)
            sv.SentryVISPRequiredComments = txtSentrySPRequired.Text;
        else
            sv.SentryVISPRequiredComments = null;

        sv.SignaturesMatch = Convert.ToInt16(ddlSentrySignaturesMatch.SelectedValue);
        sv.SignatureMatchComments = txtSentrySignaturesMatchComment.Text;

        sv.Open_Shorts_Detection = Convert.ToInt16(ddlSentryOpenShorts.SelectedValue);
        sv.OpenShortsComments = txtSentryOpenShortsComment.Text;

        sv.SignatureAnalysisPass = Convert.ToInt16(ddlSentryPassedAllSignatureAnalysis.SelectedValue);
        sv.SignatureAnalysisComments = txtSentryPassedAllSignatureAnalysisComment.Text;

        //sv.SignatureReportRequired = Convert.ToInt16(ddlSentrySignatureReportRequired.SelectedValue);
        //sv.SignatureReportComments = txtSentrySignatureReportRequiredComment.Text;

        sv.SentryOverAllResult = Convert.ToInt16(ddlSentryOverallResult.SelectedValue);


    }
    private void SaveXRFAnalysis()
    {
        xrf = ciq.XRFs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (xrf == null)
        {

            xrf = new XRF();
            xrf.ValidationReportID = GCATID;
            ciq.XRFs.InsertOnSubmit(xrf);
        }

        if (!string.IsNullOrEmpty(txtXRFSampleQTY.Text))
            xrf.XRFSampleQty = Convert.ToInt16(txtXRFSampleQTY.Text);
        else
            xrf.XRFSampleQty = 0;

        xrf.XRFServiceProviderRequired = cbxXRFSPRequired.Checked;
        if (cbxXRFSPRequired.Checked)
            xrf.XRFSPRequiredComments = txtXRFSPRequired.Text;
        else
            xrf.XRFSPRequiredComments = null;

        xrf.PackageMountTypeXRF = ddlXRFPackageType.SelectedValue;

        xrf.LeadMaterialsMatch = Convert.ToInt16(ddlXRFLeadMaterialsMatch.SelectedValue);
        xrf.LeadMaterialsMatchComments = txtXRFLeadMaterialsMatchComment.Text;

        xrf.ForeignMaterials = Convert.ToInt16(ddlXRFForeignMaterials.SelectedValue);
        xrf.ForeignMaterialsComments = txtXRFForeignMaterialsComment.Text;

        //throw new NotImplementedException();
    }



    private void SaveBasicElectrical()
    {
        be = ciq.BasicElectricals.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (be == null)
        {
            be = new BasicElectrical();
            be.timestamp = DateTime.Now;
            be.ValidationReportID = GCATID;
            ciq.BasicElectricals.InsertOnSubmit(be);
        }

        if (!string.IsNullOrEmpty(txtBasicElectricalSampleQTY.Text))
            be.SampleQty = Convert.ToInt16(txtBasicElectricalSampleQTY.Text);
        else
            be.SampleQty = 0;

        be.Test1 = txtBasicElectricalTest1.Text;
        be.Cond1 = txtBasicElectricalCondition1.Text;
        //Result
        be.Actual1 = ddlBasicElectricalResult.SelectedValue;
        be.notes = txtBasicElectricalNotes.Text;
    }


    private void SaveExtendedTesting()
    {
        et = ciq.ExtendedTestings.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (et == null)
        {
            et = new ExtendedTesting();
            et.timestamp = DateTime.Now;
            et.ValidationReportID = GCATID;
            ciq.ExtendedTestings.InsertOnSubmit(et);
        }

        if (!string.IsNullOrEmpty(txtExtendedTestingSampleQTY.Text))
            et.SampleQty = Convert.ToInt16(txtExtendedTestingSampleQTY.Text);
        else
            et.SampleQty = 0;

        //Category
        et.ExtCategory = ddlExtCategory.SelectedValue;
        if (ddlExtCategory.SelectedValue == "Other")
            et.ExtCategoryOther = txtExtCategoryOther.Text;
        else
            et.ExtCategoryOther = "";
        //Result
        et.Actual1 = ddlExtTestingResult.SelectedValue;
        et.Cond1 = txtExtCondition1.Text;
        et.notes = txtExtNotes.Text;

    }

    private void SaveMoistureBaking()
    {
        mb = ciq.MoistureBakings.Where(w => w.validationreportid == GCATID).SingleOrDefault();
        if (mb == null)
        {
            mb = new MoistureBaking();
            mb.date_created = DateTime.Now;
            mb.validationreportid = GCATID;
            ciq.MoistureBakings.InsertOnSubmit(mb);
        }


        if (!string.IsNullOrEmpty(txtMoistureBakingSampleQTY.Text))
            mb.total_quantity = Convert.ToInt16(txtMoistureBakingSampleQTY.Text);
        else
            mb.total_quantity = 0;


        if (!string.IsNullOrEmpty(txtMoistureBakingProcess.Text))
            mb.process = txtMoistureBakingProcess.Text;

        if (!string.IsNullOrEmpty(txtMoistureBakingDuration.Text))
            mb.bake_duration = txtMoistureBakingDuration.Text;

        if (!string.IsNullOrEmpty(txtMoistureBakingMSL.Text))
            mb.msl = txtMoistureBakingMSL.Text;

        if (!string.IsNullOrEmpty(txtMoistureBakingTemp.Text))
            mb.bake_temperature = txtMoistureBakingTemp.Text;

        if (!string.IsNullOrEmpty(txtMoistureBakingProcess.Text))
            mb.process = txtMoistureBakingProcess.Text;

        string mbResult = ddlMoistureBakingResult.SelectedValue;
        string mbResultComment = txtMoistureBakingComment.Text;
        if (!string.IsNullOrEmpty(mbResult))
            mb.result = mbResult;
        if (!string.IsNullOrEmpty(mbResultComment))
            mb.result_comment = mbResultComment;


    }


    private void SaveSummary()
    {
        sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (sum == null)
        {
            sum = new Summary_new();
            sum.ValidationReportID = GCATID;
            sum.date_created = DateTime.Now;
            ciq.Summary_news.InsertOnSubmit(sum);
        }
        //External Visual
        sum.ExtVisLogosPartsResult = lblSumExternalVisualLogosParts.Text;
        sum.ExtVisLogosPartsComment = txtSumExternalVisualLogosPartsComment.Text;
        sum.ExtVisDimensionalResult = lblSumExternalVisualDim.Text;
        sum.ExtVisDimensionalComment = txtSumExternalVisualDimComment.Text;
        sum.ExtVisLeadPkgResult = lblSumExternalVisualLeadPkg.Text;
        sum.ExtVisLeadPkgComment = txtSumExternalVisualLeadPkgComment.Text;
        sum.ExtVisSurfaceAltResult = lblSumExternalVisualAlteration.Text;
        sum.ExtVisSurfaceAltComment = txtSumExternalVisualAlterationComment.Text;
        //MarkingPermenancy        
        sum.MarkingAcetoneResult = lblSumMarkingAcetone.Text;
        sum.MarkingAcetoneComment = txtSumMarkingAcetoneComment.Text;
        sum.MarkingScrapeResult = lblSumMarkingScrape.Text;
        sum.MarkingScrapeComment = txtSumMarkingScrapeComment.Text;
        sum.MarkingAlterationResult = lblSumMarkingAlteration.Text;
        sum.MarkingAlterationComment = txtSumMarkingAlterationComment.Text;
        //Xray
        sum.XrayDieWireResult = lblSumXrayUniform.Text;
        sum.XrayDieWireComment = txtSumXrayUniformComment.Text;
        //Heated Solvents
        sum.HeatedMethResult = lblSumHeatedSolventsMeth.Text;
        sum.HeatedMethComment = txtHeatedMethComment.Text;
        sum.HeatedDynaResult = lblSumHeatedSolventsDyna.Text;
        sum.HeatedDynaComment = txtSumHeatedSolventsDynaComment.Text;
        sum.HeatedSecondaryResult = lblSumHeatedSolventsSecondary.Text;
        sum.HeatedSecondaryComment = txtSumHeatedSolventsSecondaryComment.Text;
        //Decap
        sum.DecapWireBondResult = lblSumDecapWireBondIntegrity.Text;
        sum.DecapWireBondComment = txtSumDecapWireBondIntegrityComment.Text;
        sum.DecapDieMarkingResult = lblSumDecapDieMarking.Text;
        sum.DecapDieMarkingComment = txtSumDecapDieMarkingComment.Text;
        //XRF
        sum.XRFLeadMatsResult = lblSumXRFLeadsMatch.Text;
        sum.XRFLeadMatsComment = txtSumXRFLeadsMatchComment.Text;
        sum.XRFForeignResult = lblSumXRFForeignMat.Text;
        sum.XRFForeignComment = txtSumXRFForeignMatComment.Text;
        //Solderability
        sum.SolPassedResult = lblSumSolderPassSolderTesting.Text;
        sum.SolPassedComment = txtSumSolderPassSolderTestingComment.Text;
        sum.SolWettingResult = lblSumSolderWetting.Text;
        sum.SolWettingComment = txtSumSolderWettingComment.Text;
        //Basic Electrical
        sum.BasicElectricalResult = lblSumBasicElectrical.Text;
        sum.BasicElectricalComment = txtSumBasicElectricalComment.Text;
        //Sentry VI
        sum.SentryVIResult = lblSumElectricalVISignature.Text;
        sum.SentryVIComment = txtSumElectricalVISignatureComment.Text;
        //Extended Testing
        sum.ExtendedTestingResult = lblSumExtendedTesting.Text;
        sum.ExtendedTestingComment = txtSumExtendedTestingComment.Text;

        //EPROM
        sum.EPROMChecksumResult = lblSumEPROMChecksum.Text;
        sum.EPROMCheckSumComment = txtSumEPROMChecksumComment.Text;
        sum.EPROMProgramValidationResult = lblSumEPROMValidation.Text;
        sum.EPROMProgramValidationComment = txtEpromVerifyComment.Text;

        //Moisture/Baking        
        sum.MoistureBakingResult = lblSumMoistureBakingResult.Text;
        sum.MoistureBakingComment = txtSumMoistureBakingComment.Text;

        //may need to make some new columns to hold this data, since the andwers are programatically derived, and may not map to an existing summary field.          
    }

    protected void lbMPNSearch_Click(object sender, EventArgs e)
    {
        pnlMPNGrid.Visible = true;
        //divCreateManufactuerPart.Visible = false;
        LoadManufacturerPartGrid();
    }

    protected void btnSelectMPN_Click(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        GridViewRow row = btn.NamingContainer as GridViewRow;
        string MPN = tools.TrimAllWhiteSpace(row.Cells[1].Text);
        MPNID = Convert.ToInt32(gvMPNSelection.DataKeys[row.RowIndex].Values[0]);
        //Update the Validation Report MPNID on new part selection.
        if (vr == null)
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (vr != null)
        {
            vr.MPNID = MPNID;
            vr.MPN = MPN;
        }
        ciq.SubmitChanges();
        GetManufacturerPart(MPNID);
        ShowMainGCATForm();
    }

    private void GetManufacturerPart(int mpnID = 0)
    {

        if (mpnID == 0)
        {
            mp = new ManufacturerPart();
            mp.date_created = DateTime.Now;
            mp.MPN = txtMPNSearch.Text;
            mp.Manufacturer = txtHeaderMFG.Text.Trim().ToUpper();
            ciq.ManufacturerParts.InsertOnSubmit(mp);
            ciq.SubmitChanges();
        }
        else
            mp = ciq.ManufacturerParts.Where(w => w.MPNID == mpnID).FirstOrDefault();

        //Set the MPN data.
        MPN = mp.MPN;
        MFG = mp.Manufacturer;
        MPNID = mp.MPNID;
        hfCastellation.Value = mp.Lead_PinCount;


    }

    protected void ShowMainGCATForm()
    {
        if (mp != null)
        {
            //Hide Header MPN Search
            pnlManufacturerPartSearch.Visible = false;
            //Hide the MPN GridView Panel
            pnlMPNGrid.Visible = false;
            //Show the Main Panel
            pnlGCATMain.Visible = true;
            //Show the HeaderDetails Panel        
            pnlHeaderData.Visible = true;
            //Set Textbox to selected Part Number 
            txtMPNSearch.Text = MPN;
            lblHeaderMPN.Text = "MPN: " + MPN;
            lblHeaderMFG.Text = "MFG: " + MFG;
            //Update the Component Tab with these Details? // NO, only after save.
            //Bind the Customer DDL
            BindCustomerDDL();
            //Show Save Button          
            //LoadPageControls();
            if (GCATID > 0)  //         
                vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();//Update      
            else
                CreateGCAT();
            //LoadAll();

        }
    }

    private void CreateGCAT()
    {
        vr = new ValidationReport();//insert
        ciq.ValidationReports.InsertOnSubmit(vr);
        vr.date_created = DateTime.Now;
        vr.ReportDate = DateTime.Today;
        vr.MPN = MPN;
        vr.MPNID = MPNID;
        vr.created_by = System.Web.Security.Membership.GetUser().UserName;
        ciq.SubmitChanges();
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        Response.Redirect(path + "?id=" + vr.ValidationReportID, false);
        //GetGCATID();
    }

    protected void lbCreateManufacturerPart_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtMPNSearch.Text))
                throw new Exception("Please provide a part number.");
            CreateManufactuerPart();
            ShowMainGCATForm();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }


    }
    private void CreateManufactuerPart()
    {
        GetManufacturerPart();
    }

    protected void lbSaveComponentIdentity_Click(object sender, EventArgs e)
    {

        try
        {
            SaveComponent();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Component Identity saved successfully.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveHeader_Click(object sender, EventArgs e)
    {
        try
        {
            SaveHeader();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Header  saved successfully.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);

        }

    }
    protected void lbSaveRequired_Click(object sender, EventArgs e)
    {
        try
        {
            SaveRequired();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Required Tests saved successfully.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);

        }

    }

    protected void lbSaveVisualComponent_Click(object sender, EventArgs e)
    {
        try
        {
            SaveVisualComponent();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            if (IsPostBack)
            {//Necessary to re-hide DDL's on postback / save
             //ScriptManager.RegisterClientScriptBlock(Page, GetType(), "handleVisualComponentPackageConnectionType", "$(document).ready(handleVisualComponentPackageConnectionType());", true);
             //ScriptManager.RegisterClientScriptBlock(Page, GetType(), "HandleUI", "$(document).ready(HandleUI());", true);
            }
            tools.HandleResult("success", "Visual Component saved successfully.");

        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveVisualSurface_Click(object sender, EventArgs e)
    {
        try
        {
            SaveVisualSurface();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Visual Surface saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveMarkingPerm_Click(object sender, EventArgs e)
    {
        try
        {
            SaveMarking();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Marking Permenancy saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail",ex.Message);
        }

    }

    protected void lbSaveXray_Click(object sender, EventArgs e)
    {
        try
        {
            SaveXray();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Xray saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveDecap_Click(object sender, EventArgs e)
    {
        try
        {
            SaveDecap();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Decap saved successfully.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            //tools.HandleResultJS(ex.Message, true, Page);
        }

    }

    protected void lbSaveHeatedSolvents_Click(object sender, EventArgs e)
    {
        try
        {
            SaveHeatedSolvents();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Heated Solvents saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveSolder_Click(object sender, EventArgs e)
    {
        try
        {
            SaveSolderability();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Solderability saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveEprom_Click(object sender, EventArgs e)
    {
        try
        {
            SaveEprom();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            {//Necessary to re-hide DDL's on postback / save
             //ScriptManager.RegisterClientScriptBlock(Page, GetType(), "handleEpromProgrammingRequired", "$(document).ready(cbxShowHideControl('#MainContent_cbxEpromProgrammingRequired', 'divEpromVerify'));", true);
            }
            tools.HandleResult("success", "EPROM saved successfully.");

        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }


    protected void lbSaveBasicElectrical_Click(object sender, EventArgs e)
    {
        try
        {
            SaveBasicElectrical();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            if (Page.IsPostBack)
            {//Necessary to re-hide DDL's on postback / save
             //ScriptManager.RegisterClientScriptBlock(Page, GetType(), "handleExtendTestingOther", "$(document).ready(ddlShowHideText('#MainContent_ddlExtCategory', 'txtExtCategoryOther', 'Other'));", true);
            }
            tools.HandleResult("success", "Basic Electrical saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }


    protected void lbSaveSentry_Click(object sender, EventArgs e)
    {
        try
        {
            SaveSentryVI();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Sentry VI saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbSaveXRF_Click(object sender, EventArgs e)
    {
        try
        {
            SaveXRFAnalysis();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "XRF saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }


    protected void lbSaveExtended_Click(object sender, EventArgs e)
    {
        try
        {
            SaveExtendedTesting();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Extended Testing saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }

    }


    protected void lbSaveMoistureBaking_Click(object sender, EventArgs e)
    {

        try
        {
            SaveMoistureBaking();
            SaveSummary();
            smb.SaveChanges();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Moisture / Baking saved successfully.");
        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.Message, true, Page);
            tools.HandleResult("fail", ex.Message);
        }
    }



    protected void lbSaveSummary_Click(object sender, EventArgs e)
    {
        try
        {
            SaveSummary();
            ciq.SubmitChanges();
            tools.HandleResult("success", "Summary saved successfully.");
        }
        catch (Exception ex)
        {

            tools.HandleResult("fail", ex.Message);
        }
    }





    protected void lbGetDataSheet_Click(object sender, EventArgs e)
    {

        try
        {
           
            DataSet ds = new DataSet();
            ds = SiliconExpertLogic.GetListPartSearchDataSetJSON_Synchronous(MPN);
            if (ds == null)
            {
                tools.HandleResultJS("No datasheets for for " + MPN, true, Page);
                return;
            }
            //dt = ds.Tables["PCNDto"];
            DataTable dt = ds.Tables["PartDto"];

            //gvDatasheets.DataSource = rdc.companies.Where(w => w.companyname == "KT Portal Test").Select(s => s.companyname);
            gvDatasheets.DataSource = dt;
            gvDatasheets.DataBind();

            // upDatasheets.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "openModal('mdlDatasheets');", true);

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    protected void lbDeleteAll_Click(object sender, EventArgs e)
    {
        try
        {
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
            if (vr != null)
                if (!vr.is_deleted)
                {
                    vr.is_deleted = true;
                    //Set is Imported = false, so if undeleted, images will get re-imported.
                    vr.is_imported = false;
                    ShowStatusDiv("danger", "deleted");
                }
            DeleteAllImages();
            ciq.SubmitChanges();
            //Loading all with the Updatepanels was too wierd without redirecting.  This is simple, and does the job, loads are pretty quick too.      
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri, false);
            tools.HandleResult("success", "All images deleted successfully.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail",ex.Message);
        }
    }
    protected void lbRestore_Click(object sender, EventArgs e)
    {
        try
        {
            vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
            if (vr != null)
                if (vr.is_deleted)
                {
                    vr.is_deleted = false;
                    divGcatStatus.Visible = false;

                }

            ciq.SubmitChanges();
            //Loading all with the Updatepanels was too wierd without redirecting.  This is simple, and does the job, loads are pretty quick too.       
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri, false);
            tools.HandleResult("success", "GCAT successfully restored.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    protected void ShowStatusDiv(string alertType, string statusMessage)
    {
        lblGcatStatus.Text = statusMessage;
        switch (alertType.ToLower())
        {
            case "danger":
                {
                    divGcatStatus.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    break;
                }
        }

        switch (statusMessage.ToLower())
        {
            case "deleted":
                {
                    lblGcatStatus.Text = "Status: Deleted";
                    break;
                }
            default:
                {
                    lblGcatStatus.Text = "Status: " + statusMessage;
                    break;
                }
        }
        divGcatStatus.Visible = true;
    }

    protected bool hasPermission(string type)
    {
        bool ret = false;
        string userName = System.Web.Security.Membership.GetUser().UserName;
        switch (type.ToLower())
        {
            case "save":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_admin") || Roles.IsUserInRole(userName, "sm_insp_user"))
                        ret = true;
                    break;
                }
            case "delete":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_admin") || Roles.IsUserInRole(userName, "sm_insp_user"))
                        ret = true;
                    break;
                }
            case "restore":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_admin"))
                        ret = true;
                    break;
                }
            case "useform":
                {
                    if (Roles.IsUserInRole(userName, "admin") || Roles.IsUserInRole(userName, "sm_insp_user"))
                        ret = true;
                    break;
                }

        }
        return ret;
    }

    private void GetAssociatedTests()
    {
        //This can probably be the place where I consolidate all future queries for individual test types into one place.
        if (GCATID == 0)
            return;
        decap = ciq.Decapsulations.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        hs = ciq.HeatedSolvents.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        mperm = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        be = ciq.BasicElectricals.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        sv = ciq.SentryVIs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        sol = ciq.Solderabilities.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        xray = ciq.XRays.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        xrf = ciq.XRFs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        ep = ciq.EPROMs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        et = ciq.ExtendedTestings.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        mb = ciq.MoistureBakings.Where(w => w.validationreportid == GCATID).SingleOrDefault();
        sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
    }



    protected void cbxTestType_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox cbx = (CheckBox)sender;
        if (cbx.Checked)
        {
            //addTest(cbx.ID);
        }
        else
            removeTest(cbx.ID);
        ScriptManager.RegisterClientScriptBlock(Page, GetType(), "GetRequiredKey", "$(document).ready(HandleUI());", true);

    }
    //private void createPDF()
    //{
    //    try
    //    {
    //        gcat.CreatePDF();

    //    }
    //    catch (Exception ex)
    //    {
    //        tools.HandleResult(Page, "fail", ex.Message);
    //    }

    //}
    protected void lbPDF_Click(object sender, EventArgs e)
    {
        //Calling this so that old reports get updated with the proper values.
        SaveAll();
        gcat = new SM_GCAT(GCATID);
        gcat.CreatePDF();
        //Reload the page to clear update progress.
    }

    //protected void lbDisbaleControls_Click(object sender, EventArgs e)
    //{
    //    string[] keepEnabled = new string[] { "lbPDF", "lbDisableControls", "lbReturn" }; // creates populated array of length 2
    //    tools.DisableAllControls(Master.FindControl("MainContent"), false);
    //    HideImageControls(Master.FindControl("MainContent"));
    //}

    private void HideImageControls(Control parentControl)
    {
        foreach (Control c in parentControl.Controls)
        {
            if (c is Panel)
            {
                Panel p = (Panel)c;
                if (p.CssClass == "hideable")
                    p.Visible = false;
            }
            //recursion
            if (c.Controls.Count > 0)
                HideImageControls(c);
        }
    }
    private void HideSaveControls(Control parentControl)
    {
        foreach (Control c in parentControl.Controls)
        {
            if (c is LinkButton)
            {
                string lbName = null;
                LinkButton l = (LinkButton)c;
                if (l.ID.Length >= 6)
                {
                    lbName = l.ID.Substring(0, 6);
                    if (lbName.ToLower() == "lbsave")
                        l.Visible = false;
                }

            }
            //recursion
            if (c.Controls.Count > 0)
                HideSaveControls(c);
        }
    }



    protected void lbChgangeMPN_Click(object sender, EventArgs e)
    {
        pnlGCATMain.Visible = false;
        pnlManufacturerPartSearch.Visible = true;
    }

    protected void lbComplete_Click(object sender, EventArgs e)
    {

        try
        {
            SaveAll();//We have validated all answered if we have reached this.

            bool alreadyCompleted = vr.is_completed ?? false;

            if (!alreadyCompleted)
                CompleteInspection();
            else
                UncompleteInspection();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void UncompleteInspection()
    {
        string message;
        vr.is_completed = false;
        vr.completed_by = null;
        vr.completed_date = null;
        lbComplete.CssClass = "btn btn-smc btn-smc-primary";
        message = "GCAT has been un-completed.";
        ciq.SubmitChanges();
        tools.HandleResult("success", message);
    }

    private void CompleteInspection()
    {
        string message;
        vr.is_completed = true;
        vr.completed_by = System.Web.Security.Membership.GetUser().UserName;
        vr.completed_date = DateTime.Now;
        lbComplete.CssClass = "btn btn-smc btn-smc-success";
        //send email       
        if (System.Web.Security.Membership.GetUser().UserName == "kevint")
            tools.SendMail("gcat_complete@sensiblemicro.com", "ktill@sensiblemicro.com", "GCAT# " + GCATID + " is complete.", "Click <a href=\"https://portal.sensiblemicro.com/secure_sm/gcat/gcat_testing.aspx?id=" + GCATID + "\">here</a> to review the report. ");
        else
            tools.SendMail("gcat_complete@sensiblemicro.com", "qc@sensiblemicro.com", "GCAT# " + GCATID + " is complete.", "Click <a href=\"https://portal.sensiblemicro.com/secure_sm/gcat/gcat_testing.aspx?id=" + GCATID + "\">here</a> to review the report. ");
        message = "GCAT has been completed, and notification email has been sent to internal users for review.";
        ciq.SubmitChanges();
        tools.HandleResult("success", message);
    }

    private void CloneReport()
    {
        //didn't find an easy way to clone validationreports and children (and they are not currently serializable, which might fix).
        //Will use the Save Routines to create cloning routines.
        //omit the ValidationReportIDs, they will get new ones.
        CloneValidationReport();
        //if (mp != null)   Should I clone a Manufacturer Part?  Don't Think So
        GetAssociatedTests();
        if (mperm != null)
            CloneMarkingPermenancy();
        if (xray != null)
            CloneXray();
        if (hs != null)
            CloneHeatedSolvent();
        if (decap != null)
            CloneDecap();
        if (sol != null)
            CloneSolderability();
        if (ep != null)
            CloneEPROM();
        if (sv != null)
            CloneSentry();
        if (xrf != null)
            CloneXRF();
        if (be != null)
            CloneBasicElectrical();
        if (et != null)
            CloneExtendedTesting();
        if (sum != null)
            CloneSummary();

        ciq.SubmitChanges();

    }
    private void CloneValidationReport()
    {
        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (vr == null)
            throw new Exception("Invalid ValidationReport");
        if (!ciq.ValidationReports.Where(w => w.demo_source_id == vr.ValidationReportID).Any())//If a demo for this report doesn't already exist, create a new one
        {
            vr2 = new ValidationReport();
            vr2.MPNID = vr.MPNID;
            vr2.CPNID = vr.CPNID;
            ciq.ValidationReports.InsertOnSubmit(vr2);
            ciq.SubmitChanges();//Submit Changes so I can get a ValidationReportID for vr2
        }
        else
            vr2 = ciq.ValidationReports.Where(w => (w.demo_source_id == vr.ValidationReportID) && (w.is_demo ?? false == true)).SingleOrDefault();
        if (vr2 == null)
            throw new Exception("Invalid demo ValidationReport");

        vr2.ReportDate = vr.ReportDate;


        vr2.Form = vr.Form;
        vr2.Rev = vr.Rev;
        vr2.MPN = vr.MPN;

        vr2.SalesOrderNumberOLD = vr.SalesOrderNumberOLD;
        //vr2.CustomerPO = vr.CustomerPO;
        vr2.AccountManagerID = vr.AccountManagerID;
        vr2.Image_Top = vr.Image_Top;
        vr2.Image_Bottom = vr.Image_Bottom;
        vr2.DateCode1 = vr.DateCode1;
        vr2.DateCode2 = vr.DateCode2;
        vr2.DateCode3 = vr.DateCode3;
        vr2.DateCode4 = vr.DateCode4;
        vr2.DateCode5 = vr.DateCode5;
        vr2.DateCode6 = vr.DateCode6;

        vr2.Quantity1 = vr.Quantity1;
        vr2.Quantity2 = vr.Quantity2;
        vr2.Quantity3 = vr.Quantity3;
        vr2.Quantity4 = vr.Quantity4;
        vr2.Quantity5 = vr.Quantity5;
        vr2.Quantity6 = vr.Quantity6;
        vr2.CustomerTypeA = vr.CustomerTypeA;
        vr2.CustomerTypeB = vr.CustomerTypeB;
        vr2.CustomerTypeC = vr.CustomerTypeC;
        vr2.CustomerTypeD = vr.CustomerTypeD;

        vr2.Test_StandardA = vr.Test_StandardA;
        vr2.Test_StandardB = vr.Test_StandardB;
        vr2.Test_StandardC = vr.Test_StandardC;
        vr2.Test_StandardD = vr.Test_StandardD;

        vr2.is_AS6081 = vr.is_AS6081;
        vr2.is_CCAP = vr.is_CCAP;

        vr2.Customer_RequirementA = vr.Customer_RequirementA;
        vr2.Customer_RequirementB = vr.Customer_RequirementB;
        vr2.Customer_RequirementC = vr.Customer_RequirementC;
        vr2.Customer_RequirementD = vr.Customer_RequirementD;

        vr2.Test_TypeA = vr.Test_TypeA;
        vr2.Test_TypeB = vr.Test_TypeB;
        vr2.Test_TypeC = vr.Test_TypeC;
        vr2.Test_TypeD = vr.Test_TypeD;
        vr2.Test_TypeE = vr.Test_TypeE;
        vr2.Test_TypeF = vr.Test_TypeF;
        vr2.Test_TypeG = vr.Test_TypeG;
        vr2.Test_TypeH = vr.Test_TypeH;
        vr2.Test_TypeI = vr.Test_TypeI;
        vr2.Test_TypeJ = vr.Test_TypeJ;

        vr2.New = vr.New;
        vr2.Used = vr.Used;
        vr2.Refurbished = vr.Refurbished;

        vr2.VCA_Image1 = vr.VCA_Image1;
        vr2.VCA_Image2 = vr.VCA_Image2;
        vr2.VCA_Image3 = vr.VCA_Image3;

        vr2.VisualComponentSampleQty = vr.VisualComponentSampleQty;

        vr2.Width_Actual = vr.Width_Actual;
        vr2.UnitWidth = vr.UnitWidth;
        vr2.Pass_W = vr.Pass_W;
        vr2.CommentsWidth = vr.CommentsWidth;

        vr2.Length_Actual = vr.Length_Actual;
        vr2.UnitLength = vr.UnitLength;
        vr2.Pass_L = vr.Pass_L;
        vr2.CommentsLength = vr.CommentsLength;

        vr2.Thickness_Actual = vr.Thickness_Actual;
        vr2.UnitThickness = vr.UnitThickness;
        vr2.Pass_T = vr.Pass_T;
        vr2.CommentsThickness = vr.CommentsThickness;

        vr2.ComponentType = vr.ComponentType;
        vr2.SolderBalls_Spheres = vr.SolderBalls_Spheres;
        vr2.Uniform_Concentricity = vr.Uniform_Concentricity;
        vr2.Crushed_Damages_Spheres = vr.Crushed_Damages_Spheres;
        vr2.CommentSolderBalls = vr.CommentSolderBalls;
        vr2.CommentUniformity = vr.CommentUniformity;
        vr2.CommentDamagedSpheres = vr.CommentDamagedSpheres;

        vr2.Lead_Count = vr.Lead_Count;
        vr2.Lead_PadCondition = vr.Lead_PadCondition;
        vr2.LeadCoplanarity = vr.LeadCoplanarity;
        vr2.Oxidation_Discoloration = vr.Oxidation_Discoloration;
        vr2.SignsExposedBaseMetal = vr.SignsExposedBaseMetal;
        vr2.CommentLeadCount = vr.CommentLeadCount;
        vr2.CommentLead_Pad = vr.CommentLead_Pad;
        vr2.CommentLeadCoplanarity = vr.CommentLeadCoplanarity;
        vr2.CommentO2Discoloration = vr.CommentO2Discoloration;
        vr2.CommentExpBaseMetal = vr.CommentExpBaseMetal;

        vr2.VSA_Image1 = vr.VSA_Image1;
        vr2.VSA_Image2 = vr.VSA_Image2;
        vr2.VSA_Image3 = vr.VSA_Image3;

        vr2.VisualSurfaceSampleQty = vr.VisualSurfaceSampleQty;

        vr2.Top_Pass = vr.Top_Pass;
        vr2.Bottom_Pass = vr.Bottom_Pass;
        vr2.Comments_Top = vr.Comments_Top;
        vr2.Comments_Bottom = vr.Comments_Bottom;
        vr2.LogosPartnumbersPass = vr.LogosPartnumbersPass;
        vr2.LogosPartnumbersComment = vr.LogosPartnumbersComment;

        //vr2.upsize_ts = vr.upsize_ts;
        vr2.Traceability = vr.Traceability;
        //vr2.SalesOrderNumber = vr.SalesOrderNumber;
        vr2.MPNID = vr.MPNID;
        vr2.EXTID = vr.EXTID;
        vr2.Manufacturer = vr.Manufacturer;
        vr2.DateCodeUnltd = vr.DateCodeUnltd;
        vr2.summaryNotes = vr.summaryNotes;
        vr2.orddet_line_fullpartnumber = vr.orddet_line_fullpartnumber;
        //vr2.orddet_line_uid = vr.orddet_line_uid;
        //vr2.orderid_sales = vr.orderid_sales;
        //vr2.orderid_purchase = vr.orderid_purchase;
        //vr2.agent_uid = vr.agent_uid;
        //vr2.agent_name = vr.agent_name;

        vr2.is_deleted = vr.is_deleted;
        vr2.is_imported = vr.is_imported;
        vr2.is_completed = vr.is_completed;
        vr2.completed_by = vr.completed_by;
        vr2.completed_date = vr.completed_date;
        vr2.is_approved = vr.is_approved;
        vr2.approved_by = vr.approved_by;
        vr2.approved_date = vr.approved_date;

        vr2.date_created = DateTime.Now;
        //vr2.is_imported = false; now that I am getting demo images based on source GCATID, don't need to force re-import
        vr2.is_demo = true;
        vr2.company_uid = "037ED306-8D90-42D6-AAAA-AD91B900F263";
        vr2.company_name = "Sensible Micro Corporation";
        vr2.CPN = "CUSTOMERINTERNAL-PART";
        vr2.CustomerPO = "1977-C3PO";
        vr2.SalesOrderNumber = "132645";
        vr2.agent_uid = "Demo";
        vr2.agent_name = "Inspection Team";
        vr2.orddet_line_uid = "Demo";
        vr2.orderid_sales = "Demo";
        vr2.orderid_purchase = "Demo";
        vr2.demo_source_id = vr.ValidationReportID;
    }
    private void CloneSummary()

    {
        Summary_new sum2;
        sum2 = ciq.Summary_news.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (sum2 == null)
        {
            sum2 = new Summary_new();
            sum2.ValidationReportID = vr2.ValidationReportID;
            ciq.Summary_news.InsertOnSubmit(sum2);
        }

        sum2.ExtVisLogosPartsResult = sum.ExtVisLogosPartsResult;
        sum2.ExtVisLogosPartsComment = sum.ExtVisLogosPartsComment;
        sum2.ExtVisDimensionalResult = sum.ExtVisDimensionalResult;
        sum2.ExtVisDimensionalComment = sum.ExtVisDimensionalComment;
        sum2.ExtVisLeadPkgResult = sum.ExtVisLeadPkgResult;
        sum2.ExtVisLeadPkgComment = sum.ExtVisLeadPkgComment;
        sum2.ExtVisSurfaceAltResult = sum.ExtVisSurfaceAltResult;
        sum2.ExtVisSurfaceAltComment = sum.ExtVisSurfaceAltComment;

        sum2.MarkingAcetoneResult = sum.MarkingAcetoneResult;
        sum2.MarkingAcetoneComment = sum.MarkingAcetoneComment;
        sum2.MarkingScrapeResult = sum.MarkingScrapeResult;
        sum2.MarkingScrapeComment = sum.MarkingScrapeComment;
        sum2.MarkingAlterationResult = sum.MarkingAlterationResult;
        sum2.MarkingAlterationComment = sum.MarkingAlterationComment;

        sum2.XrayDieWireResult = sum.XrayDieWireResult;
        sum2.XrayDieWireComment = sum.XrayDieWireComment;

        sum2.HeatedMethResult = sum.HeatedMethResult;
        sum2.HeatedMethComment = sum.HeatedMethComment;
        sum2.HeatedDynaResult = sum.HeatedDynaResult;
        sum2.HeatedDynaComment = sum.HeatedDynaComment;
        sum2.HeatedSecondaryResult = sum.HeatedSecondaryResult;
        sum2.HeatedSecondaryComment = sum.HeatedSecondaryComment;

        sum2.DecapWireBondResult = sum.DecapWireBondResult;
        sum2.DecapWireBondComment = sum.DecapWireBondComment;
        sum2.DecapDieMarkingResult = sum.DecapDieMarkingResult;
        sum2.DecapDieMarkingComment = sum.DecapDieMarkingComment;

        sum2.XRFLeadMatsResult = sum.XRFLeadMatsResult;
        sum2.XRFLeadMatsComment = sum.XRFLeadMatsComment;
        sum2.XRFForeignResult = sum.XRFForeignResult;
        sum2.XRFForeignComment = sum.XRFForeignComment;

        sum2.SolPassedResult = sum.SolPassedResult;
        sum2.SolPassedComment = sum.SolPassedComment;
        sum2.SolWettingResult = sum.SolWettingResult;
        sum2.SolWettingComment = sum.SolWettingComment;

        sum2.BasicElectricalResult = sum.BasicElectricalResult;
        sum2.BasicElectricalComment = sum.BasicElectricalComment;

        sum2.SentryVIResult = sum.SentryVIResult;
        sum2.SentryVIComment = sum.SentryVIComment;

        sum2.ExtendedTestingResult = sum.ExtendedTestingResult;
        sum2.ExtendedTestingComment = sum.ExtendedTestingComment;

        sum2.EPROMChecksumResult = sum.EPROMChecksumResult;
        sum2.EPROMCheckSumComment = sum.EPROMCheckSumComment;
        sum2.EPROMProgramValidationResult = sum.EPROMProgramValidationResult;
        sum2.EPROMProgramValidationComment = sum.EPROMProgramValidationComment;

        sum2.date_created = sum.date_created;
    }
    private void CloneExtendedTesting()
    {
        ExtendedTesting et2;
        et2 = ciq.ExtendedTestings.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (et2 == null)
        {
            et2 = new ExtendedTesting();
            et2.ValidationReportID = vr2.ValidationReportID;
            ciq.ExtendedTestings.InsertOnSubmit(et2);
        }

        et2.ExtCategory = et.ExtCategory;
        et2.ExtTitle = et.ExtTitle;
        et2.ExtImage = et.ExtImage;
        et2.SampleQty = et.SampleQty;
        et2.Test1 = et.Test1;
        et2.Cond1 = et.Cond1;
        et2.Res1 = et.Res1;
        et2.Actual1 = et.Actual1;
        et2.timestamp = et.timestamp;
        et2.notes = et.notes;
        et2.ExtCategoryOther = et.ExtCategoryOther;
        et2.ExtTestingResult = et.ExtTestingResult;
    }
    private void CloneBasicElectrical()
    {

        BasicElectrical be2;
        be2 = ciq.BasicElectricals.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (be2 == null)
        {
            be2 = new BasicElectrical();
            be2.ValidationReportID = vr2.ValidationReportID;
            ciq.BasicElectricals.InsertOnSubmit(be2);
        }




        be2.Category = be.Category;
        be2.Title = be.Title;
        be2.Image = be.Image;
        be2.SampleQty = be.SampleQty;
        be2.Test1 = be.Test1;
        be2.Cond1 = be.Cond1;
        be2.Res1 = be.Res1;
        be2.Actual1 = be.Actual1;
        be2.timestamp = be.timestamp;
        be2.notes = be.notes;
        be2.CategoryOther = be.CategoryOther;
        be2.TestingResult = be.TestingResult;

    }
    private void CloneXRF()
    {
        XRF xrf2;
        xrf2 = ciq.XRFs.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (xrf2 == null)
        {
            xrf2 = new XRF();
            xrf2.ValidationReportID = vr2.ValidationReportID;
            ciq.XRFs.InsertOnSubmit(xrf2);
        }
        xrf2.XRFSampleQty = xrf.XRFSampleQty;
        xrf2.PackageMountTypeXRF = xrf.PackageMountTypeXRF;
        xrf2.XRFServiceProviderRequired = xrf.XRFServiceProviderRequired;
        xrf2.XRFSPRequiredComments = xrf.XRFSPRequiredComments;
        xrf2.LeadMaterialsMatch = xrf.LeadMaterialsMatch;
        xrf2.LeadMaterialsMatchComments = xrf.LeadMaterialsMatchComments;
        xrf2.ForeignMaterials = xrf.ForeignMaterials;
        xrf2.ForeignMaterialsComments = xrf.ForeignMaterialsComments;
        //xrf2.upsize_ts = xrf.upsize_ts;
    }
    private void CloneSentry()
    {
        SentryVI sv2;
        sv2 = ciq.SentryVIs.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (sv2 == null)
        {
            sv2 = new SentryVI();
            sv2.ValidationReportID = vr2.ValidationReportID;
            ciq.SentryVIs.InsertOnSubmit(sv2);
        }

        sv2.SentryVISampleQty = sv.SentryVISampleQty;
        sv2.PackageMountTypeSentryVI = sv.PackageMountTypeSentryVI;
        sv2.ServiceProviderRequiredSentryVI = sv.ServiceProviderRequiredSentryVI;
        sv2.SentryVISPRequiredComments = sv.SentryVISPRequiredComments;
        sv2.SignaturesMatch = sv.SignaturesMatch;
        sv2.SignatureMatchComments = sv.SignatureMatchComments;
        sv2.Open_Shorts_Detection = sv.Open_Shorts_Detection;
        sv2.OpenShortsComments = sv.OpenShortsComments;
        sv2.SignatureAnalysisPass = sv.SignatureAnalysisPass;
        sv2.SignatureAnalysisComments = sv.SignatureAnalysisComments;
        sv2.SignatureReportRequired = sv.SignatureReportRequired;
        sv2.SignatureReportComments = sv.SignatureReportComments;
        sv2.SentryOverAllResult = sv.SentryOverAllResult;
        sv2.SentryICResults = sv.SentryICResults;
        //sv2.upsize_ts = sv.upsize_ts;
    }
    private void CloneEPROM()
    {
        EPROM ep2;
        ep2 = ciq.EPROMs.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (ep2 == null)
        {
            ep2 = new EPROM();
            ep2.ValidationReportID = vr2.ValidationReportID;
            ciq.EPROMs.InsertOnSubmit(ep2);
        }

        ep2.EPROMSampleQty = ep.EPROMSampleQty;
        ep2.PackageMountTypeEPROM = ep.PackageMountTypeEPROM;
        ep2.EPROMServiceProviderRequired = ep.EPROMServiceProviderRequired;
        ep2.EPROMSPRequiredComments = ep.EPROMSPRequiredComments;
        ep2.ChecksumAnalysis = ep.ChecksumAnalysis;
        ep2.ChecksumComment = ep.ChecksumComment;
        ep2.ProgrammingRequired = ep.ProgrammingRequired;
        ep2.ProgrammingRequiredComment = ep.ProgrammingRequiredComment;
        ep2.VerifyProgramLoad = ep.VerifyProgramLoad;
        ep2.ProgramLoadComment = ep.ProgramLoadComment;
        //ep2.upsize_ts = ep.upsize_ts;

    }
    private void CloneSolderability()
    {
        Solderability sol2;
        sol2 = ciq.Solderabilities.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (sol2 == null)
        {
            sol2 = new Solderability();
            sol2.ValidationReportID = vr2.ValidationReportID;
            ciq.Solderabilities.InsertOnSubmit(sol2);
        }
        sol2.SolderabilitySampleQty = sol.SolderabilitySampleQty;
        sol2.PackageMountTypeSolderability = sol.PackageMountTypeSolderability;
        sol2.SolderabilityServiceProviderRequired = sol.SolderabilityServiceProviderRequired;
        sol2.SolderabilitySPRequiredComments = sol.SolderabilitySPRequiredComments;
        sol2.RoHS = sol.RoHS;
        sol2.RoHSComments = sol.RoHSComments;
        sol2.SolderWettingAcceptable = sol.SolderWettingAcceptable;
        sol2.SolderWettingComments = sol.SolderWettingComments;
        sol2.VoidFree = sol.VoidFree;
        sol2.VoidFreeComments = sol.VoidFreeComments;
        sol2.ContaminantFree = sol.ContaminantFree;
        sol2.ContaminantFreeComments = sol.ContaminantFreeComments;
        sol2.SolderabilityImage1 = sol.SolderabilityImage1;
        sol2.SolderabilityImage2 = sol.SolderabilityImage2;
        sol2.SolderabilityImage3 = sol.SolderabilityImage3;

        //sol2.upsize_ts = sol.upsize_ts;
    }
    private void CloneDecap()
    {
        Decapsulation decap2;
        decap2 = ciq.Decapsulations.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (decap2 == null)
        {
            decap2 = new Decapsulation();
            decap2.ValidationReportID = vr2.ValidationReportID;
            ciq.Decapsulations.InsertOnSubmit(decap2);
        }

        decap2.DeCap_SampleQty = decap.DeCap_SampleQty;
        decap2.MFGLogoPass = decap.MFGLogoPass;
        decap2.MFGLogoComments = decap.MFGLogoComments;
        decap2.LogoMatchPass = decap.LogoMatchPass;
        decap2.LogoMatchComments = decap.LogoMatchComments;
        decap2.DiePNMatchPass = decap.DiePNMatchPass;
        decap2.DiePNMatchComments = decap.DiePNMatchComments;
        decap2.DieBondWiresPass = decap.DieBondWiresPass;
        decap2.DieBondWiresComments = decap.DieBondWiresComments;
        decap2.DeCapImage1 = decap.DeCapImage1;
        decap2.DeCapImage2 = decap.DeCapImage2;
        decap2.DeCapImage3 = decap.DeCapImage3;
        decap2.DeCapImage4 = decap.DeCapImage4;

        //decap2.upsize_ts = decap.upsize_ts;
    }
    private void CloneHeatedSolvent()
    {
        HeatedSolvent hs2;
        hs2 = ciq.HeatedSolvents.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (hs2 == null)
        {
            hs2 = new HeatedSolvent();
            hs2.ValidationReportID = vr2.ValidationReportID;
            ciq.HeatedSolvents.InsertOnSubmit(hs2);
        }


        hs2.HeatedSolventSampleQty = hs.HeatedSolventSampleQty;
        hs2.Methyl_PyrrolidinonePass = hs.Methyl_PyrrolidinonePass;
        hs2.Methyl_PyrrolidinoneComments = hs.Methyl_PyrrolidinoneComments;
        hs2.DynasolvePass = hs.DynasolvePass;
        hs2.DynasolveComments = hs.DynasolveComments;
        hs2.SecondaryCoatingPass = hs.SecondaryCoatingPass;
        hs2.SecondaryCoatingComments = hs.SecondaryCoatingComments;

        hs2.Heated_SolventsImage1 = hs.Heated_SolventsImage1;
        hs2.Heated_SolventsImage2 = hs.Heated_SolventsImage2;
        hs2.Heated_SolventsImage3 = hs.Heated_SolventsImage3;

        hs2.date_created = hs.date_created;
    }
    private void CloneMarkingPermenancy()
    {
        MarkingPermanency mperm2;
        mperm2 = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (mperm2 == null)
        {
            mperm2 = new MarkingPermanency();
            mperm2.ValidationReportID = vr2.ValidationReportID;
            ciq.MarkingPermanencies.InsertOnSubmit(mperm2);
        }

        mperm2.MarkingSample_Qty = mperm.MarkingSample_Qty;
        mperm2.AcetoneTestPass = mperm.AcetoneTestPass;
        mperm2.Comments_Acetone = mperm.Comments_Acetone;
        mperm2.Alcohol_MineralSpiritsTestPass = mperm.Alcohol_MineralSpiritsTestPass;
        mperm2.Comments_Alcohol = mperm.Comments_Alcohol;
        mperm2.ScrapeTestPass = mperm.ScrapeTestPass;
        mperm2.Comments_ScrapeTest = mperm.Comments_ScrapeTest;
        mperm2.SecondaryCoatingTestPass = mperm.SecondaryCoatingTestPass;
        mperm2.Comments_SecondaryCoating = mperm.Comments_SecondaryCoating;
        mperm2.PartNumber_MarkingTestPass = mperm.PartNumber_MarkingTestPass;
        mperm2.Comments_PartNumber = mperm.Comments_PartNumber;
        mperm2.CaseMarking_LogoTestPass = mperm.CaseMarking_LogoTestPass;
        mperm2.Comments_CaseMarking = mperm.Comments_CaseMarking;
        mperm2.Marking_Image1 = mperm.Marking_Image1;
        mperm2.Marking_Image2 = mperm.Marking_Image2;
        mperm2.Marking_Image3 = mperm.Marking_Image3;
    }
    private void CloneXray()
    {
        XRay xray2;
        xray2 = ciq.XRays.Where(w => w.ValidationReportID == vr2.ValidationReportID).SingleOrDefault();
        if (xray2 == null)
        {
            xray2 = new XRay();
            xray2.ValidationReportID = vr2.ValidationReportID;
            ciq.XRays.InsertOnSubmit(xray2);
        }


        xray2.XRaySampleQty = xray.XRaySampleQty;
        xray2.DiePresentPass = xray.DiePresentPass;
        xray2.DiePresentComments = xray.DiePresentComments;
        xray2.BondWiresPresentPass = xray.BondWiresPresentPass;
        xray2.BondWiresPresentComment = xray.BondWiresPresentComment;
        xray2.Damaged_MissingWiresPass = xray.Damaged_MissingWiresPass;
        xray2.Damaged_MissingWiresComment = xray.Damaged_MissingWiresComment;
        xray2.DieLeadUniformityPass = xray.DieLeadUniformityPass;
        xray2.DieLeadUniformityComment = xray.DieLeadUniformityComment;
        xray2.DieFRameSize_LocationPass = xray.DieFRameSize_LocationPass;
        xray2.DieFrameSize_LocationComment = xray.DieFrameSize_LocationComment;
        xray2.XRay_Image1 = xray.XRay_Image1;
        xray2.XRay_Image2 = xray.XRay_Image2;
        xray2.XRay_Image3 = xray.XRay_Image3;
        xray2.XRay_Image4 = xray.XRay_Image4;




    }
    protected void lbSetDemoGCAT_Click(object sender, EventArgs e)
    {
        try
        {
            CloneReport();
            tools.HandleResult("success", "You have successfully created a demo version of this report.  The ID is: " + vr2.ValidationReportID);
            //tools.HandleResult(Page,  "success");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            //tools.HandleResult(Page,  "fail");
        }

    }


    protected void lbApprove_Click(object sender, EventArgs e)
    {
        ApproveGCAT();
    }

    private void ApproveGCAT()
    {
        string message;
        //We have validated all answered if we have reached this.
        try
        {
            SaveAll();
            if ((!vr.is_completed ?? false) && (!vr.is_approved ?? false)) //Don't allow approval if GCAT Is not yet approved, or complete.  If Already approved, need to be able to un-approve
            {
                throw new Exception("This GCAT is not yet complete, and cannot be approved for the Portal.  Please see inspection team for completion status.");
            }
            if (!vr.is_approved ?? false)
            {
                vr.is_approved = true;
                vr.approved_by = System.Web.Security.Membership.GetUser().UserName;
                vr.approved_date = DateTime.Now;
                lbApprove.CssClass = "btn btn-smc btn-smc-success";
                message = "GCAT has been approved and is now visible to customers in the Portal.";
            }
            else
            {
                vr.is_approved = false;
                lbApprove.CssClass = "btn btn-smc btn-smc-primary";
                message = "GCAT has been un-approved, and is no longer visible to customers in the Portal.";
            }
            ciq.SubmitChanges();
            tools.HandleResult("success", message);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }




    //protected void addExtendedImage_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int qty = 1;
    //        if (!int.TryParse(txtExtendedImageAddQty.Text, out qty))
    //            throw new Exception("Please provide a valid number of images to add.");

    //        AddImageExtendedImageControls(qty);
    //        //Reload the page after adding the controls.  The Onload will add them to the page, then users can set the image
    //        var currentUrl = Request.Url.ToString();
    //        Response.Redirect(currentUrl,false);

    //    }
    //    catch (Exception ex)
    //    {
    //        tools.HandleResult("fail", ex.Message);
    //    }
    //}

    private void AddImageExtendedImageControls(int qty)
    {
        //Add one to the highest sectionID index in the database.
        int nextImageIndex = SM_GCAT.GetNextGcatSectionImageIndex(smb, SM_Enums.InspectionType.gcat, GCATID, "ExtendedImagery");
        int totalNewQty = qty + nextImageIndex;
        for (int i = nextImageIndex; i < totalNewQty; i++)
        {


            //Add image to Database, no blob.  On Postback, controls should render.
            insp_images ii = new insp_images();
            ii.insp_id = GCATID;
            ii.insp_type = SM_Enums.InspectionType.gcat.ToString();
            ii.insp_section_id = "ExtendedImagery" + i.ToString();
            ii.img_name = "";
            ii.img_type = "";
            string image_id = Guid.NewGuid().ToString();
            ii.insp_image_id = image_id;
            //Temp / Empty Blob, will set after image manager added to page.
            byte[] tempBlob = new byte[0];
            ii.img_blob = tempBlob;
            ii.img_description = "";
            smb.insp_images.Add(ii);

        }

        smb.SaveChanges();
    }
}


