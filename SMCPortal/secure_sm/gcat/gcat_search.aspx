﻿<%@ Page Title="GCAT Search" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="gcat_search.aspx.cs" Inherits="secure_sm_gcat_gcat_search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h2>GCAT Inspection System</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Search Controls--%>
    <div class="container">
        <div class="row">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="lbSearch">
                <div id="inspectionSearchControls" style="padding-bottom: 10px;">
                    <div class="form-inline">
                        <div class="form-group" style="padding-right: 20px;">
                            <asp:LinkButton ID="lbNewGcat" CssClass="btn btn-smc btn-smc-primary" runat="server" Text="Search" OnClick="lbNewGcat_Click">
                             New GCAT    
                            </asp:LinkButton>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtSearch" runat="server" Placeholder="ID, SO, Cust PO, SMC PO" CssClass="form-control"></asp:TextBox>
                            <asp:LinkButton ID="lbSearch" CssClass="btn btn-success btn-med" runat="server" Text="Search" OnClick="lbSearch_Click" OnClientClick="ShowUpdateProgress();">
                                <span aria-hidden="true" class="fa fa-search" ></span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbClear" CssClass="btn btn-danger btn-med" runat="server" Text="Search" OnClick="lbClear_Click">
                                 <span aria-hidden="true" class="fa fa-times" ></span>
                            </asp:LinkButton>
                        </div> 
                        <div style="margin-left:15px"></div>
                        <div class="form-check form-check-inline">
                            <asp:CheckBox ID="cbxShowDeleted" runat="server" OnCheckedChanged="cbxShowDeleted_CheckedChanged" AutoPostBack="true" />
                            <label class="form-check-label" for="cbxShowDeleted">Show Deleted</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <asp:CheckBox ID="cbxShowsamples" runat="server" OnCheckedChanged="cbxShowsamples_CheckedChanged" AutoPostBack="true" />
                            <label class="form-check-label" for="cbxShowsamples">Show sample reports</label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="row">
            <div id="gcatSearchResults" style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">

                <asp:GridView ID="gvGcat" runat="server" CssClass="table table-hover table-striped gvstyling stacktable" GridLines="None" AllowPaging="True" PageSize="7" AllowSorting="True" CellPadding="5" EmptyDataText="No Records Found" AutoGenerateColumns="False" DataKeyNames="ValidationReportID" PagerStyle-Wrap="False" PagerStyle-BorderStyle="None" PagerSettings-Mode="Numeric" OnSelectedIndexChanged="gvGcat_SelectedIndexChanged" OnPageIndexChanging="gvGcat_PageIndexChanging" OnRowDataBound="gvGcat_RowDataBound">
                    <EmptyDataTemplate>
                        <div class="alert alert-warning">No GCATs found that match search criteria.</div>
                    </EmptyDataTemplate>
                    <PagerStyle CssClass="pagination-ys" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <span style="display: table-cell; vertical-align: middle; float: none; width: 100%;">
                                    <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="False" Text="Select" CssClass="btn btn-default btn-sm btn-block" OnClick="lbSelect_Click" OnClientClick="ShowUpdateProgress();"></asp:LinkButton>
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ValidationReportID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ValidationReportID" />
                        <asp:BoundField DataField="ReportDate" HeaderText="Date" SortExpression="ReportDate" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="MPN" HeaderText="Part Number" SortExpression="MPN" />
                        <asp:BoundField DataField="company_name" HeaderText="Company" SortExpression="company_name" />
                        <asp:BoundField DataField="SalesOrderNumber" HeaderText="Sales Order" SortExpression="SalesOrderNumber" />
                        <asp:BoundField DataField="CustomerPO" HeaderText="Customer PO" SortExpression="CustomerPO" />
                       <asp:BoundField DataField="created_by" HeaderText="Inspector" SortExpression="created_by" />
                    </Columns>
                    <PagerStyle Wrap="False" BorderStyle="None"></PagerStyle>
                    <SortedAscendingHeaderStyle CssClass="sortasc" />
                    <SortedDescendingHeaderStyle CssClass="sortdesc" />
                </asp:GridView>
            </div>
        </div>
        <label>
            <asp:Label ID="lblTotalResults" runat="server" Visible="false"></asp:Label></label>
    </div>
    <%--End Search Controls--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

