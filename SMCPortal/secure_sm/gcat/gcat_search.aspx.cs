﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;

public partial class secure_sm_gcat_gcat_search : System.Web.UI.Page
{
    gcatDataContext ciq = new gcatDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            LoadGCATGrid(cbxShowDeleted.Checked, cbxShowsamples.Checked);
    }

    private void LoadGCATGrid(bool showDeleted, bool showSamples = false)
    {
        List<ValidationReport> gcatList = null;
        string searchTerm = txtSearch.Text.Trim().ToUpper();

        int value;
        if (showSamples)
        {
            if (!string.IsNullOrEmpty(searchTerm))
            {
                if (int.TryParse(searchTerm, out value))//ValidationReportID is an INT so ....
                {   //ID, SO, Cust PO, PartNumber
                    gcatList = ciq.ValidationReports.Where(w => w.is_deleted == showDeleted && (w.is_demo ?? false == true) && (w.ValidationReportID == Convert.ToInt32(searchTerm) || w.SalesOrderNumber.Contains(searchTerm) || w.CustomerPO.Contains(searchTerm) || w.MPN.Contains(searchTerm))).ToList();
                }
                else
                {
                    //SO, Cust PO, PartNumber
                    gcatList = ciq.ValidationReports.Where(w => w.is_deleted == showDeleted && (w.is_demo ?? false == true) && (w.SalesOrderNumber.Contains(searchTerm) || w.CustomerPO.Contains(searchTerm) || w.MPN.Contains(searchTerm) || w.company_name.Contains(searchTerm))).ToList();
                }
            }
            else
                gcatList = ciq.ValidationReports.Where(w => w.is_deleted == showDeleted && (w.is_demo ?? false == true)).ToList();
        }


        else
        {

            if (!string.IsNullOrEmpty(searchTerm))
            {

                if (int.TryParse(searchTerm, out value))//ValidationReportID is an INT so ....
                {   //ID, SO, Cust PO, PartNumber
                    gcatList = ciq.ValidationReports.Where(w => w.is_deleted == showDeleted && (w.ValidationReportID == Convert.ToInt32(searchTerm) || w.SalesOrderNumber.Contains(searchTerm) || w.CustomerPO.Contains(searchTerm) || w.MPN.Contains(searchTerm))).ToList();
                }
                else
                {
                    //SO, Cust PO, PartNumber
                    gcatList = ciq.ValidationReports.Where(w => w.is_deleted == showDeleted && (w.SalesOrderNumber.Contains(searchTerm) || w.CustomerPO.Contains(searchTerm) || w.MPN.Contains(searchTerm) || w.company_name.Contains(searchTerm))).ToList();
                }
            }
            else
            {
                gcatList = ciq.ValidationReports.Where(w => w.is_deleted == showDeleted).OrderByDescending(o => o.ValidationReportID).ToList();
            }
        }
        gvGcat.DataSource = gcatList;
        gvGcat.DataBind();
        if (gcatList.Count > 0)
        {
            lblTotalResults.Text = "Total Results: " + gcatList.Count;
            lblTotalResults.Visible = true;
        }
        else
            lblTotalResults.Visible = false;
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        LoadGCATGrid(cbxShowDeleted.Checked, cbxShowsamples.Checked);
    }

    protected void gvGcat_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void gvGcat_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGcat.PageIndex = e.NewPageIndex;
        LoadGCATGrid(cbxShowDeleted.Checked, cbxShowsamples.Checked);
        //upGcat.Update();
    }

    protected void gvGcat_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void lbSelect_Click(object sender, EventArgs e)
    {
        LinkButton btn = sender as LinkButton;
        GridViewRow row = btn.NamingContainer as GridViewRow;
        string validationreportid = row.Cells[1].Text;
        Response.Redirect("gcat_testing.aspx?id=" + validationreportid, false);
    }

    protected void lbClear_Click(object sender, EventArgs e)
    {
        //no code needed, page load refreshes Grid
    }

    protected void lbNewGcat_Click(object sender, EventArgs e)
    {
        Response.Redirect("/secure_sm/gcat/gcat_testing.aspx", false);
    }

    protected void cbxShowDeleted_CheckedChanged(object sender, EventArgs e)
    {
        LoadGCATGrid(cbxShowDeleted.Checked, cbxShowsamples.Checked);
    }


    protected void cbxShowsamples_CheckedChanged(object sender, EventArgs e)
    {       
            LoadGCATGrid(cbxShowDeleted.Checked, cbxShowsamples.Checked);       
    }

    //private void loadSamples(string searchstring = null)
    //{
    //    string companyid;

    //    if (Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_internal"))
    //    {
    //        if (SM_Global.DisplayCompany == null)
    //            companyid = "64DCFCC2-1E68-4AE8-98D1-09CC3E757C29";
    //        else
    //            companyid = SM_Global.DisplayCompany.unique_id;
    //    }
    //    else
    //        companyid = Profile.CompanyID;
    //    if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
    //        searchstring = txtSearch.Text.Trim();
    //    GridView2.DataSource = ql.GetGCATandIDEADataTable(null, searchstring, ddlTestType.SelectedValue, true);
    //    GridView2.DataBind();
    //}

}