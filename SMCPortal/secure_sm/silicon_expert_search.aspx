﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="silicon_expert_search.aspx.cs" Inherits="component_intel" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .CrossRefLegend{
            font-size:10px;          
        }

        .gridIcon{
                font-size: 30px;        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>SILICON EXPERT SEARCH</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">



    <asp:Panel ID="pnlSearchMain" runat="server" DefaultButton="btnPartListSearchXML">

        <div class="row">
            <div class="col-md-4">
                <asp:TextBox ID="txtPartnumber" runat="server" PlaceHolder="Part Number" CssClass="form-control"> </asp:TextBox>
            </div>
            <div class="col-md-4">
                <asp:Button ID="btnPartListSearchXML" runat="server" Text="Search" OnClick="btnPartListSearchXML_Click" CssClass="btn-smc btn-smc-primary" OnClientClick="ShowUpdateProgress();" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlPartList" runat="server">
                    <div class="row" style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">

                        <asp:GridView ID="gvPartList" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-striped gvstyling tablesorter" GridLines="None">
                            <EmptyDataTemplate>
                                <div class="alert alert-warning">
                                    <strong>No data found for the search term.</strong>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField DataField="PartNumber" HeaderText="Part Number" SortExpression="partNumber" />
                                <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" SortExpression="manufacturer" />
                                <asp:BoundField DataField="rohs" HeaderText="RoHS" SortExpression="rohs" />
                                <asp:TemplateField HeaderText="Details">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbDetails" OnClick="lbDetails_Click" OnClientClick="ShowUpdateProgress();"><i class="fas fa-info-circle gridIcon"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="True" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Crosses">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbCrosses" OnClick="lbCrosses_Click" OnClientClick="ShowUpdateProgress();"><i class="fas fa-exchange-alt gridIcon"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="True" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pricing">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbPricing" OnClick="lbPricing_Click" OnClientClick="ShowUpdateProgress();"><i class="fas fa-dollar-sign gridIcon"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="True" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </asp:Panel>
            </div>
        </div>

        <%-- Modals--%>
        <%-- Lifecycle Legend Modal--%>
        <div id="mdlMain" class="modal modal-xl fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            <asp:Label ID="lblModalTitle" runat="server" Text="(Title)"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <%--Generic Grid--%>
                        <asp:GridView ID="gvModal" CssClass="table table-hover table-striped gvstyling tablesorter" runat="server" Visible="false" EmptyDataText="No data found for this part."></asp:GridView>
                        <%--Generic DetailsView--%>
                        <asp:DetailsView ID="dtvModal" runat="server" CssClass="table table-hover table-striped" Visible="false" EmptyDataText="No data found for this part."></asp:DetailsView>

                        <%--Pricing--%>
                        <asp:Panel ID="pnlPricing" runat="server" Visible="false">
                            <label>
                                Silicon Expert Pricing
                            </label>
                            <br />
                            <asp:GridView ID="gvPricingSE" CssClass="table table-hover table-striped gvstyling" runat="server" EmptyDataText="No Pricing data found for this part." AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="MinimumPrice" HeaderText="Min" SortExpression="MinimumPrice" />
                                    <asp:BoundField DataField="AveragePrice" HeaderText="Avg" SortExpression="AveragePrice" />
                                    <asp:BoundField DataField="MinLeadtime" HeaderText="Min LT" SortExpression="MinLeadtime" />
                                    <asp:BoundField DataField="Maxleadtime" HeaderText="Max LT" SortExpression="Maxleadtime" />
                                    <asp:BoundField DataField="LastUpdatedate" HeaderText="Last Update" SortExpression="LastUpdatedate" DataFormatString="{0:d}" />
                                </Columns>
                            </asp:GridView>
                            <br />
                            <label>Rz Sales Historical Pricing</label>
                            <br />
                            <asp:GridView ID="gvPricingRzSales" CssClass="table table-hover table-striped gvstyling" runat="server" EmptyDataText="No Rz Pricing data found for this part." AutoGenerateColumns="false" OnRowDataBound="gvPricingRzSales_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="PartNumber" HeaderText="Part Number" SortExpression="PartNumber" />
                                    <asp:BoundField DataField="LastSale" HeaderText="Last Sale" SortExpression="LastSale" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="Min" HeaderText="Min" SortExpression="Min" />
                                    <asp:BoundField DataField="Max" HeaderText="Max" SortExpression="Max" />
                                    <asp:BoundField DataField="Avg" HeaderText="Avg" SortExpression="Avg" />

                                </Columns>
                            </asp:GridView>

                        </asp:Panel>
                        <%--Crosses--%>
                        <asp:Panel ID="pnlCross" runat="server" Visible="false">


                          

                            <asp:GridView ID="gvCross" CssClass="table table-hover table-striped gvstyling" runat="server" EmptyDataText="No Cross data found for this part." AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="CrossPartNumber" HeaderText="Cross PN" SortExpression="CrossPartNumber" />
                                    <asp:BoundField DataField="CrossManufacturer" HeaderText="Cross MFG" SortExpression="CrossManufacturer" />
                                    <asp:BoundField DataField="CrossType" HeaderText="Type" SortExpression="CrossType" />
                                    <asp:BoundField DataField="Comment" HeaderText="Comment" SortExpression="Comment" />
                                </Columns>
                            </asp:GridView>
                            <br />
                             <h5>Rating Scale  - from Silicon Expert</h5>
                            <p class="CrossRefLegend"><strong>A:</strong> Pin to Pin drop in replacement with exact electrical features.</p>
                            <p class="CrossRefLegend"><strong>A/Upgrade:</strong> Pin to Pin drop in replacement, but the crossed device has better performance in specific key parameters.</p>
                            <p class="CrossRefLegend"><strong>A/Downgrade:</strong> Pin to Pin drop in replacement, but the original device has better performance in specific key parameters.</p>
                            <p class="CrossRefLegend"><strong>B:</strong> Pin to Pin compatible with minor electrical differences and/or minor package dimension.</p>
                            <p class="CrossRefLegend"><strong>B/Upgrade:</strong> Pin to Pin compatible with minor electrical differences and/or minor package dimension, but the crossed device has better performance in specific key parameters.</p>
                            <p class="CrossRefLegend"><strong>B/Downgrade:</strong> Pin to Pin compatible with minor electrical differences and/or minor package dimension, but the original device has better performance in specific key parameters.</p>
                            <p class="CrossRefLegend"><strong>C:</strong> Pin to Pin compatible with major electrical differences.</p>
                            <p class="CrossRefLegend"><strong>C/Upgrade:</strong> Pin to Pin compatible with major electrical differences, but the crossed device has better performance in specific key parameters.</p>
                            <p class="CrossRefLegend"><strong>C/Downgrade:</strong> Pin to Pin compatible with major electrical differences, but the original device has better performance in specific key parameters.</p>
                            <p class="CrossRefLegend"><strong>D:</strong> The two devices have similar functionality with a different package and/or pinout.</p>
                            <p class="CrossRefLegend"><strong>F/Downgrade:</strong> The crossed device has the same functionality with a smaller number of logic cells (in FBGA or CPLD projects).</p>
                            <p class="CrossRefLegend"><strong>F/Upgrade:</strong> The crossed device has the same functionality with a larger number of logic cells (in FBGA or CPLD projects).</p>
                            <p class="CrossRefLegend"><strong>F:</strong> The crossed device has the same functionality (in FBGA or CPLD projects).</p>
                            <br />
                        </asp:Panel>

                        <asp:Panel ID="pnlDetail" runat="server" Visible="false">
                            <label>Summary</label>
                            <asp:DetailsView ID="dtvSummary" runat="server" EmptyDataText="No Data." CssClass="table table-hover table-striped gvstyling" OnDataBound="dtv_DataBound"></asp:DetailsView>
                            <br />
                            <label>Parametric</label>
                            <asp:DetailsView ID="dtvPara" runat="server" CssClass="table table-hover table-striped gvstyling" OnDataBound="dtv_DataBound"></asp:DetailsView>
                            <br />
                            <label>Packaging</label>
                            <asp:DetailsView ID="dtvPackaging" runat="server" CssClass="table table-hover table-striped gvstyling" OnDataBound="dtv_DataBound"></asp:DetailsView>
                            <br />
                            <label>Manufacturing</label>
                            <asp:DetailsView ID="dtvManufacturing" runat="server" CssClass="table table-hover table-striped gvstyling" OnDataBound="dtv_DataBound"></asp:DetailsView>
                            <br />
                            <label>Counterfeit Risk:</label>
                            <asp:DetailsView ID="dtvCounterfeitRisk" runat="server" CssClass="table table-hover table-striped gvstyling" OnDataBound="dtv_DataBound"></asp:DetailsView>
                            <br />
                        </asp:Panel>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>
