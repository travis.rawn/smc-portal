﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;


public partial class secure_sm_NonCon_NonconSummary : System.Web.UI.Page
{
    //DataBind via LINQ Query
    RzDataContext RZDC = new RzDataContext();
    sm_nonconEntities NCDC = new sm_nonconEntities();

    public int NonConID
    {
        get
        {
            return Convert.ToInt32(ViewState["NonConID"].ToString());
        }
        set
        {
            ViewState["NonConID"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        

        //Is the querystring a valid NonCon?  If not spank
        if (Request.QueryString.Count > 0)
        {
            string strNonConID = Tools.Strings.SanitizeInput(Request.QueryString["NonConID"]).ToString();
            if (string.IsNullOrEmpty(strNonConID))
                throw new Exception(strNonConID + " is not a valid NonConID");
            int testNonConID = 0;
            if (!Int32.TryParse(strNonConID, out testNonConID))
                throw new Exception(strNonConID + " is not a valid NonConID");
            NonConID = testNonConID;
            if(NonConID == 0)
                throw new Exception(NonConID.ToString() + " is not a valid NonConID");
            LoadData(NonConID.ToString());


            if (!NCDC.NonCon_Head.Any(h => h.NonConID == NonConID))
            {
                lblPageError.Text = "This is not a valid nonconformance ID, please try again";

            }
            else
            {
                //Populate Dispo
                var DispoQuery =
            from disposition in NCDC.NonCon_Disposition
            where (disposition.NonConID == NonConID)
            select disposition;
                foreach (NonCon_Disposition disposition in DispoQuery)
                {
                    cbxAs_Is.Checked = Convert.ToBoolean(disposition.as_is);
                    if (disposition.vendor_return == "1")
                    {
                        cbxVendor_Return.Checked = true;
                    }
                    else
                    {
                        cbxVendor_Return.Checked = false;
                    }
                    cbxReplace.Checked = Convert.ToBoolean(disposition.replace);
                    cbxReturn_To_Stock.Checked = Convert.ToBoolean(disposition.return_to_stock);
                    cbxQuarantined.Checked = Convert.ToBoolean(disposition.quarantined);
                    cbxScrap.Checked = Convert.ToBoolean(disposition.scrap);

                }
                var HeadQuery =
           from header in NCDC.NonCon_Head
           where (header.NonConID == NonConID)
           select header;
                foreach (NonCon_Head header in HeadQuery)
                {
                    txtNonconRemarks.Text = header.Description;
                }
                //Populate Payment
                var PaymentQuery =
            from payment in NCDC.NonCon_Payment
            where (payment.NonConID == NonConID)
            select payment;
                foreach (NonCon_Payment payment in PaymentQuery)
                {
                    if (Convert.ToBoolean(payment.isPaymentDue) == true)
                    {
                        txtPaymentDue.Text = "Yes";
                    }
                    else if (payment.isPaymentDue == null)
                    {
                        txtPaymentDue.Text = "";
                    }
                    else
                    {
                        txtPaymentDue.Text = "";
                    }
                    if (Convert.ToBoolean(payment.isPaymentRefunded) == true)
                    {
                        txtPaymentRefunded.Text = "Yes";
                    }
                    else if (payment.isPaymentRefunded == null)
                    {
                        txtPaymentRefunded.Text = "";
                    }
                    else
                    {
                        txtPaymentRefunded.Text = "N/A";
                    }

                    if (payment.dateRefunded != null)
                    {
                        txtDateRefunded.Text = Convert.ToDateTime(payment.dateRefunded.ToString()).ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        txtDateRefunded.Text = "";
                    }

                }
            }
        }
    }

    private void LoadData(string nonConID)
    {
        var query = NCDC.NonCon_SummaryData(nonConID);
        dtvNonConSummary.DataSource = query.Select(s => new
        {
            NonConID = s.NonConID,
            Date = s.Date,
            SubmittedBy = s.Submited_By,
            Customer = s.Customer,
            CustomerPO = s.Customer_PO,
            Sale = s.SO__,
            Vendor = s.Vendor,
            VendorPO = s.VendorPO,
            ContainmentNotes = s.Containment_Notes,
            PartNumber = s.Part_Number,
            QTY = s.QTY









        });
        dtvNonConSummary.DataBind();
    }

    protected void btnReturnNonCon_Click(object sender, EventArgs e)
    {
        Response.Redirect("Nonconformance.aspx?NonConID=" + Request.QueryString["NonConID"], false);
    }
}