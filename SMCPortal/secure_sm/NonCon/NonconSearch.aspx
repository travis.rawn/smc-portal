﻿<%@ Page Title="Nonconformance Search" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="NonconSearch.aspx.cs" Inherits="secure_sm_NonCon_Search_NonCon2" %>

<%@ Register Src="~/assets/controls/sm_tooltip.ascx" TagPrefix="uc1" TagName="sm_tooltip" %>
<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <%-- <script src="../../Scripts/jquery-1.11.2.min.js"></script>--%>

    <style>
        .tooltip {
            font-size: 18px;
        }

        .tooltip-inner {
            max-width: 50vw;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>NONCONFORMANCE SEARCH</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container">
        <div class="card">

            <asp:Panel ID="pnlSearch" runat="server">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <asp:Button ID="btnNewNoncon" runat="server" OnClick="btnNewNoncon_Click" CssClass="form-control" Text="Create New Nonconformance" OnClientClick="ShowUpdateProgress();" />
                        <%--<a href="http://sm1/Reports_SQLEXPRESS/Pages/Report.aspx?ItemPath=%2fRz4+Reports%2fShipping+and+QC%2fNonconformance+and+RMA+Report" target="_blank" class="btn-smc btn-smc-info">Open Nonconformance Report</a>--%>
                    </div>
                    <div class="col-sm-2">
                        <div class="pull-left">
                            <asp:CheckBox ID="cbxDeleted" runat="server" Text="Show Deleted" AutoPostBack="True" onchange="ShowUpdateProgress();" />
                        </div>
                    </div>
                     <div class="col-sm-4">
                       
                    </div>


                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <em> <p>** Rows colored <strong><span style="color:darkorange">Orange</span></strong> have already been submitted for closure.</p></em>
                    </div>
                </div>
            </asp:Panel>

            <uc1:sm_datatable runat="server" ID="smdt" />

        </div>
    </div>   
</asp:Content>

