﻿using SensibleDAL.dbml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.ef;

public partial class secure_sm_NonCon_Search_NonCon2 : System.Web.UI.Page
{
    //Globals

    List<NonConSearchObject> nonconList = new List<NonConSearchObject>();

    SM_Tools tools = new SM_Tools();
    private string submittedBy = "%";

    //Rz References
    List<string> rmaIDList = new List<string>();
    List<string> vrmaIDList = new List<string>();
    List<ordhed_rma> rmaList = new List<ordhed_rma>();
    List<ordhed_vendrma> vrmaList = new List<ordhed_vendrma>();
    List<orddet_line> linesList = new List<orddet_line>();




    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            smdt.RowDataBound += new GridViewRowEventHandler(smdt_RowDataBound);
            CheckPermissions();
            LoadDataTable();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }


    class NonConSearchObject
    {
        public int NonConID { get; set; }
        public DateTime NonConDate { get; set; }
        public string SubmittedBy { get; set; }
        public string VendorName { get; set; }
        public string VendorPO { get; set; }
        public string CustomerName { get; set; }
        public string PartNumber { get; set; }
        public string Status { get; set; }
        public string RMA_Comment { get; set; }
        public string isComplete { get; set; }
        public bool  SubmittedForClosure { get; set; }
    }


    private void smdt_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string nonConID = e.Row.Cells[0].Text;
           
            HyperLink hl = new HyperLink();
            hl.Text = nonConID.ToString();
            hl.Target = "_blank";
            hl.NavigateUrl = "/secure_sm/NonCon/Nonconformance.aspx?NonConID=" + nonConID;
            e.Row.Cells[0].Controls.Add(hl);


            //ToolTip
            if (e.Row.Cells[8] == null)
                return;
            string rma_comment_text = e.Row.Cells[8].Text;
            if (string.IsNullOrEmpty(rma_comment_text))
                return;
            //if (rma_comment_text == "N/A")
                //return;
            assets_controls_sm_tooltip tt = (assets_controls_sm_tooltip)Page.LoadControl("~/assets/controls/sm_tooltip.ascx");
            tt.dataPlacement = "left";
            tt.toolTipBody = e.Row.Cells[8].Text;
            tt.toolTipTitle = "RMA DATA";
            tt.LoadTooltip();
            e.Row.Cells[8].Text = "";
            e.Row.Cells[8].Controls.Add(tt);


            //Color code Open Noncons that have been submitted for closure.
            NonConSearchObject nco = nonconList.Where(w => w.NonConID.ToString() == nonConID).FirstOrDefault();
            if (nco.SubmittedForClosure == true)
                if (nco.Status == "Open")
                {
                    //e.Row.Cells[0].Style.Add("background-color", SM_Tools.Colors.smOrangeRgba);
                    e.Row.Style.Add("background-color", SM_Tools.Colors.smOrangeRgba);

                }
                   
        }
    }


    protected void GetHeaderData()
    {
        //These is shared between the 2 datasets, declare outside the usings
        List<string> lineIdList = new List<string>();
        List<SensibleDAL.ef.NonCon_Head> headerList = new List<SensibleDAL.ef.NonCon_Head>();
        List<SensibleDAL.ef.NonCon_Customer> customerList = new List<SensibleDAL.ef.NonCon_Customer>();
        List<SensibleDAL.ef.NonCon_RMA> RMAList = new List<SensibleDAL.ef.NonCon_RMA>();
        using (sm_nonconEntities NCDC = new sm_nonconEntities())
        {


            headerList = NCDC.NonCon_Head.ToList();
            //if (!cbxDeleted.Checked)
                headerList = headerList.Where(w => (w.deleted ?? false) == cbxDeleted.Checked).ToList();
            if (headerList == null || headerList.Count <= 0)
                throw new Exception("No Nonconformanmce data found that matches your query.");

            lineIdList = headerList.Select(s => s.partNumberID).Distinct().ToList();

            List<string> nonConIDList = headerList.Select(s => s.NonConID.ToString()).Distinct().ToList();
            customerList = NCDC.NonCon_Customer.Where(w => nonConIDList.Contains(w.NonConID.ToString())).ToList();
            RMAList = NCDC.NonCon_RMA.Where(w => nonConIDList.Contains(w.NonConID.ToString())).ToList();



        }





        using (RzDataContext rdc = new RzDataContext())
        {
            linesList = rdc.orddet_lines.Where(w => lineIdList.Contains(w.unique_id)).ToList();
            rmaIDList = linesList.Where(w => lineIdList.Contains(w.unique_id)).Select(s => s.orderid_rma).Distinct().ToList();
            vrmaIDList = linesList.Where(w => lineIdList.Contains(w.unique_id)).Select(s => s.orderid_vendrma).Distinct().ToList();

            //Populate the objects
            rmaList = rdc.ordhed_rmas.Where(w => rmaIDList.Contains(w.unique_id)).ToList();
            vrmaList = rdc.ordhed_vendrmas.Where(w => rmaIDList.Contains(w.unique_id)).ToList();
        }





        //HeaderData = NCDC.NonCon_HeaderInfo("%", "%", "0", cbxDeleted.Checked, submittedBy).ToList();      

        foreach (SensibleDAL.ef.NonCon_Head h in headerList)
        {
            NonConSearchObject o = new NonConSearchObject();
            o.NonConID = h.NonConID;
            o.NonConDate = h.NonConDate ?? DateTime.MinValue;
            o.SubmittedBy = h.submittedBy;
            o.VendorName = h.vendorName;
            o.VendorPO = h.vendorPO;
            o.PartNumber = h.partNumber;
            o.Status = h.status;
            o.isComplete = h.isComplete == "1" ? "True" : "False";

            SensibleDAL.ef.NonCon_Customer c = customerList.Where(w => w.NonConID == h.NonConID).FirstOrDefault();
            if (c != null)
                o.CustomerName = c.customerName;
            else
                o.CustomerName = "N/A";

            o.RMA_Comment = BuildRMACommentString(h.partNumberID);
            o.SubmittedForClosure = !string.IsNullOrEmpty(h.closure_submit_by);

            nonconList.Add(o);
        }


    }

    private string BuildRMACommentString(string orddet_line_uid)
    {
        //Get RMA and VRMA related to this PO.  Return string reporting each.
        string defaultValue = "N/A";
        string ret = "";
        orddet_line l = linesList.Where(w => w.unique_id == orddet_line_uid).FirstOrDefault();
        if (l == null)
            return defaultValue;

        foreach (ordhed_rma o in rmaList.Where(w => w.unique_id == l.orderid_rma))
            ret += "RMA " + o.ordernumber + ": " + o.internalcomment + "\n";
        foreach (ordhed_vendrma o in vrmaList.Where(w => w.unique_id == l.orderid_vendrma))
            ret += "VRMA " + o.ordernumber + ": " + o.internalcomment + "\n";

        if (!string.IsNullOrEmpty(ret))
            return ret;
        else
            return defaultValue;
    }

    private void LoadDataTable()
    {

        //Set DataTable Options
        smdt.sortColumn = 0;
        smdt.sortDirection = "desc";

        //Load the Data.
        GetHeaderData();

        //Formatted Query
        smdt.dataSource = nonconList.Select(s => new
        {
            ID = s.NonConID,
            Date = s.NonConDate.ToShortDateString(),
            Creator = s.SubmittedBy,
            Vendor = s.VendorName,
            PO = s.VendorPO,
            Customer = s.CustomerName,
            PartNumber = s.PartNumber,
            s.Status,
            Comment = s.RMA_Comment,
            //SubmitForClosure = s.SubmittedForClosure

        });


        smdt.loadGrid();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "gridviewKey", "loadGrid();", true);


    }

    protected void btnNewNoncon_Click(object sender, EventArgs e)
    {
        Response.Redirect("Nonconformance.aspx", false);
    }


    protected void CheckPermissions()
    {
        if (Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_nonconadmin") || Roles.IsUserInRole("sm_nonconuser") || Roles.IsUserInRole("sm_internal_executive") || Roles.IsUserInRole("sm_nonconviewer"))
        {
            //SqlDataSource1.SelectParameters["submittedBy"].DefaultValue = "%";
            return;
        }
        else
            submittedBy = System.Web.Security.Membership.GetUser().ToString();

    }




}