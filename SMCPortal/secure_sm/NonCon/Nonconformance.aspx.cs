﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using System.Data;
using SensibleDAL.ef;

public partial class secure_sm_NonCon_New_NonCon : System.Web.UI.Page
{




    //NONCON_RMADataContext ndc = new NONCON_RMADataContext();
    RzDataContext rdc = new RzDataContext();
    SM_Tools tools = new SM_Tools();
    SM_Quality_Logic ql = new SM_Quality_Logic();
    sm_nonconEntities ndc = new sm_nonconEntities();

    SensibleDAL.ef.NonCon_Head Header;
    SensibleDAL.ef.NonCon_Details Details;
    SensibleDAL.ef.NonCon_RMA RMA;
    SensibleDAL.ef.NonCon_Payment Payment;
    SensibleDAL.ef.NonCon_Disposition Dispo;
    SensibleDAL.ef.NonCon_Customer NonConCustomer;
    bool AllowEdit = false;



    List<orddet_line> TheOrderLines = new List<orddet_line>();
    List<service_line> TheServiceLines = new List<service_line>();
    List<partrecord> ThePartRecords = new List<partrecord>();
    List<orddet_line_canceled> TheCanceledLines = new List<orddet_line_canceled>();


    [Serializable]
    public class LineObject
    {
        public string Part { get; set; }
        public long QTY { get; set; }
        public string MFG { get; set; }
        public string PONumber { get; set; }
        public string PurchaseID { get; set; }
        public string SONumber { get; set; }
        public string SalesID { get; set; }
        public long NoconID { get; set; }
        public string Unique_id { get; set; }
        public string date { get; set; }

    }
    //This list will contain existing noncon objects
    public List<LineObject> lineObjectList = new List<LineObject>();



    int NonConID
    {
        get
        {
            if (ViewState["NonConID"] == null)
                return 0;
            else if (ViewState["NonConID"].ToString() == "")
                return 0;
            else return Convert.ToInt32(ViewState["NonConID"]);
        }
        set
        {
            ViewState["NonConID"] = value;
        }
    }
    List<insp_images> nonConImages = new List<insp_images>();
    List<InspectionImageManagerControl> imcList = new List<InspectionImageManagerControl>();
    List<string> ImageControlIdList
    {
        get
        {
            if (ViewState["ImageControlIdList"] == null)
                return new List<string>();
            else
                return (List<string>)ViewState["ImageControlIdList"];
        }
        set
        {
            ViewState["ImageControlIdList"] = value;
        }
    }

    //Methods
    protected override void OnPreLoad(EventArgs e)
    {

        base.OnPreLoad(e);
        //HidePanels();
        LoadHeaderObject();
        LoadImagery();
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        smdtPartData.RowDataBound += new GridViewRowEventHandler(gvParts_RowDataBound);
        //smdtPartData.RowCreated += new GridViewRowEventHandler(gvParts_RowCreated);
        try
        {
            lblPageSuccess.Text = "";


            rzpa.PartSelected += new EventHandler(OnPartrecordSelected);
            //The Vendor DDL Is the starting point for all NonCons.
            LoadVendorDDL();
            //Load Header Object  
            if (!Page.IsPostBack)
            {
                //LoadHeaderObject();
                if (Header == null)
                    return;
                //LoadHeaderDetailsView();
                //LoadCustomerDetailsView();
                LoadDates();
                //Load Admin Options
                LoadAdminOptions();

                //Approval logic.
                LoadApprovalOptions();
            }
            //Load Panels
            LoadTabs();
            LoadButtons();

            if (!Page.IsPostBack)
                LoadAllSectionsFromHeaderObject();
            //GatherLines();

            //Maintain Datatables.Net after Postback
            //DrawDataTable();
            //if (!Page.IsPostBack)
                LoadNonConPartsGrid();




        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    private void LoadDates()
    {
        if (Header.NonConDate != null)
            dtp.SelectedDate = Header.NonConDate.Value;
        else
            dtp.SelectedDate = DateTime.Now;
    }
    private void LoadButtons()
    {
        if (Header == null)
            return;



        if (Header.status == SM_Enums.NonConStatus.Complete.ToString())
        {
            lbCompleteNonCon.Visible = false;
            lbUncompleteNonCon.Visible = true;


        }
        else
        {
            lbCompleteNonCon.Visible = true;
            lbUncompleteNonCon.Visible = false;
        }
    }
    
    private void LoadAllSectionsFromHeaderObject()
    {
        //Header Section
        LoadHeader();
        //Details Section           
        LoadDetails();
        //Dispo Section
        LoadDisposition();
        //Payment Section
        LoadPayment();
        //Grid - Load any existing NonconObjects, the DDLs will simply add more options.
        //LoadExistingNonConObjects();

        //Details Section
        //LoadDetails();
        //Imagery
        //LoadImageryData();
    }
    

    private List<LineObject> BuildLineObjectList()
    {
        lineObjectList.Clear();
        LineObject lo = null;
        if (TheOrderLines.Count > 0)
        {
            foreach (orddet_line l in TheOrderLines)
            {
                if (!lineObjectList.Any(a => a.Unique_id == l.unique_id))
                {
                    lo = CreateLineObject_OrddetLine(l);
                    if (lo != null)
                        lineObjectList.Add(lo);
                }
            }
        }
        if (ThePartRecords.Count > 0)
        {
            foreach (partrecord l in ThePartRecords)
            {
                if (!lineObjectList.Any(a => a.Unique_id == l.unique_id))
                {
                    lo = CreateLineObject_Partrecord(l);
                    if (lo != null)
                        lineObjectList.Add(lo);
                }
            }
        }
        if (TheServiceLines.Count > 0)
        {
            foreach (service_line ll in TheServiceLines)
            {
                if (!lineObjectList.Any(a => a.Unique_id == ll.the_orddet_line_uid))
                {
                    lo = CreateLineObject_Service(ll);
                    if (lo != null)
                        lineObjectList.Add(lo);
                }
            }
        }
        if (TheCanceledLines.Count > 0)
        {
            foreach (orddet_line_canceled cl in TheCanceledLines)
            {
                if (!lineObjectList.Any(a => a.Unique_id == cl.unique_id))
                {
                    lo = CreateLineObject_Canceled(cl);
                    if (lo != null)
                        lineObjectList.Add(lo);
                }
            }
        }
        return (lineObjectList);
    }

    private LineObject CreateLineObject(object o)
    {
        if (o is orddet_line)
            return CreateLineObject_OrddetLine(o);
        if (o is partrecord)
            return CreateLineObject_Partrecord(o);
        if (o is orddet_line_canceled)
            return CreateLineObject_Canceled(o);
        if (o is service_line)
            return CreateLineObject_Service(o);
        return null;
    }
    private LineObject CreateLineObject_OrddetLine(object o)
    {
        orddet_line l = (orddet_line)o;

        LineObject lo = lineObjectList.Where(w => w.Unique_id == l.unique_id).FirstOrDefault();
        if (lo == null)
            lo = new LineObject();

        lo.Part = (l.fullpartnumber ?? "N/A").Trim().ToUpper();
        lo.MFG = (l.manufacturer ?? "N/A").Trim().ToUpper();
        lo.QTY = (long)l.quantity;
        lo.NoconID = 0;
        if (l.nonconid != null)
            lo.NoconID = (long)l.nonconid;
        lo.PONumber = (l.ordernumber_purchase ?? "N/A").Trim().ToUpper() ?? "";
        lo.PurchaseID = (l.orderid_purchase ?? "N/A").Trim().ToUpper() ?? "";
        lo.SONumber = (l.ordernumber_sales ?? "N/A").Trim().ToUpper() ?? "";
        lo.SalesID = (l.orderid_sales ?? "N/A").Trim().ToUpper() ?? "";
        lo.date = l.date_created.Value.ToShortDateString();
        lo.Unique_id = l.unique_id;

        return lo;
    }
    private LineObject CreateLineObject_Partrecord(object o)
    {
        partrecord p = (partrecord)o;
        LineObject lo = lineObjectList.Where(w => w.Unique_id == p.unique_id).FirstOrDefault();
        if (lo == null)
            lo = new LineObject();

        lo.Part = (p.fullpartnumber ?? "N/A").Trim().ToUpper();
        lo.MFG = (p.manufacturer ?? "N/A").Trim().ToUpper();
        lo.QTY = (long)p.quantity;
        lo.NoconID = 0;
        if (p.nonconid != null)
            lo.NoconID = (long)p.nonconid;
        lo.date = p.date_created.Value.ToShortDateString();
        lo.Unique_id = p.unique_id;
        return lo;
    }
    private LineObject CreateLineObject_Canceled(object o)
    {
        orddet_line_canceled c = (orddet_line_canceled)o;
        LineObject lo = lineObjectList.Where(w => w.Unique_id == c.unique_id).FirstOrDefault();
        if (lo == null)
            lo = new LineObject();
        lo.Part = (c.fullpartnumber ?? "N/A").Trim().ToUpper() + " (Canceled)";
        lo.MFG = (c.manufacturer ?? "N/A").Trim().ToUpper();
        lo.QTY = (long)c.quantity;
        lo.NoconID = (long)c.nonconid;
        lo.PONumber = (c.ordernumber_purchase ?? "N/A").Trim().ToUpper() ?? "";
        lo.PurchaseID = (c.orderid_purchase ?? "N/A").Trim().ToUpper() ?? "";
        lo.SONumber = (c.ordernumber_sales ?? "N/A").Trim().ToUpper() ?? "";
        lo.SalesID = (c.orderid_sales ?? "N/A").Trim().ToUpper() ?? "";
        lo.date = c.date_created.Value.ToShortDateString();
        lo.Unique_id = c.unique_id;
        return lo;
    }
    private LineObject CreateLineObject_Service(object o)
    {
        service_line s = (service_line)o;
        //if (lineObjectList.Select(ss => ss.Unique_id).ToList().Contains(s.unique_id))
        //    return null;
        orddet_line l = rdc.orddet_lines.Where(w => w.unique_id == s.the_orddet_line_uid).FirstOrDefault();
        if (l == null)
            return null;
        LineObject lo = lineObjectList.Where(w => w.Unique_id == l.unique_id).FirstOrDefault();
        if (lo == null)
            lo = new LineObject();


        lo.Part = (l.fullpartnumber ?? "N/A").Trim().ToUpper();
        lo.MFG = (l.manufacturer ?? "N/A").Trim().ToUpper();
        lo.QTY = (long)l.quantity;
        lo.NoconID = 0;
        if (l.nonconid != null)
            lo.NoconID = (long)l.nonconid;
        lo.PONumber = (l.ordernumber_purchase ?? "N/A").Trim().ToUpper() ?? "";
        lo.PurchaseID = (l.orderid_purchase ?? "N/A").Trim().ToUpper() ?? "";
        lo.SONumber = (l.ordernumber_sales ?? "N/A").Trim().ToUpper() ?? "";
        lo.SalesID = (l.orderid_sales ?? "N/A").Trim().ToUpper() ?? "";
        lo.date = l.date_created.Value.ToShortDateString();
        lo.Unique_id = l.unique_id;
        return lo;
    }







    private void LoadNonConPartsGrid()
    {
        GatherLines();
        if (lineObjectList.Count <= 0)
            return;
        //Clear Existing so stale data doesn't remain
        smdtPartData.dataSource = null;
        smdtPartData.loadGrid();
        //if (lineObjectList.Count <= 0)
        //    throw new Exception("No line item object found related to the selected Vendor or PartNumber");
        var query = lineObjectList.Select(s => new
        {
            Select = "",
            s.Part,
            s.MFG,
            s.QTY,
            s.PONumber,
            s.SONumber,
            s.NoconID,
            Date = s.date,
            s.Unique_id,
            s.PurchaseID,
            s.SalesID




        });
        smdtPartData.dataSource = query.OrderBy(oo => oo.Part);
        smdtPartData.loadGrid();
        pnlPartData.Visible = true;
        //DrawDataTable();
        //smdtPartData.theGridView

    }
    private void LoadTabs()
    {
        mnuDetails.Visible = false;
        mnuDisposition.Visible = false;
        mnuPayment.Visible = false;
        mnuImagery.Visible = false;


        if (Header == null)
            return;
        if (!(Header.is_approved ?? false))
            return;
        mnuDetails.Visible = true;
        mnuDisposition.Visible = true;
        mnuPayment.Visible = true;
        mnuImagery.Visible = true;

    }
    protected void OnPartrecordSelected(object sender, EventArgs e)
    {

        //This is accessed via 2 ways
        //1) Vendor Name set to Stock Check
        //2) DDlSales order SEt to Stock Check (we have a PO, but no Sales Order)
        try
        {
            //ResetDependentControls();            
            string selectedpart = rzpa.PartNumber.Trim().ToLower();
            string selectedPartID = rzpa.PartUID;

            if (ddlVendorName.SelectedValue == "1" || ddlSalesOrders.SelectedValue == "1")
            {
                //We might have a sales order, linked to a stock part with no vendor.  
                if (!string.IsNullOrEmpty(selectedpart))
                {
                    //Check Sales 1st, if nothing, search for partrecords.
                    List<orddet_line> matchingLines = rdc.orddet_lines.Where(w => w.fullpartnumber.Trim().ToUpper() == selectedpart.Trim().ToUpper()).ToList();
                    if (matchingLines != null && matchingLines.Count > 0)
                    {
                        //load sales picker
                        LoadSalesPanel(matchingLines.Select(s => s.orderid_sales).Distinct().ToList());
                    }
                    else
                    {
                        //Load PArtrecord Picker
                        LoadPartrecords_PartNumber(selectedpart);
                        //LoadNonConPartsGrid(ThePartRecords);
                    }



                }
            }


        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
    protected void LoadVendorDDL()
    {
        //DataBind ddlVendorName
        if (!Page.IsPostBack)
        {
            ddlVendorName.Items.Clear();
            ddlVendorName.DataSource = null;
            ddlVendorName.DataBind();
            ddlVendorName.DataSource = rdc.SMCPortal_Orddet_Vendors().Where(w => w.vendor_name.ToLower() != "stock");
            ddlVendorName.DataTextField = "vendor_name";
            ddlVendorName.DataValueField = "vendor_uid";
            ddlVendorName.Items.Add(new ListItem("--Choose Vendor--", "0"));
            ddlVendorName.Items.Add(new ListItem("--Unknown Stock Vendor / Stock Check--", "1"));
            ddlVendorName.AppendDataBoundItems = true;
            ddlVendorName.DataBind();
        }


    }
    private void LoadAdminOptions()
    {
        var admins = new List<string> { "admin", "sm_nonconadmin" };
        var nonconusers = new List<string> { "sm_nonconuser" };
        var userRoles = Roles.GetRolesForUser(User.Identity.Name);
        pnlAdminOptions.Visible = false;
        pnlNonconUser.Visible = false;



        //NopnCon Admins
        if (userRoles.Any(u => admins.Contains(u)))
        {
            pnlAdminOptions.Visible = true;
            pnlNonconUser.Visible = true;
        }
        //Noncon Users
        else if (userRoles.Any(u => nonconusers.Contains(u)))
        {

            //pnlDelete.Visible = true;
            pnlNonconUser.Visible = true;


        }



        if (!Roles.IsUserInRole("admin") && !Roles.IsUserInRole("sm_internal_executive") && !Roles.IsUserInRole("sm_nonconadmin") && !Roles.IsUserInRole("sm_nonconuser") && !Roles.IsUserInRole("sm_nonconviewer"))
        {
            if (Header.submittedBy != System.Web.Security.Membership.GetUser().ToString())
            {
                TabContainer.Visible = false;
                pnlNonconInfo.Visible = false;
                lblPageError.Text = "Sorry, you are not authorized to access this nonconformance.";
            }
        }

        //Only allow edit NonCon if the current user is the "submittedby" user, or an site or nonconuser or nonconadmin
        var nonconowner = new List<string> { "admin", "sm_nonconadmin", "sm_sonconuser" };
        AllowEdit = (userRoles.Any(u => admins.Contains(u)) || userRoles.Any(u => nonconusers.Contains(u)) || System.Web.Security.Membership.GetUser().ToString() == Header.submittedBy);

        //hide the submit for closure button if status is "Pending Closure"
        if (Header.status == SM_Enums.NonConStatus.PendingCompletion.ToString())
        {
            btnSubmitCLosure.Visible = false;
        }

        if (!Page.IsPostBack) //Make sure to keep this out of postback, otherwise submitting will overwrite this with whatever is alreay in the DB.   If Null it's "<blank>"
        {
            txtClosureRemarks.Text = Header.ClosureRemarks;
        }


        if (Header.deleted == true)
        {
            //btnDeleteNoncon.Text = "Undelete Nonconformance";
            lblPageError.Text = "This nonconformance has been flagged for deletion.  Please see your manager if you think this in error";
            TabContainer.Visible = false;
            //pnlAdminOptions.Visible = false;
            pnlNonconUser.Visible = false;

        }

    }
    private void LoadRMA()
    {
        if (!Page.IsPostBack)
        {

            RMA = ndc.NonCon_RMA.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (RMA == null)
                return;
            cbxisVRMA.Checked = Convert.ToBoolean(RMA.isVRMA);
            if (txtVRMAQTY.Text != null)
                txtVRMAQTY.Text = RMA.VRMAQTY.ToString();
            cbxisCRMA.Checked = Convert.ToBoolean(RMA.isCRMA);
            if (txtCRMAQTY.Text != null)
                txtCRMAQTY.Text = RMA.CRMAQTY.ToString();
            cbxisFunctionalIssue.Checked = Convert.ToBoolean(RMA.isFunctionalIssue);
            cbxfnSolderFail.Checked = Convert.ToBoolean(RMA.fnSolderFail);
            cbxfnElecFail.Checked = Convert.ToBoolean(RMA.fnElecFail);
            cbxfnCoplanFail.Checked = Convert.ToBoolean(RMA.fnCoplanFail);
            cbxfnProgramFail.Checked = Convert.ToBoolean(RMA.fnProgramFail);
            txtfnRemarks.Text = RMA.fnRemarks;
            cbxisSuspectCounterfeit.Checked = Convert.ToBoolean(RMA.isSuspectCounterfeit);
            cbxscSecondaryCoating.Checked = Convert.ToBoolean(RMA.scSecondaryCoating);
            cbxscRemarked.Checked = Convert.ToBoolean(RMA.scRemarked);
            cbxscCloned.Checked = Convert.ToBoolean(RMA.scCloned);
            txtscRemarks.Text = RMA.scRemarks;
            cbxisDamaged.Checked = Convert.ToBoolean(RMA.isDamaged);
            cbxdmCarrier.Checked = Convert.ToBoolean(RMA.dmCarrier);
            cbxdmSMCReceipt.Checked = Convert.ToBoolean(RMA.dmSMCReceipt);
            cbxdmCustReceipt.Checked = Convert.ToBoolean(RMA.dmCustReceipt);
            cbxdmSMCWarehouse.Checked = Convert.ToBoolean(RMA.dmSMCWarehouse);
            cbxdmProduct.Checked = Convert.ToBoolean(RMA.dmProduct);
            cbxdmPackage.Checked = Convert.ToBoolean(RMA.dmPackage);
            txtdmRemarks.Text = RMA.dmRemarks;
            cbxisClerical.Checked = Convert.ToBoolean(RMA.isClerical);
            cbxclSMC.Checked = Convert.ToBoolean(RMA.clSMC);
            cbxclVendor.Checked = Convert.ToBoolean(RMA.clVendor);
            cbxclCustomer.Checked = Convert.ToBoolean(RMA.clCustomer);
            txtclRemarks.Text = RMA.clRemarks;
            cbxisImproperPKG.Checked = Convert.ToBoolean(RMA.isImproperPKG);
            cbxipContainer.Checked = Convert.ToBoolean(RMA.ipContainer);
            cbxipMSL.Checked = Convert.ToBoolean(RMA.ipMSL);
            cbxipESD.Checked = Convert.ToBoolean(RMA.ipESD);
            cbxipTandR.Checked = Convert.ToBoolean(RMA.ipTandR);
            txtipRemarks.Text = RMA.ipRemarks;
            txtRMARemarks.Text = RMA.RMARemarks;



            //RMA Panels Section
            if (cbxisFunctionalIssue.Checked == true)
            {
                pnlisFunctionalIssue.Style["display"] = "block";
            }
            else
            {
                pnlisFunctionalIssue.Style["display"] = "none";
            }
            if (cbxisSuspectCounterfeit.Checked == true)
            {
                pnlisSuspectCounterfeit.Style["display"] = "block";
            }
            else
            {
                pnlisSuspectCounterfeit.Style["display"] = "none";
            }
            if (cbxisDamaged.Checked == true)
            {
                pnlisDamaged.Style["display"] = "block";
            }
            else
            {
                pnlisDamaged.Style["display"] = "none";
            }
            if (cbxisClerical.Checked == true)
            {
                pnlisClerical.Style["display"] = "block";
            }
            else
            {
                pnlisClerical.Style["display"] = "none";
            }
            if (cbxisImproperPKG.Checked == true)
            {
                pnlisImproperPKG.Style["display"] = "block";
            }
            else
            {
                pnlisImproperPKG.Style["display"] = "none";
            }


        }

    }
    private void LoadPayment()
    {
        if (!Page.IsPostBack)
        {


            Payment = ndc.NonCon_Payment.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (Payment == null)
                return;



            if (Payment.isPaymentDue ?? false)
            {
                pnlisPaymentDue.Style.Add("display", "block");
                cbxisPaymentDue.Checked = true;
            }
            else
                pnlisPaymentDue.Style.Add("display", "none");


            //Check Payment type and display appropriate panels
            pnlWireDetails.Style.Add("display", "none");
            pnlCheckDetails.Style.Add("display", "none");
            pnlCreditCard.Style.Add("display", "none");
            pnlPayPalDetails.Style.Add("display", "none");
            if (Payment.paymentType == "Wire")
            {
                ddlPaymenttype.SelectedValue = "1";
            }
            else if (Payment.paymentType == "Check")
            {
                ddlPaymenttype.SelectedValue = "2";
            }
            else if (Payment.paymentType == "Credit Card")
            {
                ddlPaymenttype.SelectedValue = "3";
            }
            else if (Payment.paymentType == "PayPal")
            {
                ddlPaymenttype.SelectedValue = "4";
            }
            else
                ddlPaymenttype.SelectedValue = "0";

            ////Show appropriate detail panel based on ddl selection       
            ////Wire
            //if (ddlPaymenttype.SelectedValue == "1")
            //    pnlWireDetails.Style.Add("display", "block");


            ////Check
            //else if (ddlPaymenttype.SelectedValue == "2")
            //    pnlCheckDetails.Style.Add("display", "block");

            ////Credit
            //else if (ddlPaymenttype.SelectedValue == "3")
            //    pnlCreditCard.Style.Add("display", "block");

            ////PAyPal     
            //else if (ddlPaymenttype.SelectedValue == "4")
            //    pnlPayPalDetails.Style.Add("display", "block");



            cbxisPaymentRefunded.Checked = Convert.ToBoolean(Payment.isPaymentRefunded);
            if (cbxisPaymentDue.Checked == true)
                pnlisPaymentDue.Style.Add("display", "block");
            else
                pnlisPaymentRefunded.Style.Add("display", "none");


            if (Payment.dateRefunded != null && Payment.dateRefunded != DateTime.MinValue)
                dtp_DateRefunded.SelectedDate = Convert.ToDateTime(Payment.dateRefunded);


            cbxWireRecievedinQB.Checked = Convert.ToBoolean(Payment.wirePaymentReceivedQB);
            cbxCheckCancelledQB.Checked = Convert.ToBoolean(Payment.checkCancelledQB);
            txtCheckNumber.Text = Payment.checkNumber;
            cbxCreditPaymentReceived.Checked = Convert.ToBoolean(Payment.creditPaymentReceivedQB);
            txtPaymentRemarks.Text = Payment.remarks;




        }
    }
    private void LoadDisposition()
    {
        if (!Page.IsPostBack)
        {
            Dispo = ndc.NonCon_Disposition.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (Dispo == null)
                return;
            cbxas_is.Checked = Convert.ToBoolean(Dispo.as_is);
            cbxreplace.Checked = Convert.ToBoolean(Dispo.replace);
            if (Dispo.vendor_return == "1")
            {
                cbxvendor_return.Checked = true;
            }
            else
            {
                cbxvendor_return.Checked = false;
            }

            cbxreturn_to_stock.Checked = Convert.ToBoolean(Dispo.return_to_stock);
            cbxquarantined.Checked = Convert.ToBoolean(Dispo.quarantined);
            txtcontainment_notes.Text = Dispo.containment_notes;
            cbxscrap.Checked = Convert.ToBoolean(Dispo.scrap);
            txtremarks.Text = Dispo.remarks;

        }
    }
    private void LoadDetails()
    {
        if (!Page.IsPostBack)
        {

            Details = ndc.NonCon_Details.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (Details == null)
                return;
            if (Details.isLeadsBodyIssue == true)
            {
                pnlLeadsBody.Style["display"] = "block";
                cbxLeadsBody.Checked = true;
            }
            else
            {
                pnlLeadsBody.Style["display"] = "none";
                cbxLeadsBody.Checked = false;
            }

            cbxRetin.Checked = Details.Retin ?? false;
            cbxOxidation.Checked = Details.Oxidation ?? false;
            cbxPull.Checked = Details.Pull ?? false;
            cbxRefurb.Checked = Details.Refurb ?? false;
            cbxGenDamage.Checked = Details.GenDamage ?? false;


            txtLeadsMemo.Text = Details.leadsMemo;
            cbxisNonconforming.Checked = Convert.ToBoolean(Details.isNonconforming);
            if (cbxisNonconforming.Checked == true)
                ddlisVendorFeedbackUpdated.SelectedValue = Details.IsVendorFeedbackUpdated;





            if (cbxisNonconforming.Checked == true)
            {
                pnlisNonconforming.Style["display"] = "block";
            }
            else
            {
                pnlisNonconforming.Style["display"] = "none";
            }


            //RMA Section
            LoadRMA();


        }
    }
    private void LoadApprovalOptions()
    {
        List<string> nonConApprovers = new List<string>() { "joem", "seanm", "christ", "kevint", "jenc" };

        if (!(Header.is_approved ?? false))
        {
            if (!nonConApprovers.Contains(System.Web.Security.Membership.GetUser().UserName))
                btnApproveNoncon.Enabled = false;
            else
                btnApproveNoncon.Enabled = true;
            return;
        }
        else
            btnApproveNoncon.Enabled = false;


    }
    //Header Cancel Button
    protected void btnCancelHeader_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonconSearch.aspx", false);

    }
    //Save Details
    protected void btnSubmitDetails_Click(object sender, EventArgs e)
    {
        try
        {
            SaveDetails();
            SaveRMA();
            //lblDetailSuccess.Text = "Success!  You have saved Details for NonConID: " + Details.NonConID.ToString();
            tools.HandleResult("success", "Success!  You have saved Details for NonConID: " + Details.NonConID.ToString());
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", "Error saving Details: " + ex.Message);
        }

    }
    private void SaveDetails()
    {
        Details = ndc.NonCon_Details.Where(w => w.NonConID == NonConID).FirstOrDefault();
        if (Details == null)
        {
            Details = new NonCon_Details();
            Details.NonConID = NonConID;
            Details.dateSubmitted = DateTime.Now;
            ndc.NonCon_Details.Add(Details);
        }

        if (Page.IsValid == true)
            //Check to see if Existing Details record for this NonConID

            Details.isNonconforming = cbxisNonconforming.Checked;
        if (cbxisNonconforming.Checked == true)
        {
            Details.IsVendorFeedbackUpdated = ddlisVendorFeedbackUpdated.SelectedValue;
        }
        if (cbxLeadsBody.Checked == true)
        {
            Details.isLeadsBodyIssue = true;

            if (cbxRetin.Checked)
                Details.Retin = true;
            else
                Details.Retin = false;

            if (cbxOxidation.Checked)
                Details.Oxidation = true;
            else
                Details.Oxidation = false;

            if (cbxPull.Checked)
                Details.Pull = true;
            else
                Details.Pull = false;

            if (cbxRefurb.Checked)
                Details.Refurb = true;
            else
                Details.Refurb = false;

            if (cbxGenDamage.Checked)
                Details.GenDamage = true;
            else
                Details.GenDamage = false;

            Details.leadsMemo = txtLeadsMemo.Text;
        }
        else
        {
            Details.isLeadsBodyIssue = false;
            Details.Retin = false;
            Details.Oxidation = false;
            Details.GenDamage = false;
            Details.leadsMemo = "";
        }

        Details.NonConID = NonConID;
        Details.submittedBy = System.Web.Security.Membership.GetUser().ToString();
        //nd.dateSubmitted = DateTime.Now;
        Details.detailsComplete = true;

        ndc.SaveChanges();



    }
    //RMA Section    
    protected void SaveRMA()
    {

        RMA = ndc.NonCon_RMA.Where(w => w.NonConID == NonConID).FirstOrDefault();
        if (RMA == null)
        {
            RMA = new SensibleDAL.ef.NonCon_RMA();
            RMA.NonConID = NonConID;
            RMA.dateSubmitted = DateTime.Now;
            ndc.NonCon_RMA.Add(RMA);
        }

        RMA.isVRMA = cbxisVRMA.Checked;
        if (txtVRMAQTY.Text.Length != 0)
            RMA.VRMAQTY = Convert.ToInt32(txtVRMAQTY.Text);
        RMA.isCRMA = cbxisCRMA.Checked;
        if (txtCRMAQTY.Text.Length != 0)
            RMA.CRMAQTY = Convert.ToInt32(txtCRMAQTY.Text);
        //Functionality Issue
        RMA.isFunctionalIssue = cbxisFunctionalIssue.Checked;
        RMA.fnSolderFail = cbxfnSolderFail.Checked;
        RMA.fnElecFail = cbxfnElecFail.Checked;
        RMA.fnCoplanFail = cbxfnCoplanFail.Checked;
        RMA.fnProgramFail = cbxfnProgramFail.Checked;
        RMA.fnRemarks = txtfnRemarks.Text;
        //Suspect Counterfeit
        RMA.isSuspectCounterfeit = cbxisSuspectCounterfeit.Checked;
        RMA.scSecondaryCoating = cbxscSecondaryCoating.Checked;
        RMA.scRemarked = cbxscRemarked.Checked;

        //RMA.scRefurb = cbxscRefurb.Checked;
        //RMA.scPulls = cbxscPulls.Checked;
        RMA.scCloned = cbxscCloned.Checked;
        RMA.scRemarks = txtscRemarks.Text;
        //Damage
        RMA.isDamaged = cbxisDamaged.Checked;
        RMA.dmCarrier = cbxdmCarrier.Checked;
        RMA.dmSMCReceipt = cbxdmSMCReceipt.Checked;
        RMA.dmCustReceipt = cbxdmCustReceipt.Checked;
        RMA.dmSMCWarehouse = cbxdmSMCWarehouse.Checked;
        RMA.dmProduct = cbxdmProduct.Checked;
        RMA.dmPackage = cbxdmPackage.Checked;
        RMA.dmRemarks = txtdmRemarks.Text;
        //Clerical
        RMA.isClerical = cbxisClerical.Checked;
        RMA.clSMC = cbxclSMC.Checked;
        RMA.clVendor = cbxclVendor.Checked;
        RMA.clCustomer = cbxclCustomer.Checked;
        RMA.clRemarks = txtclRemarks.Text;
        //Improper Packaging
        RMA.isImproperPKG = cbxisImproperPKG.Checked;
        RMA.ipContainer = cbxipContainer.Checked;
        RMA.ipMSL = cbxipMSL.Checked;
        RMA.ipESD = cbxipMSL.Checked;
        RMA.ipTandR = cbxipTandR.Checked;
        RMA.ipRemarks = txtipRemarks.Text;
        //RMA Remarks
        RMA.RMARemarks = txtRMARemarks.Text;
        RMA.submittedBy = System.Web.Security.Membership.GetUser().ToString();
        RMA.RMAComplete = true;
        //Submit Database Changes
        //ndc.NonCon_RMAs.InsertOnSubmit(RMA);
        //ndc.SubmitChanges();



        if (cbxisFunctionalIssue.Checked == false)
        {
            //reset functionality values on uncheck / save
            RMA.isFunctionalIssue = false;
            RMA.fnSolderFail = false;
            RMA.fnElecFail = false;
            RMA.fnCoplanFail = false;
            RMA.fnProgramFail = false;
            RMA.fnRemarks = null;

        }
        else
        {
            RMA.isFunctionalIssue = cbxisFunctionalIssue.Checked;
            RMA.fnSolderFail = cbxfnSolderFail.Checked;
            RMA.fnElecFail = cbxfnElecFail.Checked;
            RMA.fnCoplanFail = cbxfnCoplanFail.Checked;
            RMA.fnProgramFail = cbxfnProgramFail.Checked;
            RMA.fnRemarks = txtfnRemarks.Text;
        }
        //Suspect Counterfeit
        if (cbxisSuspectCounterfeit.Checked == false)
        {

            RMA.isSuspectCounterfeit = false;
            RMA.scSecondaryCoating = false;
            RMA.scRemarked = false;
            RMA.scRefurb = false;
            RMA.scPulls = false;
            RMA.scCloned = false;
            RMA.scRemarks = null;
        }
        else
        {
            RMA.isSuspectCounterfeit = cbxisSuspectCounterfeit.Checked;
            RMA.scSecondaryCoating = cbxscSecondaryCoating.Checked;
            RMA.scRemarked = cbxscRemarked.Checked;
            //RMA.scRefurb = cbxscRefurb.Checked;
            //RMA.scPulls = cbxscPulls.Checked;
            RMA.scCloned = cbxscCloned.Checked;
            RMA.scRemarks = txtscRemarks.Text;
        }

        //Damage
        if (cbxisDamaged.Checked == false)
        {
            //reset functionality values on uncheck / save
            RMA.isDamaged = false;
            RMA.dmCarrier = false;
            RMA.dmSMCReceipt = false;
            RMA.dmCustReceipt = false;
            RMA.dmSMCWarehouse = false;
            RMA.dmProduct = false;
            RMA.dmPackage = false;
            RMA.dmRemarks = null;
        }
        else
        {
            RMA.isDamaged = cbxisDamaged.Checked;
            RMA.dmCarrier = cbxdmCarrier.Checked;
            RMA.dmSMCReceipt = cbxdmSMCReceipt.Checked;
            RMA.dmCustReceipt = cbxdmCustReceipt.Checked;
            RMA.dmSMCWarehouse = cbxdmSMCWarehouse.Checked;
            RMA.dmProduct = cbxdmProduct.Checked;
            RMA.dmPackage = cbxdmPackage.Checked;
            RMA.dmRemarks = txtdmRemarks.Text;
        }

        //Clerical
        if (cbxisClerical.Checked == false)
        {
            //reset functionality values on uncheck / save
            RMA.isClerical = false;
            RMA.clSMC = false;
            RMA.clVendor = false;
            RMA.clCustomer = false;
            RMA.clRemarks = null;
        }
        else
        {
            RMA.isClerical = cbxisClerical.Checked;
            RMA.clSMC = cbxclSMC.Checked;
            RMA.clVendor = cbxclVendor.Checked;
            RMA.clCustomer = cbxclCustomer.Checked;
            RMA.clRemarks = txtclRemarks.Text;
        }

        //Improper Packaging
        if (cbxisImproperPKG.Checked == false)
        {
            //reset functionality values on uncheck / save
            RMA.isImproperPKG = false;
            RMA.ipContainer = false;
            RMA.ipMSL = false;
            RMA.ipESD = false;
            RMA.ipTandR = false;
            RMA.ipRemarks = null;
        }
        else
        {
            RMA.isImproperPKG = cbxisImproperPKG.Checked;
            RMA.ipContainer = cbxipContainer.Checked;
            RMA.ipMSL = cbxipMSL.Checked;
            RMA.ipESD = cbxipMSL.Checked;
            RMA.ipTandR = cbxipTandR.Checked;
            RMA.ipRemarks = txtipRemarks.Text;
        }

        RMA.isVRMA = cbxisVRMA.Checked;
        if (txtVRMAQTY.Text.Length != 0)
        {
            RMA.VRMAQTY = Convert.ToInt32(txtVRMAQTY.Text);
        }
        else
        {
            RMA.VRMAQTY = null;
        }
        RMA.isCRMA = cbxisCRMA.Checked;
        if (txtCRMAQTY.Text.Length != 0)
        {
            RMA.CRMAQTY = Convert.ToInt32(txtCRMAQTY.Text);
        }
        else
        {
            RMA.CRMAQTY = null;
        }


        //RMA Remarks
        RMA.RMARemarks = txtRMARemarks.Text;
        RMA.submittedBy = System.Web.Security.Membership.GetUser().ToString();
        ndc.SaveChanges();
        //lblRMASuccess.Text = "Success!  You have updated the RMA details for NonConID: " + RMA.NonConID.ToString();

    }
    protected void btnCancelRMA_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonconSearch.aspx", false);
    }
    protected void btnCancelDetails_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonconSearch.aspx", false);
    }
    //Save Disposition
    protected void btnSubmitDispo_Click(object sender, EventArgs e)
    {
        try
        {
            SaveDisposition();
            //lblDetailSuccess.Text = "Success!  You have saved Disposition for NonConID: " + Details.NonConID.ToString();
            tools.HandleResult("success", "Success!  You have saved Disposition for NonConID: " + Details.NonConID.ToString());
        }
        catch (Exception ex)
        {
            //lblDispoFail.Text = ex.Message;
            tools.HandleResult("fail", "Error saving Disposition: " + ex.Message);
        }
    }
    private void SaveDisposition()
    {
        Page.Validate("Disposition");
        if (Page.IsValid == true)
        {
            Dispo = ndc.NonCon_Disposition.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (Dispo == null)
            {
                Dispo = new SensibleDAL.ef.NonCon_Disposition();
                Dispo.NonConID = NonConID;
                Dispo.dateSubmitted = DateTime.Now;
                ndc.NonCon_Disposition.Add(Dispo);
            }
            Dispo.as_is = cbxas_is.Checked;
            Dispo.replace = cbxreplace.Checked;
            if (cbxvendor_return.Checked == true)
                Dispo.vendor_return = "1";
            else
                Dispo.vendor_return = "0";

            Dispo.return_to_stock = cbxreturn_to_stock.Checked;
            Dispo.quarantined = cbxquarantined.Checked;
            Dispo.containment_notes = txtcontainment_notes.Text;
            Dispo.remarks = txtremarks.Text;
            Dispo.scrap = cbxscrap.Checked;
            //Submit Database Changes            
            ndc.SaveChanges();


        }


    }

    private void SavePayment()
    {



        if (Page.IsValid == true)
        {
            Payment = ndc.NonCon_Payment.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (Payment == null)
            {
                Payment = new SensibleDAL.ef.NonCon_Payment();
                Payment.NonConID = NonConID;
                Payment.dateSubmitted = DateTime.Now;
                ndc.NonCon_Payment.Add(Payment);
            }

            //Intially reset all payment details fields, this way, if it's changed from one thing to another, won't leave traces of the other thing in the DB.
            Payment.checkNumber = "";
            Payment.wirePaymentReceivedQB = false;
            Payment.creditPaymentReceivedQB = false;
            Payment.paypalNetAmnt = null;
            Payment.checkCancelledQB = null;
            Payment.dateRefunded = null;
            Payment.paymentType = null;


            //Payment Type
            Payment.paymentType = ddlPaymenttype.SelectedItem.ToString();

            Payment.isPaymentDue = cbxisPaymentDue.Checked;
            Payment.isPaymentRefunded = cbxisPaymentRefunded.Checked;

            if (dtp_DateRefunded.SelectedDate != null)
                Payment.dateRefunded = Convert.ToDateTime(dtp_DateRefunded.SelectedDate);
            else
                Payment.dateRefunded = null;
            if (Payment.paymentType == "Wire")
            {
                //Wire
                Payment.wirePaymentReceivedQB = cbxWireRecievedinQB.Checked;
            }
            else if (Payment.paymentType == "Check")
            {
                //Check                
                Payment.checkCancelledQB = cbxCheckCancelledQB.Checked;
                Payment.checkNumber = txtCheckNumber.Text;
            }
            else if (Payment.paymentType == "Credit Card")
            {
                //Credit

                Payment.creditPaymentReceivedQB = cbxCreditPaymentReceived.Checked;
            }
            else if (Payment.paymentType == "PayPal")
            {
                //PayPal

                Payment.paypalNetAmnt = txtPayPalNetAmnt.Text;
            }




            //Payment.vendorCreditReceivedQB = cbxisVendorCredit.Checked;
            Payment.submittedBy = System.Web.Security.Membership.GetUser().ToString();
            Payment.paymentComplete = Convert.ToBoolean(1);
            Payment.remarks = txtPaymentRemarks.Text;
            Payment.NonConID = NonConID;

            //Submit Database Changes            
            ndc.SaveChanges();

        }

    }
    protected void btnCancelPayment_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonconSearch.aspx", false);
    }

    protected void btnNonConSummary_Click(object sender, EventArgs e)
    {
        //NONCON_RMADataContext ndc.= new NONCON_RMADataContext();
        Response.Redirect("NonconSummary.aspx?NonConID=" + NonConID, false);
    }
    protected void btnReSendEmail_Click(object sender, EventArgs e)
    {
        if (ValidatePage())
            sendEmail("Resend");
    }
    protected void btnSubmitCLosure_Click(object sender, EventArgs e)
    {
        try
        {
            if (ValidatePage())
            {
                Header = ndc.NonCon_Head.Where(w => w.NonConID == NonConID).FirstOrDefault();
                if(Header != null)
                {
                    Header.closure_submit_by = System.Web.Security.Membership.GetUser().UserName;
                    Header.closure_submit_date = DateTime.Now;
                    ndc.SaveChanges();
                    sendEmail("Closure");
                }
                

            }

               
        }
        catch(Exception ex)
        {

        }
        
    }
    protected void btnEditHeader_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonconHeader.aspx?NonConID=" + NonConID, false);
    }
    protected void btnDeleteNoncon_Click(object sender, EventArgs e)
    {
        try
        {

            if (Header.deleted == true)
            {
                btnDeleteNoncon.Text = "Undelete Nonconformance";
                UpdateRzNonCons(Header.NonConID, true);
                Header.deleted = false;
                //tools.JS_Modal_Redirect("You have successfully restored this nonconformance.", Request.RawUrl);

            }
            else
            {
                //ConfirmButtonExtender1.ConfirmText = "Are you sure you want to delete this nonconformance?";
                btnDeleteNoncon.Text = "Delete Nonconformance";
                UpdateRzNonCons(Header.NonConID, false);
                Header.deleted = true;
                //tools.JS_Modal_Redirect("You have successfully deleted this nonconformance.", "/secure_sm/NonCon/NonConSearch.aspx");
            }
            ndc.SaveChanges();
            Response.Redirect("/secure_sm/NonCon/NonConSearch.aspx", false);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    protected void UpdateRzNonCons(int NonConID, bool set)
    {
        //ArrayList ExistingRzStockNonCons = new ArrayList();        
        TheOrderLines = rdc.orddet_lines.Where(b => b.nonconid == NonConID).ToList();
        ThePartRecords = rdc.partrecords.Where(s => s.nonconid == NonConID).ToList();
        TheServiceLines = rdc.service_lines.Where(s => s.nonconid == NonConID).ToList();
        TheCanceledLines = rdc.orddet_line_canceleds.Where(w => w.nonconid == NonConID).ToList();

        if (set)
        {
            foreach (orddet_line l in TheOrderLines)
                l.nonconid = NonConID;
            foreach (partrecord p in ThePartRecords)
                p.nonconid = NonConID;
            foreach (service_line s in TheServiceLines)
                s.nonconid = NonConID;
        }
        else
        {
            foreach (orddet_line l in TheOrderLines)
                l.nonconid = null;
            foreach (partrecord p in ThePartRecords)
                p.nonconid = null;
            foreach (service_line s in TheServiceLines)
                s.nonconid = null;
        }
        rdc.SubmitChanges();

    }
    protected void btnSubmitPopup_Click(object sender, EventArgs e)
    {
        try
        {
            if (Header == null)
            {
                throw new Exception(lblPageError.Text = "The Header for this NonCon cannot be location.  Please choose or create a valid NonConID");
            }



            if (User.IsInRole("admin") | User.IsInRole("sm_nonconadmin"))
            {
                Header.isComplete = "0";
                Header.status = SM_Enums.NonConStatus.Open.ToString();
                Header.completedBy = null;
                Header.completedDate = null;

                //lblNonconComplete.Text = "";
                lbCompletionPanel.Visible = true;
                //btnNonConReopen.Visible = false;
                lblNonConStatus.Text = GetNonConStatusString(Header.status);
            }
            else
            {
                lblPageError.Text = "You are not authorized reopen a completed Nonconformance.  Please see your manager to request access.";
                lblPageError.ForeColor = System.Drawing.Color.Red;
                return;
            }

            Header.isComplete = "1";
            Header.status = SM_Enums.NonConStatus.Complete.ToString();
            Header.completedBy = System.Web.Security.Membership.GetUser().ToString();
            Header.completedDate = DateTime.Now;
            Header.ClosureRemarks = txtClosureRemarks.Text;
            ndc.SaveChanges();

            //Set UI Elements
            //lblNonconComplete.Text = "  -  Completed by " + Header.completedBy + " on: " + Convert.ToDateTime(Header.completedDate.ToString()).ToString("MM/dd/yyyy");
            lbCompletionPanel.Visible = false;
            lblNonConStatus.Text = GetNonConStatusString(Header.status);

        }
        catch (Exception ex)
        {
            if (ex.Message != null)
                lblPageError.Text = ex.Message.ToString();
        }

    }
    public void sendEmail(string type)
    {

        try
        {
            MailMessage Msg = new MailMessage();
            string SubmittedBy = Header.submittedBy;
            //string NonConID = nonconformance.NonConID.ToString();
            string NonConDate = Convert.ToDateTime(Header.NonConDate.ToString()).ToString("MM/dd/yyyy");
            string PartNumber = Header.partNumber;
            string nonconaddress = "https://portal.sensiblemicro.com/secure_sm/NonCon/Nonconformance.aspx?NonConID=" + Header.NonConID;
            string QTY = Header.QTY.ToString();
            string VendorName = Header.vendorName;
            string VendorPO = Header.vendorPO;
            string SONumber = null;
            string SaleID = null;
            string CustomerName = null;
            string Description = Header.Description;
            string isshortship;

            if (Header.isShortShip == true)
                isshortship = "Yes";
            else
                isshortship = "No";

            //if (Header.isSale == true)
            SensibleDAL.ef.NonCon_Customer theCustomer = ndc.NonCon_Customer.Where(w => w.NonConID == NonConID).FirstOrDefault();
            if (theCustomer != null)
            {
                SONumber = theCustomer.SONumber;
                CustomerName = theCustomer.customerName;
                SaleID = theCustomer.SONumberUID;
            }
            else
            {
                SONumber = "No Sales Order";
                CustomerName = "No Customer";
            }

            //Get Seller Email from Sale
            if (!string.IsNullOrEmpty(SaleID))
            {
                string sellerID = "";
                string sellerEmail = "";
                ordhed_sale s = rdc.ordhed_sales.Where(w => w.unique_id == SaleID).FirstOrDefault();
                if (s != null)
                    sellerID = s.base_mc_user_uid;
                n_user salesAgent = rdc.n_users.Where(w => w.unique_id == sellerID).FirstOrDefault();
                if (salesAgent != null)
                    sellerEmail = salesAgent.email_address;
                if (Tools.Email.IsEmailAddress(sellerEmail))
                    Msg.To.Add(sellerEmail);
            }



            Msg.Subject = "Nonconformance " + NonConID.ToString() + " created";
            if (System.Web.Security.Membership.GetUser().ToString() == "kevint")
            {
                Msg.To.Add("ktill@sensiblemicro.com");
            }
            else
            {

                if (type == "Closure")
                    Msg.To.Add("jcanizio@sensiblemicro.com");
                else
                    Msg.To.Add("noncon@sensiblemicro.com");
            }



            StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/NewNonconformance.htm"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;
            StrContent = StrContent.Replace("[nonconaddress]", nonconaddress);
            StrContent = StrContent.Replace("[ShortShip]", isshortship);
            StrContent = StrContent.Replace("[SubmittedBy]", SubmittedBy);
            StrContent = StrContent.Replace("[NonConDate]", NonConDate);
            StrContent = StrContent.Replace("[PartNumber]", PartNumber);
            StrContent = StrContent.Replace("[NonConID]", NonConID.ToString());
            StrContent = StrContent.Replace("[QTY]", QTY);
            StrContent = StrContent.Replace("[VendorName]", VendorName);
            StrContent = StrContent.Replace("[VendorPO]", VendorPO);
            StrContent = StrContent.Replace("[SONumber]", SONumber);
            //StrContent = StrContent.Replace("[CustomerName]", CustomerName);
            StrContent = StrContent.Replace("[Description]", Description);
            if (type == "Resend")
                Msg.Subject = SubmittedBy.ToString() + " is re-sending the nonconformance email for VendorPO: " + VendorPO;
            else if (type == "Closure")
                Msg.Subject = SubmittedBy.ToString() + " has submitted this nonconformance for closure approval.  VendorPO: " + VendorPO;
            else
                Msg.Subject = "NonconID " + NonConID + " has been approved.  VendorPO: " + VendorPO;
            Msg.Body = StrContent.ToString();
            Msg.IsBodyHtml = true;
            // your remote SMTP server IP.
            Msg.From = new MailAddress("noncon@sensiblemicro.com");
            SmtpClient smtp = new SmtpClient();
            smtp.Send(Msg);
            if (type == "Resend")
                lblPageSuccess.Text = "Nonconformance Email successfully re-sent.";
            else if (type == "Closure")
                lblPageSuccess.Text = "Nonconformance Email successfully submitted for closure.";

        }

        catch (Exception ex)
        {
            if (ex.Message != null)
                lblPageError.Text = ex.Message.ToString();
        }
    }
    public bool ValidatePage()
    {
        Page.Validate("NonConHead");
        if (Page.IsValid == true)
            return true;
        else
            return false;
    }
    protected void btnApproveNoncon_Click(object sender, EventArgs e)
    {
        try
        {
            bool alreadyApproved = (Header.is_approved ?? false);


            if (!alreadyApproved)
            {
                Header.is_approved = true;
                Header.approval_date = DateTime.Now;
                Header.approval_user = System.Web.Security.Membership.GetUser().UserName;
                Header.status = GetNonConStatusString(SM_Enums.NonConStatus.Open.ToString());
                sendEmail(null);

                tools.HandleResult("success", "You have successfully approved this noncon.");
                //Header.approval_email_sent = true;

            }
            else
            {
                //Already approved
                Header.is_approved = false;
                Header.approval_date = null;
                Header.approval_user = null;
                //Header.approval_email_sent = false;
                tools.HandleResult("success", "You have successfully un-approved this noncon.");

            }

            ndc.SaveChanges();
            LoadHeader();
            LoadTabs();
            //Response.Redirect("Nonconformance.aspx?NonConID=" + NonConID, false);


        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
    //Imagery
    protected void lbSaveImagery_Click(object sender, EventArgs e)
    {

        try
        {
            SaveImagery();

            tools.HandleResult("success", "Successfully saved all Imagery");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
    private void LoadImagery()
    {
        //Main Image Settings
        imcMainImage.InspectionID = NonConID;
        imcMainImage.InspectionType = "noncon";
        imcMainImage.SectionID = "main";
        //Other Imagery Gallery
        iig.InspectionID = NonConID;
        iig.InspectionType = "noncon";
        iig.SectionName = "other";
        iig.EnableControls = true;
        iig.ShowDescription = true;
        iig.EnableUpload = true;

        //Images List
        nonConImages = ql.LoadInspectionImagery("noncon", NonConID);

        if (nonConImages.Count <= 0)
            return;

        //Other Images
        List<insp_images> otherImages = nonConImages.Where(w => w.insp_section_id.Contains("other")).ToList();
        if (otherImages != null && otherImages.Count > 0)
            iig.LoadGallery(otherImages, SM_Enums.InspectionType.noncon.ToString().ToUpper(), NonConID, "other");

        //Header Image
        insp_images headerImage = nonConImages.Where(w => w.insp_section_id.Contains("main")).FirstOrDefault();
        if (headerImage != null)
            imcMainImage.init(headerImage);
    }
    private void SaveImagery()
    {
        //Main Image
        if (imcMainImage.hasFile)
            imcMainImage.SaveImageAndData();
    }
    //Header Logic
    private SensibleDAL.ef.NonCon_Head LoadHeaderObject()
    {
        SensibleDAL.ef.NonCon_Head ret;

        int intNonConID = 0;
        string strNonConID = Tools.Strings.SanitizeInput(Request.QueryString["NonConID"]);
        if (string.IsNullOrEmpty(strNonConID))
            return null;
        //Confirm valid int32
        if (!int.TryParse(strNonConID, out intNonConID))
            throw new Exception(strNonConID + " is not a valid NonCon ID");
        if (intNonConID == 0)
            throw new Exception(strNonConID + " is not a valid NonCon ID");
        //Set the ViewState Variable for NonConID

        //Header Section
        ret = ndc.NonCon_Head.Where(w => w.NonConID == intNonConID).FirstOrDefault();
        if (ret == null)
            throw new Exception("Could not locate a Non Con Header with ID: " + intNonConID);
        NonConID = intNonConID;
        Header = ret;
        return ret;



    }
    protected void LoadHeader()
    {
        if (Header == null)
            return;
        lblNonConStatus.Text = GetNonConStatusString(Header.status);
        dtp.SelectedDate = Convert.ToDateTime(Header.NonConDate);
        txtDescription.Text = Header.Description;
        if (Header.isShortShip ?? false)
            cbxShortShip.Checked = true;
        if (!string.IsNullOrEmpty(Header.partNumber))
            txtPartNumber.Text = Header.partNumber;
        if (!string.IsNullOrEmpty(Header.MFG))
            txtMFG.Text = Header.MFG;
        if (Header.QTY > 0)
            txtQTY.Text = Header.QTY.ToString();
        //Not all headers have a vendor (i.e. some stock sales and checks)
        if (!string.IsNullOrEmpty(Header.VendorUID))
        {
            ddlVendorName.SelectedValue = Header.VendorUID;
            //Vendor / PO Data
            if (!string.IsNullOrEmpty(Header.vendorPOID))
            {
                if (Header.vendorPOID != "1")//Unknown Vendor PO
                {
                    if (!rdc.ordhed_purchases.Where(w => w.unique_id == Header.vendorPOID).Any() && !rdc.ordhed_services.Where(w => w.unique_id == Header.vendorPOID).Any())
                        throw new Exception("No PO could be found in Rz matching unique_id " + Header.vendorPOID);
                }
                //Vendor DDL should already be loaded
                ddlVendorName.SelectedValue = Header.VendorUID;
                //Load the Vendor PODDL
                LoadVendorPODDL();
                ddlVendorPO.SelectedValue = Header.vendorPOID;
                pnlPoDetail.Visible = true;
            }
        }


        //Customer / Sales Data       
        NonConCustomer = ndc.NonCon_Customer.SingleOrDefault(c => c.NonConID == Header.NonConID);
        if (NonConCustomer != null)
        {

            //Customer and PO Labels
            if (!string.IsNullOrEmpty(NonConCustomer.SONumberUID))
                ddlSalesOrders.SelectedValue = NonConCustomer.SONumberUID;
            lblCustomerName.Text = NonConCustomer.customerName;
            lblCustomerPO.Text = NonConCustomer.customerPO ?? "N/A";

            if (!rdc.ordhed_sales.Where(w => w.unique_id == NonConCustomer.SONumberUID).Any())
                throw new Exception("No Sale could be found in Rz matching unique_id " + NonConCustomer.SONumberUID);
            //Load Sales Order DDL
            LoadSalesPanel(NonConCustomer.SONumberUID);
        }

        if (NonConCustomer != null)
        {

        }


        ////Load the Parts Grid       
        //object o = GetRzObjectFromLegacyQueries();
        //if (o == null)
        //    throw new Exception("Rz did not return a valid Legacy detail object");
        //LoadNonConPartsGrid(o);


    }
    private void LoadSalesPanel(string ordhed_sales_uid)
    {
        List<string> orderidList = new List<string>();
        orderidList.Add(ordhed_sales_uid);
        LoadSalesPanel(orderidList);
    }
    private void LoadSalesPanel(List<string> orderIDSalesList)
    {

        if (orderIDSalesList.Count > 0)
        {
            //Sales Search
            LoadSalesOrderDDL(orderIDSalesList);
        }

        pnlSaleDetail.Visible = true;
    }

    protected void ddlVendorName_SelectedIndexChanged(object sender, EventArgs e)
    {

        ResetDependentControls("ddlVendorName");

        if (ddlVendorName.SelectedValue == "0")
            return;
        if (ddlVendorName.SelectedValue == "1")
        {
            //We may be doing a noncon for sale line, linked to stock part with no vendor           
            //If it's for a sale line, it should be in stock, else it would be attached to a PO, waiting to be received.
            //Therefore, just do a Plain Part Search when we select Unknown Vendor.
            pnlPartrecordSearch.Visible = true;
        }

        else
            LoadVendorPODDL();



    }
    private void LoadVendorPODDL()
    {
        //Hide results from previous Query:
        ResetDependentControls("ddlVendorPO");
        ddlVendorPO.DataSource = null;
        ddlVendorPO.Items.Clear();
        ddlVendorPO.Items.Add(new ListItem("--Choose--", "0"));
        ddlVendorPO.Items.Add(new ListItem("--Unspecified PO--", "1"));
        ddlVendorPO.AppendDataBoundItems = true;
        pnlPoDetail.Visible = true;
        var poQuery = rdc.ordhed_purchases.Where(w => w.base_company_uid == ddlVendorName.SelectedValue).Select(s => new { s.unique_id, s.ordernumber }).Distinct();
        var poSvcquery = poQuery.Union(rdc.ordhed_services.Where(w => w.base_company_uid == ddlVendorName.SelectedValue).Select(s => new { s.unique_id, s.ordernumber }).Distinct());

        ddlVendorPO.DataSource = poSvcquery.OrderByDescending(o => o.ordernumber);
        ddlVendorPO.DataTextField = "ordernumber";
        ddlVendorPO.DataValueField = "unique_id";
        ddlVendorPO.DataBind();


    }
    protected void ddlVendorPO_SelectedIndexChanged(object sender, EventArgs e)
    {
        try

        {
            List<string> orderIDSalesList = new List<string>();
            ResetDependentControls("ddlVendorPO");
            if (ddlVendorPO.SelectedValue == "0")
                return;

            List<string> SalesOrderIdList = GetSalesOrderIdList();
            //Load Sales DDL even if no Vendor PO, could be a sold stock line.

            //If the list of SalesIDs is empty, then we are doing a stock check.
            if (SalesOrderIdList.Count <= 0)
            {
                //If stock check, will be partrecords asssociated with the vendor;
                if (ddlVendorName.SelectedValue.Length > 2)
                {
                    //LoadPartrecords_Vendor(ddlVendorName.SelectedValue);
                    //BuildLineObjectList();
                    //GatherLines();
                    //BuildLineObjectList();
                    LoadNonConPartsGrid();
                    //LoadNonConPartsGrid(ThePartRecords);
                }


            }
            else
                LoadSalesPanel(SalesOrderIdList);


        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }





    }
    private List<string> GetSalesOrderIdList()
    {
        //Returning an empty list will prompt stock search
        List<string> ret = new List<string>();
        //we have an unknown vendor, and unknown vendor po
        if (ddlVendorName.SelectedValue == "1" && ddlVendorPO.SelectedValue == "1")
        {
            //Stock Check of Unknown Vendor Source
            return ret;
        }
        //we have a known vendor, and an unknown vendor PO      
        else if (ddlVendorName.SelectedValue.Length > 2 && ddlVendorPO.SelectedValue.Length < 2)
        {
            //This could be a stock check (no sales orders) OR it could be a Vendor Stock Sale
            //In the case of a stock check, the sales orders will be empty.  Stock Check will be the only option for the ddl.
            ret = rdc.orddet_lines.Where(w => w.vendor_uid == ddlVendorName.SelectedValue).Select(s => s.orderid_sales).Distinct().ToList();
        }
        //we have a known vendor and an known vendor PO
        else if (ddlVendorName.SelectedValue.Length > 2 && ddlVendorPO.SelectedValue.Length > 2)
        {
            string POID = "";
            string ServiceID = "";

            //Check for Purchase Order
            POID = rdc.ordhed_purchases.Where(w => w.unique_id == ddlVendorPO.SelectedValue).Select(s => s.unique_id).FirstOrDefault();
            if (string.IsNullOrEmpty(POID))
                ServiceID = rdc.ordhed_services.Where(w => w.unique_id == ddlVendorPO.SelectedValue).Select(s => s.unique_id).FirstOrDefault();
            if (string.IsNullOrEmpty(POID) && string.IsNullOrEmpty(ServiceID))
                throw new Exception("No PO found with id: " + ddlVendorPO.SelectedValue);

            if (!string.IsNullOrEmpty(POID))
                ret = rdc.orddet_lines.Where(w => w.orderid_purchase == POID && (w.orderid_sales ?? "").Length > 0).Select(s => s.orderid_sales).Distinct().ToList();
            else
                ret = rdc.orddet_lines.Where(w => w.orderid_service == ServiceID && (w.orderid_sales ?? "").Length > 0).Select(s => s.orderid_sales).Distinct().ToList();


            //we may still not have a Sales ID if the PO referenced above was a lot buy, in which case, we need Sales Orders with this vendor and Partnumber
            if (ret.Count == 0)
            {
                //PO
                ret = rdc.orddet_lines.Where(w => w.vendor_uid == ddlVendorName.SelectedValue && (w.stocktype != "Buy")).Select(s => s.orderid_sales).Distinct().ToList();
                if (ret == null)//Service PO
                    ret = rdc.orddet_lines.Where(w => w.service_vendor_uid == ddlVendorName.SelectedValue && (w.stocktype != "Buy")).Select(s => s.orderid_sales).Distinct().ToList();
            }
        }
        return ret;
    }
    //private void LoadPartrecords_Vendor(string strVendorID)
    //{
    //    ThePartRecords = rdc.partrecords.Where(w => (w.stocktype.ToLower() == SM_Enums.StockType.consign.ToString() || w.stocktype.ToLower() == SM_Enums.StockType.stock.ToString()) && w.base_company_uid == strVendorID).ToList();


    //}
    private void LoadPartrecords_PartNumber(string partNumber)
    {
        ThePartRecords = rdc.partrecords.Where(w => (w.stocktype.ToLower() == SM_Enums.StockType.consign.ToString() || w.stocktype.ToLower() == SM_Enums.StockType.stock.ToString()) && w.fullpartnumber == partNumber).ToList();
    }
    private void LoadSalesOrderDDL(List<string> orderIDSalesList = null)
    {


        //Load DDL For Sales Orders
        List<ordhed_sale> saleList = rdc.ordhed_sales.Where(w => orderIDSalesList.Contains(w.unique_id)).Distinct().OrderByDescending(o => o.date_created).ToList();
        ddlSalesOrders.DataSource = null;
        ddlSalesOrders.Items.Clear();
        ddlSalesOrders.Items.Add(new ListItem("--Choose--", "0"));
        ddlSalesOrders.Items.Add(new ListItem("--Stock Check--", "1"));
        ddlSalesOrders.AppendDataBoundItems = true;
        ddlSalesOrders.DataSource = saleList.OrderByDescending(o => o.ordernumber);
        ddlSalesOrders.DataTextField = "ordernumber";
        ddlSalesOrders.DataValueField = "unique_id";
        ddlSalesOrders.DataBind();


        ddlSalesOrders.Visible = true;



    }
    private void ResetDependentControls(string s = null)
    {

        //gvParts is dependent on all downstream elements.
        ResetGridviewPanel();
        ResetPartSearchPanel();
        if (string.IsNullOrEmpty(s))
            return;
        //rzpa.Clear();        
        switch (s)
        {
            case "ddlVendorName":
                {
                    ResetVendorPOPanel();
                    ResetSalesPanel();
                    break;
                }


            case "ddlVendorPO":
                {
                    ResetSalesPanel();
                    break;
                }
        }


    }
    private void ResetPartSearchPanel()
    {
        pnlPartrecordSearch.Visible = false;
        rzpa.Clear();
    }
    private void ResetGridviewPanel()
    {
        smdtPartData.dataSource = null;
        smdtPartData.loadGrid();
        pnlPartData.Visible = false;

    }
    private void ResetSalesPanel()
    {
        ddlSalesOrders.DataSource = null;
        ddlSalesOrders.Items.Clear();
        ddlSalesOrders.DataBind();
        ddlSalesOrders.Visible = false;
        pnlSaleDetail.Visible = false;

    }
    private void ResetVendorPOPanel()
    {
        ddlVendorPO.DataSource = null;
        ddlVendorPO.Items.Clear();
        ddlVendorPO.DataBind();
        pnlPoDetail.Visible = false;


    }
    protected void ddlSalesOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //GatherLines();

            ResetDependentControls("dlSalesOrders");
            //This will gather lines, and build lineObject List.  If after this, no LineObjects, show part search.
            LoadNonConPartsGrid();
            if (lineObjectList.Count <= 0)
                pnlPartrecordSearch.Visible = true;


        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void GatherLines()
    {
        //List<orddet_line> lines = new List<orddet_line>();
        //Also include canceled lines, as lines that are short shipped will be canceled
        string poid = ddlVendorPO.SelectedValue;
        string soid = ddlSalesOrders.SelectedValue;

        ////Set the company name label
        //if (ddlSalesOrders.SelectedValue == "0")
        //    return;


        TheServiceLines.Clear();
        ThePartRecords.Clear();
        TheCanceledLines.Clear();
        TheOrderLines.Clear();

        //REgular lines
        TheOrderLines = GatherOrderLines(soid, poid);
        TheCanceledLines = GatherCanceledLines(soid, poid);
        if (ddlVendorName.SelectedValue == "1")
            ThePartRecords = GatherPartrecords(soid, poid);
        TheServiceLines = GatherServiceLines(soid, poid);
        BuildLineObjectList();





    }

    private List<service_line> GatherServiceLines(string soid, string poid)
    {

        List<string> salesLineIds = new List<string>();
        List<string> purchaseLineIds = new List<string>();


        List<service_line> ret = new List<service_line>();
        if (!string.IsNullOrEmpty(soid))
        {
            salesLineIds = rdc.orddet_lines.Where(w => w.orderid_sales == soid).Select(s => s.unique_id).Distinct().ToList();
            ret.AddRange(rdc.service_lines.Where(w => salesLineIds.Contains(w.the_orddet_line_uid)));
        }
        if (!string.IsNullOrEmpty(poid))
        {
            //lblPartSelectionGrid.Text += "  Canceled PO Lines: ";
            purchaseLineIds = rdc.orddet_lines.Where(w => w.orderid_purchase == poid).Select(s => s.unique_id).Distinct().ToList();
            ret.AddRange(rdc.service_lines.Where(w => purchaseLineIds.Contains(w.the_orddet_line_uid)));
        }


        ////include existing Noncons
        //if (NonConID > 0)
        //    ret.AddRange(rdc.service_lines.Where(w => w.nonconid == NonConID).ToList());
        return ret;
    }

    private List<partrecord> GatherPartrecords(string soid, string poid)
    {

        List<partrecord> ret = new List<partrecord>();
        if (!string.IsNullOrEmpty(soid))
        {

        }
        if (!string.IsNullOrEmpty(poid))
        {
            //lblPartSelectionGrid.Text += "  Canceled PO Lines: ";
            ret.AddRange(rdc.partrecords
           .Where(w => w.buy_purchase_id == poid)
           .ToList());
        }
        ////include existing Noncons
        //if (NonConID > 0)
        //    ret.AddRange(rdc.partrecords.Where(w => w.nonconid == NonConID).ToList());

        return ret;

    }

    private List<orddet_line_canceled> GatherCanceledLines(string soid, string poid)
    {

        List<orddet_line_canceled> ret = new List<orddet_line_canceled>();
        if (!string.IsNullOrEmpty(soid))
        {
            ret.AddRange(rdc.orddet_line_canceleds
           .Where(w => w.orderid_sales == soid)
           .ToList());
            //lblPartSelectionGrid.Text += "  Canceled Sales Lines: ";

        }
        if (!string.IsNullOrEmpty(poid))
        {
            //lblPartSelectionGrid.Text += "  Canceled PO Lines: ";
            ret.AddRange(rdc.orddet_line_canceleds
           .Where(w => w.orderid_purchase == poid)
           .ToList());
        }

        ////include existing Noncons
        //if (NonConID > 0)
        //    ret.AddRange(rdc.orddet_line_canceleds.Where(w => w.nonconid == NonConID).ToList());

        return ret;
    }



    private List<orddet_line> GatherOrderLines(string soid, string poid)
    {
        //There is overlap here, some lines associate with both a po and an so, will de-dupe at the end.
        List<orddet_line> poRelatedLines = new List<orddet_line>();
        List<orddet_line> soRelatedLines = new List<orddet_line>();


        List<orddet_line> ret = new List<orddet_line>();
        if (ddlSalesOrders.SelectedValue == "1") //Stock Check, just reference Lines from the PO
        {

            //ret = rdc.orddet_lines.Where(w => w.orderid_purchase == ddlVendorPO.SelectedValue).ToList();
            if (ddlVendorPO.SelectedValue.Length > 2)
            {
                poRelatedLines = rdc.orddet_lines.Where(w => w.orderid_purchase == ddlVendorPO.SelectedValue).ToList();
                lblPartSelectionGrid.Text = "  Purchase Order: " + ddlVendorPO.SelectedItem;
            }

        }
        else if (!string.IsNullOrEmpty(soid))
        {
            //Grab sales lines related to selection.
            if (ddlSalesOrders.SelectedValue.Length > 2)
            {
                soRelatedLines = rdc.orddet_lines.Where(w => w.orderid_sales == soid).ToList();
                ordhed_sale s = rdc.ordhed_sales.Where(w => w.unique_id == soid).FirstOrDefault();
                lblPartSelectionGrid.Text = "  Sales Order: " + s.ordernumber;
                lblCustomerName.Text = s.companyname;
                lblCustomerPO.Text = s.orderreference;
            }

        }


        //Combine and dedupe line items.
        foreach (orddet_line l in soRelatedLines)
            if (!ret.Contains(l))
                ret.Add(l);
        foreach (orddet_line l in poRelatedLines)
            if (!ret.Contains(l))
                ret.Add(l);
        //if (!string.IsNullOrEmpty(poid))
        //    ret.AddRange(rdc.orddet_lines.Where(w => w.orderid_purchase == poid).ToList());



        ////include existing Noncons
        //if (NonConID > 0)
        //    ret.AddRange(rdc.orddet_lines.Where(w => w.nonconid == NonConID).ToList());
        return ret;
    }

    //protected void gvParts_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        CheckBox cb = new CheckBox();
    //        cb.ID = "cbxPartSelect";
    //        int cell = 0;
    //        //Clear existing values
    //        e.Row.Cells[cell].Text = string.Empty;
    //        //Add the Control
    //        e.Row.Cells[cell].Controls.Add(cb);
    //    }
    //}

    protected void gvParts_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        // <asp:CheckBox ID="cbxPartSelect" runat="server" />
        btnAutofillPartData.Visible = true;
        if (e.Row.Cells.Count > 1)// When I bind null to clear grid, row count will == 1, thus do nothing for empty grids
        {
            //e.Row.Cells[4].CssClass = "hiddencol";//PurchaseID
            //e.Row.Cells[6].CssClass = "hiddencol";//SalesID
            //e.Row.Cells[8].CssClass = "hiddencol";//Unique_id
            e.Row.Cells[8].CssClass = "hiddencol";//Unique_id
            e.Row.Cells[9].CssClass = "hiddencol";//PurchaseID
            e.Row.Cells[10].CssClass = "hiddencol";//SalesID


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Set Checkbox Values

                CheckBox cb = new CheckBox();
                cb.ID = "cbxPartSelect";
                int cell = 0;
                //Clear existing values
                e.Row.Cells[cell].Text = string.Empty;
                //Add the Control
                e.Row.Cells[cell].Controls.Add(cb);


                //If there is a valid NonCon ID, check the box
                string nonConCellValue = e.Row.Cells[6].Text;
                if (NonConID > 0)
                    cb.Checked = nonConCellValue == NonConID.ToString();
            }
        }
    }
    protected void btnSaveNonConformance_Click(object sender, EventArgs e)
    {
        try
        {
            SaveAll();

            //IF the page doesn't have a querystring, then redirect to one that does           
            if (Request.QueryString["NonConID"] == null)
                RedirectNewNoncon();
            else
                tools.HandleResult("success", "Nonconformance saved!  <em><a href=\"NonconSearch.aspx\">Return to Search ... </a></em>", 0);




        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message + ex.InnerException);
        }
    }
    private void SaveAll()
    {
        SaveHeader();
        //GatherLines();
        //Send the email AFTER the noncon is created
        bool approvalEmailSent = Header.approval_email_sent ?? false;
        if (!approvalEmailSent)
            SendApprovalRequestEmail();

        //With the ID we can save values back to Rz.
        SaveRzDetailData();
        SaveDetails();
        SaveRMA();
        SaveDisposition();
        SavePayment();
        SaveImagery();

    }
    protected string ValidateFields()
    {

        string inValidFields = null;
        if (String.IsNullOrEmpty(txtPartNumber.Text))
        {
            inValidFields += "Part Number";
            if (inValidFields.Length > 0)
                inValidFields += ", ";
        }


        if (String.IsNullOrEmpty(txtMFG.Text))
        {
            inValidFields += "Manufacturer";
            if (inValidFields.Length > 0)
                inValidFields += ", ";
        }
        if (String.IsNullOrEmpty(txtQTY.Text))
        {
            inValidFields += "Nonconforming QTY";
            if (inValidFields.Length > 0)
                inValidFields += ", ";
        }
        if (String.IsNullOrEmpty(txtDescription.Text))
            inValidFields += "Description";

        if (!string.IsNullOrEmpty(inValidFields))
            inValidFields = "Critical Information Missing: " + inValidFields;
        return inValidFields;
    }
    protected void SaveHeader()
    {
        Page.Validate("NonConHead");
        if (Page.IsValid == true)
        {

            string invalidFields = ValidateFields();
            if (!string.IsNullOrEmpty(invalidFields))
                throw new Exception(invalidFields);
            //LoadHeaderObject();
            if (Header == null)
            {
                Header = new SensibleDAL.ef.NonCon_Head();
                Header.dateSubmitted = DateTime.Now;
                Header.NonConDate = DateTime.Now;
                Header.submittedBy = System.Web.Security.Membership.GetUser().UserName;
                Header.status = GetNonConStatusString(SM_Enums.NonConStatus.PendingApproval.ToString());
                ndc.NonCon_Head.Add(Header);
            }


            //Vendor Name
            Header.vendorName = ddlVendorName.SelectedItem.ToString();
            Header.VendorUID = ddlVendorName.SelectedValue;

            //Vendor PO
            if (!string.IsNullOrEmpty(ddlVendorPO.SelectedValue))
            {
                //Can be null if Unknown Vendor.
                Header.vendorPO = ddlVendorPO.SelectedItem.ToString();
                Header.vendorPOID = ddlVendorPO.SelectedValue;
            }



            if (cbxShortShip.Checked)
                Header.isShortShip = true;
            else
                Header.isShortShip = false;

            Header.Description = txtDescription.Text;
            if (dtp.SelectedDate != DateTime.MinValue)
                Header.NonConDate = Convert.ToDateTime(dtp.SelectedDate.Value);
            Header.QTY = Convert.ToInt32(txtQTY.Text);
            Header.MFG = txtMFG.Text;
            Header.partNumber = txtPartNumber.Text;


            ndc.SaveChanges();
            //We should have a NonConID at this point.
            NonConID = Header.NonConID;
            //Sale Data
            if (ddlSalesOrders.SelectedValue != "1")
            {
                SaveCusomerAndSaleData();
            }

        }
    }
    private void RedirectNewNoncon()
    {
        string returnUrl = "Nonconformance.aspx?NonConID=" + Header.NonConID;
        string currentUrl = Request.Url.ToString();
        if (currentUrl != returnUrl)
            Response.Redirect(returnUrl, false);
    }
    private string GetNonConStatusString(string statusString)
    {
        //Pending Approval
        //Open
        //Pending Closure
        //Short-Ship
        //Pending
        //Complete
        switch (statusString)
        {
            case "Pending":
            case "PendingApproval":
            case "Pending Approval":
                //return SM_Enums.NonConStatus.PendingApproval.ToString();
                return "Pending Approval";
            case "Open":
                return "Open";
            case "Pending Closure":
            case "PendingCompletion":
                return "Pending Completion";
            case "Complete":
                {
                    return "Complete <em>(" + Header.completedBy + " - " + Header.completedDate.Value.ToShortDateString() + ")</em>";
                }

            case "Short-Ship":
            case "ShortShip":
                return "Short Ship";

        }

        return null;
    }
    private void SaveCusomerAndSaleData()
    {
        ordhed_sale Sale = null;
        NonConCustomer = ndc.NonCon_Customer.FirstOrDefault(c => c.NonConID == NonConID);
        if (ddlSalesOrders.SelectedValue == null || ddlSalesOrders.SelectedValue.Length <= 0)
            return;
        if (NonConCustomer == null)
        {
            NonConCustomer = new SensibleDAL.ef.NonCon_Customer();
            NonConCustomer.NonConID = NonConID;
            NonConCustomer.submittedBy = System.Web.Security.Membership.GetUser().UserName;
            NonConCustomer.dateSubmitted = DateTime.Now;
            ndc.NonCon_Customer.Add(NonConCustomer);
        }

        string selectedSaleUID = ddlSalesOrders.SelectedValue;
        if (!string.IsNullOrEmpty(selectedSaleUID))
            Sale = rdc.ordhed_sales.Where(w => w.unique_id == selectedSaleUID).FirstOrDefault();

        if (Sale == null)
            throw new Exception("Unable to save Customer Sale data, no sale found with uid: " + selectedSaleUID);

        NonConCustomer.CustomerUID = Sale.base_company_uid;
        NonConCustomer.customerName = Sale.companyname;
        NonConCustomer.sellerName = Sale.agentname;
        NonConCustomer.SONumber = Sale.ordernumber;
        NonConCustomer.SONumberUID = Sale.unique_id;
        NonConCustomer.customerPO = Sale.orderreference;
        var Seller = rdc.n_users.SingleOrDefault(u => u.unique_id == Sale.base_mc_user_uid);
        if (Seller != null)
            NonConCustomer.sellerEmail = Seller.email_address;

    }
    protected void SaveRzDetailData()
    {

        List<string> addNonConRzLineIds = new List<string>();
        List<string> removeNonConRzLineIds = new List<string>();
        foreach (GridViewRow row in smdtPartData.theGridView.Rows)
        {


            //We could have a CHECKED row, that needs saving, or an UNCHECKED row that needs clearing.  
            CheckBox check = (CheckBox)row.FindControl("cbxPartSelect");
            string existingNonConID = row.Cells[6].Text;
            //Don't disrupt NonCons belonging to another order.
            if (existingNonConID != "0")
                if (existingNonConID != NonConID.ToString())
                    continue;
            //Check the detailID for this row
            string detailId = row.Cells[8].Text;
            if (string.IsNullOrEmpty(detailId))
                continue;

            if (check.Checked)
            {
                //Take Row information from each column (Cell) and display it

                if (!addNonConRzLineIds.Contains(detailId))
                    addNonConRzLineIds.Add(detailId);
            }
            else if (!string.IsNullOrEmpty(existingNonConID))
            {
                //IF unchecked BUT Existing NonConID, add it so we can clear the check
                int lineNonConID = 0;
                int.TryParse(existingNonConID, out lineNonConID);
                if (lineNonConID > 0)
                    removeNonConRzLineIds.Add(detailId);
            }

        }


        //bool notValid = ProcessValidNonconRzObject(detailID);
        //if (notValid)
        //    throw new Exception("The Selected line is not valid.  It may belong to an existing Nonconformance, check the grid.");
        ////Set the NonCon ID.
        SetNonConIdForRzLine(addNonConRzLineIds, removeNonConRzLineIds);

        //Set the Rz Object ID on the Header        
        //Header.partNumberID = detailID;
        ndc.SaveChanges();
        LoadNonConPartsGrid();


    }
    private void SetNonConIdForRzLine(List<string> addLineIds, List<string> removeLineIDs)
    {


        GatherLines();
        //string alreadyExistsMessage = "This line is already associated with NonConID: ";

        foreach (orddet_line l in TheOrderLines)
        {
            if (!BelongsToAnotherNonCon(l))
            {
                if (addLineIds.Contains(l.unique_id))
                    l.nonconid = NonConID;
                else if (removeLineIDs.Contains(l.unique_id))
                    l.nonconid = 0;
            }


        }
        foreach (service_line s in TheServiceLines)
        {
            if (!BelongsToAnotherNonCon(s))
            {
                if (addLineIds.Contains(s.unique_id))
                    s.nonconid = NonConID;
                else if (removeLineIDs.Contains(s.unique_id))
                    s.nonconid = 0;
            }
        }
        foreach (partrecord p in ThePartRecords)
        {
            if (!BelongsToAnotherNonCon(p))
            {
                if (addLineIds.Contains(p.unique_id))
                    p.nonconid = NonConID;
                else if (removeLineIDs.Contains(p.unique_id))
                    p.nonconid = 0;
            }
        }
        foreach (orddet_line_canceled c in TheCanceledLines)
        {
            if (!BelongsToAnotherNonCon(c))
            {
                if (addLineIds.Contains(c.unique_id))
                    c.nonconid = NonConID;
                else if (removeLineIDs.Contains(c.unique_id))
                    c.nonconid = 0;
            }


        }
        rdc.SubmitChanges();

    }

    private bool BelongsToAnotherNonCon(object o)
    {
        int objectNonConID = 0;
        if (o is orddet_line)
        {
            orddet_line l = (orddet_line)o;
            objectNonConID = l.nonconid ?? 0;

        }
        else if (o is service_line)
        {
            service_line l = (service_line)o;
            objectNonConID = l.nonconid ?? 0;

        }
        else if (o is partrecord)
        {
            partrecord l = (partrecord)o;
            objectNonConID = l.nonconid ?? 0;

        }
        else if (o is orddet_line_canceled)
        {
            orddet_line_canceled l = (orddet_line_canceled)o;
            objectNonConID = l.nonconid ?? 0;

        }

        //Not yet associated - OK
        if (objectNonConID == 0)
            return false;
        //Belongs to this NonCon - OK
        if (objectNonConID == NonConID)
            return false;

        //Already belongs to another noncon
        return true;
    }

    //private bool ProcessValidNonconRzObject(string detailID)
    //{
    //    //string type = selectedRow.Cells[11].Text;
    //    //string detailID = selectedRow.Cells[9].Text;
    //    orddet_line l;
    //    service_line s;
    //    partrecord p;
    //    orddet_line_canceled c;

    //    l = rdc.orddet_lines.Where(w => w.unique_id == detailID).FirstOrDefault();
    //    if (l != null)
    //    {
    //        if (l.nonconid > 0)
    //        {
    //            //If there is a nonconid, it needs to be same, or is conflict
    //            if (l.nonconid != NonConID)
    //                return true;
    //        }
    //        TheOrderLines.Add(l);
    //        return false;
    //    }


    //    s = rdc.service_lines.Where(w => w.unique_id == detailID).FirstOrDefault();
    //    if (s != null)
    //    {
    //        if (s.nonconid > 0)
    //        {
    //            //If there is a nonconid, it needs to be same, or is conflict
    //            if (s.nonconid != NonConID)
    //                return true;
    //        }
    //        TheServiceLines.Add(s);
    //        return false;
    //    }

    //    p = rdc.partrecords.Where(w => w.unique_id == detailID).FirstOrDefault();
    //    if (p != null)
    //    {
    //        if (p.nonconid > 0)
    //        {
    //            //If there is a nonconid, it needs to be same, or is conflict
    //            if (p.nonconid != NonConID)
    //                return true;
    //        }
    //        ThePartRecords.Add(p);
    //        return false;
    //    }

    //    c = rdc.orddet_line_canceleds.Where(w => w.unique_id == detailID).FirstOrDefault();
    //    if (c != null)
    //    {
    //        if (c.nonconid > 0)
    //        {
    //            //If there is a nonconid, it needs to be same, or is conflict
    //            if (c.nonconid != NonConID)
    //                return true;
    //        }
    //        TheCanceledLines.Add(p);
    //        return false;
    //    }





    //    return false;
    //}
    //Approval / Completion
    protected void SendApprovalRequestEmail()
    {


        //Send email to noncon group as well as sales agent.
        //NonConGroup
        string SubmittedBy = Header.submittedBy;
        string NonConDate = Convert.ToDateTime(Header.NonConDate.ToString()).ToString("MM/dd/yyyy");
        string PartNumber = Header.partNumber;
        string nonconaddress = "https://portal.sensiblemicro.com/secure_sm/NonCon/Nonconformance.aspx?NonConID=" + NonConID;
        string QTY = Header.QTY.ToString();
        string VendorName = Header.vendorName;
        string VendorPO = Header.vendorPO;
        string CustomerName = null;
        string SONumber = null;
        if (NonConCustomer != null)
        {
            if (!string.IsNullOrEmpty(NonConCustomer.SONumber))
            {
                SONumber = NonConCustomer.SONumber;
                CustomerName = NonConCustomer.customerName;
            }
        }
        else
        {
            SONumber = "No Sale";
            CustomerName = "No Customer";
        }
        string Description = Header.Description;
        MailMessage Msg = new MailMessage();
        Msg.From = new MailAddress("noncon_approval@sensiblemicro.com", "Noncon Approval");
        //if (Membership.GetUser().UserName == "kevint")
        //    Msg.To.Add("ktill@sensiblemicro.com");
        //else
        Msg.To.Add("noncon@sensiblemicro.com");


        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/noncon_approval.htm"));
        string readFile = reader.ReadToEnd();
        string StrContent = "";
        StrContent = readFile;
        //Here replace the Link with [nonconaddress]
        Msg.Subject = "Noncon Approval Needed: ";
        if (Header.isShortShip ?? false)
        {
            StrContent = StrContent.Replace("[ShortShip]", "Short-Ship");
            Msg.Subject = SubmittedBy.ToString() + " has created a new Short-Ship nonconformance for VendorPO: " + VendorPO;
        }
        else
        {
            StrContent = StrContent.Replace("[ShortShip]", "N/A");
            Msg.Subject += SubmittedBy.ToString() + " has created a new nonconformance for VendorPO: " + VendorPO;
        }
        StrContent = StrContent.Replace("[nonconaddress]", nonconaddress);
        StrContent = StrContent.Replace("[SubmittedBy]", SubmittedBy);
        StrContent = StrContent.Replace("[NonConDate]", NonConDate);
        StrContent = StrContent.Replace("[PartNumber]", PartNumber);
        StrContent = StrContent.Replace("[NonConID]", Header.NonConID.ToString());
        StrContent = StrContent.Replace("[QTY]", QTY);
        StrContent = StrContent.Replace("[VendorName]", VendorName);
        StrContent = StrContent.Replace("[VendorPO]", VendorPO);
        StrContent = StrContent.Replace("[CustomerName]", CustomerName);
        StrContent = StrContent.Replace("[SONumber]", SONumber);
        StrContent = StrContent.Replace("[Description]", Description);

        Msg.Body = StrContent.ToString();
        Msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.Send(Msg);

        Header.approval_email_sent = true;
        ndc.SaveChanges();

    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonconSearch.aspx", false);
    }
    protected void lbCompleteNonCon_Click(object sender, EventArgs e)
    {
        if (User.IsInRole("admin") | User.IsInRole("sm_nonconadmin"))
        {


            Header.isComplete = "1";
            Header.status = SM_Enums.NonConStatus.Complete.ToString();
            Header.completedBy = System.Web.Security.Membership.GetUser().UserName;
            Header.completedDate = DateTime.Now;
            ndc.SaveChanges();
            //lblNonconComplete.Text = "";
            lbCompletionPanel.Visible = true;
            lblNonConStatus.Text = GetNonConStatusString(Header.status);

        }
        else
        {
            lblPageError.Text = "You are not authorized to complete this Nonconformance.  Please see your manager to request access.";
            lblPageError.ForeColor = System.Drawing.Color.Red;

        }
        LoadButtons();

    }
    protected void lbUncompleteNonCon_Click(object sender, EventArgs e)
    {

        if (User.IsInRole("admin") | User.IsInRole("sm_nonconadmin"))
        {


            Header.isComplete = "0";
            Header.status = SM_Enums.NonConStatus.Open.ToString();
            Header.completedBy = null;
            Header.completedDate = null;
            ndc.SaveChanges();
            //lblNonconComplete.Text = "";
            lbCompletionPanel.Visible = true;
            lblNonConStatus.Text = GetNonConStatusString(Header.status);

        }
        else
        {
            lblPageError.Text = "You are not authorized to reopen a completed Nonconformance.  Please see your manager to request access.";
            lblPageError.ForeColor = System.Drawing.Color.Red;

        }
        LoadButtons();
    }
    //Report
    protected void lbGenerateReport_Click(object sender, EventArgs e)
    {
        GeneratePdfReport();
    }
    private void GeneratePdfReport()
    {
        SM_Quality_Logic smq = new SM_Quality_Logic();
        smq.CreateNonconPDF(NonConID);



    }
}