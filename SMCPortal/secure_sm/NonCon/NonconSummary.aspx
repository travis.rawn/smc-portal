﻿<%@ Page Title="Nonconformance Summary" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="NonconSummary.aspx.cs" Inherits="secure_sm_NonCon_NonconSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
     <div id="pageTitle">
        <h1>NONCONFORMANCE SUMMARY</h1>
    </div>   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
  
  <%--  Print Specific Div--%>
     <script type="text/javascript">
         function printDiv(divID) {
             //Get the HTML of div
             var divElements = document.getElementById(divID).innerHTML;
             //Get the HTML of whole page
             var oldPage = document.body.innerHTML;
             //Reset the page's HTML with div's HTML only
             document.body.innerHTML =
               "<html><head><title></title></head><body>" +
               divElements + "</body>";
             //Print Page
             window.print();
             //Restore orignal HTML
             document.body.innerHTML = oldPage;
         }
    </script>
   
    <asp:Label ID="lblPageError" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#CC0000"></asp:Label>
    <asp:Button ID="btnPrintSummary" runat="server" Text="Print Summary" OnClientClick="javascript: printDiv('NonConSummary')"/>
    <asp:Button ID="btnReturnNonCon" runat="server" Text="Return to Nonconformance" OnClick="btnReturnNonCon_Click" />
    <div id="NonConSummary" align:"left" Width="700px">
        <div style= "text-align:left">
        <p style="text-align:right"><%--<asp:Label ID="lblISOFormNumber" runat="server" Text="F-830-001 Rev J" Font-Bold="True"></asp:Label>--%></p><br />
        <asp:Label ID="lblSummaryTitle" runat="server" Text="NonConformance Summary" Font-Bold="True" Font-Italic="False" Font-Size="X-Large"></asp:Label><br />
        <asp:Label ID="lblNonConHead" runat="server" Text="Header Information" Font-Bold="True" Font-Size="Large"></asp:Label>
        
        <asp:DetailsView ID="dtvNonConSummary" runat="server" Height="50px" Width="700px" FieldHeaderStyle-Width="150" RowStyle-HorizontalAlign="Left" AutoGenerateRows="False">
        <FieldHeaderStyle Width="150px"></FieldHeaderStyle>
        <Fields>
            <asp:BoundField DataField="NonConID" HeaderText="ID" SortExpression="NonConID"/>
            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="SubmittedBy" HeaderText="Submited By" SortExpression="Submited By" />
            <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
            <asp:BoundField DataField="CustomerPO" HeaderText="Customer PO" SortExpression="Customer PO" />
            <asp:BoundField DataField="Sale" HeaderText="SO #" SortExpression="SO #" />
            <asp:BoundField DataField="Vendor" HeaderText="Vendor" SortExpression="Vendor" />
            <asp:BoundField DataField="VendorPO" HeaderText="VendorPO" SortExpression="VendorPO" />
            <asp:BoundField DataField="ContainmentNotes" HeaderText="Containment Notes" SortExpression="Containment Notes" />            
            <asp:BoundField DataField="PartNumber" HeaderText="Part Number" SortExpression="Part Number" />                      
            <asp:BoundField DataField="QTY" HeaderText="QTY" SortExpression="QTY" />            
        </Fields>
<RowStyle HorizontalAlign="Left"></RowStyle>
        </asp:DetailsView>
        <br />
            <asp:Label ID="lblDispo" runat="server" Text="Disposition" Font-Bold="True" Font-Size="Large"></asp:Label><br />
            <asp:Table ID="Table1" runat="server" BorderWidth="1" GridLines="Both" Width="700px">
<asp:TableRow>
    <asp:TableCell><asp:Label ID="lblDispoCat" runat="server" Text="Dispo:" Font-Bold="True"></asp:Label></asp:TableCell>
    <asp:TableCell ><asp:CheckBox ID="cbxAs_Is" runat="server" Text="As is:" Enabled="false"/></asp:TableCell>
    <asp:TableCell><asp:CheckBox ID="cbxVendor_Return" runat="server" Text="Vendor Return:" Enabled="false"/></asp:TableCell>
    <asp:TableCell><asp:CheckBox ID="cbxReplace" runat="server" Text="Replace" Enabled="false"/></asp:TableCell>
    <asp:TableCell><asp:CheckBox ID="cbxReturn_To_Stock" runat="server" Text="Return to Stock:" Enabled="false"/></asp:TableCell>
    <asp:TableCell><asp:CheckBox ID="cbxQuarantined" runat="server" Text="Quarantine:" Enabled="false"/></asp:TableCell>
    <asp:TableCell> <asp:CheckBox ID="cbxScrap" runat="server" Text="Scrap" Enabled="false"/></asp:TableCell>
</asp:TableRow>
                <asp:TableRow>
    <asp:TableCell><asp:Label ID="lblDispoInit" runat="server" Text="Initials:" Font-Bold="True"></asp:Label></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
    <asp:TableCell><asp:Label ID="lblDate" runat="server" Text="Date:" Font-Bold="True"></asp:Label></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
            </asp:Table>             
            <br />
        <asp:Label ID="lblNonconRemarks" runat="server" Text="Remarks:" Font-Bold="True"></asp:Label><br />
        <asp:TextBox ID="txtNonconRemarks" runat="server" Rows="6" TextMode="MultiLine" Width="700px" Enabled="false"></asp:TextBox>

        <br />
        <asp:Label ID="lblPayment" runat="server" Text="Payment" Font-Bold="True" Font-Size="Large"></asp:Label><br />
            <asp:Label ID="lblPaymentDue" runat="server" Text="Is Payment due to SMC?"></asp:Label>
            <asp:TextBox ID="txtPaymentDue" runat="server" Enabled="false" Width="30px"></asp:TextBox>
            <asp:Label ID="lblPaymentRefunded" runat="server" Text="Has payment been refunded?" ></asp:Label>
            <asp:TextBox ID="txtPaymentRefunded" runat="server" Enabled="false" Width="30px"></asp:TextBox>
        <asp:Label ID="lblDateRefunded" runat="server" Text="Date Refunded:"></asp:Label>
        <asp:TextBox ID="txtDateRefunded" runat="server" width="90px" Enabled="false"></asp:TextBox><br /><br />

         <asp:Label ID="lblAdminNotes" runat="server" Text="Admin Notes:(Manual Entry)" Font-Bold="True" Font-Size="Large"></asp:Label><br />
        <asp:Label ID="lblRmaNumber" runat="server" Text="RMANumber: "></asp:Label><asp:TextBox ID="txtRmaNumber" runat="server" Width="100px"></asp:TextBox>
        <asp:CheckBox ID="cbxClosed" runat="server" Text="Closed?" Enabled="false"/><asp:CheckBox ID="cbxOpen" runat="server" Text="Open?" Enabled="false"/>       
            </div>
        </div>
    <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NONCON_RMAConnectionString %>" SelectCommand="NonCon_SummaryData" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="NonConID" QueryStringField="NonConID" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
</asp:Content>

