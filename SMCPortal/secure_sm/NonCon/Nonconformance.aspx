﻿<%@ Page Title="Nonconformance System" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="Nonconformance.aspx.cs" Inherits="secure_sm_NonCon_New_NonCon" %>
<%@ Register Src="~/assets/controls/rz_partrecord_autocomplete.ascx" TagPrefix="uc1" TagName="rz_partrecord_autocomplete" %>
<%@ Register Src="~/assets/controls/InspectionImageGallery.ascx" TagPrefix="uc1" TagName="InspectionImageGallery" %>
<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        #divTitle {
            display: none; /*Hide the hs_flex section padding*/
        }

        .save-controls {
            margin-top: 20px
        }

        .customer-labels {
            padding-top: 15px;
            font-weight: bold;
            font-size: 14px;
        }

        .completion-panel {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfCurrentTab" runat="server" />
    <div>
        <asp:Panel ID="pnlNonconInfo" runat="server">
            <div class="form-inline">
                <h3>Nonconformance System
                    <%--<asp:Label ID="lblNonconComplete" runat="server" ForeColor="#33CC33"></asp:Label>--%>
                </h3>
                <div class="pull-right">
                    <asp:Button ID="btnSaveNonConformance" runat="server" Text="Save Nonconformance" OnClick="btnSaveNonConformance_Click" CssClass="form-control" OnClientClick="ShowUpdateProgress()" />
                    <asp:Button ID="btnReturn" runat="server" Text="Return to Search" OnClick="btnReturn_Click" CssClass="form-control" OnClientClick="ShowUpdateProgress();" />
                </div>
            </div>
            <h5>Current Status:
            <asp:Label ID="lblNonConStatus" runat="server"></asp:Label></h5>
        </asp:Panel>
        <asp:Label ID="lblPageError" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#CC0000"></asp:Label>
        <asp:Label ID="lblPageSuccess" runat="server" ForeColor="#33CC33" Font-Bold="True" Font-Italic="True"></asp:Label>



        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tabHeader" id="mnuHeader" runat="server" onclick="LoadCurrentTab()();">Header</a></li>
            <li><a data-toggle="tab" href="#tabDetails" id="mnuDetails" runat="server" visible="false" onclick="LoadCurrentTab()();">Details</a></li>
            <li><a data-toggle="tab" href="#tabDisposition" id="mnuDisposition" runat="server" visible="false" onclick="LoadCurrentTab()();">Disposition</a></li>
            <li><a data-toggle="tab" href="#tabPayment" id="mnuPayment" runat="server" visible="false" onclick="LoadCurrentTab()();">Payment</a></li>
            <li><a data-toggle="tab" href="#tabImagery" id="mnuImagery" runat="server" visible="false" onclick="LoadCurrentTab()();">Imagery</a></li>
        </ul>

        <div class="tab-content well well-lg" id="TabContainer" runat="server">

            <div id="tabHeader" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-sm-12">
                        <h5>Header Data:</h5>
                        <div class="row">
                            <div class="col-sm-2">
                                <label>Nonconformance Date:</label>
                                <uc1:datetime_flatpicker runat="server" ID="dtp" />                                
                            </div>

                            <div class="col-sm-4">
                                <label>Vendor:</label><asp:DropDownList ID="ddlVendorName" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlVendorName_SelectedIndexChanged" CssClass="form-control" onchange="ShowUpdateProgress();"></asp:DropDownList>

                            </div>
                            <div class="col-sm-2">
                                <asp:Panel ID="pnlPoDetail" runat="server" Visible="false">
                                    Vendor PO:<asp:DropDownList ID="ddlVendorPO" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVendorPO_SelectedIndexChanged" CssClass="form-control" onchange="ShowUpdateProgress();" /><br />
                                </asp:Panel>
                            </div>
                            <div class="col-sm-4">
                            </div>
                        </div>






                        <%--PartSearch Panel--%>
                        <asp:Panel ID="pnlPartrecordSearch" runat="server" Visible="false">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Part Search</label>
                                    <br />
                                    Use the field below to search Rz for sales lines or inventory parts not linked to a PO.
                                <br />
                                    <div class="form-inline">
                                        <uc1:rz_partrecord_autocomplete runat="server" ID="rzpa" />
                                    </div>

                                </div>
                            </div>
                        </asp:Panel>

                        <%--  Sales Panel--%>
                        <asp:Panel ID="pnlSaleDetail" runat="server" Visible="false">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h5>Sales Order:</h5>
                                    <asp:DropDownList ID="ddlSalesOrders" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesOrders_SelectedIndexChanged" CssClass="form-control" onchange="ShowUpdateProgress();" Visible="false" /><br />
                                </div>
                                <div class="col-sm-4 customer-labels">
                                    Customer:
                                        <em>
                                            <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                                        </em>
                                    <br />
                                    Customer PO:
                                    <em>
                                        <asp:Label ID="lblCustomerPO" runat="server"></asp:Label></em>

                                </div>

                            </div>
                        </asp:Panel>


                        <%-- Partrecord Panel--%>
                        <div class="row">
                            <div class="col-sm-12">

                                <asp:Panel ID="pnlPartData" runat="server" Visible="false">
                                    <h5>Part Selection:
                                        <asp:Label ID="lblPartSelectionGrid" runat="server"></asp:Label></h5>
                                    Please select the nonconfirming parts from the list below: <i>(note if the parts belong to more than on Sales Order, you need to create a separate noncon for each)</i>
                                  <%--  <asp:GridView ID="gvParts" runat="server" OnRowDataBound="gvParts_RowDataBound" EmptyDataText="No parts found in our inventory that match the search text." GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbxPartSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>--%>
                                    <uc1:sm_datatable runat="server" ID="smdtPartData" />


                                    <%-- <asp:Button ID="btnAutofillPartData" runat="server" Text="Autofill Part Data" OnClick="btnAutofillPartData_Click" Visible="false" CssClass="btn btn-sm btn-primary" onchange="ShowUpdateProgress();"/><br />--%>
                                    <asp:LinkButton ID="btnAutofillPartData" runat="server" Text="Autofill Part Data" Visible="false" OnClientClick="return AutofillPartData();" /><br />
                                    <div class="form-inline">
                                        <asp:TextBox ID="txtPartNumber" runat="server" CssClass="form-control" placeholder="Part Number"></asp:TextBox>
                                        <asp:TextBox ID="txtMFG" runat="server" CssClass="form-control" placeholder="Manufacturer"></asp:TextBox>
                                        <asp:TextBox ID="txtQTY" runat="server" placeholder="Noncon QTY" CssClass="form-control" ToolTip="Input the Qty that is nonconforming.  For instance, in a short-ship, if we ordered 100, and only received 92, then the nonconforming QTY would be 8."></asp:TextBox>
                                        <asp:CheckBox ID="cbxShortShip" runat="server" Text="Short Ship?" TextAlign="Left" />
                                    </div>

                                </asp:Panel>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <label>Description:</label><br />
                                <asp:TextBox ID="txtDescription" runat="server" Rows="6" TextMode="MultiLine" CssClass="form-control"></asp:TextBox><br />
                                <%-- <div class="row save-controls">
                                    <div class="col-sm-12">
                                        <asp:Button ID="btnSaveNonConformance" runat="server" Text="Save Header" OnClick="btnSaveNonConformance_Click" CssClass="form-control" OnClientClick="ShowUpdateProgress()" />
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="tabDetails" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Nonconformance Details:</h5>
                        <asp:Label ID="lblisNonconforming" runat="server" Text="Is the product nonconforming?" Font-Bold="true" />
                        <asp:CheckBox ID="cbxisNonconforming" runat="server" onchange="onChecked(this);" /><br />
                        <asp:Panel ID="pnlisNonconforming" runat="server">
                            <asp:Label ID="lblisVendorFeedbackUpdated" runat="server" Text="Has vendor feedback been updated?"></asp:Label><asp:DropDownList ID="ddlisVendorFeedbackUpdated" runat="server">
                                <asp:ListItem Text="<choose>" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                <asp:ListItem Text="N/A" Value="3"></asp:ListItem>
                            </asp:DropDownList><br />
                        </asp:Panel>

                        <%--<asp:Label ID="lblisActionRequired" runat="server" Text="Is action required?" Font-Bold="true" />
                        <asp:CheckBox ID="cbxisActionRequired" runat="server" onchange="onChecked(this);" /><br />
                        <asp:Panel ID="pnlisActionRequired" runat="server">
                            <asp:Label ID="lblCANum" runat="server" Text="CANum:"></asp:Label><asp:TextBox ID="txtCANum" runat="server"></asp:TextBox><br />
                            <asp:Label ID="lblSCNum" runat="server" Text="SCNum:"></asp:Label><asp:TextBox ID="txtSCNum" runat="server"></asp:TextBox><br />
                            <asp:Label ID="lblPANum" runat="server" Text="PANum:"></asp:Label><asp:TextBox ID="txtPANum" runat="server"></asp:TextBox>
                        </asp:Panel>
                        <hr />--%>
                        <h5>RMA Details:</h5>
                        <asp:Label ID="lblisVRMA" runat="server" Text="Is there a Vendor RMA?" Font-Bold="true" /><asp:CheckBox ID="cbxisVRMA" runat="server" /><asp:Label ID="lblVRMAQTY" runat="server" Text="QTY: "></asp:Label><asp:TextBox ID="txtVRMAQTY" runat="server" Width="50px"></asp:TextBox><br />
                        <asp:Label ID="lblisCRMA" runat="server" Text="Is there a Customer RMA?" Font-Bold="true" /><asp:CheckBox ID="cbxisCRMA" runat="server" /><asp:Label ID="lblCRMAQTY" runat="server" Text="QTY: "></asp:Label><asp:TextBox ID="txtCRMAQTY" runat="server" Width="50px"></asp:TextBox><br />
                        <asp:TextBox ID="txtRMARemarks" runat="server" Rows="4" Width="90%" TextMode="MultiLine"></asp:TextBox>
                    </div>

                    <div class="col-sm-6">
                        <h5>Product Observations:</h5>
                        <asp:Panel ID="pnlProductObservations" runat="server">
                            <asp:Label ID="lblLeadsBody" runat="server" Text="Were there issues with the leads or Body?" Font-Bold="True"></asp:Label>
                            <asp:CheckBox ID="cbxLeadsBody" CssClass="prod-observation-cbx" runat="server" ToolTip="Please leave detailed information about the condition of the leads / Body." onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlLeadsBody" runat="server">
                                <asp:Label ID="lblRetin" runat="server" Text="Re-tinned"></asp:Label><asp:CheckBox ID="cbxRetin" runat="server" ToolTip="Leads showing signs of Re-tinning." /><br />
                                <asp:Label ID="lblOxidation" runat="server" Text="Oxidation"></asp:Label><asp:CheckBox ID="cbxOxidation" runat="server" ToolTip="Leads showing signs of Oxidation." /><br />
                                <asp:Label ID="lblPull" runat="server" Text="Pulled"></asp:Label><asp:CheckBox ID="cbxPull" runat="server" ToolTip="Leads / Body showing signs of being pulled." /><br />
                                <asp:Label ID="lblRefurb" runat="server" Text="Refurbishment"></asp:Label><asp:CheckBox ID="cbxRefurb" runat="server" ToolTip="Leads / Body unexpectedly showing signs of refurbishment." /><br />
                                <asp:Label ID="lblGenDamage" runat="server" Text="General Damage (bent, broken, clipped, etc ...)"></asp:Label><asp:CheckBox ID="cbxGenDamage" runat="server" ToolTip="Leads showing signs of General Damage." /><br />
                                <asp:Label ID="lblLeadsMemo" runat="server" Text="Leads / Body Remarks:" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtLeadsMemo" runat="server" TextMode="MultiLine" Width="404px"></asp:TextBox>
                                <hr />
                            </asp:Panel>
                            <asp:Label ID="lblisFunctionalIssue" runat="server" Text="Was there a functionality issue?" Font-Bold="True"></asp:Label>
                            <asp:CheckBox ID="cbxisFunctionalIssue" CssClass="prod-observation-cbx" runat="server" onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlisFunctionalIssue" runat="server">
                                <asp:Label ID="lblfnSolderFail" runat="server" Text="Solderability Failure:"></asp:Label><asp:CheckBox ID="cbxfnSolderFail" runat="server" /><br />
                                <asp:Label ID="lblfnElecFail" runat="server" Text="Electrical Failure:"></asp:Label><asp:CheckBox ID="cbxfnElecFail" runat="server" /><br />
                                <asp:Label ID="lblfnCoplanFail" runat="server" Text="Coplanarity Failure:"></asp:Label><asp:CheckBox ID="cbxfnCoplanFail" runat="server" /><br />
                                <asp:Label ID="lblfnProgramFail" runat="server" Text="Programability Failure:"></asp:Label><asp:CheckBox ID="cbxfnProgramFail" runat="server" /><br />
                                <asp:Label ID="lblfnRemarks" runat="server" Text="Functionality Remarks:" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtfnRemarks" runat="server" Rows="4" TextMode="MultiLine" Width="404px"></asp:TextBox>
                                <hr />
                            </asp:Panel>
                            <asp:Label ID="lblisSuspectCounterfeit" runat="server" Text="Is the product suspect counterfeit?" Font-Bold="True"></asp:Label>
                            <asp:CheckBox ID="cbxisSuspectCounterfeit" CssClass="prod-observation-cbx" runat="server" onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlisSuspectCounterfeit" runat="server">
                                <asp:Label ID="lblscSecondaryCoating" runat="server" Text="Secodary Coating:"></asp:Label><asp:CheckBox ID="cbxscSecondaryCoating" runat="server" /><br />
                                <asp:Label ID="lblscRemarked" runat="server" Text="Remarked:"></asp:Label><asp:CheckBox ID="cbxscRemarked" runat="server" /><br />
                                <asp:Label ID="lblscCloned" runat="server" Text="Cloned:"></asp:Label><asp:CheckBox ID="cbxscCloned" runat="server" /><br />
                                <asp:Label ID="lblscRemarks" runat="server" Text="Suspect Counterfeit Remarks:" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtscRemarks" runat="server" Rows="4" TextMode="MultiLine" Width="404px"></asp:TextBox>
                                <hr />
                            </asp:Panel>
                            <asp:Label ID="lblisDamaged" runat="server" Text="Is the product damaged?" Font-Bold="True"></asp:Label>
                            <asp:CheckBox ID="cbxisDamaged" CssClass="prod-observation-cbx" runat="server" onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlisDamaged" runat="server">
                                <asp:Label ID="lbldmCarrier" runat="server" Text="Damaged by Carrier:"></asp:Label><asp:CheckBox ID="cbxdmCarrier" runat="server" /><br />
                                <asp:Label ID="lbldmSMCReceipt" runat="server" Text="Damaged on SMC Receive:"></asp:Label><asp:CheckBox ID="cbxdmSMCReceipt" runat="server" /><br />
                                <asp:Label ID="lbldmCustReceipt" runat="server" Text="Damaged on Customer Receive:"></asp:Label><asp:CheckBox ID="cbxdmCustReceipt" runat="server" /><br />
                                <asp:Label ID="lbldmSMCWarehouse" runat="server" Text="Damaged in SMC Warehouse:"></asp:Label><asp:CheckBox ID="cbxdmSMCWarehouse" runat="server" /><br />
                                <asp:Label ID="lbldmProduct" runat="server" Text="Damage to the product:"></asp:Label><asp:CheckBox ID="cbxdmProduct" runat="server" /><br />
                                <asp:Label ID="lbldmPackage" runat="server" Text="Damaged to the Packaging:"></asp:Label><asp:CheckBox ID="cbxdmPackage" runat="server" /><br />
                                <asp:Label ID="lbldmRemarks" runat="server" Text="Damage Remarks:" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtdmRemarks" runat="server" Rows="4" TextMode="MultiLine" Width="404px"></asp:TextBox>
                                <hr />
                            </asp:Panel>
                            <asp:Label ID="lblisClerical" runat="server" Text="Was there clerical error?" Font-Bold="True"></asp:Label>
                            <asp:CheckBox ID="cbxisClerical" CssClass="prod-observation-cbx" runat="server" onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlisClerical" runat="server">
                                <asp:Label ID="lblclSMC" runat="server" Text="SMC Error:"></asp:Label><asp:CheckBox ID="cbxclSMC" runat="server" /><br />
                                <asp:Label ID="lblclVendor" runat="server" Text="Vendor Error:"></asp:Label><asp:CheckBox ID="cbxclVendor" runat="server" /><br />
                                <asp:Label ID="lblclCustomer" runat="server" Text="Customer Error:"></asp:Label><asp:CheckBox ID="cbxclCustomer" runat="server" /><br />
                                <asp:Label ID="lblclRemarks" runat="server" Text="Clerical Remarks:" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtclRemarks" runat="server" Rows="4" TextMode="MultiLine" Width="404px"></asp:TextBox>
                                <hr />
                            </asp:Panel>
                            <asp:Label ID="lblisImproperPKG" runat="server" Text="Improper Packaging?" Font-Bold="True"></asp:Label>
                            <asp:CheckBox ID="cbxisImproperPKG" CssClass="prod-observation-cbx" runat="server" onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlisImproperPKG" runat="server">
                                <asp:Label ID="lblipContainer" runat="server" Text="Improper container:"></asp:Label>
                                <asp:CheckBox ID="cbxipContainer" runat="server" /><br />
                                <asp:Label ID="lblipMSL" runat="server" Text="Improper MSL:"></asp:Label><asp:CheckBox ID="cbxipMSL" runat="server" /><br />
                                <asp:Label ID="lblipESD" runat="server" Text="Improper ESD:"></asp:Label><asp:CheckBox ID="cbxipESD" runat="server" /><br />
                                <asp:Label ID="lblipTandR" runat="server" Text="Faulty Tape and Reel:"></asp:Label><asp:CheckBox ID="cbxipTandR" runat="server" /><br />
                                <asp:Label ID="lblipRemarks" runat="server" Text="Improper Packaging Remarks:" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtipRemarks" runat="server" Rows="4" TextMode="MultiLine" Width="404px"></asp:TextBox>
                                <hr />
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                </div>
                <%-- <div class="row save-controls">
                    <div class="col-sm-6">
                        <asp:Button ID="btnSubmitDetails" runat="server" Text="Save Details" OnClick="btnSubmitDetails_Click" CssClass="form-control" />
                    </div>
                    <div class="col-sm-6">
                        <asp:Button ID="btnCancelDetails" runat="server" Text="Cancel" OnClick="btnCancelDetails_Click" CssClass="form-control" />
                    </div>
                </div>--%>
            </div>


            <div id="tabDisposition" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Disposition:</h5>
                        <asp:Label ID="lblas_is" runat="server" Text="Customer to accept 'As Is':"></asp:Label><asp:CheckBox ID="cbxas_is" runat="server" /><br />
                        <asp:Label ID="lblreplace" runat="server" Text="Replace:"></asp:Label><asp:CheckBox ID="cbxreplace" runat="server" /><br />
                        <asp:Label ID="lblvendor_return" runat="server" Text="Return to Vendor:"></asp:Label><asp:CheckBox ID="cbxvendor_return" runat="server" /><br />
                        <asp:Label ID="lblreturn_to_stock" runat="server" Text="Return to Stock:"></asp:Label><asp:CheckBox ID="cbxreturn_to_stock" runat="server" /><br />
                        <asp:Label ID="lblquarantined" runat="server" Text="Quarantined:"></asp:Label><asp:CheckBox ID="cbxquarantined" runat="server" /><br />
                        <asp:Label ID="lblscrap" runat="server" Text="Scrapped:"></asp:Label><asp:CheckBox ID="cbxscrap" runat="server" /><br />
                    </div>
                    <div class="col-sm-6">
                        <h5>
                            <asp:Label ID="lblcontainment_notes" runat="server" Text="Containment Notes:"></asp:Label></h5>
                        <br />
                        <asp:TextBox ID="txtcontainment_notes" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        <h5>
                            <asp:Label ID="lblremarks" runat="server" Text="Disposition Remarks:"></asp:Label></h5>
                        <asp:TextBox ID="txtremarks" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <%--      <div class="row save-controls">
                    <div class="col-sm-6">
                        <asp:Button ID="btnSubmitDispo" runat="server" Text="Save Disposition" OnClick="btnSubmitDispo_Click" CssClass="form-control" />
                    </div>
                    <div class="col-sm-6">
                        <asp:Button ID="btnCancelDispo" runat="server" Text="Cancel" OnClick="btnCancelDispo_Click" CssClass="form-control" />
                    </div>
                </div>--%>
            </div>


            <div id="tabPayment" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Payment Information:</h5>
                        <%--<asp:Label ID="lblconfirmedNetProfit" runat="server" Text="Current Net Profit (Matched against Excel and RZ):"></asp:Label><asp:TextBox ID="txtconfirmedNetProfit" runat="server"></asp:TextBox><br />--%>
                        <asp:Label ID="lblisPaymentDue" runat="server" Text="SMC payment to be refunded?"></asp:Label>
                        <asp:CheckBox ID="cbxisPaymentDue" runat="server" onchange="onChecked(this);" /><br />
                        <%--<asp:Label ID="lblisVendorCredit" runat="server" Text="Was a vendor credit applied in Quickbooks and Rz?"></asp:Label><asp:CheckBox ID="cbxisVendorCredit" runat="server" />--%>
                        <br />
                        <asp:Panel ID="pnlisPaymentDue" runat="server">
                            <asp:Label ID="lblisPaymentRefunded" runat="server" Text="Has payment been refunded?"></asp:Label>
                            <asp:CheckBox ID="cbxisPaymentRefunded" runat="server" onchange="onChecked(this);" /><br />
                            <asp:Panel ID="pnlisPaymentRefunded" runat="server">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:Label ID="lblDateRefunded" runat="server" Text="Date Refunded:"></asp:Label>
                                        <uc1:datetime_flatpicker runat="server" ID="dtp_DateRefunded" />
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:Label ID="lblRefundType" runat="server" Text="Refund Type:"></asp:Label>
                                        <asp:DropDownList ID="ddlPaymenttype" runat="server" onchange="TogglePanel(this);">
                                            <asp:ListItem Text="<choose>" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Wire" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Check" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Credit Card" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Paypal" Value="4"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:Panel ID="pnlWireDetails" runat="server">
                                            <asp:Label ID="lblWireRecievedinQB" runat="server" Text="Wire Recieved in QB?" />
                                            <asp:CheckBox ID="cbxWireRecievedinQB" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCheckDetails" runat="server">
                                            <asp:Label ID="lblCheckCancelledQB" runat="server" Text="Cancelled QB?" /><asp:CheckBox ID="cbxCheckCancelledQB" runat="server" /><br />
                                            <asp:Label ID="lblCheckNumber" runat="server" Text="Check Number: "></asp:Label><asp:TextBox ID="txtCheckNumber" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCreditCard" runat="server">
                                            <asp:Label ID="lblCreditPaymentReceived" runat="server" Text="Credit Card Payment Recieved?" /><asp:CheckBox ID="cbxCreditPaymentReceived" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlPayPalDetails" runat="server">
                                            <asp:Label ID="lblPayPal" runat="server" Text="Net Amount Returned?" /><asp:TextBox ID="txtPayPalNetAmnt" runat="server" />
                                        </asp:Panel>

                                    </div>
                                </div>

                            </asp:Panel>
                        </asp:Panel>
                    </div>
                    <div class="col-sm-6">
                        <h5>
                            <asp:Label ID="lblPaymentRemarks" runat="server" Text="Remarks:"></asp:Label></h5>
                        <asp:TextBox ID="txtPaymentRemarks" runat="server" Rows="6" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <%--    <div class="row save-controls">
                    <div class="col-sm-6">
                        <asp:Button ID="btnSubmitPayment" runat="server" Text="Save Payment" OnClick="btnSubmitPayment_Click" CssClass="form-control" />
                    </div>
                    <div class="col-sm-6">
                        <asp:Button ID="btnCancelPayment" runat="server" Text="Cancel" OnClick="btnCancelPayment_Click" CssClass="form-control" /><br />
                    </div>
                </div>--%>
            </div>


            <div id="tabImagery" class="tab-pane fade">
                <%--Main Imagery--%>
                <div class="row">
                    <div class="col-sm-12 ">
                        <h5>Main Image:</h5>
                        <div id="divMainImage" runat="server" class="flex-grid">
                            <uc1:InspectionImageManagerControl runat="server" ID="imcMainImage" InspectionType="noncon" />
                        </div>
                    </div>
                </div>
                <hr />
                <%--Other Imagery--%>
                <div class="row">
                    <div class="col-sm-12 ">
                        <h5>Other Imagery:</h5>
                        <em>Multiple: (10 Max)</label></em>
                        <%--Controls--%>
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-12">
                                    <uc1:InspectionImageGallery runat="server" ID="iig" />
                                </div>
                                <%-- <div class="col-sm-10">
                                    <asp:FileUpload ID="fuOtherImagery" runat="server" AllowMultiple="True" ToolTip="Select up to 10 Images to upload." />
                                </div>
                                <div class="col-sm-2">
                                    <asp:LinkButton ID="lbSaveOtherImagery" runat="server" OnClick="lbSaveImagery_Click" CssClass="form-control">Save Imagery</asp:LinkButton>
                                </div>--%>
                            </div>
                        </div>
                        <%-- <div id="divOtherImages" runat="server" class="flex-grid"></div>--%>
                    </div>
                </div>
            </div>







        </div>


        <asp:Panel ID="pnlNonconUser" runat="server" CssClass="well well-lg">

            <h5>Noncon User Panel</h5>
            <div class="row">
                <div class="col-sm-4">
                    <asp:Button ID="btnSubmitCLosure" runat="server" Text="Submit for Closure" OnClick="btnSubmitCLosure_Click" CssClass="form-control" />
                </div>
                <div class="col-sm-4">
                    <asp:Button ID="btnReSendEmail" runat="server" Text="Re-Send Nonconformance Email" OnClick="btnReSendEmail_Click" CssClass="form-control" />
                </div>
                <div class="col-sm-4">
                    <asp:Button ID="btnNonConSummary" runat="server" Text="View / Print Summary" OnClick="btnNonConSummary_Click" CssClass="form-control" />
                </div>
                <%--  <div class="col-sm-3">
                    <asp:Button ID="btnReturn" runat="server" Text="Return to Search" OnClick="btnReturn_Click" CssClass="form-control" />
                </div>--%>
            </div>

        </asp:Panel>

        <asp:Panel ID="pnlAdminOptions" runat="server" CssClass="well well-lg">
            <div>
                <h5>Noncon Admin Panel</h5>
                <div class="row">
                    <div class="col-sm-4">
                        <asp:Button ID="btnApproveNoncon" runat="server" Text="Approve Noncon" OnClick="btnApproveNoncon_Click" OnClientClick="return confirm('Are you sure you want to approve this nonconformance?');" CssClass="form-control" />

                    </div>
                    <div class="col-sm-4" style="text-align: center;">
                        <asp:LinkButton ID="lbCompletionPanel" runat="server" data-toggle="modal" data-target="#closureModal" CssClass="form-control">Completion Panel</asp:LinkButton>

                    </div>

                    <div class="col-sm-4">
                        <asp:Button ID="btnDeleteNoncon" runat="server" OnClick="btnDeleteNoncon_Click" OnClientClick="return confirmDelete(); " CssClass="form-control" Text="Delete / Reopen" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <asp:LinkButton ID="lbGenerateReport" runat="server" CssClass="form-control" OnClick="lbGenerateReport_Click">Generate Report</asp:LinkButton>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="closureModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <asp:Panel ID="pnlCloseNoncon" runat="server" CssClass="completion-panel">
                                    <h5>Closure Remarks</h5>
                                    <div>
                                        <asp:Label ID="lblCloseError" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#CC0000"></asp:Label><br />
                                        <asp:TextBox ID="txtClosureRemarks" runat="server" Rows="6" TextMode="MultiLine" CssClass="form-control"></asp:TextBox><br />
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <asp:LinkButton ID="lbCompleteNonCon" runat="server" OnClick="lbCompleteNonCon_Click" CssClass="form-control">Complete Nonconformance</asp:LinkButton>
                                                <asp:LinkButton ID="lbUncompleteNonCon" runat="server" OnClick="lbUncompleteNonCon_Click" CssClass="form-control">Uncomplete Nonconformance</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </asp:Panel>
    </div>


    <script>

        $(function LoadUI() {

            //Loop through the product observations checkboxes to see if panel needs reveal.
            $('.prod-observation-cbx').each(function (i, obj) {
                var cbx = $(this).children(":first");
                ShowHidePanel(cbx);
                TogglePanel($("#MainContent_ddlPaymenttype"));
                LoadCurrentTab();

            });
        });

        //Maintain current Tab.
        function LoadCurrentTab() {
            //On every click fan a tag with dat-toggle tab, set the tab ID in Javascript local storage.
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                //Save the Href of clicked object to Hidden field
                $("#MainContent_hfCurrentTab").val($(e.target).attr('href'))
                //I didn't like localstorage, because it was too sticky.  Would keep same tab even on freash load, or even opening different Noncons
                //localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            //var activeTab = localStorage.getItem('activeTab');
            var activeTab = $("#MainContent_hfCurrentTab").val();
            if (activeTab) {
                //On load, if we have an active tab, get it's menu object and show it.
                var tab = activeTab.replace("#tab", "mnu");
                $("#MainContent_" + tab).tab('show');
            }
        };

        function onChecked(span) {
            var cbx = $(span).children(":first");
            ShowHidePanel(cbx)

        }

        function ShowHidePanel(cbx) {
            var panelID = cbx.attr('id').replace("cbx", "pnl");
            var panel = $("#" + panelID + "");
            if (cbx.is(':checked'))
                panel.show();
            else
                panel.hide();
        }

        function AutofillPartData() {
            var rows = [];
            $('#MainContent_smdtPartData_gvMain').find('tr').each(function () {
            //$('#MainContent_gvParts').find('tr').each(function () {
                var row = this;
                if ($(row).find('input[type="checkbox"]').is(':checked'))// && row.find('textarea').val().length <= 0)   
                    rows.push(row);//Add checked rows to array
            });

            //if (rows.length === 0 || rows.length > 1)//if Array is more or less than exactly one part, alert
            //    alert('Please select a single row from the grid.');
            //else {


            //Rows 1,2,3 map to what I want (zero indexed, 0 cell is checkbox) 
            //When Multiple, just take 1st found PN and MFG, but SUM all QTY
            var firstRow = rows[0];
            var part = $(firstRow).find("td:eq(1)").text();
            var mfg = $(firstRow).find("td:eq(2)").text();
            //Set the hidden fields
            $("#MainContent_txtPartNumber").val(part);
            $("#MainContent_txtMFG").val(mfg);
            var qty = Number(0);
            $(rows).each(function (index) {
                // var row = rows[0];
                var row = this;
                var rowQty = Number($(row).find("td:eq(3)").text());
                qty = qty + rowQty;
            });
            //Set the summed value tyo the HF
            $("#MainContent_txtQTY").val(qty);
            return false;
        }

        function TogglePanel(control) {
            var control = $(control);
            var controlID = control.attr("id");
            var selectedValue = $("#" + controlID + " option:selected").text();
            //MainContent_pnlWireDetails.Visible = true;
            //MainContent_pnlCheckDetails.Visible = false;
            //MainContent_pnlCreditCard.Visible = false;
            //MainContent_pnlPayPalDetails.Visible = false;
            switch (controlID) {
                case "MainContent_ddlPaymenttype":
                    {
                        if (selectedValue == "Wire") {
                            //"Wire"
                            $("#MainContent_pnlWireDetails").show();
                            $("#MainContent_pnlCheckDetails").hide();
                            $("#MainContent_pnlCreditCard").hide();
                            $("#MainContent_pnlPayPalDetails").hide();
                            break;
                        }
                        else if (selectedValue == "Check") {
                            //"Check"
                            $("#MainContent_pnlCheckDetails").show();
                            $("#MainContent_pnlWireDetails").hide();
                            $("#MainContent_pnlCreditCard").hide();
                            $("#MainContent_pnlPayPalDetails").hide();
                            break;
                        }
                        else if (selectedValue == "Credit Card") {
                            //"Credit Card"
                            $("#MainContent_pnlCreditCard").show();
                            $("#MainContent_pnlWireDetails").hide();
                            $("#MainContent_pnlCheckDetails").hide();
                            $("#MainContent_pnlPayPalDetails").hide();
                            break;
                        }
                        else if (selectedValue == "Paypal") {
                            //"Paypal"
                            $("#MainContent_pnlPayPalDetails").show();
                            $("#MainContent_pnlWireDetails").hide();
                            $("#MainContent_pnlCheckDetails").hide();
                            $("#MainContent_pnlCreditCard").hide();

                            break;
                        }
                        else {
                            $("#MainContent_pnlWireDetails").hide();
                            $("#MainContent_pnlCheckDetails").hide();
                            $("#MainContent_pnlCreditCard").hide();
                            $("#MainContent_pnlPayPalDetails").hide();
                        }

                    }
                    break;
            }
        }


        function confirmDelete() {
            if (confirm('Are you sure you want to delete / restore this nonconformance.')) {

                ShowUpdateProgress();
                return true;
            }
            return false;

        }

    </script>
</asp:Content>

