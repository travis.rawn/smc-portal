﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;


public partial class component_intel : System.Web.UI.Page
{
    SM_Data smd = new SM_Data();
    SM_Tools tools = new SM_Tools();  
   
    string searchParts = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Roles.IsUserInRole(SM_Enums.PortalRole.sm_internal.ToString()) && !Roles.IsUserInRole(SM_Enums.PortalRole.admin.ToString()))
        {
            pnlSearchMain.Visible = false;
            tools.HandleResult("fail", "Unauthorized", 100000);
        }

    }


    protected void btnPartListSearchXML_Click(object sender, EventArgs e)
    {        
        try
        {
            searchParts = txtPartnumber.Text.Trim();
            doSearch();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message + ex.InnerException);
        }
    }

   

    private void doSearch()
    {
        DataSet ds = SiliconExpertLogic.GetListPartSearchDataSetXML(searchParts);
        if (ds == null)
            throw new Exception("No Results Found.");
        var query = from myRow in ds.Tables["Result"].AsEnumerable()

                    select new
                    {
                        ComID = myRow.Field<string>("ComID").ToString(),
                        PartNumber = myRow.Field<string>("PartNumber").ToString(),
                        Manufacturer = myRow.Field<string>("Manufacturer").ToString(),
                        //Type = myRow.Field<string>("PlName").ToString(),
                        DataSheet = myRow.Field<string>("Datasheet").ToString(),
                        Lifecycle = myRow.Field<string>("LifeCycle").ToString(),
                        Rohs = myRow.Field<string>("RoHs").ToString() + "(" + myRow.Field<string>("ComID").ToString() + ")",
                    };
        if (query.Any())
        {
            gvPartList.DataKeyNames = new string[] { "ComID", "PartNumber" };
            gvPartList.DataSource = query;
            gvPartList.DataBind();
            gvPartList.UseAccessibleHeader = true;
            gvPartList.HeaderRow.TableSection = TableRowSection.TableHeader;
        }



    }

    protected void lbPricing_Click(object sender, EventArgs e)
    {
        try
        {
            pnlDetail.Visible = false;
            pnlPricing.Visible = true;
            pnlCross.Visible = false;

            DataTable dt = null;
            LinkButton lb = sender as LinkButton;
            GridViewRow row = lb.NamingContainer as GridViewRow;
            string ComID = gvPartList.DataKeys[row.RowIndex].Values["ComID"].ToString();
            string partNumber = gvPartList.DataKeys[row.RowIndex].Values["PartNumber"].ToString();
            DataSet ds = SiliconExpertLogic.GetPartDetailDataSetXML(ComID, Membership.GetUser().UserName);
            if (ds == null)
                return;
            dt = ds.Tables["PricingData"];
            lblModalTitle.Text = "Pricing Data: " + partNumber;
            ShowModalPricing(dt, partNumber);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
      



    }


    protected void lbCrosses_Click(object sender, EventArgs e)
    {

        try
        {
            pnlDetail.Visible = false;
            pnlPricing.Visible = false;
            pnlCross.Visible = true;

            DataTable dt = null;
            LinkButton lb = sender as LinkButton;
            GridViewRow row = lb.NamingContainer as GridViewRow;
            string ComID = gvPartList.DataKeys[row.RowIndex].Values["ComID"].ToString();
            DataSet ds = SiliconExpertLogic.GetPartDetailDataSetXML(ComID, Membership.GetUser().UserName);
            if (ds == null)
                return;
            dt = ds.Tables["CrossDto"];
            lblModalTitle.Text = "Cross Data";
            ShowModal(gvCross, dt);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
       
    }
    protected void lbDetails_Click(object sender, EventArgs e)
    {
        try
        {
            pnlDetail.Visible = true;
            pnlPricing.Visible = false;
            pnlCross.Visible = false;

            LinkButton lb = sender as LinkButton;
            GridViewRow row = lb.NamingContainer as GridViewRow;
            string ComID = gvPartList.DataKeys[row.RowIndex].Values["ComID"].ToString();
            DataSet ds = SiliconExpertLogic.GetPartDetailDataSetXML(ComID, Membership.GetUser().UserName);
            if (ds == null)
                return;
            lblModalTitle.Text = "Component Details";
            ShowModalDetail(ds);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
      
    }

    private void ShowModal(object dataObject, DataTable dt)
    {     

        if (dataObject is GridView)
        {
            GridView gv = (GridView)dataObject;
            gv.DataSource = null;
            gv.DataSource = dt;
            gv.DataBind();
            //ShowHideDataControls(gv.ID);
            if (dt != null)
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;

        }
        if (dataObject is DetailsView)
        {
            DetailsView dtv = (DetailsView)dataObject;
            dtv.DataSource = null;
            dtv.DataSource = dt;
            dtv.DataBind();
            //ShowHideDataControls(dtv.ID);

        }

        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#mdlMain').modal('show');", true);

    }

    private void ShowModalPricing(DataTable dt, string partNumber)
    {
        RzTools rzt = new RzTools();      

        //Silicon Expert
        gvPricingSE.DataSource = dt;
        gvPricingSE.DataBind();        
        if (dt != null)
            gvPricingSE.HeaderRow.TableSection = TableRowSection.TableHeader;



        //Rz
        List<orddet_line> linePricing = rzt.GetLinePricing(partNumber, "Sales");
        if (linePricing != null)
        {
            DateTime start = new DateTime(2005, 05, 04);//setting this to companhy inception to strip out non-sold parts
            List<string> badpartnames = new List<string>() {"test", "descruct", "gcat" };
            var groupedPricing = linePricing.Where(w => !badpartnames.Contains(w.fullpartnumber) && w.orderdate_sales >= start && w.unit_price != null && w.unit_price > 0).GroupBy(g => g.part_number_stripped).Select(s => new
            {
                PartNumber = s.Key,
                Min = string.Format("{0:C}", s.Min(ss => ss.unit_price) ?? 0),
                Max = string.Format("{0:C}", s.Max(ss => ss.unit_price) ?? 0),
                Avg = string.Format("{0:C}", s.Average(ss => ss.unit_price) ?? 0),
                LastSale = s.Max(ss => ss.orderdate_sales) ?? DateTime.MinValue
            });
            gvPricingRzSales.DataSource = groupedPricing.OrderByDescending(o => o.LastSale);
            gvPricingRzSales.DataKeyNames = new string[] {"PartNumber" };
            gvPricingRzSales.DataBind();
            if (groupedPricing.Any())
            {
                gvPricingRzSales.HeaderRow.TableSection = TableRowSection.TableHeader;
               
            }
                

        }
        else
        { //Null
            gvPricingRzSales.DataSource = null;
            gvPricingRzSales.DataBind();            
        }

        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#mdlMain').modal('show');", true);
    }


    private void ShowModalDetail(DataSet ds)
    {
       
        dtvSummary.DataSource = ds.Tables["SummaryData"];
        dtvSummary.DataBind();

        dtvPara.DataSource = ds.Tables["ParametricData"];
        dtvPara.DataBind();

        dtvPackaging.DataSource = ds.Tables["PackageData"];
        dtvPackaging.DataBind();

        dtvManufacturing.DataSource = ds.Tables["ManufacturingData"];
        dtvManufacturing.DataBind();

        dtvCounterfeitRisk.DataSource = ds.Tables["RiskData"];
        dtvCounterfeitRisk.DataBind();

        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#mdlMain').modal('show');", true);
    }

        
    protected void dtv_DataBound(object sender, EventArgs e)
    {
        DetailsView dtv = (DetailsView)sender;
        foreach (DetailsViewRow row in dtv.Rows)
        {
            if (row.Cells[1].Text.Contains("http"))
            {
                string test = tools.ConvertUrlsToLinks(row.Cells[1].Text, "Click to view");
                row.Cells[1].Text = test;
                row.Cells[1].ForeColor = System.Drawing.Color.Blue;
            }

        }


    }


    protected void gvPricingRzSales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string searchedPart = Tools.Strings.FilterTrash(txtPartnumber.Text).Trim();
        GridView gv = (GridView)sender;
        foreach(GridViewRow row in gv.Rows)
        {
            string part = gv.DataKeys[row.RowIndex].Values["PartNumber"].ToString();
            string rowPart = Tools.Strings.FilterTrash(part).Trim();

            if (rowPart.ToLower() == searchedPart.ToLower())
                row.BackColor = System.Drawing.Color.Yellow;
        }
    }
}