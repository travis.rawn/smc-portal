﻿<%@ Page Title="Import | Export Compliance" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="SM_ExportCompliance.aspx.cs" Inherits="secure_sm_PartIQ" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>








<asp:Content ID="Content1" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>EXPORT COMPLIANCE</h1>
    </div>
    <p>Search for an existing record.  If you don't find what you're looking for you can then create a new record.</p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="row" class="card">
        <div class="col-sm-12">
            <asp:Label ID="lblPageAlert" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"></asp:Label>
        </div>
        <div class="col-sm-12">
            <asp:Button ID="btnAdd" runat="server" Text="Add New MPN" OnClick="btnAdd_Click" CssClass="btn-smc btn-smc-primary" />

        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <asp:HiddenField ID="hfMPNID" runat="server" />
                <uc1:sm_datatable runat="server" ID="smdt_Results" />
            </div>
        </div>
        <div class="col-sm-6">

            <asp:Panel ID="pnlEditPart" runat="server" CssClass="card" DefaultButton="btnSubmit">

                <h4>Create or Edit new export compliance record:</h4>
                <p>You don't have to know the ECCN or Harm Code up front, but the Part Number is obviously required</p>




                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>MFG Part Number</label>
                            <asp:TextBox ID="txtPartNumber" runat="server" placeholder="Enter Part Number" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Manufacturer</label>
                            <asp:TextBox ID="txtMFG" runat="server" placeholder="Enter Part Manufacturer" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>ECCN Code</label>
                            <asp:TextBox ID="txtECCN" runat="server" placeholder="Enter ECCN Number" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Harmonized Code</label>
                            <asp:TextBox ID="txtHarmCode" runat="server" placeholder="Enter Hamonized Code" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <%--<asp:LinkButton ID="lbEditDetails" runat="server" OnClick="lbEditDetails_Click" CssClass="btn-smc btn-smc-primary">Edit Details</asp:LinkButton>--%>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="submitDetails_Click" CssClass=" btn-smc btn-smc-success  pull-right" />
                        <asp:Button ID="btnCancelAdd" runat="server" Text="Cancel" OnClick="btnCancelAdd_Click" CssClass="btn-smc btn-smc-danger pull-right" />
                    </div>
                </div>
            </asp:Panel>

            <%--  <asp:Panel ID="pnlDeatils" runat="server" CssClass="card">
                <h4>Compliance record Details:
                </h4>
                <asp:LinkButton ID="lbEditDetails" runat="server" OnClick="lbEditDetails_Click" CssClass="btn-smc btn-smc-primary">Edit Details</asp:LinkButton>
                <asp:HiddenField ID="hfMPNID" runat="server" />
                <asp:DetailsView ID="dtvDetails" runat="server" Height="50px" AutoGenerateRows="False" DataKeyNames="MPNID" CssClass="table table-hover table-striped" GridLines="None">
                    <Fields>
                        <asp:BoundField DataField="MPN" HeaderText="MPN" SortExpression="MPN" />
                        <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" SortExpression="Manufacturer" />
                        <asp:BoundField DataField="ECCN" HeaderText="ECCN" SortExpression="ECCN" />
                        <asp:BoundField DataField="HarmonizedCode" HeaderText="HarmonizedCode" SortExpression="HarmonizedCode" />
                        <%--  <asp:BoundField DataField="PackageType" HeaderText="PackageType" SortExpression="PackageType" />
                        <asp:BoundField DataField="Lead_PinCount" HeaderText="Lead_PinCount" SortExpression="Lead_PinCount" />
                        <asp:BoundField DataField="Lead_PinPitch" HeaderText="Lead_PinPitch" SortExpression="Lead_PinPitch" />
                        <asp:BoundField DataField="Width" HeaderText="Width" SortExpression="Width" />
                        <asp:BoundField DataField="Length" HeaderText="Length" SortExpression="Length" />
                        <asp:BoundField DataField="Thickness" HeaderText="Thickness" SortExpression="Thickness" />
                        <asp:BoundField DataField="RoHS_Compliant" HeaderText="RoHS_Compliant" SortExpression="RoHS_Compliant" />
                        <asp:CheckBoxField DataField="SurfaceMount" HeaderText="SurfaceMount" SortExpression="SurfaceMount" />
                        <asp:CheckBoxField DataField="ThroughHole" HeaderText="ThroughHole" SortExpression="ThroughHole" />
                        <asp:CheckBoxField DataField="BGA" HeaderText="BGA" SortExpression="BGA" />
                        <asp:BoundField DataField="MemoryDevice" HeaderText="MemoryDevice" SortExpression="MemoryDevice" />
                        <asp:BoundField DataField="MSD_MSL" HeaderText="MSD_MSL" SortExpression="MSD_MSL" />
                        <asp:BoundField DataField="MoistureLevel" HeaderText="MoistureLevel" SortExpression="MoistureLevel" />
                        <asp:BoundField DataField="MPNID" HeaderText="MPNID" InsertVisible="False" ReadOnly="True" SortExpression="MPNID" />
                       <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                        <asp:BoundField DataField="Import_ID" HeaderText="Import_ID" SortExpression="Import_ID" />
                  </Fields>
                </asp:DetailsView>     
            </asp:Panel>--%>
        </div>
    </div>

    <script>
        $(function setGridClickEvents() {
            //Get the Grid ID (can be different if different master pages, shouldn't hard-code)
            var gridID = $("[id$=_gvMain]").attr("id");
            if (!gridID)
                return;
            gridID = '#' + gridID;
            //Instantiate the table, this will NOT Reload the table.  Datatabeles.net will just load the existing table when it detected teh table has already fired .DataTable();
            var table = $(gridID).DataTable();

            //Set the Click Events            
            $(gridID + ' tbody').on('click', 'tr', function () {
                //handle the Visual UI row highlighting
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }

                //Get the MPNID from column 4
                var MPNID = table.row(this).data()[4];
                if (MPNID != null && MPNID.length > 0) {
                    //Set the Hidden Value to be retained after postback.
                    $("#hfSelectedValue").val(MPNID);
                }
            });



        });
    </script>
</asp:Content>

