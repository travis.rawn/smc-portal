﻿<%@ Page Title="IDEA Inspection System" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="SM_Inspection_form.aspx.cs" Inherits="secure_sm_SM_Inspection_form" EnableEventValidation="false" %>


<%@ Register Src="~/assets/controls/InspectionImageManagerControl.ascx" TagPrefix="uc1" TagName="InspectionImageManagerControl" %>
<%@ Register Src="~/assets/controls/InspectionImageGallery.ascx" TagPrefix="uc1" TagName="InspectionImageGallery" %>
<%@ Register Src="~/assets/controls/sm_checkbox.ascx" TagPrefix="uc1" TagName="sm_checkbox" %>



<%@ MasterType VirtualPath="~/hs_flex.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

     <%--Make ActionPanel icons white--%>
    <style>       
        .ActionPanel a, a > span, p > a > span{
            color:#FFFFFF;
            color:white;           
        }
    </style>



    <script src="/Content/scripts/sm_idea.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $("#<% =txtCustomer.ClientID%>").bind("autocompleteselect", function (event, ui) {
                //This shows the select button if visible.
                $("#MainContent_lbselectStockCustomer").show();
            });


            $("#<% =txtCustomer.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/assets/webmethods/sm_webmethods.aspx/LoadCustomers",
                        data: "{input:'" + request.term + "'}",
                        dataType: "json",
                        autoFocus: true,
                        select: function (event, ui) {
                            if (ui.item) {
                                $("#<% =txtCustomer.ClientID%>").val(ui.item.value);
                                $("#MainContent_lbselectStockCustomer").show();
                            }
                        },
                        success: function (output) {
                            response(output.d);
                        },
                        error: function (errormsg) {
                            alert(errormsg.responseText);
                        }
                    });
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>&nbsp;IDEA INSPECTION FORM</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfTabName" runat="server" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" />
    <asp:Label ID="lblInspectionID" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Larger"></asp:Label><br />
    <asp:Label ID="lblPageError" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Larger" Style="color: red"></asp:Label><br />
    <%--Main Tab Interface--%>
    <div class="panel-default">
        <%--Tab Navigation--%>
        <ul class="nav nav-tabs" style="border-bottom: 0px" id="tabs">
            <li class="active"><a id="aHeader" href="#tpnlHeader" data-toggle="tab" onclick="GetActiveTab('MainContent_tpnlHeader'); return false;">Header<asp:Label ID="lblHeaderCheck" runat="server" Style="margin-left: 5px;" /></a></li>
            <li class=""><a id="aWarehouse" href="#tpnlWarehouse" data-toggle="tab" onclick="GetActiveTab('MainContent_tpnlWarehouse'); return false;">Warehouse<asp:Label ID="lblWarehouseCheck" runat="server" Style="margin-left: 5px;" /></a></li>
            <li class=""><a id="aDetail" href="#tpnlDetail" data-toggle="tab" onclick="GetActiveTab('MainContent_tpnlDetail'); return false;">Detail<asp:Label ID="lblDetailCheck" runat="server" Style="margin-left: 5px;" /></a></li>
            <li class=""><a id="aExtendedImagery" href="#tpnlExtendedImagery" data-toggle="tab" onclick="GetActiveTab('MainContent_tpnlExtendedImagery'); return false;">Imagery<asp:Label ID="lblExtendedImageryCheck" runat="server" Style="margin-left: 5px;" /></a></li>
            <li class=""><a id="aTraveler" href="#tpnlTraveler" data-toggle="tab" onclick="GetActiveTab('MainContent_tpnlTraveler'); return false;">Traveler<asp:Label ID="lblTravelerCheck" runat="server" Style="margin-left: 5px;" /></a></li>
        </ul>
        <%-- Tab Sections--%>
        <div class="tab-content">
            <%-- Header Tab--%>
            <div class="tab-pane fade active in" id="tpnlHeader">
                <%-- Header Content--%>
                <asp:Panel ID="pnlHeader" runat="server" CssClass="panel panel-default" Style="padding: 5px; border-radius: 0 !important;">
                    <div class="row">

                        <div class="col-sm-4">
                            <%--<asp:HiddenField ID="hfseller_email" runat="server" />--%>
                            <asp:HiddenField ID="hfinsp_id" runat="server" />

                            <div class="form-group">
                                <label>Supplier Data:</label>
                                <div class="well">
                                    <label>Date:</label>
                                    <%--<uc1:datetime_picker runat="server" ID="dtpHeader" />--%>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpHeader" />
                                    <%-- <asp:TextBox ID="txtInspectionDate" runat="server" TabIndex="1" Style="max-width: 320px" CssClass="form-control" placeholder="Date"></asp:TextBox>--%>
                                </div>
                                <div class="well">
                                    <label>Vendor Name:</label>
                                    <asp:DropDownList ID="ddlVendor" runat="server" Style="max-width: 320px" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="True" TabIndex="2" CssClass="form-control" placeholder="Choose Vendor:" onchange="ShowUpdateProgress();"></asp:DropDownList>
                                    <asp:Panel ID="pnlVendorPO" runat="server" Style="display: none">
                                        <label>Vendor PO:</label>
                                        <asp:DropDownList ID="ddlVendorPO" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVendorPO_SelectedIndexChanged" TabIndex="3" Style="max-width: 320px; display: none;" CssClass="form-control" placeholder="SMC PO" onchange="ShowUpdateProgress();"></asp:DropDownList>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlStockCustomer" runat="server" Style="display: none">
                                        <label>Customer:</label><span class="pull-right"><asp:CheckBox ID="cbxNoCustomer" runat="server" ToolTip="Choose this if you are inspecting a part not connected to any Customer or Sale" OnCheckedChanged="cbxNoCustomer_CheckedChanged" AutoPostBack="true" />No Customer:</span>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control" placeholder="Start typing to search"></asp:TextBox>
                                                <asp:HiddenField ID="hfCustomerID" runat="server" />
                                                <asp:HiddenField ID="hfSaleID" runat="server" />
                                                <asp:HiddenField ID="hfLineID" runat="server" />
                                            </div>
                                            <div class="form-group">
                                                <asp:LinkButton ID="lbselectStockCustomer" runat="server" OnClick="lbselectStockCustomer_Click" OnClientClick="ShowUpdateProgress();" CssClass="btn btn-sm btn-success hiddenElement">Select</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSale" runat="server" Style="display: none;">
                                        <label>Sales Order#:</label>
                                        <asp:Label ID="lblSMCSO" runat="server" TabIndex="4"></asp:Label>
                                        <asp:DropDownList ID="ddlStockSale" runat="server" OnSelectedIndexChanged="ddlStockSale_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" onchange="ShowUpdateProgress();"></asp:DropDownList><br />
                                    </asp:Panel>
                                    <div id="divCustomerPO" style="display: none" runat="server">
                                        <label>Customer PO: </label>
                                        <asp:Label ID="lblCustPO" runat="server" Enabled="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <label>Component Details:</label>
                            <div class="well">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Part Number:</label><asp:TextBox ID="txtPartNumber" runat="server" TabIndex="8" CssClass="form-control" placeholder="Part Number"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Manufacturer:</label><asp:TextBox ID="txtMFG" runat="server" TabIndex="9" CssClass="form-control" placeholder="Manufacturer"></asp:TextBox><br />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>QTY:</label><asp:TextBox ID="txtQTY" runat="server" TabIndex="10" Style="max-width: 320px" CssClass="form-control" placeholder="Quantity"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>D/C</label><asp:TextBox ID="txtDC" runat="server" TabIndex="11" Style="max-width: 320px" CssClass="form-control" placeholder="Date Code"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Lot#:</label><asp:TextBox ID="txtLot" runat="server" TabIndex="12" Style="max-width: 320px" CssClass="form-control" placeholder="Lot Number"></asp:TextBox><br />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>COO:</label>
                                            <asp:TextBox ID="txtCOO" runat="server" TabIndex="13" Style="max-width: 320px" CssClass="form-control" placeholder="Country of Origin"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Packaging:</label>
                                            <asp:TextBox ID="txtPackaging" runat="server" TabIndex="14" Style="max-width: 320px" CssClass="form-control" placeholder="Packaging"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Package Type:</label>
                                            <asp:TextBox ID="txtPackageType" runat="server" TabIndex="15" Style="max-width: 320px" CssClass="form-control" placeholder="Package Type"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Panel ID="pnl_txtgvPartData_Search" runat="server" Style="display: none;">
                                <div class="well">
                                    <label>No Customer:</label>
                                    Search for a stock part to inspect (i.e. "Stock Inspection")
                                    <div class="row form-group form-group-sm">
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtgvPartData_Search" runat="server" CssClass="form-control" Placeholder="Seach for part number"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4" style="padding-bottom: 10px;">
                                            <asp:Button ID="btngvPartData_Search" runat="server" Text="Search" OnClick="btngvPartData_Search_Click" CssClass="btn btn-success btn-sm" OnClientClick="ShowUpdateProgress();" />

                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <asp:Panel ID="pnl_gvPartData" runat="server" Style="display: none;">

                                <div class="well">
                                    <label>Select a Part:</label>
                                    <asp:Button ID="btnAutofillPartData" runat="server" Text="Autofill Selected Part Data" OnClick="btnAutofillPartData_Click" CssClass="btn btn-success btn-sm btn-block" OnClientClick="ShowUpdateProgress();" />
                                    <asp:GridView ID="gvPartData" runat="server" AllowPaging="false" CssClass="table table-hover table-striped gvstyling" GridLines="None" OnPageIndexChanging="gvPartData_PageIndexChanging" OnPageIndexChanged="gvPartData_PageIndexChanged" AutoGenerateColumns="false" EmptyDataText="No parts found.">
                                        <EmptyDataTemplate>
                                            <div class="panel panel-warning">
                                                <h5>No components found for this order.</h5>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbxPartSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Order" HeaderText="Order" SortExpression="Order" HtmlEncode="false" />
                                            <asp:BoundField DataField="OrderLine" HeaderText="OrderLine" SortExpression="OrderLine" ItemStyle-Font-Bold="true" />

                                            <asp:BoundField DataField="Part" HeaderText="Part" SortExpression="Part" />

                                            <asp:BoundField DataField="MFG" HeaderText="MFG" SortExpression="MFG" DataFormatString="{0:c}" />
                                            <asp:BoundField DataField="QTY" HeaderText="QTY" SortExpression="QTY" />
                                            <asp:BoundField DataField="ID" HeaderText="InspID" SortExpression="ID" />
                                            <asp:BoundField DataField="lineId" HeaderText="LineID" SortExpression="lineId" ItemStyle-CssClass="hideGridColumn" HeaderStyle-CssClass="hideGridColumn" />
                                        </Columns>

                                    </asp:GridView>

                                </div>
                            </asp:Panel>
                        </div>
                    </div>

                    <hr />
                    <%-- Header Imagery--%>
                    <h4><span class="label label-default center-block">Header Imagery</span></h4>
                    <div class="row">
                        <%--<asp:Panel ID="pnlHeaderImagery" runat="server">--%>
                        <div class="col-sm-12 imageManagerGroup">
                            <uc1:InspectionImageManagerControl runat="server" ID="imHeaderImageTop" />
                            <uc1:InspectionImageManagerControl runat="server" ID="imHeaderImageBot" />
                            <uc1:InspectionImageManagerControl runat="server" ID="imHeaderImageOther" />
                        </div>


                        <%--  </asp:Panel>--%>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 15px;" id="divSaveHeader">
                            <asp:LinkButton ID="lbSaveHeader" runat="server" Text="Save Header" OnClick="lbSaveHeader_Click" CssClass="btn btn-success btn-med btn-block" OnClientClick="ShowUpdateProgress();" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <%-- Warehouse Tab--%>
            <div class="tab-pane fade" id="tpnlWarehouse">
                <asp:Panel ID="pnlWarehouse" runat="server" CssClass="panel panel-default" Style="padding: 5px; border-radius: 0 !important;">
                    <%--General Carton Content --%>
                    <asp:Table ID="tblGeneralCarton" runat="server" Style="text-align: center" GridLines="None" CssClass="table table-hover table-striped">
                        <asp:TableHeaderRow>
                            <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">General Carton Inspection (IDEA 10.1.5.1)</asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                            <asp:TableCell Width="12%">Result</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlGenCarton1" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Complete</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Weigh product contents and record</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtGenCarton1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlGenCarton2" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Inspect outer package for evidence of damage</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtGenCarton2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlGenCarton3" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Complete</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Photograph / Record any findings</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtGenCarton3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlGenCarton4" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Inspect the sealing tape for evidence of tampering</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtGenCarton4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlGenCarton5" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>If damage or evidence of tampering is present, note findings and alert vendor and carrier for a possible freight claim</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtGenCarton5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="3">
                                         <div class="row">
                                           <h4><span class="label label-default center-block">General Carton Imagery</span></h4>
                                         </div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <%-- GenCarton Imagery--%>
                    <div class="row">
                        <div class="col-sm-12 imageManagerGroup">
                            <uc1:InspectionImageManagerControl runat="server" ID="imGenCarton1" />
                            <uc1:InspectionImageManagerControl runat="server" ID="imGenCarton2" />
                            <uc1:InspectionImageManagerControl runat="server" ID="imGenCarton3" />
                        </div>
                    </div>
                    <%--!Outer Carton --%>
                    <asp:Table ID="tblOuterCarton" runat="server" Style="text-align: center" GridLines="None" CssClass="table table-hover table-striped">
                        <asp:TableHeaderRow>
                            <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Outer Product Carton Inspection (IDEA 10.1.5.2)</asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                            <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton1" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Inspect package for any signs of damage or signs or being opened</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton2" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Complete</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Photograph contents while in box if they exhibit damage, tampering, or nonconformance</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton3" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Complete</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Photograph labels and sealing tape if they exhibit damage, tampering, or nonconformance</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton4" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Inspect sealing tapes</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton5" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Inspect labels and verify data</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton6" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Complete</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Examine the box and compare with manufacturer's website or golden sample</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton6" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlOuterCarton7" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Pass</asp:ListItem>
                                    <asp:ListItem Value="2">Fail</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Scan barcode to verify information is readable</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtOuterCarton7" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <%-- Outer Carton Imagery--%>
                    <asp:Panel ID="Panel1" runat="server">
                        <h4><span class="label label-default center-block">Outer Carton Imagery</span></h4>
                        <div class="row">
                            <div class="col-sm-12 imageManagerGroup">
                                <uc1:InspectionImageManagerControl runat="server" ID="imOuterCarton1" />
                                <uc1:InspectionImageManagerControl runat="server" ID="imOuterCarton2" />
                                <uc1:InspectionImageManagerControl runat="server" ID="imOuterCarton3" />
                            </div>
                        </div>

                    </asp:Panel>
                    <%-- Warehouse Save Panel--%>
                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 15px;" id="divSaveWhse">
                            <asp:Button ID="btnSaveWhse" runat="server" Text="Save Warehouse" OnClick="btnSaveWhse_Click" CssClass="btn btn-success btn-med btn-block" OnClientClick="ShowUpdateProgress();" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <%--Detail Tab--%>
            <div class="tab-pane fade" id="tpnlDetail">
                <asp:Panel ID="pnlDetail" runat="server" CssClass="panel panel-default" Style="padding: 5px; border-radius: 0 !important;">
                    <%--Detail Content--%>
                    <%-- Inner Contents--%>
                    <asp:Table ID="tblInnerContents" runat="server" Style="text-align: center" GridLines="None" CssClass="table table-hover table-striped">
                        <asp:TableHeaderRow>
                            <asp:TableCell ColumnSpan="3" Font-Bold="True" HorizontalAlign="Center" CssClass="bg-primary">Inner Contents Inspection (IDEA 10.1.5.3) | (Sections 10.1.5.1 & 10.1.5.2, Recorded on F-750-015)</asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                            <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlInnerContents1" runat="server" CssClass="form-control">
                                    <%--<asp:DropDownList ID="ddlInnerContents1" runat="server" AutoPostBack="True" OnChange="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(this.name, '', true, '', '', false, true))" OnSelectedIndexChanged="ddlInnerContents1_SelectedIndexChanged">--%>
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Inspect inner contents, packaging, bag seal, and labels; photograph if they exhibit damage, tampering, or nonconformance</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlInnerContents2" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Compare inner labels with outer carton labels and photograph</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlInnerContents3" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Verify bag seal date with product date code</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <%-- <asp:DropDownList ID="ddlInnerContents4" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlInnerContents4_SelectedIndexChanged" EnableViewState="false">--%>
                                <asp:DropDownList ID="ddlInnerContents4" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Scan the barcode to verify the information is readable and matches the product identification, record results</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlInnerContents5" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Verify the product is properly packaged for the required Moisture Sensitivity Label (MSL) and compare the MSL level with datasheet</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlInnerContents6" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Verify the package is ESD compliant</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents6" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlInnerContents7" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                    <asp:ListItem Value="other">Other</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell><asp:TableCell>Read and Record Status of the Humidity Indicator Card (HIC)</asp:TableCell><asp:TableCell>
                                <asp:TextBox ID="txtInnerContents7" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <%-- Inner Contents Imagery--%>
                    <asp:Panel ID="Panel2" runat="server">
                        <h4><span class="label label-default center-block">Inner Contents Imagery</span></h4>
                        <div class="row">
                            <div class="col-sm-12 imageManagerGroup">
                                <uc1:InspectionImageManagerControl runat="server" ID="imInnerContents1" />
                                <uc1:InspectionImageManagerControl runat="server" ID="imInnerContents2" />
                                <uc1:InspectionImageManagerControl runat="server" ID="imInnerContents3" />
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Carrier Type Selector--%>
                    <div class="form-group">
                        <asp:Table ID="tblCarrierInsp" runat="server" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow Style="text-align: center">
                                <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Inspection of Carrier (IDEA 10.1.5.4)</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow>
                                <asp:TableCell>

                                    <div class="input-group">
                                        <label for="ddlCarrierType" style="float: left;">Please choose the carrier type:</label>

                                        <asp:DropDownList ID="ddlCarrierType" runat="server" CssClass="form-control" Style="width: 109px; margin-left: 10px;">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Tube</asp:ListItem>
                                            <asp:ListItem Value="2">Reel</asp:ListItem>
                                            <asp:ListItem Value="3">Tray</asp:ListItem>
                                            <asp:ListItem Value="4">Bulk</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>


                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </div>

                    <asp:Panel ID="pnlTube" runat="server" Style="display: none">
                        <asp:Table ID="tblTube" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow>
                                <asp:TableCell CssClass="bg-primary" ColumnSpan="3" Font-Bold="True" HorizontalAlign="Center">Tube</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow CssClass="bg-info" Font-Italic="True" HorizontalAlign="Center">
                                <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTube1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>If factory packaged, verify tubes are imprinted with manufacturer name / logo</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTube1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTube2" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify all tubes are the same length and look clean and new (not yellowed or excessively scratched)</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTube2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTube3" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify parts in the tube are all oriented in the same direction</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTube3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTube4" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify quantity of parts in tubes is consistent</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTube4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTube5" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify all tubes have stoppers and they are the same in each tube</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTube5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%-- Tube Carrier Imagery--%>
                        <asp:Panel ID="Panel3" runat="server">
                            <h4><span class="label label-default center-block">Tube Carrier Imagery</span></h4>
                            <div class="row">
                                <div class="col-sm-12 imageManagerGroup">
                                    <uc1:InspectionImageManagerControl runat="server" ID="imTube1" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imTube2" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imTube3" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%-- Reel Section--%>
                    <asp:Panel ID="pnlReel" runat="server" Style="display: none">
                        <asp:Table ID="tblReel" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow>
                                <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Reel</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                                <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify labels and label placement</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel2" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Scan and verify that barcode scan and printed information on label are  consistent with each other.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel3" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify that size of reel and the material (paper /plastic) match manufacturer spec sheet.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel4" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify factory reel has proper leader tape.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel5" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify reel count (no empty pockets) and cover film is properly attached.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel6" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Compare part orientation in tape with manufacturer spec sheet and verify the parts oriented in the tape are consistent.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel6" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlReel7" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify that there are no splices or cuts in the tape.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtReel7" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%-- Reel CarrierImagery--%>
                        <asp:Panel ID="Panel4" runat="server">
                            <h4><span class="label label-default center-block">Reel Imagery</span></h4>
                            <div class="row">
                                <div class="col-sm-12 imageManagerGroup">
                                    <uc1:InspectionImageManagerControl runat="server" ID="imReel1" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imReel2" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imReel3" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%-- Tray Section--%>
                    <asp:Panel ID="pnlTray" runat="server" Style="display: none">
                        <asp:Table ID="tblTray" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow>
                                <asp:TableCell CssClass="bg-primary" ColumnSpan="3" Font-Bold="True" HorizontalAlign="Center">Tray</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow CssClass="bg-info" Font-Italic="True" HorizontalAlign="Center">
                                <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTray1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify the color and width of the banding is what is expected.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTray1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTray2" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Note if the banding has any pre-printed markings.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTray2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTray3" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify that the trays are oriented the same way.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTray3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTray4" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify that there is a top tray to protect the parts.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTray4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTray5" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify and record if covered by cardboard and verify if cardboard is anti-static.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTray5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlTray6" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify the chips&#39; orientation in the tray is consistent with no missing pieces.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtTray6" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%-- Try Carrier Imagery--%>
                        <asp:Panel ID="Panel5" runat="server">
                            <h4><span class="label label-default center-block">Tray Carrier Imagery</span></h4>
                            <div class="row">
                                <div class="col-sm-12 imageManagerGroup">
                                    <uc1:InspectionImageManagerControl runat="server" ID="imTray1" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imTray2" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imTray3" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%--Bulk Section--%>
                    <asp:Panel ID="pnlBulk" runat="server" Style="display: none">
                        <asp:Table ID="tblBulk" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow>
                                <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Bulk</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                                <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlBulk1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify count.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtBulk1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlBulk2" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify the bag is correct for the type of parts received, (i.e.. ESD or MBB).</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtBulk2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlBulk3" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify any manufacturer markings on the bag match specification.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtBulk3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlBulk4" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Scan and verify that barcode scan and printed information on label are consistent with each other.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtBulk4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%-- Bulk Carrier Imagery--%>
                        <asp:Panel ID="Panel6" runat="server">
                            <h4><span class="label label-default center-block">Bulk Carrier Imagery</span></h4>
                            <div class="row">
                                <div class="col-sm-12 imageManagerGroup">
                                    <uc1:InspectionImageManagerControl runat="server" ID="imBulk1" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imBulk2" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imBulk3" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>

                    <%-- Initial Inspection (IDEA 10.2.1) Section --%>
                    <asp:Panel ID="pnlInitInspection" runat="server" Visible="true">
                        <asp:Table ID="tblInitInspection" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow>
                                <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Initial Inspection (IDEA 10.2.1)</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                                <asp:TableCell Width="12%">Complete?</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlInitInspection1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify the part number, manufacturer, and quantity match the purchase order and packing slip.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtInitInspection1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlInitInspection2" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify that there is an original factory label; insure there is not a label over label - Confirm MFG Logo is printed on label and spelled correctly & smudge free.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtInitInspection2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlInitInspection4" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Confirm the date code meets any restrictions specified on the purchase agreement.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtInitInspection4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlInitInspection5" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="3">Unknown</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Are the receiving parts (as defined by MPN or datasheet) RoHS Compliant?.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtInitInspection5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlInitInspection6" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Does the Customer's PO, Quality Notes, or other requirements define RoHS compliance?</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtInitInspection6" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlInitInspection7" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Is there any indication of damage to the product or packaging?  If so, record any signs of damage to the product or packaging.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtInitInspection7" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%-- Initial Inspection Imagery--%>
                        <asp:Panel ID="pnlInitInspectionImagery" runat="server">
                            <h4><span class="label label-default center-block">Initial Inspection Imagery</span></h4>
                            <div class="row">
                                <div class="col-sm-12 imageManagerGroup">
                                    <uc1:InspectionImageManagerControl runat="server" ID="imInitInspection1" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imInitInspection2" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imInitInspection3" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%--Detailed Inspection (Visual)  (IDEA 10.3.1) Section--%>
                    <asp:Panel ID="pnlDetailVisual" runat="server" Visible="true">
                        <asp:Table ID="tblDetailVisual" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                            <asp:TableHeaderRow>
                                <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Detailed Inspection (Visual)  (IDEA 10.3.1)</asp:TableCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                                <asp:TableCell Width="12%">Result</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify the logo and markings match the manufacturer's specifications.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual2" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Confirm the markings are clear and do not appear to be re-marked or re-stamped.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual3" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Confirm the markings are consistent throughout the package type from part to part and of the top and bottom of the parts (placement, font type, color, and texture).</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual4" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Inspect laser marks for burn holes caused by after market laser equipment.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual4" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual5" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Inspect for Inconsistencies in package indents shape, size, and locations.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual5" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual6" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Confirm that there are no burn or blister marks, or evidence of exposure to excessive heat.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual6" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual7" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Ensure there are no colored dots or ink marks on the tops of components indicating previous testing or programming, unless allowed or required by the purchase order.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual7" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual8" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Look for flux or chemical residue and tool marks or heat sink markings indicating refurbished parts.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual8" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual9" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Confirm there are no cracks on the surface of the parts.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual9" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual10" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Verify the lead / pin count and formation/terminal layout or type of lead (DIP, SMB, GULL WING, etc.) match the datasheet.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual10" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual11" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Inspect for damaged leads (bent, scratched, broken, dented, missing, coplanarity, etc.) indicating the part has been salvaged or mishandled.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual11" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual12" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Ensure that leads are not oxidized, re-tinned with solder (re-balled for BGAs), show signs of corrosion, or contamination from foreign substances.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual12" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual13" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Look for leads that are too shiny for older date codes, or too dull for new date codes; the pins should be similar in gloss or shine, color, and texture.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual13" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual14" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Confirm there are no scratches on the inside and outside of leads; scratches under the BGA spheres, are typically a sign of re-balled parts.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual14" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual15" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                        <asp:ListItem Value="other">Other</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Inspect BGAs, LGAs and any terminals, lugs, or connectors to ensure that the component has not been used, refurbished, mishandled, or contaminated.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual15" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual16" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>Photograph markings front and back for records.</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual16" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell>
                                    <asp:DropDownList ID="ddlDetailVisual17" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:TableCell><asp:TableCell>If nonconforming, document and photograph nonconformance(s).</asp:TableCell><asp:TableCell>
                                    <asp:TextBox ID="txtDetailVisual17" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%-- >Detail Visual Imagery--%>
                        <asp:Panel ID="pnlDetailVisualImagery" runat="server">
                            <h4><span class="label label-default center-block">Detail Visual Imagery</span></h4>
                            <div class="row">
                                <div class="col-sm-12 imageManagerGroup">
                                    <uc1:InspectionImageManagerControl runat="server" ID="imDetailVisual1" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imDetailVisual2" />
                                    <uc1:InspectionImageManagerControl runat="server" ID="imDetailVisual3" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%-- Detailed Inspection (Solvents)  (IDEA 10.3.2) --%>
                    <div class="form-group">
                        <label>Is Solvent testing being performed?</label>
                        <asp:DropDownList ID="ddlis_solvent" runat="server" CssClass="form-control">
                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:DropDownList><br />
                        <asp:Panel ID="pnlDetailSolvents" runat="server">
                            <asp:Table ID="tblDetailSolvents" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                                <asp:TableHeaderRow>
                                    <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Detailed Inspection (Solvents)  (IDEA 10.3.2)</asp:TableCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                                    <asp:TableCell Width="12%">Result</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell>


                                        <asp:DropDownList ID="ddlDetailSolvents1" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                            <%--<asp:ListItem Value="3">N/A</asp:ListItem>--%>
                                            <asp:ListItem Value="other">Other</asp:ListItem>
                                        </asp:DropDownList>


                                    </asp:TableCell><asp:TableCell>Perform device marking test (test for remarking - mineral spirits & alcohol swab).</asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtDetailSolvents1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell>


                                        <asp:DropDownList ID="ddlDetailSolvents2" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                            <%--<asp:ListItem Value="3">N/A</asp:ListItem>--%>
                                            <asp:ListItem Value="other">Other</asp:ListItem>
                                        </asp:DropDownList>


                                    </asp:TableCell><asp:TableCell>Perform device surface test (test for blacktopping-acetone swab).</asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtDetailSolvents2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell>
                                        <asp:DropDownList ID="ddlDetailSolvents3" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                            <%--<asp:ListItem Value="3">N/A</asp:ListItem>--%>
                                            <asp:ListItem Value="other">Other</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell><asp:TableCell>Perform scrape test (as needed).</asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtDetailSolvents3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <%-- Heated Solvents Imagery--%>
                            <asp:Panel ID="Panel9" runat="server">
                                <h4><span class="label label-default center-block">Heated Solvents Imagery</span></h4>
                                <div class="row">
                                    <div class="col-sm-12 imageManagerGroup">
                                        <uc1:InspectionImageManagerControl runat="server" ID="imDetailSolvents1" />
                                        <uc1:InspectionImageManagerControl runat="server" ID="imDetailSolvents2" />
                                        <uc1:InspectionImageManagerControl runat="server" ID="imDetailSolvents3" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                    <%--Detailed Inspection (Mechanical) (IDEA 10.3.3) Section--%>
                    <div class="form-group">
                        <label>Is mechanical testing being performed?</label>
                        <asp:DropDownList ID="ddlis_mech" runat="server" CssClass="form-control">
                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Panel ID="pnlMech" runat="server">
                            <asp:Table ID="tblpnlDetailMech" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                                <asp:TableHeaderRow>
                                    <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Detailed Inspection (Mechanical)  (IDEA 10.3.3)</asp:TableCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow HorizontalAlign="Center" Font-Italic="True" CssClass="bg-info">
                                    <asp:TableCell Width="12%">Result</asp:TableCell><asp:TableCell Width="35%">Characteristics Verified</asp:TableCell><asp:TableCell Width="50%">Comments/Observations</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell>
                                        <asp:DropDownList ID="ddlDetailMech1" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                            <%--<asp:ListItem Value="3">N/A</asp:ListItem>--%>
                                            <asp:ListItem Value="other">Other</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell><asp:TableCell>Determine the min / max or acceptable tolerance range of each measurement being taken from the manufacturer datasheet.</asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtDetailMech1" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell>
                                        <asp:DropDownList ID="ddlDetailMech2" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                            <asp:ListItem Value="other">Other</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell><asp:TableCell>Measure, verify and record the package dimensions.</asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtDetailMech2" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell>
                                        <asp:DropDownList ID="ddlDetailMech3" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                            <asp:ListItem Value="other">Other</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell><asp:TableCell>Measure for thickness variation.</asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtDetailMech3" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <%-- Mechanical Inspection Imagery--%>
                            <asp:Panel ID="Panel10" runat="server">
                                <h4><span class="label label-default center-block">Mechanical Inspection Imagery</span></h4>
                                <div class="row">
                                    <div class="col-sm-12 imageManagerGroup">
                                        <uc1:InspectionImageManagerControl runat="server" ID="imDetailMech1" />
                                        <uc1:InspectionImageManagerControl runat="server" ID="imDetailMech2" />
                                        <uc1:InspectionImageManagerControl runat="server" ID="imDetailMech3" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                    <%--Detailed Inspection (Additional Testing) Section--%>
                    <div class="form-group">
                        <label>Is additional testing being performed?</label>
                        <asp:DropDownList ID="ddlisAddTesting" runat="server" CssClass="form-control">
                            <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Panel ID="pnlisAddTesting" runat="server">
                            <asp:Table ID="tblAddTesting" runat="server" Style="text-align: left" GridLines="None" CssClass="table table-hover table-striped">
                                <asp:TableHeaderRow>
                                    <asp:TableCell ColumnSpan="3" CssClass="bg-primary" Font-Bold="True" HorizontalAlign="Center">Additional Testing Performed</asp:TableCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow HorizontalAlign="Left" Font-Italic="True" CssClass="bg-info">
                                    <asp:TableCell Width="100%">Details</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow VerticalAlign="Middle">
                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                                        <asp:TextBox ID="txtAddTesting" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Type additional testing information here."></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <%-- Additioinal Testing Imagery--%>
                            <asp:Panel ID="Panel11" runat="server">
                                <h4><span class="label label-default center-block">Additioinal Testing Imagery</span></h4>
                                <div class="row">
                                    <div class="col-sm-12 imageManagerGroup">
                                        <uc1:InspectionImageManagerControl runat="server" ID="imAddTesting1" />
                                        <uc1:InspectionImageManagerControl runat="server" ID="imAddTesting2" />
                                        <uc1:InspectionImageManagerControl runat="server" ID="imAddTesting3" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 15px;" id="divSaveDetail">
                            <asp:Button ID="btnSaveDetail" runat="server" Text="Save Detailed Inspection" OnClick="btnSaveDetail_Click" CssClass="btn btn-success btn-md btn-block" OnClientClick="ShowUpdateProgress();" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <%--  Extended Imagery--%>
            <div class="tab-pane fade" id="tpnlExtendedImagery">
                <div class="panel-group">
                    <div class="panel panel-default ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Extended Imagery</label>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col.sm-12">
                                    <uc1:InspectionImageGallery runat="server" ID="iig" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Traveler Tab--%>
            <div class="tab-pane fade" id="tpnlTraveler">
                <asp:Panel ID="pnlTraveler" runat="server" CssClass="panel panel-default" Style="padding: 5px; border-radius: 0 !important;">
                    <h5>Lab Traveler</h5>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerMoisture" autoPostBack="false" theText="Moisture (WI854-001)" />
                    <div id="divTravelerMoisture">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-3">
                                    Required Temperature?
                                <asp:TextBox ID="txtTravelerReqTemp" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    Required Cycle Time?
                                <asp:TextBox ID="txtTravelerReqCycle" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    Start time:
                             <uc1:datetime_flatpicker runat="server" ID="dtpTravelerMoistureStart" />
                                </div>
                                <div class="col-sm-3">
                                    Completion Time:
                                <%--<asp:TextBox ID="txtCompletionTime" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                    <uc1:datetime_flatpicker runat="server" ID="dtpTravelerMoistureEnd" />

                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerXray" autoPostBack="false" theText="Xray (WI854-004)" />
                    <div id="divTravelerXray">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerXrayQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerXrayResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerXrayResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerHeated" autoPostBack="false" theText="Heated (WI854-002)" />
                    <div id="divTravelerHeated">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerHeatedQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerHeatedResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerHeatedResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerJetEtch" autoPostBack="false" theText="JetEtch (WI854-007)" />
                    <div id="divTravelerJetEtch">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerJetEtchQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerJetEtchResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerJetEtchResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerBasicElec" autoPostBack="false" theText="Basic Electric" />
                    <div id="divTravelerBasicElec">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerBasicElecQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerBasicElecResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerBasicElecResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerSentry" autoPostBack="false" theText="Sentry (WI854-005)" />
                    <div id="divTravelerSentry">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerSentryQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerSentryResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerSentryResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerSolder" autoPostBack="false" theText="Solderability (WI854-006)" />
                    <div id="divTravelerSolder">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerSolderQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerSolderResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerSolderResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerXeltek" autoPostBack="false" theText="Xeltek" />
                    <div id="divTravelerXeltek">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerXelTekQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments
                                   <asp:TextBox ID="txtTravelerXelTekResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerXelTekResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:sm_checkbox runat="server" ID="cbxTravelerXrf" autoPostBack="false" theText="XRF" />
                    <div id="divTravelerXrf">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-2">
                                    Sample Qty:
                                <asp:TextBox ID="txtTravelerXrfQty" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-8">
                                    Analysis / Comments <strong><em><span style="margin-left:10px; font-size:12px;">**RoHS 3 Substances: Pb, Cd, Hg, Cr VI, PBB, PBDE, DEHP, BBP, DBP, DIBP**</span></em></strong>
                                   <asp:TextBox ID="txtTravelerXrfResult" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Result:
                                <asp:DropDownList ID="ddlTravelerXrfResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-Choose-" Value="Choose"></asp:ListItem>
                                    <asp:ListItem Text="Pass" Value="Pass"></asp:ListItem>
                                    <asp:ListItem Text="Fail" Value="Fail"></asp:ListItem>
                                    <asp:ListItem Text="Inconclusive" Value="Inc"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </div>


        </div>
    </div>
    <%-- Main Save / Delete panel--%>
    <div class="container">
        <div class="row">
            <div class="well well-sm">
                <asp:Button ID="btnReturn" runat="server" Text="Return to Search" OnClick="btnReturn_Click" CssClass="btn btn-primary btn-med" />
                <asp:Button ID="btnSaveAll" runat="server" Text="Save All" OnClick="btnSaveAll_Click" CssClass="btn btn-success btn-med" OnClientClick="ShowUpdateProgress();" />
                <asp:Button ID="btnDelete" runat="server" Style="display: none;" OnClick="btnDelete_Click" CssClass="btn btn-danger btn-med" />
                <strong>
                    <asp:Label ID="lblInspector" runat="server"></asp:Label></strong>
                <asp:Label ID="lblHeaderError" runat="server" Style="display: none;" CssClass="alert alert-danger"></asp:Label>
            </div>
        </div>
    </div>
    <%-- Floating Action Panel--%>
    <div id="divAction" class="ActionPanel" runat="server">
        <asp:Panel ID="pnlAction" runat="server" CssClass="collapse-content" Style="display: none">
            <div id="APHeader" style="text-align: center; color: black;">
                <span style="font-weight: bolder; font-size: 16px;">Action Panel</span><br />
                Status:<asp:Label ID="lblAP_Status" runat="server" Font-Bold="True" Font-Italic="True" Style="color: black;"></asp:Label><br />
            </div>
            <%-- <asp:Panel ID="pnlCompletion" Style="display: none;" runat="server">--%>
            <hr class="separator" />
            <asp:Panel ID="pnlAPResults" runat="server">
                <label>Initial Inspection</label><br />
                <div id="divResult">
                    <asp:Label ID="lblResult" runat="server" Font-Italic="True" Text="Result:" Style="color: black;"></asp:Label>
                    <asp:DropDownList ID="ddlResult" runat="server" CssClass="form-control">
                        <asp:ListItem Value="choose">-Choose-</asp:ListItem>
                        <asp:ListItem Value="1">Pass</asp:ListItem>
                        <asp:ListItem Value="2">Fail</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:Panel ID="pnlPass" runat="server">
                    <asp:Label ID="lblExpectedShip" runat="server" Font-Italic="True" Text="Expected Ship Date:" Style="color: black;"></asp:Label>
                    <%--<uc1:datetime_picker runat="server" ID="dtpExpectedShip" />--%>
                    <uc1:datetime_flatpicker runat="server" ID="dtpExpectedShip" />
                </asp:Panel>
                <asp:Label ID="lblResultNotes" runat="server" Font-Italic="True" Text="Result Notes:" Style="color: black;"></asp:Label><br />
                <asp:TextBox ID="txtResultNotes" runat="server" TextMode="MultiLine" Height="50px" CssClass="form-control"></asp:TextBox>
                <div id="pnlCompletionFooter" style="text-align: center">
                    <asp:Button ID="btnAP_MarkComplete" Text="CompletionButton" runat="server" Visible="true" OnClick="btnAP_MarkComplete_Click" CssClass="btn btn-sm btn-success btn-block" OnClientClick="ShowUpdateProgress();" />
                </div>
                <asp:Panel ID="pnlFinalInspection" runat="server" Visible="false">
                    <br />
                    <hr class="separator" />
                    <label>Final Inspection</label><br />
                    <asp:TextBox ID="txtTurnBackNotes" runat="server" TextMode="MultiLine" CssClass="form-control" Placeholder="Turn-back issues (If any)."></asp:TextBox>
                    <asp:LinkButton ID="lbFinalComplete" runat="server" CssClass="btn btn-sm btn-success btn-block" OnClientClick="ShowUpdateProgress();" OnClick="lbFinalComplete_Click" Visible="false">Final Complete</asp:LinkButton>
                    <asp:LinkButton ID="lbTurnBack" runat="server" CssClass="btn btn-sm btn-danger btn-block" OnClientClick="ShowUpdateProgress();" OnClick="lbTurnBack_Click" Visible="false">Turn-Back</asp:LinkButton>
                    <asp:LinkButton ID="lbResolveTurnBack" runat="server" CssClass="btn btn-sm btn-success btn-block" OnClientClick="ShowUpdateProgress();" OnClick="lbResolveTurnBack_Click" Visible="false">Resolve Turn-Back</asp:LinkButton>
                    <div style="text-align: center">
                        <h5><a href="#" data-toggle="modal" data-target="#mdlTurnbacks" onclick="return false;" style="margin-left: 10px;">Turn-Back history</a><br />
                        </h5>
                    </div>


                    <asp:Button ID="btnReport" runat="server" Text="View Report" Visible="false" CssClass="btn btn-sm btn-info btn-block" OnClientClick="showReport(); return false;" />
                    <asp:Button ID="btnSummary" runat="server" Text="View Summary" Visible="false" CssClass="btn btn-sm btn-info btn-block" OnClientClick="showSummary();return false;" />
                    <asp:Button ID="btnTestEmail" runat="server" Text="Test Email" OnClick="btnTestEmail_Click" CssClass="btn btn-sm btn-info btn-block" Visible="false" />
                </asp:Panel>
            </asp:Panel>
            <%-- </asp:Panel>--%>
            <hr class="separator" />
        </asp:Panel>
        <div id="APFooter" style="text-align: center">

            <div class="text-right">


                <span data-toggle="tooltip" title="Save All">
                    <asp:LinkButton ID="btnAP_SaveAll" Text="Save All" runat="server" OnClick="btnSaveAll_Click" CssClass="btn btn-sm btn-primary btn-block" OnClientClick="ShowUpdateProgress();">
                          <span   class="far fa-save"></span>
                    </asp:LinkButton>
                </span>
                <span data-toggle="tooltip" title="Clone">
                    <asp:LinkButton ID="lbCloneReport" Text="Clone Report" runat="server" OnClick="lbCloneReport_Click" CssClass="btn btn-sm btn-primary btn-block" OnClientClick="ShowUpdateProgress();">
                          <span   class="fa fa-copy"></span>
                    </asp:LinkButton>
                </span>


                <span data-toggle="tooltip" title="Return to Search">
                    <asp:LinkButton ID="btnAP_Return" runat="server" Text="Return to Search" OnClick="btnReturn_Click" CssClass="btn btn-sm btn-primary btn-block">
                         <span   class="fas fa-reply"></span>
                    </asp:LinkButton>
                </span>
                <span data-toggle="tooltip" title="Return to Top">
                    <asp:LinkButton ID="lbScrollTop" runat="server" Text="Return to Top" OnClientClick="window.scrollTo(0,0); return false;" CssClass="btn btn-sm btn-primary btn-block">
                         <span   class="fa fa-arrow-up"></span>
                    </asp:LinkButton>
                </span>
                <span data-toggle="tooltip" title="Toggle Action Panel">
                    <button id="btnAPtoggle" type="button" class="btn btn-success btn-block">
                        <span id="APtoggle" class="fas fa-bars"></span>
                    </button>
                </span>
                <%--<asp:Button ID="btnAP_CompleteInsp" runat="server" Text="Complete Inspection" Style="display: none;" CssClass="btn btn-sm btn-success btn-block" OnClientClick="return ToggleUpdateCompletionPanel();" />--%>
            </div>
            <%-- <asp:Label ID="lblAPSave_Status" Style="display: none;" runat="server" Font-Bold="True" Font-Italic="True" /><br />--%>
        </div>
    </div>
    <%-- Re-Send Email Modal--%>
    <div id="divModal" class="ModalHidden" runat="server">
        <div style="text-align: center">
            <asp:Label ID="lblModalText" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Larger" Text="This is Modal Text.  Stuff goes here programatically." /><br />
            <br />
            <asp:Button ID="btnModalYes" runat="server" Text="Yes" OnClick="btnModalYes_Click" />
            <asp:Button ID="btnModalNo" runat="server" Text="No" OnClick="btnModalNo_Click" />
        </div>
    </div>
    <div id="divModalOverlay" runat="server"></div>





    <%-- Turnback Modal--%>
    <div id="mdlTurnbacks" class="modal fade" role="dialog" style="width: 100%">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Turnback History</h4>
                </div>
                <div class="modal-body">
                    <asp:GridView ID="gvTurnBacks" runat="server" CssClass="table table-hover table-striped tablesorter" GridLines="None" EmptyDataText="No new customers." AutoGenerateColumns="false">
                        <Columns>
                            <asp:BoundField DataField="inspection_agent" HeaderText="Inspector" SortExpression="inspection_agent" />
                            <asp:BoundField DataField="turn_back_date" HeaderText="Date" SortExpression="turn_back_date" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="turn_back_notes" HeaderText="Notes" SortExpression="turn_back_notes" />
                            <asp:BoundField DataField="turn_back_agent" HeaderText="TB Agent" SortExpression="turn_back_agent" />
                            <asp:BoundField DataField="turn_back_resolution_date" HeaderText="Res Date" SortExpression="turn_back_resolution_date" DataFormatString="{0:d}" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>

        </div>
    </div>
    <%--End Turnback Modal--%>


    <script>

        $("#MainContent_btnAutofillPartData").click(function () { ShowUpdateProgress(); });
    </script>
</asp:Content>

