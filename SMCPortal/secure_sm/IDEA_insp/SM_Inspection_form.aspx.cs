﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_sm_SM_Inspection_form : Page
{
    //Public Variables
    //RzDataContext RZDC = new RzDataContext();
    //SMCPortalDataContext PDC = new SMCPortalDataContext();
    sm_binaryEntities smb = new sm_binaryEntities();


    insp_head header = null;
    insp_detail detail = null;
    insp_whse whse = null;
    insp_traveler traveler = null;

    qc_turn_back lastTurnBack;
    List<qc_turn_back> turnbackList = new List<qc_turn_back>();
    company TheCustomer;
    company TheVendor;
    ordhed_sale TheSale;
    ordhed_purchase ThePurchase;
    n_user TheAgent;

    List<insp_images> images;
    int UploadedImagesCount = 0;


    //int insp_id;
    string insp_modified_date;

    //int whse_id;
    string inspector;
    string CurrentStatus = "";
    string Result = "";
    bool headerSaved = false;
    List<DropDownList> alldropdowns = new List<DropDownList>();
    List<string> CarrierTypesList = new List<string>() { "ddlTube", "ddlReel", "ddlTray", "ddlBulk", "ddlDetailSolvents", "ddlDetailMech" };
    //This declares a checkmark for use on TabPanels
    const char chkMark = (char)(0X2713);

    SM_Tools tools = new SM_Tools();
    List<string> SalesLineIDs = new List<string>();
    List<string> POLineIDs = new List<string>();
    List<string> lineIdList = new List<string>();


    string strInspID = "";
    private int intInspID = 0;

    protected List<string> warehouseSectionList = new List<string> { "OuterCarton", "GenCarton" };
    List<string> detailSectionList = new List<string> {
       "InnerContents",
       "Tube",
       "Reel",
       "Tray",
       "Bulk",
       "InitInspection",
       "DetailVisual",
       "DetailSolvents",
       "DetailMech",
       "AddTesting"
        };



    protected string SetActiveTab(string sectionID)
    {

        switch (sectionID)
        {
            case "MainContent_tpnlWarehouse":
                {
                    return "Warehouse";
                }
            case "MainContent_tpnlDetail":
                {
                    return "Detail";
                }
            case "MainContent_tpnlExtendedImagery":
                {
                    return "Detail";
                }
            default:
                return "ExtendedImagery";
        }
    }


    protected override void OnPreLoad(EventArgs e)
    {

        base.OnPreLoad(e);
        strInspID = Tools.Strings.SanitizeInput(Request.QueryString["insp_id"]);
        if (!string.IsNullOrEmpty(strInspID))
            intInspID = Convert.ToInt32(strInspID);
        if (intInspID == 0)
            return;
        //When I update Text on a control, the Pre-Load resets this text to whatever was in the DB
        //f (!Page.IsPostBack)
        LoadExtendedImagery();

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LoadDates();
            strInspID = Tools.Strings.SanitizeInput(Request.QueryString["insp_id"]);
            if (string.IsNullOrEmpty(strInspID))
                Response.Redirect("~/secure_sm/IDEA_insp/SM_Inspection_search.aspx", false);
            else
                intInspID = Convert.ToInt32(strInspID);

            if (!GetHeader())
                tools.HandleResultJS("Invalid Inspection ID, returning to search.", false, Page, "~/secure_sm/IDEA_insp/SM_Inspection_search.aspx");
            LoadAllSectionObjects();
            GetCustomerAndVendorObjects();
            LoadAll();




        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void LoadAll()
    {
        if (!Page.IsPostBack) //Not a refresh, is new page load.
        {
            //Here I should be pulling data from Database, Since it'll only run on initial load.
            //Should explicitly call methods that update this data on particular events.
            LoadAllValues();
            SetTabComplete();
            SetImageControlProperties();
            //Load Images   
            LoadAllImages();

        }

        //Load Page Controls, Panels, etc.

        LoadAgent();
        LoadUI();
    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {

            dtpHeader.SelectedDate = DateTime.Now;
            dtpTravelerMoistureStart.SelectedDate = DateTime.Now;
            dtpTravelerMoistureStart.EnableTime = true;
            dtpTravelerMoistureEnd.SelectedDate = DateTime.Now;
            dtpTravelerMoistureEnd.EnableTime = true;
        }

    }

    private void LoadUI()//HAs to run on Postbacks and non postbacks, bacsuer of
    {

        //Vendor PO
        if (ddlVendor.SelectedValue == "1")//Stock inspection, show Customer Search, hide PO DDL
        {
            pnlVendorPO.Style.Add("display", "none");
            pnlStockCustomer.Style.Add("display", "inline");
        }
        else if (ddlVendor.SelectedValue != "0") //Actual Vendor Selected, show po ddl
        {
            if (!string.IsNullOrEmpty(header.vendor_po_uid) && header.vendor_uid != "1")
            {
                ddlVendorPO.SelectedValue = header.vendor_po_uid;
                pnlVendorPO.Style.Add("display", "inline");
                pnlSale.Style.Add("display", "none");

            }
        }

        //Stock Sale Panel
        if (ddlVendor.SelectedValue == "0")
            pnlStockCustomer.Style.Add("display", "none");

        //Sale Data (visible for both Stock and Vendor Inspections
        if (TheSale != null)
        {
            divCustomerPO.Style.Add("display", "inline");
            lblCustPO.Text = TheSale.orderreference;
        }


    }

    private void LoadAllImages()
    {
        LoadSectionImages("insp_head");
        if (whse != null)
            LoadSectionImages("insp_whse");
        LoadSectionImages("insp_detail");
        LoadExtendedImagery();
    }

    private void LoadTurnBacks()
    {
        using (RzDataContext rdc = new RzDataContext())
            turnbackList = rdc.qc_turn_backs.Where(w => w.insp_id == strInspID).OrderByDescending(o => o.turn_back_date.Value).ToList();
        if (turnbackList.Count > 0)
            lastTurnBack = turnbackList.OrderByDescending(o => o.turn_back_date.Value).FirstOrDefault();

        gvTurnBacks.DataSource = turnbackList;
        gvTurnBacks.DataBind();

    }

    private void SetImageControlProperties()
    {
        foreach (InspectionImageManagerControl c in GetImageManagerControlList())
        {
            c.InspectionType = "IDEA";
            c.InspectionID = intInspID;
            SM_Quality_Logic smq = new SM_Quality_Logic();
            c.SectionID = smq.GetSectionIDfromClientID(c.ClientID);
        }
    }
    private List<InspectionImageManagerControl> GetImageManagerControlList()
    {
        List<InspectionImageManagerControl> ret = new List<InspectionImageManagerControl>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, ret);
        return ret;
    }

    protected void SaveHeader(bool alert = true)
    {
        if (header == null)
            throw new Exception("Header not found");

        if (header.insp_id <= 0)
            throw new Exception("Insp_id not found");



        //else
        //    insp_id = header.insp_id;

        using (sm_portalEntities smp = new sm_portalEntities())
        {
            header = smp.insp_head.Where(w => w.insp_id == header.insp_id).FirstOrDefault();



            //Header / Part Data
            header.insp_date = dtpHeader.SelectedDate.Value;
            header.packaging = txtPackaging.Text;
            header.manufacturer = txtMFG.Text;
            header.fullpartnumber = txtPartNumber.Text;
            if (!string.IsNullOrEmpty(txtQTY.Text))
                header.quantity = Convert.ToInt32(txtQTY.Text);
            header.date_code = txtDC.Text;
            header.lot = txtLot.Text;
            header.coo = txtCOO.Text;
            header.package_type = txtPackageType.Text;
            //header.is_complete = true;
            string headerValidationStatus = ValidateHeader();
            if (headerValidationStatus == "valid")
                header.is_complete = true;
            //HeadComplete = true;
            else
                header.is_complete = false;
            //HeadComplete = false;

            SetTabComplete();

            //PO Data 
            header.vendor_uid = ddlVendor.SelectedValue;//Alwaus saving this id, using it on page load to show "Choose" or whatever
            if (ddlVendor.SelectedValue == "1")//If this is a stock sale
            {
                header.vendor_name = "Unspecified Stock Vendor";
                header.vendor_po = null;
                header.vendor_po_uid = null;
            }
            else
            {
                header.vendor_name = ddlVendor.SelectedItem.ToString();

                if (!string.IsNullOrEmpty(ddlVendorPO.SelectedValue) && ddlVendorPO.SelectedValue != "0")
                {
                    header.vendor_po = ddlVendorPO.SelectedItem.ToString();
                    header.vendor_po_uid = ddlVendorPO.SelectedValue;
                }

            }


            SaveRzLineData();//This is where hfCustomerID.Value gets set, needs to be before stuff that relies on it.

            if (TheCustomer != null)
            {
                header.customer_name = TheCustomer.companyname;
                header.customer_uid = TheCustomer.unique_id;
                string salesEmail = GetSellerEmail(TheCustomer);
                header.sales_email = salesEmail ?? "";
            }
            else
            {
                header.customer_name = null;
                header.customer_uid = null;
                header.sales_email = null;
            }
            //Sale Order Data     
            //if salesid still null, probably because no Gridview Selecte yet, check ddlDMCSO
            if (string.IsNullOrEmpty(hfSaleID.Value))
                if (ddlVendor.SelectedValue == "1")//If Stock, sale may have come from ddlSMCSO
                    if (ddlStockSale.SelectedValue != "0")//Nto set to choose, then must be valid SO
                        hfSaleID.Value = ddlStockSale.SelectedValue;
            if (!String.IsNullOrEmpty(hfSaleID.Value))
            {
                using (RzDataContext rdc = new RzDataContext())
                    TheSale = rdc.ordhed_sales.Where(w => w.unique_id == hfSaleID.Value).FirstOrDefault();
                //Nope, this can come either from the selected Line Item(s) (usually) or if stock inspection from the ddlSMCSO
                header.ordernumber_sales = TheSale.ordernumber;
                header.orderid_sales = TheSale.unique_id;
                header.customer_po = TheSale.orderreference;

            }
            else
            {
                header.ordernumber_sales = null;
                header.orderid_sales = null;
                header.customer_po = null;
            }


            //Test if vendor id is about to be overwritten.
            if (ddlVendor.SelectedIndex == 0)
            {
                tools.SendMail("inspection_test@sensiblemicro.com", "ktill@sensiblemicro.com", "Header almost overwritten:  insp_id" + intInspID, "Someone tried to save " + intInspID + " but the vendor was set to the '0' value.  Whatever caused this may be what causes the header to be overwirtten. ");
                throw new Exception("Cannot Save - Please select the Vendor on the Header.");
            }
            //Save to DB
            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                pdc.SaveChanges();
            }


            //Send initial Inspection Email Alert
            SendInspectionStartedAlert();

            //Check Complete
            CheckSectionComplete("Header");
            if (alert)
                tools.HandleResult("success", "Successfully updated header information for inspeciton id: " + intInspID);

            smp.SaveChanges();
        }


    }

    private string GetSellerEmail(company theCustomer)
    {
        n_user salesAgent = null;
        using (RzDataContext rdc = new RzDataContext())
            salesAgent = rdc.n_users.Where(w => w.unique_id == TheCustomer.base_mc_user_uid).FirstOrDefault();
        string ret = "";
        if (salesAgent != null)
        {

            ret = Tools.Strings.SanitizeInput(salesAgent.email_address);
            if (!string.IsNullOrEmpty(ret))
                if (Tools.Email.IsEmailAddress(ret))
                    return ret;
        }
        return null;
    }

    private void SendInspectionStartedAlert()
    {

        try
        {
            if (header.initial_alert_sent ?? false == true)
                return;
            List<string> ccList = new List<string>() { "smar@sensiblemicro.com" };
            if (TheAgent != null)
                ccList.Add(TheAgent.email_address.Trim().ToLower());
            string subject = "IDEA Inspection has begun for ID: " + intInspID;
            string body = "<b>Part Number: </b>" + header.fullpartnumber.Trim().ToUpper() + "</br>";
            body += "<b>Customer: </b>" + (header.customer_name ?? "<Unknown>").Trim().ToUpper() + "</br>";
            body += "<b>SO: </b>" + (header.ordernumber_sales ?? "<Unknown>").Trim().ToUpper() + "</br>";
            body += "<b>Vendor: </b>" + (header.vendor_name ?? "<Unknown>").Trim().ToUpper() + "</br>";
            body += "<b>PO: </b>" + (header.vendor_po ?? "<Unknown>").Trim().ToUpper() + "</br>";
            SystemLogic.Email.SendMail("inspection_alerts@sensiblemicro.com", TheAgent.email_address.Trim().ToLower(), subject, body, ccList.ToArray());
            using (sm_portalEntities smp = new sm_portalEntities())
            {
                header.initial_alert_sent = true;
                smp.SaveChanges();
            }

        }
        catch (Exception ex)
        {

        }



    }

    protected void lbSaveHeader_Click(object sender, EventArgs e)
    {
        try
        {

            SaveHeader();
            SaveAllImages();
            LoadHeaderValues();
            tools.HandleResult(SM_Enums.LogType.Information.ToString(), "success");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message + " Inner: " + ex.InnerException);


        }

    }

    protected void LoadHeaderValues()
    {

        //Ensure we're working with latest header saved changes
        GetHeader();

        if (intInspID > 0)
            hfinsp_id.Value = intInspID.ToString();
        else
            throw new Exception("Inspection not found.");


        LoadButtons();
        inspector = "    " + Profile.FirstName + " " + Profile.LastName;

        CurrentStatus = header.status ?? "In  Progress";
        Result = header.result;
        //HeadComplete = header.is_complete ?? false;
        //txtInspectionDate.Text = header.insp_date.Value.ToString("yyyy-MM-dd");
        dtpHeader.SelectedDate = header.insp_date.Value;

        //Vendor
        BindVendorDDL();
        ddlVendor.SelectedValue = header.vendor_uid ?? "0";

        if (header.vendor_uid != "1")
        {
            BindVendorPODDL();
            if (!string.IsNullOrEmpty(header.vendor_po_uid))
                ddlVendorPO.SelectedValue = header.vendor_po_uid;
        }
        else //Else they may or may not be a customer / sale
        {
            //Else we need to check if a customer is selected, if so, lead the Sales Order ddl
            if (!string.IsNullOrEmpty(header.customer_uid))
            {

                if (!string.IsNullOrEmpty(header.customer_name))
                    txtCustomer.Text = header.customer_name;
                Bind_ddlStockSale();
                if (!string.IsNullOrEmpty(header.orderid_sales))
                {
                    ddlStockSale.SelectedValue = header.orderid_sales;
                }

            }
            else
                txtCustomer.Text = "";
        }



        BindgvPartData();


        txtResultNotes.Text = header.resultnotes;
        txtPackaging.Text = header.packaging;
        txtMFG.Text = header.manufacturer;
        txtPartNumber.Text = header.fullpartnumber;
        txtQTY.Text = header.quantity.ToString();
        txtDC.Text = header.date_code;
        txtLot.Text = header.lot;
        txtCOO.Text = header.coo;
        txtPackageType.Text = header.package_type;
        insp_modified_date = header.date_modified.Value.ToString("yyyy-MM-dd HH':'mm':'ss");

        if (header.is_deleted == true)
            lblInspectionID.Text = "Inspection ID:" + intInspID + " ~  Last updated: " + insp_modified_date + "[DELETED]";
        else
            lblInspectionID.Text = "Inspection ID:" + intInspID + " ~  Last updated: " + insp_modified_date + " [" + header.status + "]";

        if (check_admin())
            lblInspector.Text = "Inspector: " + header.inspector + " [Admin]";
        else
            lblInspector.Text = "Inspector: " + header.inspector;
    }

    private void LoadAgent()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            if (TheSale != null)
            {
                TheAgent = rdc.n_users.Where(w => w.unique_id == TheSale.base_mc_user_uid).FirstOrDefault();
                return;
            }

            if (ThePurchase != null)
            {
                TheAgent = rdc.n_users.Where(w => w.unique_id == ThePurchase.base_mc_user_uid).FirstOrDefault();
                return;
            }
        }


    }

    private void LoadButtons()
    {
        if (check_admin())
        {
            btnDelete.Style.Add("display", "inline");
            lblInspector.Text = "Inspector: " + inspector + " [Admin]";
        }
        else
        {
            btnDelete.Style.Add("display", "none");
            lblInspector.Text = "Inspector: " + inspector;
        }

        //if Complete, allow clone
        bool clonable = header.status == "Final Inspection Complete";
        lbCloneReport.Visible = clonable;
        //if (CurrentStatus == "Final Inspection Complete")
        //    lbCloneReport.Visible = true;
        //else
        //    lbCloneReport.Visible = false;


        //Check for Deleted
        if (checkdeleted())
        {
            btnDelete.Text = "Undelete";
        }
        else
        {
            btnDelete.Text = "Delete";
        }

        btnSaveAll.Style.Add("display", "inline");
    }



    protected bool GetHeader()
    {


        if (intInspID > 0)
            hfinsp_id.Value = intInspID.ToString();
        else
            throw new Exception("Inspection not found.");



        if (string.IsNullOrEmpty(strInspID))
        {
            tools.HandleResult("fail", "No inspection ID detected.");
            return false;
        }
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            if (!pdc.insp_head.Any(h => h.insp_id == intInspID))//this insp_id is not in the database, therefore can't load
            {
                tools.HandleResult("fail", "Not a valid inspection id.");
                return false;
            }
            else
                header = pdc.insp_head.SingleOrDefault(h => h.insp_id == intInspID);
        }



        return true;
    }

    protected void Validate(Control parentControl)
    {
        foreach (Control c in parentControl.Controls)
        {
            if (c is TextBox)
            {
                if (c.ToString().Length == 0)
                {
                    tools.HandleResult("fail", c.ID + " must have a value.");
                }
            }
        }
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/secure_sm/IDEA_insp/SM_Inspection_search.aspx", false);
    }

    protected void btnSaveDetail_Click(object sender, EventArgs e)
    {

        try
        {
            SaveHeader(false);
            SaveDetail();
            SaveAllImages();
            LoadDetailValues();
            tools.HandleResult("success", "Successfully saved detail information for inspeciton id: " + intInspID);

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    protected void SaveDetail(bool alert = true)
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            //Check for existing detail
            //insp_detail d = null;
            if (detail == null)
            {

                detail = pdc.insp_detail.SingleOrDefault(c => c.insp_id == intInspID);
                if (detail == null)//if true (i.e. insp_id DOESN'T exist) insert, else update
                {
                    detail = new insp_detail();
                    detail.date_created = DateTime.Now;
                    detail.insp_id = intInspID;
                    pdc.insp_detail.Add(detail);
                }
            }
            else
                detail = pdc.insp_detail.Where(w => w.insp_id == detail.insp_id).FirstOrDefault();





            detail.date_modified = DateTime.Now;

            //Inner Contents
            detail.inner_cont1_result = ddlInnerContents1.SelectedValue;
            detail.inner_cont1_text = txtInnerContents1.Text;
            detail.inner_cont2_result = ddlInnerContents2.SelectedValue;
            detail.inner_cont2_text = txtInnerContents2.Text;
            detail.inner_cont3_result = ddlInnerContents3.SelectedValue;
            detail.inner_cont3_text = txtInnerContents3.Text;
            detail.inner_cont4_result = ddlInnerContents4.SelectedValue;
            detail.inner_cont4_text = txtInnerContents4.Text;
            detail.inner_cont5_result = ddlInnerContents5.SelectedValue;
            detail.inner_cont5_text = txtInnerContents5.Text;
            detail.inner_cont6_result = ddlInnerContents6.SelectedValue;
            detail.inner_cont6_text = txtInnerContents6.Text;
            detail.inner_cont7_result = ddlInnerContents7.SelectedValue;
            detail.inner_cont7_text = txtInnerContents7.Text;
            //Set package type adn pertinent details
            switch (ddlCarrierType.SelectedValue)
            {
                case "1":
                    detail.is_tube = true;
                    detail.is_reel = false;
                    detail.is_tray = false;
                    detail.is_bulk = false;
                    detail.tube1_result = ddlTube1.SelectedValue;
                    detail.tube1_text = txtTube1.Text;
                    detail.tube2_result = ddlTube2.SelectedValue;
                    detail.tube2_text = txtTube2.Text;
                    detail.tube3_result = ddlTube3.SelectedValue;
                    detail.tube3_text = txtTube3.Text;
                    detail.tube4_result = ddlTube4.SelectedValue;
                    detail.tube4_text = txtTube4.Text;
                    detail.tube5_result = ddlTube5.SelectedValue;
                    detail.tube5_text = txtTube5.Text;
                    break;
                case "2":
                    detail.is_reel = true;
                    detail.is_tube = false;
                    detail.is_tray = false;
                    detail.is_bulk = false;
                    detail.reel1_result = ddlReel1.SelectedValue;
                    detail.reel1_text = txtReel1.Text;
                    detail.reel2_result = ddlReel2.SelectedValue;
                    detail.reel2_text = txtReel2.Text;
                    detail.reel3_result = ddlReel3.SelectedValue;
                    detail.reel3_text = txtReel3.Text;
                    detail.reel4_result = ddlReel4.SelectedValue;
                    detail.reel4_text = txtReel4.Text;
                    detail.reel5_result = ddlReel5.SelectedValue;
                    detail.reel5_text = txtReel5.Text;
                    detail.reel6_result = ddlReel6.SelectedValue;
                    detail.reel6_text = txtReel6.Text;
                    detail.reel7_result = ddlReel7.SelectedValue;
                    detail.reel7_text = txtReel7.Text;
                    break;
                case "3":
                    detail.is_tray = true;
                    detail.is_tube = false;
                    detail.is_reel = false;
                    detail.is_bulk = false;
                    detail.tray1_result = ddlTray1.SelectedValue;
                    detail.tray1_text = txtTray1.Text;
                    detail.tray2_result = ddlTray2.SelectedValue;
                    detail.tray2_text = txtTray2.Text;
                    detail.tray3_result = ddlTray3.SelectedValue;
                    detail.tray3_text = txtTray3.Text;
                    detail.tray4_result = ddlTray4.SelectedValue;
                    detail.tray4_text = txtTray4.Text;
                    detail.tray5_result = ddlTray5.SelectedValue;
                    detail.tray5_text = txtTray5.Text;
                    detail.tray6_result = ddlTray6.SelectedValue;
                    detail.tray6_text = txtTray6.Text;
                    break;
                case "4":
                    detail.is_bulk = true;
                    detail.is_tube = false;
                    detail.is_reel = false;
                    detail.is_tray = false;
                    detail.bulk1_result = ddlBulk1.SelectedValue;
                    detail.bulk1_text = txtBulk1.Text;
                    detail.bulk2_result = ddlBulk2.SelectedValue;
                    detail.bulk2_text = txtBulk2.Text;
                    detail.bulk3_result = ddlBulk3.SelectedValue;
                    detail.bulk3_text = txtBulk3.Text;
                    detail.bulk4_result = ddlBulk4.SelectedValue;
                    detail.bulk4_text = txtBulk4.Text;
                    break;

            }

            //Initial Inspection
            detail.initial1_result = ddlInitInspection1.SelectedValue;
            detail.initial1_text = txtInitInspection1.Text;
            detail.initial2_result = ddlInitInspection2.SelectedValue;
            detail.initial2_text = txtInitInspection2.Text;
            //d.initial3_result = ddlInitInspection3.SelectedValue;
            //d.initial3_text = txtInitInspection3.Text;
            detail.initial4_result = ddlInitInspection4.SelectedValue;
            detail.initial4_text = txtInitInspection4.Text;
            detail.initial5_result = ddlInitInspection5.SelectedValue;
            detail.initial5_text = txtInitInspection5.Text;
            detail.initial6_result = ddlInitInspection6.SelectedValue;
            detail.initial6_text = txtInitInspection6.Text;
            detail.initial7_result = ddlInitInspection7.SelectedValue;
            detail.initial7_text = txtInitInspection7.Text;

            //Detailed Visual Inspection
            detail.detail1_result = ddlDetailVisual1.SelectedValue;
            detail.detail1_text = txtDetailVisual1.Text;
            detail.detail2_result = ddlDetailVisual2.SelectedValue;
            detail.detail2_text = txtDetailVisual2.Text;
            detail.detail3_result = ddlDetailVisual3.SelectedValue;
            detail.detail3_text = txtDetailVisual3.Text;
            detail.detail4_result = ddlDetailVisual4.SelectedValue;
            detail.detail4_text = txtDetailVisual4.Text;
            detail.detail5_result = ddlDetailVisual5.SelectedValue;
            detail.detail5_text = txtDetailVisual5.Text;
            detail.detail6_result = ddlDetailVisual6.SelectedValue;
            detail.detail6_text = txtDetailVisual6.Text;
            detail.detail7_result = ddlDetailVisual7.SelectedValue;
            detail.detail7_text = txtDetailVisual7.Text;
            detail.detail8_result = ddlDetailVisual8.SelectedValue;
            detail.detail8_text = txtDetailVisual8.Text;
            detail.detail9_result = ddlDetailVisual9.SelectedValue;
            detail.detail9_text = txtDetailVisual9.Text;
            detail.detail10_result = ddlDetailVisual10.SelectedValue;
            detail.detail10_text = txtDetailVisual10.Text;
            detail.detail11_result = ddlDetailVisual11.SelectedValue;
            detail.detail11_text = txtDetailVisual11.Text;
            detail.detail12_result = ddlDetailVisual12.SelectedValue;
            detail.detail12_text = txtDetailVisual12.Text;
            detail.detail13_result = ddlDetailVisual13.SelectedValue;
            detail.detail13_text = txtDetailVisual13.Text;
            detail.detail14_result = ddlDetailVisual14.SelectedValue;
            detail.detail14_text = txtDetailVisual14.Text;
            detail.detail15_result = ddlDetailVisual15.SelectedValue;
            detail.detail15_text = txtDetailVisual15.Text;
            detail.detail16_result = ddlDetailVisual16.SelectedValue;
            detail.detail16_text = txtDetailVisual16.Text;
            detail.detail17_result = ddlDetailVisual17.SelectedValue;
            detail.detail17_text = txtDetailVisual17.Text;

            //Detailed Solvents
            if (ddlis_solvent.SelectedValue == "1")
            {
                detail.is_solvent = true;
            }
            else
            {
                detail.is_solvent = false;
            }
            detail.solvent1_result = ddlDetailSolvents1.SelectedValue;
            detail.solvent1_text = txtDetailSolvents1.Text;
            detail.solvent2_result = ddlDetailSolvents2.SelectedValue;
            detail.solvent2_text = txtDetailSolvents2.Text;
            detail.solvent3_result = ddlDetailSolvents3.SelectedValue;
            detail.solvent3_text = txtDetailSolvents3.Text;

            //Detailed Mechanical
            if (ddlis_mech.SelectedValue == "1")
            {
                detail.is_mech = true;
            }
            else
            {
                detail.is_mech = false;
            }
            detail.mech1_result = ddlDetailMech1.SelectedValue;
            detail.mech1_text = txtDetailMech1.Text;
            detail.mech2_result = ddlDetailMech2.SelectedValue;
            detail.mech2_text = txtDetailMech2.Text;
            detail.mech3_result = ddlDetailMech3.SelectedValue;
            detail.mech3_text = txtDetailMech3.Text;

            //Additional Testing
            detail.is_addtesting = ddlisAddTesting.SelectedValue == "1";
            detail.addtesting_text = txtAddTesting.Text;


            //if (CheckSectionComplete("Detail"))
            //    detail.is_complete = true;
            //else
            //    detail.is_complete = false;
            string validateDetailStatus = ValidateDetail().ToLower();
            if (validateDetailStatus == "valid")
            {
                detail.is_complete = true;
                //DetailComplete = true;
            }

            else
            {
                detail.is_complete = false;
                // DetailComplete = false;
            }
            SetTabComplete();


            pdc.SaveChanges();
        }
        if (alert)
            tools.HandleResult("success", "Successfully updated details for inspeciton id: " + intInspID);
        //}
        //catch (Exception ex)
        //{
        //    tools.HandleResult(Page, "fail", ex.Message);
        //}
    }

    //Load Detail Values
    protected void LoadDetailValues()
    {
        //if (detail.is_complete == true)
        //    DetailComplete = true;

        ddlInnerContents1.SelectedValue = detail.inner_cont1_result;
        txtInnerContents1.Text = detail.inner_cont1_text;
        ddlInnerContents2.SelectedValue = detail.inner_cont2_result;
        txtInnerContents2.Text = detail.inner_cont2_text;
        ddlInnerContents3.SelectedValue = detail.inner_cont3_result;
        txtInnerContents3.Text = detail.inner_cont3_text;
        ddlInnerContents4.SelectedValue = detail.inner_cont4_result;
        txtInnerContents4.Text = detail.inner_cont4_text;
        ddlInnerContents5.SelectedValue = detail.inner_cont5_result;
        txtInnerContents5.Text = detail.inner_cont5_text;
        ddlInnerContents6.SelectedValue = detail.inner_cont6_result;
        txtInnerContents6.Text = detail.inner_cont6_text;
        ddlInnerContents7.SelectedValue = detail.inner_cont7_result;
        txtInnerContents7.Text = detail.inner_cont7_text;

        if (detail.is_tube == true)
        {
            ddlCarrierType.SelectedValue = "1";
            ddlTube1.SelectedValue = detail.tube1_result;
            txtTube1.Text = detail.tube1_text;
            ddlTube2.SelectedValue = detail.tube2_result;
            txtTube2.Text = detail.tube2_text;
            ddlTube3.SelectedValue = detail.tube3_result;
            txtTube3.Text = detail.tube3_text;
            ddlTube4.SelectedValue = detail.tube4_result;
            txtTube4.Text = detail.tube4_text;
            ddlTube5.SelectedValue = detail.tube5_result;
            txtTube5.Text = detail.tube5_text;
        }

        if (detail.is_reel == true)
        {
            ddlCarrierType.SelectedValue = "2";
            ddlReel1.SelectedValue = detail.reel1_result;
            txtReel1.Text = detail.reel1_text;
            ddlReel2.SelectedValue = detail.reel2_result;
            txtReel2.Text = detail.reel2_text;
            ddlReel3.SelectedValue = detail.reel3_result;
            txtReel3.Text = detail.reel3_text;
            ddlReel4.SelectedValue = detail.reel4_result;
            txtReel4.Text = detail.reel4_text;
            ddlReel5.SelectedValue = detail.reel5_result;
            txtReel5.Text = detail.reel5_text;
            ddlReel6.SelectedValue = detail.reel6_result;
            txtReel6.Text = detail.reel6_text;
            ddlReel7.SelectedValue = detail.reel7_result;
            txtReel7.Text = detail.reel7_text;

        }

        if (detail.is_tray == true)
        {
            ddlCarrierType.SelectedValue = "3";
            ddlTray1.SelectedValue = detail.tray1_result;
            txtTray1.Text = detail.tray1_text;
            ddlTray2.SelectedValue = detail.tray2_result;
            txtTray2.Text = detail.tray2_text;
            ddlTray3.SelectedValue = detail.tray3_result;
            txtTray3.Text = detail.tray3_text;
            ddlTray4.SelectedValue = detail.tray4_result;
            txtTray4.Text = detail.tray4_text;
            ddlTray5.SelectedValue = detail.tray5_result;
            txtTray5.Text = detail.tray5_text;
            ddlTray6.SelectedValue = detail.tray6_result;
            txtTray6.Text = detail.tray6_text;
        }


        if (detail.is_bulk == true)
        {
            ddlCarrierType.SelectedValue = "4";
            ddlBulk1.SelectedValue = detail.bulk1_result;
            txtBulk1.Text = detail.bulk1_text;
            ddlBulk2.SelectedValue = detail.bulk2_result;
            txtBulk2.Text = detail.bulk2_text;
            ddlBulk3.SelectedValue = detail.bulk3_result;
            txtBulk3.Text = detail.bulk3_text;
            ddlBulk4.SelectedValue = detail.bulk4_result;
            txtBulk4.Text = detail.bulk4_text;
        }


        //Initial Inspection (IDEA 10.2.1)
        ddlInitInspection1.SelectedValue = detail.initial1_result;
        txtInitInspection1.Text = detail.initial1_text;
        ddlInitInspection2.SelectedValue = detail.initial2_result;
        txtInitInspection2.Text = detail.initial2_text;
        //ddlInitInspection3.SelectedValue = detail.initial3_result; - Question removed per Sean
        //txtInitInspection3.Text = detail.initial3_text;
        ddlInitInspection4.SelectedValue = detail.initial4_result;
        txtInitInspection4.Text = detail.initial4_text;
        ddlInitInspection5.SelectedValue = detail.initial5_result;
        txtInitInspection5.Text = detail.initial5_text;
        ddlInitInspection6.SelectedValue = detail.initial6_result;
        txtInitInspection6.Text = detail.initial6_text;
        ddlInitInspection7.SelectedValue = detail.initial7_result;
        txtInitInspection7.Text = detail.initial7_text;

        //Detailed Inspection (Visual)  (IDEA 10.3.1)
        ddlDetailVisual1.SelectedValue = detail.detail1_result;
        txtDetailVisual1.Text = detail.detail1_text;
        ddlDetailVisual2.SelectedValue = detail.detail2_result;
        txtDetailVisual2.Text = detail.detail2_text;
        ddlDetailVisual3.SelectedValue = detail.detail3_result;
        txtDetailVisual3.Text = detail.detail3_text;
        ddlDetailVisual4.SelectedValue = detail.detail4_result;
        txtDetailVisual4.Text = detail.detail4_text;
        ddlDetailVisual5.SelectedValue = detail.detail5_result;
        txtDetailVisual5.Text = detail.detail5_text;
        ddlDetailVisual6.SelectedValue = detail.detail6_result;
        txtDetailVisual6.Text = detail.detail6_text;
        ddlDetailVisual7.SelectedValue = detail.detail7_result;
        txtDetailVisual7.Text = detail.detail7_text;
        ddlDetailVisual8.SelectedValue = detail.detail8_result;
        txtDetailVisual8.Text = detail.detail8_text;
        ddlDetailVisual9.SelectedValue = detail.detail9_result;
        txtDetailVisual9.Text = detail.detail9_text;
        ddlDetailVisual10.SelectedValue = detail.detail10_result;
        txtDetailVisual10.Text = detail.detail10_text;
        ddlDetailVisual11.SelectedValue = detail.detail11_result;
        txtDetailVisual11.Text = detail.detail11_text;
        ddlDetailVisual12.SelectedValue = detail.detail12_result;
        txtDetailVisual12.Text = detail.detail12_text;
        ddlDetailVisual13.SelectedValue = detail.detail13_result;
        txtDetailVisual13.Text = detail.detail13_text;
        ddlDetailVisual14.SelectedValue = detail.detail14_result;
        txtDetailVisual14.Text = detail.detail14_text;
        ddlDetailVisual15.SelectedValue = detail.detail15_result;
        txtDetailVisual15.Text = detail.detail15_text;
        ddlDetailVisual16.SelectedValue = detail.detail16_result;
        txtDetailVisual16.Text = detail.detail16_text;
        ddlDetailVisual17.SelectedValue = detail.detail17_result;
        txtDetailVisual17.Text = detail.detail17_text;

        //Detailed Inspection (Solvents)  (IDEA 10.3.2)
        if (detail.is_solvent == true)
        {
            ddlis_solvent.SelectedValue = "1";
            pnlDetailSolvents.Style.Add("display", "inline");
        }
        else
        {
            ddlis_solvent.SelectedValue = "2";
            pnlDetailSolvents.Style.Add("display", "none");
        }
        ddlDetailSolvents1.SelectedValue = detail.solvent1_result;
        txtDetailSolvents1.Text = detail.solvent1_text;
        ddlDetailSolvents2.SelectedValue = detail.solvent2_result;
        txtDetailSolvents2.Text = detail.solvent2_text;
        ddlDetailSolvents3.SelectedValue = detail.solvent3_result;
        txtDetailSolvents3.Text = detail.solvent3_text;

        //Detailed Inspection (Mechanical)  (IDEA 10.3.3)
        if (detail.is_mech == true)
        {
            ddlis_mech.SelectedValue = "1";
            pnlMech.Style.Add("display", "inline");
        }
        else
        {
            ddlis_mech.SelectedValue = "2";
            pnlMech.Style.Add("display", "none");
        }
        ddlDetailMech1.SelectedValue = detail.mech1_result;
        txtDetailMech1.Text = detail.mech1_text;
        ddlDetailMech2.SelectedValue = detail.mech2_result;
        txtDetailMech2.Text = detail.mech2_text;
        ddlDetailMech3.SelectedValue = detail.mech3_result;
        txtDetailMech3.Text = detail.mech3_text;

        if (detail.is_addtesting == true)
        {
            ddlisAddTesting.SelectedValue = "1";
            pnlisAddTesting.Style.Add("display", "inline");
        }
        else
        {
            ddlisAddTesting.SelectedValue = "2";
            pnlisAddTesting.Style.Add("display", "none");
        }
        txtAddTesting.Text = detail.addtesting_text;

        set_carrierType(ddlCarrierType.SelectedValue);
    }


    //Load Traveler Values
    private void LoadTravelerValues()
    {

        //dtpMoistureStart.DateOnly = false;
        //dtpMoistureEnd.DateOnly = false;
        using (sm_portalEntities pdc = new sm_portalEntities())
            traveler = pdc.insp_traveler.Where(w => w.insp_id == intInspID).SingleOrDefault();
        if (traveler == null)
            return;
        if (traveler.has_moisture)
        {
            cbxTravelerMoisture.isChecked = true;
            txtTravelerReqTemp.Text = traveler.moisture_req_temp;
            txtTravelerReqCycle.Text = traveler.moisture_req_cycle;
            if (traveler.moisture_start_time != null)
                dtpTravelerMoistureStart.SelectedDate = traveler.moisture_start_time.Value;
            if (traveler.moisture_end_time != null)
                dtpTravelerMoistureEnd.SelectedDate = traveler.moisture_end_time.Value;
        }

        if (traveler.has_xray)
        {

            cbxTravelerXray.isChecked = true;
            txtTravelerXrayQty.Text = traveler.xray_qty.Value.ToString();
            txtTravelerXrayResult.Text = traveler.xray_comments;
            ddlTravelerXrayResult.SelectedValue = traveler.xray_result;
        }
        if (traveler.has_heated)
        {
            cbxTravelerHeated.isChecked = true;
            txtTravelerHeatedQty.Text = traveler.heated_qty.Value.ToString();
            txtTravelerHeatedResult.Text = traveler.heated_comments;
            ddlTravelerHeatedResult.SelectedValue = traveler.heated_result;
        }

        if (traveler.has_jetetch)
        {
            cbxTravelerJetEtch.isChecked = true;
            txtTravelerJetEtchQty.Text = traveler.jetetch_qty.Value.ToString();
            txtTravelerJetEtchResult.Text = traveler.jetetch_comments;
            ddlTravelerJetEtchResult.SelectedValue = traveler.jetetch_result;
        }

        if (traveler.has_basicelec)
        {
            cbxTravelerBasicElec.isChecked = true;
            txtTravelerBasicElecQty.Text = traveler.baselec_qty.Value.ToString();
            txtTravelerBasicElecResult.Text = traveler.baselec_comments;
            ddlTravelerBasicElecResult.SelectedValue = traveler.baselec_result;

        }
        if (traveler.has_sentry)
        {
            cbxTravelerSentry.isChecked = true;
            txtTravelerSentryQty.Text = traveler.sentry_qty.Value.ToString();
            txtTravelerSentryResult.Text = traveler.sentry_comments;
            ddlTravelerSentryResult.Text = traveler.sentry_result;
        }

        if (traveler.has_solder)
        {
            cbxTravelerSolder.isChecked = true;
            txtTravelerSolderQty.Text = traveler.solder_qty.Value.ToString();
            txtTravelerSolderResult.Text = traveler.solder_comments;
            ddlTravelerSolderResult.SelectedValue = traveler.solder_result;
        }

        if (traveler.has_xeltek)
        {
            cbxTravelerXeltek.isChecked = true;
            txtTravelerXelTekQty.Text = traveler.xeltek_qty.Value.ToString();
            txtTravelerXelTekResult.Text = traveler.xeltek_comments;
            ddlTravelerXelTekResult.SelectedValue = traveler.xeltek_result;
        }

        if (traveler.has_xrf)
        {
            cbxTravelerXrf.isChecked = true;
            txtTravelerXrfQty.Text = traveler.xrf_qty.Value.ToString();
            txtTravelerXrfResult.Text = traveler.xrf_comments;
            ddlTravelerXrfResult.SelectedValue = traveler.xrf_result;

        }




    }


    private void LoadSectionImages(string sectionType)
    {
        SM_Quality_Logic smq = new SM_Quality_Logic();
        switch (sectionType)
        {
            case "insp_head":
                {
                    {
                        if (header.source_insp_id > 0)
                            images = smb.insp_images.Where(w => w.insp_id == header.source_insp_id && w.insp_type == "IDEA" && w.insp_section_id.ToLower().Contains("header")).ToList();
                        else
                            images = smb.insp_images.Where(w => w.insp_id == intInspID && w.insp_type == "IDEA" && w.insp_section_id.ToLower().Contains("header")).ToList();
                        foreach (insp_images i in images)
                        {
                            LoadImageControl(i);
                        }
                        break;
                    }

                }
            case "insp_whse":
                {
                    {
                        List<string> warehouseControlIDs = GetImageManagerControlList().Where(w => warehouseSectionList.Contains(smq.GetSectionIDfromClientID(w.ClientID, true))).Select(s => smq.GetSectionIDfromClientID(s.ClientID)).ToList();
                        //GetList of all ImageManager Controls ID;s i.e. imGenCarton1 - replace("im", "")
                        warehouseControlIDs = warehouseControlIDs.Union(smq.GetLegacyWhseImageSectionIDs()).ToList();
                        if (header.source_insp_id > 0)
                            images = smb.insp_images.Where(w => w.insp_id == header.source_insp_id && w.insp_type == "IDEA" && (warehouseControlIDs.Contains(w.insp_section_id))).ToList();
                        else
                            images = smb.insp_images.Where(w => w.insp_id == intInspID && w.insp_type == "IDEA" && (warehouseControlIDs.Contains(w.insp_section_id))).ToList();
                        foreach (insp_images i in images)
                        {
                            LoadImageControl(i);
                        }
                        break;

                    }

                }
            case "insp_detail":
                {
                    {
                        List<string> detailControlIDs = GetImageManagerControlList().Where(w => detailSectionList.Contains(smq.GetSectionIDfromClientID(w.ClientID, true))).Select(s => smq.GetSectionIDfromClientID(s.ClientID)).ToList();
                        detailControlIDs = detailControlIDs.Union(smq.GetLegacyDetailImageSectionIDs()).ToList();
                        //detailControlIDs.Add("MechImage");//Adding for legacy Compatibility - see ID # 12699
                        //detailControlIDs.Add("DetailVisualImage");// see ID#6070
                        if (header.source_insp_id > 0)
                            images = smb.insp_images.Where(w => w.insp_id == header.source_insp_id && w.insp_type == "IDEA" && detailControlIDs.Contains(w.insp_section_id)).ToList();
                        else
                            images = smb.insp_images.Where(w => w.insp_id == intInspID && w.insp_type == "IDEA" && detailControlIDs.Contains(w.insp_section_id)).ToList();
                        foreach (insp_images i in images)
                        {
                            LoadImageControl(i);
                        }
                        break;
                    }

                }
        }


    }
    private void SaveAllImages()
    {
        List<FileUpload> allFileUploads = new List<FileUpload>();
        tools.GetControlList(Master.FindControl("MainContent").Controls, allFileUploads);
        string sectionID = "";
        //images = smb.insp_images.Where(w => w.insp_id == insp_id && w.insp_type == "IDEA" && w.insp_section_id.ToLower().Contains("header")).ToList();
        UploadedImagesCount = 0;
        foreach (FileUpload f in allFileUploads)
        {
            sectionID = f.ClientID.Replace("MainContent_", "");
            sectionID = sectionID.Replace("_fu", "");
            if (f.HasFile)
            {

                InspectionImageManagerControl c = (InspectionImageManagerControl)Page.Master.FindControl("MainContent").FindControl(sectionID);
                if (c == null)
                    throw new Exception("Cannot save image for " + sectionID + ": Inspection Image Manager Control not found.");
                c.SaveImage(intInspID, "IDEA");
                UploadedImagesCount++;
            }

        }
        List<InspectionImageManagerControl> listImControlList = GetImageManagerControlList();
        foreach (InspectionImageManagerControl c in listImControlList)
        {
            //if (c.txtChanged)
            c.SaveDescription(intInspID, "IDEA");
        }

        LoadAllImages();
    }





    protected void set_carrierType(string selection)
    {
        switch (selection)
        {
            case "1"://Tube
                {
                    pnlTube.Style.Add("display", "inline");
                    pnlReel.Style.Add("display", "none");
                    pnlTray.Style.Add("display", "none");
                    pnlBulk.Style.Add("display", "none");
                    break;
                }
            case "2"://Reel
                {

                    pnlTube.Style.Add("display", "none");
                    pnlReel.Style.Add("display", "inline");
                    pnlTray.Style.Add("display", "none");
                    pnlBulk.Style.Add("display", "none");
                    break;
                }
            case "3"://Tray
                {
                    pnlTube.Style.Add("display", "none");
                    pnlReel.Style.Add("display", "none");
                    pnlTray.Style.Add("display", "inline");
                    pnlBulk.Style.Add("display", "none");
                    break;
                }
            case "4"://Bulk
                {
                    pnlTube.Style.Add("display", "none");
                    pnlReel.Style.Add("display", "none");
                    pnlTray.Style.Add("display", "none");
                    pnlBulk.Style.Add("display", "inline");
                    break;
                }
            default:
                {
                    pnlTube.Style.Add("display", "none");
                    pnlReel.Style.Add("display", "none");
                    pnlTray.Style.Add("display", "none");
                    pnlBulk.Style.Add("display", "none");
                    break;
                }
        }
    }




    //Load Warehouse Values
    protected void LoadWarehouseValues()
    {

        //WhseComplete = whse.is_complete ?? false;

        //Load General Carton
        ddlGenCarton1.SelectedValue = whse.gencarton1_result;
        txtGenCarton1.Text = whse.gencarton1_text;
        ddlGenCarton2.SelectedValue = whse.gencarton2_result;
        txtGenCarton2.Text = whse.gencarton2_text;
        ddlGenCarton3.SelectedValue = whse.gencarton3_result;
        txtGenCarton3.Text = whse.gencarton3_text;
        ddlGenCarton4.SelectedValue = whse.gencarton4_result;
        txtGenCarton4.Text = whse.gencarton4_text;
        ddlGenCarton5.SelectedValue = whse.gencarton5_result;
        txtGenCarton5.Text = whse.gencarton5_text;

        //Load Outer Carton
        ddlOuterCarton1.SelectedValue = whse.outcarton1_result;
        txtOuterCarton1.Text = whse.outcarton1_text;
        ddlOuterCarton2.SelectedValue = whse.outcarton2_result;
        txtOuterCarton2.Text = whse.outcarton2_text;
        ddlOuterCarton3.SelectedValue = whse.outcarton3_result;
        txtOuterCarton3.Text = whse.outcarton3_text;
        ddlOuterCarton4.SelectedValue = whse.outcarton4_result;
        txtOuterCarton4.Text = whse.outcarton4_text;
        ddlOuterCarton5.SelectedValue = whse.outcarton5_result;
        txtOuterCarton5.Text = whse.outcarton5_text;
        ddlOuterCarton6.SelectedValue = whse.outcarton6_result;
        txtOuterCarton6.Text = whse.outcarton6_text;
        ddlOuterCarton7.SelectedValue = whse.outcarton7_result;
        txtOuterCarton7.Text = whse.outcarton7_text;

    }

    protected void btnSaveWhse_Click(object sender, EventArgs e)
    {
        try
        {
            SaveHeader(false);
            SaveWhse();
            SaveAllImages();

            LoadWarehouseValues();
            tools.HandleResult("success", "Successfully saved warehouse information for inspeciton id: " + intInspID);

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }


    }


    protected void SaveWhse(bool alert = true)
    {


        using (sm_portalEntities pdc = new sm_portalEntities())
        {


            insp_whse w = null;
            if (!pdc.insp_whse.Any(i => i.insp_id == intInspID))//if true (i.e. insp_id DOESN'T exist) insert, else update          
            {
                w = new insp_whse();
                w.date_created = DateTime.Now;
                pdc.insp_whse.Add(w);
            }

            else
                w = pdc.insp_whse.SingleOrDefault(i => i.insp_id == intInspID);

            w.insp_id = intInspID;
            //General Carton Inspection (IDEA 10.1.5.1)
            w.date_modified = DateTime.Now;
            w.gencarton1_result = ddlGenCarton1.SelectedValue;
            w.gencarton1_text = txtGenCarton1.Text;
            w.gencarton2_result = ddlGenCarton2.SelectedValue;
            w.gencarton2_text = txtGenCarton2.Text;
            w.gencarton3_result = ddlGenCarton3.SelectedValue;
            w.gencarton3_text = txtGenCarton3.Text;
            w.gencarton4_result = ddlGenCarton4.SelectedValue;
            w.gencarton4_text = txtGenCarton4.Text;
            w.gencarton5_result = ddlGenCarton5.SelectedValue;
            w.gencarton5_text = txtGenCarton5.Text;

            //Outer Product Carton Inspection (IDEA 10.1.5.2)
            w.outcarton1_result = ddlOuterCarton1.SelectedValue;
            w.outcarton1_text = txtOuterCarton1.Text;
            w.outcarton2_result = ddlOuterCarton2.SelectedValue;
            w.outcarton2_text = txtOuterCarton2.Text;
            w.outcarton3_result = ddlOuterCarton3.SelectedValue;
            w.outcarton3_text = txtOuterCarton3.Text;
            w.outcarton4_result = ddlOuterCarton4.SelectedValue;
            w.outcarton4_text = txtOuterCarton4.Text;
            w.outcarton5_result = ddlOuterCarton5.SelectedValue;
            w.outcarton5_text = txtOuterCarton5.Text;
            w.outcarton6_result = ddlOuterCarton6.SelectedValue;
            w.outcarton6_text = txtOuterCarton6.Text;
            w.outcarton7_result = ddlOuterCarton7.SelectedValue;
            w.outcarton7_text = txtOuterCarton7.Text;
            //if (CheckSectionComplete("Warehouse"))
            //    w.is_complete = true;
            //else
            //    w.is_complete = false;
            string whseValidationStatus = ValidateWhse();
            if (whseValidationStatus == "valid")
            {
                w.is_complete = true;
                //WhseComplete = true;
            }
            else
            {
                w.is_complete = false;
                //WhseComplete = false;
            }
            SetTabComplete();


            pdc.SaveChanges();
        }
        if (alert)
            tools.HandleResult("success", "Successfully saved warehouse details for inspeciton id: " + intInspID);

    }
    protected void btnSaveAll_Click(object sender, EventArgs e)
    {

        try
        {
            SaveAll();
            LoadAllValues();
            tools.HandleResult("success", "Successfully saved all sections for inspeciton id: " + intInspID);

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void SaveAll(bool alert = true)
    {

        SaveHeader(false);
        //if (!isStockInspection())
        if (ddlVendor.SelectedValue != "1")//Stock Sales don't receive anythign therefore no Warehouse needed
            SaveWhse(false);
        SaveDetail(false);
        SaveAllImages();
        SaveTraveler();
        //LoadAll(header);       
        if (alert)
            tools.HandleResult("success", "Successfully saved all sections for inspeciton id: " + intInspID);

    }

    private void SaveTraveler()
    {
        //insp_traveler trav = null;
        using (sm_portalEntities pdc = new sm_portalEntities())
        {


            traveler = pdc.insp_traveler.Where(w => w.insp_id == intInspID).FirstOrDefault();
            if (traveler == null)
            {
                traveler = new insp_traveler();
                traveler.insp_id = intInspID;
                traveler.date_created = DateTime.Now;
                traveler.date_modified = DateTime.Now;
                pdc.insp_traveler.Add(traveler);

            }

            traveler.date_modified = DateTime.Now;
            traveler.has_basicelec = cbxTravelerBasicElec.isChecked;
            traveler.has_heated = cbxTravelerHeated.isChecked;
            traveler.has_jetetch = cbxTravelerJetEtch.isChecked;
            traveler.has_moisture = cbxTravelerMoisture.isChecked;
            traveler.has_sentry = cbxTravelerSentry.isChecked;
            traveler.has_solder = cbxTravelerSolder.isChecked;
            traveler.has_xeltek = cbxTravelerXeltek.isChecked;
            traveler.has_xray = cbxTravelerXray.isChecked;
            traveler.has_xrf = cbxTravelerXrf.isChecked;




            //Moisture
            if (cbxTravelerMoisture.isChecked)
            {
                traveler.has_moisture = true;
                traveler.moisture_req_temp = txtTravelerReqTemp.Text;
                traveler.moisture_req_cycle = txtTravelerReqCycle.Text;
                traveler.moisture_start_time = dtpTravelerMoistureStart.SelectedDate.Value;
                traveler.moisture_end_time = dtpTravelerMoistureEnd.SelectedDate.Value;
            }

            //Xray
            if (cbxTravelerXray.isChecked)
            {
                traveler.has_xray = true;
                traveler.xray_qty = Convert.ToInt32(txtTravelerXrayQty.Text);
                traveler.xray_comments = txtTravelerXrayResult.Text;
                traveler.xray_result = ddlTravelerXrayResult.SelectedValue;
            }
            else
            {
                traveler.has_xray = false;
                traveler.xray_qty = null;
                traveler.xray_comments = null;
                traveler.xray_result = null;
            }

            //Heated Solvents
            if (cbxTravelerHeated.isChecked)
            {
                traveler.has_heated = true;
                traveler.heated_qty = Convert.ToInt32(txtTravelerHeatedQty.Text);
                traveler.heated_comments = txtTravelerHeatedResult.Text;
                traveler.heated_result = ddlTravelerHeatedResult.SelectedValue;
            }
            else
            {
                traveler.has_heated = false;
                traveler.heated_qty = null;
                traveler.heated_comments = null;
                traveler.heated_result = null;
            }


            //JetEtch
            if (cbxTravelerJetEtch.isChecked)
            {
                traveler.has_jetetch = true;
                traveler.jetetch_qty = Convert.ToInt32(txtTravelerJetEtchQty.Text);
                traveler.jetetch_comments = txtTravelerJetEtchResult.Text;
                traveler.jetetch_result = ddlTravelerJetEtchResult.SelectedValue;
            }
            else
            {
                traveler.has_jetetch = false;
                traveler.jetetch_qty = null;
                traveler.jetetch_comments = null;
                traveler.jetetch_result = null;
            }

            //Basic Electrical
            if (cbxTravelerBasicElec.isChecked)
            {
                traveler.has_basicelec = true;
                traveler.baselec_qty = Convert.ToInt32(txtTravelerBasicElecQty.Text);
                traveler.baselec_comments = txtTravelerBasicElecResult.Text;
                traveler.baselec_result = ddlTravelerBasicElecResult.SelectedValue;

            }
            else
            {
                traveler.has_basicelec = false;
                traveler.baselec_qty = null;
                traveler.baselec_comments = null;
                traveler.baselec_result = null;
            }
            //Sentry
            if (cbxTravelerSentry.isChecked)
            {
                traveler.has_sentry = true;
                traveler.sentry_qty = Convert.ToInt32(txtTravelerSentryQty.Text);
                traveler.sentry_comments = txtTravelerSentryResult.Text;
                traveler.sentry_result = ddlTravelerSentryResult.Text;
            }
            else
            {
                traveler.has_sentry = false;
                traveler.sentry_qty = null;
                traveler.sentry_comments = null;
                traveler.sentry_result = null;

            }

            //Solderability
            if (cbxTravelerSolder.isChecked)
            {
                traveler.has_solder = true;
                traveler.solder_qty = Convert.ToInt32(txtTravelerSolderQty.Text);
                traveler.solder_comments = txtTravelerSolderResult.Text;
                traveler.solder_result = ddlTravelerSolderResult.SelectedValue;
            }
            else
            {

                traveler.has_solder = false;
                traveler.solder_qty = null;
                traveler.solder_comments = null;
                traveler.solder_result = null;
            }

            //Xeltek
            if (cbxTravelerXeltek.isChecked)
            {
                traveler.has_xeltek = true;
                traveler.xeltek_qty = Convert.ToInt32(txtTravelerXelTekQty.Text);
                traveler.xeltek_comments = txtTravelerXelTekResult.Text;
                traveler.xeltek_result = ddlTravelerXelTekResult.SelectedValue;
            }
            else
            {
                traveler.has_xeltek = false;
                traveler.xeltek_qty = null;
                traveler.xeltek_comments = null;
                traveler.xeltek_result = null;
            }

            //XRF
            if (cbxTravelerXrf.isChecked)
            {
                traveler.has_xrf = true;
                traveler.xrf_qty = Convert.ToInt32(txtTravelerXrfQty.Text);
                traveler.xrf_comments = txtTravelerXrfResult.Text;
                traveler.xrf_result = ddlTravelerXrfResult.SelectedValue;

            }
            else
            {
                traveler.has_xrf = false;
                traveler.xrf_qty = null;
                traveler.xrf_comments = null;
                traveler.xrf_result = null;
            }


            pdc.SaveChanges();
        }

    }

    private void LoadAllValues() //
    {
        LoadAllSectionObjects();
        //Load the Data / UI Elements
        LoadSectionValues("header");


        //Warehouse Section
        if (ddlVendor.SelectedValue != "1")//If not a Stock Inspection, load Warehouse, else hide it. 
        {
            if (whse != null)
                LoadSectionValues("whse");
        }
        else
            HidePanels("Warehouse");

        //Detail Sections
        if (detail != null)
            LoadSectionValues("detail");

        //Detail Sections
        if (traveler != null)
            LoadSectionValues("traveler");


        LoadAllImages();
        LoadTurnBacks();
        LoadActionPanel();


    }

    private void LoadExtendedImagery()
    {

        SM_Quality_Logic smq = new SM_Quality_Logic();
        images = smq.LoadInspectionImagery(SM_Enums.InspectionType.idea.ToString(), intInspID, "ExtendedImagery");
        iig.EnableControls = true;
        iig.ShowDescription = true;
        iig.EnableUpload = true;
        iig.LoadGallery(images, SM_Enums.InspectionType.idea.ToString().ToUpper(), intInspID, "ExtendedImagery");

    }


    //This works to Validate all ddl answers.  From: http://stackoverflow.com/questions/7362482/c-sharp-get-all-web-controls-on-page
    private void GetControlList<T>(ControlCollection controlCollection, List<T> resultCollection) where T : Control
    {

        foreach (Control control in controlCollection)
        {
            //if (control.GetType() == typeof(T))
            if (control is T) // This is cleaner
                resultCollection.Add((T)control);
            if (control.HasControls())
                GetControlList(control.Controls, resultCollection);
        }
    }

    protected bool ValidateAll()
    {

        // string headerImageValidStatus = ValidateHeaderImages();

        if (!ValidateHeaderImages()) //Header Images
        {
            throw new Exception("Cannot complete.  Please provide component header imagery.");
        }
        if (ddlVendor.SelectedValue != "1")//IF not a stock Inspection, allow complete
        {
            string whseValidStatus = ValidateWhse();
            if (ddlVendor.SelectedValue != "1")//Stock Selected, Warehouse not needed
            {
                if (whseValidStatus != "valid")
                {
                    hfTabName.Value = "MainContent_tpnlWarehouse";
                    //throw new Exception(whseValidStatus);
                    tools.HandleResult("fail", whseValidStatus);
                    return false;
                }

            }
        }
        string detailValidStatus = ValidateDetail();
        if (detailValidStatus != "valid")//Details validated
        {
            hfTabName.Value = "MainContent_tpnlDetail";
            tools.HandleResult("fail", detailValidStatus); return false;


        }
        return true;
    }

    protected string ValidateHeader()
    {

        int n;
        bool isNumeric = int.TryParse(txtQTY.Text, out n);

        if (txtMFG.Text.Length == 0)
        {
            return ("Sorry, you must provide the manufacturer.");

        }

        if (txtPartNumber.Text.Length == 0)
        {
            return ("Sorry, you must provide the part number.");

        }

        if (txtQTY.Text.Length == 0 || isNumeric == false)
        {
            return ("Sorry, you must provide the quantity, and it must be a whole number.");

        }
        if (txtDC.Text.Length == 0)
        {
            return ("Sorry, you must provide the date code.");

        }

        if (txtLot.Text.Length == 0)
        {
            return ("Sorry, you must provide the lot code.");

        }

        if (txtCOO.Text.Length == 0)
        {
            return ("Sorry, you must provide the country of origin.");

        }

        if (txtPackageType.Text.Length == 0)
        {
            return ("Sorry, you must provide the package type.");

        }
        // var columnnames = from t in typeof(SMCPortalDataContext).GetProperties() select t.Name;

        if (txtDC.Text.Length > 250)
        {
            return ("Sorry, the date code field is limited to 250 characters, if you need more space to fulfill a business need, please email helpdesk@sensiblemicro.com");

        }

        if (!check_gvPartData() && System.Web.Security.Membership.GetUser().UserName != "kevint")
        {
            return ("Please select at least one item from the Part Data grid to inspect.");

        }

        if (!check_gvPartData())
        {
            return ("Please select at least one item from the Part Data grid to inspect.");
        }

        return "valid";
    }

    public string ValidateWhse()
    {
        List<DropDownList> allControls = new List<DropDownList>();
        GetControlList<DropDownList>(pnlWarehouse.Controls, allControls);


        foreach (var childControl in allControls)
        {
            //     call for all controls of the page
            if (childControl.SelectedValue == "choose" && childControl.Visible == true)
            {
                return ("Please supply a value for " + childControl.ID);
            }
        }
        return "valid";
    }

    public string ValidateDetail()
    {
        string validateCarrierType = ValidateCarrierType();
        string validateOtherDetail = ValidateOtherDetail();

        if (ddlCarrierType.SelectedValue == "choose")
        {
            //TabContainer1.ActiveTab = tpnlDetail;
            return ("Please indicate the carrier type.");
        }
        else if (ddlis_mech.SelectedValue == "choose")
        {
            //TabContainer1.ActiveTab = tpnlDetail;
            return ("Please indicate whether mechanical testing is being performed.");
        }
        else if (ddlis_solvent.SelectedValue == "choose")
        {
            //TabContainer1.ActiveTab = tpnlDetail;
            return ("Please indicate whether heated solvent testing is being performed.");
        }
        else if (ddlisAddTesting.SelectedValue == "choose")
        {
            //TabContainer1.ActiveTab = tpnlDetail;
            return ("Please indicate whether additional testing is being performed.");
        }

        else if (validateCarrierType != "valid")
        {
            return (validateCarrierType);
        }

        else if (validateOtherDetail != "valid")
        {
            return (validateOtherDetail);
        }


        List<DropDownList> detailDropDowns = new List<DropDownList>();
        GetControlList<DropDownList>(pnlDetail.Controls, detailDropDowns);//Ignore Carrier Type here.
        foreach (var childControl in detailDropDowns)
        {
            string trimmedID = tools.trimControlNumberSuffix(childControl.ID);
            if (!CarrierTypesList.Contains(trimmedID))
                //     call for all controls of the page
                if (childControl.SelectedValue == "choose")
                {

                    return ("Please supply a value for " + childControl.ID + ".");
                }
        }
        return ("valid");
    }

    private string ValidateCarrierType()
    {
        List<DropDownList> carrierTypelDropDowns = new List<DropDownList>();


        switch (ddlCarrierType.SelectedValue.ToLower())
        {
            case "1": //Tube
                {
                    GetControlList<DropDownList>(pnlTube.Controls, carrierTypelDropDowns);
                    break;
                }
            case "2"://Reel
                {
                    GetControlList<DropDownList>(pnlReel.Controls, carrierTypelDropDowns);
                    break;
                }
            case "3"://Tray
                {
                    GetControlList<DropDownList>(pnlTray.Controls, carrierTypelDropDowns);
                    break;
                }
            case "4"://Bulk
                {
                    GetControlList<DropDownList>(pnlBulk.Controls, carrierTypelDropDowns);
                    break;
                }
        }
        foreach (var childControl in carrierTypelDropDowns)
        {
            //bool test = childControl.Style.Value == "display:inline";
            //     call for all controls of the page
            if (childControl.SelectedValue == "choose")
            {

                return ("Please supply a value for the carrier:  " + childControl.ID);
            }
        }
        return "valid";
    }

    private string ValidateOtherDetail()
    {
        //Checking AddTesting1st so i don't have to loop through the other controls if this is already invalid
        if (ddlisAddTesting.SelectedValue == "1")
            if (string.IsNullOrEmpty(txtAddTesting.Text))
                return "Please provide notes for AdditionalTesting";

        List<DropDownList> otherDropDowns = new List<DropDownList>();
        if (ddlis_solvent.SelectedValue == "1")
            GetControlList<DropDownList>(pnlDetailSolvents.Controls, otherDropDowns);
        if (ddlis_mech.SelectedValue == "1")
            GetControlList<DropDownList>(pnlMech.Controls, otherDropDowns);

        foreach (var childControl in otherDropDowns)
        {
            if (childControl.SelectedValue == "choose")
                return ("Please supply a value for :  " + childControl.ID);
        }
        return "valid";
    }

    public bool ValidateHeaderImages()
    {
        if (!imHeaderImageTop.imageLoaded || !imHeaderImageBot.imageLoaded || !imHeaderImageOther.imageLoaded)
        {
            //throw new Exception("Please provide all three header images.");            
            return false;
        }
        return true;

    }

    //Delete / Undelete button
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            delete_undelete();
        }
        catch(Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
        
    }
    private void delete_undelete()
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {


            //SMCPortalDataContext PDC = new SMCPortalDataContext();
            string message = "";
            var isdeleted =
                from insp in pdc.insp_head
                where (insp.insp_id == intInspID)
                select insp;
            foreach (insp_head i in isdeleted)
            {

                //if (i.is_deleted == true)
                if (checkdeleted())
                {
                    btnDelete.Text = "Undelete";
                    i.is_deleted = false;    
                    
                    message = "Inspection # " + intInspID + " has been successfully restored";
                }
                else
                {

                    btnDelete.Text = "Delete";
                    i.is_deleted = true;                    
                    RemoveRzLineID();
                    DeleteAllImages();
                    message = "Inspection # " + intInspID + " has been successfully deleted";
                    Response.Redirect("~/secure_sm/IDEA_insp/SM_Inspection_search.aspx", false);
                }

            }

            pdc.SaveChanges();
            tools.HandleResult("success", message);
        }
    }

    protected void RemoveRzLineID()
    {
        List<orddet_line> lineList = new List<orddet_line>();
        using (RzDataContext rdc = new RzDataContext())
        {
            lineList = rdc.orddet_lines.Where(X => X.insp_id == intInspID.ToString()).ToList();

            foreach (orddet_line l in lineList)
                l.insp_id = null;

            rdc.SubmitChanges();
        }


    }

    protected bool checkdeleted()
    {


        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            //SMCPortalDataContext PDC = new SMCPortalDataContext();
            var isdeleted =
                from insp in pdc.insp_head
                where (insp.insp_id == intInspID)
                select insp;
            foreach (insp_head h in isdeleted)
            {
                if (h.is_deleted == true)
                    return true;
            }
            return false;
        }
    }

    protected bool check_admin()
    {
        if (User.IsInRole("sm_insp_admin") || User.IsInRole("admin"))
            return true;
        return false;

    }
    protected bool check_dev()
    {
        if (System.Web.Security.Membership.GetUser().ToString() == "kevint")
            return true;
        return false;

    }
    protected bool check_insp_user()
    {
        if (User.IsInRole("sm_insp_user"))
            return true;
        return false;
    }

    protected bool validateInteger(string input)
    {
        string i = input;
        int x;
        if (int.TryParse(input, out x))
            return false;
        else return true;
    }

    protected void SetTabComplete()
    {
        if (header != null)
            if (header.is_complete ?? false)
                lblHeaderCheck.CssClass = "fa fa-check-circle";
            else
                lblHeaderCheck.CssClass = "";
        if (whse != null)
            if (whse.is_complete ?? false)
                lblWarehouseCheck.CssClass = "fa fa-check-circle";
            else
                lblWarehouseCheck.CssClass = "";
        if (detail != null)
            if (detail.is_complete ?? false)
                lblDetailCheck.CssClass = "fa fa-check-circle";
            else
                lblDetailCheck.CssClass = "";



    }

    protected void SendMail()
    {


        ProfileCommon userProfile = Profile.GetProfile(System.Web.Security.Membership.GetUser().ToString());
        if (header == null)
            throw new Exception("Header object not found.");

        string Inspector = header.inspector;
        //string insp_id = header.insp_id.ToString();
        string insp_date = Convert.ToDateTime(header.insp_date.ToString()).ToString("MM/dd/yyyy");
        string PartNumber = header.fullpartnumber;
        string QTY = header.quantity.ToString();
        string VendorPO = header.vendor_po;
        string VendorName = header.vendor_name;
        string ResultNotes;

        ResultNotes = header.resultnotes;

        MailMessage Msg = new MailMessage();
        Msg.From = new MailAddress("sm_shipping@sensiblemicro.com");
        if (check_dev())
        {
            ////For Testing purposes
            Msg.To.Add("ktill@sensiblemicro.com");
        }
        //else
        //{
        //KT - Having to send "TO" shipping@ instead of sm_shipping
        Msg.To.Add("shipping@sensiblemicro.com");

        if (!string.IsNullOrEmpty(header.sales_email))
        {
            if (Tools.Email.IsEmailAddress(header.sales_email))
                Msg.CC.Add(header.sales_email);

            //Check if the salesperson has assistants, if so add them.
            using (RzDataContext rdc = new RzDataContext())
            {
                n_user u = rdc.n_users.Where(w => w.email_address.ToLower().Trim() == header.sales_email.ToLower().Trim()).FirstOrDefault();
                if (u != null)
                {
                    List<n_user> salesAssistants = new List<n_user>();
                    salesAssistants = rdc.n_users.Where(w => w.assistant_to_uid == u.unique_id).ToList();
                    if (salesAssistants.Count > 0)
                        if (Tools.Email.IsEmailAddress(salesAssistants[0].email_address))
                            Msg.CC.Add(salesAssistants[0].email_address);

                }
            }

        }

        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/inspection_notify.htm"));
        string readFile = reader.ReadToEnd();
        string StrContent = "";
        StrContent = readFile;
        StrContent = StrContent.Replace("[insp_date]", insp_date);
        StrContent = StrContent.Replace("[PartNumber]", PartNumber);
        StrContent = StrContent.Replace("[insp_id]", strInspID);
        StrContent = StrContent.Replace("[QTY]", QTY);
        StrContent = StrContent.Replace("[VendorName]", VendorName);
        StrContent = StrContent.Replace("[VendorPO]", VendorPO);
        StrContent = StrContent.Replace("[Inspector]", Inspector);
        StrContent = StrContent.Replace("[resultNotes]", ResultNotes);
        StrContent = StrContent.Replace("[Result]", header.result);
        StrContent = StrContent.Replace("[InspID]", strInspID);


        if (header.result != "Fail")
        {
            DateTime expectedShipDate = dtpExpectedShip.SelectedDate.Value;
            StrContent = StrContent.Replace("[expectedShipDate]", Convert.ToDateTime(header.expectedShipDate.ToString()).ToString("MM/dd/yyyy"));
        }

        Msg.Subject = Inspector + " has completed incoming inspection for: Vendor PO:" + VendorPO + " // Part Number:" + PartNumber;
        Msg.Body = StrContent.ToString();
        Msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.Send(Msg);
        tools.HandleResult("success", "Inspection Email sent. Shipping and Sales Agent (if any) successfully notified.");


    }

    protected DateTime CalcEstimatedShipDate(DateTime insp_completion_date)
    {
        DateTime ret = Tools.Dates.AddBusinessDays(DateTime.Now, 1);
        return ret;


    }


    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {

        ClearFields("ddlVendor");
        detect_OrderChange();
        if (ddlVendor.SelectedValue == "1")//Stock Vendor
        {
            pnlStockCustomer.Style.Add("display", "inline");
            txtCustomer.Text = null;
        }
        else if (ddlVendor.SelectedValue == "0")
            ddlVendorPO.Style.Add("display", "none");
        else
        {
            pnlVendorPO.Style.Add("display", "inline");
            BindVendorPODDL();
        }

    }

    protected void ddlVendorPO_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddlStockSale.Items.Clear();
        //ddlStockSale.DataBind();
        if (ddlVendorPO.SelectedValue != "0")//If it's not set to "choose"
        {
            hfCustomerID.Value = null;
            hfSaleID.Value = null;
            //LoadInspectionType();
            ClearFields("ddlVendorPO");
            detect_OrderChange();
            //GetSalesData();
            //Show_gvPartData();
            BindgvPartData();
        }
        else
        {
            pnlSale.Style.Add("display", "none");
            pnl_gvPartData.Visible = false;

        }


    }


    protected void lbselectStockCustomer_Click(object sender, EventArgs e)
    {
        try
        {
            hfCustomerID.Value = null;
            hfSaleID.Value = null;
            pnl_gvPartData.Style.Add("display", "none");
            using (RzDataContext rdc = new RzDataContext())
                TheCustomer = rdc.companies.Where(w => w.companyname == txtCustomer.Text).FirstOrDefault();

            if (TheCustomer == null)
            {
                //  throw new Exception("Company not found.  Please search again and select the company from the autocomplete list.");
                tools.HandleResult("fail", "Company not found.  Please search again and select the company from the autocomplete list.");
                return;
            }

            hfCustomerID.Value = TheCustomer.unique_id;
            Bind_ddlStockSale();

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void ddlStockSale_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearFields("ddlSMCSO");
        hfSaleID.Value = null;
        if (ddlStockSale.SelectedValue != "1" && ddlStockSale.SelectedValue != "0")
        {
            using (RzDataContext rdc = new RzDataContext())
                TheSale = rdc.ordhed_sales.Where(w => w.unique_id == ddlStockSale.SelectedValue).FirstOrDefault();
            if (TheSale != null)
                hfSaleID.Value = TheSale.unique_id;
        }

        BindgvPartData();
        //Show_gvPartData();

    }

    protected void Bind_ddlStockSale()
    {
        string companyID = "";
        if (!string.IsNullOrEmpty(hfCustomerID.Value))
            companyID = hfCustomerID.Value;


        using (RzDataContext rdc = new RzDataContext())
        {
            var ddlStockSale_query =
                 (from h in rdc.ordhed_sales
                  from l in rdc.orddet_lines
                  orderby h.ordernumber descending
                  where h.base_company_uid == companyID && h.unique_id == l.orderid_sales
                  select new
                  {
                      h.ordernumber,
                      h.orderdate,
                      h.unique_id
                  }).Distinct();
            ddlStockSale.DataSource = ddlStockSale_query.OrderByDescending(o => o.orderdate);
            pnlSale.Style.Add("display", "inline");





            //if (ddlCustomer.SelectedValue != "0")

            ddlStockSale.DataTextField = "ordernumber";
            ddlStockSale.DataValueField = "unique_id";
            ddlStockSale.Items.Clear();
            ddlStockSale.Items.Add(new ListItem("--Choose Sale--", "0"));
            ddlStockSale.AppendDataBoundItems = true;
            ddlStockSale.DataBind();
            ddlStockSale.SelectedValue = "0";
            pnlStockCustomer.Visible = true;
        }
    }

    protected void BindVendorDDL()
    {

        using (RzDataContext rdc = new RzDataContext())
        {
            var data = rdc.SMCPortal_Orddet_Vendors();
            ddlVendor.DataSource = data.ToList();
            ddlVendor.DataTextField = "vendor_name";
            ddlVendor.DataValueField = "vendor_uid";
            ddlVendor.Items.Add(new ListItem("--Choose Vendor--", "0"));
            ddlVendor.Items.Add(new ListItem("--Stock--", "1"));
            //ddlVendor.Items.Add(new ListItem("--Unknown Stock Vendor--", "1"));
            ddlVendor.AppendDataBoundItems = true;
            ddlVendor.DataBind();
        }

    }


    protected void BindVendorPODDL()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            var poquery =
                             (from po in rdc.orddet_lines
                              orderby po.ordernumber_purchase descending
                              where po.vendor_uid == ddlVendor.SelectedValue && po.ordernumber_purchase.Length > 0 && po.orderdate_purchase > Convert.ToDateTime("1-1-2012")
                              select new
                              {
                                  po.ordernumber_purchase,
                                  po.orderid_purchase,
                              }).Distinct();



            ddlVendorPO.Items.Clear();
            ddlVendorPO.Items.Add(new ListItem("--Choose Vendor PO--", "0"));
            ddlVendorPO.AppendDataBoundItems = true;
            ddlVendorPO.DataSource = poquery.OrderByDescending(po => po.ordernumber_purchase);
            ddlVendorPO.DataTextField = "ordernumber_purchase";
            ddlVendorPO.DataValueField = "orderid_purchase";
            ddlVendorPO.DataBind();
            ddlVendorPO.Style.Add("display", "inline");

        }
    }



    protected void BindgvPartData()
    {
        //Only show these parts if:
        //1) we know the Vendor PO
        //2) it's a stock vendor, and we have a Sales Order        
        //3) No Sale and search has been performed on the Stock Inspection thing. - Talking to Sean, this never happens, will hold off on coding for it.
        using (RzDataContext rdc = new RzDataContext())
        {


            //It's a STock Part with no PO, but we have a customer and Sale
            if (ddlVendor.SelectedValue == "1")
            {
                //Stock Vendor
                if (TheSale != null) // has Sale
                {
                    var SOQuery_Stock =
                 (from s in rdc.orddet_lines
                  where s.orderid_sales == TheSale.unique_id

                  select new
                  {
                      Order = s.ordernumber_sales,
                      OrderLine = s.linecode_sales,
                      Part = s.fullpartnumber,
                      MFG = s.manufacturer,
                      QTY = s.quantity,
                      ID = s.insp_id,
                      lineId = s.unique_id
                  });

                    gvPartData.DataSource = SOQuery_Stock;
                    if (SOQuery_Stock.Any())
                    {
                        btnAutofillPartData.Style.Add("display", "inline");
                        gvPartData.DataBind();
                    }

                }
            }
            //Not a Stock PArt, has Vendor PO
            else if (!string.IsNullOrEmpty(ddlVendorPO.SelectedValue) && ddlVendorPO.SelectedValue != "0" && ddlVendorPO.SelectedValue != "1")
            {
                var SOQuery =
                       (from s in rdc.orddet_lines
                            //where s.orderid_sales == ddlSMCSO.SelectedValue && s.orderid_purchase == ddlVendorPO.SelectedValue
                        where s.orderid_purchase == ddlVendorPO.SelectedValue
                        select new
                        {
                            Order = s.ordernumber_sales,
                            OrderLine = s.linecode_purchase,
                            Part = s.fullpartnumber,
                            MFG = s.manufacturer,
                            QTY = s.quantity,
                            ID = s.insp_id,
                            lineId = s.unique_id
                        });
                gvPartData.DataSource = SOQuery;

                if (SOQuery.Any())
                {
                    btnAutofillPartData.Style.Add("display", "inline");
                    gvPartData.DataBind();
                }

                //KT Set check values if insp_id if present
            }

            else //this is Stock Inspction, 
            {
                //Not Implemented

            }


            pnl_gvPartData.Style.Add("display", "inline");
            //if (!Page.IsPostBack)
            //    gvPartData.DataBind();

            if (rdc.orddet_lines.Any(l => l.insp_id == strInspID))
                SetChecks();

        }
    }

    protected void SetChecks()
    {

        foreach (GridViewRow row in gvPartData.Rows)
        {
            if (row.Cells[6].Text == strInspID)
            {
                ((CheckBox)row.FindControl("cbxPartSelect")).Checked = true;
            }
        }

    }


    protected void GetCustomerAndVendorObjects()
    {
        //Get Customer
        using (RzDataContext rdc = new RzDataContext())
        {
            if (!string.IsNullOrEmpty(header.customer_uid))
                TheCustomer = rdc.companies.Where(w => w.unique_id == header.customer_uid).FirstOrDefault();
            else if (!string.IsNullOrEmpty(hfCustomerID.Value))
                TheCustomer = rdc.companies.Where(w => w.unique_id == hfCustomerID.Value).FirstOrDefault();

            TheSale = rdc.ordhed_sales.Where(w => w.unique_id == hfSaleID.Value).FirstOrDefault();
            if (TheSale == null)
                TheSale = rdc.ordhed_sales.Where(w => w.unique_id == header.orderid_sales).FirstOrDefault();

            //Vendor
            if (!string.IsNullOrEmpty(header.vendor_uid))
                TheVendor = rdc.companies.Where(w => w.unique_id == header.vendor_uid).FirstOrDefault();

            if (!string.IsNullOrEmpty(header.vendor_po_uid))
                ThePurchase = rdc.ordhed_purchases.Where(w => w.unique_id == header.vendor_po_uid).FirstOrDefault();
        }






    }


    protected void ClearFields(String ControlID = null)
    {
        txtPartNumber.Text = null;
        txtMFG.Text = null;
        lblCustPO.Text = null;



        gvPartData.DataSource = null;
        gvPartData.DataBind();
        ddlStockSale.DataSource = null;
        ddlStockSale.DataBind();

        lblCustPO.Text = "";
        pnl_gvPartData.Style.Add("display", "none");

        if (ControlID != null)
        {
            switch (ControlID)
            {
                case "ddlVendor":
                    hfCustomerID.Value = null;
                    hfSaleID.Value = null;
                    pnlSale.Style.Add("display", "none");
                    pnlStockCustomer.Style.Add("display", "none");
                    pnlVendorPO.Style.Add("display", "none");

                    break;
                case "ddlVendorPO":
                    {
                        hfCustomerID.Value = null;
                        hfSaleID.Value = null;
                        break;
                    }

                case "ddlCustomer":
                    {
                        pnlSale.Style.Add("display", "none");
                        hfSaleID.Value = null;
                    }

                    break;

            }
        }

        lblPageError.Text = "";
        lblHeaderError.Text = "";
    }

    protected string CheckComplete()
    {
        //Check Header 
        //bool headerComplete = false;
        //bool whseComplete = false;
        //bool detailsComplete = false;
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            //Header
            header = pdc.insp_head.Where(w => w.insp_id == intInspID).SingleOrDefault();
            if (header == null)
                return ("Header section is incomplete.  ");
            //headerComplete = header.is_complete ?? false;
            if (!header.is_complete ?? false)
                return ("Header section is incomplete.  ");

            // If there is a warehouse, check it, otherwise skip.
            if (!string.IsNullOrEmpty(header.vendor_uid) && header.vendor_uid != "1") //stock inspection, no warehouse needed.
            {

                whse = pdc.insp_whse.Where(w => w.insp_id == intInspID).FirstOrDefault();
                if (whse == null)
                    return ("Warehouse record not found.");

                //whseComplete = whse.is_complete ?? false;
                if (!whse.is_complete ?? false)
                    return ("Warehouse section is incomplete.  ");
            }

            //Details

            //detailsComplete = detail.is_complete ?? false;

            detail = pdc.insp_detail.Where(w => w.insp_id == intInspID).SingleOrDefault();
            if (detail == null)
                return ("Detail returned null");
            if (!detail.is_complete ?? false)
                return ("Details section is incomplete.  ");

            return "complete";
        }

    }

    protected bool CheckSectionComplete(string SectionID)
    {

        using (sm_portalEntities pdc = new sm_portalEntities())
            switch (SectionID)
            {

                case "Header":
                    {
                        //Check Header
                        //ValidateHeader();
                        return pdc.insp_head.Any(h => h.insp_id == intInspID && h.is_complete == true);
                    }
                case "Warehouse":
                    {
                        //Check Warehouse
                        //ValidateWhse();
                        return pdc.insp_whse.Any(w => w.insp_id == intInspID && w.is_complete == true);
                    }
                case "Detail":
                    {
                        //Check Detail
                        //ValidateDetail();
                        return pdc.insp_detail.Any(d => d.insp_id == intInspID && d.is_complete == true);
                    }
                default:
                    {
                        return false;
                    }
            }

    }


    protected void btnReport_Click(object sender, EventArgs e)
    {

        Response.Redirect("http://sm1/ReportServer_SQLEXPRESS/Pages/ReportViewer.aspx?%2fRz4+Reports%2fShipping+and+QC%2fIDEA+Inspection+Report&rs:Command=Render&rc:Parameters=false&prmInsp_id=" + intInspID, false);

    }


    protected void btnAutofillPartData_Click(object sender, EventArgs e)
    {
        try
        {


            txtPartNumber.Text = "";
            txtMFG.Text = "";
            txtQTY.Text = "";
            int QTY = 0;
            List<string> PartNumberComparison = new List<string>();
            List<string> MFGComparison = new List<string>();
            int i = 0;

            foreach (GridViewRow row in gvPartData.Rows)
            {

                CheckBox cbx = (CheckBox)row.FindControl("cbxPartSelect");
                if (cbx != null)
                    if (cbx.Checked)
                    {
                        i++;
                        PartNumberComparison.Add(row.Cells[3].Text.Trim());
                        string mfg = row.Cells[4].Text.Trim() ?? "";
                        if (mfg != "&nbsp;")
                            MFGComparison.Add(mfg);
                        QTY += Convert.ToInt32(row.Cells[5].Text);
                        lineIdList.Add(row.Cells[7].Text);
                    }
            }
            if (i == 0)
            {
                tools.HandleResult("fail", "Please select at least one part from the list to inspect.");
                return;
            }

            //For all selected parts, check if the quote partnumber matches the line part number)
            string mismatch = CheckForQuotePartMismatch(lineIdList);
            if (!string.IsNullOrEmpty(mismatch))
                tools.HandleResult(SM_Enums.LogType.Information.ToString(), mismatch);

            List<string> PartNumberDupes = PartNumberComparison.Select(x => x).Distinct().ToList();
            List<string> MFGDupes = MFGComparison.Select(x => x).Distinct().ToList();
            if (PartNumberDupes.Count > 1 || MFGDupes.Count > 1)
            {
                lblHeaderError.Style.Add("display", "inline");
                lblHeaderError.Text = "It appears you have more than one unique part number or manufacturer selected.  Either do a separate report for each individual part, or manually type the part data";
            }
            else
            {
                lblHeaderError.Style.Add("display", "inline");
                txtPartNumber.Text = PartNumberDupes.First();
                txtMFG.Text = MFGDupes.FirstOrDefault() ?? "Mfg Not Found";

            }

            txtQTY.Text = QTY.ToString();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }


    protected void SaveRzLineData()
    {
        //To set insp_id on appropriate Rz Lines
        List<string> SelectedLineIDs = new List<string>();
        //To Remove insp_id on save if needed.
        List<string> UnSelectedLineIDs = new List<string>();
        //GET Selected and Unselected lines in their own lists.
        foreach (GridViewRow row in gvPartData.Rows)
        {
            if (((CheckBox)row.FindControl("cbxPartSelect")).Checked)
            {
                if (!SelectedLineIDs.Contains(row.Cells[7].Text))
                    SelectedLineIDs.Add(row.Cells[7].Text);
            }
            else
            {
                if (!UnSelectedLineIDs.Contains(row.Cells[7].Text))
                    UnSelectedLineIDs.Add(row.Cells[7].Text);
            }
        }
        //Get List of RZ Lines
        //List<orddet_line> RzLines = RZDC.orddet_lines.Where(l => l.orderid_sales == ddlSMCSO.SelectedValue && l.orderid_purchase == ddlVendorPO.SelectedValue).ToList();
        //List<orddet_line> RzLines = RZDC.orddet_lines.Where(l => (SelectedLineIDs.Contains(l.unique_id) || UnSelectedLineIDs.Contains(l.unique_id)) && l.orderid_purchase == ddlVendorPO.SelectedValue).ToList();
        using (RzDataContext rdc = new RzDataContext())
        {
            List<orddet_line> RzLines = rdc.orddet_lines.Where(l => (SelectedLineIDs.Contains(l.unique_id) || UnSelectedLineIDs.Contains(l.unique_id))).ToList();
            List<string> selectedSaleIDs = new List<string>();//If lines selected for multiple customer
            if (TheSale != null)
                selectedSaleIDs.Add(TheSale.unique_id);
            foreach (orddet_line l in RzLines)
            {
                //Save the is if the box is checked
                if (SelectedLineIDs.Contains(l.unique_id))
                {
                    //Ensure no Inspection ID already Exists on the RzLine.
                    if (!string.IsNullOrEmpty(l.insp_id) && l.insp_id != strInspID)
                    {

                        //  throw new Exception("Company not found.  Please search again and select the company from the autocomplete list.");
                        tools.HandleResult("fail", "Could not save inspection ID in Rz, this line is already associated with inspection ID " + l.insp_id + ".");
                        return;

                        //Don't allow changing of existing lineID                    
                        //throw new Exception("Could not save inspection ID in Rz, this line is already associated with inspection ID " + l.insp_id + ".");
                    }
                    else
                    {
                        l.insp_id = header.insp_id.ToString();
                        if (!string.IsNullOrEmpty(l.orderid_sales))//If there is not salds order, allow the insp_id, since it could be a stock overbuy.  We don't want to lose the fact that these have been inspected.
                            if (!selectedSaleIDs.Contains(l.orderid_sales))
                                selectedSaleIDs.Add(l.orderid_sales);
                        if (l.qc_status == SM_Enums.QcStatus.Inbound.ToString())
                            l.qc_status = SM_Enums.QcStatus.In_House.ToString();
                    }

                }
                else if (UnSelectedLineIDs.Contains(l.unique_id) && l.insp_id == strInspID)
                    l.insp_id = null;

            }
            if (selectedSaleIDs.Count == 1)
            {
                ordhed_sale s = rdc.ordhed_sales.Where(w => w.unique_id == selectedSaleIDs[0]).FirstOrDefault();
                if (s != null)
                {
                    hfCustomerID.Value = s.base_company_uid;
                    hfSaleID.Value = s.unique_id;
                }
                else
                {
                    hfCustomerID.Value = null;
                    hfSaleID.Value = null;
                }

            }
            else if (selectedSaleIDs.Count > 1)
            {
                // throw new Exception("It seems like you've selected lines that belong to multiple sales.  You'll need to do a separate inspection for each. If yout think this is in error, it may be a bug, please alert your manager.");
                tools.HandleResult("fail", "It seems like you've selected lines that belong to multiple sales.  You'll need to do a separate inspection for each. If yout think this is in error, it may be a bug, please alert your manager.");
            }

            if (strInspID != "0")
                rdc.SubmitChanges();
            else
            {
                throw new Exception("Inspection ID cannot be 0");
            }
        }

    }

    protected void ShowReportButton()
    {
        btnReport.Style.Add("display", "inline");
    }


    private void DoRzLineCompletion(string type)
    {
        List<orddet_line> lines = new List<orddet_line>();
        using (RzDataContext rdc = new RzDataContext())
        {
            lines = rdc.orddet_lines.Where(w => w.insp_id == strInspID).ToList();
            if (lines.Count == 0)
                return;

            foreach (orddet_line l in lines)
            {
                switch (type)
                {
                    case "complete":
                        {
                            l.qc_status = SM_Enums.QcStatus.Final_Inspection.ToString();
                            break;
                        }
                    default:
                        {
                            l.qc_status = SM_Enums.QcStatus.In_House.ToString();
                            break;
                        }
                }
                rdc.SubmitChanges();
            }

        }

    }


    protected void validatePassable() // if marking "Pass" ensure no critical values are not failed.
    {
        //Detail Table Answers
        //Initial

        insp_detail d = null;
        using (sm_portalEntities pdc = new sm_portalEntities())
            d = pdc.insp_detail.SingleOrDefault(s => s.insp_id == intInspID);
        if (d == null)
        {
            tools.HandleResult("fail", "No Details data found for this report.");
            return;
        }


        if (d.initial1_result == "2")
        {
            //throw new Exception("You have indicated a failure in Initial Inspection (IDEA 10.2.1).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Initial Inspection (IDEA 10.2.1).  Cannot pass this report.");
            return;
        }

        if (d.initial4_result == "2")
        {
            //throw new Exception("You have indicated a failure in Initial Inspection (IDEA 10.2.1).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Initial Inspection (IDEA 10.2.1).  Cannot pass this report.");
            return;
        }

        //Detal Visual
        if (d.detail1_result == "2")
        {
            // throw new Exception("You have indicated a failure in Detailed Inspection (Visual) (IDEA 10.3.1).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Detailed Inspection (Visual) (IDEA 10.3.1).  Cannot pass this report.");
            return;
        }

        if (d.detail11_result == "2")
        {
            //throw new Exception("You have indicated a failure in Detailed Inspection (Visual) (IDEA 10.3.1).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Detailed Inspection (Visual) (IDEA 10.3.1).  Cannot pass this report.");
            return;
        }

        //Solvents
        if (d.solvent1_result == "2")
        {
            //throw new Exception("You have indicated a failure in Detailed Inspection (Solvents)(IDEA 10.3.2).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Detailed Inspection (Solvents)(IDEA 10.3.2).  Cannot pass this report.");
            return;
        }

        if (d.solvent2_result == "2")
        {
            //throw new Exception("You have indicated a failure in Detailed Inspection (Solvents)(IDEA 10.3.2).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Detailed Inspection (Solvents)(IDEA 10.3.2).  Cannot pass this report.");
            return;

        }
        if (d.solvent3_result == "2")
        {
            //throw new Exception("You have indicated a failure in Detailed Inspection (Solvents)(IDEA 10.3.2).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Detailed Inspection (Solvents)(IDEA 10.3.2).  Cannot pass this report.");
            return;
        }


        //Mech
        if (d.mech2_result == "2")
        {
            //throw new Exception("You have indicated a failure in Detailed Inspection (Mechanical) (IDEA 10.3.3).  Cannot pass this report.");
            tools.HandleResult("fail", "You have indicated a failure in Detailed Inspection (Mechanical) (IDEA 10.3.3).  Cannot pass this report.");
            return;
        }

    }



    protected void btnTestModal_Click(object sender, EventArgs e)
    {
        try
        {
            ShowModal();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    protected void btnModalYes_Click(object sender, EventArgs e)
    {
        try
        {


            SendMail();
            CloseModal();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    protected void btnModalNo_Click(object sender, EventArgs e)
    {
        CloseModal();
    }
    protected void ShowModal()
    {
        divModalOverlay.Attributes["class"] = "ModalOverlay";
        divModal.Attributes["class"] = "ModalVisible";
    }
    protected void CloseModal()
    {
        divModalOverlay.Attributes["class"] = "";
        divModal.Attributes["class"] = "ModalHidden";
    }
    protected void reSendEmail()
    {
        lblModalText.Text = "This inspection was previously completed.  Do you want to re-send the inspection email?";
        ShowModal();
    }

    protected void LoadActionPanel()
    {

        //txtExpectedShip.Text = DateTime.Now.ToString("MM/dd/yyyy");
        dtpExpectedShip.SelectedDate = DateTime.Now;
        lblAP_Status.Text = CurrentStatus ?? "No Status Detected";
        if (lastTurnBack != null)
            if (!lastTurnBack.turn_back_resolution_date.HasValue)//last turnback is unresolved, show it's note.
                txtTurnBackNotes.Text = lastTurnBack.turn_back_notes ?? null;
            else
                txtTurnBackNotes.Text = null;

        if (CurrentStatus == "Complete")
        {

            //if (header.expectedShipDate != null)
            //    txtExpectedShip.Text = Convert.ToDateTime(header.expectedShipDate.ToString()).ToString("MM/dd/yyyy");
            //else
            //    txtExpectedShip.Text = "(choose date)";

            btnAP_MarkComplete.Text = "Mark Incomplete";
            //btnAP_CompleteInsp.Style.Add("display", "none");
            //dtpExpectedShip.Enabled = false;
            txtResultNotes.Enabled = false;
            divAction.Style["background-color"] = "#dff0d8";
            ddlResult.Enabled = false;

            //Final Inspection Stuff
            pnlFinalInspection.Visible = true;
            lbFinalComplete.Visible = true;
            lbTurnBack.Visible = true;
            lbResolveTurnBack.Visible = false;

        }
        else if (CurrentStatus == "Turned Back")
        {
            pnlFinalInspection.Visible = true;
            lbFinalComplete.Visible = false;
            lbTurnBack.Visible = false;
            lbResolveTurnBack.Visible = true;
            txtTurnBackNotes.Enabled = false;
            btnAP_MarkComplete.Text = "Mark Incomplete";
        }
        else if (CurrentStatus == "Final Inspection Complete")
        {
            btnAP_MarkComplete.Text = "Mark Incomplete";
            btnReport.Visible = true;
            btnSummary.Visible = true;
            btnTestEmail.Visible = true;
            lbTurnBack.Visible = false;
            lbFinalComplete.Visible = false;
        }
        else
        {
            btnAP_MarkComplete.Text = "Mark Complete";
            //btnAP_CompleteInsp.Style.Add("display", "inline");
            dtpExpectedShip.Enabled = true;
            txtResultNotes.Enabled = true;
            divAction.Style["background-color"] = "#d9edf7";
            ddlResult.Enabled = true;
            pnlFinalInspection.Visible = false;
        }


        HandleActionPanelResult();
    }

    private void HandleActionPanelResult()
    {
        switch (Result)
        {
            case "Pass":
                {
                    pnlPass.Style.Add("display", "inline");
                    ddlResult.SelectedValue = "1";
                    break;
                }
            case "Fail":
                {
                    ddlResult.SelectedValue = "2";
                    pnlPass.Style.Add("display", "none");
                    break;
                }
            default:
                {
                    ddlResult.SelectedValue = "choose";
                    pnlPass.Style.Add("display", "none");
                    break;
                }
        }

    }


    protected bool check_gvPartData()
    {

        if (gvPartData.Visible == true)
        {
            int i = 0;
            foreach (GridViewRow row in gvPartData.Rows)
            {

                CheckBox check = (CheckBox)row.FindControl("cbxPartSelect");
                if (check.Checked == true)
                    i++;
            }
            if (i == 0)
                return false;
            else
                return true;
        }
        else
        {
            return true;
        }
    }

    protected void detect_OrderChange()
    {
        List<orddet_line> linesList = new List<orddet_line>();
        using (RzDataContext rdc = new RzDataContext())
        {
            if (!string.IsNullOrEmpty(strInspID))
            {
                var query =
                from l in rdc.orddet_lines
                where l.insp_id == strInspID
                select l;

                foreach (orddet_line l in query)
                {
                    l.insp_id = null;
                }
                rdc.SubmitChanges();
            }
        }

        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            header.fullpartnumber = null;
            header.orddet_line_uid = null;
            header.orderid_sales = null;
            header.ordernumber_sales = null;
            header.customer_name = null;
            header.customer_uid = null;
            header.customer_po = null;
            header.vendor_name = null;
            header.vendor_uid = null;
            header.vendor_po = null;
            header.vendor_po_uid = null;
            pdc.SaveChanges();
        }


    }
    protected void btnTestEmail_Click(object sender, EventArgs e)
    {
        try
        {
            SendMail();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    protected string CheckForQuotePartMismatch(List<string> partIdList)
    {
        string message = "";
        using (RzDataContext rdc = new RzDataContext())
        {

            var query = (from l in rdc.orddet_lines
                         join q in rdc.orddet_quotes on l.quote_line_uid equals q.unique_id
                             into t
                         from rt in t.DefaultIfEmpty()
                         where partIdList.Contains(l.unique_id)
                         orderby l.orderdate_sales
                         select new
                         {
                             sm_part = l.fullpartnumber,
                             quote_part = rt.fullpartnumber
                         }).ToList();
            foreach (var s in query)
            {
                if (s.sm_part != s.quote_part)
                {
                    // tools.JS_Modal_Confirm("Warning, the quoted part number(" + s.quote_part + ") differs from the line item part number (" + s.sm_part + ") (Is this a private label part?).  Please be sure the correct customer-facing part number and manufacturer are used", Page);
                    message = "Heads up, the quoted part number(" + s.quote_part + ") differs from the line item part number (" + s.sm_part + ") (Is this a private label part?).  Please be sure the correct customer-facing part number and manufacturer are used";
                }

            }
        }
        return message;
    }


    protected void btngvPartData_Search_Click(object sender, EventArgs e)
    {
        //Show_gvPartData();
        BindgvPartData();
    }

    protected void HidePanels(string list)
    {
        List<string> panels = list.Split(',').ToList();
        foreach (string s in panels)
        {
            if (panels.Contains("Warehouse"))
            {
                //lblWarehouse.Style.Add("display", "none");
                pnlWarehouse.Style.Add("display", "none");
                //tpnlWarehouse.Style.Add("visibility", "hidden");
            }
            if (panels.Contains("Details"))
            {
                //lblDetail.Style.Add("display", "none");
                pnlDetail.Style.Add("display", "none");
                //tpnlDetail.Style.Add("visibility", "hidden");
            }
        }

    }

    protected void LoadImageControl(insp_images i)
    {
        SM_Quality_Logic smq = new SM_Quality_Logic();
        string imageManagerControlid = "";
        if (smq.GetAllLegacyImageSectionIDs().Contains(i.insp_section_id))////Legacy Section IDs            
            LoadImageControl_LegacySectionID(i);
        else
        {
            //Match up the insp_images to the proper control by SectionID
            imageManagerControlid = "im" + i.insp_section_id;
            InspectionImageManagerControl c = (InspectionImageManagerControl)Page.Master.FindControl("MainContent").FindControl(imageManagerControlid);
            if (c == null)
                throw new Exception("Could not locate Image Manager Control");

            c.init(i);
        }
    }

    protected void LoadImageControl_LegacySectionID(insp_images i)
    {
        //Get the base section ID, i.e. DetailVisual
        string sectionID = i.insp_section_id.Replace("Image", "");
        InspectionImageManagerControl c = GetImageManagerControlList().Where(w => w.ID.Contains(sectionID) && !w.imageLoaded).FirstOrDefault();
        if (c == null)
            throw new Exception("Could not locate Image Manager Control for " + sectionID);//imDetailSolvents2
        c.init(i);

    }


    protected void ValidateFileType(string fileName)
    {
        List<string> AllowedMIMETypes = new List<string>();
        AllowedMIMETypes.Add("image/jpeg");
        AllowedMIMETypes.Add("image/gif");
        AllowedMIMETypes.Add("image/png");
        AllowedMIMETypes.Add("image/bmp");
        string AllowedMIMETypesString = string.Join(",", AllowedMIMETypes.ToArray());
        string ext = SM_Tools.MIMEAssistant.GetMIMEType(fileName);


        if (!AllowedMIMETypes.Contains(ext))
        {
            throw new Exception("Sorry, ." + ext + " isn't allowed.  Allowed filetypes are: " + AllowedMIMETypesString);
        }
    }

    private void LoadAllSectionObjects()
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            if (header == null)
                throw new Exception("Inspection Header not found.");
            whse = pdc.insp_whse.Where(w => w.insp_id == intInspID).SingleOrDefault();
            detail = pdc.insp_detail.Where(w => w.insp_id == intInspID).SingleOrDefault();
            traveler = pdc.insp_traveler.Where(w => w.insp_id == intInspID).SingleOrDefault();
        }

    }


    private void LoadSectionValues(string s)
    {

        //string sectionType = o.GetType().BaseType.FullName;
        //Load Data
        switch (s)
        {
            //case "SensibleDAL.ef.insp_head":
            case "header":
                {
                    LoadHeaderValues();

                    break;
                }
            //case "SensibleDAL.ef.insp_whse":
            case "whse":
                {
                    LoadWarehouseValues();
                    break;
                }
            //case "SensibleDAL.ef.insp_detail":
            case "detail":
                {
                    LoadDetailValues();
                    break;
                }
            //case "SensibleDAL.ef.insp_traveler":
            case "traveler":
                {
                    LoadTravelerValues();
                    break;
                }
        }
    }


    private void DeleteAllImages()
    {
        if (images != null)
            foreach (insp_images i in images)
            {
                smb.insp_images.Remove(i);
            }
        smb.SaveChanges();
    }

    protected void gvPartData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //Show_gvPartData();
        gvPartData.PageIndex = e.NewPageIndex;
        //gvPartData.DataBind();
    }

    protected void gvPartData_PageIndexChanged(object sender, EventArgs e)
    {
        using (RzDataContext rdc = new RzDataContext())
            if (rdc.orddet_lines.Any(l => l.insp_id == strInspID))
                SetChecks();
    }



    protected void CloneReport(out int cloneID)
    {
        //from: https://stackoverflow.com/questions/637535/duplicate-linq-to-sql-entity-record
        //Created a copy of the datacontext
        insp_head originalHead = null;
        insp_whse originalWhse = null;
        insp_detail originalDetail = null;

        using (sm_portalEntities pdcOriginal = new sm_portalEntities())
        {
            //Disable ObjectTracking ont he copy datacontext
            //pdcOriginal.ObjectTrackingEnabled = false;
            pdcOriginal.Configuration.AutoDetectChangesEnabled = false;
            //Create New Object instance
            originalHead = pdcOriginal.insp_head.Single(s => s.insp_id == intInspID);
        }
        if (originalHead == null)
            throw new Exception("Clone failed.  Could not obtain original header.");

        insp_head cloneHead = null;
        cloneHead = originalHead;
        //Change an necessary Parameters
        cloneHead.customer_name = "Sensible Micro Corporation";
        cloneHead.customer_uid = "037ED306-8D90-42D6-AAAA-AD91B900F263";
        cloneHead.customer_po = "1977_C3PO";
        cloneHead.ordernumber_sales = "THX1138";
        cloneHead.is_demo = true;
        cloneHead.is_deleted = false;
        cloneHead.source_insp_id = intInspID;




        insp_whse cloneWhse = null;
        insp_detail cloneDetail = null;
        using (sm_portalEntities pdc = new sm_portalEntities())
        {

            pdc.insp_head.Add(cloneHead);
            cloneWhse = pdc.insp_whse.SingleOrDefault(s => s.insp_id == intInspID);
            cloneDetail = pdc.insp_detail.SingleOrDefault(s => s.insp_id == intInspID);
            if (originalWhse != null)
            {
                cloneWhse = originalWhse;
                cloneWhse.insp_id = cloneHead.insp_id;
                pdc.insp_whse.Add(cloneWhse);
            }


            if (originalDetail != null)
            {
                cloneDetail = originalDetail;
                cloneDetail.insp_id = cloneHead.insp_id;
                pdc.insp_detail.Add(cloneDetail);
            }
            pdc.SaveChanges();
        }
        cloneID = cloneHead.insp_id;

    }


    protected void lbCloneReport_Click(object sender, EventArgs e)
    {
        try
        {
            int cloneID = 0;
            CloneReport(out cloneID);
            tools.HandleResult("success", "You have successfully created a clone of this IDEA Inspection Report. Clode report ID: " + cloneID + ". Redirecting now ... please wait");
            System.Threading.Thread.Sleep(5000);
            Response.Redirect("SM_Inspection_form.aspx?insp_id=" + cloneID, false);
        }
        catch (Exception ex)
        {
            tools.HandleResult(ex.Message, "fail");
        }

    }


    protected void btnAP_MarkComplete_Click(object sender, EventArgs e)
    {
        try
        {
            if (header == null)
                throw new Exception("Cannot complete. Header not found.");

            bool completing = false;
            if (header.status == "In Progress")
                completing = true;

            //SaveHeader(false);
            SaveAll(false);
            doCompletion(completing);
            LoadAllValues();
            //Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
    protected void doCompletion(bool completing = true)
    {

        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            insp_head h = pdc.insp_head.Where(w => w.insp_id == intInspID).SingleOrDefault();
            if (h == null)
                throw new Exception("Problem completing inspection, header returned null.");

            if (completing)
            {
                if (ValidateAll())
                    CompleteInspection(h);
            }
            else
                UnCompleteInspection(h);
            pdc.SaveChanges();
        }


        LoadActionPanel();
    }




    protected void CompleteInspection(insp_head h)
    {
        DateTime expShipDate = dtpExpectedShip.SelectedDate.Value;
        if (ddlResult.SelectedValue == "choose")
            throw new Exception("Please choose an inspection result.");
        if (ddlResult.SelectedValue == "1")
        {
            if (dtpExpectedShip.SelectedDate <= DateTime.MinValue)
                throw new Exception("Please provide a valid expected ship date.");
            else
            {
                using (RzDataContext rdc = new RzDataContext())
                {

                    header.expectedShipDate = expShipDate;
                    orddet_line l = rdc.orddet_lines.Where(w => w.insp_id == strInspID).FirstOrDefault();
                    l.ship_date_due = expShipDate;
                    rdc.SubmitChanges();
                }
            }

        }

        //ensure no critical elements are marked as fail if we are passing.           

        if (txtResultNotes.Text.Length > 0 && ddlResult.SelectedValue != "0")
        {

            string completeStatus = CheckComplete();
            if (completeStatus != "complete")
                throw new Exception("Cannot complete. " + completeStatus);
            h.expectedShipDate = expShipDate;
            h.status = "Complete";
            h.is_insp_complete = true;
            h.completed_date = DateTime.Now;
            h.completed_by = inspector;
            h.result = ddlResult.SelectedItem.ToString();
            h.resultnotes = txtResultNotes.Text;
            DoRzLineCompletion("complete");
            Result = h.result;
            tools.HandleResult("success", "Nice work inspector " + inspector + "! You have successfully completed inspeciton id: " + intInspID + " and it is now ready for Final Inspection."); //Since i'm response.redirecting on complete need to call this on page load.

            SendFinalInspectionReadyEmail();
        }
        else
        {
            throw new Exception("Cannot complete, please ensure you have entered result and accompanying notes.");
            //tools.HandleResult(Page, "fail", "Cannot complete, please ensure you have entered result and accompanying notes.");
            //return;
        }


    }




    protected void UnCompleteInspection(insp_head h)
    {


        DoRzLineCompletion("incomplete");
        txtResultNotes.Text = "";
        dtpExpectedShip.SelectedDate = DateTime.MinValue;


        h.status = "In Progress";
        h.is_insp_complete = false;
        h.completed_date = null;
        h.completed_by = "";
        h.result = "";
        h.resultnotes = "";
        ddlResult.SelectedValue = "choose";



        using (sm_portalEntities pdc = new sm_portalEntities())
        {

            tools.HandleResult("success", "You have un-copmpleted inspeciton id: " + intInspID, 20000);

            detail = pdc.insp_detail.Where(w => w.insp_id == intInspID).SingleOrDefault();
            if (detail != null)
                detail.is_complete = false;


            whse = pdc.insp_whse.Where(w => w.insp_id == intInspID).SingleOrDefault();
            if (whse != null)
                whse.is_complete = false;
            pdc.SaveChanges();
        }


    }

    protected void lbFinalComplete_Click(object sender, EventArgs e)
    {
        try
        {
            if (header.status == "Turned Back")
                throw new Exception("Cannot Final Complete.  Please resolve pending turnbacks.");

            if (!string.IsNullOrEmpty(header.current_turn_back_id))
                throw new Exception("Cannot Final Complete.  Please resolve turnback ID#." + header.current_turn_back_id);



            //If there is an unresolved turnback, cannot complete.
            if (lastTurnBack != null)
            {
                if (lastTurnBack.turn_back_resolution_date == null)
                    throw new Exception("Cannot Final Complete.  Please resolve pending turnbacks.");

            }

            //if this users is the same user that owns the report, they cannot be the final inspection.
            string userName = System.Web.Security.Membership.GetUser().UserName;
            if (userName == header.inspector)
                throw new Exception("Cannot final approve your own inspection.");
            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                insp_head h = pdc.insp_head.Where(w => w.insp_id == intInspID).SingleOrDefault();
                if (h == null)
                    throw new Exception("Error, cannot mark Final Complete, header returned null.");
                h.final_InspectionComplete = true;
                h.status = "Final Inspection Complete";
                h.final_InspectionCompleteDate = DateTime.Now;
                h.final_Inspector = userName;
                h.final_Inspector_uid = Profile.RzUserID;


                if (h.email_sent ?? false != true)
                    SendMail();
                else
                    reSendEmail();

                h.email_sent = true;
                pdc.SaveChanges();
            }

            LoadAllValues();
            //PDC.SubmitChanges(); - This already happens in caller
            //Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }



    protected void lbTurnBack_Click(object sender, EventArgs e)
    {
        try
        {
            qc_turn_back qct = new qc_turn_back();
            n_user originalInspector = null;
            using (RzDataContext rdc = new RzDataContext())
            {
                if (string.IsNullOrEmpty(txtTurnBackNotes.Text))
                    throw new Exception("Please provide some detail on the turnback issues before submitting turn-back.");
                //Turn back data

                qct.unique_id = Guid.NewGuid().ToString();
                qct.date_created = DateTime.Now;
                qct.inspection_agent = header.inspector;
                originalInspector = rdc.n_users.Where(w => w.name == header.inspector).FirstOrDefault();
                if (originalInspector != null)
                    qct.inspection_agent_uid = originalInspector.unique_id;
                qct.turn_back_date = DateTime.Now;
                qct.turn_back_notes = txtTurnBackNotes.Text;
                qct.turn_back_agent = System.Web.Security.Membership.GetUser().UserName;
                qct.insp_id = strInspID;
                n_user turnbackAgent = rdc.n_users.Where(w => w.login_name == qct.turn_back_agent).FirstOrDefault();
                if (turnbackAgent != null)
                    qct.turn_back_agent_uid = turnbackAgent.unique_id;


                //Header Data
                header.current_turn_back_id = qct.unique_id;
                header.final_InspectionComplete = false;
                header.final_InspectionCompleteDate = null;
                header.final_Inspector = null;
                header.final_Inspector_uid = null;
                header.status = "Turned Back";
                rdc.qc_turn_backs.InsertOnSubmit(qct);
                rdc.SubmitChanges();
            }
            using (sm_portalEntities pdc = new sm_portalEntities())
                pdc.SaveChanges();

            SendTurnbackEmail(qct, originalInspector);
            Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private void SendTurnbackEmail(qc_turn_back qct, n_user originalInspector)
    {
        if (originalInspector == null)
            return;
        string subject = "Inspection " + intInspID + " has been turned back.  Please correct any issues.";
        string to = originalInspector.email_address;
        string body = "The following issues were found:  <br /><br /><b>" + qct.turn_back_notes + "</b><br /><br />Click <a href=\"https://portal.sensiblemicro.com/secure_sm/IDEA_insp/SM_Inspection_form.aspx?insp_id=" + intInspID + "\">here to edit the inspection.</a> to access.";

        tools.SendMail(System.Web.Security.Membership.GetUser().Email, to, subject, body);
    }
    private void SendFinalInspectionReadyEmail(bool turnbackResolved = false)
    {
        string subject = "Inspection " + intInspID + " is ready for final inspection.";
        string to = "shipping@sensiblemicro.com";
        string body = "Click <a href=\"https://portal.sensiblemicro.com/secure_sm/IDEA_insp/SM_Inspection_form.aspx?insp_id=" + intInspID + "\">here</a> to access the inspection.";
        if (turnbackResolved)
            subject = "Turn-backs for Inspection " + intInspID + " are resolved.  Ready for final inspection.";
        tools.SendMail(System.Web.Security.Membership.GetUser().Email, to, subject, body);

    }
    protected void lbResolveTurnBack_Click(object sender, EventArgs e)
    {
        try
        {
            using (RzDataContext rdc = new RzDataContext())
            {
                //There should ever only be 1 OPEN turn back. I.e. Only one with no resolution date.
                qc_turn_back qct = rdc.qc_turn_backs.Where(w => w.unique_id == header.current_turn_back_id && w.insp_id == intInspID.ToString()).FirstOrDefault();
                if (qct == null)
                    return;
                qct.turn_back_resolution_date = DateTime.Now;
                rdc.SubmitChanges();
            }
            header.status = "Complete";
            header.current_turn_back_id = null;
            using (sm_portalEntities pdc = new sm_portalEntities())
                pdc.SaveChanges();
            SendFinalInspectionReadyEmail(true);
            Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void cbxNoCustomer_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxNoCustomer.Checked)
        {
            pnl_txtgvPartData_Search.Style.Add("display", "inline");
            txtCustomer.Enabled = false;

        }
        else
        {
            pnl_txtgvPartData_Search.Style.Add("display", "none");
            txtCustomer.Enabled = true;
        }

    }
}