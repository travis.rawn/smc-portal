﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_sm_IDEA_insp_SM_Inspection_search : System.Web.UI.Page
{
    
    MembershipUser mu;
    RzTools rzt = new RzTools();
    ProfileCommon profile;
    SM_Tools tools = new SM_Tools();
    SM_Quality_Logic smq = new SM_Quality_Logic();
    //SM_PDF smp = new SM_PDF();
    bool LINQ = true;
    //private object TheData = null;
    List<insp_head> inspectionList = new List<insp_head>();
    object theData;
    protected void Page_Load(object sender, EventArgs e)
    {

        theData = LoadInspections();
        BindGrid();
        GetViewState();
        SetInspectionCounts();

    }

    private void BindGrid()
    {
        GridViewLinq.Visible = true;
        GridViewLinq.DataBind();
        if (GridViewLinq.Rows.Count > 0)
            GridViewLinq.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    private object LoadInspections()
    {
        bool showDeleted = cbxIncludeDeleted.Checked;
        bool showSamples = cbxShowsamples.Checked;
        bool onlyOwned = OnlyOwned();
        string queryString = txtSearch.Text.Trim();
        string statusType = GetStatusType();



        inspectionList = smq.GetActiveInspections();
        //inspectionList = inspectionList.Where(w => w.date_created >= DateTime.Today.AddDays(-1)).ToList();
        var query = inspectionList.Select(i => new
        {

            ID = i.insp_id,
            Date = i.insp_date,
            //Need to set "" for nulls, or the search query will not work, since we not doing  a "Like" SQL operation
            Part = i.fullpartnumber ?? "",
            i.date_modified,
            Sale = i.ordernumber_sales ?? "",
            CPO = i.customer_po ?? "",
            VPO = i.vendor_po ?? "",
            QTY = i.quantity,
            Status = i.status,
            Inspector = i.inspector,
            result = i.result,
            i.is_deleted,
            i.is_demo,
            i.sales_email
        });

        //Filter Deleted
        if (!showDeleted)//show deleted
        {
            query = query.Where(w => w.is_deleted == false || w.is_deleted == null);
        }
        else
        {
            query = query.Where(w => w.is_deleted ?? false == true);
        }

        //Filter Demo       
        if (!showSamples)
        {
            query = query.Where(w => w.is_demo == false || w.is_demo == null);
        }
        else
        {
            query = query.Where(w => w.is_demo == true && w.Status.ToLower().Contains("complete"));
        }
        //Filter Permission
        if (!onlyOwned)
            query = query.Where(w => w.sales_email.ToLower() == GetSalesEmail());
        //Filter Status Tupe     
        if (statusType != "All")
            query = query.Where(w => w.Status == statusType);
        //Filter Search String
        if (queryString.Length > 0)
            query = query.Where(w => w.Part.Contains(queryString) || w.CPO.Contains(queryString) || w.ID.ToString().Contains(queryString) || w.Sale.ToString().Contains(queryString) || w.VPO.ToString().Contains(queryString));


        return query.OrderByDescending(o => o.ID);
    }

    private string GetStatusType()
    {
        //In Progress,Turned Back, Final Inspection Complete, In Progress //all, progress, initialComplete, finalComplete
        switch (ddlStatus.SelectedValue)
        {
            case "progress":
                return "In Progress";
            case "initialComplete":
                return "Complete";
            case "finalComplete":
                return "Final Inspection Complete";
            case "turnedBack":
                return "Turned Back";
            default:
                return "All";
        }
    }

    private string GetSalesEmail()
    {
        if (Profile.RzUserID != null)
            return rzt.GetRzRepEmail(Profile.RzUserID);
        return null;

    }

    protected bool check_admin()
    {
        if (User.IsInRole("sm_insp_admin") || User.IsInRole("admin"))
            return true;
        return false;

    }
    protected bool check_insp_user()
    {
        if (User.IsInRole("sm_insp_user"))
            return true;
        return false;
    }
    protected void GetViewState()
    {
        if (mu == null)
            mu = System.Web.Security.Membership.GetUser();
        if (profile == null)
            profile = Profile.GetProfile(mu.UserName);
        if (profile == null)
            lblError.Text = "There was a problem loading the user's profile";
        else
        {
            ViewState["FullName"] = profile.FirstName + " " + profile.LastName;
            string test = ViewState["FullName"].ToString();
        }

    }
    protected void SetInspectionCounts()
    {

        //string UserFilter;
        //UserFilter = "%";
        //if (Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_insp_admin") || Roles.IsUserInRole("sm_insp_user") || Roles.IsUserInRole("sm_internal_executive"))
        //{
        //    UserFilter = "%";
        //}
        //else
        //{
        //    UserFilter = ViewState["FullName"].ToString();
        //    divCounts.Visible = false;
        //}
        if (!Roles.IsUserInRole("admin") || !Roles.IsUserInRole("sm_insp_admin") || !Roles.IsUserInRole("sm_insp_user") || !Roles.IsUserInRole("sm_internal_executive"))
        {
            string userName = ViewState["FullName"].ToString();
            inspectionList.Where(w => w.inspector == userName).ToList();
        }


        //lblCountTotalInspections.Text = inspectionList.Count(x => ((x.is_deleted == false || x.is_deleted == null) && x.fullpartnumber.Length > 0 )).ToString();
        lblCompleteInsp.Text = inspectionList.Count(x => ((x.is_deleted == false || x.is_deleted == null) && x.fullpartnumber.Length > 0 && x.status == "Complete")).ToString();
        lblInProgressInsp.Text = inspectionList.Count(x => ((x.is_deleted == false || x.is_deleted == null) && x.fullpartnumber.Length > 0 && x.status == "In Progress")).ToString();
        lblTodaysInsp.Text = inspectionList.Count(x => ((x.is_deleted == false || x.is_deleted == null) && x.fullpartnumber.Length > 0 && x.completed_date >= DateTime.Today)).ToString();
        //Turn Backs
        using (RzDataContext rdc = new RzDataContext())
            lblOpenTurnBacks.Text = rdc.qc_turn_backs.Count(c => c.turn_back_resolution_date == null && c.inspection_agent != "Kevin Till").ToString();

    }
    protected void GridViewLinq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewLinq.PageIndex = e.NewPageIndex;
        GridViewLinq.DataBind();
    }
    protected void GridViewLinq_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            //LinkButton lb1Edit = (LinkButton)e.Row.FindControl("lbEdit");
            HyperLink hlEdit = (HyperLink)e.Row.FindControl("hlEdit");
            LinkButton lb = (LinkButton)e.Row.FindControl("lbSummaryReport");

            HtmlGenericControl inProgressLabel;
            string insp_id = "";
            if (e.Row.RowIndex >= 0)
            {
                insp_id = GridViewLinq.DataKeys[e.Row.RowIndex].Value.ToString();
                //lb1Edit.PostBackUrl = "~/secure_sm/IDEA_insp/SM_Inspection_form.aspx?insp_id=" + insp_id;
                hlEdit.NavigateUrl = "~/secure_sm/IDEA_insp/SM_Inspection_form.aspx?insp_id=" + insp_id;
                

                inProgressLabel = (HtmlGenericControl)e.Row.FindControl("lblInProgress");
                if (!inspectionList.Where(w => w.insp_id.ToString() == insp_id && w.is_insp_complete == true).Any())
                {
                    e.Row.FindControl("btnReport").Visible = false;
                    e.Row.FindControl("hlSummaryReport").Visible = false;


                    inProgressLabel.Attributes["Style"] = "display:block; font-weight:bold; font-style:italic; font-size:11px;";
                }
                else
                    inProgressLabel.Attributes["Style"] = "display:none;";


            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //doSearch(LINQ);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }


    }

    private bool OnlyOwned()
    {
        //if (Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_insp_admin") || Roles.IsUserInRole("sm_insp_user") || Roles.IsUserInRole("sm_internal_executive"))
        if (Roles.IsUserInRole("sm_internal"))
            return true;
        return false;
    }

    protected void btnNewInsp_Click(object sender, EventArgs e)
    {
        try
        {
            insp_head h = smq.CreateNewIDEAInspectionHeader(Profile);
            if (h == null)
                throw new Exception("Invalid inspection header.");
            if (h.insp_id <= 0)
                throw new Exception("Invalid inspection id.");
            Response.Redirect("/secure_sm/IDEA_insp/SM_Inspection_form.aspx?insp_id=" + h.insp_id, false);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", "Cannot create inspection: " + ex.Message);
        }
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = sender as LinkButton;
            GridViewRow row = btn.NamingContainer as GridViewRow;
            string insp_id = GridViewLinq.DataKeys[row.RowIndex].Values[0].ToString();
            smq.CreateIDEAInspectionPDF(insp_id);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
    protected void cbxShowsamples_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void cbxIncludeDeleted_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        e.Result = theData;
    }


    protected void lbClear_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl, false);
    }



    protected void GridViewLinq_DataBound(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_insp_admin") || Roles.IsUserInRole("sm_insp_user"))
            GridViewLinq.Columns[0].Visible = true;
        else
            GridViewLinq.Columns[0].Visible = false;
    }

    protected void lbSummaryReport_Click(object sender, EventArgs e)
    {

    }
}