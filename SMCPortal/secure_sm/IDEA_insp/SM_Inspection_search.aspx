﻿<%@ Page Title="IDEA Inspection Search" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="SM_Inspection_search.aspx.cs" Inherits="secure_sm_IDEA_insp_SM_Inspection_search" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="headContent" runat="Server">
    <style>
        .countBlock{
            text-align:center;
        }
        .countLabel{
            font-size:14px;
        }

    </style>
    </asp:Content>
<asp:Content ID="LeadContent" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>IDEA INSPECTION SYSTEM</h1>
    </div>
    <div class="panel panel-heading">Search for an inspection by Sales Order, Customer PO, or Vendor PO.</div>
    <asp:Label ID="lblError" runat="server"></asp:Label>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSearch">

        <%--Totals section--%>
        <div class="well well-sm" id="divCounts" runat="server">
            <div class="row" style="width: 100%;">
               <%-- <div class="col-sm-3 countBlock">
                     Total Inspections<br />
                    <span class="label label-success countLabel">
                        <asp:Label ID="lblCountTotalInspections" runat="server" /></span>
                </div>--%>
                <div class="col-sm-3 countBlock">
                     Completed Inspections<br />
                    <span class="label label-success countLabel">
                        <asp:Label ID="lblCompleteInsp" runat="server" /></span>
                </div>
                <div class="col-sm-3 countBlock">
                     In Progress<br />
                    <span class="label label-success countLabel">
                        <asp:Label ID="lblInProgressInsp" runat="server" /></span>
                </div>
                <div class="col-sm-3 countBlock">
                    Completed today<br />
                    <span class="label label-success countLabel">
                        <asp:Label ID="lblTodaysInsp" runat="server" /></span> 
                </div>
                <div class="col-sm-3 countBlock">                    
                     Open TurnBacks<br />
                    <span class="label label-success countLabel">
                        <asp:Label ID="lblOpenTurnBacks" runat="server" /></span>
                </div>
            </div>
        </div>
        <%--/Totals section--%>
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div id="inspectionSearchControls" style="padding-bottom: 10px;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <asp:LinkButton ID="btnNewInsp" runat="server" CssClass="btn-smc btn-success" ToolTip="New Inspection" Text="Create New Inspection" OnClick="btnNewInsp_Click">Create New</asp:LinkButton>
                                <asp:TextBox ID="txtSearch" runat="server" Placeholder="ID, SO, Cust PO, SMC PO" CssClass="form-control"></asp:TextBox>
                                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" CssClass="form-control">
                                    <asp:ListItem Value="all">All</asp:ListItem>
                                    <asp:ListItem Value="progress">In Progress</asp:ListItem>
                                    <asp:ListItem Value="initialComplete">Initial Complete</asp:ListItem>
                                    <asp:ListItem Value="finalComplete">Final Complete</asp:ListItem>
                                    <asp:ListItem Value="turnedBack">Turned Back</asp:ListItem>
                                    
                                </asp:DropDownList>
                                <asp:CheckBox ID="cbxShowsamples" runat="server"  CssClass="form-control" AutoPostBack="false" Text="Show sample reports" />
                                <asp:CheckBox ID="cbxIncludeDeleted" runat="server" CssClass="form-control" AutoPostBack="false" Text="Show Deleted" />
                                <asp:LinkButton ID="btnSearch" CssClass="btn-smc btn-smc-primary" runat="server" Text="Search" ToolTip="Search" OnClick="btnSearch_Click">Search</asp:LinkButton>
                                <asp:LinkButton ID="lbClear" CssClass="btn-smc btn-smc-danger" runat="server" Text="Search" ToolTip="Clear Filter" OnClick="lbClear_Click">Clear</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>




        <%--Gridview section--%>

        <div id="InspectionSearchResults" style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">            
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" OnSelecting="LinqDataSource1_Selecting"></asp:LinqDataSource>
            <%--      <asp:GridView ID="GridViewLinq" runat="server" CssClass="table table-hover table-striped gvstyling stacktable" GridLines="None" DataSourceID="LinqDataSource1" AllowPaging="true" AllowSorting="true" CellPadding="5" EmptyDataText="No Records Found" AutoGenerateColumns="False" DataKeyNames="ID" PagerStyle-Wrap="False" PagerStyle-BorderStyle="None" PagerSettings-Mode="Numeric" OnPageIndexChanging="GridViewLinq_PageIndexChanging" PagerSettings-Visible="False">
            --%>
            <asp:GridView ID="GridViewLinq" runat="server" CssClass="table table-hover table-striped gvstyling" GridLines="None" AllowPaging="True" AllowSorting="True" DataSourceID="LinqDataSource1" CellPadding="5" EmptyDataText="No Records Found" AutoGenerateColumns="False" DataKeyNames="ID" PagerStyle-Wrap="False" PagerStyle-BorderStyle="None" PagerSettings-Mode="Numeric" OnPageIndexChanging="GridViewLinq_PageIndexChanging" OnRowDataBound="GridViewLinq_RowDataBound" OnDataBound="GridViewLinq_DataBound">

                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField ShowHeader="False" HeaderStyle-Width="60px">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlEdit" runat="server" Text="" Target="_Blank" CssClass="btn btn-sm btn-default" ToolTip="Edit / View this Inspection" ><span class="far fa-edit" style="font-size:30px;"></span></asp:HyperLink>
                            <%--<asp:LinkButton ID="lbEdit" runat="server" CausesValidation="False" CommandName="Select" Text="" Target="_Blank" CssClass="btn btn-sm btn-default" ToolTip="Edit / View this Inspection" OnClientClick="ShowUpdateProgress();"><span class="far fa-edit" style="font-size:30px;"></span></asp:LinkButton>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Report" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <span id="lblInProgress" style="display: none; font-weight: bold; font-style: italic; font-size: 9px;" runat="server">In Progress</span>
                            <asp:LinkButton ID="btnReport" runat="server" CssClass="btn btn-sm btn-default btn-block"   ToolTip="Download PDF" Text="Full" OnClick="btnReport_Click" OnClientClick="slideAlert( 'success', 'Your PDF is being generated and will be downloaded momentarily.');"></asp:LinkButton>
                             <%--<asp:LinkButton ID="lbSummaryReport" runat="server" CssClass="btn btn-sm btn-default btn-block"   ToolTip="View Summary Report" Text="Summary" OnClick="lbSummaryReport_Click"></asp:LinkButton>--%>
                            
                            <asp:HyperLink ID="hlSummaryReport" runat="server" CssClass="btn btn-sm btn-default btn-block" NavigateUrl='<%# Eval("ID", "/secure_members/qc_center/IDEA_summary_report.aspx?insp_id={0}") %>' Target="_blank" Text="" ToolTip="View Summary Report">Summary</asp:HyperLink>
                        </ItemTemplate>
                        <ItemStyle Wrap="True" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="Part" HeaderText="Part" SortExpression="Part" />
                    <asp:BoundField DataField="QTY" HeaderText="QTY" SortExpression="QTY" />
                    <asp:BoundField DataField="Sale" HeaderText="Sale" SortExpression="Sale" />
                    <asp:BoundField DataField="CPO" HeaderText="CustPO" SortExpression="CPO" />
                    <asp:BoundField DataField="date_modified" HeaderText="Modified" SortExpression="date_modified" DataFormatString="{0:d}" HtmlEncode="false" />
                    <asp:BoundField DataField="Inspector" HeaderText="Inspector" SortExpression="Inspector" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                </Columns>
                <PagerStyle Wrap="False" BorderStyle="None"></PagerStyle>
                <SortedAscendingHeaderStyle CssClass="sortasc" />
                <SortedDescendingHeaderStyle CssClass="sortdesc" />
            </asp:GridView>
        </div>

    </asp:Panel>
</asp:Content>
