﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Security;
using SensibleDAL;
using SensibleDAL.dbml;
public partial class services : System.Web.UI.Page
{
    //RzDataContext rdc = new RzDataContext();
    SM_Tools tools = new SM_Tools();
    //bool nonRecurring = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        //string azureConnString = System.Configuration.ConfigurationManager.ConnectionStrings["AzureConnection"].ConnectionString;





        try
        {
            string specified_svc = "";
            DateTime startTime = DateTime.Now;

            if (!isDuringBusinessHours())
                return;


            CheckQueryStrings();

            //Check for a specified service.
            specified_svc = Tools.Strings.SanitizeInput(Request.QueryString["ssv"]);

            string operationName = "Recurring Services";
            if (!string.IsNullOrEmpty(specified_svc))
                operationName = specified_svc;


            using (RzDataContext rdc = new RzDataContext())
            {

                if (string.IsNullOrEmpty(specified_svc))
                    RunRecurringServices(rdc);
                else
                    RunSpecifiedServices(rdc, specified_svc);
            }
            
            TimeSpan duration = Tools.Dates.GetTimeSpan(startTime, DateTime.Now);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Sucessfully completed " + operationName + ".  Total Time: " + duration.Minutes + " : " + duration.Seconds);


        }

        catch (Exception ex)
        {
            if (ex is System.Threading.ThreadAbortException)
                return;
            string message = "Page Load Exception: " + ex.Message;
            SystemLogic.Logs.LogEvent(ex, message, true);
            //tools.SendMail(SM_Security.EmailGroupAddress.PortalAlert, SM_Security.EmailGroupAddress.Systems, "Portal Automated Service Failure", "Error:<br /><br />" + ex.Message + "<br /><br /> Inner Exception:<br /><br />" + ex.InnerException + "<br />" + specified_svc);
        }


    }

    private void RunSpecifiedServices(RzDataContext rdc, string specified_svc)
    {

        try
        {


            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Starting specified services");

            //Erai Import
            if (specified_svc == SM_Enums.AutomateProcess.erai_import.ToString())
                Do_Erai_import(rdc);
            //Aging Quotes
            if (specified_svc == SM_Enums.AutomateProcess.aged_quotes.ToString())
                ManageAgingQuoteData(rdc);

            //SiteMap Generator
            if (specified_svc == SM_Enums.AutomateProcess.sitemap_generator.ToString())
                GenerateSiteMaps(rdc);

            //Daily Call Count Digest
            if (specified_svc == SM_Enums.AutomateProcess.daily_call_count.ToString())
                SendNoCallEmailAlert(rdc);

            //Truncate Engagements
            if (specified_svc == SM_Enums.AutomateProcess.truncate_hubspot_engagements.ToString())
                TruncateHubspotEngagementTable(rdc);

            //TBD Lines Report
            if (specified_svc == SM_Enums.AutomateProcess.source_tbd_report.ToString())
                LineLogic.GenerateSourceTBDEmailReport(rdc);

            //Hubspot Deal Sync
            if (specified_svc == SM_Enums.AutomateProcess.hubspot_deal_sync.ToString())
            {
                //Currently running this every hour, for deals updated in the past 65 minutes.
                int updatedDealCount = 0;

                //Scheduled task runs every 30 minutes, therefore check modified objects within the last 45 minutes should give us enough buffer while keeping the queries / sync volumed count low.
                DateTime startDate = DateTime.Now.AddMinutes(-45);
                //DateTime startDate = new DateTime(2022, 08, 10);
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Hubspot Deal Sync Started.");
                string syncReport = "";
                try
                {
                    HubspotLogic.SynchronizeHubspotDeals(rdc, startDate, out updatedDealCount, out syncReport);
                    //SystemLogic.Email.SendMail("hs_sync@sensiblemicro.com", "systems@sensiblemicro.com", "Success: Hubspot Deal Sync " + DateTime.Now.ToShortTimeString(), "Sync Report: < br /> "+syncReport);
                    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Hubspot Deal Sync Complete. " + updatedDealCount + " deals processed");
                }
                catch (Exception ex)
                {
                    SystemLogic.Email.SendMail("hs_sync@sensiblemicro.com", "systems@sensiblemicro.com", "Error: Hubspot Deal Sync " + DateTime.Now.ToShortTimeString(), ex.Message + "<br /> Sync Report: <br />" + syncReport);
                }

            }



            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Specified services processed successfully.");
        }
        catch (Exception ex)
        {
            string message = "Specified Services Error: " + ex.Message;
            string innerException = ex.InnerException.Message;
            SystemLogic.Logs.LogEvent(ex, message + "Inner: " + innerException, true);
        }
    }


    private void TruncateHubspotEngagementTable(RzDataContext rdc)
    {

        //Clear anything older than 1 year from today's date
        DateTime today = DateTime.Today.Date;
        DateTime oneYearAgo = today.AddYears(-1);

        var query = rdc.hubspot_engagements.Where(w => w.date_created <= oneYearAgo);

        foreach (var v in query)
        {
            rdc.hubspot_engagements.DeleteOnSubmit(v);
        }

        try
        {
            rdc.SubmitChanges();
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Truncate Engagement Success: truncated " + query.Count() + " rows.");
            tools.SendMail("portal_automations@sensiblemicro.com", "systems@sensiblemicro.com", "Truncate Engagement Operation Successful.", query.Count() + " erngagements truncated.");
        }
        catch (Exception ex)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Truncate Hubspot Engagement Error: " + ex.Message, true);
        }



    }

    private void SendNoCallEmailAlert(RzDataContext rdc)
    {
        //Called via Scheduled Tasks at 1pm and once more 4 hours later

        try
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Running Daily Call Count Alert.");
            HubspotLogic.HubspotCalls.DoNoCallAlertEmail(rdc);
        }
        catch (Exception ex)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Error Running Daily Call Count Alert. <br />" + ex.Message);
        }
    }

    private void GenerateSiteMaps(RzDataContext rdc)
    {
        try
        {
            sitemap_generator sg = new sitemap_generator(rdc);

            sg.CreateSitemaps("all");

            //Success notification
            string message = "Successfully created sitemap files: " + string.Join(", ", sitemap_generator.SitemapFileNames);

            //tools.HandleResult("success", "Successfully generated sitemap files. ");
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Sitemap generation completed successfully. " + message);
            tools.SendMail("sitemap@sensiblemicro.com", SystemLogic.Email.EmailGroup.Systems, "Sitemap(s) Successfully Generated " + DateTime.Now.ToShortDateString(), message);
        }
        catch (Exception ex)
        {
            string message = "Generate Sitemaps Error: " + ex.Message;
            SystemLogic.Logs.LogEvent(ex, message, true);
            //Fail notification            
            tools.SendMail("sitemap@sensiblemicro.com", SystemLogic.Email.EmailGroup.Systems, "Sitemap(s) Failed to Generate " + DateTime.Now.ToShortDateString(), ex.Message + " " + ex.InnerException);
        }
    }

    private void CheckQueryStrings()
    {
        string queryString = Tools.Strings.SanitizeInput(Request.QueryString["anhsertd"]);
        if (string.IsNullOrEmpty(queryString))
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, "Access querystring not provided.");
            Bounce();
        }

        if (queryString != "a7942f18b4c3b9e8cf31c0d0fb7ebc7e")
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, "Invalid access querystring. (" + queryString + ")");
            Bounce();
        }
    }

    private bool isDuringBusinessHours()
    {
        TimeSpan start = new TimeSpan(08, 0, 0); //8 o'clock
        TimeSpan end = new TimeSpan(19, 0, 0); //7 o'clock, 
        TimeSpan now = DateTime.Now.TimeOfDay;

        bool isBusinessHours = false;
        if ((now > start) && (now < end))
            isBusinessHours = true;
        return isBusinessHours;
    }

    private async Task RunRecurringServices(RzDataContext rdc)
    {
        DateTime startTime = DateTime.Now;
        TimeSpan duration;
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Starting recurring services");
        try
        {

            //Commenting out for now, as this could impact our production Hubspot Pipeline.
            //RefreshHubspotData(rdc);

            await CheckSiliconExpertUsage(rdc);

            //CloseRzDeals(rdc);

            duration = Tools.Dates.GetTimeSpan(startTime, DateTime.Now);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Recurring services processed successfully. Total Time: " + duration.Minutes + ": " + duration.Seconds);

        }
        catch (Exception ex)
        {
            duration = Tools.Dates.GetTimeSpan(startTime, DateTime.Now);
            bool emailAlert = true;
            //I don't need an alert for these.
            if (ex.Message.Contains("Unexpected character encountered while parsing value"))
                emailAlert = false;
            string message = "Recurring Services Error: " + ex.Message + "<br />Total Time: " + duration.Minutes + ": " + duration.Seconds;
            SystemLogic.Logs.LogEvent(ex, message, emailAlert);
        }


    }


    private void Do_Erai_import(RzDataContext rdc)
    {
        double lastUpdatedDaysAgo = 0;
        int staleDays = -2;
        try
        {

            // Set the root path that will contain the file.
            string root_path = @"\\storage.sensiblemicro.local\sm_storage\shared_docs\ftp\ftp_erai";
            //Get a list of files existing at the root path
            string[] arrErai_files = Directory.GetFiles(root_path);
            //Set variable for the full file path
            string filePath = "";
            //If the file exists at the root patch, set the full path variable
            if (arrErai_files.Contains(root_path + "\\erai_parts_list.csv"))
                filePath = root_path + "\\erai_parts_list.csv";
            //If for soem reason, the strin is empty, throw exception.
            if (string.IsNullOrEmpty(filePath))
                throw new Exception("The string vairable for the ERAI csv file was empty. (filePath: " + filePath + ")");
            if (!File.Exists(filePath))
                throw new Exception("The file doesn't exists at the specified file path. (filePath: " + filePath + ")");


            //Get the date the erai csv was updated by FTP
            DateTime fileCreationDate = File.GetLastWriteTime(filePath);


            //Set the variable for last update date.
            lastUpdatedDaysAgo = (fileCreationDate - DateTime.Now).TotalDays;

            //Do the database operations

            //Get the current records, store them during import, delete if import successful.
            List<erai_import> existingEraiList = rdc.erai_imports.ToList();
            List<erai_import> newEraiList = new List<erai_import>();
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line = string.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] strRow = line.Split(',');
                    erai_import e = new erai_import();
                    e.fullpartnumber = strRow[0];
                    e.manufacturer = strRow[1];
                    e.datecode = strRow[2];
                    e.description = strRow[3];
                    e.date_reported = strRow[4];
                    e.yesno = strRow[5];
                    e.blank = strRow[6];
                    e.url = strRow[7];
                    newEraiList.Add(e);
                }
            }



            //Check to see new list was filled with records
            if (newEraiList.Count <= 0)
                return;
            //Import the new lines - doing a separate SubMitChanges separately as a precaution in case for some reason the import fails, I won't delete what we already have.
            rdc.erai_imports.InsertAllOnSubmit(newEraiList);
            rdc.SubmitChanges();

            //Delete Current Records
            rdc.erai_imports.DeleteAllOnSubmit(existingEraiList);
            rdc.SubmitChanges();



            //Build confirmation email.
            string body = "<b>Import Successful.</b><b />";
            body += "Completed at: " + DateTime.Now.ToShortDateString() + "<br />";
            body += "Last Update from ERAI: " + fileCreationDate.ToShortDateString() + " " + fileCreationDate.ToShortTimeString() + "<br />";
            body += newEraiList.Count + " parts imported. " + "<br />";
            body += existingEraiList.Count + " parts truncated." + "<br />";
            tools.SendMail("erai_import@sensiblemicro.com", SystemLogic.Email.EmailGroupAddress.Systems, "ERAI Import was successful", body);
            //}

            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "ERAI Import completed successfully.");

        }
        catch (Exception ex)
        {
            string message = "ERAI Import Error: " + ex.Message;
            if (lastUpdatedDaysAgo < staleDays)
                message += "It has been more than " + lastUpdatedDaysAgo + " days since the last erai FTP upload.";
            SystemLogic.Logs.LogEvent(ex, message, true);
            tools.SendMail("erai_import@sensiblemicro.com", SystemLogic.Email.EmailGroupAddress.Systems, "ERAI Import was NOT successful", "Please Investigate. <br /><br /><b>Exception Message </b>: " + message + "<br /><b>Inner Exception: </b>" + ex.InnerException);
        }


    }





    private void CloseRzDeals(RzDataContext rdc)
    {
        try
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Closing Rz Deals");
            int count = 0;
            string closureReport = HubspotLogic.CloseRzDealsFromHubspot(rdc, out count);
            if (count <= 0)
                return;
            if (string.IsNullOrEmpty(closureReport))
                return;
            tools.SendMail("huspot_deal_closures@sensiblemicro.com", "marketing@sensiblemicro.com", "Hubspot API Deal Closure Report", closureReport);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Close Rz Deals processed successfully.");
        }
        catch (Exception ex)
        {
            string message = "Close Rz Deals Error: " + ex.Message;
            SystemLogic.Logs.LogEvent(ex, message, true);
        }
    }

    private async Task CheckSiliconExpertUsage(RzDataContext rdc)
    {
        try
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Checking Silicon Expert usage");

            //using (RzDataContext rdc = new RzDataContext())
            //{

            //Get the current Usage Stats from Silicon Expert.
            //silicon_expert se = new silicon_expert();
            //Instantiate the comparison objects
            SiliconExpertLogic.seUsageStatus currentUsage = await SiliconExpertLogic.GetUserStatusObjectAsync();
            //Save them to the Database
            if (currentUsage == null)
                return;

            //Build the dbml object from the SiliconExpert Api object for database save
            se_user_status current = new se_user_status();
            current.date_created = DateTime.Now;
            current.CreationDate = currentUsage.CreationDate;
            current.ExpirationDate = currentUsage.ExpirationDate;
            current.PartDetailLimit = currentUsage.PartDetailLimit;
            current.PartDetailCount = currentUsage.PartDetailCount;
            current.PartDetailRemaining = currentUsage.PartDetailRemaining;

            rdc.se_user_status.InsertOnSubmit(current);
            //If Database holds more than 1 entry, compare most recent to the prior            
            se_user_status previous = rdc.se_user_status.OrderByDescending(o => o.date_created).FirstOrDefault();

            bool usageSpikeDetected = false;
            int spikeDelta = 0;
            if (previous != null)
            {
                usageSpikeDetected = DetectUsageSpike(current, previous, out spikeDelta);
            }
            if (usageSpikeDetected)
            {
                //Enable the stop search flag
                //NOT IMPLEMENTED YET
                string ip = NetworkLogic.GetVisitorIpAddress();
                string body = "<b>Details:</b><br />";
                body += "<b>IP: </b>" + ip + " <br />";
                body += "<b>CurrentRemaining </b>" + current.PartDetailRemaining + "<br />";
                body += "<b>CurrentRemaining </b>" + current.PartDetailRemaining + "<br />";
                body += "<b>Delta </b>" + spikeDelta + "<br />";
                //tools.BlockIP(ip);
                string heading = "Silicon Expert Usage Spike Detected";
                tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, heading, body);
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, heading + " " + body);
            }

            //Commit changed at teh end, incase "EnableSearch" needs to be set false.
            rdc.SubmitChanges();

            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Silicon Expert Usage Spike detection completed successfully.");
            //}

        }
        catch (Exception ex)
        {
            string message = "Check Silicon Expert Usage Error: " + ex.Message;
            SystemLogic.Logs.LogEvent(ex, message, true);
        }

        //If the usage has increased more than 10% of the remaining available, send an email alert.
        //If there are less than 10% left of total searches, disable searches, notify admin.

    }

    private bool DetectUsageSpike(se_user_status current, se_user_status previous, out int delta)
    {

        //Since this is runnig every 5 minutes instead of 30, need to divide my threshold yby 6 (i.e. 5*6 = 30) so 100 /6  = 16.7, so, I'll say 17

        bool spikeDetected = false;

        //get the delta between the 2 numbers
        delta = previous.PartDetailRemaining - current.PartDetailRemaining;
        //one percent of our total 10000 is 100.
        //I want to know if that ever disappears in 1/2 hou
        //And since this check is done every 5 minutes, then 100/6 = 17
        if (delta >= 17)
            spikeDetected = true;
        return spikeDetected;
    }



    private void ManageAgingQuoteData(RzDataContext rdc)
    {
        try
        {
            //Void any sales orders older than X days that have no sale.
            //Mark any associated Hubspot Deals as "Close Lost", Reason, "Aged Quote" (or something).
            int count = 0;
            string htmlResults = "";
            //using (RzDataContext rdc = new RzDataContext())
            //{
            //using (RzDataContext rdc = new RzDataContext())
            SalesLogic.HandleAgedDealsRz(rdc, 30, null);
            //}
            string[] arrCC = new string[] { "marketing@sensiblemicro.com" }; // creates populated array of length 2
            string message = count + " Hubspot / Rz Deals aged via the API";
            tools.SendMail("aging_deals@sensiblemicro.com", "ktill@sensiblemicro.com", message, htmlResults, arrCC);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Aging Quote Data processed successfully.");
        }
        catch (Exception ex)
        {
            string message = "Aging Quote Data Error: " + ex.Message;
            SystemLogic.Logs.LogEvent(ex, message, true);
        }
    }

    private void RefreshHubspotData(RzDataContext rdc)
    {
        //After reading a great article by Rick Strahl, it seems best to use datacontext for each Atomic set of Database activities.  For expensive ones, I can set the Command Timeout.
        //https://weblog.west-wind.com/posts/2008/feb/05/linq-to-sql-datacontext-lifetime-management
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Refreshing Hubspot Engagements");
        string logMessage = "Results:" + Environment.NewLine;

        //Add new engagemetns to local database
        int added = 0;
        //using (RzDataContext rdc = new RzDataContext())
        //{
        //    //Extend Command on these expensive queries.  default 30.  Due to web API calls, this can take a while.
        //    rdc.CommandTimeout = 240;

        DateTime dtRefreshHubspotEngagents = DateTime.Now;


        HubspotLogic.RefreshHubspotEngagements(rdc, out added);
        TimeSpan tsRefreshHubspotEngagents = Tools.Dates.GetTimeSpan(dtRefreshHubspotEngagents, DateTime.Now);
        string strRefreshHubspotEngagents = " Hubspot Engagements Refreshed. Time: " + tsRefreshHubspotEngagents.Minutes + " : " + tsRefreshHubspotEngagents.Seconds + Environment.NewLine;


        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, strRefreshHubspotEngagents);
        logMessage += strRefreshHubspotEngagents;
        //}
        logMessage += added + " engagements added." + Environment.NewLine;

        //Update In PRogress No Dispo
        int updatedInProgress = 0;
        try
        {
            DateTime dtInProgressCalls = DateTime.Now;
            //using (RzDataContext rdc = new RzDataContext())
            HubspotLogic.UpdateInProgressCallDispositions(rdc, out updatedInProgress);
            TimeSpan tsInProgressCalls = Tools.Dates.GetTimeSpan(dtInProgressCalls, DateTime.Now);
            string strInProgressCalls = updatedInProgress + " 'In Progress' engagements updated. Time: " + tsInProgressCalls.Minutes + " : " + tsInProgressCalls.Seconds + Environment.NewLine;
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, strInProgressCalls);
            logMessage += strInProgressCalls;
        }
        catch (Exception exInProgress)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Hubspot Call InProgress Refresh Error: " + exInProgress.Message);
        }



        //Update Null Disposition
        int updatedNullDispo = 0;
        try
        {
            DateTime dtNullDispoStart = DateTime.Now;
            //using (RzDataContext rdc = new RzDataContext())
            HubspotLogic.UpdateCompletedNullDispositions(rdc, out updatedNullDispo, 0);
            TimeSpan tsNullDispo = Tools.Dates.GetTimeSpan(dtNullDispoStart, DateTime.Now);
            string strNullDispo = updatedNullDispo + " 'Null Disposition' engagements updated. Time: " + tsNullDispo.Minutes + " : " + tsNullDispo.Seconds + Environment.NewLine;
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, strNullDispo);
            logMessage += strNullDispo;

        }
        catch (Exception exDispo)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Hubspot Null Disposition Error: " + exDispo.Message);
        }



        //Fix Missing Recordings
        int fixedMissingRecordings = 0;
        try
        {
            DateTime dtMissingRecordings = DateTime.Now;
            //using (RzDataContext rdc = new RzDataContext())
            HubspotLogic.FixMissingRecordings(rdc, out fixedMissingRecordings, 0);
            TimeSpan tsMissingRecordings = Tools.Dates.GetTimeSpan(dtMissingRecordings, DateTime.Now);

            string strMissingRecordings = fixedMissingRecordings + " missing recordings fixed. Time: " + tsMissingRecordings.Minutes + " : " + tsMissingRecordings.Seconds + Environment.NewLine;
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, strMissingRecordings);
            logMessage += strMissingRecordings;
        }
        catch (Exception exRecordings)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Hubspot Missing Recordings Error: " + exRecordings.Message);
        }


        //Total
        int totalUpdated = updatedInProgress + updatedNullDispo + fixedMissingRecordings;
        logMessage += "Added " + added + ". Updated " + totalUpdated;
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Sucessfully refreshed Hubspot Data. " + logMessage);


    }




    private void Bounce()
    {
        Response.Clear();
        Response.StatusCode = 404;
        Response.End();
    }
}