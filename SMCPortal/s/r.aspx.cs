﻿using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class s_r : System.Web.UI.Page
{
    private web_rfq rfq = new web_rfq();


    private string key { get; set; }
    //Sanitized Variables
    private string Part { get; set; }
    private string Mfg { get; set; }
    private long Qty { get; set; }    
    private string ContactName { get; set; }
    private string ContactEmail { get; set; }
    private string CustomerName { get; set; }
    private string ContactPhone { get; set; }
    private string AffiliateID { get; set; }

    //Affiliate Customer Info
    private string strCustomerName { get; set; }
    private string strContactName { get; set; }
    private string strContactEmail { get; set; }
    private string strContactPhone { get; set; }
    private string strAffiliateID { get; set; }

    //RFQ
    private string strPart { get; set; }
    private string strMfg { get; set; }
    private string strQty { get; set; }





    protected void Page_Load(object sender, EventArgs e)
    {
        //Check Querystrings for RFQ
        //only accept input with proper "key"
        //key == "fnb742bd3h2j8s56updbuicx9laovhpqgun8-565"
        //sample Url = /s/r.aspx?nrbbvu=fnb742bd3h2j8s56updbuicx9laovhpqgun8-565&p=sfdhjgbsalhbesaigyubseyhg&m=asjfbghjsdaujbgvlsedb&q=123244658798&e=fsdbnhsifbg@gmail.com&p=23489573489563489&a=dueishgjfhserp89574y895
        //validate ALL input
        string queryString = Tools.Strings.SanitizeInput(Request.QueryString["nrbbvu"]);
        //Confirm key sent
        if (string.IsNullOrEmpty(queryString))
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, "Access querystring not provided.");
            Bounce();
        }
        //Confirm Corret Key Sent
        if (queryString != "fnb742bd3h2j8s56updbuicx9laovhpqgun8-565")
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, "Invalid access querystring. (" + queryString + ")");
            Bounce();
        }

        SanitizeInputs();
        ValidateInputs();
        LogRfqToDataBase();
        SendNotificationEmail();
        CreateRzObjects();








    }

    private void CreateRzObjects()
    {
        CreateOrderBatch();
    }

    private void CreateOrderBatch()
    {
        //Need to adopt the same logic from Portal Quotes here.  Refactor it into DAL
    }

    private void SendNotificationEmail()
    {
        try
        {
            string subject = "New Web RFQ: " + Part;
            string body = "<b>A new req has been captured:</b><br />";
            body += "Part: " + Part + "<br />";
            body += "Mfg: " + Mfg + "<br />";
            body += "Qty: " + Qty + "<br />";
            body += "Email: " + ContactEmail + "<br />";
            body += "Phone: " + ContactPhone + "<br />";
            body += "AffiliateID: " + AffiliateID + "<br />";
            body += "Customer Name: " + CustomerName + "<br />";
            body += "Contact Name: " + ContactName + "<br />";
            body += "Contact Email: " + ContactEmail + "<br />";
            string to = "web_rfq@sensiblemicro.com";
            string from = "web_rfq@sensiblemicro.com";

            SystemLogic.Email.SendMail(from, to, subject, body);
        }
        catch (Exception ex)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "web_rfq Error: " + ex.Message);
        }




    }

    private void LogRfqToDataBase()
    {
        
        rfq.part_number = Part;
        rfq.manufacturer = Mfg;
        rfq.quantity = Qty;
        rfq.email = ContactEmail;
        rfq.phone = ContactPhone;
        rfq.affiliate_id = AffiliateID;
        rfq.customer_name = CustomerName;
        rfq.contact_name = ContactName;
        rfq.contact_email = ContactEmail;



        rfq.date_created = DateTime.Now;
        using (RzDataContext rdc = new RzDataContext())
        {
            rdc.web_rfqs.InsertOnSubmit(rfq);
            rdc.SubmitChanges();
        }

    }

    private void SanitizeInputs()
    {
        //Decode and Sanitize querystring inputs.
        strPart = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["p"]));
        strMfg = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["m"]));
        strQty = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["q"]));
        strCustomerName = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["c"]));
        strContactName = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["cn"]));
        strContactEmail = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["e"]));
        strContactPhone = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["h"]));
        strAffiliateID = Tools.Strings.SanitizeInput(HttpUtility.HtmlDecode(Request.QueryString["a"]));



    }
    private void ValidateInputs()
    {
        //Check for missing required inputs   
        //Part
        if (string.IsNullOrEmpty(strPart))
            return;
        Part = strPart;


        //Checking Null for all below sinec if the Sanitization fails, then these values would be null.

        //MFG 
        if (!string.IsNullOrEmpty(strMfg))
            Mfg = strMfg ?? "";

        //Confirm Valid Quantity
        long lngQty = 0;
        if (!long.TryParse(strQty, out lngQty))
            return;
        Qty = lngQty;

        //Contact Information
        //Must have at least ONE form of contact, email, phone, or affiliateid
        if (string.IsNullOrEmpty(strContactEmail) && string.IsNullOrEmpty(strContactPhone) && string.IsNullOrEmpty(strAffiliateID))
            return;
        if (!string.IsNullOrEmpty(strCustomerName))
            CustomerName = strCustomerName;
        if (!string.IsNullOrEmpty(strContactName))
            ContactName = strContactName;
        if (!string.IsNullOrEmpty(strContactEmail))
            ContactEmail = strContactEmail;
        if (!string.IsNullOrEmpty(strContactPhone))
            ContactPhone = strContactPhone;
        if (!string.IsNullOrEmpty(strAffiliateID))
            AffiliateID = strAffiliateID;
        //Confirm Valid Email Address
        if (!string.IsNullOrEmpty(strContactEmail))
            if (Tools.Email.IsEmailAddress(strContactEmail))
                ContactEmail = strContactEmail;
    }



    private void Bounce()
    {
        Response.Clear();
        Response.StatusCode = 404;
        Response.End();
    }
}