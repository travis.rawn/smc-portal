﻿using System;
using SensibleDAL.dbml;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_logs_sm_logging : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadGrid();
    }

    private void LoadGrid()
    {
        using (SeriLogDataContext sdc = new SeriLogDataContext())
        {
            var query = sdc.Logs.OrderByDescending(o => o.TimeStamp).Take(1000).ToList();
            smdt_serilog.dataSource = query;
            smdt_serilog.dateTimeFormat = "'M/D/YYYY h:mm:ss A'";
            smdt_serilog.loadGrid();
        }
    }
}