﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="maintenance_dashboard.aspx.cs" Inherits="secure_admin_maintenance_maintenance_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Testing Dashboard</h1>
        </div>        
    </div>
    <div class="card">
        <div class="sm-list">
            <ul>
                <li>
                    <a href="/secure_admin/maintenance/unit_tests.aspx" target="_blank">
                        <label>Unit Tests</label>
                        <p>Portal Unit Tests</p>
                    </a>
                </li>
                <li>
                    <a href="/secure_admin/integrations/hubspot/hubspot_api_admin.aspx" target="_blank">
                        <label>Tools</label>
                        <p>Administration Tools.</p>
                    </a>
                </li>               
                <li>
                    <a href="/secure_admin/sandbox/admin_sandbox.aspx" target="_blank">
                        <label>Sandbox</label>
                        <p>Play in the sandbox.</p>
                    </a>
                </li>
                <li>
                    <a href="/secure_admin/maintenance/maintenance.aspx" target="_blank">
                        <label>Maintenance</label>
                        <p>Maintenance Operations</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

