﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="TestingTools.aspx.cs" Inherits="secure_admin_TestingTools" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeadContent" runat="Server">
    <h1 class="page-header">Testing Tools</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script>
                $(function () {
                    $("#dialog").dialog();
                });
            </script>
            <div>
                <div style="width: 30%; float: left;">
                    <asp:Button ID="Submit1" runat="server"
                        CommandArgument="1" OnClick="Submit_Click"
                        Text="Button 1" /><br />
                    <p>
                        Click this button to create an InvalidOperationException.<br />
                        Page_Error will catch this and redirect to GenericErrorPage.aspx.
                
                    </p>
                    <asp:Button ID="Submit2" runat="server"
                        CommandArgument="2" OnClick="Submit_Click"
                        Text="Button 2" /><br />
                    <p>
                        Click this button to create an ArgumentOutOfRangeException.<br />
                        Page_Error will catch this and handle the error.
                
                    </p>
                    <asp:Button ID="Submit3" runat="server" CommandArgument="3" OnClick="Submit_Click" Text="Button 3" /><br />
                    <p>
                        Click this button to create a generic Exception.<br />
                        Application_Error will catch this and handle the error.
                
                    </p>
                    <asp:Button ID="Submit4" runat="server"
                        CommandArgument="4" OnClick="Submit_Click"
                        Text="Button 4" /><br />
                    <p>
                        Click this button to create an HTTP 404 (not found) error.<br />
                        Application_Error will catch this 
      and redirect to HttpErrorPage.aspx.
                
                    </p>

                    <asp:Button ID="Submit5" runat="server"
                        CommandArgument="5" OnClick="Submit_Click"
                        Text="Button 5" /><br />
                    <p>
                        Click this button to create an HTTP 404 (not found) error.<br />
                        Application_Error will catch this 
      but will not take any action on it, and ASP.NET
      will redirect to Http404ErrorPage.aspx. 
      The original exception object will not be
      available.
                
                    </p>
                    <asp:Button ID="Button1" runat="server"
                        CommandArgument="6" OnClick="Submit_Click"
                        Text="Button 6" /><br />
                    <p>
                        Click this button to create an HTTP 400 (invalid url) error.<br />
                        Application_Error will catch this 
      but will not take any action on it, and ASP.NET
      will redirect to DefaultRedirectErrorPage.aspx. 
      The original exception object will not
      be available.
                
                    </p>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

