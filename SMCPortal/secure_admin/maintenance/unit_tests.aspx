﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="unit_tests.aspx.cs" Inherits="secure_admin_tests_function_tests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnValidUser">
         <div id="pageTitle">
        <h1>Unit Tests</h1>
    </div>
        <div class="container" style="text-align: center">
            <div style="height: 50px;">
                <h2>
                    <asp:Label ID="lblResult" runat="server"></asp:Label>
                </h2>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label>Valid Portal User Test</label>
                            </div>
                            <div class="panel-body">
                                <asp:TextBox ID="txtValidUser" runat="server" CssClass="form-control" Placeholder="Email:"></asp:TextBox><br />
                                <asp:Button ID="btnValidUser" runat="server" Text="Test Valid User" OnClick="btnValidUser_Click" CssClass="btn btn-smc btn-smc-primary btn-block" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <h3>Email Tests</h3>
                    <asp:LinkButton ID="lbSendEmail" runat="server" OnClick="lbSendEmail_Click" CssClass="btn-smc btn-smc-primary">Test Email</asp:LinkButton>
                    
                </div>
                <div class="col-sm-3"></div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

