﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_tests_function_tests : System.Web.UI.Page
{
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnValidUser_Click(object sender, EventArgs e)
    {
        bool validUser = false;
        string email = txtValidUser.Text;
        if (string.IsNullOrWhiteSpace(email))
            email = "''";
        validUser = SM_Security.IsValidPortalEmail(txtValidUser.Text.Trim().ToLower());
        if (validUser)
            HandleResult(email + " is a valid user.");
        else
            HandleResult(email + " is NOT A valid user.", false);

    }






    protected void HandleResult(string message, bool success = true)
    {
        if (success)
            lblResult.Attributes.Add("class", "label label-success");
        else
            lblResult.Attributes.Add("class", "label label-danger");
        lblResult.Text = message;
    }

    protected void lbSendEmail_Click(object sender, EventArgs e)
    {
        try
        {
            tools.SendMail("test@sensiblemicro.com", "ktill@sensiblemicro.com", "Portal Email Test", "<b>This is a test!</b><br /> Did it work?");
            tools.HandleResult("success", "Email sent!");
        }
        catch(Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
        
           
       
    }
}