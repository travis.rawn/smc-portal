﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="maintenance.aspx.cs" Inherits="secure_admin_maintenance" %>

<%@ Register Src="~/assets/controls/InspectionImageGallery.ascx" TagPrefix="uc1" TagName="InspectionImageGallery" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="col-sm-12">
            <div class="well well-sm">
                Confirm existence of all Inspection photos, report on anything missing
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            Image Maintenance  
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <asp:LinkButton ID="lbCreateMissingIDEAImages" runat="server" OnClick="lbCreateMissingIDEAImages_Click" CssClass="hs-button primary small">Create IDEA Images From Database</asp:LinkButton>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbCreateMissingGCATImages" runat="server" OnClick="lbCreateMissingGCATImages_Click" CssClass="hs-button primary small">Create GCAT Images From Database</asp:LinkButton>
        </div>       
    </div>
      <div class="row">
        <div class="col-sm-4">
              <asp:LinkButton ID="lbconfirmImages" runat="server" OnClick="lbconfirmImages_Click" CssClass="hs-button primary small">Confirm Filepath Images</asp:LinkButton>
        </div>
        <div class="col-sm-4">
         <asp:LinkButton ID="lbExportRzAttachments" runat="server" OnClick="lbExportRzAttachments_Click" CssClass="hs-button primary small">Export Rz Attachments</asp:LinkButton>
        </div>
        <div class="col-sm-4">
          
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            Tests            
            <asp:Label ID="lblPath" runat="server" Text="Path:"></asp:Label>
            <uc1:InspectionImageManagerControl runat="server" ID="iic1" />

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

