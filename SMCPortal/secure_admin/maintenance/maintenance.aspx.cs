﻿using System;
using SensibleDAL;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.ef;

public partial class secure_admin_maintenance : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        iic1.InspectionID = 4;
        iic1.InspectionType = "test";
    }

    protected void lbconfirmImages_Click(object sender, EventArgs e)
    {

    }

    protected void lbCreateMissingIDEAImages_Click(object sender, EventArgs e)
    {
        CreateMissingImages(SM_Enums.InspectionType.idea);
    }

    protected void lbCreateMissingGCATImages_Click(object sender, EventArgs e)
    {
        CreateMissingImages(SM_Enums.InspectionType.gcat);
    }

    private void CreateMissingImages(SM_Enums.InspectionType inspType)
    {
        try
        {

            string Report = "<strong>" + inspType.ToString().ToUpper() + " Results:</strong><br />";
            long totalImagesCreated = 0;
            long count = 0;
            int index = 0;
            int take = 100;

            using (sm_binaryEntities smb = new sm_binaryEntities())
            {
                var query = smb.insp_images.Where(w => (w.img_path_web == null || w.img_path_web == "") && w.insp_type.ToUpper() == inspType.ToString().ToUpper() && w.insp_id > 0);//.ToList() closes the reader.
                count = query.Count();
            }
            //List<insp_images> imgList = new List<insp_images>();
            List<insp_images> databaseImagesList = new List<insp_images>();

            while (index < count)
            {
                string result = "";
                CreateImagesFromDataBase(inspType, index, take, out result);
                index += take;
                Report += result;
            }

            Report += " <br /><strong>Summary: </strong><br />";
            Report += "Total Images Created: " + totalImagesCreated + " <br />";
            SystemLogic.Email.SendMail("images@sensiblemicro.com", "systems@sensiblemicro.com", "Image Creation Report", Report);
            tools.HandleResult("success", "Done!!");
        }

        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void CreateImagesFromDataBase(SM_Enums.InspectionType inspType, int index, int take, out string report)
    {
        //Loop through the images, if no imp_path_web, create the image and set it.
        string imageDumpResult = "";
        report = "";

        List<insp_images> imageList = new List<insp_images>();
        using (sm_binaryEntities smb = new sm_binaryEntities())
        {
            imageList = smb.insp_images.Where(w => (w.img_path_web == null || w.img_path_web == "") && w.insp_type.ToUpper() == inspType.ToString().ToUpper() && w.insp_id > 0).OrderBy(o => o.unique_id).Skip(index).Take(take).ToList();//.ToList() closes the reader.

            foreach (insp_images i in imageList)
            {

                index++;
                //If path already exists, continue through loop.
                if (!string.IsNullOrEmpty(i.img_path_web))
                {
                    //Note in report
                    imageDumpResult = "Path Exists, Skipping: " + i.insp_id + " Path: " + i.img_path_web;
                    report += imageDumpResult + "<br />";
                    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, imageDumpResult, false);
                    continue;
                }
                string saveReport = "";
                //Perform the Database conversion logic
                i.img_path_web = BinaryLogic.SaveDataBaseImageToFolder(i, out saveReport) ?? "";// Don't want to save nulls.
                report += saveReport;
                if (string.IsNullOrEmpty(i.img_path_web))
                {
                    report += "Unable to save image for " + i.insp_type + " ID: " + i.insp_id + " no path could be derived.";
                    continue;
                }

                if (!string.IsNullOrEmpty(i.img_path_web))
                {
                    //Create result string
                    imageDumpResult = "File successfully created from DataBase: " + i.insp_id + " Path: " + i.img_path_web;
                    //Note in the Report
                    report += imageDumpResult + "<br />";
                    //Log success event
                    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, imageDumpResult, false);

                }
                //Commit the database  
                smb.SaveChanges();
            }
        }
    }










    protected void lbExportRzAttachments_Click(object sender, EventArgs e)
    {
        //DateTime startTime = DateTime.Now;
        //DateTime endTime;
        //TimeSpan opTime = new TimeSpan();
        //try
        //{

        //    string fileType = ".jpg";
        //    int successCount = 0;
        //    ExportRzAttachments(fileType, out successCount);
        //    endTime = DateTime.Now;
        //    string timeMsg = GetTimeMessage(opTime, startTime, endTime);
            
        //    tools.HandleResult("success", "Success!  Total Rz Attachments Exported: " + successCount.ToString() + "<br /><br />" + timeMsg, 0);
        //    SensibleDAL.SystemLogic.Email.SendMail("rz_attachement@sensiblemicrocom", "systems@sensiblemicro.com", "Attachment Export Success", "All requested exports have succeeded. <br />"+timeMsg);
        //}
        //catch (Exception ex)
        //{
        //    endTime = DateTime.Now;
        //    string timeMsg = GetTimeMessage(opTime, startTime, endTime);
        //    tools.HandleResult("fail", ex.Message +"<br /><br />"+timeMsg);

        //}

    }

    //private string GetTimeMessage(TimeSpan opTime, DateTime startTime, DateTime endTime)
    //{
    //    opTime = endTime - startTime;
    //    string totalMinutes = opTime.TotalMinutes.ToString();
    //    string totalSeconds = opTime.TotalSeconds.ToString();
    //    string timeMsg = "Start Time: " + startTime.ToShortTimeString() + " End Time: " + endTime.ToShortTimeString() + "Total Minutes: " + totalMinutes.ToString();
    //    return timeMsg;
    //}

    //private void ExportRzAttachments(string fileType, out int successCount)
    //{
    //    successCount = 0;
    //    string logPath = "\\\\storage\\sm_storage\\rz_attachments\\log.txt";
    //    //Create a Log File for Review
    //    string now = DateTime.Now.ToString();
    //    now = now.Replace('/', '_');
    //    now = now.Replace(' ', '_');
    //    now = now.Replace(':', '_');
    //    using (StreamWriter sw = File.AppendText(logPath + now + ".txt"))
    //    {
    //        //    try
    //        //    {
    //        sw.Write("Export Rz Attachment Results:" + Environment.NewLine);
    //        long count = 0;
    //        int index = 0;
    //        int take = 300000;

    //        using (rz3_picturesEntities rzp = new rz3_picturesEntities())
    //        {
    //            var query = rzp.partpictures.Where(w => (w.file_path == null || w.file_path == ""));//.ToList() closes the reader.
    //            count = query.Count();
    //        }
    //        sw.Write("Total Count: " + count);
    //        //List<insp_images> imgList = new List<insp_images>();
    //        //List<partpicture> attachmentList = new List<partpicture>();


    //        //while (index < count)
    //        //{
    //        //string result = "";
    //        while (successCount < take)
    //        {
    //            int batchSuccessCount = 0;
    //            // Do it in batches of 100, else Database Timeout.
    //            int batchSize = 100;
    //            ExportAttachmentsFromDatabase(sw, index, batchSize, fileType, out batchSuccessCount);

    //            index += batchSize;
    //            successCount += batchSuccessCount;
    //            //sw.Write(result);
    //        }


    //        //}

    //        sw.Write(" <br /><strong>Summary: </strong><br />" + Environment.NewLine);
    //        sw.Write("Total Images Created: " + successCount + " <br />" + Environment.NewLine);



    //        //SystemLogic.Email.SendMail("images@sensiblemicro.com", "systems@sensiblemicro.com", "Image Creation Report");
    //        sw.Write("Done.");
    //        //tools.HandleResult("success", "Done!!");
    //        //}

    //        //catch (Exception ex)
    //        //{
    //        //    string error = ex.Message;
    //        //    if (ex.InnerException != null)
    //        //        error += ex.InnerException.Message;
    //        //    sw.Write("Error: " + error + "");
    //        //    //tools.HandleResult("fail", error);
    //        //}
    //    }
    //}

    //private void ExportAttachmentsFromDatabase(StreamWriter sw, int index, int take, string fileType, out int successCount)
    //{
    //    //Loop through the images, if no imp_path_web, create the image and set it.

    //    successCount = 0;
    //    List<partpicture> attachmentList = new List<partpicture>();
    //    using (rz3_picturesEntities rzp = new rz3_picturesEntities())
    //    {
    //        ///THis is a VERY long running query

    //        rzp.Database.CommandTimeout = 240;
    //        sw.Write("Setting Database Command Timeout to " + rzp.Database.CommandTimeout.ToString() + Environment.NewLine);
    //        //attachmentList = rzp.partpictures.Where(w => (w.file_path == null || w.file_path == "")).OrderBy(o => o.date_created).Skip(index).Take(take).ToList();//.ToList() closes the reader.
    //        attachmentList = rzp.partpictures.Where(w => w.file_path == null && w.filetype == fileType).OrderBy(o => o.date_created).Skip(index).Take(take).ToList();//.ToList() closes the reader.
    //        int fileCount = 0;
    //        //Loop Through each partrecord and create the file path copy.
    //        foreach (partpicture p in attachmentList)
    //        {
    //            sw.Write(Environment.NewLine + "Creating file " + fileCount + Environment.NewLine);

    //            //If path already exists, continue through loop.
    //            if (!string.IsNullOrEmpty(p.file_path))
    //            {
    //                sw.Write("Path Exists, Skipping: " + p.unique_id + " Path: " + p.file_path + Environment.NewLine);
    //                throw new Exception("Critical: Query should be omitting empty or null file paths.  We should not have a blank file path here.");
    //            }

    //            //Perform the Database conversion logic
    //            p.file_path = tools.SaveDataBaseAttachmentToFolder(sw, p) ?? "";// Don't want to save nulls.
    //            if (string.IsNullOrEmpty(p.file_path))
    //                throw new Exception("Error: Invalid File Path.");
    //            sw.Write("Successfully created file: " + p.file_path + Environment.NewLine);


    //            //Commit the database      
    //            sw.Write("Saving path to Database." + Environment.NewLine);
    //            rzp.SaveChanges();

    //            //Create result string
    //            string message = "";
    //            message = "File successfully created file " + fileCount + " FileName:" + p.filename + " Path: " + p.file_path + Environment.NewLine;
    //            sw.Write(message + Environment.NewLine);
    //            successCount++;

    //            //Log success event
    //            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, message, false);
    //            fileCount++;

    //        }
    //    }
    //}

}
