﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnKillSwitch_Click(object sender, EventArgs e)
    {
        string appPath = Request.PhysicalApplicationPath;
        string filename = "app_online.htm";
        string oldPath = Path.Combine(appPath, filename);
        try
        {
            if (File.Exists(oldPath))
            {
                string newfilename = "app_offline.htm";
                string newPath = Path.Combine(appPath, newfilename);
                File.Copy(oldPath, newPath);
                string message = "Done. " + filename + " changed to " + newfilename;
                ClientScript.RegisterStartupScript(GetType(), "FileChanged", "alert('" + message + "');", true);
                Response.Redirect("/", false);
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "NotFound", "alert('Not Found.');", true);
            }
        }
        catch(Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "Error", "alert('"+ex.Message+"');", true);
        }
    }      
    
}