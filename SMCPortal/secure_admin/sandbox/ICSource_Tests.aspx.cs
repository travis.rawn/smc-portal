﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using System.Data;
using System.Text;
using System.Globalization;
using SensibleDAL;

public partial class secure_admin_sandbox_ICSource_Tests : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    List<string> testPartsList = new List<string>();
    List<SensibleDAL.dbml.icsource_search> resultList = new List<SensibleDAL.dbml.icsource_search>();
    int resultCount = 0;



    protected void Page_Load(object sender, EventArgs e)
    {      
        SearchPartsList();

    }

    private void SearchPartsList()
    {
       
       
        DateTime startTime = DateTime.Now;        
        string testPart = "at45db";
        string testUrl = @"http://xmlsearch.icsource.com/xmlsearch.asmx/SearchInventory?User=sensible&Key=SOFYqmEd18&Part=" + testPart + "&SearchType=1&Mfg=Atmel&DateCode=1012&DateCodeCompareType=1&MinQuantity=1000&StockFlag=2&PricesOnly=False&PreferredVendorsOnly=False&Region=0&MaxResults=10&PageNum=1&Sort=Price DESC";
       
        //string result = Get(testUrl);   
        
        ConvertXmlToPartObject(testUrl);
        resultCount = resultList.Count();
        string message = "Search Complete.  Total Results " + resultCount.ToString() + "<br /><br />";
        //foreach(SensibleDAL.dbml.icsource_search ics in resultList)
        //{
        //    message += "Part: " + ics.PartNumber + "<br />";
        //}
        var query = resultList.Select(s => new {

            Part = s.PartNumber,
            Mfg = s.Mfg,
            DateCode = s.DateCode,
            QTY = s.Quantity,            

        }).ToList();
        smdt.dataSource = query;
        //smdt.showCheckboxes = true;
        smdt.loadGrid();


        DateTime endTime = DateTime.Now;
        TimeSpan duration = endTime - startTime;
        message += "<br /><br />" + "Duration: "+duration.Seconds;
        //How to get this into an IQueryable object so I can fill a GridView
        //tools.HandleResult(Page, "success", message);
    }

    private void ConvertXmlToPartObject(string url)
    {       
        var request = WebRequest.Create(url) as HttpWebRequest;
        var response = request.GetResponse();

        Stream receiveStream = response.GetResponseStream();
        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

        var result = readStream.ReadToEnd();


        var xmldoc = new XmlDocument();
        xmldoc.LoadXml(result);

        XmlNode root = xmldoc.ChildNodes[1].ChildNodes[1].FirstChild;


        int count = 0;
        //Display the contents of the child nodes.
        if (root.HasChildNodes)
        {
            using (SensibleDAL.dbml.RzDataContext rdc = new SensibleDAL.dbml.RzDataContext())
            {
                foreach (XmlNode node in root.ChildNodes)
                {
                    string ip_address = NetworkLogic.GetVisitorIpAddress();
                    SensibleDAL.dbml.icsource_search ics = new SensibleDAL.dbml.icsource_search();
                    ics.ip_address = ip_address;
                    ics.Contact = node["Contact"].InnerText ?? "";
                    ics.Phone = node["Phone"].InnerText ?? "";
                    ics.Fax = node["Fax"].InnerText ?? "";
                    ics.Email = node["Email"].InnerText ?? "";
                    ics.Address1 = node["Address1"].InnerText ?? "";
                    ics.Address2 = node["Address2"].InnerText ?? "";
                    ics.City = node["City"].InnerText ?? "";
                    ics.State = node["State"].InnerText ?? "";
                    ics.PostalCode = node["PostalCode"].InnerText ?? "";
                    ics.CountryName = node["CountryName"].InnerText ?? "";
                    ics.Quantity = Convert.ToInt32(node["Quantity"].InnerText);

                    //DateTime comes through like this: 2019-02-19T08:00:25.473-05:00- notoe the "T" before the time
                    string strTimeStamp = node["LastUpdated"].InnerText;
                    strTimeStamp = strTimeStamp.Replace("T", " ");
                    DateTime timestamp = DateTime.Parse(strTimeStamp);
                    ics.LastUpdated = timestamp;
                    ics.PartNumber = node["PartNumber"].InnerText ?? "";
                    ics.Mfg = node["Mfg"].InnerText.ToUpper() ?? "";
                    ics.DateCode = node["DateCode"].InnerText ?? "";
                    ics.Price = node["Price"].InnerText ?? "";
                    ics.Comment = node["Comment"].InnerText ?? "";
                    ics.IsStock = Convert.ToInt32(node["IsStock"].InnerText);
                    ics.IsPreferredVendor = Convert.ToBoolean(node["IsPreferredVendor"].InnerText);
                    ics.CompanyName = node["CompanyName"].InnerText ?? "";                    
                    ics.CompanyID = Convert.ToInt32(node["CompanyID"].InnerText);            
                    ics.InventoryID = Convert.ToInt64(node["InventoryID"].InnerText);
                    count++;
                    resultList.Add(ics);
                }
            }


        }
       
    }

    

    public string Get(string uri)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        using (Stream stream = response.GetResponseStream())
        using (StreamReader reader = new StreamReader(stream))
        {
            return reader.ReadToEnd();
        }
    }
}