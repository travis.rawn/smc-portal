﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;

public partial class secure_admin_sandbox_ImageUpload_MYSQL_GridView : System.Web.UI.Page
{
    //string MySQLconstr = ConfigurationManager.ConnectionStrings["MYSQL_ConnectionString"].ConnectionString;
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        //divError.Visible = false;
        if (!IsPostBack)
        {
            BindHeaderImageGrids();
        }
    }

    private void BindHeaderImageGrids()
    {

        BindGrid(gvHeaderImageTop);
        BindGrid(gvHeaderImageBot);
        BindGrid(gvHeaderImageOther);

    }


    private void BindGrid(GridView gv)
    {
        string SectionID = gv.ID.Substring(2);
        //using (MySqlConnection con = new MySqlConnection(MySQLconstr))
        //{
        //    using (MySqlCommand cmd = new MySqlCommand())
        //    {
        //        cmd.CommandText = "SELECT unique_id, img_path, img_type, img_blob FROM insp_images WHERE insp_section_id= '"+SectionID+"'";
        //        cmd.Connection = con;
        //        using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
        //        {
        //            DataTable dt = new DataTable();
        //            sda.Fill(dt);
        //            gv.DataSource = dt;
        //            gv.DataBind();
        //        }
        //    }
        //}
    }

    protected void UploadFile(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string SectionID = btn.ID.Substring(3);       
        FileUpload fu = (FileUpload)Page.Master.FindControl("MainContent").FindControl("fu"+SectionID);
        GridView gv = (GridView)Page.Master.FindControl("MainContent").FindControl("gv"+SectionID);

        string filename = Path.GetFileName(fu.PostedFile.FileName);
        string contentType = fu.PostedFile.ContentType;
        using (Stream fs = fu.PostedFile.InputStream)
        {
            using (BinaryReader br = new BinaryReader(fs))
            {
                byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                //using (MySqlConnection con = new MySqlConnection(MySQLconstr))
                //{
                //    string query = "INSERT INTO insp_images (img_path, img_type, img_blob, img_description, insp_id, insp_section_id, insp_image_id) VALUES(@img_path, @img_type, @img_blob, @img_description, @insp_id, @insp_section_id, @insp_image_id); ";

                //    try {
                //        using (MySqlCommand cmd = new MySqlCommand(query))
                //        {
                //            cmd.Connection = con;


                //            cmd.Parameters.Clear();
                //            //cmd.Parameters.AddWithValue("@img_path", ImageJobs[i].FinalPath);
                //            cmd.Parameters.AddWithValue("@img_path", filename);
                //            //cmd.Parameters.AddWithValue("@img_type", ImageJobs[i].ResultFileExtension);
                //            cmd.Parameters.AddWithValue("@img_type", contentType);
                //            cmd.Parameters.AddWithValue("@img_blob", bytes);


                //            //cmd.Parameters.AddWithValue("@img_description", HeaderImageTextBox.Text);
                //            cmd.Parameters.AddWithValue("@img_description", "img_description");
                //            //cmd.Parameters.AddWithValue("@insp_id", insp_id);
                //            cmd.Parameters.AddWithValue("@insp_id", 1);
                //            //cmd.Parameters.AddWithValue("@insp_section_id", SectionID);
                //            cmd.Parameters.AddWithValue("@insp_section_id", SectionID);
                //            //if (ImageID == null)
                //            //    myCmd.Parameters.AddWithValue("@insp_image_id", Guid.NewGuid());
                //            //else
                //            //    myCmd.Parameters.AddWithValue("@insp_image_id", ImageID);
                //            //cmd.Parameters.AddWithValue("@insp_image_id", "insp_image_id");
                //            cmd.Parameters.AddWithValue("@insp_image_id", "insp_image_id");

                //            con.Open();
                //            cmd.ExecuteNonQuery();
                //            con.Close();
                //            Response.Write("<script>alert('Nailed it!  Image Uploaded!');</script>");
                           
                //            BindGrid(gv);
                //        }
                //    }
                //    catch(Exception ex)
                //    {
                //        Response.Write("<script>alert('"+ ex.Message+"');</script>"); 
                //    }
                //}
            }
        }
        //Response.Redirect(Request.Url.AbsoluteUri,false);
    }


    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            byte[] bytes = (byte[])(e.Row.DataItem as DataRowView)["img_blob"];
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            (e.Row.FindControl("Image1") as Image).ImageUrl = "data:image/png;base64," + base64String;
        }
    }
}