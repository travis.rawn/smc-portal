﻿using System;


public partial class secure_admin_sandbox_datetime_picker_tests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPickerOptions();
    }

    private void LoadPickerOptions()
    {

        DateTime testDate = new DateTime(2021, 5, 4, 1, 7, 2, DateTimeKind.Local);
        dtpDate.SelectedDate = testDate;



    }
}