﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_nonconformance_selector : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    //Needs to be golobal to work with current Control Indexing logic
    int controlID = 1;

    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreInit(e);
        AddCheckboxObjects();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void AddCheckboxObjects()
    {
        //Use this list to loop through and set common settings, like autopostback, en-masse.
        List<assets_controls_sm_checkbox> cbList = new List<assets_controls_sm_checkbox>();
        List<string> checkboxList = new List<string>() { "checkbox 1", "checkbox 2", "checkbox 3", "checkbox 4", };
        List<string> sectionList = new List<string>() { "cat1Sub1Checkboxes", "cat1Sub2Checkboxes", "cat1Sub3Checkboxes", "cat1Sub4Checkboxes" };

        foreach (string divID in sectionList)
        {
            Control checkboxDiv = this.Master.FindControl("MainContent").FindControl(divID);
            foreach (string s in checkboxList)
                checkboxDiv.Controls.Add(CreateCheckboxControl(s));


        }
    }

    private assets_controls_sm_checkbox CreateCheckboxControl(string checkboxText)
    {
        assets_controls_sm_checkbox ret = (assets_controls_sm_checkbox)Page.LoadControl("~/assets/controls/sm_checkbox.ascx");
        ret.theText = checkboxText;
        ret.autoPostBack = false;
        ret.ID = controlID.ToString();
        controlID++;
        return ret;
    }

    protected void SaveCheckboxValues()
    {
        List<assets_controls_sm_checkbox> listCbx = new List<assets_controls_sm_checkbox>();
        string resultText = "";

        tools.GetControlList(Master.FindControl("MainContent").Controls, listCbx);
        foreach (assets_controls_sm_checkbox cb in listCbx)
        {
            cb.theHiddenField.Value = (!cb.isChecked).ToString();
            if (cb.isChecked)
            {
                if (String.IsNullOrEmpty(resultText))
                    resultText = "The following boxes were checked: ";
                resultText += "<br /> Checkbox " + cb.ID;
            }

        }
        if (String.IsNullOrEmpty(resultText))
            tools.HandleResult("fail", "Nothing checked");
        else
            tools.HandleResult("success", resultText);
    }

    protected void lbSave_Click(object sender, EventArgs e)
    {
        SaveCheckboxValues();
    }


}