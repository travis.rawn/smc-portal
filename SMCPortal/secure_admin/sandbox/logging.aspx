﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="logging.aspx.cs" Inherits="secure_admin_sandbox_logging" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
    <h1>Logging</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="card">
        <asp:LinkButton ID="logBtn" runat="server" OnClick="logBtn_Click">Log Something:</asp:LinkButton>
    </div>
    <div class="card">
        <asp:LinkButton ID="logError" runat="server" OnClick="logError_Click">Log An Error:</asp:LinkButton>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" Runat="Server">
</asp:Content>

