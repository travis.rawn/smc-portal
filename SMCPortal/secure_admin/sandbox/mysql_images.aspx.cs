﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;
using ImageResizer;
using ImageResizer.ExtensionMethods;
using System.Web;
using System.Collections.Generic;
using System.Text;

public partial class secure_admin_sandbox_mysql_images : System.Web.UI.Page
{
    List<ImageJob> ImageList = new List<ImageJob>();

    string constr = ConfigurationManager.ConnectionStrings["MYSQL_ConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindGrid();
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        ResizeAndSave();
        DeleteTempFile();
    }

    protected void ResizeAndSave()
    {
        try
        {

            //Loop through each uploaded file
            HttpFileCollection hfc = Request.Files;
            for (int i = 0; i < hfc.Count; i++)
            {
                HttpPostedFile hpf = hfc[i];
                if (hpf.ContentLength <= 0) continue; //Skip unused file controls.
                                                      //The resizing settings can specify any of 30 commands.. See http://imageresizing.net for details.
                                                      //Destination paths can have variables like <guid> and <ext>, or 
                                                      //even a santizied version of the original filename, like <filename:A-Za-z0-9>

                ImageJob j = new ImageJob(hpf, "~/images/temp/<guid>.<ext>", new Instructions("width=1024;height=768;format=jpg;mode=max;scale=both"));
                j.CreateParentDirectory = true; //Auto-create the uploads directory.
                j.Build();
                ImageList.Add(j);
            }
            if (ImageList.Count > 0)
                SaveToDataBase(ImageList);
        }
        catch (Exception ex)
        {

            HandleError(ex.ToString());

        }

    }


    protected void DeleteTempFile()
    {
        
    }

    protected void SaveToDataBase(List<ImageJob> ImageList)
    {

        if (ImageList.Count > 0)
        {
            try
            {
                foreach (ImageJob j in ImageList)
                {
                    string filename = j.FinalPath;
                    string contentType = j.ResultFileExtension;


                    using (Stream fs = (Stream)j.Source)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            using (MySqlConnection con = new MySqlConnection(constr))
                            {
                                string query = "INSERT INTO insp_images(img_path, img_type, img_blob, img_description) VALUES (@FileName, @ContentType, @Content, @Description)";
                                using (MySqlCommand cmd = new MySqlCommand(query))
                                {
                                    cmd.Connection = con;
                                    cmd.Parameters.AddWithValue("@FileName", filename);
                                    cmd.Parameters.AddWithValue("@ContentType", contentType);
                                    cmd.Parameters.AddWithValue("@Content", "blahblah");
                                    cmd.Parameters.AddWithValue("@Description", "Test Description");
                                    con.Open();
                                    cmd.ExecuteNonQuery();
                                    con.Close();
                                }
                            }
                        }
                    }
                    BindGrid();
                    HandleSuccess("Image added");
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Error " + ex.ToString() + "');", true);
            }
        }
        else
        {
            HandleError("Please select a file to upload.");
        }
    }



    protected void SaveToDataBase_old(List<ImageJob> ImageList)
    {



        if (ImageList.Count > 0)
        {
            try
            {
                using (var connection = new MySqlConnection(constr))
                {

                    // first we'll build our query string. Something like this :
                    // INSERT INTO myTable VALUES (NULL, @number0, @text0), (NULL, @number1, @text1)...; 
                    StringBuilder queryBuilder = new StringBuilder("INSERT INTO insp_images VALUES ");
                    for (int i = 0; i < ImageList.Count; i++)
                    {
                        queryBuilder.AppendFormat("(NULL,@FileName{0},@ContentType{0},@Content{0},@Description{0}),", i);
                        //once we're done looping we remove the last ',' and replace it with a ';'
                        if (i == ImageList.Count - 1)
                        {
                            queryBuilder.Replace(',', ';', queryBuilder.Length - 1, 1);
                        }
                    }


                    MySqlCommand command = new MySqlCommand(queryBuilder.ToString(), connection);
                    byte[] ImageBlob = null;
                    Int32 FileSize = 0;
                    //assign each parameter its value
                    for (int i = 0; i < ImageList.Count; i++)
                    {

                        FileStream fs = new FileStream(ImageList[i].FinalPath, FileMode.Open, FileAccess.Read);
                        FileSize = Convert.ToInt32(fs.Length);
                        fs.Read(ImageBlob, 0, FileSize);
                        fs.Close();

                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            ImageBlob = br.ReadBytes(FileSize);
                        }


                        command.Parameters.AddWithValue("@FileName" + i, ImageList[i].FinalPath);
                        command.Parameters.AddWithValue("@ContentType" + i, ImageList[i].ResultFileExtension);
                        command.Parameters.AddWithValue("@Content" + i, ImageBlob);
                        command.Parameters.AddWithValue("@Description" + i, "Test Description");

                    }

                    connection.Open();
                    command.ExecuteNonQuery();
                }



                //ImageBuilder.Current.Build(AsyncFileUpload1.FileName, "~/images/photo.png", new ResizeSettings("format=png"));


                //string filename = i.FinalPath;
                //string contentType = i.Instructions.Format.ToString();


                //using (Stream fs = (Stream)i.Source)
                //{
                //    using (BinaryReader br = new BinaryReader(fs))
                //    {
                //        byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //        using (MySqlConnection con = new MySqlConnection(constr))
                //        {
                //            string query = "INSERT INTO insp_images(img_path, img_type, img_blob, img_description) VALUES (@FileName, @ContentType, @Content, @Description)";
                //            using (MySqlCommand cmd = new MySqlCommand(query))
                //            {
                //                cmd.Connection = con;
                //                cmd.Parameters.AddWithValue("@FileName", filename);
                //                cmd.Parameters.AddWithValue("@ContentType", contentType);
                //                cmd.Parameters.AddWithValue("@Content", fs);
                //                cmd.Parameters.AddWithValue("@Description", "Test Description");
                //                con.Open();
                //                cmd.ExecuteNonQuery();
                //                con.Close();
                //            }
                //        }
                //    }
                //}
                BindGrid();
                HandleSuccess("Image added");

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Error " + ex.ToString() + "');", true);
            }
        }
        else
        {
            HandleError("Please select a file to upload.");
        }
    }


    protected void HandleError(string message)
    {
        lblError.Text = message;
        ErrorDiv.Visible = true;
    }


    private void BindGrid()
    {
        //string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        using (MySqlConnection con = new MySqlConnection(constr))
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.CommandText = "SELECT unique_id, img_path, img_type, img_blob, img_description FROM insp_images";
                cmd.Connection = con;
                using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    gvImages.DataSource = dt;
                    gvImages.DataBind();
                }
            }
        }
    }
    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            byte[] bytes = (byte[])(e.Row.DataItem as DataRowView)["img_blob"];
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            (e.Row.FindControl("Image1") as System.Web.UI.WebControls.Image).ImageUrl = "data:image/png;base64," + base64String;
        }
    }



    protected void DeleteImage(string id)

    {
        if (!string.IsNullOrEmpty(id))
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(constr))
                {
                    string query = "DELETE from insp_images where unique_id =  '" + id + "'";
                    using (MySqlCommand cmd = new MySqlCommand(query))
                    {
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    BindGrid();
                    HandleSuccess("Image deleted");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex.ToString());
            }
        }
        else {
            HandleError("ID not found.  Cannot delete.");
        }
    }

    protected void gvImages_RowCommand(object sender, GridViewDeleteEventArgs e)
    {
        int index = Convert.ToInt32(e.RowIndex);
        GridViewRow gvRow = gvImages.Rows[index];
        string id = gvRow.Cells[0].Text;
        DeleteImage(id);
    }

    protected void HandleSuccess(string message)
    {
        lblSuccess.Text = message;
        SuccessDiv.Visible = true;
    }
    //protected Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
    //{
    //    var ratioX = (double)maxWidth / image.Width;
    //    var ratioY = (double)maxHeight / image.Height;
    //    var ratio = Math.Min(ratioX, ratioY);

    //    var newWidth = (int)(image.Width * ratio);
    //    var newHeight = (int)(image.Height * ratio);

    //    var newImage = new Bitmap(newWidth, newHeight);

    //    using (var graphics = Graphics.FromImage(newImage))
    //        graphics.DrawImage(image, 0, 0, newWidth, newHeight);

    //    return newImage;
    //}

    //protected System.Drawing.Image ScaleImage(System.Drawing.Image image, double maxHeight)
    //{
    //    try
    //    {
    //        double ratio = maxHeight / image.Height;
    //        var newWidth = (int)(image.Width * ratio);
    //        var newHeight = (int)(image.Height * ratio);
    //        var newImage = new Bitmap(newWidth, newHeight);
    //        using (var g = Graphics.FromImage(newImage))
    //        {
    //            g.DrawImage(image, 0, 0, newWidth, newHeight);
    //        }
    //        return newImage;
    //    }
    //    catch(Exception ex)
    //    {
    //        HandleError(ex.ToString());
    //        return null;
    //    }

    //}
}



