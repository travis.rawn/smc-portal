﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="ajax_autocomplete_tests.aspx.cs" Inherits="secure_admin_sandbox_ajax_autocomplete_tests" %>

<%@ Register Src="~/assets/controls/rz_partrecord_autocomplete.ascx" TagPrefix="uc1" TagName="rz_partrecord_autocomplete" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="javascript">
        $(document).ready(function () {
            $("#<% =txtCountry.ClientID%>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "/secure_sm/sm_webmethods/sm_webmethods.aspx/LoadCountry",
                            data: "{input:'" + request.term + "'}",
                            dataType: "json",
                            success: function (output) {
                                response(output.d);
                            },
                            error: function (errormsg) {
                                alert(errormsg.responseText);
                            }
                        });
                    }
                });
            });


            $(document).ready(function () {
                $("#<% =txtCustomer.ClientID%>").autocomplete({
                         source: function (request, response) {
                             $.ajax({
                                 type: "POST",
                                 contentType: "application/json; charset=utf-8",
                                 url: "/secure_sm/sm_webmethods/sm_webmethods.aspx/LoadCustomers",
                                 data: "{input:'" + request.term + "'}",
                                 dataType: "json",
                                 success: function (output) {
                                     response(output.d);                                     
                                 },                                 
                                 error: function (errormsg) {
                                     alert(errormsg.responseText);
                                 }
                             });
                         }
                     });
                 });


    </script>



    <div>
        Country :
        <asp:TextBox ID="txtCountry" runat="server">
        </asp:TextBox>
    </div>

    <div>
        Customer :
        <asp:TextBox ID="txtCustomer" runat="server"></asp:TextBox>
        <asp:HiddenField ID="hfCustomerID" runat="server" />
        <a href="#" class="btn btn-sm btn-success" onclick="alert($('#MainContent_txtCustomer').val());">Choose</a>
    </div>


    <h3>Sensible Custom Controls</h3>
    <div class="row">
        <div class="col-sm-6">
            <p>Company / Contact</p>
            <uc1:rz_company_autocomplete runat="server" ID="rz_company_autocomplete" />
        </div>
         <div class="col-sm-6">
             <p>Partrecord</p>
             <uc1:rz_partrecord_autocomplete runat="server" ID="rz_partrecord_autocomplete" />
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

