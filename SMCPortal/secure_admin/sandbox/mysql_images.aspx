﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="mysql_images.aspx.cs" Inherits="secure_admin_sandbox_mysql_images" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h2 class="well well-sm">Imgaes MYSQL</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    
    <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="True" />
    <br />
    <asp:Button ID="btnUpload" runat="server" Text="Upload Image" CssClass="btn btn-sm btn-success" OnClick="Button1_Click"/>    

    <asp:GridView ID="gvImages" runat="server" CssClass="table table-hover table-striped gvstyling" GridLines="None" AutoGenerateColumns="False" OnRowDataBound="OnRowDataBound" OnRowDeleting="gvImages_RowCommand" EmptyDataText="No Images in Database">
        <Columns>
            <asp:BoundField HeaderText="ID" DataField="unique_id"/>
            <asp:BoundField HeaderText="img_path" DataField="img_path" />
            <asp:BoundField HeaderText="img_type" DataField="img_type" />
            <asp:BoundField HeaderText="img_description" DataField="img_description" />
            <asp:TemplateField HeaderText="Image">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="80" Width="80" />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-sm btn-danger btn-block" CommandName="Delete"/>
                </ItemTemplate>
            </asp:TemplateField>
           
            
        </Columns>
    </asp:GridView>


    <hr />
    New Stuff: <br />
    <asp:Image ID="Image2" runat="server" /><br />
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

    <div class="alert alert-danger" id="ErrorDiv" runat="server" visible="false">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!  </strong><asp:Label ID="lblError" runat="server"></asp:Label>
    </div>

    <div class="alert alert-success" id="SuccessDiv" runat="server" visible="false">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!  </strong><asp:Label ID="lblSuccess" runat="server"></asp:Label>
    </div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>




