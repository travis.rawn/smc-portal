﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="csv.aspx.cs" Inherits="secure_admin_test_csv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Button ID="btnSearch" runat="server" Text="Search CSV" OnClick="btnSearch_Click" CssClass="btn btn-primary btn-sm"/>
    &nbsp;<asp:Label ID="lblResults" runat="server" Text="Label"></asp:Label>
    &nbsp;<asp:TextBox ID="txtSearch" runat="server" CssClass="form-control">
    </asp:TextBox><asp:GridView ID="GridView1" runat="server"  CssClass="table table-hover table-striped" GridLines="None">
    </asp:GridView>
    <br />
</asp:Content>

