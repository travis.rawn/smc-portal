﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_commissions : System.Web.UI.Page
{
    DateTime startDate;//{7/26/2022 12:00:00 AM}
    DateTime endDate;
    //DateTime startDate = new DateTime(2022, 09, 01);//{7/26/2022 12:00:00 AM}
    //DateTime endDate = new DateTime(2022, 10, 01);
    //static string MikeID = "e9197ee25bf84dbfae1b13f38ec582c2";
    //static string DavidID = "dc72e774b8a84648813f03fb054aba68";
    //List<string> agentIdList = new List<string>() { DavidID };
    RzTools rzt = new RzTools();





    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDatePicker();
        LoadAgentPicker();
        if (Page.IsPostBack)
        {
            LoadAll();
        }

    }


    private void LoadDatePicker()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Today.Date.AddMonths(-6);
            dtpEnd.SelectedDate = DateTime.Today.Date;
        }

        startDate = dtpStart.SelectedDate.Value;
        endDate = dtpEnd.SelectedDate.Value;

    }

    private void LoadAgentPicker()
    {   //List<string> salesTeams = new List<string>() { "Sales", "Distributor Sales" };       
        List<string> salesTeams = new List<string>() { "Sales" };
        List<n_user> allSalesUsers = rzt.GetUsersForTeams(salesTeams);
        if (!Page.IsPostBack)
        {
            rzap.LoadPicker(allSalesUsers);
            rzap.SelectedUserID = "all";
            rzap.Enabled = false;

            //Get list of authorized users, if current user is in that list, Enable the control.
            List<n_user> managementUsers = rzt.GetUsersForTeams(new List<String>() { "Sales Management" });
            List<string> managementUserIDs = managementUsers.Select(s => s.unique_id).Distinct().ToList();
            string CurrentUserID = Profile.RzUserID;
            if (managementUserIDs.Contains(CurrentUserID) || Membership.GetUser().UserName == "kevint")
                rzap.Enabled = true;
            else
                rzap.SelectedUserID = CurrentUserID;
        }


    }


    private void LoadAll()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            //Sale Lines in date range
            var saleQuery = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= startDate && w.date_created.Value.Date <= endDate && (w.orderid_sales ?? "").Length > 0);


            //Specific Invoice
            //saleQuery = saleQuery.Where(w => w.ordernumber_invoice == "26931");

            //string saleCount = "Sale Line Count: " + saleQuery.Count();
            //saleCount.Dump();
            //Get split Ids for all sale lines in this range.
            var saleSplitCommissionIds = saleQuery.Select(s => s.split_commission_ID).Distinct();
            //string saleSplitCount = "Sale  Split Commission Count: " + saleSplitCommissionIds.Count();
            //saleSplitCount.Dump();

            //Quote lines in date range
            var quoteQuery = rdc.orddet_quotes.Where(w => w.date_created.Value.Date >= startDate && w.date_created.Value.Date <= endDate
            && (w.fullpartnumber ?? "").Length > 0);
            //string quoteCount = "Quote Line Count: " + quoteQuery.Count();
            //quoteCount.Dump();
            //Get split Ids for all quote lines in this range.
            var quoteSplitCommissionIds = quoteQuery.Select(s => s.split_commission_ID).Distinct();
            //string quoteSplitCount = "Quote Split Commission Count: " + quoteSplitCommissionIds.Count();
            //quoteSplitCount.Dump();

            //Combine split commission ids into single list
            var allSplitCommissionIds = quoteSplitCommissionIds.Union(saleSplitCommissionIds);

            //List of all splitCommissions
            List<split_commission> scList = rdc.split_commissions.Where(w => allSplitCommissionIds.Contains(w.unique_id)).ToList();
            string scCount = "Combined Split Commission Count: " + scList.Count();
            //scCount.Dump();





            //Quote Objects
            //var quoteObjects = quoteQuery.Select(s => new
            //{

            //    OrderNumber = s.ordernumber,
            //    OrderID = s.base_ordhed_uid,
            //    OrderDate = s.orderdate.Value,
            //    LineID = s.unique_id,
            //    Part = s.fullpartnumber,
            //    Type = s.base_ordhed_uid.Length > 0 ? "Quote" : "Req",
            //    Agent = s.agentname,
            //    AgentID = s.base_mc_user_uid,
            //    SplitID = s.split_commission_ID,
            //    SplitAgent = "",
            //    SplitAgentID = "",
            //    UnitCost = s.unitcost ?? 0,
            //    UnitPrice = s.unitprice ?? 0,
            //    GrossProfit = s.grossprofit ?? 0,
            //    Qty = s.quantity,
            //    CustomerName = s.companyname,
            //    CustomerID = s.base_company_uid,

            //    InvoiceNumber = "",
            //    InvoiceID = "",
            //});

            //quoteObjects.Dump();



            //Sale Objects
            var saleObjects = saleQuery.Select(s => new
            {

                OrderNumber = s.ordernumber_sales,
                OrderID = s.orderid_sales,
                OrderDate = s.orderdate_sales.Value,
                LineID = s.unique_id,
                Part = s.fullpartnumber,
                Type = "Sale",
                Agent = s.seller_name,
                AgentID = s.seller_uid,
                SplitID = s.split_commission_ID,
                SplitAgent = "",
                SplitAgentID = "",
                UnitCost = s.unit_cost ?? 0,
                UnitPrice = s.unit_price ?? 0,
                GrossProfit = s.gross_profit ?? 0,
                Qty = s.quantity,
                CustomerName = s.customer_name,
                CustomerID = s.customer_uid,


                InvoiceNumber = s.ordernumber_invoice,
                InvoiceID = s.orderid_invoice,

            });


            //List of Objects, generic for now, but should be formal class for calculating

            //var allObjects = quoteObjects.Union(saleObjects);
            var allObjects = saleObjects;
            //var allObjects = saleObjects;

            //List of users to get their current commission percent
            List<n_user> agentList = new List<n_user>();
            List<string> agentIds = allObjects.Select(s => s.AgentID).Distinct().ToList();
            agentList = rdc.n_users.Where(w => agentIds.Contains(w.unique_id)).ToList();

            //Now we need a list of invoices and their payments.
            List<InvoicePaidObject> invoicePaymentList = new List<InvoicePaidObject>();
            var listInvoiceIds = allObjects.Where(w => w.InvoiceID.Length > 0).Select(s => s.InvoiceID).Distinct();
            List<ordhed_invoice> listInvoices = rdc.ordhed_invoices.Where(w => listInvoiceIds.Contains(w.unique_id)).Distinct().ToList();
            List<checkpayment> checkpaymentList = rdc.checkpayments.Where(w => listInvoiceIds.Contains(w.base_ordhed_uid)).Distinct().ToList();


            foreach (string invoiceID in listInvoiceIds)
            {
                ordhed_invoice i = listInvoices.FirstOrDefault(f => f.unique_id == invoiceID);
                if (i == null)
                    continue;

                InvoicePaidObject ipo = new InvoicePaidObject();
                ipo.InvoiceID = i.unique_id;
                ipo.InvoiceNumber = i.ordernumber;
                ipo.InvoiceTotal = i.ordertotal ?? 0;
                ipo.InvoicePaidAmount = 0;
                ipo.PaidInFull = false;

                //Payment Info	
                List<checkpayment> cp = checkpaymentList.Where(w => w.base_ordhed_uid == ipo.InvoiceID).ToList();
                if (cp != null && cp.Count > 0)
                {
                    ipo.InvoicePaidAmount = cp.Sum(s => s.transamount ?? 0);
                    ipo.MaxPaidDate = cp.Max(m => m.transdate.Value);
                    if (ipo.InvoicePaidAmount >= ipo.InvoiceTotal)
                        ipo.PaidInFull = true;
                }




                invoicePaymentList.Add(ipo);
            }




            ////////Master Dataset of all Line and quote Object results.
            /////Now put it all together in one final list.
            List<LineCommissionObject> lcoList = new List<LineCommissionObject>();
            foreach (var o in allObjects)
            {
                LineCommissionObject lco = new LineCommissionObject();
                lco.OrderNumber = o.OrderNumber ?? "";
                lco.OrderID = o.OrderID ?? "";
                lco.OrderDate = o.OrderDate;
                lco.Part = o.Part ?? "";
                lco.Type = o.Type ?? "";
                lco.Agent = o.Agent ?? "";
                lco.AgentID = o.AgentID ?? "";
                lco.SplitID = "" ?? "";
                lco.LineID = o.LineID;
                lco.MaxPercent = 0;
                lco.AgentPercent = 0;
                lco.GrossProfit = o.GrossProfit;
                //lco.AgentAmount = Convert.ToDouble((o.UnitPrice - o.UnitCost) * o.Qty);

                lco.UnitCost = o.UnitCost;
                lco.UnitPrice = o.UnitPrice;
                lco.Qty = o.Qty ?? 0;
                lco.CustomerName = o.CustomerName;
                lco.CustomerID = o.CustomerID;

                n_user normalAgent = agentList.FirstOrDefault(f => f.unique_id == o.AgentID);
                if (normalAgent != null)
                {
                    lco.AgentPercent = Convert.ToDecimal(normalAgent.commission_percent ?? 0);
                    lco.MaxPercent = lco.AgentPercent;

                }



                if (!string.IsNullOrEmpty(o.SplitID))
                {
                    lco.SplitID = o.SplitID;
                    split_commission s = scList.Where(w => w.unique_id == o.SplitID).FirstOrDefault();

                    if (s != null && lco.AgentID != s.split_commission_agent_id)
                    {
                        lco.SplitAgentID = s.split_commission_agent_id;
                        lco.SplitAgentName = s.split_commission_agent;
                        lco.SplitType = s.split_commission_type;

                        lco.SplitPercent = Convert.ToDecimal(s.split_commission_percent);
                        lco.AgentPercent = lco.AgentPercent - lco.SplitPercent;


                    }
                }

                lco.AgentGross = (double)lco.AgentPercent * lco.GrossProfit;
                if (lco.SplitPercent > 0)
                    lco.SplitGross = (double)lco.SplitPercent * lco.GrossProfit;

                //Invoice DAta
                lco.InvoiceID = o.InvoiceID;
                lco.InvoiceNumber = o.InvoiceNumber;
                InvoicePaidObject ipo = invoicePaymentList.FirstOrDefault(a => a.InvoiceID == lco.InvoiceID);
                if (ipo != null)
                {
                    lco.InvoiceTotal = ipo.InvoiceTotal;
                    lco.InvoicePaidAmount = ipo.InvoicePaidAmount;
                    lco.PaidInFull = ipo.PaidInFull;
                    lco.MaxPaidDate = ipo.MaxPaidDate;
                }

                //Bogey



                lcoList.Add(lco);
            }




            ///////From the MaiQuery, we can identify the Splits information


            var finalQuery = lcoList.Select(lco => new
            {
                Company = lco.CustomerName,
                Agent = lco.Agent,
                AgentPct = lco.AgentPercent,
                SplitAgent = lco.SplitAgentName,
                SplitPct = lco.SplitPercent,
                OrderNumber = lco.OrderNumber,
                OrderDate = lco.OrderDate,
                OrderType = lco.Type,
                InvoiceNumber = lco.InvoiceNumber,
                AgentAmount = lco.AgentGross,
                SplitAmount = lco.SplitGross,
                InvoiceTotal = lco.InvoiceTotal,
                LastPaid = lco.MaxPaidDate,
                PaymentTotal = lco.InvoicePaidAmount,
                PaidInFull = lco.PaidInFull,
                PaidDate = lco.MaxPaidDate,
                AgentID = lco.AgentID,
                SplitAgentID = lco.SplitAgentID,

            }
            );

            if (rzap.SelectedUserID != "all")
                finalQuery = finalQuery.Where(w => w.AgentID == rzap.SelectedUserID || w.SplitAgentID == rzap.SelectedUserID);

            smdtSales.dataSource = finalQuery.Select(s => new {
                Company = s.Company,
                Sale = s.OrderNumber,
                Invoice = s.InvoiceNumber,
                PayDate = s.PaidDate.ToShortDateString(),
                AmntPaid = s.PaymentTotal.ToString("C"),
                TotPrice = s.InvoiceTotal,
                Deds = 0,
                TotCost = 0,
                NetProfit = 0,
                NetPct = 0,
                ComPCt = 0,
                AgentGC = s.AgentAmount,
                AgentPct = s.AgentPct,
                SplitGC = s.SplitAmount,
                SplitPct = s.SplitAmount






            }).OrderBy(o => o.PayDate);
            smdtSales.loadGrid();
        }
    }

    class LineCommissionObject
    {

        public string OrderType { get; set; }
        public string OrderNumber { get; set; }
        public string OrderID { get; set; }
        public DateTime? OrderDate { get; set; }


        public string LineID { get; set; }
        public string Part { get; set; }
        public string Type { get; set; }
        public string Agent { get; set; }
        public string AgentID { get; set; }

        public decimal? MaxPercent { get; set; }
        public decimal? AgentPercent { get; set; }
        public double AgentGross { get; set; }

        public string SplitID { get; set; }
        public string SplitAgentName { get; set; }
        public string SplitAgentID { get; set; }
        public string SplitType { get; set; }
        public decimal? SplitPercent { get; set; }
        public double SplitGross{ get; set; }


        public double UnitCost { get; set; }
        public double UnitPrice { get; set; }
        public double GrossProfit { get; set; }
        public int Qty { get; set; }

        public string CustomerName { get; set; }
        public string CustomerID { get; set; }

        //Invoice Info
        public string InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }

        //Payment Info
        public double InvoiceTotal { get; set; }
        public double InvoicePaidAmount { get; set; }
        public DateTime MaxPaidDate { get; set; }
        public bool PaidInFull { get; set; }

    }


    class InvoicePaidObject
    {
        public string InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }
        public double InvoiceTotal { get; set; }
        public double InvoicePaidAmount { get; set; }
        public DateTime MaxPaidDate { get; set; }
        public bool PaidInFull { get; set; }
    }


    //**columns**
    //Company
    //Sale#
    //Invoice#
    //PayDate
    //Amnt Paid
    //Total Price
    //Deductions
    //Total Cost
    //Net Profit
    //Net Percent
    //Comm%
    //Line Comm

    //**Subtotals**
    //Total Sales
    //Total Cost
    //Total Deduct
    //PAyroll Deds
    //Total NP
    //Gross Comm:
    //Bogey:
    //Inv Credits:
    //Net Comm:













}