﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="cross_reference_logic.aspx.cs" Inherits="secure_admin_sandbox_cross_reference_logic" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>CROSS REFERENCE LOGIC</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="card well">
        <p><strong>On any Stock, consign, Excess import AND/OR Batch Saved AND/OR hourly trigger is hit: </strong></p>
        <br />
        <p>For all lines we have, compare to:</p>
        <ul>
            <li>Rfqs
            </li>
            <li>Sales
            </li>
        </ul>
        <p>When new item found that didn't exist before, notify:</p>
        <ul>
            <li>Current Rz / Hubspot Contact / Company Owner -OR- if unowned - add to master list for Management
            </li>
            <li>Management Master List
            </li>
        </ul>


    </div>
    <div class="card form-inline">
        <asp:TextBox ID="txtPartNumbers" runat="server" Width="500px" CssClass="form-control" TextMode="MultiLine" Placeholder="pn,pn,pn"></asp:TextBox>
        <asp:LinkButton ID="lbGo" runat="server" OnClick="lbGo_Click" CssClass="btn-smc btn-smc-info form-control">Go</asp:LinkButton>
    </div>
    <div class="card">
        <div class="row">
            <div class="col-sm-6">
                <h5>RFQs</h5>
                <uc1:sm_datatable runat="server" ID="smdtRfqs" />
            </div>
            <div class="col-sm-6">
                <h5>Sales</h5>
                <uc1:sm_datatable runat="server" ID="smdtSales" />
            </div>
        </div>
        <label></label>

    </div>


</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

