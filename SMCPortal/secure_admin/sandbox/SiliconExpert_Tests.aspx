﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="SiliconExpert_Tests.aspx.cs" Inherits="secure_admin_sandbox_SiliconExpert_JSON" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h2 class="well">Silicon Expert Tests</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel">
        <%-- Search Controls--%>
        <div class="row">
            <div class="col-md-4">
                <asp:TextBox ID="txtPartnumber" runat="server" PlaceHolder="Part Number(s)" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-4">
                <asp:Button ID="btnPartListSearchXML" runat="server" Text="Part List Search XML" OnClick="btnPartListSearchXML_Click" CssClass="btn btn-default" />
               <%-- <asp:Button ID="btnPartListSearchJSON" runat="server" Text="Part List Search JSON" OnClick="btnPartListSearchJSON_Click" CssClass="btn btn-default" />--%>
                <asp:Button ID="btnPartListSearchMulti" runat="server" Text="Part List Search XML (Multiple Parts)" OnClick="btnPartListSearchMulti_Click" CssClass="btn btn-default" />
            </div>
        </div>
        <%-- XML Grid--%>
        <div class="row">
            <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-striped gvstyling" GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="partNumber" HeaderText="Part Number" SortExpression="partNumber" />
                        <asp:BoundField DataField="manufacturer" HeaderText="Manufacturer" SortExpression="manufacturer" />
                        <asp:BoundField DataField="description" HeaderText="Description" SortExpression="description" />
                        <asp:BoundField DataField="lifecycle" HeaderText="Lifecycle" SortExpression="lifecycle" />
                        <asp:BoundField DataField="rohs" HeaderText="RoHS" SortExpression="rohs" />
                        <asp:TemplateField SortExpression="datasheet" HeaderText="Datasheet">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlDataSheet" runat="server" CssClass="btn btn-sm btn-success btn-block" NavigateUrl='<%# Eval("datasheet") %>' Text="DataSheet" Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle Wrap="True" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <%-- JSON Grid--%>
        <div class="row">
            <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
                <asp:GridView ID="GridView2" runat="server" CssClass="table table-hover table-striped gvstyling" GridLines="None">
                    <%--  <Columns>
                        <asp:BoundField DataField="partNumber" HeaderText="Part Number" SortExpression="partNumber" />
                        <asp:BoundField DataField="manufacturer" HeaderText="Manufacturer" SortExpression="manufacturer" />
                        <asp:BoundField DataField="description" HeaderText="Description" SortExpression="description" />
                        <asp:BoundField DataField="lifecycle" HeaderText="Lifecycle" SortExpression="lifecycle" />
                        <asp:BoundField DataField="rohs" HeaderText="RoHS" SortExpression="rohs" />
                        <asp:TemplateField SortExpression="datasheet" HeaderText="Datasheet">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlDataSheet" runat="server" CssClass="btn btn-sm btn-success btn-block" NavigateUrl='<%# Eval("datasheet") %>' Text="DataSheet" Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle Wrap="True" />
                        </asp:TemplateField>
                    </Columns>--%>
                </asp:GridView>
            </div>
        </div>
        <%-- Experimental--%>
        <div class="row">
            <label>Experimental:</label><br />
            <asp:Button ID="btnPartDetailSearchXML" runat="server" Text="Part Detail Search" OnClick="btnPartDetailSearchXML_Click" CssClass="btn btn-default" />
            <br />
            <asp:Label ID="lblPartnumberResult" runat="server"></asp:Label>
            <asp:Label ID="lblJSONString" runat="server" Text="Label"></asp:Label>
            <div>
                <div class="panel panel-heading">
                    <label>Get ComID</label>
                </div>
                <div class="panel panel-body">
                    <div class="co-md-6">
                         <asp:Button ID="btnGetComID" runat="server" Text="Get ComID" OnClick="btnGetComID_Click" CssClass="btn btn-default" />
                    </div>
                    <div class="co-md-6">
                       <asp:Label ID="lblComID" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

