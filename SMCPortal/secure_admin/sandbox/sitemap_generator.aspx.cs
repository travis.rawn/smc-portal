﻿using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class secure_admin_sandbox_sitemap_generator : System.Web.UI.Page
{

    SM_Tools tools = new SM_Tools();
    
    sitemap_generator sg = new sitemap_generator(new RzDataContext());

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lbGenerateStockSiteMap_Click(object sender, EventArgs e)
    {
        try
        {
            sg.CreateSitemaps("stock");
            tools.HandleResult("success", "Success", 120000);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbGenerateConsignSiteMap_Click(object sender, EventArgs e)
    {

        try
        {
            sg.CreateSitemaps("consign");
            tools.HandleResult("success", "Success", 120000);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }


    protected void lbGenerateExcessSitemap_Click(object sender, EventArgs e)
    {

        try
        {
            sg.CreateSitemaps("excess");
            tools.HandleResult("success", "Success", 120000);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbGenerateAllSitemaps_Click(object sender, EventArgs e)
    {

        try
        {
            sg.CreateSitemaps("all");

            //Success notification
            string message = "Successfully created sitemap files: " + string.Join(", ", sitemap_generator.SitemapFileNames);
            lblSiteMapResults.Text = message;
            tools.HandleResult("success", "Successfully generated sitemap files. ");
            tools.SendMail("sitepmap@sensiblemicro.com", SystemLogic.Email.EmailGroup.Systems, "Sitemap(s) Successfully Generated " + DateTime.Now.ToShortDateString(), message);
        }
        catch (Exception ex)
        {
            //Fail notification
            tools.HandleResult("fail", ex.Message);
            tools.SendMail("sitepmap@sensiblemicro.com", SystemLogic.Email.EmailGroup.Systems, "Sitemap(s) Failed to Generate " + DateTime.Now.ToShortDateString(), ex.Message + " " + ex.InnerException);
        }
    }

}