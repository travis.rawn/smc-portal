﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="2019_flex_login_registration.aspx.cs" Inherits="secure_admin_sandbox_2019_flex_login_registration" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .section {
            border-style: dotted;
            padding: 10px;
        }

        .button {
            padding: 5px;
        }
    </style>


    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        //Ensure captcha reloads on asyncppostback
        function CaptchaReload() {
            Recaptcha.create("6Lc1qSUTAAAAAPrvNw45yv", '#g-recaptcha', {
                theme: 'white',
                callback: grecaptcha.reset()

            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FullWidthContent" runat="Server">
    <div style="text-align: center">
        <h2>
            <asp:Label ID="lblMessage" runat="server" Style="display: none;" /></h2>
    </div>



    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="lbLogin">
        <h2>Login</h2>
        <div class="section">

            <div id="hs_cos_wrapper_widget_1550858841443" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
                <div id="section1" class="dual-column__wrapper background__white">
                    <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__small">
                        <div class="col6 content__container">
                            <h3 class="">Login</h3>
                            <div class="supporting-content__container ">

                                <div class="supporting-content   icon-list__container icon-list__checkmark">
                                    <p>&nbsp;In the customer portal, you have access to:</p>
                                    <ul>
                                        <li>Your orders</li>
                                        <li>Tracking numbers and status updates</li>
                                        <li>Quality inspection &amp; lab reports</li>
                                        <li><span>Component intelligence such as Datasheet, Product Change Notifications, Lifecycle information and Environmental Data</span></li>
                                        <li>Unlimited access to our part search</li>
                                    </ul>
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                        <!-- end of column1 -->
                        <!-- start of column2 -->

                        <div class="col6 content__container">
                            <div class="form__container form__container-boxed">
                                <span id="hs_cos_wrapper_widget_1550858841443_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_form" style="" data-hs-cos-general-type="widget" data-hs-cos-type="form">
                                    <h3 id="hs_cos_wrapper_form_685905058_title" class="hs_cos_wrapper form-title" data-hs-cos-general-type="widget_field" data-hs-cos-type="text"></h3>
                                    <div id="hs_form_target_form_685905058">
                                        <%--<form novalidate="" accept-charset="UTF-8" action="https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/1878634/229380ec-e432-42a0-9edc-ea09c3e94af3" enctype="multipart/form-data" id="hsForm_229380ec-e432-42a0-9edc-ea09c3e94af3_210" method="POST" class="hs-form stacked hs-custom-form hs-form-private hs-form-229380ec-e432-42a0-9edc-ea09c3e94af3_13c2191c-da4c-4940-a734-6def04d0785a" data-form-id="229380ec-e432-42a0-9edc-ea09c3e94af3" data-portal-id="1878634" target="target_iframe_229380ec-e432-42a0-9edc-ea09c3e94af3_210" data-reactid=".hbspt-forms-1">--%>
                                        <div data-reactid=".hbspt-forms-1.1:$0">
                                            <div class="hs-richtext hs-main-font-element" data-reactid=".hbspt-forms-1.1:$0.0">
                                                <h3>Login Now</h3>
                                            </div>
                                            <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0.$email">
                                                <label id="label-email-229380ec-e432-42a0-9edc-ea09c3e94af3_210" class="" placeholder="Enter your User name / email:" for="email-229380ec-e432-42a0-9edc-ea09c3e94af3_210" data-reactid=".hbspt-forms-1.1:$0.$email.0"><span data-reactid=".hbspt-forms-1.1:$0.$email.0.0">User name / email:</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$0.$email.1"></legend>
                                                <div class="input">
                                                    <%--<input id="email-229380ec-e432-42a0-9edc-ea09c3e94af3_210" class="hs-input" type="email" name="email" placeholder="" value="" autocomplete="email" data-reactid=".hbspt-forms-1.1:$0.$email.$email.0" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">--%>
                                                    <asp:TextBox ID="txtUserName" runat="server" class="hs-input" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hs_password hs-password hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$1">
                                            <label id="label-password-229380ec-e432-42a0-9edc-ea09c3e94af3_210" class="" placeholder="Enter your Password" for="password-229380ec-e432-42a0-9edc-ea09c3e94af3_210" data-reactid=".hbspt-forms-1.1:$1.0"><span data-reactid=".hbspt-forms-1.1:$1.0.0">Password</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$1.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$1.$password">
                                                <%--<input id="password-229380ec-e432-42a0-9edc-ea09c3e94af3_210" class="hs-input" type="text" name="password" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$1.$password.0" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">--%>
                                                <asp:TextBox ID="txtPassword" runat="server" class="hs-input" type="text" name="password" TextMode="Password" value="" placeholder="" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />
                                            </div>
                                        </div>
                                        <noscript data-reactid=".hbspt-forms-1.2"></noscript>

                                        <%--  Remember ME--%>
                                        <div class="hs_remember_me hs-remember_me hs-fieldtype-booleancheckbox field hs-form-field" data-reactid=".hbspt-forms-0.1:$2">
                                            <legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-0.1:$2.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-0.1:$2.$remember_me">
                                                <label>
                                                    Remember me: 
                                                            <asp:CheckBox ID="cbxRememberMe" runat="server" CssClass="hs-input" />
                                                    <%--<input id="remember_me-229380ec-e432-42a0-9edc-ea09c3e94af3" class="hs-input" type="checkbox" name="remember_me" value="true" data-reactid=".hbspt-forms-0.1:$2.$remember_me.0.0.0.0"><span data-reactid=".hbspt-forms-0.1:$2.$remember_me.0.0.0.1">Remember Me</span>--%>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                            <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                            <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                                <asp:LinkButton ID="lbLogin" runat="server" CssClass="hs-button primary large" OnClick="lbLogin_Click" OnClientClick="">Login</asp:LinkButton>
                                                <asp:LinkButton ID="lbLogout" runat="server" CssClass="hs-button default large" OnClick="lbLogout_Click" OnClientClick="">Logout</asp:LinkButton>
                                            </div>
                                            <noscript data-reactid=".hbspt-forms-1.6"></noscript>
                                            <%--<input name="hs_context" type="hidden" value="{&quot;rumScriptExecuteTime&quot;:781.844999997702,&quot;rumServiceResponseTime&quot;:921.104999993986,&quot;rumFormRenderTime&quot;:1.7799999986891635,&quot;rumTotalRenderTime&quot;:923.1349999972736,&quot;rumTotalRequestTime&quot;:137.0699999970384,&quot;pageUrl&quot;:&quot;https://info.sensiblemicro.com/login?hs_preview=hwbBgPYQ-8456210079&quot;,&quot;pageTitle&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;source&quot;:&quot;FormsNext-static-3.239&quot;,&quot;timestamp&quot;:1554904485596,&quot;userAgent&quot;:&quot;Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36&quot;,&quot;originalEmbedContext&quot;:{&quot;formsBaseUrl&quot;:&quot;/_hcms/forms/&quot;,&quot;portalId&quot;:&quot;1878634&quot;,&quot;formId&quot;:&quot;229380ec-e432-42a0-9edc-ea09c3e94af3&quot;,&quot;formInstanceId&quot;:&quot;210&quot;,&quot;pageId&quot;:&quot;8456210079&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;inlineMessage&quot;:true,&quot;rawInlineMessage&quot;:&quot;Thanks for submitting the form.&quot;,&quot;hsFormKey&quot;:&quot;2b4354f7ec0c2963cdb6335f3b96d198&quot;,&quot;deactivateSmartForm&quot;:true,&quot;target&quot;:&quot;#hs_form_target_form_685905058&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;formData&quot;:{&quot;cssClass&quot;:&quot;hs-form stacked hs-custom-form&quot;},&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;},&quot;pageId&quot;:&quot;8456210079&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;formInstanceId&quot;:&quot;210&quot;,&quot;urlParams&quot;:{&quot;hs_preview&quot;:&quot;hwbBgPYQ-8456210079&quot;},&quot;formValidity&quot;:{&quot;email&quot;:{&quot;valid&quot;:false,&quot;errors&quot;:[&quot;Please enter a valid email address.&quot;],&quot;errorTypes&quot;:[&quot;INVALID_EMAIL&quot;]}},&quot;rawInlineMessage&quot;:&quot;Thanks for submitting the form.&quot;,&quot;hsFormKey&quot;:&quot;2b4354f7ec0c2963cdb6335f3b96d198&quot;,&quot;formTarget&quot;:&quot;#hs_form_target_form_685905058&quot;,&quot;correlationId&quot;:&quot;07497232-f16c-427d-b839-451d4b1a662f&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;,&quot;isHostedOnHubspot&quot;:true}" data-reactid=".hbspt-forms-1.7"><iframe name="target_iframe_229380ec-e432-42a0-9edc-ea09c3e94af3_210" style="display: none;" data-reactid=".hbspt-forms-1.8"></iframe>--%>
                                        </div>
                                        <%--</form>--%>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <!-- end of column2 -->
                    </div>
                </div>
            </div>

        </div>
    </asp:Panel>
    <asp:Panel ID="pnlRegister" runat="server" DefaultButton="lbLogin">
        <h2>Register</h2>
        <div class="section">
            <div id="section2" class="dual-column__wrapper background__white">
                <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__small">

                    <div class="col6 content__container">

                        <h3 class="">How to Register</h3>

                        <div class="supporting-content__container supporting-content-bar__container">

                            <div class="supporting-content supporting-content-bar  icon-list__container icon-list__checkmark">
                                <p>To create your account, our team will add your information to the Customer Portal.</p>
                                <p>Simply fill out the form on this page to have your account created. Once your account is setup , you will be able to Log In and will have complete&nbsp;access to the Customer Portal.</p>
                                <p>In the portal, you are able to review:</p>
                                <ul>
                                    <li>Your orders,</li>
                                    <li>Tracking numbers and status updates,</li>
                                    <li>Quality reports,</li>
                                    <li>and more!</li>
                                </ul>
                                <p>&nbsp;</p>
                            </div>
                        </div>
                    </div>
                    <!-- end of column1 -->
                    <!-- start of column2 -->

                    <div class="col6 content__container">

                        <div class="form__container form__container-boxed">
                            <div id="hs_cos_wrapper_widget_1550858841443_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_form" style="" data-hs-cos-general-type="widget" data-hs-cos-type="form">
                                <h3 id="hs_cos_wrapper_form_618646516_title" class="hs_cos_wrapper form-title" data-hs-cos-general-type="widget_field" data-hs-cos-type="text"></h3>

                                <div id="hs_form_target_form_618646516">
                                    <div data-reactid=".hbspt-forms-1.1:$0">
                                        <div class="hs-richtext hs-main-font-element" data-reactid=".hbspt-forms-1.1:$0.0">
                                            <h4>Register Now</h4>
                                        </div>
                                        <div class="hs_firstname hs-firstname hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0.$firstname">
                                            <label id="label-firstname-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" class="" placeholder="Enter your First name" for="firstname-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" data-reactid=".hbspt-forms-1.1:$0.$firstname.0"><span data-reactid=".hbspt-forms-1.1:$0.$firstname.0.0">First name</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$0.$firstname.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$0.$firstname.$firstname">
                                                <%--<input id="firstname-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" class="hs-input" type="text" name="firstname" value="" placeholder="" autocomplete="given-name" data-reactid=".hbspt-forms-1.1:$0.$firstname.$firstname.0" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">--%>
                                                <asp:TextBox ID="txtFirstName" runat="server" class="hs-input" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="hs_lastname hs-lastname hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$1">
                                        <label id="label-lastname-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" class="" placeholder="Enter your Last name" for="lastname-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" data-reactid=".hbspt-forms-1.1:$1.0"><span data-reactid=".hbspt-forms-1.1:$1.0.0">Last name</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$1.1"></legend>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$1.$lastname">
                                            <asp:TextBox ID="txtLastName" runat="server" class="hs-input" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />
                                            <%--<input id="lastname-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" class="hs-input" type="text" name="lastname" value="" placeholder="" autocomplete="family-name" data-reactid=".hbspt-forms-1.1:$1.$lastname.0">--%>
                                        </div>
                                    </div>
                                    <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$2">
                                        <label id="label-email-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" class="" placeholder="Enter your Email" for="email-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" data-reactid=".hbspt-forms-1.1:$2.0"><span data-reactid=".hbspt-forms-1.1:$2.0.0">Email</span><span class="hs-form-required" data-reactid=".hbspt-forms-1.1:$2.0.1">*</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$2.1"></legend>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$2.$email">
                                            <%--<input id="email-5ac13e0b-dac9-4770-9469-fcf6ac762208_103" class="hs-input" type="email" name="email" required="" placeholder="" value="" autocomplete="email" data-reactid=".hbspt-forms-1.1:$2.$email.0">--%>
                                            <asp:TextBox ID="txtEmail" runat="server" class="hs-input" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />

                                        </div>
                                    </div>
                                    <noscript data-reactid=".hbspt-forms-1.2"></noscript>
                                    <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                        <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                        <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                            <%--<input type="submit" value="Register" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0">--%>
                                            <div class="g-recaptcha" data-sitekey="6Lc1qSUTAAAAAPrvNw45yv-TJN0N8A4CBIQx8k4Y"></div>
                                            <asp:LinkButton ID="lbRegister" runat="server" CssClass="hs-button primary large" OnClick="lbRegister_Click">Register</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of column2 -->

                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlResetPassword" runat="server" DefaultButton="lbLogin">
        <h2>Reset Password</h2>
        <div class="section">
            <div id="section4" class="dual-column__wrapper background__white">
                <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__small">
                    <div class="col6 content__container">
                        <h3 class="">Password Reset</h3>
                        <div class="supporting-content__container ">
                            <div class="supporting-content   icon-list__container icon-list__checkmark">
                                <p>Use this form to reset your password if:</p>
                                <ul>
                                    <li>You have forgotten your password</li>
                                    <li>You want to change your password to something new</li>
                                    <li>It has been more than 30 days since your last successful login</li>
                                </ul>
                                <p>&nbsp;</p>
                            </div>
                        </div>
                    </div>
                    <!-- end of column1 -->
                    <!-- start of column2 -->
                    <div class="col6 content__container">
                        <div class="form__container form__container-boxed">
                            <div class="hs-richtext hs-main-font-element" data-reactid=".hbspt-forms-1.1:$0.0">
                                <h3>Password Reset</h3>
                                <p>&nbsp;</p>
                                <p>Please enter your username or email below, to initiate a password reset.</p>
                            </div>
                            <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0.$email">
                                <div class="input" data-reactid=".hbspt-forms-1.1:$0.$email.$email">
                                    <asp:TextBox ID="txtPasswordReset" runat="server" class="hs-input" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />
                                </div>
                            </div>
                        </div>
                        <asp:LinkButton ID="lbReset" runat="server" OnClick="lbReset_Click" CssClass="hs-button primary large">Reset My Password</asp:LinkButton>
                    </div>
                </div>
            </div>
            <!-- end of column2 -->
        </div>
    </asp:Panel>
    <h2>Temporary Password Reset</h2>
    <div class="section">
        <asp:Panel ID="pnlTempPasswordReset" runat="server">
            <div id="hs_cos_wrapper_widget_1550858841443" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
                <div id="section1" class="dual-column__wrapper background__white">
                    <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__small">
                        <div class="col6 content__container">
                            <h3 class="">Change Your Temporary Password</h3>
                            <div class="supporting-content__container ">
                                <div class="supporting-content   icon-list__container icon-list__checkmark">
                                    <p>Your password is currently set to a temporary password, and has been emailed to you.&nbsp; Please change it now to something more secure.&nbsp;</p>
                                    <ul>
                                        <li>Must be at least 3 non-alphanumberic characters (#,*, !, etc)</li>
                                        <li>For account security, do not re-use passwords from other sites.</li>
                                        <li>Consider using a password manager for increased account security.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end of column1 -->
                        <!-- start of column2 -->
                        <div class="col6 content__container">
                            <div class="form__container form__container-boxed">
                                <h3 id="hs_cos_wrapper_form_601518990_title" class="hs_cos_wrapper form-title" data-hs-cos-general-type="widget_field" data-hs-cos-type="text"></h3>

                                <div id="hs_form_target_form_601518990">
                                    <div class="hs_temporary_password hs-temporary_password hs-fieldtype-text field hs-form-field">
                                        <h6>Temporary Password</h6>
                                        <label class="hs-field-desc" style="display: block;">Please enter the temporary password that was emailed to your account.</label>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$0.$temporary_password">
                                            <%--Temp Password Textbox--%>
                                            <%--<input id="temporary_password-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6598" class="hs-input" type="text" name="temporary_password" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$0.$temporary_password.0" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">--%>
                                            <asp:TextBox ID="txtTempPassword" runat="server" CssClass="hs-input"  TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="hs_temporary_password_new hs-temporary_password_new hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$1">
                                        <h6>New Password</h6>
                                        <label class="hs-field-desc" style="display: block;">Please enter a new password</label>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$1.$temporary_password_new">
                                            <%--New Password Textbox--%>
                                            <%--<input id="temporary_password_new-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6598" class="hs-input" type="text" name="temporary_password_new" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$1.$temporary_password_new.0">--%>
                                            <asp:TextBox ID="txtNewPassword" runat="server" CssClass="hs-input"  TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="hs_temporary_password_repeat hs-temporary_password_repeat hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$2">
                                        <h6>Repeat New Password</h6>
                                        <label class="hs-field-desc" style="display: block;" data-reactid=".hbspt-forms-1.1:$2.1">Please repeat the new password</label>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$2.$temporary_password_repeat">
                                            <%--New Password Textbox--%>
                                            <%--<input id="temporary_password_repeat-a3a35734-9147-4125-beb1-ceb1c7d69f7e_6598" class="hs-input" type="text" name="temporary_password_repeat" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$2.$temporary_password_repeat.0">--%>
                                            <asp:TextBox ID="txtRepeatNewPassword" runat="server" CssClass="hs-input" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <noscript data-reactid=".hbspt-forms-1.2"></noscript>
                                    <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                        <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                        <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                            <%--Change Password Button--%>
                                            <%--<input type="submit" value="Change Temporary Password" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0">--%>
                                            <asp:LinkButton ID="lbChanceTemporaryPassword" runat="server" OnClick="lbChanceTemporaryPassword_Click" CssClass="hs-button primary large">Change Temporary Password</asp:LinkButton>
                                        </div>
                                    </div>
                                    <noscript data-reactid=".hbspt-forms-1.6"></noscript>
                                    <input name="hs_context" type="hidden" value="{&quot;rumScriptExecuteTime&quot;:797.4299999768846,&quot;rumServiceResponseTime&quot;:981.8399999930989,&quot;rumFormRenderTime&quot;:1.8250000139232725,&quot;rumTotalRenderTime&quot;:983.8799999852199,&quot;rumTotalRequestTime&quot;:182.4999999953434,&quot;pageUrl&quot;:&quot;https://info.sensiblemicro.com/login-1&quot;,&quot;pageTitle&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;source&quot;:&quot;FormsNext-static-3.243&quot;,&quot;timestamp&quot;:1555083200169,&quot;userAgent&quot;:&quot;Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36&quot;,&quot;originalEmbedContext&quot;:{&quot;formsBaseUrl&quot;:&quot;/_hcms/forms/&quot;,&quot;portalId&quot;:&quot;1878634&quot;,&quot;formId&quot;:&quot;a3a35734-9147-4125-beb1-ceb1c7d69f7e&quot;,&quot;formInstanceId&quot;:&quot;6598&quot;,&quot;pageId&quot;:&quot;8843825770&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;inlineMessage&quot;:true,&quot;rawInlineMessage&quot;:&quot;Thanks for submitting the form.&quot;,&quot;hsFormKey&quot;:&quot;7749a8269b39c5f6fc702131002dc4bf&quot;,&quot;target&quot;:&quot;#hs_form_target_form_601518990&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;formData&quot;:{&quot;cssClass&quot;:&quot;hs-form stacked hs-custom-form&quot;},&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;},&quot;pageId&quot;:&quot;8843825770&quot;,&quot;pageName&quot;:&quot;Login to the Sensible Micro Customer Portal&quot;,&quot;formInstanceId&quot;:&quot;6598&quot;,&quot;rawInlineMessage&quot;:&quot;Thanks for submitting the form.&quot;,&quot;hsFormKey&quot;:&quot;7749a8269b39c5f6fc702131002dc4bf&quot;,&quot;formTarget&quot;:&quot;#hs_form_target_form_601518990&quot;,&quot;correlationId&quot;:&quot;676b3d62-9293-4ce7-80bb-4bdfbb98047c&quot;,&quot;contentType&quot;:&quot;landing-page&quot;,&quot;hutk&quot;:&quot;dccea9473dc441e492582e0627efd2a7&quot;}" data-reactid=".hbspt-forms-1.7"><iframe name="target_iframe_a3a35734-9147-4125-beb1-ceb1c7d69f7e_6598" style="display: none;" data-reactid=".hbspt-forms-1.8"></iframe>
                                </div>









                            </div>



                        </div>
                        <!-- end of column2 -->

                    </div>
                </div>
            </div>

        </asp:Panel>
    </div>

</asp:Content>

