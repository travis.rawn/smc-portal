﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="datetime_picker_tests.aspx.cs" Inherits="secure_admin_sandbox_datetime_picker_tests" %>

<%@ Register Src="~/assets/controls/datetime_flatpicker.ascx" TagPrefix="uc1" TagName="datetime_flatpicker" %>


<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

 



    <h4>Jquery DateTimePicker</h4>
    <div class="row">
        <div class="col-sm-6">
            <label>Date & Time</label>
            <uc1:datetime_flatpicker runat="server" ID="dtpDateTime" />           

        </div>
        <div class="col-sm-6">
            <label>DateOnly</label>
            <uc1:datetime_flatpicker runat="server" ID="dtpDate" />            
        </div>
    </div>    
        
</asp:Content>


