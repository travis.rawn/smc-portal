﻿using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_silicon_expet_top_customers_taxonomy : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    DateTime startDate;
    DateTime endDate;
    silicon_expert se = new silicon_expert();


    [Serializable]
    protected class TopCustomerMFGDetail
    {
        public string partNumber { get; set; }
        public string customerName { get; set; }
        public string siliconExpertMFG { get; set; }
        public string rzMFG { get; set; }
        public string type { get; set; } //Taxonomy or "PIName", this is likely the "Type"
        public string description { get; set; }
        public string taxonomyPath { get; set; }
        public long mfgCount { get; set; }
        public double totalSales { get; set; }
    }

    protected List<TopCustomerMFGDetail> TopCustomerMFGDetailList
    {
        get
        {
            return (List<TopCustomerMFGDetail>)ViewState["TopCustomerMFGDetailList"];
        }
        set
        {
            ViewState["TopCustomerMFGDetailList"] = value;
        }
    }

    //protected DataSet PartSummaryDataSet
    //{
    //    get
    //    {
    //        return (DataSet)ViewState["PartSummaryDataSet"];
    //    }
    //    set
    //    {
    //        ViewState["PartSummaryDataSet"] = value;
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDates();
    }

    private void LoadDates()
    {

        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Today.Date.AddMonths(-6);
            dtpEnd.SelectedDate = DateTime.Today.Date;
        }

        startDate = dtpStart.SelectedDate.Value.Date;
        endDate = dtpEnd.SelectedDate.Value.Date;


    }

    private void LoadData()
    {
        List<string> topCustomerIds = new List<string>();
        using (RzDataContext rdc = new RzDataContext())
        {

            //dictTopCustomers = rdc.orddet_lines.Where(w => (w.orderid_invoice ?? "").Length > 0 && (w.manufacturer ?? "").Length > 0 && w.date_created.Value.Date >= startDate.Date).ToDictionary(d => d.customer_uid, d => d.customer_name);
            BuildMfgDetailList(rdc);

        }

    }

    private void BuildMfgDetailList(RzDataContext rdc)
    {

        //First I need Rz Specific data, distinct Partnumbers grouped by MFG, and then Count, then SUM dollars.
        var baseQuery = rdc.orddet_lines.Where(
            w => w.date_created.Value.Date >= startDate.Date
        && w.date_created.Value <= endDate.Date
        && w.fullpartnumber != "GCAT - LAB ANALYSIS"
        && (w.orderid_invoice ?? "").Length > 0
        && w.total_price.Value > 0
         //&& w.customer_name == "Trane Technologies (Trane U.S. Inc.)"
         //&& w.fullpartnumber == "H11L1SR2VM"	
         ).Select(s => new
         {
             s.customer_uid,
             s.customer_name,
             Part = s.fullpartnumber.Replace(" ", "").Trim(),
             MFG = s.manufacturer.Replace(" ", "").Trim(),
             TotalPrice = s.total_price.Value
         });

        var GroupQuery = baseQuery.GroupBy(g => new
        {
            g.customer_uid,
            g.Part,
            g.MFG

        }).Select(s => new
        {
            Customer = s.Max(m => m.customer_name),
            CustomerID = s.Max(m => m.customer_uid),
            Part = s.Key.Part,
            MFG = s.Key.MFG,
            Count = s.Key.MFG.Count(),
            TotalPrice = s.Sum(ss => ss.TotalPrice)
        }).OrderByDescending(o => o.Count);



        ////Get a List of Companies
        //List<company> companyList = new List<company>();
        ////companyList = rdc.ordhed_invoices.Join(j => rdc.companies on)
        //List<orddet_line> invoicedLines = rdc.orddet_lines.Where(w => w.orderdate_invoice >= new DateTime(2022, 06, 01) && (w.fullpartnumber ?? "").Length > 0).ToList();
        //List<string> invoicedCompanyIds = invoicedLines.Select(s => s.customer_uid).Distinct().ToList();
        ////foreach distinct companyID, get company, and partnumbers.

        //List<string> customerNamewList = GroupQuery.Select(s => s.Customer).Distinct().ToList();
        List<string> customerIdList = GroupQuery.Select(s => s.CustomerID).Distinct().ToList();
        TopCustomerMFGDetailList = new List<TopCustomerMFGDetail>();
        foreach (string id in customerIdList)
        {
            string companyID = id;
            var companyQuery = GroupQuery.Where(w => w.CustomerID == id);


            string companyName = companyQuery.Max(m => m.Customer) ?? "N/A";
            List<string> companyParts = companyQuery.Select(s => s.Part).Distinct().ToList();
            DataSet ds = SiliconExpertLogic.GetListPartSearchDataSetXML(companyParts);
            //PartSummaryDataSet = se.GetListPartSearchDataSetXML(uniquePartNumbers);
            if (ds == null)
                continue;
            if (ds.Tables.Count == 0)
                continue;
            if (ds.Tables[4] == null)
                continue;

            //Part-Level detail
            foreach (DataRow dr in ds.Tables[4].Rows)
            {
                string Part = dr.Table.Columns.Contains("PartNumber") ? dr["PartNumber"].ToString().Trim().ToUpper() : "";
                if (string.IsNullOrEmpty(Part))
                    continue;

                //Get the Silicon Expoert MFG (May not match what salesperson has in Rz)
                string SliconExpertMFG = dr.Table.Columns.Contains("Manufacturer") ? dr["Manufacturer"].ToString().Trim().ToUpper() : "";
                if (string.IsNullOrEmpty(SliconExpertMFG))
                    continue;
                //Filter GroupQuery to Part
                var partQuery = companyQuery.Where(w => w.Part == Part).GroupBy(g => new { g.Part, g.MFG });
                if (!partQuery.Any())
                    continue;
                //Get the Rz MFG
                string RzMFG = partQuery.Select(s => s.Max(m => m.MFG)).FirstOrDefault();

                TopCustomerMFGDetail td = new TopCustomerMFGDetail();
                td.customerName = companyName;
                //If RzMFG != SiliconExportMfg - adjust string to reflect
                td.siliconExpertMFG = SliconExpertMFG ?? "N/A";
                td.rzMFG = RzMFG ?? "N/A";
                //if (SliconExpertMFG != RzMFG)
                //    td.mfgName = SliconExpertMFG + "(Rz: " + RzMFG + ")";
                //else
                //    td.mfgName = SliconExpertMFG;
                td.partNumber = Part;
                td.type = dr.Table.Columns.Contains("PlName") ? dr["PlName"].ToString() : "N/A";
                td.description = dr.Table.Columns.Contains("Description") ? dr["Description"].ToString() : "N/A";
                td.taxonomyPath = dr.Table.Columns.Contains("TaxonomyPath") ? dr["TaxonomyPath"].ToString() : "N/A";
                //td.mfgCount = partQuery.GroupBy(g => g.Key.MFG).Count();

                //td.totalSales = )
                //else td.totalSales = 0;
                td.totalSales = partQuery.Sum(s => s.Sum(ss => ss.TotalPrice));
                TopCustomerMFGDetailList.Add(td);

            }

        }


        //Group these results, for the final list.
        TopCustomerMFGDetailList = TopCustomerMFGDetailList.GroupBy(g => new { g.partNumber, g.rzMFG, g.siliconExpertMFG }).Select(s => new TopCustomerMFGDetail {
            partNumber = s.Key.partNumber,
            rzMFG = s.Key.rzMFG,
            siliconExpertMFG = s.Key.siliconExpertMFG,
            type = s.Max(m => m.type),
            description = s.Max(m => m.description), 
            taxonomyPath = s.Max(m => m.taxonomyPath),
            mfgCount = s.Key.rzMFG.Count(),
            totalSales = s.Sum(ss => ss.totalSales)

        }).ToList();

    }

    private void LoadGrid()
    {
        smdt.dataSource = TopCustomerMFGDetailList;
        smdt.loadGrid();

    }

    protected void lbExport_Click(object sender, EventArgs e)
    {
        try
        {
            ExportData();
            
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void ExportData()
    {
        tools.ExportListToCsv(TopCustomerMFGDetailList, "TopCustomerMFG_Data_"+dtpStart.SelectedDate.Value.Date.ToShortDateString() + "-"+dtpEnd.SelectedDate.Value.Date.ToShortDateString());
        tools.HandleResult("success", "Yay, your export has been downloaded.");
    }

    protected void lbSearch_Click(object sender, EventArgs e)
    {
        try
        {
            LoadData();
            LoadGrid();
            ExportData();
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }
}