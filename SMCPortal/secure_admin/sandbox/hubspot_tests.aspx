﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="hubspot_tests.aspx.cs" Inherits="hs_test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent1" runat="Server">
    <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
        <div id="hs_cos_wrapper_module_1557258237389588" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
            <div class="ribbon-bg" style="background-image: url(https://cdn2.hubspot.net/hub/1878634/hubfs/Sensible_Micro_Early2019/Banner/Curve-gray.png?width=900&amp;name=Curve-gray.png);">

                <div class="section-container__narrow padding-top__large padding-bottom__medium section-header__container">
                    <div class="main-content align__center">
                        <h1 class="relative">Sensible Micro Portal
                        </h1>
                        <div class="paragraph-large ">
                            <p>Placeholder text</p>
                        </div>
                    </div>
                </div>

                <div class="section-container padding-top__large padding-bottom__medium section-header__container">
                    <div class="portal-card-grid">

                        <a href="https://portal.sensiblemicro.com/public/search/part_search.aspx" class="portal-card portal-card-grid__item">
                            <img src="https://www.sensiblemicro.com/hubfs/icon__search__lblue.svg" alt="" class="portal-card__icon">
                            <h2 class="portal-card__title">Preferred Parts Search
                            </h2>
                            <div class="portal-card__desc">
                                <p>Gain component intelligence such as Datasheet, PDN, Environmental Data as well as view our preferred market available stock.</p>
                            </div>
                        </a>

                        <a href="https://portal.sensiblemicro.com/secure_members/buyers/Orders.aspx" class="portal-card portal-card-grid__item">
                            <img src="https://www.sensiblemicro.com/hubfs/parts-icon.svg" alt="" class="portal-card__icon">
                            <h2 class="portal-card__title">Order Center
                            </h2>
                            <div class="portal-card__desc">
                                <p>Track your orders, view their status, and cross reference them with our component intelligence platform for Datasheet, PDN, and other useful data.</p>
                            </div>
                        </a>

                        <a href="https://portal.sensiblemicro.com/secure_members/qc_center/my_qc.aspx" class="portal-card portal-card-grid__item">
                            <img src="https://www.sensiblemicro.com/hubfs/check.svg" alt="" class="portal-card__icon">
                            <h2 class="portal-card__title">Quality Center
                            </h2>
                            <div class="portal-card__desc">
                                <p>View your IDEA 1010 Inspection reports, including hi-res imagery captured in our state of the art in-house component inspection facility.</p>
                            </div>
                        </a>

                    </div>
                </div>


            </div>

        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent2" runat="Server">
    <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
        <div id="hs_cos_wrapper_module_1557323488885813" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">

            <div class="background__lightestGrey">
                <div class="section-container padding-top__large padding-bottom__medium section-header__container">

                    <div class="main-content">
                        <h2 class="side-title relative">Featured Products
                        </h2>
                    </div>

                    <div class="prod-card-slider js-prod-slider">

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/aw-parts/TIM-5H-0-004.jpg);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">DLP-FPGA-M</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">DLP Design</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">292818</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=38027114">More Information →</a>
                            </div>
                        </div>

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/DRAM.png);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">SRT-96B-MEZ-FPGA</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">Shiratech Solutions Ltd</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">321</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=405545145">More Information →</a>
                            </div>
                        </div>

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/QFP.png);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">EPF10K30RC240-4N</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">Intel</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">28000</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=19967377">More Information →</a>
                            </div>
                        </div>

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/aw-parts/XCS20XL-4VQG100I.jpg);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">M3300A</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">Keysight Technologies, Inc</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">32</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=84683731">More Information →</a>
                            </div>
                        </div>

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/aw-parts/GWIXP425ABDT.jpg);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">DRA-MJS-VW1-1</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">Fujitsu</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">522</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=75643538">More Information →</a>
                            </div>
                        </div>

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/Email_2019/power-cords-fiox.png);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">SRT-96B-MEZ-FPGA-2</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">Shiratech Solutions Ltd</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">121</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=405545145">More Information →</a>
                            </div>
                        </div>

                        <div class="prod-card prod-card-slider__slide js-slide">
                            <div class="prod-card__graphic" style="background-image: url(https://www.sensiblemicro.com/hubfs/aw-parts/LUPXA255A0E400.jpg);"></div>
                            <div class="prod-card__body">
                                <dl class="prod-card__details detail-list">
                                    <dt class="detail-list__title">Part Number</dt>
                                    <dd class="detail-list__value">M3300A-2</dd>
                                    <dt class="detail-list__title">Manufacturer</dt>
                                    <dd class="detail-list__value">Keysight Technologies, Inc</dd>
                                    <dt class="detail-list__title">Quantity in Stock</dt>
                                    <dd class="detail-list__value">98</dd>
                                </dl>
                                <a class="prod-card__link" href="https://portal.sensiblemicro.com/public/search/part_detail.aspx?id=84683731">More Information →</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>
    <!--end widget-span -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent3" runat="Server">
    <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
        <div id="hs_cos_wrapper_module_1557320680122665" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">

            <div class="background__blue offer-section">

                <div class="section-container__narrow padding-top__large padding-bottom__medium section-header__container">
                    <div class="main-content align__center">
                        <h2 class="relative">The Sensible Approach
                        </h2>
                        <div class="paragraph-large">
                            <p>Are you looking for the perfect soucing solution? Request a quote today to find the parts you need.</p>
                            <p><a href="https://www.sensiblemicro.com/contact-us" target="_blank">Request a Quote</a></p>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!--end widget-span -->
</asp:Content>

