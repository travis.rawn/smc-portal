﻿using SensibleDAL;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_serilog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lbSimpleLogTest_Click(object sender, EventArgs e)
    {
        Log.Logger = new LoggerConfiguration()
      .WriteTo.Console()
      .WriteTo.File("C:\\Users\\kevint\\Desktop\\Logs\\log.log")
      .CreateLogger();

        foreach (var i in Enumerable.Range(0, 100))
        {
            Log.Information("Hello loop {Counter}", i);
        }
        Log.CloseAndFlush();
    }

    protected void lbPortalLogTest_Click(object sender, EventArgs e)
    {
        
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Error Test!");
    }
}