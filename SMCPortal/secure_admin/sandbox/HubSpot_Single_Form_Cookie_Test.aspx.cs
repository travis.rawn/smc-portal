﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_HubSpot_Single_Form_Cookie_Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Do Nothing   
    }
    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        SetTestCookie("FartCookie");
        SetHubSpotCookie();
    }

    protected void SetTestCookie(string Name)
    {
        HttpCookie SMTestCookie = new HttpCookie(Name);
        SMTestCookie.Value = "SM_Test_Cookie";
        SMTestCookie.Expires = DateTime.Now.AddHours(1);
        Response.Cookies.Add(SMTestCookie);
        if (Request.Cookies.Get("SM_Test_Cookie") != null)
            pnlSuccess.Visible = true;
        else
        {
            pnlError.Visible = true;
            lblError.Text = "Error:  Cookie not found.";
        }
    }

    protected void SetHubSpotCookie()
    {
        // Build dictionary of field names/values (must match the HS field names)
        Dictionary<string, string> dictFormValues = new Dictionary<string, string>();
        dictFormValues.Add("email", txtEmail.Text);
        // Form Variables (from the HubSpot Form Edit Screen)
        int intPortalID = 1878634; //place your portal ID here
        string strFormGUID = "3220ecd1-85fa-48a5-a5e9-6a33e51dcf76"; //place your form guid here

        // Tracking Code Variables
        string strHubSpotUTK = Request.Cookies["hubspotutk"].Value;
        string strIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

        //Retrieve Hubspot cookie then save to client
        //SetTestCookie("TestCookie");
        HttpCookie HubSpotCookie = Request.Cookies["hubspotutk"];
        Response.Cookies.Add(HubSpotCookie);

        // Page Variables
        string strPageTitle = this.Title;
        string strPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

        // Do the post, returns true/false
        string strError = "Oh snap, that didn't work!";
        bool blnRet = Post_To_HubSpot_FormsAPI(intPortalID, strFormGUID, dictFormValues, strHubSpotUTK, strIpAddress, strPageTitle, strPageURL, ref strError);
        if (blnRet)
        {
            pnlSuccess.Visible = true;
        }
        else
        {
            pnlError.Visible = true;
            lblError.Text = strError;
        }

    }




    /// 
    /// This helper function sends data to the the HubSpot Forms API
    /// 
    /// HubSpot Portal ID, or 'HUB ID'
    /// Unique ID for the form
    /// Dictionary containing all of the field names/values
    /// UserToken from the visitor's browser
    /// IP Address of the visitor
    /// Title of the page they visited
    /// URL of the page they visited
    /// 
    /// 
    public bool Post_To_HubSpot_FormsAPI(int intPortalID, string strFormGUID, Dictionary<string, string> dictFormValues, string strHubSpotUTK, string strIpAddress, string strPageTitle, string strPageURL, ref string strMessage)
    {

        // Build Endpoint URL
        string strEndpointURL = string.Format("https://forms.hubspot.com/uploads/form/v2/{0}/{1}", intPortalID, strFormGUID);

        // Setup HS Context Object
        Dictionary<string, string> hsContext = new Dictionary<string, string>();
        hsContext.Add("hutk", strHubSpotUTK);
        hsContext.Add("ipAddress", strIpAddress);
        hsContext.Add("pageUrl", strPageURL);
        hsContext.Add("pageName", strPageTitle);

        // Serialize HS Context to JSON (string)
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        string strHubSpotContextJSON = json.Serialize(hsContext);

        // Create string with post data
        string strPostData = "";

        // Add dictionary values
        foreach (var d in dictFormValues)
        {
            strPostData += d.Key + "=" + Server.UrlEncode(d.Value) + "&";
        }

        // Append HS Context JSON
        strPostData += "hs_context=" + Server.UrlEncode(strHubSpotContextJSON);

        // Create web request object
        System.Net.HttpWebRequest r = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strEndpointURL);

        // Set headers for POST
        r.Method = "POST";
        r.ContentType = "application/x-www-form-urlencoded";
        r.ContentLength = strPostData.Length;
        r.KeepAlive = false;

        // POST data to endpoint
        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(r.GetRequestStream()))
        {
            try
            {
                sw.Write(strPostData);
            }
            catch (Exception ex)
            {
                // POST Request Failed
                strMessage = ex.Message;
                return false;
            }
        }

        return true; //POST Succeeded

    }


}

