﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_tests_sandbox_AutoGenerateMenuLinks : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        listallfilesaslnks();
    }
    protected void listallfilesaslnks()
    {
        DirectoryInfo dir;
        StringBuilder sb = new StringBuilder();
        FileInfo[] files;

        dir = new DirectoryInfo(Server.MapPath("."));
        files = dir.GetFiles();
        foreach (FileInfo f in files)
        {
            
            if (Path.GetExtension(f.Name) == ".aspx")
            {
                sb.Append("<li><a href=\"" + f.Name.ToString() + "\">");
                sb.Append(f.Name.ToString() + "</li></a><br />");
            }
           
        }
        lblLinks.Text = sb.ToString();
    }
}