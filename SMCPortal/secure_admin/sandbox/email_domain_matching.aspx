﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="email_domain_matching.aspx.cs" Inherits="secure_admin_sandbox_email_domain_matching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-4">
            <asp:CheckBox ID="cbxCreateContactIfMissing" runat="server" Text="Create contact if not found?" />
            <div class="row">
                <div class="col-sm-6">
                    <asp:TextBox ID="txtCustomerEmail" runat="server" Placeholder="Please enter an email address"></asp:TextBox>
                    <asp:TextBox ID="txtFirstName" runat="server" Placeholder="First Name"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtPhone" runat="server" Placeholder="Phone Number"></asp:TextBox>
                    <asp:TextBox ID="txtLastName" runat="server" Placeholder="Last Name"></asp:TextBox>

                </div>
            </div>
            <asp:Button ID="btnMatchEmail" runat="server" OnClick="btnMatchEmail_Click" Text="Match" CssClass="hs-button primary btn-block" />
        </div>

        <div class="col-sm-4">
            <h4>Match Results</h4>

            <label>Contact Name: </label>
            <asp:Label ID="lblContactName" runat="server"></asp:Label><br />
            <label>Contact Email: </label>
            <asp:Label ID="lblContactEmail" runat="server"></asp:Label><br />
            <label>Company Name: </label>
            <asp:Label ID="lblCompanyName" runat="server"></asp:Label><br />
            <label>Agent Name: </label>
            <asp:Label ID="lblAgentName" runat="server"></asp:Label><br />

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Italic="true" Font-Size="Small"></asp:Label>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

