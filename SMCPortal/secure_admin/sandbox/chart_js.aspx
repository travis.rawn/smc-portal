﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="chart_js.aspx.cs" Inherits="secure_admin_sandbox_chart_js" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="wrapper" style="position: relative; height: 400px;">
        <canvas id="myChart"></canvas>
    </div>
    <div style="margin-top: 30px;">
        <canvas id="canvas"></canvas>
    </div>
    <br />
    <br />
    <button id="randomizeData" type="button">Randomize Data</button>
    <button id="addDataset" type="button">Add Dataset</button>
    <button id="removeDataset" type="button">Remove Dataset</button>
    <button id="addData" type="button">Add Data</button>
    <button id="removeData" type="button">Remove Data</button>

    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    responsive: true,
                    maintainAspectRatio: false
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>





    <script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "My First dataset",
                    backgroundColor: 'rgba(4, 8, 32, 0.8)',
                    borderColor: 'rgba(87, .2, 14, 74,.11)',
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }, {
                    label: "My Second dataset",
                    fill: false,
                    backgroundColor: 'rgba(59, .6, 7, 2)',
                    borderColor: 'rgba(.09, 84, 31, 9, .5)',
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };
        window.onload = function () {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };
        document.getElementById('randomizeData').addEventListener('click', function () {
            config.data.datasets.forEach(function (dataset) {
                dataset.data = dataset.data.map(function () {
                    return randomScalingFactor();
                });
            });
            window.myLine.update();
            return false;
        });
        //var colorNames[] = {'farts','teal','hummus','sandy','pearl','diver','slick'};
        document.getElementById('addDataset').addEventListener('click', function () {
            var colorName = dynamicColors();
            var newColor = dynamicColors();
            var newDataset = {
                label: 'Dataset ' + config.data.datasets.length,
                backgroundColor: newColor,
                borderColor: newColor,
                data: [],
                fill: false
            };
            for (var index = 0; index < config.data.labels.length; ++index) {
                newDataset.data.push(randomScalingFactor());
            }
            config.data.datasets.push(newDataset);
            window.myLine.update();
            return false;
        });
        document.getElementById('addData').addEventListener('click', function () {
            if (config.data.datasets.length > 0) {
                var month = MONTHS[config.data.labels.length % MONTHS.length];
                config.data.labels.push(month);
                config.data.datasets.forEach(function (dataset) {
                    dataset.data.push(randomScalingFactor());
                });
                window.myLine.update();
                return false;
            }
        });
        document.getElementById('removeDataset').addEventListener('click', function () {
            config.data.datasets.splice(0, 1);
            window.myLine.update();
            return false;
        });
        document.getElementById('removeData').addEventListener('click', function () {
            config.data.labels.splice(-1, 1); // remove the label first
            config.data.datasets.forEach(function (dataset, datasetIndex) {
                dataset.data.pop();
            });
            window.myLine.update();
            return false;
        });


        function randomScalingFactor() {
            return Math.random() * (0 - 100) + 0;
        }
    </script>


    <script>
        window.onload = function () {
            multipleTest();
        };
        function multipleTest() {
            var config = {
                type: 'line',
                data: {
                    labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                    datasets: [
                    {
                        label: '1st dataset',
                        data: ["45", "3", "14", "88", "54", "13", "20"]
                    },
                    {
                        label: '2nd dataset',
                        data: ["3", "97", "54", "21", "79", "40", "15"]

                    },
                    ]
                }
            };
            var myChart = new Chart(ctx, config);

        };
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

