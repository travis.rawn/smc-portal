﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="gauge_charting.aspx.cs" Inherits="secure_admin_sandbox_gauge_charting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

   
    <script src="/Content/plugins/gauge_js/gauge.min.js"></script>
    <script src="/Content/plugins/svg-gauge/dist/gauge.min.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h2 class="well">HTML JQuery Gauge Plugin</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-primary">
        <div class="panel-heading">Notes</div>
        <div class="panel-body">This is nicely responsive.  Lacking tickmarks, etc. that jpPlot has.</div>
    </div>
    <h2>Gauge.js</h2>
    <a href="https://bernii.github.io/gauge.js/" target="_blank">Source</a>
    <canvas id="gauge-js"></canvas>


    <h2>SVG Gauge</h2>
    <a href="https://github.com/naikus/svg-gauge" target="_blank">Source</a>
    <style>
        .gauge-container {
            max-width: 300px;
        }

            .gauge-container > .gauge .dial {
                stroke: #334455;
                stroke-width: 10;
            }

            .gauge-container > .gauge .value {
                stroke: orange;
                stroke-dasharray: none;
                stroke-width: 13;
            }

            .gauge-container > .gauge .value-text {
                fill: #ccc;
                font-weight: 100;
                font-size: 1em;
            }
    </style>
    <div id="svgGauge" class="gauge-container" runat="server"></div>


    <script>        

        var Value = 2;

        $(function loadSVGGauge() {
            // Create a new Gauge
            var svgGauge = Gauge(
                document.getElementById("MainContent_svgGauge"), {
                    min: 0,
                    max: 5,
                    dialStartAngle: 180,
                    dialEndAngle: 0,
                    value: Value,
                    color: function (value) {
                        switch (value) {

                            case 2:
                                return "#4cb944"; //Green
                            case 3:
                                return "#FFBA08"; //Yellow                       
                            case 4:
                                return "#EE2738"; //Red  
                            case 5:
                                return "#5F0F40"; //Tyrian Purple  
                            default:
                                return "#C2B8B2";//Silver

                        }
                    }
                });


            //Randomly set the value and animate
            setInterval(function () {

                // update the above chart...               
                var value = Math.floor(Math.random() * 6);


                // Set value and animate (value, animation duration in seconds)
                svgGauge.setValueAnimated(value, 1);
            }, 1800);
        });



        $(function loadGaugeJs() {
            var gauge = new RadialGauge({
                renderTo: 'gauge-js',
                width: 300,
                height: 300,
                units: "Risk Score",
                value: Value,
                minValue: 0,
                startAngle: 90,
                ticksAngle: 180,
                valueBox: true,
                maxValue: 6,
                majorTicks: [
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                ],
                minorTicks: 2,
                strokeTicks: true,
                highlights: [
                    {
                        "from": 0,
                        "to": 1,
                        "color": "rgba(255,255,255, .75)" //White
                    },
                    {
                        "from": 1,
                        "to": 2,
                        "color": "rgba(194, 184, 178, .2)" //Pale Silver

                    },
                    {
                        "from": 2,
                        "to": 3,
                        "color": "rgba(76,185, 68, .75)" //Dark Pastel Green
                    },
                    {
                        "from": 3,
                        "to": 4,
                        "color": "rgba(36,110,185, .75)" //Spanish Blue
                    },
                    {
                        "from": 4,
                        "to": 5,
                        "color": "rgba(255, 186, 8, .75)" //Selective Yellow
                    },
                    {
                        "from": 5,
                        "to": 6,
                        "color": "rgba(238, 39, 56, .75)" //Imperial Red
                    }
                ],
                colorPlate: "#fff",
                borderShadowWidth: 0,
                borders: false,
                needleType: "arrow",
                needleWidth: 5,
                needleCircleSize: 2,
                needleCircleOuter: true,
                needleCircleInner: false,
                animationDuration: 1500,
                animationRule: "linear"
            }).draw();

            //Randomly set the value and animate
            setInterval(function () {

                // update the above chart...
                var value = Math.floor(Math.random() * 6);
                gauge.value = value;

                //    // Update the declarative chart...
                //    document.getElementById("gauge-a").setAttribute("data-value", value);
            }, 1800);
        });
        $(function loadHtmlGaugeMeter() {
            $(".GaugeMeter").gaugeMeter();
        });
    </script>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

