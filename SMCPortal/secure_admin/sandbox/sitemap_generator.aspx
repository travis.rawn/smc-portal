﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="sitemap_generator.aspx.cs" Inherits="secure_admin_sandbox_sitemap_generator" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:LinkButton ID="lbGenerateStockSiteMap" runat="server" OnClick="lbGenerateStockSiteMap_Click">Generate Stock SiteMap</asp:LinkButton><br />
    <asp:LinkButton ID="lbGenerateConsignSiteMap" runat="server" OnClick="lbGenerateConsignSiteMap_Click">Generate Consign SiteMap</asp:LinkButton><br />
    <asp:LinkButton ID="lbGenerateExcessSitemap" runat="server" OnClick="lbGenerateExcessSitemap_Click">Generate Excess SiteMap</asp:LinkButton><br />
    <asp:LinkButton ID="lbGenerateAllSitemaps" runat="server" OnClick="lbGenerateAllSitemaps_Click">Generate All SiteMaps</asp:LinkButton><br />
    <label>Results:</label><br />
    <asp:Label ID="lblSiteMapResults" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

