﻿using SensibleDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_Ajax_Webmethod_Tests : System.Web.UI.Page
{
    //silicon_expert si = new silicon_expert();
    string part = null;
   
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public  DataTable GetEnvironmentalData(string ComId = null)
    {

        if (ComId == null)
            ComId = "51448203";
        DataTable EnvDataTable = null;
        EnvDataTable = SiliconExpertLogic.GetEnvDataTableXML(ComId, part, Membership.GetUser().UserName);
        if (EnvDataTable != null)
        {

            EnvDataTable.Columns.Add("PartNumber");
            EnvDataTable.Columns.Add("ComID");
            SiliconExpertLogic.LogSEEnvironmentalDto(EnvDataTable);
            dtvEnvData.DataSource = EnvDataTable;
        }

        else dtvEnvData.DataSource = null;
        dtvEnvData.DataBind();
        return EnvDataTable;
    }

    [WebMethod]
    public static string TestMethod(string message)
    {
        return "The message is " + message;
    }

    //[WebMethod]
    //protected void GetPCNData(string PartNumber)
    //{
    //    DataTable PcnDataTable = null;
    //    PcnDataTable = si.GetPCNDataTableXML(part);
    //    if (PcnDataTable != null)
    //    {
    //        PcnDataTable.Columns.Add("PartNumber");
    //        si.LogSEPCNOperation(PcnDataTable);
    //        gvPcnData.DataSource = PcnDataTable;
    //    }
    //    else
    //        gvPcnData.DataSource = null;
    //    gvPcnData.DataBind();
    //}

}