﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_alerts : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void lbSuccess_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterClientScriptBlock(this, GetType(), "successKey", "successAlert();", false);
        string alertType = "fail";       
        string fadeDuration = "100";
        string message = "test message with duration of " + fadeDuration;
        Page page = HttpContext.Current.Handler as Page;
        tools.HandleResult(alertType, message);
        
    }

    
}