﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_hs_flex_test1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       LoadGrid();
    }

    private void LoadGrid()
    {
        List<company> companies = new List<company>();
        using (RzDataContext rdc = new RzDataContext())
        {
            companies = rdc.companies.Where(w => w.date_created >= new DateTime(2019, 01, 01)).Take(20).ToList();
        }

        var query = companies.Select(s => new
        {
            Company = s.companyname,
            Agent = s.agentname,
            Modified = s.date_modified.Value.ToShortDateString()


        });

        if (query.Any())
        {
            smdt.dataSource = query.OrderByDescending(o => o.Modified).ToList();
            smdt.loadGrid();

        }
    }
}