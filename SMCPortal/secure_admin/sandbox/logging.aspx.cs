﻿using SensibleDAL;
using System;


public partial class secure_admin_sandbox_logging : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void logBtn_Click(object sender, EventArgs e)
    {
       SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "That sure is one nice log mister!  Love, SMCPortal");
    }

    protected void logError_Click(object sender, EventArgs e)
    {
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Test Error");
    }
}