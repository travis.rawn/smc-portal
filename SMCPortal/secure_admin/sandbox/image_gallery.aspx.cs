﻿using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_image_gallery : System.Web.UI.Page
{
    SM_Quality_Logic ql = new SM_Quality_Logic();
    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreLoad(e);

        int GCATID = 1821;
        List<insp_images> listExtendedImages = ql.LoadInspectionImagery("GCAT", GCATID, "ExtendedImagery");
        iig.EnableControls = true;
        iig.ShowDescription = true;
        iig.LoadGallery(listExtendedImages, SM_Enums.InspectionType.gcat.ToString().ToUpper(), GCATID, "ExtendedImagery");
    }

    protected void Page_Load(object sender, EventArgs e)
    {

      
    }
    
    
}