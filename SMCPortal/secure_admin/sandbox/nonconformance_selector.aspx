﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="nonconformance_selector.aspx.cs" Inherits="secure_admin_sandbox_nonconformance_selector" %>

<%@ Register Src="~/assets/controls/sm_checkbox.ascx" TagPrefix="uc1" TagName="sm_checkbox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">



    <div class="row">
        <div class="col-sm-12">
            <asp:LinkButton ID="lbSave" CssClass="btn btn-default" runat="server" OnClick="lbSave_Click">Save</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#category1" role="button" aria-expanded="false" aria-controls="collapseExample">Category1</a>
            </p>

            <%--Category1--%>
            <div class="collapse" id="category1">
                <%--Subcategory1--%>
                <p><a data-toggle="collapse" href="#cat1sub1" role="button" aria-expanded="false" aria-controls="collapseExample">Cat1Sub1</a></p>
                <div class="collapse" id="cat1sub1">
                    <div id="cat1Sub1Checkboxes" runat="server">
                    </div>
                </div>
                <%--Subcategory2--%>
                <p><a data-toggle="collapse" href="#cat1sub2" role="button" aria-expanded="false" aria-controls="collapseExample">Cat1Sub2</a></p>
                <div class="collapse" id="cat1sub2">
                    <div id="cat1Sub2Checkboxes" runat="server">
                    </div>
                </div>
                <%--Subcategory3--%>
                <p><a data-toggle="collapse" href="#cat1sub3" role="button" aria-expanded="false" aria-controls="collapseExample">Cat1Sub3</a></p>
                <div class="collapse" id="cat1sub3">
                    <div id="cat1Sub3Checkboxes" runat="server">
                    </div>
                </div>
                <%--Subcategory4--%>
                <p><a data-toggle="collapse" href="#cat1sub4" role="button" aria-expanded="false" aria-controls="collapseExample">Cat1Sub4</a></p>
                <div class="collapse" id="cat1sub4">
                    <div id="cat1Sub4Checkboxes" runat="server">
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6">
        </div>
    </div>

    <script>

        $(function handleCollapse() {
            //For DIV  with class "collapse
            $(".collapse").each(function () {
                //Loop through all checkboxes, per section div, if any Checked, show div.
                //identify the collapse Div ID
                var collapseDiv = this;
                //Get a cound of all checked boxes in this section
                var $checkedBoxes = $(this).children().find('input[type=checkbox]:checked');
                //Collapse based on checked state
                if ($checkedBoxes.length > 0)
                    $(collapseDiv).collapse("show")
                else
                    $(collapseDiv).collapse("hide")
            });
        })
    </script>


</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

