﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="SensibleDAL_test.aspx.cs" Inherits="secure_admin_sandbox_SensibleDAL_test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div id="pageTitle">
        <h1>DAL Tests</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="card">
        <div class="row">
            <div class="col-sm-12">
                <asp:CheckBox runat="server" ID="cbxUseTest" Checked="true" Text="Use Test Database" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <asp:LinkButton ID="lbSubmit" runat="server" OnClick="lbSubmit_Click" Text="Agent Count" CssClass="btn-smc btn-smc-primary"></asp:LinkButton>
            </div>
            <div class="col-sm-6">
                <asp:Label ID="lblResult" Text="Result" runat="server"></asp:Label>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

