﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_datetime_flatpicker_tests : System.Web.UI.Page
{




    protected void Page_Load(object sender, EventArgs e)
    {

        DateTime testDate = new DateTime(2021, 5, 4, 1, 7, 2, DateTimeKind.Local);
        dtpDate.SelectedDate = testDate;
        dtpDateTime.SelectedDate = DateTime.Now;
        dtpDateTime.EnableTime = true;
    }


}