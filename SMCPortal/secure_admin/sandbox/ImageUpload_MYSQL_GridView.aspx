﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="ImageUpload_MYSQL_GridView.aspx.cs" Inherits="secure_admin_sandbox_ImageUpload_MYSQL_GridView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <asp:FileUpload ID="fuHeaderImageTop" runat="server" />
                <asp:Button Text="Upload" runat="server" OnClick="UploadFile" ID="btnHeaderImageTop" />
                <asp:GridView ID="gvHeaderImageTop" runat="server" AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound">
                    <Columns>
                        <asp:BoundField HeaderText="File Id" DataField="unique_id" />
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" Height="80" Width="80" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="col-sm-4">
                <asp:FileUpload ID="fuHeaderImageBot" runat="server" />
                <asp:Button Text="Upload" runat="server" OnClick="UploadFile" ID="btnHeaderImageBot" />
                <asp:GridView ID="gvHeaderImageBot" runat="server" AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound">
                    <Columns>                        
                        <asp:BoundField HeaderText="File Id" DataField="unique_id" />
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" Height="80" Width="80" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="col-sm-4">
                <asp:FileUpload ID="fuHeaderImageOther" runat="server" />
                <asp:Button Text="Upload" runat="server" OnClick="UploadFile" ID="btnHeaderImageOther" />
                <asp:GridView ID="gvHeaderImageOther" runat="server" AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound">
                    <Columns>
                        <asp:BoundField HeaderText="File Id" DataField="unique_id" />
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" Height="80" Width="80" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

