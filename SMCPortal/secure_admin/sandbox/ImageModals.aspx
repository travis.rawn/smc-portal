﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="ImageModals.aspx.cs" Inherits="secure_admin_sandbox_ImageModals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--  <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox();
        });
    </script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox({
                beforeShow: function () {
                    // set new fancybox width
                    var newWidth = this.width * 2;
                    // apply new size to img
                    $(".fancybox-image").css({
                        "width": newWidth,
                        "height": "auto"
                    });
                    // set new values for parent container
                    this.width = newWidth;
                    this.height = $(".fancybox-image").innerHeight();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <h2 class="well well-sm">Bootstrap Modal Images</h2>
    <p class="panel-heading">Experiments with modals in images using Bootstrap.</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
    <div class="container">
        <div class="row">
            <label>Fancybox:</label>
            <a class="fancybox" rel="group" href="../../Images/Headshots/Joe_Mar_160_200.jpg">
                <img src="../../Images/Headshots/Joe_Mar_160_200.jpg" class="img-responsive" /></a>
        </div>
    </div>
</asp:Content>

