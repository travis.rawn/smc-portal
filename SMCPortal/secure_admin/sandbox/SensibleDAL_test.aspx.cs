﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_admin_sandbox_SensibleDAL_test : System.Web.UI.Page
{
    string databaseType = "Live";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lbSubmit_Click(object sender, EventArgs e)
    {
        if (cbxUseTest.Checked)
            databaseType = "Test";
        string connSTring = DataLogic.GetRzConnectionString(cbxUseTest.Checked);
        using (RzDataContext rdc = new RzDataContext(connSTring))
        {
            int count = rdc.n_users.Count();
            lblResult.Text = count.ToString()+ " users in the "+ databaseType .ToUpper()+ " database";
        }
    }
}