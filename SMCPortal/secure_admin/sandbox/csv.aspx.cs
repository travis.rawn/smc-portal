﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_admin_test_csv : System.Web.UI.Page
{
    RzDataContext RZDC = new RzDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
   
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchRz();
    }

    protected void SearchRz()
    {
        var query = from c in RZDC.erai_imports
                    where c.fullpartnumber.Contains(txtSearch.Text)
                    select c;
        if (query.Any())
        {
            GridView1.DataSource = query;
            GridView1.DataBind();
            lblResults.Text = query.Count() + " total results.";
        }
        else
            lblResults.Text = "No results.";

    }

    protected void SearchCSV()
    {
        string[] allLines = File.ReadAllLines(@"Y:\STOCK\SensibleExcessUpload.csv");



        var query = from line in allLines
                    where line.Contains(txtSearch.Text)
                    let data = line.Split(',')

                    select new

                    {

                        MPN = data[0].ToString(),

                        QTY = data[2].ToString()

                    };
        if (query.Any())
        {
            GridView1.DataSource = query;
            GridView1.DataBind();
            lblResults.Text = query.Count() + " total results.";
        }
        else
            lblResults.Text = "No results.";

    }


}