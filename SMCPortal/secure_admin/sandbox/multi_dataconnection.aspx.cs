﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_multi_dataconnection : System.Web.UI.Page
{
    RzDataContext rdc;
    string ProdConnectionStringName = "Rz3ConnectionString";
    string AzureConnectionStringName = "AzureConnection";
    //string CurrentConnectionStringName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        rdc = new RzDataContext();
        if (!Page.IsPostBack)
            ShowDataConnection(ProdConnectionStringName);


    }

    private void LoadLineInfo()
    {
        orddet_line l = rdc.orddet_lines.OrderByDescending(o => o.date_created).FirstOrDefault();
        lblMostRecentLineInfo.Text = l.fullpartnumber + " | Date: " + l.date_created.Value.ToShortDateString();
    }

    private void ShowDataConnection(string connStringName)
    {
        string connString = System.Configuration.ConfigurationManager.ConnectionStrings[connStringName].ConnectionString;
        rdc = new RzDataContext(connString);
        lblCurrentDataconnection.Text = rdc.Connection.ConnectionString;
        LoadLineInfo();

    }



    protected void lbProdConnection_Click(object sender, EventArgs e)
    {
        //CurrentConnectionStringName = ProdConnectionStringName;
        ShowDataConnection(ProdConnectionStringName);
    }



    protected void lbAzureConnection_Click(object sender, EventArgs e)
    {
        ShowDataConnection(AzureConnectionStringName);
    }
}