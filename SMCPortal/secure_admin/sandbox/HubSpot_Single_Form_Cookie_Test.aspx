﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="HubSpot_Single_Form_Cookie_Test.aspx.cs" Inherits="secure_admin_sandbox_HubSpot_Single_Form_Cookie_Test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <label>Email:</label><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-default btn-sm" />
    <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
        <div class="alert alert-success">
            <strong>Success!</strong> Ya done did it!
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <div class="alert alert-danger">
  <strong>Danger!</strong>
            <asp:Label ID="lblError" runat="server" Text="Label"></asp:Label>
</div>
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

