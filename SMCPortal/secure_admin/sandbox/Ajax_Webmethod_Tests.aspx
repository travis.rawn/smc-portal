﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="Ajax_Webmethod_Tests.aspx.cs" Inherits="secure_admin_sandbox_Ajax_Webmethod_Tests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">




    <script type="text/javascript">
        function GetEnvironmentalData() {
            $.ajax({
                type: "POST",
                url: "Ajax_Webmethod_Tests.aspx/GetEnvironmentalData",
                data: '{ComId: "' + $("#<%=txtComId.ClientID%>")[0].value + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(data.d);
                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
        }
    </script>

    <script type="text/javascript">
        function test() {
            $.ajax({
                type: "POST",
                url: 'Ajax_Webmethod_Tests.aspx/TestMethod',
                data: '{message: "HAI" }',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    alert(data.d);
                    console.log(data);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <div id="pageTitle">
        <h1>Ajax + WebMethods</h1>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:TextBox ID="txtComId" runat="server" CssClass="form-control" placeholder="Enter ComId"></asp:TextBox>
    <button type="button" class="btm btn-sm btn-success" onclick="GetEnvironmentalData()">Env</button>
    <button type="button" class="btm btn-sm btn-success" onclick="test()">Test</button>

    <asp:DetailsView ID="dtvEnvData" runat="server" CssClass="table table-hover table-striped CenterDataView" GridLines="None" AutoGenerateRows="false" EmptyDataText="No Environmental data found.  Not all manufacturers make this data available.  To inquire further, please contact your sales representative.">
        <EmptyDataTemplate>
            <div class="alert alert-warning">
                <strong>No Environmental Data found for search term.</strong>
            </div>
        </EmptyDataTemplate>
        <Fields>
            <asp:BoundField DataField="ConflictMineralStatus" HeaderText="Conflict Mineral Status:" SortExpression="ConflictMineralStatus" />
            <asp:TemplateField SortExpression="ConflictMineralStatement" HeaderText="Conflict Mineral Statement:">
                <ItemTemplate>
                    <asp:HyperLink ID="hlConflictMineralStatement" runat="server" CssClass="btn btn-sm btn-success" NavigateUrl='<%# Eval("ConflictMineralStatement") %>' Text="Conflict Mineral Statement" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle Wrap="True" />
            </asp:TemplateField>
            <asp:BoundField DataField="RoHSStatus" HeaderText="RoHS Status:" SortExpression="RoHSStatus" />
            <asp:BoundField DataField="PartNumber" HeaderText="PartNumber:" SortExpression="PartNumber" />
            <asp:BoundField DataField="ComID" HeaderText="ComID:" SortExpression="ComID" />
            <asp:BoundField DataField="Source" HeaderText="Source:" SortExpression="Source" />
            <asp:BoundField DataField="SourceType" HeaderText="Source Type:" SortExpression="SourceType" />
            <asp:BoundField DataField="Exemption" HeaderText="Exemption:" SortExpression="Exemption" />
            <asp:BoundField DataField="ExemptionType" HeaderText="Exemption Type:" SortExpression="ExemptionType" />
            <asp:BoundField DataField="RohsIdentifier" HeaderText="Rohs Identifier:" SortExpression="RohsIdentifier" />
            <asp:BoundField DataField="LeadFree" HeaderText="Lead Free:" SortExpression="LeadFree" />
            <asp:BoundField DataField="RareEarthElementInformation" HeaderText="Rare Earth ElementInformation:" SortExpression="RareEarthElementInformation" />
            <asp:BoundField DataField="EICCMembership" HeaderText="EICC Membership:" SortExpression="EICCMembership" />
            <asp:BoundField DataField="ResultDto_Id" HeaderText="ResultDto_Id" SortExpression="ResultDto_Id" />
        </Fields>
    </asp:DetailsView>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

