﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SalesScreen.master" AutoEventWireup="true" CodeFile="flexbox-tests.aspx.cs" Inherits="secure_admin_sandbox_flexbos_tests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <style>
        body, html {
            margin: 0;
            font-family: 'Roboto', sans-serif;
            background-color: rgb(27, 28, 31);
        }


        .wrapper {
            width: 1910px;
            height: 1070px;
            margin: 3px;
            color: whitesmoke;
        }

        .parent {
            background-color: rgb(27, 28, 31);
            margin: 5px;
            display: flex;
            height: 480px; /* Or whatever */
        }

        .banner {
            /*margin: auto;*/ /* Magic! */
            background-color: rgb(42, 42, 42);
            width: 100%;
            height: 80px;
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            color: whitesmoke;
            font-size: 68px;
        }



        .tile {
            background-color: rgb(42, 42, 42);
            width: 100%;
            height: 100%;
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            margin: 2px;
        }

        .tile-name {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            font-size: 56px;
            float: left;
        }

        .tile-image {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            float: left;
        }

        .tile-data {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            margin-top:15px;
            display: inline-block;
            font-size: 42px;
            width: 100%;
            float: left;
        }

        .tile-scroller {
            /*border: 1px;
            border-style: solid;
            border-color: whitesmoke;*/
            height: 470px;
            width: 100%;
            color: greenyellow;
            font-size: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <div class="wrapper">
        <div class="banner">Banner</div>

        <div class="parent">
            <div class="tile">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="tile-name">
                            Adam
                        </div>
                        <div class="tile-image">
                            <img src="http://media.giphy.com/media/Dr4qwqylyBnUY/giphy.gif" class="img img-center img-responsive" />
                        </div>
                        <div class="tile-data">
                            Quoted: $12000.01<br />
                            Booked: $1000.00<br />
                            Invoiced: $500.99<br />
                            MTD: $1249.76           
                        </div>

                    </div>
                    <div class="col-sm-3">
                        <div class="tile-scroller">                           
                            <br />
                            $1234.56
                            <br />
                            $1200.00
                            <br />
                            $100.00
                            <br />
                            $5.00
                            <br />
                            $900.45
                            <br />
                            $100.00
                            <br />
                            $5.00
                            <br />
                            $900.45
                        </div>
                    </div>
                </div>
            </div>
            <div class="tile">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="tile-name">
                            Adam
                        </div>
                        <div class="tile-image">
                            <img src="http://media.giphy.com/media/Dr4qwqylyBnUY/giphy.gif" class="img img-center img-responsive" />
                        </div>
                        <div class="tile-data">
                            Quoted: $12000.01<br />
                            Booked: $1000.00<br />
                            Invoiced: $500.99<br />
                            MTD: $1249.76           
                        </div>

                    </div>
                    <div class="col-sm-3">
                        <div class="tile-scroller">                          
                            <br />
                            $1234.56
                            <br />
                            $1200.00
                            <br />
                            $100.00
                            <br />
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="tile">child 3</div>
        </div>
        <div class="parent">
            <div class="tile">child 4</div>
            <div class="tile">child 5</div>
            <div class="tile">child 6</div>
        </div>
    </div>

</asp:Content>

