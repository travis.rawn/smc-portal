﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="multi_dataconnection.aspx.cs" Inherits="secure_admin_sandbox_multi_dataconnection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
    <h3>Multi-Datacontext Tests
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-12">
            <p>The goal is to programatically switch between two or more datacontexts, such as test system and the production system.</p>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h4><asp:Label ID="lblCurrentDataconnection" runat="server"></asp:Label></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <asp:LinkButton ID="lbProdConnection" runat="server" OnClick="lbProdConnection_Click">Select Production</asp:LinkButton>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbAzureConnection" runat="server" OnClick="lbAzureConnection_Click">Select Azure</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm=12">
            <div class="well-well-sm">
                Moste Recent Line:  <asp:Label ID="lblMostRecentLineInfo" runat="server"></asp:Label><br />

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

