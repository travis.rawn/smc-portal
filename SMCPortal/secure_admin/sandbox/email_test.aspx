﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="email_test.aspx.cs" Inherits="secure_admin_sandbox_email_test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="col-sm-12">
            <asp:Button ID="btnTestGeneral" runat="server" OnClick="btnTestGeneral_Click" Text="Test General Email" />
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <label>From Address:</label>
            <asp:TextBox ID="txtFromAddress" runat="server"></asp:TextBox>
            <label>From Name:</label>
            <asp:TextBox ID="txtFromName" runat="server"></asp:TextBox>
            <label>To Address:</label>
            <asp:TextBox ID="txtToAddress" runat="server"></asp:TextBox>
            <label>Subject:</label>
            <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
            <label>Body:</label>
            <asp:TextBox ID="txtBody" runat="server" TextMode="MultiLine"></asp:TextBox>
            <asp:Button ID="btnSendEmail" runat="server" OnClick="btnSendEmail_Click" Text="Send Email" />
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <asp:Button ID="btnTestPortalAlerts" runat="server" OnClick="btnTestPortalAlerts_Click" Text="Test Portal Alert Group" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

