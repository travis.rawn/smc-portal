﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="silicon_expet_top_customers_taxonomy.aspx.cs" Inherits="secure_admin_sandbox_silicon_expet_top_customers_taxonomy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="col-sm-3">
            <uc1:datetime_flatpicker runat="server" ID="dtpStart" />
        </div>
        <div class="col-sm-3">
           <uc1:datetime_flatpicker runat="server" ID="dtpEnd" />
        </div>
        <div class="col-sm-1">
            <asp:LinkButton ID="lbSerach" runat="server" OnClick="lbSearch_Click">Search</asp:LinkButton>
        </div>
        <div class="col-sm-1">
            <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">Export</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

