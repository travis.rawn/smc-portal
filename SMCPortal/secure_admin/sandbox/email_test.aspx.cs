﻿using SensibleDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_email_test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {

        string fromAddress = txtFromAddress.Text;
        string toAddress = txtToAddress.Text;
        string fromName = txtFromName.Text;

        string subject = txtSubject.Text;
        if (string.IsNullOrEmpty(subject))
            subject = "Hello from Silverback Communications";
        string body = txtBody.Text;
        if (string.IsNullOrEmpty(body))
            body = "Hi Sean, <br /><br />It was a pleasure speaking with you today.  As promised, here's a little video about what we do.  Please pass along to your IT Director. <br /><br />All the best,<br /><br /><a href=\"https://tenor.com/xwLD.gif\" target=\"_blank\">See what Silverback can do for Sensible Micro</a><br /><br />Jake Trimmins<br /><a href=\"mailto:smar@sensiblemicro.com\">jake@sbnetworkit.com</a><br />http://sbnetworkit.com/<br />813-236.6549<br /><img style=\"width:175px;\" src=\"http://sbnetworkit.com/wp-content/uploads/2018/05/silver-back-communications.png\">";

        SM_Tools tools = new SM_Tools();
        tools.SendMail(fromAddress, toAddress, subject, body);

    }

    protected void btnTestPortalAlerts_Click(object sender, EventArgs e)
    {
        string to = SystemLogic.Email.EmailGroup.PortalAlerts;
        string from = SystemLogic.Email.EmailGroupAddress.PortalAlert;
        SM_Tools tools = new SM_Tools();
        tools.SendMail(from, to, "Portal Alerts Email Test", "This a test of the Portal Alert Email System.  This is only a test.");
    }

    protected void btnTestGeneral_Click(object sender, EventArgs e)
    {
        string to = "systems@sensiblemicro.com";
        string from = "test@sensiblemicro.com";
        string subject = "Test Subject";
        string body = "<b>Html Body</b>";
        SystemLogic.Email.SendMail(from, to, subject, body);
    }
}