﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class secure_admin_sandbox_group_by_week : System.Web.UI.Page
{
    List<DateTime> datesList = new List<DateTime>();
    DateTime startDate = new DateTime(2022, 01, 01);
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            datesList = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= startDate).Select(s => s.date_created.Value.Date).Distinct().ToList();
        }
        var query = datesList.OrderByDescending(o => o).Select(s => new { Date = s.ToShortDateString(), WeekNumber = Tools.Dates.GetWeekNumberFromDate(s) });
        smdt.dataSource = query;
        smdt.pageLength = 100;
        smdt.loadGrid();


        lblDatesGrid.Text = "Unique order line dates between " + startDate.ToShortDateString() + " and " + DateTime.Today.Date.ToShortDateString();
    }

    
}