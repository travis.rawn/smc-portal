﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;

public partial class secure_admin_sandbox_email_domain_matching : System.Web.UI.Page
{

    SM_Tools tools = new SM_Tools();


    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
    }



    private void MatchVisitorToRzByDomain()
    {
        string customerEmail = txtCustomerEmail.Text.Trim().ToLower();
        try
        {
            SensibleDAL.RzLogic.RzDomainMatchResults dmr;
            bool createIfMissing = cbxCreateContactIfMissing.Checked;

            string firstName = txtFirstName.Text.Trim();
            string lastName = txtLastName.Text.Trim();
            string phone = txtPhone.Text.Trim();
            string[] firstLastPhone = { firstName ?? "No First Name Given", lastName ?? "No Last Name Given", phone ?? "No Phone Given" };


            using (RzDataContext rdc = new RzDataContext())
                SensibleDAL.RzLogic.MatchVisitorToRzByDomain(rdc, customerEmail, createIfMissing, firstLastPhone, out dmr);

            //Set the Label Fields
            SetResultLabels(dmr.rzContact, dmr.rzCompany, dmr);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }



    private void SetResultLabels(companycontact rzContact, company rzCompany, SensibleDAL.RzLogic.RzDomainMatchResults dmr)
    {
        //The Rzcontact object may not be null, but it's properties could be, check them separately.
        if (rzContact != null)
        {
            lblContactName.Text = rzContact.contactname ?? "No Contact Name found.";
            lblContactEmail.Text = rzContact.primaryemailaddress ?? "No Contact Email found.";

        }
        else
        {
            lblContactName.Text = "No Contact match.";
            lblContactEmail.Text = "No Contact match.";
        }

        if (rzCompany != null)
        {
            lblCompanyName.Text = rzCompany.companyname ?? "No Company Name found.";

        }
        else
        {
            lblCompanyName.Text = "No company match.";
        }


        if (dmr.rzCompanyAgent != null)
        {
            lblAgentName.Text = dmr.rzCompanyAgent.name + " (" + dmr.rzCompanyAgent.email_address.Trim().ToLower() + ")";
        }
        else
        {
            lblAgentName.Text = "No agent match";
        }
        //Dump any error text to the label.
        if (!string.IsNullOrEmpty(dmr.matchResultLog))
        {
            lblError.Text = dmr.matchResultLog;
        }

    }



    protected void btnMatchEmail_Click(object sender, EventArgs e)
    {
        MatchVisitorToRzByDomain();
    }
}