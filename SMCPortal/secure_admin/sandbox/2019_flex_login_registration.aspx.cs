﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_2019_flex_login_registration : System.Web.UI.Page
{
    //https://www.aspsnippets.com/Articles/Authenticate-users-without-Login-Control-in-ASP.Net-Membership.aspx

    //MembershipUser CurrentUser = null;
    SM_Tools tools = new SM_Tools();
    string registerFirstName;
    string registerLastName;
    string registerEmail;



    protected void Page_Load(object sender, EventArgs e)
    {
        //check valid email

        //if not valid email return        
        lbLogout.Visible = SM_Global.CurrentUser != null;
        lbLogin.Visible = !lbLogout.Visible;
    }

    protected void lbLogin_Click(object sender, EventArgs e)
    {
        lblMessage.Style.Add("display", "none");
        Login();

    }


    protected void Login()
    {
        try
        {
            string userName = txtUserName.Text.Trim().ToLower();
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                ShowMessage("Invalid user name.");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                ShowMessage("Invalid password.");
                return;
            }


            if (!Membership.ValidateUser(userName, txtPassword.Text))
            {
                ShowMessage("Username or password incorrect.");
                return;
            }

            //Good UserName and Password
            SM_Global.CurrentUser = Membership.GetUser(userName);

            //Check Null
            if (SM_Global.CurrentUser == null)
            {
                ShowMessage("Current User Object is invalid.");
                //ValidationStatus = SM_Security.ValidationStatus.InvalidUser;
                return;
            }
            ////Set the Session User
            //SM_Security.UpdateCurrentUser();


            //Check User Valid Login criteria
            SM_Global.CurrentUser = SM_Global.CurrentUser;
            SM_Security.HandleUserSecurityValidation();
            //Set the Login Message
            SetLoginMessage();




            //Set Cookie if desired
            HandlePersistenceCookie();

            //Successful login - redirect to home.
            //Response.Redirect("~/secure_members/Default.aspx", false);
            SetLoginMessage();
        }
        catch (Exception ex)
        {

        }


    }

    private void ShowMessage(string v)
    {
        lblMessage.Style.Add("display", "block");
        lblMessage.Text = v;
    }

    private void SetLoginMessage()
    {
        string loginMessage = GetLoginMessage();
        ShowMessage(loginMessage);
    }

    private string GetLoginMessage()
    {
        string ret = "Login Process Successful.";

        if (SM_Security.HasTempPassword(SM_Global.CurrentUser))
            return "Please login using the temporary password we emailed to you.  If you do not currently have access to this, please perform a password reset.";
        if (SM_Global.CurrentUser.IsLockedOut)
        {
            return "We're sorry, your account is locked.  Please contact your sales representative to resovle.  Thank you.";

        }
        if (!SM_Global.CurrentUser.IsApproved)
        {
            return "We're sorry, your account is not approved for access.  Please contact your sales representative to resovle.  Thank you.";

        }
        if (SM_Security.IsAgendUserLogin(SM_Global.CurrentUser))
        {
            return "Since it's been more than 30 days since your last successful login, for your security, we require that you reset your password to regain access.";

        }

        return ret;
    }

    private void HandlePersistenceCookie()
    {
        bool rememberMe = cbxRememberMe.Checked;
        if (rememberMe)
        {
            FormsAuthentication.SetAuthCookie(txtUserName.Text, true);
        }
        else
        {
            FormsAuthentication.SetAuthCookie(txtUserName.Text, false);
        }

    }

    protected void Logout()
    {
        FormsAuthentication.SignOut();
        Session.Abandon();
    }

    protected void lbLogout_Click(object sender, EventArgs e)
    {
        Logout();
    }


    protected void lbRegister_Click(object sender, EventArgs e)
    {
        int passwordLength = 12;
        int nonAlphaNumericCharacterCount = 2;
        string password = Membership.GeneratePassword(passwordLength, nonAlphaNumericCharacterCount);
        registerFirstName = txtFirstName.Text.Trim(); ;
        registerLastName = txtLastName.Text.Trim();
        registerEmail = txtEmail.Text.Trim();


        //Check valid Email Address
        if (!Tools.Email.IsEmailAddress(registerEmail))
        {
            ShowMessage(registerEmail + " is not a valid email address.");
            return;
        }

        //Check Valid Portal Email Address
        if (!SM_Security.IsValidPortalEmail(registerEmail))
        {
            ShowMessage(registerEmail + " is not a valid customer email address.");
            return;
        }



        MembershipCreateStatus createStatus;
        bool registerSuccess = SM_Security.RegisterNewExternalUser(registerFirstName, registerLastName, registerEmail, out createStatus);
    }

    protected void Register()
    {



        ////Check Captcha
        //if (!tools.ValidateCaptcha(false))
        //{
        //    ShowMessage("Please click the Captcha below.");
        //    ScriptManager.RegisterStartupScript(this, GetType(), "CaptchaReload", "$.getScript(\"https://www.google.com/recaptcha/api.js\", function () {});", true);

        //    return;
        //}

        MembershipCreateStatus createStatus;
        bool created = SM_Security.RegisterNewExternalUser(registerFirstName, registerLastName, registerEmail, out createStatus);
        if (!created)
        {
            ShowMessage(createStatus.ToString());
            return;
        }


        if (createStatus == MembershipCreateStatus.Success)
        {
            MembershipUser newUser = Membership.GetUser(registerEmail);
            ShowCreateStatusMessage(createStatus);
        }

    }

    private void ShowCreateStatusMessage(MembershipCreateStatus createStatus)
    {


        switch (createStatus)
        {
            case MembershipCreateStatus.Success:
                ShowMessage("The user account was successfully created!");
                break;
            case MembershipCreateStatus.DuplicateUserName:
                ShowMessage("There already exists a user with this username.");
                break;
            case MembershipCreateStatus.DuplicateEmail:
                ShowMessage("There already exists a user with this email address.");
                break;
            case MembershipCreateStatus.InvalidUserName:
                ShowMessage("Invalid Username.");
                break;
            case MembershipCreateStatus.InvalidEmail:
                ShowMessage("There email address you provided in invalid.");
                break;
            case MembershipCreateStatus.InvalidAnswer:
                ShowMessage("There security answer was invalid.");
                break;
            case MembershipCreateStatus.InvalidPassword:
                ShowMessage("The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");
                break;
            default:
                ShowMessage("There was an unknown error; the user account was NOT created.");
                break;

        }
    }


    protected void lbReset_Click(object sender, EventArgs e)
    {
        try
        {
            //Check Empty Field
            if (string.IsNullOrEmpty(txtPasswordReset.Text.Trim()))
            {
                ShowMessage("Please enter your username or email address.");
                return;
            }

            //The the username from the field
            string resetPasswordUserName = txtPasswordReset.Text.Trim();

            //Get the membership from resetPasswordName
            MembershipUser u = Membership.GetUser(resetPasswordUserName);
            if (u == null)
            {
                ShowMessage("No user account found for: " + resetPasswordUserName);
                return;
            }

            //Do The Reset
            if (SM_Security.ResetPassword(u))
                ShowMessage("Password Successfully Reset.");
        }
        catch (Exception ex)
        {
            ShowMessage(ex.Message);
        }
    }




    protected void lbChanceTemporaryPassword_Click(object sender, EventArgs e)
    {
        try
        {
            if (ChangeTemporaryPassword())
                ShowMessage("Temporary password changed successfully.");
        }
        catch (Exception ex)
        {
            ShowMessage(ex.Message);
        }
    }

    private bool ChangeTemporaryPassword()
    {
        LoadCurrentUser();
        if (SM_Global.CurrentUser == null)
        {
            ShowMessage("Current user not identified.");
            return false;
        }
        string tempPassword = txtTempPassword.Text.Trim();
        string newPassword = txtNewPassword.Text.Trim();
        string repeatNewPassword = txtRepeatNewPassword.Text.Trim();



        if (!Membership.ValidateUser(SM_Global.CurrentUser.UserName, tempPassword))
        {
            ShowMessage("Temporary password does not match our records.");
            return false;
        }

        if (newPassword != repeatNewPassword)
        {
            ShowMessage("New Password and Repeated Password Do Not Match.  Please Try Again.");
            return false;
        }


        if (!SM_Global.CurrentUser.ChangePassword(tempPassword, newPassword))
        {
            ShowMessage("Password change failed. Please re-enter your values and try again.");
            return false;
        }

        //Success    

        FormsAuthentication.SetAuthCookie(SM_Global.CurrentUser.UserName, false);
        return true;


    }

    private void LoadCurrentUser()
    {
        //CurrentUser = SM_Security.LoadCurrentUser();
        if (SM_Global.CurrentUser == null)
        {
            string userID = Tools.Strings.SanitizeInput(Request.QueryString["id"]);
            if (Request.QueryString.Count > 0 && !string.IsNullOrEmpty(userID))
            {
                Guid userGuid = new Guid(userID);
                SM_Global.CurrentUser = Membership.GetUser(userGuid);
            }

        }

    }

}
