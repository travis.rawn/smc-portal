﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using SensibleDAL;

public partial class secure_admin_sandbox_SiliconExpert_JSON : System.Web.UI.Page
{
    SM_Data smd = new SM_Data();
    SM_Tools tools = new SM_Tools();
    //silicon_expert si = new silicon_expert();
    protected void Page_Load(object sender, EventArgs e)
    {
        //lblJSONString.Text = SiliconExpertLogic.QueryJson();
    }


    //from json2csharp.com - Awesome Reference
    public class Status
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Success { get; set; }
    }

    public class PartDto
    {
        public string ComID { get; set; }
        public string PartNumber { get; set; }
        public string Manufacturer { get; set; }
        public string PlName { get; set; }
        public string Description { get; set; }
        public string Datasheet { get; set; }
        public string Lifecycle { get; set; }
        public string RoHS { get; set; }
    }

    public class PartList
    {
        public PartDto PartDto { get; set; }
    }

    public class PartData
    {
        public string RequestedPart { get; set; }
        public string RequestedManufacturer { get; set; }
        public PartList PartList { get; set; }
    }

    public class Result
    {
        public List<PartData> PartData { get; set; }
    }

    public class RootObject
    {
        public Status Status { get; set; }
        public Result Result { get; set; }
    }



    protected void btnPartDetailSearchXML_Click(object sender, EventArgs e)
    {
        try
        {
            PartDetailSearchXML();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

    }




    protected void doSearchXML()
    {


        string username = "sensiblemicrotrial";
        string password = "8175918411";
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://app.siliconexpert.com/SearchService/search/authenticateUser?login=" + username + "&apiKey=" + password);
        request.ContentType = "application/json; charset=utf-8";
        CookieContainer cookieContainer = request.CookieContainer = new CookieContainer();
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        string searchUrl = "https://app.siliconexpert.com/SearchService/search/listPartSearch?partNumber=[{\"partNumber\":\"bav99wt\",\"manufacturer\":\"NXP Semiconductors\"},{\"partNumber\":\"bav99\",\"manufacturer\":\"Vishay\"}]";
        request = (HttpWebRequest)WebRequest.Create(searchUrl);
        request.CookieContainer = cookieContainer;
        response = (HttpWebResponse)request.GetResponse();
        string results = new System.IO.StreamReader(response.GetResponseStream(), Encoding.ASCII).ReadToEnd();

        XmlSerializer serializer = new XmlSerializer(typeof(Result), new XmlRootAttribute("ServiceResult"));
        XmlReader reader = XmlReader.Create(searchUrl);

        // Declare an object variable of the type to be deserialized.
        //PartDto part = new PartDto();


        // Use the Deserialize method to restore the object's state.
        StringReader rdr = new StringReader(results);
        Result result = (Result)serializer.Deserialize(rdr);

        DataSet ds = new DataSet();
        ds.ReadXml(reader);
        GridView1.DataSource = ds.Tables[1].DefaultView;
        GridView1.DataBind();


        //foreach(var p in result.partDataList)
        //{
        //    PartDto pd = p.partlist.partdto;
        //}








        //siAuthStatus sia = JsonConvert.DeserializeObject<siAuthStatus>(results);

        //string code = sia.Code;
        //string message = sia.Message;
        //bool success = sia.Success;
        //lblJSONString.Text = code + "  " + message + "  " + success;



        //lblJSONString.Text = results;



        //Console.WriteLine(results);
        // Console.ReadLine();

    }


    protected void btnPartListSearchJSON_Click(object sender, EventArgs e)
    {
        try
        {
            PartListSearchJSON();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

    }

    //protected void PartListSearchXML()
    //{
    //    // Use the Deserialize method to restore the object's state.

    //    string part = txtPartnumber.Text;
    //    string username = "sensiblemicrotrial";
    //    string password = "8175918411";
    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://app.siliconexpert.com/SearchService/search/authenticateUser?login=" + username + "&apiKey=" + password);
    //    request.ContentType = "application/json; charset=utf-8";
    //    CookieContainer cookieContainer = request.CookieContainer = new CookieContainer();
    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    //string searchUrl = "https://app.siliconexpert.com/SearchService/search/listPartSearch?partNumber=[{\"partNumber\":\"bav99wt\",\"manufacturer\":\"NXP Semiconductors\"},{\"partNumber\":\"bav99\",\"manufacturer\":\"Vishay\"}]";
    //    string searchUrl = "https://app.siliconexpert.com/SearchService/search/listPartSearch?partNumber=[{\"partNumber\":\"" + part + "\"}]&mode=beginwith";
    //    request = (HttpWebRequest)WebRequest.Create(searchUrl);
    //    request.CookieContainer = cookieContainer;
    //    response = (HttpWebResponse)request.GetResponse();
    //    string results = new System.IO.StreamReader(response.GetResponseStream(), Encoding.ASCII).ReadToEnd();
    //    //XmlSerializer serializer = new XmlSerializer(typeof(Result), new XmlRootAttribute("ServiceResult"));
    //    StringReader rdr = new StringReader(results);
    //    //Result result = (Result)serializer.Deserialize(rdr);
    //    XmlReader reader = XmlReader.Create(rdr);

    //    DataSet ds = new DataSet();
    //    ds.ReadXml(reader);
    //    GridView1.DataSource = ds.Tables[4].DefaultView;
    //    GridView1.DataBind();
    //}

    protected void PartListSearchJSON()
    {
        string username = "sensiblemicrotrial";
        string password = "8175918411";
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://app.siliconexpert.com/SearchService/search/authenticateUser?login=" + username + "&apiKey=" + password);
        request.Accept = "application/json";
        CookieContainer cookieContainer = request.CookieContainer = new CookieContainer();
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        string searchUrl = "https://app.siliconexpert.com/SearchService/search/listPartSearch?partNumber=[{\"partNumber\":\"bav99wt\",\"manufacturer\":\"NXP Semiconductors\"},{\"partNumber\":\"bav99\",\"manufacturer\":\"Vishay\"}]";
        request = (HttpWebRequest)WebRequest.Create(searchUrl);
        request.Accept = "application/json";
        request.CookieContainer = cookieContainer;
        response = (HttpWebResponse)request.GetResponse();
        String results = new System.IO.StreamReader(response.GetResponseStream(), ASCIIEncoding.ASCII).ReadToEnd();

        lblJSONString.Text = results;
        ////Result result = (Result)serializer.Deserialize(rdr);

        //Newtonsoft.Json
        //List<PartDto> pdtList  = new List<PartDto>();
        //var list = JsonConvert.DeserializeObject<List<PartDto>>(results);
        RootObject datalist = JsonConvert.DeserializeObject<RootObject>(results);

        var obj = JsonConvert.DeserializeObject<RootObject>(results);
        List<PartDto> pList = new List<PartDto>();

        foreach (var v in obj.Result.PartData)
        {
            PartDto p = (PartDto)v.PartList.PartDto;
            pList.Add(p);
        }

        GridView2.DataSource = pList;
        GridView2.DataBind();
    }

    public DataTable DerializeDataTable(string data)
    {
        string json = data; //"data" should contain your JSON 
        var table = JsonConvert.DeserializeObject<DataTable>(json);
        return table;
    }

    protected void PartDetailSearchXML()
    {
        //KT Apprently not currently authorized for this.
        string part = txtPartnumber.Text;
        if (string.IsNullOrEmpty(part))
            throw new Exception("Please provide a part number to search");
        string username = "sensiblemicrotrial";
        string password = "8175918411";
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://app.siliconexpert.com/ProductAPI/search/authenticateUser?login=" + username + "&apiKey=" + password);
        CookieContainer cookieContainer = request.CookieContainer = new CookieContainer();
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        string searchUrl = "https://app.siliconexpert.com/ProductAPI/search/partDetail?comIds=52659330";
        request = (HttpWebRequest)WebRequest.Create(searchUrl);
        request.CookieContainer = cookieContainer;
        response = (HttpWebResponse)request.GetResponse();
        string results = new System.IO.StreamReader(response.GetResponseStream(), ASCIIEncoding.ASCII).ReadToEnd();

        //XmlSerializer serializer = new XmlSerializer(typeof(Result), new XmlRootAttribute("ServiceResult"));
        StringReader rdr = new StringReader(results);
        //Result result = (Result)serializer.Deserialize(rdr);
        XmlReader reader = XmlReader.Create(rdr);
        DataSet ds = new DataSet();
        ds.ReadXml(reader);
        GridView1.DataSource = ds.Tables[0].DefaultView;
        GridView1.DataBind();

    }



    protected void btnGetComID_Click(object sender, EventArgs e)
    {
        //lblComID.Text = "ComID for AT91SAM7X256C-AUR = " +si.GetComIDXML("AT91SAM7X256C-AUR");
    }

    protected void btnPartListSearchMulti_Click(object sender, EventArgs e)
    {
        doPartSearch();


    }

    protected void btnPartListSearchXML_Click(object sender, EventArgs e)
    {

        doPartSearch();

    }

    private void doPartSearch()
    {
        try
        {
            if (string.IsNullOrEmpty(txtPartnumber.Text))
                throw new Exception("Please provide 1 or more part numbers.");

            GridView2.DataSource = null;
            GridView2.DataBind();
            //List<string> parts = getPartNumbersFromString(txtPartnumber.Text);
            DataSet ds =SiliconExpertLogic.GetListPartSearchDataSetXML(txtPartnumber.Text, true);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables["PartDto"] != null)
                    {
                        GridView2.DataSource = ds.Tables["PartDto"];
                        GridView2.DataBind();
                    }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private List<string> getPartNumbersFromString(string s)
    {
        List<string> ret = new List<string>();
        string[] values = s.Split(',');
        foreach (string v in values)
        {
            ret.Add(v.Trim().ToUpper());
        }
        return ret;
    }
}