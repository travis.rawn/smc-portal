﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="admin_sandbox.aspx.cs" Inherits="secure_admin_tests_sandbox_AutoGenerateMenuLinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sandbox</h1>
        </div>
    </div>
    <div class="card">
        <asp:Label ID="lblLinks" runat="server"></asp:Label>
    </div>

</asp:Content>

