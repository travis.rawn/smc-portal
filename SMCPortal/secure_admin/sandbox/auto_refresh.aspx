﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="auto_refresh.aspx.cs" Inherits="secure_admin_sandbox_auto_refresh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">

   
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align:center;"
            <!-- your controls in panel -->
            <h1>
                <asp:Label ID="lblTest" Text ="Start Timer" runat="server"></asp:Label>
            </h1>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Timer ID="Timer1" runat="server" Interval="2000" OnTick="Timer1_Tick"></asp:Timer>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

