﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Linq;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_admin_sandbox_ajax_autocomplete_tests : System.Web.UI.Page
{

    static RzTools rzt = new RzTools();
    static RzDataContext rdc = new RzDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
       

    }

    [WebMethod]
    public static List<string> LoadCountry(string input)
    {
        return GetCountries().FindAll(item => item.ToLower().Contains(input.ToLower()));
    }
    [WebMethod]
    //public static List<string> LoadCustomers(string input)
    public static List<string> LoadCustomers(string input)
    {
        //return GetCustomersWithSales(new DateTime(2015, 08, 01), DateTime.Now).FindAll(item => item.ToLower().Contains(input.ToLower()));
        //return rzt.GetCustomersWithSales(new DateTime(2015, 08, 01), DateTime.Now).Where(w => w.Value.Contains("input")).Distinct().ToDictionary(d => d.Key, d => d.Value);
        return rdc.orddet_lines.Where(w => w.orderdate_sales >= new DateTime(2015, 08, 01) && w.customer_name.Length > 0 && w.customer_name.Contains(input)).Select(s => s.customer_name).ToList();
    }

    public static List<string> GetCountries()
    {
        List<string> CountryInformation = new List<string>();
        CountryInformation.Add("India");
        CountryInformation.Add("United States");
        CountryInformation.Add("United Kingdom");
        CountryInformation.Add("Canada");
        CountryInformation.Add("South Korea");
        CountryInformation.Add("France");
        CountryInformation.Add("Mexico");
        CountryInformation.Add("Russia");
        CountryInformation.Add("Australia");
        CountryInformation.Add("Turkey");
        CountryInformation.Add("Kenya");
        CountryInformation.Add("New Zealand");
        return CountryInformation;
    }

}
