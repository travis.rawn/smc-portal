﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_sandbox_auto_refresh : System.Web.UI.Page
{
    public int tick
    {
        get
        {
            return Convert.ToInt32(ViewState["tick"]);
        }
        set
        {
            ViewState["tick"] = value;
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        lblTest.Text = tick.ToString();
        tick++;
    }
}