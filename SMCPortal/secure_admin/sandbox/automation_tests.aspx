﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="automation_tests.aspx.cs" Inherits="secure_admin_sandbox_automation_tests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="card">
        <h4>Void Old Quotes Rz/Hubspot</h4>
        <div class="row">
            <div class="col-sm-4">
                 <label>Quote Number:</label><br />
                <asp:TextBox ID="txtQuoteNumber" runat="server" CssClass="form-control" Text=""></asp:TextBox>
            </div>
            <div class="col-sm-4">
                <label>Days Old:</label><br />
                 <asp:TextBox ID="txtOlderThanDays" runat="server" CssClass="form-control" Text="40"></asp:TextBox>
            </div>
            <div class="col-sm-4">
                <asp:LinkButton ID="lbVoidAgedQuotes" runat="server" OnClick="lbVoidAgedQuotes_Click">Void Aged Quotes</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

