﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="rep_management.aspx.cs" Inherits="secure_admin_rep_management" %>




<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2 class="page-header">Rep Management.</h2>    

    <div class="panel panel-default">
        <div class="panel-heading row">
            <div class="col-sm-2">
                <h4>Current Reps</h4>
            </div>
            <div class="col-sm-10">
                <asp:Button ID="btnAddRep" runat="server" OnClick="btnAddRep_Click" CssClass="btn btn-smc btn-smc-success" Text="Add Rep from Rz" />
            </div>
        </div>
        <div class="panel-body">
            <div class="col-sm-4">

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDeleting="GridView1_RowDeleting" EmptyDataText="No Reps have yet been defined." OnSelectedIndexChanged="GridView1_SelectedIndexChanged" CssClass="table table-striped table-bordered table-hover" GridLines="None">
                    <PagerStyle CssClass="pagination-ys" />
                    <Columns>
                        <asp:ImageField DataImageUrlField="leaderboard_image_path">
                            <ControlStyle Height="50px" />
                        </asp:ImageField>
                        <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="FullName" />
                        <asp:CommandField ShowSelectButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-smc btn-smc-primary" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteRep" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this user?')" CssClass="btn btn-smc btn-smc-danger" OnClick="btnDeleteRep_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>

            </div>
            <div class="col-sm-6">
                <asp:Panel ID="pnlCreateRep" runat="server" Visible="false">
                    Choose an Rz user to add:<br />
                    <asp:DropDownList ID="ddlRzRep" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRzRep_SelectedIndexChanged" />
                    <asp:Button ID="btnCreateRep" runat="server" OnClick="btnCreateRep_Click" Text="Create Rep" Visible="false" />
                    <br />
                    <asp:Label ID="lblRzRepAlert" runat="server"></asp:Label><br />
                </asp:Panel>
                <asp:Panel ID="pnlEditRep" runat="server" Visible="false">

                    <div style="float: left">
                        <h5>Headshot</h5>
                        <asp:Label ID="lblHeadshotAlert" runat="server">Upload Headshot</asp:Label><br />
                        <asp:Image ID="imgHeadshot" runat="server" Style="max-width: 180px;" /><br />
                        <br />
                    </div>
                    <asp:Label ID="lblFileUpload" runat="server"></asp:Label><br />
                    <asp:FileUpload ID="fuHeadshot" runat="server" />
                    <asp:Button ID="Button_HeadshotUpload" runat="server" OnClick="Button_HeadshotUpload_Click" Text="Update Headshot" /><br />
                    <asp:Label ID="StatusLabel" runat="server"></asp:Label>                    
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

