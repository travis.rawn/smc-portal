﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_role_management : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DisplayRolesInGrid();
            LoadUsersDDL();
        }
            
    }
    private void DisplayRolesInGrid()
    {
        GridView1.DataSource = Roles.GetAllRoles();
        GridView1.DataBind();
    }
    protected void btnCreateRole_Click(object sender, EventArgs e)
    {
        string newRoleName = txtRoleName.Text.Trim();
        if (newRoleName.Length > 0)
        {
            if (!Roles.RoleExists(newRoleName))
            {
               
                // Create the role
                Roles.CreateRole(newRoleName);
                HandleSuccess("Created role: " + newRoleName);
                // Refresh the RoleList Grid    
                DisplayRolesInGrid();
            }
            else
            {
                HandleError("Cannot create role '" + newRoleName + "'. Role already exists.");
                return;
            }        
            txtRoleName.Text = string.Empty;
        }
        else
        {
            HandleError("I need a role name . . . . any role name.  Gimme SOMETHING!!");
        }
    }

    protected void RoleList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (pnlEditRole.Visible == false)
        {
            pnlEditRole.Visible = true;
        }      
        GridViewRow row = GridView1.SelectedRow;
        Label role = (Label)row.FindControl("lblRoleName");
        if (!String.IsNullOrEmpty(role.Text))
            LoadRolePanel(role.Text);
    }

    protected void LoadRolePanel(string RoleName)
    {
        lblRoleName.Text = RoleName;

        LoadRoleData(RoleName);
        List<string> Users = LoadRoleData(RoleName);
        cblUsersInRole.DataSource = Users;
        cblUsersInRole.DataBind();
        foreach (ListItem i in cblUsersInRole.Items)
       {
           i.Selected = true;
       }
        
        btnDeleteRole.Visible = true;
       
    }
   
    public static List<string> LoadRoleData(string RoleName)
    {
        var userList = RoleName.Select(role => Roles.GetUsersInRole(RoleName))
                            .Aggregate((a, b) => a.Union(b).ToArray())
                            .Distinct()
                            .ToList();
        return userList;
    }

    protected void btnDeleteRole_Click(object sender, EventArgs e)
    {
        

        // Delete the role
        if (lblRoleName.Text == "admin")
        {
            HandleError("Sorry, cannot delete the admin role.");

       }
        else
        {
            try
            {
                Roles.DeleteRole(lblRoleName.Text, false);
                // Rebind the data to the RoleList grid                
                DisplayRolesInGrid();
                pnlEditRole.Visible = false;
                HandleSuccess("Deleted role '" + lblRoleName.Text + "'.");
            } 
            catch(Exception ex)
            {
                HandleError(ex.ToString());
            }
                
        }
    }
    protected void HandleError(string msg)
    {
        lblPageAlert.Visible = true;
        lblPageAlert.Text = msg;
        lblPageAlert.ForeColor = System.Drawing.Color.Red;
        //KT for javascript to survive a postabck in an updatepanel, I'd need unneseccarily complex code.  I'll just use labels.
        //tools.JS_Modal("Error: "+msg, this.Page);
    }

    protected void HandleSuccess(string msg)
    {
        lblPageAlert.Visible = true;
        lblPageAlert.Text = msg;
        lblPageAlert.ForeColor = System.Drawing.Color.Green;
        //KT for javascript to survive a postabck in an updatepanel, I'd need unneseccarily complex code.  I'll just use labels.
        //tools.JS_Modal("Success: " + msg, this.Page);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.NewSelectedIndex];
    }
    protected void btnSaveRoles_Click(object sender, EventArgs e)
    {
        SaveRoleChanges(lblRoleName.Text);

    }

    protected void SaveRoleChanges(string RoleName)
    {
        foreach (ListItem i in cblUsersInRole.Items)
        {
            if (i.Selected == false)
                Roles.RemoveUserFromRole(i.Text, RoleName);
        }
        LoadRolePanel(RoleName);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DisplayRolesInGrid();
        
    }

    protected void ShowAddUserToRolesPanel_Click(object sender, EventArgs e)
    {
        //ShowHideAddUserToRolesPanel();       
        
    }

    protected void LoadUsersDDL()
    {
        ddlAddUserToRoles.DataSource = Membership.GetAllUsers();
        ddlAddUserToRoles.DataBind();
    }

    protected void AddUserToRoles_Click(object sender, EventArgs e)
    {
        AddUserToRoles(ddlAddUserToRoles.SelectedItem.ToString());
            
    }

    //protected void ShowHideAddUserToRolesPanel()
    //{
    //    if (pnlAddUserToRoles.Visible == false)
    //    {
    //        ShowAddUserToRolesPanel.Visible = false;
    //        pnlAddUserToRoles.Visible = true;
    //        LoadUsersDDL();
    //    }
    //    else
    //    {
    //        pnlAddUserToRoles.Visible = false;
    //        ShowAddUserToRolesPanel.Visible = true;
    //    }
    //}
    
    protected void btnCancelAddUserToRoles_Click(object sender, EventArgs e)
    {
        //ShowHideAddUserToRolesPanel();
            
    }
    

    protected void AddUserToRoles(string username)
    {
        //Loop through the grid, and get a list of checked roles
        List<string> SelectedRoles = new List<string>();
        List<string> CurrentRolesForUser = new List<string>();
        foreach (string r in Roles.GetRolesForUser(username))
        {
            CurrentRolesForUser.Add(r);
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox cbx = (CheckBox)row.FindControl("cbxSelectRole");
                if (cbx.Checked == true)
                {
                    Label roleName = (Label)row.FindControl("lblRoleName");
                    
                    SelectedRoles.Add(roleName.Text);
                    if (CurrentRolesForUser.Contains(roleName.Text))//If User is alreay in Role, handle error.
                    {
                        HandleError(ddlAddUserToRoles.SelectedItem.ToString() + " is already in " + roleName.Text + ". Cannot Add.");
                        return;
                    }
                        
                }

            }
        }
        if (SelectedRoles.Count == 0)
        {
            HandleError("You must select at least one role to add " + ddlAddUserToRoles.SelectedItem + " to.");
            return;
        }
        else
        {
            Roles.AddUserToRoles(ddlAddUserToRoles.SelectedItem.ToString(), SelectedRoles.ToArray());
            HandleSuccess(ddlAddUserToRoles.SelectedItem.ToString() + " successfully added to:   " + String.Join(",", SelectedRoles));
        }
       
    }
}