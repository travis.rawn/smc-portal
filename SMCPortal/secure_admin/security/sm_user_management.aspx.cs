﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data;
using System.Collections;
using System.Data.Linq.SqlClient;

public partial class secure_admin_user_management : System.Web.UI.Page
{
    //Public Variables
    //SMCPortalDataContext PDC = new SMCPortalDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        ListOnlineUsers();       
    }

    protected void ListOnlineUsers()
    {
        lblCurrentUsers.Text = "Total users online: " + Membership.GetNumberOfUsersOnline().ToString();
        Dictionary<string, DateTime> OnlineUsers = new Dictionary<string, DateTime>();
        var query = from MembershipUser user in Membership.GetAllUsers()
                    select user;
        foreach (MembershipUser u in query)
        {
            TimeSpan loggedintime = new TimeSpan(0, 0, 30, 0, 0);
            if (u.LastActivityDate >= DateTime.Now.Subtract(loggedintime))
            {
                OnlineUsers.Add(u.UserName, u.LastActivityDate);
            }
        }
        var orderedlist = from o in OnlineUsers
                          orderby o.Value descending
                          select o;
        lbOnlineUsers.DataSource = orderedlist;
        lbOnlineUsers.DataBind();
    }

    protected void SearchUsers()
    {        
        GridView1.DataBind();
        btnClearSearch.Visible = true;
    }

    protected void ClearSearch()
    {
        txtSearch.Text = "";
        GridView1.DataBind();        
        btnClearSearch.Visible = false;
    }

    public void GetMembershipDataLinq(string searchString)
    {
        if (searchString == null)
        {
            var query = (from MembershipUser u in Membership.GetAllUsers()
                         select u).ToList();
            GridView1.DataBind();
        }
        else
        {
            var query = (from MembershipUser u in Membership.GetAllUsers()
                         where u.UserName.Contains(searchString) || u.Email.Contains(searchString)
                         select u).ToList();
            if (query.Any())
                GridView1.DataBind();
            else
                HandleError("No such user found.");
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow row = GridView1.SelectedRow;
        MembershipUser mu = Membership.GetUser(row.Cells[1].Text.ToString());
        Session["UserEditId"] = mu.ProviderUserKey;
        string test = Session["UserEditId"].ToString();
        Response.Redirect("~/secure_admin/security/user_edit.aspx?uid="+mu.ProviderUserKey, false);
    }
    protected void btnNewUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("/secure_admin/security/user_new.aspx", false);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchUsers();
    }

    protected void btnClearSearch_Click(object sender, EventArgs e)
    {
        ClearSearch();
    }


    protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        string searchString = null; bool boolSens = cbxShowSensible.Checked;

        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            searchString = txtSearch.Text.ToLower();
            var query = (from MembershipUser u in Membership.GetAllUsers()
                         where u.Email.Contains("sensiblemicro.com") == boolSens &&(u.Email.Contains(searchString) || u.UserName.Contains(searchString))
                         select u).ToList();
            e.Result = query;
        }
        else
        {

            var query = (from MembershipUser u in Membership.GetAllUsers()
                         where u.Email.Contains("sensiblemicro.com") == boolSens
                         select u).ToList();
            e.Result = query;
        }
            

       

           
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        LinqDataSource1.OrderBy = e.SortExpression;
    }

    protected void HandleError(string msg)
    {
        lblPageAlert.Visible = true;
        lblPageAlert.Text = msg;
        lblPageAlert.ForeColor = System.Drawing.Color.Red;
    }

    protected void HandleSuccess(string msg)
    {
        lblPageAlert.Visible = true;
        lblPageAlert.Text = msg;
        lblPageAlert.ForeColor = System.Drawing.Color.Green;
    }

   
}
