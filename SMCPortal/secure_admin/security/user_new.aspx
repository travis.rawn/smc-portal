﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="user_new.aspx.cs" Inherits="secure_admin_CreateUserWizardWithRoles" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">


        <div style="text-align: center">
        <h1 class="page-header">New User</h1>
    </div>
    <asp:CreateUserWizard ID="RegisterUser" runat="server"
        ContinueDestinationPageUrl="~/secure_admin/security/user_management.aspx"
        LoginCreatedUser="False" OnCreatingUser="RegisterUser_CreatingUser" OnCreatedUser="RegisterUser_CreatedUser" AutoGeneratePassword="True" CompleteSuccessText="Account has been successfully created." EmailRegularExpression="\w+([-+.&amp;apos;]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" EmailRegularExpressionErrorMessage="That appears to be an invalid email." EnableTheming="True">
        <WizardSteps>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                <ContentTemplate>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4  well well-lg form-group">
                                <div class="row">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                    <asp:TextBox ID="UserName" runat="server" CssClass="form-control"></asp:TextBox>                                   
                                </div>
                                <div class="row">
                                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                                    <asp:TextBox ID="Email" runat="server" CssClass="form-control"></asp:TextBox>                                   
                                </div>
                                <div class="row" style="color: Red;">
                                    <%--<asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </ContentTemplate>
                <CustomNavigationTemplate>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Create User"  CssClass="btn-smc btn-smc-success btn-block" />
                    </div>
                    <div class="col-sm-4"></div>


                </CustomNavigationTemplate>
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
            </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
</asp:Content>
