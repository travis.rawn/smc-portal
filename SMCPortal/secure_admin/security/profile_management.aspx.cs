﻿using System;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;

public partial class secure_admin_security_profile_management : System.Web.UI.Page
{

    public DataTable ProfileDataTable
    {
        get
        {
            return (DataTable)(ViewState["ProfileDataTable"] ?? GetProfileDataTable());
        }
        set
        {
            ViewState["ProfileDataTable"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // On initial Page Load, Load full datatable, then filter later.
        if (!Page.IsPostBack)
        {
            //Build DataTable
            ProfileDataTable = GetProfileDataTable();
            //FillDatatable
            FilterDataTable();
            //BindProfileGrid(ProfileDataTable);
        }

    }

    protected DataTable GetProfileDataTable()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("userName", typeof(string));
        dt.Columns.Add("CompanyName", typeof(string));
        dt.Columns.Add("FullName", typeof(string));
        dt.Columns.Add("SensibleRep", typeof(string));
        dt.Columns.Add("UserID", typeof(string));
        foreach (MembershipUser u in Membership.GetAllUsers())
        {
            ProfileCommon p = (ProfileCommon)System.Web.Profile.ProfileBase.Create(u.UserName);
            dt.Rows.Add(u.UserName, p.CompanyName, p.FirstName + " " + p.LastName, p.SensibleRep, u.ProviderUserKey);
        }
        return dt;
    }

    protected void BindProfileGrid(DataTable filteredDt = null)
    {

        gvProfiles.DataKeyNames = new string[] { "UserID" };
        gvProfiles.DataSource = filteredDt;
        gvProfiles.DataBind();
    }

    protected void FilterDataTable(string querystring = null, bool include_internal = false, bool missing_company = false, bool missing_name = false, bool missing_rep = false)
    {


        missing_company = cbxMissingCompany.Checked;


        string searchExpression = "";
        DataTable dtResults = new DataTable();
        //Get full Table

        //filter table
        if (!include_internal)
        {
            searchExpression += "userName LIKE '%@%' and userName NOT LIKE '%@sensiblemicro.com'";
        }

        if (!string.IsNullOrEmpty(querystring))
        {
            if (!string.IsNullOrEmpty(searchExpression))
                searchExpression += " AND ";
            searchExpression += "(FullName LIKE'%" + querystring + "%' OR CompanyName LIKE'%" + querystring + "%' OR userName LIKE'%" + querystring + "%')";

        }
        if (missing_company)
        {
            if (!string.IsNullOrEmpty(searchExpression))
                searchExpression += " AND ";
            searchExpression += "LEN(isnull(CompanyName, '')) = 0";
        }
        if (missing_name)
        {
            if (!string.IsNullOrEmpty(searchExpression))
                searchExpression += " AND ";
            searchExpression += "LEN(isnull(FullName, '')) = 0";
        }
        if (missing_rep)
        {
            if (!string.IsNullOrEmpty(searchExpression))
                searchExpression += " AND ";
            searchExpression += "LEN(isnull(SensibleRep, '')) = 0";
        }

        if (ProfileDataTable.Select(searchExpression).Any())
            dtResults = ProfileDataTable.Select(searchExpression).CopyToDataTable();
        else dtResults = null;
        BindProfileGrid(dtResults);
        if (dtResults != null)
        {
            lblQueryCount.Visible = true;
            lblQueryCount.Text = dtResults.Rows.Count + " result(s) returned";
        }
        else
            lblQueryCount.Visible = false;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        FilterDataTable(txtSearch.Text, cbxIncInternal.Checked, cbxMissingCompany.Checked, cbxMissingName.Checked, cbxMissingRep.Checked);
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        cbxIncInternal.Checked = false;
        cbxMissingCompany.Checked = false;
        cbxMissingName.Checked = false;
        cbxMissingRep.Checked = false;
        FilterDataTable();

    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        ProfileDataTable = GetProfileDataTable();
        FilterDataTable();
    }

    protected void gvProfiles_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hl = (HyperLink)e.Row.FindControl("hlSelect");
            hl.NavigateUrl = "user_edit.aspx?uid=" + gvProfiles.DataKeys[e.Row.RowIndex].Values[0].ToString();
        }
    }
}