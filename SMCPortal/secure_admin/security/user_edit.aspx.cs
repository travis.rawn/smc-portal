﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_admin_user_edit : System.Web.UI.Page
{




    RzDataContext RZDC = new RzDataContext();
    //SMCPortalDataContext PDC = new SMCPortalDataContext();
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();
    RzTools rt = new RzTools();
    Guid userId = Guid.Empty;
    Dictionary<string, string> Companies = new Dictionary<string, string>();
    Dictionary<string, string> Contacts = new Dictionary<string, string>();
    ProfileCommon CurrentProfile;
    List<string> allowedInternalDomains = new List<string>() {"sensiblemicro.com","informulate.net" };


    protected MembershipUser CurrentUser
    {
        get
        {
            if (ViewState["CurrentUser"] == null)
            {


                string strUserId = Tools.Strings.SanitizeInput(Request.QueryString["uid"]);
                if (!string.IsNullOrEmpty(strUserId))
                {
                    userId = Guid.Parse(strUserId);
                    ViewState["CurrentUser"] = System.Web.Security.Membership.GetUser(userId);
                }

            }
            return (MembershipUser)ViewState["CurrentUser"];
        }
        set
        {
            ViewState["CurrentUser"] = value;
        }
    }





    protected void Page_Load(object sender, EventArgs e)
    {
        //ClearAlerts();
        try
        {

            CheckQueryString();
            LoadCurrentUser();
            LoadProfile(CurrentUser);
            bool isRzUser = IsRzUser();

            if (!Page.IsPostBack)
            {
                ddlRzUser.Visible = isRzUser;
                spnRzUserData.Visible = isRzUser;
                spnChooseRzUser.Visible = isRzUser;
                BindDDLRzUser();

                //LoadContactData(profile.CompanyID);
                //BindDDLContact(profile.ContactID);
                LoadRepData();
                LoadCompanyDDls();
                LoadRoles();
                LoadControlValues(false);


            }

        }
        catch (Exception ex)
        {

            tools.HandleResult("fail", ex.Message);
        }
    }

    protected void CheckQueryString()
    {
        bool redirect = false;
        string strUserId = Tools.Strings.SanitizeInput(Request.QueryString["uid"]);
        if (string.IsNullOrEmpty(strUserId))
            redirect = true;

        Guid testGuid;
        if (!Guid.TryParse(strUserId, out testGuid))
            redirect = true;

        if (redirect)
            Response.Redirect("~/secure_admin/security/user_management.aspx", false);
    }



    //Page Events//
    protected void LoadControlValues(bool State)
    {

        lblCurrentUser.Text = CurrentUser.UserName;
        txtFirstName.Text = CurrentProfile.FirstName ?? "Not Set";
        txtLastName.Text = CurrentProfile.LastName ?? "Not Set";
        if (CurrentProfile.FirstName == null && CurrentProfile.LastName == null)
            lblFullName.Text = "Not Set";
        else
            lblFullName.Text = CurrentProfile.FirstName + " " + CurrentProfile.LastName;
        lblCompanyID.Text = CurrentProfile.CompanyID ?? "No Company Set";
        lblCompanyName.Text = CurrentProfile.CompanyName ?? "No Company Set";
        lblContactName.Text = CurrentProfile.ContactName ?? "No Contact Set"; ;
        lblContactID.Text = CurrentProfile.ContactID ?? "No Contact Set"; ;
        lblSensibleRep.Text = CurrentProfile.SensibleRep;
        if (IsRzUser())
        {
            spnRzUserData.Visible = true;
            lblRzUser.Text = CurrentProfile.RzUserName;
        }
        //lblRzUser.Text = profile.RzUserName;
        lblEmail.Text = CurrentUser.Email;

        if (!string.IsNullOrEmpty(CurrentProfile.SensibleRepID))
        {
            ddlSensibleRep.SelectedValue = CurrentProfile.SensibleRepID;

        }

        else
        {
            ddlSensibleRep.SelectedValue = "0";
        }

        if (State == true)
        {
            //btnEdit.Text = "Disable Editing";
            btnSaveProfile.Visible = true;
            pnlProfileEdit.Visible = true;
            pnlProfileData.Visible = false;
        }
        else
        {
            //btnEdit.Text = "Enable Editing";
            btnSaveProfile.Visible = false;
            pnlProfileEdit.Visible = false;
            pnlProfileData.Visible = true;
        }


    }

    private void BindDdlCompany_Customer(string email_address)
    {
        ddlCompany.Items.Clear();
        ddlCompany.Items.Add(new ListItem("--Choose Company--", "0"));
        ddlCompany.AppendDataBoundItems = true;
        ddlCompany.DataSource = rzt.GetCompaniesByEmail(email_address).OrderBy(o => o.companyname);
        ddlCompany.DataTextField = "companyname";
        ddlCompany.DataValueField = "unique_id";
        ddlCompany.DataBind();
        ddlCompany.Visible = true;
    }


    private void BindDdlCompany_RzUser(string userID)
    {
        ddlCompany.Items.Clear();
        ddlCompany.Items.Add(new ListItem("--Choose Company--", "0"));
        ddlCompany.AppendDataBoundItems = true;
        ddlCompany.DataSource = RZDC.companies.Where(w => w.base_mc_user_uid == userID).OrderBy(o => o.companyname).ToList();
        ddlCompany.DataTextField = "companyname";
        ddlCompany.DataValueField = "unique_id";
        ddlCompany.DataBind();

    }

    protected void EnableRoleEdit()
    {
        if (btnEditRoles.Text == "Cancel Edit")
        {
            cblRoles.Enabled = false;
            btnEditRoles.Text = "Edit Roles";
            btnSaveRoles.Visible = false;

        }
        else if (btnEditRoles.Text == "Edit Roles")
        {
            cblRoles.Enabled = true;
            btnEditRoles.Text = "Cancel Edit";
            btnSaveRoles.Visible = true;

        }
        //LoadRoles();
    }

    protected bool ValidateProfile()
    {
        if (Roles.GetRolesForUser(CurrentUser.UserName).Count() > 0) // Only do this check if it's not a non-privelaged user.
            if (CurrentProfile != null)
            {
                //Check CompanyID
                if (!string.IsNullOrEmpty(CurrentProfile.CompanyID))
                {
                }
                //Check ContactID
                if (!string.IsNullOrEmpty(CurrentProfile.ContactID))
                {

                }
                if (string.IsNullOrEmpty(CurrentProfile.FirstName) || string.IsNullOrEmpty(CurrentProfile.LastName))
                {
                    tools.HandleResult("fail", "Please set an appropriate first and last name in the user's profile.");
                    return false;
                }
                //Check Name
                if (CurrentProfile.FirstName.Length < 2 || CurrentProfile.LastName.Length < 2)
                {
                    //tools.HandleResultJS("Please enter a valid first and last name for " + mu.UserName + "'s profile.", true, Page);
                    //return false;
                }
                //Check Sensible Rep
                if (CurrentProfile.SensibleRepID.Length < 3)
                {
                    //tools.HandleResultJS("You must assign a Sensible Rep before inviting this user to the portal.", true, Page);
                    //return false;
                }
            }
        return true;
    }

    protected bool ValidateRoles(bool EmailCheck = false)
    {
        bool valid = false;
        if (CurrentUser != null)
        {
            string domain = GetEmailDomain(CurrentUser.Email);
            string[] currentRoles = Roles.GetRolesForUser(CurrentUser.UserName);
            List<string> selectedRoles = new List<string>();
            List<string> selectedRolesTrimmed = new List<string>();

            //Check Currently Selected items in UI
            int SelectedRoleCount = 0;
            //Carry a count to Ensure A role is set

            //Ensure an Admin is not being set
            foreach (ListItem i in cblRoles.Items)
            {

                if (i.Selected)
                {
                    selectedRoles.Add(i.Value);
                    //Trim the actual roles to identify "SM" roles  
                    selectedRolesTrimmed.Add(i.Value.Substring(0, 2));
                    SelectedRoleCount++;
                }
            }
            //Ensure at least one role is selected
            //if (SelectedRoleCount == 0 && EmailCheck)
            //{
            //    tools.HandleResultJS("You must assign this user to one or more roles before inviting them to the portal.", true, Page);
            //    LoadRoles();
            //    return false;
            //}
            //else
            //{
            if (selectedRoles.Contains("admin") && CurrentUser.Email != "ktill@sensiblemicro.com")
            {
                tools.HandleResultJS("Wha??? Only Kevin is admin here!  No soup for you!", true, Page);
                LoadRoles();
                return false;
            }
            if (selectedRolesTrimmed.Contains("sm") && (!allowedInternalDomains.Contains(domain)))
            {
                tools.HandleResultJS("User not allowed in Sensible role.", true, Page);
                LoadRoles();
                return false;
            }
            //}
            //Check existing role items.
            //Ensure A role is set
            //if (currentRoles.Count() == 0 && EmailCheck)
            //{
            //    tools.HandleResultJS("You must assign this user to one or more roles before inviting them to the portal.", true, Page);
            //    return false;
            //}
            //If, somehow a user is assigned to admin, and it's not me, remove that privelage
            if (currentRoles.Contains("admin"))
            {
                if (CurrentUser.Email != "ktill@sensiblemicro.com")
                {
                    Roles.RemoveUserFromRole(CurrentUser.UserName, "admin");
                    tools.HandleResultJS("Wha??? Only Kevin is admin here!  No soup for you!  Removed from admin role.", true, Page);
                    LoadRoles();
                }
            }



            //Trim the existing roles to identify "SM" roles
            List<string> currentRolesTrimmed = new List<string>();
            foreach (string s in currentRoles)
            {
                currentRolesTrimmed.Add(s.Substring(0, 2));
            }

            //Ensure "SM" roles only belong to @sensiblemicro.com users
            if (currentRolesTrimmed.Contains("sm"))
            {
                if (!allowedInternalDomains.Contains(domain))
                {
                    //Loop through the roles, and remove user form the SM roles while unchecking UI
                    foreach (string r in currentRoles)
                        if (r.Contains("sm_"))
                        {
                            Roles.RemoveUserFromRole(CurrentUser.UserName, r);
                            LoadRoles();
                        }
                    tools.HandleResultJS("The user appears to be a non-sensible employee.  Non-Sensible employees are not allowed in this role.", true, Page);
                    valid = false;
                }
                else
                    valid = true;
            }
            else
                valid = true;
        }
        else
        {
            tools.HandleResultJS("User not loaded in ViewState.", true, Page);
            return false;
        }
        return valid;
    }
    protected void ValidateConsinPartner()
    {
        //Compare company and possible contactid to Rz Consignment partners when adding to consignment role.
    }



    protected string GetEmailDomain(string addy)
    {
        MailAddress addr = new MailAddress(addy);
        return addr.Host.ToString();
    }

    protected void SendWelcomeEmail(string NewPassword, string RepEmail = null)
    {
        try
        {
            if (CurrentUser != null)
            {

                string userName = CurrentUser.UserName;
                string newPassword = NewPassword;

                MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress("no-reply@sensiblemicro.com");
                if (check_dev())
                {
                    ////For Testing purposes                       
                    Msg.Bcc.Add("ktill@sensiblemicro.com");
                }
                //else
                if (RepEmail != null)
                    Msg.To.Add(RepEmail);
                else
                    Msg.To.Add(CurrentUser.Email);

                StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/NewUser.htm"));
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                StrContent = StrContent.Replace("[UserName]", userName);
                StrContent = StrContent.Replace("[Password]", newPassword);
                if (!string.IsNullOrEmpty(CurrentProfile.FirstName))
                    StrContent = StrContent.Replace("[FirstName]", " " + CurrentProfile.FirstName);
                else
                    StrContent = StrContent.Replace("[FirstName]", "");
                Msg.Subject = "Welcome to the Sensible Micro Portal";
                Msg.Body = StrContent.ToString();
                Msg.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Send(Msg);
                //Set temporary password = true
                SM_Security.SetTemporaryPasswordStatus(CurrentUser, true);
                if (RepEmail != null)
                    tools.HandleResultJS("Welcome email successfully sent to " + RepEmail + " for " + userName, true, Page);
                else
                    tools.HandleResultJS("Welcome email successfully sent to " + userName + " " + "Email: " + CurrentUser.Email, true, Page);
                //HandleSuccess("Welcome email successfully sent to " + profile.FirstName + " " + profile.LastName);

            }
            else
            {
                tools.HandleResultJS("Cannot send welcome email, user is null", true, Page);
            }
        }
        catch (Exception ex)
        {
            tools.HandleResultJS("Welcome email was not successfully sent.  Error: " + ex.Message, true, Page);
        }

    }

    protected bool check_dev()
    {
        if (System.Web.Security.Membership.GetUser().ToString() == "kevint")
            return true;
        return false;

    }
    //End Page Events//

    //Control Events//


    protected void btnSaveProfile_Click(object sender, EventArgs e)
    {
        SaveProfileData();
    }



    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/secure_admin/security/user_management.aspx", false);
    }

    protected void btnEditRoles_Click(object sender, EventArgs e)
    {
        EnableRoleEdit();

    }

    protected void btnSaveRoles_Click(object sender, EventArgs e)
    {
        SaveRoles();
    }

    protected void btnSendWelcomeEmail_Click(object sender, EventArgs e)
    {
        //Ensure All profile information is set
        //if any roles begin with sm, ensure email address ends in sensiblemicro.com        
        if (ValidateProfile() && ValidateRoles(true))
        {//Generate a new secure password
            string newPassword;
            try
            {//send email to user with new password.
                newPassword = CurrentUser.ResetPassword();
                SendWelcomeEmail(newPassword);

            }
            catch (Exception ex)
            {
                tools.HandleResultJS(ex.Message, true, Page);
            }
        }
        else
            tools.HandleResultJS("Something went wrong, validation failed", true, Page);

    }

    protected void btnSendWelcomeEmailRep_Click(object sender, EventArgs e)
    {
        try
        {
            //Ensure All profile information is set
            //if any roles begin with sm, ensure email address ends in sensiblemicro.com        
            if (ValidateProfile() && ValidateRoles(true))
            {//Generate a new secure password
                if (CurrentProfile != null)
                {

                    string newPassword;
                    newPassword = CurrentUser.ResetPassword();
                    //Guid RepID = new Guid(profile.SensibleRepID);
                    //send email to user with new password.
                    string email = GetRepEmail(CurrentProfile.SensibleRep);
                    if (email != null)
                        SendWelcomeEmail(newPassword, email);

                }
                else
                {
                    throw new Exception("Profile not found");
                }

            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    protected string GetRepEmail(string Rep)
    {
        string email;
        using (sm_portalEntities pdc = new sm_portalEntities())
            if (pdc.Reps.Any(u => u.FullName == Rep))
            {
                email = pdc.Reps.FirstOrDefault(r => r.FullName == Rep).Email;
                return email;
            }
        return null;
    }
    //End Cotrol Events//

    //CRUD Events

    protected void LoadCurrentUser()
    {

        if (ViewState["CurrentUser"] == null)
            ViewState["CurrentUser"] = System.Web.Security.Membership.GetUser(userId);
        CurrentUser = (MembershipUser)ViewState["CurrentUser"];




        ViewState["UserEmail"] = CurrentUser.Email;
        hfUserEmail.Value = CurrentUser.Email;
        ProfileCommon prof = Profile.GetProfile(CurrentUser.UserName);
        hfUserName.Value = prof.FirstName + " " + prof.LastName;
        ViewState["UserName"] = hfUserName.Value;

        //Check Locked Status
        if (CurrentUser.IsLockedOut)
        {
            btnLockUser.Text = "Unlock User";
            btnConfirmLock.Text = "Confirm Unlock";
            btnDeleteUser.Visible = true;
        }

        else
        {

            btnLockUser.Text = "Lock User";
            btnConfirmLock.Text = "Confirm Lock";
            btnDeleteUser.Visible = false;
        }

    }


    protected void LoadProfile(MembershipUser mu)
    {

        if (CurrentProfile == null)
        {
            //profile = Profile.GetProfile(mu.ProviderUserKey.ToString());
            CurrentProfile = Profile.GetProfile(mu.UserName);
        }
    }

    protected void LoadRepImage(string SensibleRep)
    {
        //SMCPortalDataContext PDC = new SMCPortalDataContext();
        if (ddlSensibleRep.SelectedValue != "0")
        {
            Rep rep = null;
            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                rep = pdc.Reps.Where(x => x.FullName == SensibleRep).FirstOrDefault();
            }
            if (rep != null)
                if (!string.IsNullOrEmpty(rep.HSPath))
                {
                    imgSensibleRep.Visible = true;
                    imgSensibleRep.ImageUrl = rep.HSPath;
                }
                else
                {
                    imgSensibleRep.Visible = false;

                }


        }

    }

    protected void LoadRoles()
    {
        if (ViewState["RolesList"] == null)
        {
            ViewState["RolesList"] = Roles.GetAllRoles().ToList();
        }

        cblRoles.DataSource = ViewState["RolesList"];
        cblRoles.DataBind();

        //string[] RoleMembership = Roles.GetRolesForUser(ViewState["CurrentUserName"].ToString());
        foreach (ListItem i in cblRoles.Items)
        {
            //Get Selected User's Roles           
            if (Roles.IsUserInRole(CurrentUser.UserName, i.Value))
                i.Selected = true;

        }
    }

    //Save
    protected void SaveProfileData()
    {

        //LoadProfile(mu);
        if (!string.IsNullOrEmpty(txtFirstName.Text))
            CurrentProfile.FirstName = txtFirstName.Text;
        else
        {
            CurrentProfile.FirstName = null;
            //tools.HandleResultJS("Cannot save profile, please provide a valid first name.", true, Page);
            //return;
        }

        if (!string.IsNullOrEmpty(txtLastName.Text))
            CurrentProfile.LastName = txtLastName.Text;
        else
        {
            CurrentProfile.LastName = null;
            //tools.HandleResultJS("Cannot save profile, please provide a valid last name.", true, Page);
            //return;
        }

        if (ddlCompany.SelectedValue != "0" && !string.IsNullOrEmpty(ddlCompany.SelectedValue))
        {
            CurrentProfile.CompanyID = ddlCompany.SelectedValue;
            CurrentProfile.CompanyName = ddlCompany.SelectedItem.ToString();
        }
        else
        {
            CurrentProfile.CompanyID = null;
            CurrentProfile.CompanyName = null;
            //tools.HandleResultJS("Cannot save profile, please choose a valid company.", true, Page);
            //return;
        }
        if (ddlContact.Visible)
            if (ddlContact.SelectedValue != "0" && ddlContact.SelectedValue != "")
            {
                CurrentProfile.ContactID = ddlContact.SelectedValue;
                CurrentProfile.ContactName = ddlContact.SelectedItem.ToString();
            }
            else
            {
                CurrentProfile.ContactID = null;
                CurrentProfile.ContactName = null;
                //tools.HandleResultJS("Cannot save profile, please choose a valid contact.", true, Page);
                //return;
            }
        if (ddlSensibleRep.SelectedValue != "0" && ddlSensibleRep.SelectedValue != null)
        {
            CurrentProfile.SensibleRep = ddlSensibleRep.SelectedItem.ToString();
            CurrentProfile.SensibleRepID = ddlSensibleRep.SelectedValue;
        }
        else
        {
            //tools.HandleResultJS("Cannot save profile, please choose a valid Sensible rep.", true, Page);
            //return;
        }

        if (ddlRzUser.Visible == true)
        {
            if (ddlRzUser.SelectedValue != "0")
            {
                CurrentProfile.RzUserID = ddlRzUser.SelectedValue;
                CurrentProfile.RzUserName = ddlRzUser.SelectedItem.ToString();
            }

            else
            {
                //tools.HandleResultJS("Please select a valid Rz User", true, Page);
                //return;
            }
        }

        try
        {
            CurrentProfile.Save();
            LoadControlValues(false);
            LoadRepData();
            //tools.HandleResultJS("Successfully updated profile infomation for " + profile.FirstName + " " + profile.LastName, true, Page);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    protected void SaveRoles()
    {
        try
        {
            if (ValidateRoles())
            {
                if (CurrentUser != null)
                {
                    foreach (ListItem i in cblRoles.Items)
                    {
                        if (!Roles.IsUserInRole(CurrentUser.UserName, i.Value.ToString()) && i.Selected)
                        {
                            Roles.AddUserToRole(CurrentUser.UserName, i.Text);
                        }
                        else if (Roles.IsUserInRole(CurrentUser.UserName, i.Value.ToString()) && !i.Selected)
                        {
                            if (i.Text == "admin" && CurrentUser.UserName == "kevint")
                            {
                                tools.HandleResultJS("Cannot remove user from admin role", true, Page);
                                return;
                            }
                            else
                            {
                                Roles.RemoveUserFromRole(CurrentUser.UserName, i.Text);

                            }

                        }
                    }
                    EnableRoleEdit();
                    tools.HandleResultJS("Roles Updated Successfully", true, Page);
                }
                else
                {
                    tools.HandleResultJS("Viewstate is null, cannot update roles.", true, null);

                }
            }
        }

        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }


    //End CRUD Events//

    //Data Events//

    //Load Rz Data
    protected void LoadCompanyDDls()
    {

        //Load the exisitng profile values, but populate the company dropdown with all companies for Selected Sensible User.
        //At this point we have either a Sensible User or a customer/ prospect.  If email address == @sensiblemicro.com , then Sensible User
        string email_address = Tools.Strings.SanitizeInput(CurrentUser.Email).Trim().ToLower();


        //Sensible Users be set to any company associated with the "RzUser" dropdown.
        if (email_address.Contains("@sensiblemicro.com"))
            LoadSensibleUserCompanies();
        else
            LoadCustomerCompanies(email_address);

    }

    private void LoadCustomerCompanies(string email_address)
    {
        List<companycontact> cList = new List<companycontact>();
        using (RzDataContext rdc = new RzDataContext())
            cList.AddRange(rdc.companycontacts.Where(w => w.primaryemailaddress.Trim().ToLower() == email_address).ToList());

        //IF No company Matches found.
        if (cList.Count == 0)
            throw new Exception("No companycontact record found for " + email_address);

        //IF Multiple company matches found
        string msgFoundCompanies = "";
        if (cList.Count > 1)
        {

            foreach (companycontact cc in cList)
            {
                string contactuid = cc.unique_id;
                string companyuid = cc.base_company_uid;
                string contactname = cc.contactname;
                string companyName = cc.companyname;
                msgFoundCompanies += "Contact: " + contactname + ",  Company: " + companyName;
                if (cc != cList.Last())
                    msgFoundCompanies += "<br />";
            }
            throw new Exception("Multiple companycontact records found for " + email_address + "<br /><br />" + msgFoundCompanies);
        }

        //Get the singular companycontact record
        companycontact c = cList[0];
        //Sanitize the input
        string email = Tools.Strings.SanitizeInput(c.primaryemailaddress).Trim().ToLower();
        //Confirm valid email address
        if (!Tools.Email.IsEmailAddress(email))
            throw new Exception("Email address for contact ('" + email + "') is not valid.");


        BindDdlCompany_Customer(email);
    }

    private void LoadSensibleUserCompanies()
    {
        string userID = ddlSensibleRep.SelectedValue;
        n_user u = RZDC.n_users.Where(w => w.unique_id == userID).FirstOrDefault();
        if (u == null)
            return;
        BindDdlCompany_RzUser(u.unique_id);

    }

    protected void BindDDLContact(string CompanyID = null)
    {
        if (CompanyID == null)
            pnlContact.Visible = false;
        else
        {
            pnlContact.Visible = true;
            ddlContact.DataSource = rzt.GetContactsForCompany(CompanyID);
            ddlContact.DataTextField = "Value";
            ddlContact.DataValueField = "Key";
            ddlContact.Items.Clear();
            ddlContact.Items.Add(new ListItem("--Choose Contact--", "0"));
            ddlContact.AppendDataBoundItems = true;
            ddlContact.DataBind();

        }

    }

    protected void LoadRepData()
    {


        //var users = from u in RZDC.n_users
        //            select new { name = u.name, unique_id = u.unique_id };
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            var users = (from u in pdc.Reps
                         select new { name = u.FullName, RzRepID = u.RzRepID }).OrderBy(x => x.name);
            ddlSensibleRep.DataSource = users.ToList();



            ddlSensibleRep.DataTextField = "name";
            ddlSensibleRep.DataValueField = "RzRepID";
            ddlSensibleRep.Items.Clear();
            ddlSensibleRep.Items.Add(new ListItem("--Choose Rep--", "0"));
            ddlSensibleRep.AppendDataBoundItems = true;
            ddlSensibleRep.DataBind();
            if (!string.IsNullOrEmpty(CurrentProfile.SensibleRepID))
            {
                if (pdc.Reps.Where(w => w.RzRepID == CurrentProfile.SensibleRepID).Any())
                {
                    ddlSensibleRep.SelectedValue = CurrentProfile.SensibleRepID;
                    hfRepEmail.Value = GetRepEmail(CurrentProfile.SensibleRep);
                    LoadRepImage(CurrentProfile.SensibleRep);

                }
            }
            else
            {
                ddlSensibleRep.SelectedValue = "0";
            }
        }
    }


    //End Data Events//
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblCompanyID.Text = ddlCompany.SelectedValue;
        lblCompanyName.Text = ddlCompany.SelectedItem.ToString();
        //LoadContactData(ddlCompany.SelectedValue);
        BindDDLContact(ddlCompany.SelectedValue);
    }
    protected void ddlContact_SelectedIndexChanged(object sender, EventArgs e)
    {


    }

    protected void btnLockUser_Click(object sender, EventArgs e)
    {
        if (btnLockUser.Text == "Lock User")
        {
            btnLockUser.Text = "Cancel Lock";
            btnConfirmLock.Text = "Confirm Lock";
            pnlLock.Visible = true;

        }
        else if (btnLockUser.Text == "Unlock User")
        {
            btnLockUser.Text = "Cancel Unlock";
            btnConfirmLock.Text = "Confirm Unlock";
            pnlLock.Visible = true;
        }
        else
        {
            btnLockUser.Text = "Lock User";
            pnlLock.Visible = false;
        }


    }
    protected void btnConfirmLock_Click(object sender, EventArgs e)
    {
        if (btnConfirmLock.Text == "Confirm Lock")
            LockUser();
        else if (btnConfirmLock.Text == "Confirm Unlock")
        {
            UnlockUser();
        }

    }

    protected void LockUser()
    {
        if (!string.IsNullOrEmpty(txtConfirmLock.Text))
            if (txtConfirmLock.Text != CurrentUser.UserName)
            {
                tools.HandleResultJS("Lock failed. Username does not match", false, Page, null);
                return;
            }
            else if (Roles.IsUserInRole(CurrentUser.UserName, "admin"))
            {
                tools.HandleResultJS("Lock failed. Cannot lock admins.", true, Page);
                return;
            }
            else
            {
                try
                {


                    using(sm_portalEntities pdc = new sm_portalEntities())
                        {
                        var query = (from m in pdc.Memberships
                                     where m.UserId.ToString() == CurrentUser.ProviderUserKey.ToString()
                                     select m).FirstOrDefault();
                        if (query != null)
                        {
                            query.IsLockedOut = true;
                            pdc.SaveChanges();
                            string LockSuccessAlert;
                            LockSuccessAlert = "alert('Successfully locked user:   " + CurrentUser.UserName + ".');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseWindow", LockSuccessAlert, true);
                            txtConfirmLock.Text = "";
                            btnLockUser.Text = "Unlock User";
                            btnDeleteUser.Visible = true;
                            pnlLock.Visible = false;
                        }
                    }
                    

                }
                catch (Exception ex)
                {
                    tools.HandleResultJS(ex.Message, true, Page);
                }
            }
        else
            tools.HandleResultJS("Must provide the username.", true, Page);
    }

    protected void UnlockUser()
    {
        if (txtConfirmLock.Text != CurrentUser.UserName)
        {
            tools.HandleResultJS("Unlock failed. Username does not match", true, Page);
            return;
        }

        try
        {using (sm_portalEntities pdc = new sm_portalEntities())
            {

                //string test = ViewState["UserEditId"].ToString();
                //Membership.DeleteUser(ViewState["CurrentUserName"].ToString(), true);              
                var query = (from m in pdc.Memberships
                             where m.UserId == (Guid)CurrentUser.ProviderUserKey
                             select m).FirstOrDefault();
                if (query != null)
                {
                    query.IsLockedOut = false;
                    pdc.SaveChanges();
                    string LockSuccessAlert;
                    LockSuccessAlert = "alert('Successfully unlocked user:   " + CurrentUser.UserName + ".');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseWindow", LockSuccessAlert, true);
                    txtConfirmLock.Text = "";
                    pnlLock.Visible = false;
                    btnLockUser.Text = "Lock User";
                    btnDeleteUser.Visible = false;
                }
            }
               

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }



    }

    protected void btnDeleteUser_Click(object sender, EventArgs e)
    {
        string DeleteSuccessAlert;
        DeleteSuccessAlert = "alert('Successfully deleted user:   " + CurrentUser.UserName + ".');window.location.href='user_management.aspx';";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseWindow", DeleteSuccessAlert, true);
        pnlLock.Visible = false;
        System.Web.Security.Membership.DeleteUser(CurrentUser.UserName);
    }

    protected void ddlSensibleRep_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadCompanyData(ddlSensibleRep.SelectedValue);
        LoadRepImage(ddlSensibleRep.SelectedItem.ToString());
        LoadCompanyDDls();
        //BindDdlCompany(ddlSensibleRep.SelectedValue);
    }

    protected void BindDDLRzUser()
    {
        ddlRzUser.DataSource = rt.GetCurrentRzUsers_DDL();
        ddlRzUser.DataTextField = "Value";
        ddlRzUser.DataValueField = "Key";
        ddlRzUser.Items.Clear();
        ddlRzUser.Items.Add(new ListItem("--Choose Rz User--", "0"));
        ddlRzUser.AppendDataBoundItems = true;
        ddlRzUser.DataBind();
    }
    protected bool IsRzUser()
    {
        if (CurrentUser.Email.Contains("@sensiblemicro.com"))
            return true;
        return false;
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {

        btnSaveProfile.Visible = true;
        btnSaveProfile.Enabled = true;
        pnlProfileEdit.Visible = true;
        pnlProfileData.Visible = false;
        //LoadCompanyData();
        LoadRepData();
        btnDisableEdit.Visible = true;
        btnEdit.Visible = false;
    }
    protected void btnDisableEdit_Click(object sender, EventArgs e)
    {
        btnSaveProfile.Visible = false;
        btnSaveProfile.Enabled = false;
        pnlProfileEdit.Visible = false;
        pnlProfileData.Visible = true;
        btnDisableEdit.Visible = false;
        btnEdit.Visible = true;

    }

    protected void lbSetTempPassword_Click(object sender, EventArgs e)
    {
        try
        {
            SM_Security.ResetPassword(CurrentUser, false);
            tools.HandleResult("success", "User password successfully reset.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", "Password not reset. Error:  " + ex.Message);
        }

    }
}