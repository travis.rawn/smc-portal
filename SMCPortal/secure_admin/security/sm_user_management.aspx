﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="sm_user_management.aspx.cs" Inherits="secure_admin_user_management" %>


<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Search Users:</b>
                    <asp:TextBox ID="txtSearch" runat="server" Width="250px" Font-Italic="True" Placeholder="Search by Username or Email"></asp:TextBox>
                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch" WatermarkText="Search by Username or Email" />--%>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" Style="padding-left: 15px" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnClearSearch" runat="server" Text="Clear" Style="padding-left: 15px" OnClick="btnClearSearch_Click" Visible="false" />
                    <asp:CheckBox ID="cbxShowSensible" runat="server" Text="Show Sensible Users:" Checked="False"/>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnPageIndexChanging="GridView1_PageIndexChanging" OnSorting="GridView1_Sorting" DataSourceID="LinqDataSource1" CssClass="table table-striped table-bordered table-hover" GridLines="None">
                            <PagerStyle CssClass="pagination-ys" />
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="UserName" HeaderText="User" SortExpression="UserName" />
                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                <asp:BoundField DataField="CreationDate" HeaderText="Created" SortExpression="CreationDate" DataFormatString="{0:d}" HtmlEncode="false" />
                                <asp:BoundField DataField="IsApproved" HeaderText="Approved" SortExpression="IsApproved" />
                                <asp:BoundField DataField="Comment" HeaderText="TempPW" SortExpression="Comment" />
                                <asp:BoundField DataField="IsLockedOut" HeaderText="Locked" SortExpression="IsLockedOut" />
                                <asp:BoundField DataField="LastLoginDate" HeaderText="Last" SortExpression="LastLoginDate" DataFormatString="{0:d}" HtmlEncode="false" />
                                <asp:BoundField DataField="IsOnline" HeaderText="Online" SortExpression="IsOnline" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="well">
                        <asp:Button runat="server" ID="btnNewUser" Text="New User" OnClick="btnNewUser_Click" />
                        <h4>Users online in the last 30 minutes:</h4>
                        <asp:Label ID="lblCurrentUsers" runat="server" />
                        <asp:ListBox ID="lbOnlineUsers" runat="server" Style="max-width: 250px; min-width: 250px;"></asp:ListBox>
                        <asp:Label ID="lblPageAlert" runat="server" /><br />
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" OnSelecting="LinqDataSource1_Selecting"></asp:LinqDataSource>
</asp:Content>
