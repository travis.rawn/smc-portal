﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="profile_management.aspx.cs" Inherits="secure_admin_security_profile_management" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="text-align: center">
        <h1>PROFILE MANAGEMENT</h1>
    </div>

    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
        <div class="panel">
            <div class="panel panel-heading">
                <div class="row">
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-2">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="btn-smc btn-smc-primary btn-block" />
                    </div>
                    <div class="col-sm-2">
                        <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CssClass="btn-smc btn-smc-primary btn-block" />
                    </div>
                    <div class="col-sm-2">
                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" CssClass="btn-smc btn-smc-primary btn-block" />
                    </div>
                </div>
                <div class="row" style="padding-top: 10px; text-align: center;">
                    <div class="col-sm-2">
                        <asp:CheckBox ID="cbxIncInternal" runat="server" Text="Include Internal" CssClass="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <asp:CheckBox ID="cbxMissingName" runat="server" Text="Missing Name" CssClass="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <asp:CheckBox ID="cbxMissingRep" runat="server" Text="Missing Rep" CssClass="form-control" />
                    </div>
                    <div class="col-sm-3">
                        <asp:CheckBox ID="cbxMissingCompany" runat="server" Text="Missing Company" CssClass="form-control" Checked="True" />
                    </div>
                    <div class="col-sm-3">
                    </div>


                </div>
            </div>
            <div class="panel panel-body">
                <label>
                    <asp:Label ID="lblQueryCount" runat="server"></asp:Label></label>
                <asp:GridView ID="gvProfiles" runat="server" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="false" OnRowDataBound="gvProfiles_RowDataBound">
                    <EmptyDataTemplate>
                        <div class="alert alert-warning" style="text-align: center;">
                            <h5>No data found that matches your search criteria.</h5>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField ControlStyle-CssClass="btn-smc btn-smc-primary btn-block">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlSelect" runat="server" Target="_blank">Select</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="userName" HeaderText="User Name" SortExpression="userName" />
                        <asp:BoundField DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName" />
                        <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="FullName" />
                        <asp:BoundField DataField="SensibleRep" HeaderText="Rep" SortExpression="SensibleRep" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

