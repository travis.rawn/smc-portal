﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data;
using System.Collections;
using System.Data.Linq.SqlClient;

public partial class secure_admin_user_management : System.Web.UI.Page
{
    //Public Variables

    //SMCPortalDataContext PDC = new SMCPortalDataContext();
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        smdt.RowDataBound += new GridViewRowEventHandler(gv_RowDataBound);
        //if (!Page.IsPostBack)
        LoadUserGrid();
        ListOnlineUsers();

    }

    private void LoadUserGrid()
    {

        bool boolSensibleOnly = cbxShowSensible.Checked;



        var query = (from MembershipUser u in Membership.GetAllUsers()
                     where u.Email.Contains("sensiblemicro.com") == boolSensibleOnly
                     select new
                     {
                         Check = "",
                         Select = "",
                         User = u.UserName,
                         Email = u.Email,
                         Created = u.CreationDate.ToShortDateString(),
                         Approved = u.IsApproved,
                         Comment = u.Comment,
                         LockedOut = u.IsLockedOut,
                         Last = u.LastLoginDate,
                         Online = u.IsOnline,
                         //id = u.ProviderUserKey
                     }).ToList();

        smdt.pageLength = 50;
        smdt.dataSource = query;
        smdt.loadGrid();



    }

    protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //ID-ProviderUserKey / Datakey column = 10
        //Checkbox col = 0
        //Select col = 1
        //int uidCell = 10;
        int checkboxCell = 0;
        TableCell chkCell = e.Row.Cells[checkboxCell];

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbxSelect = new CheckBox();
            cbxSelect.ID = "cbxSelect";
            chkCell.Text = string.Empty;
            chkCell.Controls.Add(cbxSelect);
            AddHyperlink(e.Row);
        }


    }


    private HyperLink AddHyperlink(GridViewRow row)
    {
        HyperLink hlEdit = new HyperLink();
        hlEdit.ID = "hlSelect";
        hlEdit.Text = "Edit";
        row.Cells[1].Text = string.Empty;
        string strUserName = Tools.Strings.SanitizeInput(row.Cells[2].Text).ToUpper();
        MembershipUser mu = Membership.GetUser(strUserName);
        hlEdit.NavigateUrl = "~/secure_admin/security/user_edit.aspx?uid=" + mu.ProviderUserKey;
        hlEdit.Target = "_blank";
        row.Cells[1].Controls.Add(hlEdit);
        return hlEdit;
    }




    protected void ListOnlineUsers()
    {
        lblCurrentUsers.Text = "Total users online: " + Membership.GetNumberOfUsersOnline().ToString();
        Dictionary<string, DateTime> OnlineUsers = new Dictionary<string, DateTime>();
        var query = from MembershipUser user in Membership.GetAllUsers()
                    select user;
        foreach (MembershipUser u in query)
        {
            TimeSpan loggedintime = new TimeSpan(0, 0, 30, 0, 0);
            if (u.LastActivityDate >= DateTime.Now.Subtract(loggedintime))
            {
                OnlineUsers.Add(u.UserName, u.LastActivityDate);
            }
        }
        var orderedlist = from o in OnlineUsers
                          orderby o.Value descending
                          select o;
        lbOnlineUsers.DataSource = orderedlist;
        lbOnlineUsers.DataBind();
    }

    //protected void SearchUsers()
    //{
    //    //GridView1.DataBind();
    //    LoadUserGrid();
    //    btnClearSearch.Visible = true;
    //}

    //protected void ClearSearch()
    //{
    //    //txtSearch.Text = "";
    //    GridView1.DataBind();
    //    btnClearSearch.Visible = false;
    //}

    //public void GetMembershipDataLinq(string searchString)
    //{
    //    if (searchString == null)
    //    {
    //        var query = (from MembershipUser u in Membership.GetAllUsers()
    //                     select u).ToList();
    //        GridView1.DataBind();
    //    }
    //    else
    //    {
    //        var query = (from MembershipUser u in Membership.GetAllUsers()
    //                     where u.UserName.Contains(searchString) || u.Email.Contains(searchString)
    //                     select u).ToList();
    //        if (query.Any())
    //            GridView1.DataBind();
    //        else
    //            HandleError("No such user found.");
    //    }
    //}

    //protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    GridViewRow row = GridView1.SelectedRow;
    //    MembershipUser mu = Membership.GetUser(row.Cells[2].Text.ToString());
    //    Session["UserEditId"] = mu.ProviderUserKey;
    //    string test = Session["UserEditId"].ToString();
    //    Response.Redirect("~/secure_admin/security/user_edit.aspx?uid=" + mu.ProviderUserKey, false);
    //}
    protected void btnNewUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("/secure_admin/security/user_new.aspx", false);
    }
    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    SearchUsers();
    //}

    //protected void btnClearSearch_Click(object sender, EventArgs e)
    //{
    //    ClearSearch();
    //}


    //protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    //{
    //    string searchString = null; bool boolSens = cbxShowSensible.Checked;

    //    if (!string.IsNullOrEmpty(txtSearch.Text))
    //    {
    //        searchString = txtSearch.Text.ToLower();
    //        var query = (from MembershipUser u in Membership.GetAllUsers()
    //                     where u.Email.Contains("sensiblemicro.com") == boolSens && (u.Email.Contains(searchString) || u.UserName.Contains(searchString))
    //                     select u).ToList();
    //        e.Result = query;
    //    }
    //    else
    //    {

    //        var query = (from MembershipUser u in Membership.GetAllUsers()
    //                     where u.Email.Contains("sensiblemicro.com") == boolSens
    //                     select u).ToList();
    //        e.Result = query;
    //    }





    //}
    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridView1.PageIndex = e.NewPageIndex;
    //    GridView1.DataBind();
    //}
    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    LinqDataSource1.OrderBy = e.SortExpression;
    //}

    protected void HandleError(string msg)
    {
        lblPageAlert.Visible = true;
        lblPageAlert.Text = msg;
        lblPageAlert.ForeColor = System.Drawing.Color.Red;
    }

    protected void HandleSuccess(string msg)
    {
        lblPageAlert.Visible = true;
        lblPageAlert.Text = msg;
        lblPageAlert.ForeColor = System.Drawing.Color.Green;
    }

    protected void cbxShowSensible_CheckedChanged(object sender, EventArgs e)
    {
        LoadUserGrid();
    }

    protected void btnDeleteSelected_Click(object sender, EventArgs e)
    {
        List<MembershipUser> selectedUsers = new List<MembershipUser>();
        foreach (GridViewRow row in smdt.theGridView.Rows)
        {
            //Get a list of selected users:
            string name = row.Cells[2].Text;
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkSelected = null;

                chkSelected = (CheckBox)row.Cells[0].Controls[0];

                //(CheckBox)row.FindControl("cbxSelect");

                if (chkSelected.Checked == true)
                {
                    MembershipUser mu = Membership.GetUser(row.Cells[2].Text.ToString());
                    selectedUsers.Add(mu);
                }
            }          

        }

        //From selected users, delete.             
        if (selectedUsers.Count > 0)
        {
            foreach (MembershipUser u in selectedUsers)
            {
                Membership.DeleteUser(u.UserName);
            }
            //GridView1.DataBind();
            LoadUserGrid();
        }






    }

    protected void lbApproveToggle_Click(object sender, EventArgs e)
    {
        List<MembershipUser> selectedUsers = new List<MembershipUser>();
        foreach (GridViewRow row in smdt.theGridView.Rows)
        {
            if (((CheckBox)row.FindControl("cbxSelect")).Checked)
            {
                MembershipUser mu = Membership.GetUser(row.Cells[2].Text.ToString());
                selectedUsers.Add(mu);
            }
        }

        if (selectedUsers.Count > 0)
        {
            foreach (MembershipUser u in selectedUsers)
            {
                u.IsApproved = !u.IsApproved;
                Membership.UpdateUser(u);
            }
            //GridView1.DataBind();
            LoadUserGrid();
        }
    }

    protected void lbSetTempPasswordStatus_Click(object sender, EventArgs e)
    {
        SetTempPasswordStatus(null);

    }

    private void SetTempPasswordStatus(MembershipUser singleUser, bool includeSensible = false)
    {
        MembershipUserCollection memberColl = Membership.GetAllUsers();
        List<MembershipUser> userList = memberColl.Cast<MembershipUser>().ToList();
        if (singleUser != null)
            userList = userList.Where(w => w.UserName == singleUser.UserName).ToList();
        else if (!includeSensible)
            userList = userList.Where(w => !w.Email.ToLower().Contains("@sensiblemicro.com")).ToList();
        foreach (MembershipUser u in userList)
        {
            //Do Reset
            string tempPassword = u.ResetPassword();
            //Set Temp Status
            SM_Security.SetTemporaryPasswordStatus(u, true, "Due to recent security upgrades, we are requiring that all users reset their passwords.  Please use the 'Forgot Password' link below to reset your password.");

        }
    }

    protected void lbSetTempPwSelectedUsers_Click(object sender, EventArgs e)
    {
        List<MembershipUser> selectedUsers = new List<MembershipUser>();
        foreach (GridViewRow row in smdt.theGridView.Rows)
        {
            if (((CheckBox)row.FindControl("cbxSelect")).Checked)
            {
                MembershipUser mu = Membership.GetUser(row.Cells[2].Text.ToString());
                selectedUsers.Add(mu);
            }
        }
        if (selectedUsers.Count <= 0)
        {
            HandleError("Please select a user.");
            return;
        }

        foreach (MembershipUser u in selectedUsers)
        {
            SetTempPasswordStatus(u);
        }
        LoadUserGrid();




    }



}
