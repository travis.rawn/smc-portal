﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class secure_admin_CreateUserWizardWithRoles : System.Web.UI.Page
{
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void RegisterUser_CreatedUser(object sender, EventArgs e)
    {
        try
        {
            //Force password change on new account creation
            MembershipUser user = Membership.GetUser(RegisterUser.UserName.ToLower());
            //user.Comment = SM_Security.ValidationStatus.NeedsTempPasswordReset.ToString();
            SM_Security.SetTemporaryPasswordStatus(user, true);
            Membership.UpdateUser(user);
            tools.HandleResult("success", "User successfully created.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);

        }
    }

    protected void RegisterUser_CreatingUser(object sender, LoginCancelEventArgs e)
    {
        try
        {
            string userName = Tools.Strings.SanitizeInput(RegisterUser.UserName);
            string email = Tools.Strings.SanitizeInput(RegisterUser.Email);
            if (string.IsNullOrEmpty(userName))
                throw new Exception("Please enter a user name / email.");
            if (string.IsNullOrEmpty(email))
                throw new Exception("Please enter a user name / email.");
            if (!Tools.Email.IsEmailAddress(email))
                throw new Exception(email + " does not appear to be a valid email address.");
            
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", "Error creating user: " + ex.Message);


        }

    }
}