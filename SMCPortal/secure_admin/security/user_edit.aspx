﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="user_edit.aspx.cs" Inherits="secure_admin_user_edit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfUserEmail" runat="server" />
    <asp:HiddenField ID="hfUserName" runat="server" />
    <asp:HiddenField ID="hfRepEmail" runat="server" />




    <div style="text-align: center">
        <h2>Edit User</h2>
    </div>
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="well well-sm">
                <asp:Label ID="lblCurrentUser" runat="server" Font-Bold="true" /></h2>
        </div>
    </div>
    <div class="row">
        <%--profile--%>
        <div class="col-sm-8 form-group">
            <asp:Panel ID="pnlProfile" runat="server" CssClass="well well-sm">
                <hr />
                <h5>Profile Data:</h5>
                <div class="row">
                    <div class="col-sm-8">
                        <asp:Panel ID="pnlProfileEdit" runat="server" CssClass="well well-sm">
                            <label>First Name:</label><asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                            <label>Last Name:</label><asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox><br />
                            <label>Choose Sensible Rep:</label><asp:DropDownList ID="ddlSensibleRep" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSensibleRep_SelectedIndexChanged" CssClass="form-control" />
                            <label>Choose Company:</label>
                            <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" CssClass="form-control" /><br />
                            <asp:Panel ID="pnlContact" runat="server">
                                <label>Choose Contact:</label><asp:DropDownList ID="ddlContact" runat="server" CssClass="form-control" /><br />
                            </asp:Panel>
                            <span id="spnChooseRzUser" runat="server" visible="false">
                                <label>Set As Rz User:</label><asp:DropDownList ID="ddlRzUser" runat="server" CssClass="form-control" Visible="false" /></span>
                        </asp:Panel>
                        <asp:Panel ID="pnlProfileData" runat="server">
                            <label>Full Name:</label><asp:Label ID="lblFullName" runat="server" Text="Label"></asp:Label><br />
                            <label>Sensible Rep:</label><asp:Label ID="lblSensibleRep" runat="server"></asp:Label><br />
                            <label>Company Name:</label><asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label><br />
                            <label>CompanyID:</label><asp:Label ID="lblCompanyID" runat="server" Text="Label" Font-Size="Smaller"></asp:Label><br />
                            <label>Contact Name:</label><asp:Label ID="lblContactName" runat="server" Text="Label"></asp:Label><br />
                            <label>ContactID:</label><asp:Label ID="lblContactID" runat="server" Text="Label" Font-Size="Smaller"></asp:Label><br />
                            <label>Email:</label><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label><br />
                            <span id="spnRzUserData" runat="server" visible="false">
                                <label>Rz User:</label><asp:Label ID="lblRzUser" runat="server"></asp:Label><br />
                            </span>
                        </asp:Panel>
                    </div>
                    <div class="col-sm-4">
                        <asp:Image ID="imgSensibleRep" runat="server" CssClass="img-thumbnail img-rounded" />
                        <asp:Label ID="lblSensibleRepAlert" runat="server" Text="" Font-Bold="True" Font-Italic="True" ForeColor="red"></asp:Label>
                    </div>
                </div>
                <asp:Button ID="btnSaveProfile" runat="server" Text="Save Profile" Enabled="false" Visible="false" OnClick="btnSaveProfile_Click" CssClass="btn-smc btn-block btn-success" />
                <asp:Button ID="btnEdit" runat="server" Text="Enable Editing" OnClick="btnEdit_Click" CssClass="btn-smc btn-block btn-primary" />
                <asp:Button ID="btnDisableEdit" runat="server" Text="Disable Editing" OnClick="btnDisableEdit_Click" CssClass="btn-smc btn-block btn-primary" Visible="false" />

            </asp:Panel>
        </div>
        <%--roles--%>
        <div class="col-sm-4">
            <asp:Panel ID="pnlRoles" runat="server" CssClass="well well-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Role Data:</label>
                        <asp:CheckBoxList ID="cblRoles" runat="server" Enabled="false" CellPadding="5" CellSpacing="5" RepeatColumns="2" RepeatDirection="Horizontal"></asp:CheckBoxList>
                        <asp:Button ID="btnSaveRoles" runat="server" Text="Save Roles" Visible="false" OnClick="btnSaveRoles_Click" CssClass="btn-smc btn-block btn-success" />
                        <asp:Button ID="btnEditRoles" runat="server" Text="Edit Roles" OnClick="btnEditRoles_Click" CssClass="btn-smc btn-block btn-primary" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <asp:Button ID="btnReturn" runat="server" Text="Return" OnClick="btnReturn_Click" CssClass="btn btn-sm btn-primary" />
            <%--<asp:LinkButton ID="" runat="server" OnClick="btnSendWelcomeEmail_Click" CssClass="btn-smc btn-success" OnClientClick="return doSendWelcomeEmail();">Send Welcome Email </asp:LinkButton>--%>
            <asp:LinkButton ID="btnSendWelcomeEmail" runat="server" OnClick="btnSendWelcomeEmail_Click" CssClass="btn btn-sm btn-success" OnClientClick="return doSendWelcomeEmail();"> Send Welcome Email To User</asp:LinkButton>
            <asp:LinkButton ID="btnSendWelcomeEmailRep" runat="server" OnClick="btnSendWelcomeEmailRep_Click" CssClass="btn btn-sm btn-success" OnClientClick="return doSendWelcomeEmailToRep();"> Send Welcome Email To Rep</asp:LinkButton>
            <asp:Button ID="btnLockUser" runat="server" Text="Lock User" OnClick="btnLockUser_Click" CssClass="btn btn-sm btn-danger" />
            <asp:Button ID="btnDeleteUser" runat="server" Text="Delete User" OnClick="btnDeleteUser_Click" Visible="false" CssClass="btn btn-sm btn-danger" />
            <asp:LinkButton ID="lbSetTempPassword" runat="server" Text="Set Temp Password" OnClick="lbSetTempPassword_Click" OnClientClick="return confirm('Are you sure you want to reset this password?');" CssClass="btn btn-sm btn-danger" />
            <div id="pnlLock" runat="server" visible="false">
                <div class="row">
                    <div class="col-sm-8 alert alert-danger">
                        <asp:Label ID="lblConfirmLock" runat="server" Text="Enter the exact username (case sensitive) below to confirm." ToolTip="Enter the username" />
                        <asp:TextBox ID="txtConfirmLock" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnConfirmLock" runat="server" Text="Confirm Lock" OnClick="btnConfirmLock_Click" CssClass="btn btn-sm btn-danger" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function doSendWelcomeEmail() {
            var bool = false;
            var toEmail = $("#MainContent_hfUserEmail").val()
            var toName = $("#MainContent_hfUserName").val();
            bool = confirm("Are you sure you want to send a portal invitation email to: " + toName +"  / " + toEmail + "?");
            return bool;

        };
        function doSendWelcomeEmailToRep() {
            var bool = false;
            var toEmail = $("#MainContent_hfRepEmail").val()
            bool = confirm("Are you sure you want to send a portal invitation email to: " + toEmail + "?");
            return bool;

        };
    </script>
</asp:Content>

