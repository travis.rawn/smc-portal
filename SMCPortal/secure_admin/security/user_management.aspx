﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="user_management.aspx.cs" Inherits="secure_admin_user_management" %>


<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="text-align: center">
        <h1 class="page-header">User Management</h1>
    </div>
    <%--<div class="panel panel-default">
        <div class="panel-heading">--%>
    <div class="row">
        <div class="col-sm-12">
            <asp:LinkButton runat="server" ID="btnNewUser" Text="New User" OnClick="btnNewUser_Click" OnClientClick="ShowUpdateProgress();" />
            <asp:CheckBox ID="cbxShowSensible" runat="server" Text="Show Sensible Users:" Checked="False" AutoPostBack="true" OnCheckedChanged="cbxShowSensible_CheckedChanged" />
            <asp:LinkButton ID="lbApproveToggle" runat="server" Text="Toggle Approved" Style="padding-left: 15px" OnClick="lbApproveToggle_Click" CssClass="btn-smc btn-smc-warning" OnClientClick="return confirm('Are you sure you want to change the approval status of the selected user(s)?');"></asp:LinkButton>
            <asp:LinkButton ID="lbSetTempPwSelectedUsers" runat="server" Text="Set Temp PW" Style="padding-left: 15px" OnClick="lbSetTempPwSelectedUsers_Click" CssClass="btn-smc btn-smc-danger" OnClientClick="return confirm('Warning: Are you sure you want to set temp password status for this user?');"></asp:LinkButton>
            <asp:LinkButton ID="btnDeleteSelected" runat="server" Text="Delete" Style="padding-left: 15px" OnClick="btnDeleteSelected_Click" CssClass="btn-smc btn-smc-danger" OnClientClick="return confirm('Warning: Are you sure you want to delete this user?');"></asp:LinkButton>
        </div>
    </div>
    <%-- </div>--%>
    <!-- /.panel-heading -->
    <%--  <div class="panel-body">--%>
    <div class="row">
        <div class="col-sm-12">
            <uc1:sm_datatable runat="server" ID="smdt" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <asp:LinkButton ID="lbSetTempPasswordStatus" runat="server" Text="Set Temp Password - All Users" Style="padding-left: 15px" OnClick="lbSetTempPasswordStatus_Click" CssClass="btn-smc btn-smc-warning" OnClientClick="return confirm('Warning: This will force ALL USERS to change their password.  Continue?');"></asp:LinkButton>
        </div>
    </div>
    <div class="well">
        <h4>Users online in the last 30 minutes:</h4>
        <asp:Label ID="lblCurrentUsers" runat="server" />
        <asp:ListBox ID="lbOnlineUsers" runat="server" Style="max-width: 250px; min-width: 250px;"></asp:ListBox>
        <asp:Label ID="lblPageAlert" runat="server" /><br />
    </div>
    <%--</div>--%>
    <!-- /.panel-body -->
    <%-- </div>--%>
</asp:Content>
