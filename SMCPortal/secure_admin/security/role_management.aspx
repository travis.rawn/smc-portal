﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="role_management.aspx.cs" Inherits="secure_admin_role_management" %>


<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
     <div style="text-align: center">
        <h1 class="page-header">Role Management.</h1>
    </div>
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="form-inline">
                <asp:TextBox ID="txtRoleName" runat="server" CssClass="form-control" placeholder="New Role Name"></asp:TextBox>
                <asp:Button ID="btnCreateRole" runat="server" OnClick="btnCreateRole_Click" Text="Create Role" CssClass="btn-smc btn-success" />
                <asp:Label ID="lblPageAlert" runat="server"></asp:Label>
                <a href="#pnlAddUser" data-toggle="collapse" class="btn-smc btn-primary-sm pull-right">Add to Selected Role(s)</a>

            </div>
            <div id="pnlAddUser" class="form-inline collapse" style="margin-top: 3px;">
                <asp:DropDownList ID="ddlAddUserToRoles" runat="server" CssClass="form-control"></asp:DropDownList>
                <asp:Button ID="btnAddUserToRoles" runat="server" OnClick="AddUserToRoles_Click" Text="Add" CssClass="btn-smc btn-success" />
                <a href="#pnlAddUser" data-toggle="collapse" class="btn-smc btn-warning">Cancel</a>

                <%--<asp:Button ID="btnCancelAddUserToRoles" runat="server" OnClick="btnCancelAddUserToRoles_Click" Text="Cancel" CssClass="btn-smc btn-warning" />--%>
            </div>

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="RoleList_SelectedIndexChanged" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" AllowPaging="false" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging" CssClass="table table-striped table-bordered table-hover" GridLines="None">
                        <PagerStyle CssClass="pagination-ys" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbxSelectRole" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Roles">
                                <ItemTemplate>
                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField DeleteText="" SelectText="Manage" ShowSelectButton="True" ButtonType="Button" CancelText="" EditText="" ControlStyle-CssClass="btn-smc btn-primary" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="col-sm-6">
                    <div id="divEditRole">
                        <asp:Panel ID="pnlEditRole" runat="server" Visible="false">
                            <h3>Details for:&nbsp;<asp:Label ID="lblRoleName" runat="server"></asp:Label></h3>
                            Current Users in Role:
                       <asp:CheckBoxList ID="cblUsersInRole" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" RepeatDirection="Horizontal"></asp:CheckBoxList>
                            <asp:Button ID="btnSaveRoles" runat="server" OnClick="btnSaveRoles_Click" Text="Save changes to roles" CssClass="btn-smc btn-success" />
                            <asp:Button ID="btnDeleteRole" runat="server" OnClientClick="return confirm('Are you certain you want to delete this Role?');" OnClick="btnDeleteRole_Click" Text="Delete Role" Visible="false" CssClass="btn-smc btn-danger" /><br />
                        </asp:Panel>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->

    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

