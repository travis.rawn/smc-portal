﻿using SensibleDAL.dbml;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using SensibleDAL;
using System.Diagnostics;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Drawing;
using System.Net.Http;

public partial class secure_admin_integrations_silicon_expert_admin : System.Web.UI.Page
{
    
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            LoadControls();
        GetUserStatus();
       
    }

    private void LoadControls()
    {
        cbEnable.isChecked = false;
        using (RzDataContext rdc = new RzDataContext())
        {
            

            system_management smSiliconExpert = new system_management();
            smSiliconExpert = rdc.system_managements.Where(w => w.system_name == "silicon_expert").FirstOrDefault();
            if (smSiliconExpert == null)
            {
                smSiliconExpert = new system_management();
                smSiliconExpert.unique_id = Guid.NewGuid().ToString();
                smSiliconExpert.system_name = "silicon_expert";
                smSiliconExpert.date_created = DateTime.Now;
                smSiliconExpert.date_modified = DateTime.Now;
                rdc.system_managements.InsertOnSubmit(smSiliconExpert);
            }
            cbEnable.isChecked = smSiliconExpert.is_enabled ?? false;
            string statusMessage = "Silicon Expert API Services are currently:  ";
            if (smSiliconExpert.is_enabled ?? false)
            {
                statusMessage += "ENABLED";
               //cbEnable.BackColor = Color.Green;
            }
               
            else
            {
                statusMessage += "DISABLED";
                //lblAPIStatus.BackColor = Color.Red;
            }
            cbEnable.theText = statusMessage;
        }
    }

    protected DataTable DTResults
    {
        get
        {
            if (ViewState["DTResults"] != null)
                return (DataTable)(ViewState["DTResults"]);
            else
                return null;
        }
        set
        {
            ViewState["DTResults"] = value;
        }
    }

    protected async void btnTestAuth_Click(object sender, EventArgs e)
    {
        try
        {
            HttpResponseMessage responseMessage = await SiliconExpertLogic.AuthenticateAsync();
            if (responseMessage.IsSuccessStatusCode)
                tools.HandleResult("success", "Successfully Authenticated");
            else
                tools.HandleResult("fail", "Authentication fail.");
        }
        catch (Exception ex)
        {
            tools.HandleResult(SM_Enums.LogType.Error.ToString(), ex.Message);
        }

    }

    protected void btnTestPartSearch_Click(object sender, EventArgs e)
    {
        try
        {
            smdt.dataSource = SiliconExpertLogic.GetListPartSearchDataSetXML("at91SAM", true).Tables[2];
            smdt.loadGrid();
            //gvTest.DataSource = si.GetListPartSearchDataSetXML("at91SAM", true).Tables[2];
            //gvTest.DataBind();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

    }

    protected void btnGetUserStatus_Click(object sender, EventArgs e)
    {
        GetUserStatus();
    }

    protected async void GetUserStatus()
    {
        gvUserStatus.DataSource = null;
        gvUserStatus.DataBind();

        
        try
        {
            SiliconExpertLogic.seUsageStatus us = await SiliconExpertLogic.GetUserStatusObjectAsync();
            if (us == null)
                throw new Exception("UserStatus Object returned null.");
            //Add object to List for DataBund
            List<SiliconExpertLogic.seUsageStatus> seUList = new List<SiliconExpertLogic.seUsageStatus>() { us};

            gvUserStatus.DataSource = seUList.Select(s => new {
                CreationDate = s.CreationDate.ToShortDateString(),
                ExpirationDate = s.ExpirationDate.ToShortDateString(),
                s.PartDetailLimit,
                s.PartDetailCount,
                s.PartDetailRemaining    
            });
            gvUserStatus.DataBind();

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

       
    }

    protected async void btnTop200CustomersSiliconExpertData_Click(object sender, EventArgs e)
    {
        ////Stopwatch to track search time.
        //Stopwatch stopWatch = new Stopwatch();        
        //stopWatch.Start();
        ////Carry a string for Search Results
        //string searchResults = "";
        ////Count to take (i.e. top 20 customers, etc)      
        //int takeQty = 2;        
        ////Actual Count of found resultant companies
        //int totalCompanies = 0;
        ////Get a Dictionary<company, List<string> Partnumbers
        //Dictionary<company, List<string>> dictCompanyAndPartNumbers = new Dictionary<company, List<string>>();
        ////Get A dictionary of Total Sales by Company
        //Dictionary<string, double> invoiceCompanyTotals = new Dictionary<string, double>();
        ////Get a list of the top 200 customers by sales
        //using (RzDataContext rdc = new RzDataContext())
        //{

        //    invoiceCompanyTotals = rdc.ordhed_invoices.Where(w => (w.base_company_uid ?? "").Length > 0).GroupBy(x => x.base_company_uid).Select(g => new
        //    {
        //        company_id = g.Key,
        //        total_sales = g.Sum(x => x.ordertotal > 0 ? x.ordertotal : 0) ?? 0
        //    }).OrderByDescending(o => o.total_sales).Take(takeQty).ToDictionary(d => d.company_id, d => d.total_sales);

        //    foreach (KeyValuePair<string, double> kvp in invoiceCompanyTotals)
        //    {
        //        string companyID = kvp.Key;
        //        if (!string.IsNullOrEmpty(companyID))
        //        {
        //            company c = rdc.companies.Where(w => w.unique_id == companyID).FirstOrDefault();
        //            if (c != null)
        //            {
        //                int linesYearsAgo = 1;
        //                List<string> linePartNumbers = rdc.orddet_lines.Where(w => w.customer_uid == companyID && w.date_created >= DateTime.Today.AddYears(-linesYearsAgo) ).Select(s => s.fullpartnumber.Trim().ToUpper()).Distinct().ToList();
        //                List<string> quotePartNumbers = rdc.orddet_quotes.Where(w => w.base_company_uid == companyID && w.date_created >= DateTime.Today.AddYears(-linesYearsAgo)).Select(s => s.fullpartnumber.Trim().ToUpper()).Distinct().ToList();
        //                List<string> combinedParts = linePartNumbers.Union(quotePartNumbers).ToList();
        //                if (!dictCompanyAndPartNumbers.ContainsKey(c))
        //                    dictCompanyAndPartNumbers.Add(c, combinedParts);
        //            }

        //        }
        //    }


        //    //Build the DataTable
        //    DTResults = new DataTable();
        //    DTResults.Columns.Add("Company", typeof(string));
        //    DTResults.Columns.Add("Part", typeof(string));
        //    DTResults.Columns.Add("EOLYears", typeof(string));
        //    DTResults.Columns.Add("Lifecycle", typeof(string));
        //    DTResults.Columns.Add("Risk", typeof(string));
        //    DTResults.Columns.Add("Obsolete?", typeof(string));
        //    DTResults.Columns.Add("NRND?", typeof(string));


        //    //Loop through the Dictionary
        //    foreach (KeyValuePair<company, List<string>> kvp in dictCompanyAndPartNumbers)
        //    {
        //        company comp = kvp.Key;
        //        //Do a part search for the ComIDs
        //        DataSet dsPartSearch = await SiliconExpertLogic.GetPartSearchDataSetJSON(kvp.Value, SiliconExpertLogic.SearchType.List, true);
        //        if (dsPartSearch.Tables["PartDto"] == null)
        //            return;

        //        //Get The ComId list for the returned parts
        //        List<string> listComIds = dsPartSearch.Tables["PartDto"].AsEnumerable().Select(s => s.Field<string>("ComID").Trim()).ToList();
        //        if (listComIds == null)
        //            return;



        //        DataSet SiDataSet = await SiliconExpertLogic.GetPartSearchDataSetJSON(listComIds, SiliconExpertLogic.SearchType.Detail);
        //        if (SiDataSet == null)
        //            return;

        //        List<SiliconExpertLogic.PartRiskSummaryObject> listPrso = SiliconExpertLogic.GeneratePartRiskSummary(SiDataSet);
        //        foreach(SiliconExpertLogic.PartRiskSummaryObject prs in listPrso)
        //        {
        //            DataRow dr = DTResults.NewRow();
        //            dr[0] = comp.companyname ?? "Unknown";
        //            dr[1] = prs.partNumber ?? "Unknown";
        //            dr[2] = prs.estimatedYearsToEOL ?? "Unknown";
        //            dr[3] = prs.partLifecycleStage ?? "Unknown";
        //            dr[4] = prs.lifeCycleRiskGrade ?? "Unknown";
        //            dr[5] = prs.hasObsolescenceNotice ?? "Unknown";
        //            dr[6] = prs.hasNRNDNotice ?? "Unknown";
        //            DTResults.Rows.Add(dr);
        //        }              

        //    }
        //    totalCompanies = dictCompanyAndPartNumbers.Count();
        //    searchResults += "Companies: " + totalCompanies + "<br />";

        //}


        //var query = DTResults.AsEnumerable().Select(s => new
        //{
        //    Company = s.Field<string>("Company"),
        //    Part = s.Field<string>("Part"),
        //    EOLYears = s.Field<string>("EOLYears"),
        //    Lifecycle = s.Field<string>("Lifecycle"),
        //    Risk = s.Field<string>("Risk"),
        //    Obsolete = s.Field<string>("Obsolete?"),
        //    NRND = s.Field<string>("NRND?"),           
        //});
        //smdt.dataSource = query;
        //smdt.loadGrid();

        //stopWatch.Stop();
        //// Get the elapsed time as a TimeSpan value.
        //TimeSpan ts = stopWatch.Elapsed;
        //string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds,ts.Milliseconds / 10);
        //searchResults += "Search Time: " + elapsedTime + "<br />";
        //lblSearchResults.Text = searchResults;


    }

    protected async void btnInventorySummary_Click(object sender, EventArgs e)
    {
        //Stopwatch to track search time.
        Stopwatch stopWatch = new Stopwatch();
        int count = 0;
        try
        {
            string stockType = SM_Enums.StockType.stock.ToString();
            int maxRecords = 0;
            stopWatch.Start();
            //Get partrecords Tuple
            //List<Tuple<string, string>> tupLinePnMfg = GetPartRecordTuple(stockType, maxRecords);
            List<partrecord> pList = new List<partrecord>();
            using (RzDataContext rdc = new RzDataContext())
                pList = rdc.partrecords.Where(w => w.stocktype == stockType).ToList();

            //Get Silicon Expert Summary Results           
            List<DataTable> dtList = await GetSiliconExpertInventorySummaryResults(pList, maxRecords);

            if (dtList == null)
                throw new Exception("Summary DataTable List Null.");

            foreach (DataTable dt in dtList)
            {
                if (dt == null || dt.Rows.Count <= 0)
                    throw new Exception("DataTable empty.");

                //Sve Results to DataBase
                SaveSiliconExpertResultsToDataBase(dt);
                count += dt.Rows.Count;
            }





            //Load Grid from Rz Database
            LoadGridFromRzDataBase(dtList);

            stopWatch.Stop();
            lblElapsedTime.Text = stopWatch.Elapsed.TotalSeconds.ToString();

            //tools.HandleResult("success", count + " rows added to DataBase");
            tools.SendMail("se_import@sensiblemicro.com", "ktill@sensiblemicro.com", "Silicon Summary Success", "Saved " + count + " rows to DataBase.");
        }
        catch (Exception ex)
        {
            stopWatch.Stop();
            lblElapsedTime.Text = stopWatch.Elapsed.TotalSeconds.ToString();
            string results = ex.Message;
            tools.SendMail("se_import@sensiblemicro.com", "ktill@sensiblemicro.com", "Silicon Summary Failed", "stopped at count " + count + ".  Message: " + ex.Message);
            //tools.HandleResult("fail", count + " " + results);
            //tools.HandleResult("fail", ex.Message + "<br /> Time: " + stopWatch.Elapsed.TotalSeconds.ToString());
        }





    }



    private void UploadDataBasePartsToHubspot()
    {
        try
        {
            int tableID = 2036840;
            using (RzDataContext rdc = new RzDataContext())
            {
                ////https://app.hubspot.com/hubdb/1878634/table/2036088
                ////HubDB Table Mappings:
                //unique_id = 2
                //date_created = 3
                //date_modified = 6
                //comID = 7
                //part_number = 8
                //mfg = 9
                //PLName = 10
                //description = 11
                //datasheet = 12
                //lifecycle = 13
                //rohs = 14
                //rohsversion = 15
                //matchrating = 16
                //taxonomypath = 17
                //stocktype = 18


                var query = rdc.silicon_expert_list_summaries.ToList();
                foreach (var v in query)
                {
                    string partrecordUid = v.partrecord_uid;
                    partrecord p = rdc.partrecords.Where(w => w.unique_id == partrecordUid).FirstOrDefault();
                    long? qty = p.quantity_available;
                    Dictionary<string, object> HubDbRowData = new Dictionary<string, object>{
                { "2", v.unique_id },
                { "7", v.ComID.ToString() },
                { "8", v.PartNumber },
                { "9", v.Manufacturer},
                { "10", v.PlName},
                { "11", v.Description},
                { "12", v.Datasheet },
                { "13",v.Lifecycle },
                { "14", v.RoHS },
                { "15", v.RoHSVersion },
                { "16", v.MatchRating },
                { "17", v.TaxonomyPath },
                { "18", v.stocktype },
                {"19",  qty}

            };


                    string rowJson = JsonConvert.SerializeObject(HubDbRowData, Formatting.Indented);
                    rowJson = HubspotApis.HubspotApi.HubDBs.BuildHubDBRowString(rowJson);
                    string url = @"https://api.hubapi.com/hubdb/api/v2/tables/" + tableID + "/rows" + HubspotApis.HubspotApi.AppendAuthorization();
                    HubspotApis.HubspotApi.Post(rowJson, url);
                    //if (HubspotApis.HubspotApi.Post(rowJson, url))
                    //    throw new Exception("Hubspot row POST operation failed.");

                }
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private long? PopulateCurrentStockQty(RzDataContext rdc, silicon_expert_list_summary v)
    {

        //Quer table for parts matchint StockType        
        //DataColumn colQty = dt.Columns.Add("stocktype", typeof(string));
        long? qty = 0;
        string rowPartNumber = v.PartNumber;
        qty = rdc.partrecords.Where(w => w.fullpartnumber == rowPartNumber && w.stocktype == v.stocktype).Sum(s => s.quantity_available);
        return qty;
    }

    private void LoadGridFromRzDataBase()
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            var query = rdc.silicon_expert_list_summaries.ToList();





            smdt.dataSource = query;
            smdt.loadGrid();
        }
    }

    private void LoadGridFromRzDataBase(List<DataTable> dtList)
    {
        DataTable dtResults = null;
        foreach (DataTable dt in dtList)
        {
            if (dtResults == null)
                dtResults = dt.Clone();//Clone the structure
            dtResults.Merge(dt);
        }
        smdt.dataSource = dtResults;
        smdt.loadGrid();
    }

    private void SaveSiliconExpertResultsToDataBase(DataTable dt)
    {


        using (RzDataContext rdc = new RzDataContext())
        {
            //Save rows to silicon_expert_list_summary
            foreach (DataRow dr in dt.Rows)
            {
                silicon_expert_list_summary sel = new silicon_expert_list_summary();
                long comID = Convert.ToInt64(dr.Field<string>("ComID"));
                if (!rdc.silicon_expert_list_summaries.Any(a => a.ComID == comID))
                {
                    string unique_id = Guid.NewGuid().ToString();
                    sel.unique_id = unique_id;
                    rdc.silicon_expert_list_summaries.InsertOnSubmit(sel);
                }
                else
                    sel = rdc.silicon_expert_list_summaries.Where(w => w.ComID == comID).FirstOrDefault();


                sel.date_created = DateTime.Now;
                sel.date_modified = DateTime.Now;
                sel.ComID = Convert.ToInt64(dr.Field<string>("ComID"));
                sel.PartNumber = dr.Field<string>("PartNumber");
                sel.Manufacturer = dr.Field<string>("Manufacturer");
                sel.PlName = dr.Field<string>("PlName");
                sel.Description = dr.Field<string>("Description");
                sel.Datasheet = dr.Field<string>("Datasheet");
                sel.Lifecycle = dr.Field<string>("Lifecycle");
                sel.RoHS = dr.Field<string>("RoHS");
                sel.RoHSVersion = dr.Field<string>("RoHSVersion");
                sel.MatchRating = dr.Field<string>("MatchRating");
                sel.TaxonomyPath = dr.Field<string>("TaxonomyPath");
                sel.partrecord_uid = dr.Field<string>("partrecord_uid");
                sel.stocktype = SM_Enums.StockType.stock.ToString();
            }

            rdc.SubmitChanges();
        }
    }

    private List<Tuple<string, string>> GetPartRecordTuple(string stockType, int maxRecords)
    {
        List<Tuple<string, string>> ret = new List<Tuple<string, string>>();
        using (RzDataContext rdc = new RzDataContext())
        {
            ret = rdc.partrecords
                                   .Where(w => w.fullpartnumber.Length > 0
                                   && w.fullpartnumber != null
                                   && !w.fullpartnumber.ToLower().Contains("gcat")
                                   && w.stocktype == stockType)
                                    .Select(t => new Tuple<string, string>(t.fullpartnumber.Trim().ToUpper(), t.manufacturer.Trim().ToUpper()))
                                   .Distinct().Take(maxRecords).ToList();
        }
        return ret;
    }

    private static async Task<List<DataTable>> GetSiliconExpertInventorySummaryResults(List<partrecord> pList, int maxRecords)
    {

        //Stopwatch sw = new Stopwatch();
        //sw.Start();
        List<DataTable> dtList = new List<DataTable>();
        //int partrecordCount = 0;
        //List<partrecord> notFoundPartrecords = new List<partrecord>();
        //if (maxRecords > 0)
        //    pList = pList.Take(maxRecords).ToList();
        //foreach (partrecord p in pList)
        //{
        //    partrecordCount++;
        //    DataTable partDto = new DataTable();
        //    DataSet dsPartSearch = await SiliconExpertLogic.GetPnMfgDataSetSinglePart(p, true);

        //    if (dsPartSearch == null)
        //    {
        //        notFoundPartrecords.Add(p);
        //        continue;
        //    }

        //    //Save the ComID to the Partrecord


        //    partDto = dsPartSearch.Tables["PartDto"];
        //    if (partDto != null)
        //    {
        //        //Add the part and qty to the DataTable
        //        DataColumn partrecord_uid = new DataColumn("partrecord_uid", typeof(string));

        //        partDto.Columns.Add(partrecord_uid);


        //        foreach (DataRow row in partDto.Rows)
        //        {
        //            row["partrecord_uid"] = p.unique_id;
        //        }
        //        dtList.Add(partDto);
        //    }

        //}
        //SM_Tools tools = new SM_Tools();
        //string notFoundList = "";

        ////The not found link will have duplicated pn / mfg, as we have many lines.  For the not founds, only need distinct pn/mfg combos
        //var deDupe = notFoundPartrecords.Select(s => new
        //{

        //    part = s.fullpartnumber.ToUpper().Trim(),
        //    mfg = s.manufacturer.ToUpper().Trim()


        //}).Distinct().ToList();
        //foreach (var v in deDupe.OrderBy(o => o.part))
        //{
        //    notFoundList += v.part + ", " + v.mfg + "<br />";

        //}
        //sw.Stop();
        //string durHours = sw.Elapsed.Hours.ToString();
        //string durMins = sw.Elapsed.Minutes.ToString();
        //string durSeconds = sw.Elapsed.Seconds.ToString();
        //string durTotal = durHours + ":" + durMins + ":" + durSeconds;
        //tools.SendMail("se_summary@sensiblemicro.com", "ktill@sensiblemicro.com", "Not Found Silicon Expert Parts", "The Following Parts from Rz were not found in SiliconExpert: <br /><br /> " + notFoundList + "<br /> Total Duration " + durTotal);
        return dtList;

    }


    protected void btnUploadSESummariesToHubspot_Click(object sender, EventArgs e)
    {
        //Upload DataBase restuls to Hubspot   
        UploadDataBasePartsToHubspot();

    }

    protected void cbEnable_CheckedChanged(object sender, EventArgs e)
    {
        using (RzDataContext rdc = new RzDataContext())
        {
            system_management sm = rdc.system_managements.Where(w => w.system_name == "silicon_expert").FirstOrDefault();
            sm.is_enabled = cbEnable.isChecked;
            rdc.SubmitChanges();


        }
        LoadControls();
    }

    
   
}