﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="integrations_dashboard.aspx.cs" Inherits="secure_admin_integrations_integrations_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Integrations</h1>
        </div>        
    </div>
    <div class="card">
          <div class="sm-list">
        <ul>
            <li>
                <a href="/secure_admin/integrations/silicon_expert_admin.aspx" target="_blank">
                    <label>Silicon Expert</label>
                    <p>Silicon Expert API Implementation</p>
                </a>
            </li>
            <li>
                <a href="/secure_admin/integrations/hubspot/hubspot_api_admin.aspx" target="_blank">
                    <label>Hubspot</label>
                    <p>Hubspot API Implementation</p>
                </a>
            </li>
        </ul>
    </div>
    </div>
  
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

