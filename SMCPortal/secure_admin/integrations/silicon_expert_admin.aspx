﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="silicon_expert_admin.aspx.cs" Async="true" Inherits="secure_admin_integrations_silicon_expert_admin" %>

<%@ Register Src="~/assets/controls/sm_datatable.ascx" TagPrefix="uc1" TagName="sm_datatable" %>
<%@ Register Src="~/assets/controls/sm_checkbox.ascx" TagPrefix="uc1" TagName="sm_checkbox" %>





<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>SILICON EXPERT ADMIN</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="panel">
            <div class="panel panel-heading">
                <div class="section-header-label">
                    <h3>CURRENT USAGE DATA</h3>
                </div>
                <div class="well well-sm">
                    <div class="form-check">
                        <%-- <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked />
                    <label class="form-check-label" for="flexCheckChecked">Checked checkbox</label>--%>
                        <uc1:sm_checkbox runat="server" ID="cbEnable" OnCheckedChanged="cbEnable_CheckedChanged" CssClass="form-check-input updateProgress" OnClientClick="ShowUpdateProgress()" />
                        <%--<asp:Label ID="lblAPIStatus" runat="server" CssClass="form-check-label" for="cbEnable"></asp:Label>--%>
                    </div>


                   <%-- <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>--%>

                </div>
                <asp:GridView ID="gvUserStatus" runat="server" CssClass="table table-hover table-striped gvstyling" GridLines="None" AutoGenerateColumns="true" ShowHeader="True" />
            </div>
            <div class="panel panel-body">
                <div class="section-header-label">
                    <h3>Results</h3>
                </div>
                <%--Result Grid--%>
                <div class="row" style="text-align: center;">
                    <div class="col-sm-12">
                        <uc1:sm_datatable runat="server" ID="smdt" />
                    </div>
                </div>

                <div class="row" style="text-align: center;">
                    <div class="col-sm-4">
                        <asp:Button ID="btnTestAuth" runat="server" Text="Test Authentication" OnClick="btnTestAuth_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();" />
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnTestPartSearch" runat="server" Text="Test Search" OnClick="btnTestPartSearch_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();" />
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnGetUserStatus" runat="server" Text="Refresh Usage Data" OnClick="btnGetUserStatus_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <asp:Button ID="btnTop200CustomersSiliconExpertData" runat="server" Text="Top 200" OnClick="btnTop200CustomersSiliconExpertData_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();" />
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnInventorySummary" runat="server" Text="Inventory Summary" OnClick="btnInventorySummary_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();" />
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnUploadSESummariesToHubspot" runat="server" Text="Upload Summaries to HS" OnClick="btnUploadSESummariesToHubspot_Click" CssClass="btn-smc btn-smc-primary btn-block" OnClientClick="ShowUpdateProgress();" />
                    </div>
                </div>

            </div>
            <div class="panel panel-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <em class="h6">Search Time: 
                            <asp:Label ID="lblElapsedTime" runat="server"></asp:Label></em>
                    </div>
                </div>


            </div>

        </div>
    </div>


</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

