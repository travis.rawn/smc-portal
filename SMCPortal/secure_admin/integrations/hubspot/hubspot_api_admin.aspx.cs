﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;
using System.Data;
using HubspotApis;
using System.Web.Security;
using System.Web;
using System.Threading.Tasks;

public partial class hubspot_api_admin : System.Web.UI.Page
{
    //HubspotApi hsa = new HubspotApi();
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();
    RzDataContext rdc = new RzDataContext();
    string PostUrl = @"https://api.hubapi.com/deals/v1/deal?hapikey=56650273-6f78-4b25-9280-a47f25c423c8";
    //HubspotApi hsa = new HubspotApi();
    List<HubspotApi.Engagement> eList = null;
    List<hubspot_engagement> rzEngagementList; //Data from Rz database    
    List<hubspot_engagement> engagementData; //Dataset to use for filling grid / ui
    List<HubspotApi.Owner> OwnerList = null;
    List<n_user> includedUsers = new List<n_user>();
    long DealID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        List<string> teamList = new List<string>() { "sales", "sales_assistant" };
        rzap.LoadPicker(teamList);
        LoadDates();


        

    }

    private void LoadDates()
    {
        if (!Page.IsPostBack)
        {
            dtpStart.SelectedDate = DateTime.Today;
            dtpEnd.SelectedDate = DateTime.Today;
        }
    }

    private void LoadDealPipelineData()
    {
        lblDealStages.Text = "";
        lblPipelines.Text = "";


        //List<string> stageList = HubspotApi.DealStage.CurrentDealStages.Select(s => s.Key + " Id: " + s.Value).ToList();
        //foreach (string s in stageList)
        //{
        //    lblDealStages.Text += s;
        //    if (s != stageList.Last())
        //        lblDealStages.Text += "<br />";

        //}

        //List<string> pipelineList = HubspotApi.Pipeline.CurrentPipelines.Select(s => s.Key + " Id: " + s.Value).ToList();
        //foreach (string s in pipelineList)
        //{
        //    lblPipelines.Text += s;
        //    if (s != pipelineList.Last())
        //        lblPipelines.Text += "<br />";

        //}
    }


    protected void lbGetCompanyByDomain_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    HubspotApi hsa = new HubspotApi();
        //    txtResults.Text = "";
        //    if (string.IsNullOrEmpty(txtSearch.Text))
        //        tools.HandleResult("fail", "Please type in an email address or domain");
        //    //NEed a list if I want to bind to a grid
        //    List<HubspotApi.Company> cList = HubspotApi.Companies.GetCompaniesByDomain(txtSearch.Text);
        //    if (cList.Count > 0)
        //    {
        //        //Root Grid
        //        gv.DataSource = cList;
        //        gv.DataBind();
        //        gv.Visible = true;
        //        //Properties Grid
        //        var data = cList[0].Properties.Select(x => new { x.Key, x.Value});
        //        gv.DataSource = data;
        //        gv.DataBind();
        //        gv.Visible = true;
        //        pnlResults.Visible = true;
        //    }

        //}

        //catch (Exception ex)
        //{
        //    tools.HandleResult("fail", ex.Message);
        //}

    }
    protected void lbGetOwnerByEmail_Click(object sender, EventArgs e)
    {
        try
        {
            HubspotApi.Owner owner = null; // Has to be a list currently
            string email = txtSearch.Text;
            if (!string.IsNullOrEmpty(email))
                owner = HubspotApi.Owners.GetOwnerByEmail(email);
            if (owner == null)
                throw new Exception(txtSearch.Text + " :  No Hubspot Owner found with this email address.");
            //var query = owner.email.ToList();
            List<HubspotApi.Owner> oList = new List<HubspotApi.Owner>();
            oList.Add(owner);
            dtvDetailsView.DataSource = oList.Select(s => s.firstName).ToList();
            dtvDetailsView.DataBind();
           
           
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }
    protected void lbGetContactByEmail_Click(object sender, EventArgs e)
    {
        try
        {
            HubspotApi.Contact c = HubspotApi.Contacts.GetContactByEmail(txtSearch.Text.Trim());
            if (c != null)
            {
                //Root Grid
                List<HubspotApi.Contact> cList = new List<HubspotApi.Contact>() { c };
                smdt.dataSource = cList.Select(s => s.Properties["email"]).ToList();
                smdt.loadGrid();
                //gv.DataSource = cList;
                //gv.DataBind();
                //gv.Visible = true;
                ////Properties Grid
                //var data = c.Properties.Select(x => new { x.Key, x.Value });
                //gv.DataSource = data;
                //gv.DataBind();
                //gv.Visible = true;

            }
        }
        catch (Exception ex)
        {
            string message = ex.Message;
            if (ex.InnerException != null)
                if (ex.InnerException.Message.Contains("404"))
                    message = "No contact found for " + txtSearch.Text.Trim();
            tools.HandleResult("fail", message);
        }
    }
    protected void lbGetDealsForContact_Click(object sender, EventArgs e)
    {
        GetDealsForContact();

    }

    private async Task GetDealsForContact()
    {

        string contactID = "1009151"; //Kevin Till webmaster@sensiblemicro.com

        if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
        {
            HubspotApi.Contact contact = HubspotApi.Contacts.GetContactByEmail(txtSearch.Text.Trim());
            contactID = contact.vid.ToString();
        }


        List<HubspotApi.Deal> dList = HubspotApi.Deals.GetAssociatedDeals("contact", contactID);

        var query = dList.Select(s => new
        {
            PortalID = s.portalId,
            DealID = s.dealId,
            DealName = s.properties.Where(w => w.Key == "dealname").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            DealStage = s.properties.Where(w => w.Key == "dealstage").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            PipelineID = s.properties.Where(w => w.Key == "pipeline").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            Amount = s.properties.Where(w => w.Key == "amount").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            Date = s.properties.Where(w => w.Key == "craetedate").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            Part = s.properties.Where(w => w.Key == "part_number").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            MFG = s.properties.Where(w => w.Key == "manufacturer").Select(ss => ss.Value.ToString()).FirstOrDefault(),
            CompanyIDs = string.Join(", ", s.associations.associatedCompanyIds),
            ContactIDs = string.Join(", ", s.associations.associatedVids),
            TestAssDealIDs = string.Join(", ", s.associations.associatedDealIds)

        }).ToList();




        foreach (var v in query)
        {
            HubspotApi.Pipeline p = HubspotApi.Deals.GetPipelineData(v.PipelineID);
            HubspotApi.DealStage s = HubspotApi.Deals.GetDealStage(v.PipelineID, v.DealStage);
        }

        var query2 = query.Select(s => new
        {
            PortalID = s.PortalID,
            DealID = s.DealID,
            DealName = s.DealName,
            DealStage = HubspotApi.Deals.GetDealStage(s.PipelineID, s.DealStage).label,
            PipelineName = HubspotApi.Deals.GetPipelineData(s.PipelineID).label,
            Amount = s.Amount,
            Date = HubspotApi.Api.UnixTimeStampToDateTime(Convert.ToInt64(s.Date)),
            Part = s.Part,
            MFG = s.MFG,
            CompanyIDs = s.CompanyIDs,
            ContactIDs = s.ContactIDs
        }).ToList();


        //gv.DataSource = query2;
        //gv.DataBind();
        //gv.Visible = true;

        smdt.dataSource = query2;
       
    }

    protected void lbGetAllDeals_Click(object sender, EventArgs e)
    {

        try
        {
            List<HubspotApi.Deal> dList = HubspotApi.Deals.GetAllDeals();


            var query = dList.Select(s => new
            {
                PortalID = s.portalId,
                DealID = s.dealId,
                DealName = s.properties.Where(w => w.Key == "dealname").Select(ss => ss.Value.ToString()).FirstOrDefault(),
                DealStage = s.properties.Where(w => w.Key == "dealstage").Select(ss => ss.Value.ToString()).FirstOrDefault(),
                Amount = s.properties.Where(w => w.Key == "amount").Select(ss => ss.Value.ToString()).FirstOrDefault(),
                Date = s.properties.Where(w => w.Key == "craetedate").Select(ss => ss.Value.ToString()).FirstOrDefault(),
                Part = s.properties.Where(w => w.Key == "part_number").Select(ss => ss.Value.ToString()).FirstOrDefault(),
                MFG = s.properties.Where(w => w.Key == "manufacturer").Select(ss => ss.Value.ToString()).FirstOrDefault(),


                CompanyIDs = string.Join(", ", s.associations.associatedCompanyIds),
                ContactIDs = string.Join(", ", s.associations.associatedVids),
                TestAssDealIDs = string.Join(", ", s.associations.associatedDealIds)

            }).ToList();


            //gv.DataSource = query;
            //gv.DataBind();
            //gv.Visible = true;

            smdt.dataSource = query;
            smdt.loadGrid();

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }


    }

    protected void btnCreateDeal_Click(object sender, EventArgs e)
    {
        CreateDeal();
    }

    private void CreateTestDeal()
    {
        try
        {
            string url = @"https://api.hubapi.com/deals/v1/deal" + HubspotApi.AppendAuthorization();
            HubspotApi.Deals.CreateDealObjectTest();
            txtResults.Text = HubspotApi.responseString;

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private void CreateDeal()
    {
        try
        {
            string dealStageId = HubspotApi.DealStage.CurrentDealStages.Where(w => w.Key == "RFQ Recieved").Select(s => s.Value).FirstOrDefault();
            Dictionary<string, string> properties = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(txtBatchName.Text))
                properties.Add("dealname", txtBatchName.Text);
            if (!string.IsNullOrEmpty(txtAmount.Text))
                properties.Add("amount", txtAmount.Text);
            if (!string.IsNullOrEmpty(txtPart.Text))
                properties.Add("part_number", txtPart.Text);
            properties.Add("pipeline", "default");
            properties.Add("dealstage", dealStageId);

            if (properties.Count > 0)
            {
                HubspotApi.Deal deal = HubspotApi.Deals.CreateDeal(null, properties);
                txtResults.Text += Environment.NewLine + HubspotApi.responseString;
            }
            else
            {
                throw new Exception("No property values set for this deal. Cannot create.");
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    protected void btnCreateTestDeal_Click(object sender, EventArgs e)
    {
        CreateTestDeal();

    }

    protected void btnUpdatePipeline_Click(object sender, EventArgs e)
    {
        try
        {
            HubspotApi.Deal d = UpdateDealPipeline();

            if (d != null)
            {
                GetDealsForContact();
            }
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    private HubspotApi.Deal UpdateDealPipeline()
    {
        int dealID = Convert.ToInt32(txtCurrentDealID.Text.Trim());
        string pipelineId = txtNewPipeline.Text.Trim();
        string dealStageId = txtNewStage.Text.Trim();
        if (dealID == 0 || string.IsNullOrEmpty(pipelineId) || string.IsNullOrEmpty(dealStageId))
            throw new Exception("Must have a dealID, pipeline, and stage.  Cannot Update.");
        Dictionary<string, string> props = new Dictionary<string, string>()
        {
            { "pipeline",pipelineId},
            { "dealstage",dealStageId}
        };

        return HubspotApi.Deals.UpdateDeal(dealID, props);
    }

    protected void btnDeleteDeal_Click(object sender, EventArgs e)
    {
        try
        {
            DeleteDeal();
            tools.HandleResult( "success", "Successfully deleted deal Id: " + txtSearch.Text);
        }
        catch (Exception ex)
        {
            tools.HandleResult( "fail", ex.Message);
        }
    }

    private async Task DeleteDeal()
    {
        int dealID = 0;
        dealID = Convert.ToInt32(txtSearch.Text);
        if (dealID == 0)
            throw new Exception("Please enter a valid Deal ID into the aearch box");
        if (!HubspotApi.Deals.DeleteDeal(dealID))
            throw new Exception("Deal not deleted.  - " + HubspotApi.responseString);
    }

    protected void lbGetPipelineData_Click(object sender, EventArgs e)
    {
        List<HubspotApi.Pipeline> pList = HubspotApi.Deals.GetAllPipelines();
        //gv.DataSource = pList.Select(s => s.label + " : " + s.pipelineId).ToList();
        //gv.DataBind();
        //gv.Visible = true;
        smdt.dataSource = pList.Select(s => s.label + " : " + s.pipelineId).ToList();
        smdt.loadGrid();

        List<HubspotApi.DealStage> sList = new List<HubspotApi.DealStage>();
        foreach (HubspotApi.Pipeline p in pList)
        {
            sList.AddRange(p.stages);
        }
    }



    protected void lbGetEngagements_Click(object sender, EventArgs e)
    {

        LoadRecentEngagements();

    }

    private void LoadRecentEngagements()
    {
        try
        {

            LoadOwnerList();
            txtResults.Text = "";

            //Refresh latest data
            int addedEngagements = 0;
            tools.HandleResult("success", addedEngagements + " new engagements retreived.");
            DateTime start = dtpStart.SelectedDate.Value.Date;
            DateTime end = dtpEnd.SelectedDate.Value.Date;
            //Get The Agents
            List<n_user> agentList = rzap.GetSelectedUsers();
            //Helper List
            List<string> agentListIds = agentList.Select(s => s.unique_id).ToList();
            //Get the Engagements
            if (rzEngagementList == null || rzEngagementList.Count == 0)
                rzEngagementList = rdc.hubspot_engagements.Where(w => w.hs_date_created.Value.Date >= start && w.hs_date_created.Value.Date <= end && agentListIds.Contains(w.base_mc_user_uid)).ToList();




            engagementData = rzEngagementList;

            var query = engagementData.Select(s => new
            {
                Type = s.type,
                Date = s.hs_date_created,
                Owner = s.ownerName,
                //Owner = OwnerList.Where(w => w.ownerId == Convert.ToInt32(s.ownerID)).Select(ss => ss.firstName + " " + ss.lastName).FirstOrDefault() ?? "Not Found",
                HubID = s.hubspotID,
                Detail = s.body
            });




            if (query.Any())
            {
               
                smdt.dataSource = query;
                smdt.loadGrid();
            }

        }

        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }



    private void LoadOwnerList()
    {
        OwnerList = HubspotApi.Owners.GetAllOwners();
    }

    protected void lbGetAllCalls_Click(object sender, EventArgs e)
    {
        //Engagements has a max of 250 per call.  If HasMore == true, call again with the offset set to exclude the records we already got.
        //Keep adding to the list, unitl hasmore == false.

        //Even better, for my purposes, the "recent engagements" api seems more appropriate:
        //https://api.hubapi.com/engagements/v1/engagements/recent/modified?hapikey=demo&count=2&since=1483246800000

        //I want to time this.
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        List<n_user> includedAgents = rzt.GetUsersForTeams(new List<string>() { "sales" });
    }

    protected void lbCreateHubspotCall_Click(object sender, EventArgs e)
    {
        throw new Exception("Featurue disabled");
        //try
        //{
        //    //mristoff owner id = 7209703;
        //    //michaelr Rz uid = 192e06d0d3994bde9348f0b6fea738d8
        //    //KT Customer Test = c1ef4ae7785348988f83715df3289c90
        //    //Ender Wiggin = 98a82f38b7a6418db72fb8beaa1d6b47
        //    phonecall pc = new phonecall();
        //    pc.duration = 10000;



        //    company c = rdc.companies.Where(w => w.unique_id == "c1ef4ae7785348988f83715df3289c90").FirstOrDefault();//KT Customer Test
        //    companycontact cc = rdc.companycontacts.Where(w => w.unique_id == "98a82f38b7a6418db72fb8beaa1d6b47").FirstOrDefault(); // Ender Wiggin
        //    n_user u = rdc.n_users.Where(w => w.unique_id == "192e06d0d3994bde9348f0b6fea738d8").FirstOrDefault();//Michael Ristoff - webmaster@sensiblemicro.com
        //    pc.phonenumber = "212-555-1212";
        //    pc.unique_id = Guid.NewGuid().ToString();
        //    pc.duration = 100000;
        //    pc.base_mc_user_uid = "192e06d0d3994bde9348f0b6fea738d8";

        //    HubspotApi.HubPhoneCall hp = new HubspotApi.HubPhoneCall();
        //    hp.companyWebAddress = c.primarywebaddress;
        //    hp.contactEmailAddress = cc.primaryemailaddress;
        //    hp.durationMilliseconds = (int)pc.duration;
        //    hp.externalAccountId = u.unique_id; //rzAgent uid
        //    hp.externalId = pc.unique_id; //phonecall uid
        //    hp.ownerEmailAddress = u.email_address;
        //    hp.status = "COMPLETED";
        //    hp.toNumber = pc.phonenumber;
        //    hp.body = "Logged from Rz";
        //    //HubspotApi.Engagement theCall = hsa.CreateHubspotCall(pc, u, c, cc);
        //    HubspotApi.Engagement theCall = HubspotApi.Engagements.CreateHubspotCall(hp);
        //    tools.HandleResult("success", "Successfully created call for " + u.email_address);
        //}
        //catch (Exception ex)
        //{
        //    tools.HandleResult("fail", ex.Message);

        //}

    }

    

    //private void DeleteSelectedHubIds()
    //{
    //    try
    //    {
    //        List<long> hubIdsToDelete = new List<long>();
    //        foreach (GridViewRow row in gv.Rows)
    //        {
    //            if (row.RowType == DataControlRowType.DataRow)
    //            {
    //                CheckBox cb = (CheckBox)row.FindControl("cbxSelect");
    //                string engagementType = row.Cells[1].Text.ToLower();
    //                int hubID = Convert.ToInt32(row.Cells[4].Text);
    //                if (cb != null)
    //                {
    //                    bool delete = cb.Checked;
    //                    if (delete)
    //                    {
    //                        hubIdsToDelete.Add(hubID);
    //                        hubspot_engagement h = rdc.hubspot_engagements.Where(w => w.hubspotID == hubID).FirstOrDefault();
    //                        if (h != null)
    //                            rdc.hubspot_engagements.DeleteOnSubmit(h);
    //                    }


    //                }
    //            }
    //        }
    //        rdc.SubmitChanges();
    //        HubspotApi.Engagements.DeleteHubspotEngagements(hubIdsToDelete);
    //        LoadRecentEngagements();
    //        tools.HandleResult("success", "Successfully deleted " + hubIdsToDelete.Count + " records.");
    //    }
    //    catch (Exception ex)
    //    {
    //        tools.HandleResult("fail", ex.Message);
    //    }

    //}

    //protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
    //{

    //}

    protected void lbCheckDailyUsage_Click(object sender, EventArgs e)
    {
        try
        {

            HubspotApi.CurrentUsage cu = HubspotApi.Api.GetCurrentDailyUsage();
            string message = cu.currentUsage + " hubspot API calls made today.";
            tools.HandleResult("success", message);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    protected void btnUpdateCallDisposition_Click(object sender, EventArgs e)
    {
        try
        {
            DealID = CheckHubID();
            int updatedNullDispo = 0;
            using (RzDataContext rdc = new RzDataContext())
                HubspotLogic.UpdateCompletedNullDispositions(rdc, out updatedNullDispo, DealID);
            tools.HandleResult("success", updatedNullDispo + " dispositions updated.");
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void btnFixMissingRecordings_Click(object sender, EventArgs e)
    {
        try
        {
            DealID = CheckHubID();
            int fixedRecordings = 0;
            using (RzDataContext rsc = new RzDataContext())
                HubspotLogic.FixMissingRecordings(rdc, out fixedRecordings, DealID);
            tools.HandleResult("success", fixedRecordings + " recordings fixed.");
        }
        catch (Exception ex)
        {
            tools.HandleResult( "fail", ex.Message);
        }
    }

    private long CheckHubID()
    {
        string hubID = txtDealIDOperation.Text.Trim().ToLower();
        if (!Tools.Number.IsNumeric(hubID))
            return 0;
        long lngHubId = Convert.ToInt64(hubID);
        return lngHubId;
    }

    protected void btnAgeRecords_Click(object sender, EventArgs e)
    {
        HandleAgedDeals();
    }

    private void HandleAgedDeals()
    {

        try
        {
            int count = 0;
            string htmlResults = "";
            int olderThanDays = Convert.ToInt32(txtOlderThanDays.Text);
            using (RzDataContext rdc = new RzDataContext())
                SalesLogic.HandleAgedDealsRz(rdc, 30, null);
            string message = "Successfully updated " + count + " Deal records.";
            tools.HandleResult("success", message);
            tools.SendMail("aging_deals@senisblemicro.com", "ktill@sensiblemicro.com", "Aged Deal Results", message);

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
            tools.SendMail("aging_deals@senisblemicro.com", "ktill@sensiblemicro.com", "Error with Aging Quotes", "Error:  " + ex.Message + "<br /> Inner Exception: " + ex.InnerException);
        }



    }

    protected void btnTestAgedEmail_Click(object sender, EventArgs e)
    {

        List<dealheader> dList = GenerateSamepleDealheaders();
        List<ordhed_quote> qList = GenerateSamepleQuotes();
        string message = SalesLogic.GenerateAgedDealHtmlResults(dList, qList);
        tools.SendMail("aging_deals@senisblemicro.com", "ktill@sensiblemicro.com", "Aged Deal Results", message);
    }

    private List<ordhed_quote> GenerateSamepleQuotes()
    {

        //Company
        //Agent
        //Batch
        //Date
        List<ordhed_quote> ret = new List<ordhed_quote>();
        DateTime thirtyDaysAgo = DateTime.Now.AddDays(-31);
        ordhed_quote quote1 = new ordhed_quote();
        quote1.companyname = "Sam Wellingtons Built Systems";
        quote1.agentname = "Victoria Perenich";
        quote1.ordernumber = "0285947";
        quote1.date_created = thirtyDaysAgo;
        ret.Add(quote1);

        ordhed_quote quote2 = new ordhed_quote();
        quote2.companyname = "Jumbones Monster Parts";
        quote2.agentname = "Kevin Till";
        quote2.ordernumber = "35429876576 Farts)";
        quote2.date_created = thirtyDaysAgo;
        ret.Add(quote2);

        ordhed_quote quote3 = new ordhed_quote();
        quote3.companyname = "Can't Find My Shoes Inc";
        quote3.agentname = "Gus Gustofferson";
        quote3.ordernumber = "23634-*6789<>";
        quote3.date_created = thirtyDaysAgo;
        ret.Add(quote3);

        ordhed_quote quote4 = new ordhed_quote();
        quote4.companyname = "Auntie Tweetie's Sophisticated Electronic Stuff";
        quote4.agentname = "Adam Langenbacher";
        quote4.ordernumber = "5467348";
        quote4.date_created = thirtyDaysAgo;
        ret.Add(quote4);

        ordhed_quote quote5 = new ordhed_quote();
        quote5.companyname = "Kentucky Fried Chips";
        quote5.agentname = "Gabe Newell";
        quote5.ordernumber = "67595";
        quote5.date_created = thirtyDaysAgo;
        ret.Add(quote5);


        return ret;
    }

    private List<dealheader> GenerateSamepleDealheaders()
    {
        //Company
        //Agent
        //Batch
        //Date


        List<dealheader> ret = new List<dealheader>();
        DateTime thirtyDaysAgo = DateTime.Now.AddDays(-31);

        dealheader deal1 = new dealheader();
        deal1.customer_name = "Some Fancy Cusomer";
        deal1.agentname = "Sean Maronpot";
        deal1.dealheader_name = "Deal 1 Name (HUBID: 187964)";
        deal1.date_created = thirtyDaysAgo;
        ret.Add(deal1);

        dealheader deal2 = new dealheader();
        deal2.customer_name = "Jimmy Jam's Slimmy Sham";
        deal2.agentname = "Bill Meyers";
        deal2.dealheader_name = "Deal 2 Neat!";
        deal2.date_created = thirtyDaysAgo;
        ret.Add(deal2);

        dealheader deal3 = new dealheader();
        deal3.customer_name = "Uncle Remus Ghostbusting Investigations";
        deal3.agentname = "Peter Venkmann";
        deal3.dealheader_name = "Deal 3, Who you goanna call????";
        deal3.date_created = thirtyDaysAgo;
        ret.Add(deal3);

        dealheader deal4 = new dealheader();
        deal4.customer_name = "Mothership BOM BUY";
        deal4.agentname = "John SUllivan";
        deal4.dealheader_name = "Deal 4 Name BOM BOM BOM BOM";
        deal4.date_created = thirtyDaysAgo;
        ret.Add(deal4);




        return ret;
    }

    protected void lbRefreshEngagements_Click(object sender, EventArgs e)
    {

        int count = 0;
        using (RzDataContext rdc = new RzDataContext())
        {
            rdc.CommandTimeout = 120;
            //HubspotLogic.RefreshHubspotEngagements(rdc, out count);
            tools.HandleResult("success", "Refreshed Engagements.  (" + count + " items)");
        }

    }

    //HubDB

    protected void lbGetHubDbObject_Click(object sender, EventArgs e)
    {
        //HubspotApi.HubDBs.GetHubDbObject();
        throw new Exception("Featurue disabled");
    }

    protected void lbGetAllHubDBTables_Click(object sender, EventArgs e)
    {
        throw new Exception("Featurue disabled");
        //List<HubspotApi.HubDBTable> tables = HubspotApi.HubDBs.GetAllHubDBTables();

        //var query = tables.Select(s => new
        //{

        //    Name = s.Name,
        //    ID = s.Id,
        //    Label = s.Label,
        //    Columns = s.ColumnCount


        //}).ToList();

        //smdt_hubDbTables.dataSource = query;
        //smdt_hubDbTables.loadGrid();


    }

    protected void lbGetHubDBTableByName_Click(object sender, EventArgs e)
    {
        throw new Exception("Featurue disabled");
        //string name = txtTableName.Text.Trim().ToLower();
        //List<HubspotApi.HubDBTable> tables = HubspotApi.HubDBs.GetAllHubDBTables();

        //var query = tables.Where(w => w.Name.Trim().ToLower() == name || w.Id == Convert.ToInt32(name)).Select(s => new
        //{
        //    Name = s.Name,
        //    ID = s.Id,
        //    Label = s.Label,
        //    Columns = s.ColumnCount
        //});
        //if (query != null)
        //{
        //    smdt_hubDbTables.dataSource = query;
        //    smdt_hubDbTables.loadGrid();
        //}


    }

    protected void lbGetAllAdwordsIDs_Click(object sender, EventArgs e)
    {
        throw new Exception("Featurue disabled");
        ////1041110 is the adwords table          
        ////Column "2" is the adwordsID
        //List<HubspotLogic.HubDbPart> ListAdWordsParts = HubspotLogic.GetHubDbAdwordsParts();


        //List<long> adwordsIDs = ListAdWordsParts.Select(s => Convert.ToInt64(s.ComID)).ToList();


        //if (adwordsIDs != null)
        //{

        //    smdt_hubDbTables.dataSource = adwordsIDs.Select(s => new { SiliconExpertCOMID = s });
        //    smdt_hubDbTables.loadGrid();
        //}
        ////HubspotApi.HubDBTable adWordsTable = hubDB.Tables.Where(s => s.Id == 1041110).FirstOrDefault();

    }
    //End HubDB



    protected void lbShowRecentAPIEngagents_Click(object sender, EventArgs e)
    {
        throw new Exception("Featurue disabled");
        //pnlResults.Visible = true;
        ////List<string> testList = new List<string>() { "farts", "poops", "toots" };
        ////smdt.dataSource = testList.Select(s => new {
        ////    Name = s
        ////});
        ////smdt.loadGrid();


        //string userName = rzap.SelectedUserName;
        //string userId = rzap.SelectedUserID;
        //string type = "CALL";
        //int offset = 0;
        //int limit = 20;
        //DateTime since = new DateTime(2019, 07, 22, 03, 34, 00);
        //long searchDate = HubspotApi.ConvertDateTimeToUnixTimestampMillis(since);

        //List<HubspotApi.Engagement> engList = new List<HubspotApi.Engagement>();
        ////List to hold the API engagements
        //HubspotApi.EngagementRoot root = HubspotApi.Engagements.GetRecentEngagements(searchDate, offset, limit);
        //if (root != null)
        //    if (root.engagements != null)
        //        engList.AddRange(root.engagements);
        ////Set HasMore based on this first call
        //offset = root.offset;
        //bool hasMore = root.hasMore;
        //while (hasMore)
        //{
        //    if (engList.Count >= limit)
        //        return;
        //    hasMore = root.hasMore;
        //    offset = root.offset;
        //    root = HubspotApi.Engagements.GetRecentEngagements(searchDate, offset);
        //    engList.AddRange(root.engagements);
        //}


        //var query = engList.Select(s => new
        //{
        //    Duration = s.metadata.durationMilliseconds,
        //    ToNumber = s.metadata.toNumber,
        //    s.metadata.status,
        //    s.metadata.recordingUrl,
        //    s.metadata.disposition,
        //    Time = HubspotApi.DateTimeFromUnixTimestampMillis(Convert.ToInt64(s.engagement.createdAt)),
        //    HubId = s.engagement.id


        //});

        //if (query.Any())
        //{
        //    ViewState["query"] = query;
        //    lblGridCount.Text = query.Count() + " results.";
        //    smdt.dataSource = query.OrderByDescending(o => o.Time);
        //    smdt.loadGrid();
        //}
    }

    protected void lbExportEngagements_Click(object sender, EventArgs e)
    {
        try
        {
            List<HubspotApi.Engagement> engList = GetRecentCallEngagements();
            //Get List of Hubspot Owners to associate with each engagement:
            List<HubspotApi.Owner> ownerList = HubspotApi.Owners.GetAllOwners();

            DataTable exportDt = new DataTable();
            exportDt.Columns.Add("Agent", typeof(string));
            exportDt.Columns.Add("Minutes", typeof(long));
            exportDt.Columns.Add("To", typeof(string));
            exportDt.Columns.Add("Status", typeof(string));
            exportDt.Columns.Add("Disposition", typeof(string));
            exportDt.Columns.Add("Date", typeof(string));
            exportDt.Columns.Add("HubID", typeof(long));
            exportDt.Columns.Add("Body", typeof(string));
            exportDt.Columns.Add("Recording", typeof(string));

            foreach (HubspotApi.Engagement eng in engList)
            {
                //Agent
                long agentID = eng.engagement.ownerId;
                HubspotApi.Owner o = ownerList.Where(w => w.ownerId == agentID).FirstOrDefault();
                string ownerName = "Unknown";
                if (o != null)
                {
                    ownerName = o.firstName + " " + o.lastName;
                }
                //Date conversion from UNIX And UTC
                DateTime date = HubspotApi.Api.DateTimeESTFromUnixTimestampMillis(Convert.ToInt64(eng.engagement.createdAt));
                date = HubspotApi.ConvertUTCtoEST(date);

                long dur = eng.metadata.durationMilliseconds / 6000;
                string to = eng.metadata.toNumber;
                string status = eng.metadata.status;
                string dispo = HubspotApi.HubPhoneCall.GetDispositionName(eng.metadata.disposition);
                string strDate = date.ToShortDateString() + " " + date.ToShortTimeString();
                long hubID = eng.engagement.id;
                string body = eng.metadata.body;
                string rec = eng.metadata.recordingUrl;

                exportDt.Rows.Add(ownerName, dur, to, status, dispo, strDate, hubID, body, rec);
            }

            tools.ExportDataTableToCsv(exportDt, "engagements.csv");
            tools.HandleResult("success", "Engagements are being downloaded", 6000);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message, 6000);
        }

    }

    private List<HubspotApi.Engagement> GetRecentCallEngagements(int count = 100)
    {

        List<HubspotApi.Engagement> engList = new List<HubspotApi.Engagement>();
        string type = "CALL";
        int offset = 0;
        DateTime since = dtpStart.SelectedDate.Value.Date;
        long searchDate = HubspotApi.ConvertDateTimeToUnixTimestampMillis(since);
        //List to hold the API engagements
        HubspotApi.EngagementRoot root = HubspotApi.Engagements.GetRecentEngagements(searchDate, offset, count);
        if (root != null)
            if (root.engagements != null)
                engList.AddRange(root.engagements);
        //Set HasMore based on this first call
        offset = root.offset;
        bool hasMore = root.hasMore;
        while (hasMore)
        {
            hasMore = root.hasMore;

            offset = root.offset;
            root = HubspotApi.Engagements.GetRecentEngagements(searchDate, offset);
            engList.AddRange(root.engagements);
        }

        return engList;
    }

    class RecentDealObject
    {
        public long dealID { get; set; }
        public string dealName { get; set; }
        public string isClosed { get; set; }
        public string lastModified { get; set; }
        public string companyName { get; set; }
        public string agentName { get; set; }
        public string amount { get; set; }
    }


    protected void lbGetRecentDeals_Click(object sender, EventArgs e)
    {
        try
        {
            List<HubspotApi.Deal> listRecentlyUpdateDeals = new List<HubspotApi.Deal>();
            int minutesAgo = 6;

            //listRecentlyUpdateDeals = HubspotData.Deals.GetDealsModifiedSince(5);
            listRecentlyUpdateDeals = HubspotApi.Deals.GetDealsModifiedSinceMinutes(minutesAgo);
            DateTime dtSince = DateTime.Now.AddMinutes(-minutesAgo);
            if (listRecentlyUpdateDeals.Count <= 0)
                throw new Exception("No updated deals detected since " + dtSince.ToString("MM/dd/yyyy hh:mm:ss tt"));

            List<RecentDealObject> listRecentDealObject = new List<RecentDealObject>();
            foreach (HubspotApi.Deal d in listRecentlyUpdateDeals)
            {
                RecentDealObject rd = new RecentDealObject();
                rd.dealID = d.dealId;
                rd.isClosed = d.properties.Where(w => w.Key == "hs_is_closed").Select(ss => ss.Value.ToString()).FirstOrDefault();
                //Only show non-closed deals
                if (rd.isClosed == "true")
                    return;
                long lngLastModified = d.properties.Where(w => w.Key == "hs_lastmodifieddate").Select(ss => Convert.ToInt64(ss.Value)).FirstOrDefault();
                DateTime dtEST = HubspotApi.Api.DateTimeESTFromUnixTimestampMillis(lngLastModified);
                rd.lastModified = dtEST.ToString("MM/dd/yyyy hh:mm:ss tt");
                rd.dealName = d.properties.Where(w => w.Key == "dealname").Select(ss => ss.Value.ToString()).FirstOrDefault();
                rd.companyName = d.properties.Where(w => w.Key == "amount").Select(ss => ss.Value.ToString()).FirstOrDefault();
                listRecentDealObject.Add(rd);
            }
            smdt.dataSource = listRecentDealObject;
            smdt.loadGrid();
            txtResults.Visible = true;

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }

    }

    protected void lbCloseRzDealsFromHubspot_Click(object sender, EventArgs e)
    {
        try
        {
            int closedCount = 0;
            string closureReport = HubspotLogic.CloseRzDealsFromHubspot(rdc, out closedCount);

            //tools.SendMail("huspot_deal_closures@sensiblemicro.com", "ktill@sensiblemicro.com", "Hubspot API Deal Closure Report", closureReport);
            tools.HandleResult("success", closureReport);
            tools.SendMail("huspot_deal_closures@sensiblemicro.com", "ktill@sensiblemicro.com", "Hubspot API Deal Closure Report", closureReport);

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "AlertBox", "alert('"+ @Html.Raw(closureReport) +"');", true);
        }
        catch (Exception ex)
        {

        }
    }

    protected void lbGetCombinedRzHubspotCompanyData_Click(object sender, EventArgs e)
    {

        DataTable dt = MarketingLogic.LoadRzHubspotCompanyActivity(dtpStart.SelectedDate.Value);
        var query = dt.AsEnumerable().Select(s => new
        {
            RzCompany = s.Field<string>("RzCompanyName"),
            RzTotalSales = s.Field<string>("RzTotalSales"),// != "Unknown" ? Convert.ToDouble(s.Field<string>("RzTotalSales")).ToString("C") : "Unknown", 
            RzIndustry = s.Field<string>("RzIndustrySegment"),
            AnnualRevenue = s.Field<string>("HubspotAnnualRevenue"),//!= "Unknown" ? Convert.ToDouble(s.Field<string>("HubspotAnnualRevenue")).ToString("C") : "Unknown",
            RzAgent = s.Field<string>("RzAgentName"),
            HubspotOwner = s.Field<string>("HubspotOwnerName"),
            HubspotLastContact = s.Field<string>("HubspotLastContact"),
            HubspotLastContactDate = s.Field<string>("HubspotLastContactDate"),
            RzLastBatchDate = s.Field<string>("RzLastBatchDate"),
            RzLastQuoteDate = s.Field<string>("RzLastQuoteDate"),
            RzLastInvoiceDate = s.Field<string>("RzLastInvoiceDate"),
            Website = s.Field<string>("RzWebsiteUrl")
        });
        smdt.dataSource = query;
        smdt.loadGrid();

    }



 

    protected void lbAssociatePortalAndHubspotUsers_Click(object sender, EventArgs e)
    {
        try
        {
            List<HubspotLogic.HubspotPortalLink> hpList = new List<HubspotLogic.HubspotPortalLink>();
            List<MembershipUser> uList = Membership.GetAllUsers().Cast<MembershipUser>().Where(w => !w.Email.Contains("@sensiblemicro.com")).ToList();
            hpList = HubspotLogic.LinkPortalUsersToHubspotContacts(uList);

            string message = "Finished processing Portal User to Hubspot contact linkage.  <b>Details:<br /></b>";
            if (hpList.Count == 0)
                message += "All contacts match Portal values. Nothing updated on Hubspot.";
            else
                foreach (HubspotLogic.HubspotPortalLink hpl in hpList)
                {
                    message += "Email: " + hpl.portal_email + "Name: " + hpl.hub_contact_name + " Created: " + hpl.portal_register_date.ToShortDateString() + " LastLogin: " + hpl.portal_last_activity_date.ToShortDateString() + "<br />";
                }

            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, message);
            SystemLogic.Email.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, "ktill@sensiblemicro.com", "Portal + HubSpot contact association complete.", message);

            //gv.DataSource = hpList;
            //gv.DataBind();
            smdt.dataSource = hpList;
            smdt.loadGrid();
        }
        catch (Exception ex)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, ex.Message);
        }
    }

    protected void lbTestQuoteFormFill_Click(object sender, EventArgs e)
    {
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Filling Test Hubspot quote form");
        // Form Variables (from the HubSpot Form Edit Screen)
        //31d496c1-4001-437b-89fd-70dc7d2fc7c4 - Quote Form 2019
        //1878634 - Sensible Portal ID
        int intPortalID = 1878634;

        //Form Name: [2019] Request a Quote
        //Form ID:  31d496c1-4001-437b-89fd-70dc7d2fc7c4
        HttpContext x = HttpContext.Current;
        //IT Test Form: c370a2a1-e3a0-4a99-bfe1-f17c12db4b7f
        string itTestFormID = "c370a2a1-e3a0-4a99-bfe1-f17c12db4b7f";
        string portalQuoteFormID = "31d496c1-4001-437b-89fd-70dc7d2fc7c4";
        //string strFormGUID = portalQuoteFormID; //place your form guid here
        string strIpAddress = x.Request.UserHostAddress;

        // Page Variables
        string strPageTitle = Page.Title;
        string strPageURL = x.Request.Url.AbsoluteUri;

        Dictionary<string, string> props = new Dictionary<string, string>();
        //These are the REQUIRED properties on our Quote Form
        props.Add("part_number", "TESTPART123");
        props.Add("target_price", "2");
        props.Add("firstname", "Kevin");
        props.Add("lastname", "Test");
        props.Add("email", "ktill@sensiblemicro.com");
        props.Add("part_requirements", "Notes Text");
        props.Add("quote_quantity", "123");
        props.Add("phone", "212-555-1212");
        props.Add("company", "Test Company");
        props.Add("hs_persona", "buyer_betty");
        
        // Do the post, returns true/false
        string strError = "";

        
        bool blnRet = HubspotApi.Forms.Post_To_HubSpot_FormsAPI(x, intPortalID, portalQuoteFormID, props, strIpAddress, strPageTitle, strPageURL, ref strError, false);
        if(!blnRet)
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Failure Filling Test Hubspot quote form");
        else
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Success Filling Test Hubspot quote form");
    }


    

    protected void lbBatchUpdateDeals_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime startDate = DateTime.Now.AddMinutes(-5);
            //Debug Date, or for one-time operations like back-dating.
            //startDate = DateTime.Today;
            startDate = new DateTime(2022,11,29);

            //Timer for log
            DateTime timerStart = DateTime.Now;
            int updatedDealCount = 0;
            string syncReport = "";
            HubspotLogic.SynchronizeHubspotDeals(rdc, startDate, out updatedDealCount, out syncReport);
            DateTime timerEnd = DateTime.Now;
            TimeSpan ts = Tools.Dates.GetTimeSpan(timerStart, timerEnd);
            string message = "Successfully updated " + updatedDealCount + " deals.  Total Time: "+ts.TotalMinutes.ToString("N2") +":"+ts.TotalSeconds.ToString("N2");
            tools.HandleResult("success",message, 999999);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, message);
            SystemLogic.Email.SendMail("hs_sync@sensiblemicro.com", "systems@sensiblemicro.com", "Hubspot Sync Report: " + DateTime.Now.ToShortDateString(), message + "<br /><br /> Report:<br />" + syncReport);
        }
        catch(Exception ex)
        {
            tools.HandleResult("fail", ex.Message, 999999);
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Failed to Sync Hubspot Deal.  Error: " + ex.Message);
        }
        
    }
}