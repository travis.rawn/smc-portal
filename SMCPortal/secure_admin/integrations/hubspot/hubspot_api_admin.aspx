﻿<%@ page title="" language="C#" async="true" masterpagefile="~/Customer.master" autoeventwireup="true" codefile="hubspot_api_admin.aspx.cs" inherits="hubspot_api_admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .row a {
            display: block;
        }

        .card {
            margin-bottom: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
    <div id="pageTitle">
        <h1>Hubspot API Tests</h1>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row panel card">
        <div class="panel-heading">
            <asp:LinkButton ID="lbCheckDailyUsage" runat="server" OnClick="lbCheckDailyUsage_Click" CssClass="btn-smc btn-smc-success">Check Daily Usage</asp:LinkButton>
        </div>
    </div>

    <div class="row panel ">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-3">
                    <label>Start Date</label><br />
                    <uc1:datetime_flatpicker runat="server" id="dtpStart" />
                </div>
                <div class="col-sm-3">
                    <label>End Date</label><br />
                    <uc1:datetime_flatpicker runat="server" id="dtpEnd" />
                </div>
                <div class="col-sm-3">
                    <label>Agent(s)</label><br />
                    <uc1:rzagentpicker runat="server" id="rzap" />
                </div>
                <div class="col-sm-3">
                    <label>Search Text</label><br />
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Placeholder="Search"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel-body">



                <div class="panel-heading">
                    <h3>Results:</h3>
                </div>
                Textbox:<br />
                <asp:TextBox ID="txtResults" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox><br />
                DataTable:<br />
                <uc1:sm_datatable runat="server" id="smdt" /><br />
                DetailsView:<br />
                <asp:DetailsView ID="dtvDetailsView" runat="server" Width="100%"></asp:DetailsView>

            </div>
        </div>
    </div>
    <h4>General Reporting</h4>
    <div class="row">
        <div class="col-sm-4">
            <asp:LinkButton ID="lbGetCombinedRzHubspotCompanyData" runat="server" OnClick="lbGetCombinedRzHubspotCompanyData_Click" CssClass="btn-smc btn-smc-success">Combined Rz / Hubspot Company Data</asp:LinkButton>


        </div>
    </div>

    <h4>Engagements</h4>
    <div class="row">
        <div class="col-sm-12">

            <div class="row">
                <div class="col-sm-4">
                    <asp:LinkButton ID="lbGetEngagements" runat="server" OnClick="lbGetEngagements_Click" CssClass="btn-smc btn-smc-success">Get Engagements</asp:LinkButton>

                </div>
                <div class="col-sm-4">
                    <asp:LinkButton ID="lbGetAllCalls" runat="server" OnClick="lbGetAllCalls_Click" CssClass="btn-smc btn-smc-success">Get Combined Calls</asp:LinkButton>

                </div>
                <div class="col-sm-4">
                    <asp:LinkButton ID="lbCreateHubspotCall" runat="server" OnClick="lbCreateHubspotCall_Click" CssClass="btn-smc btn-smc-success">Log A Call</asp:LinkButton>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <asp:LinkButton ID="lbShowRecentAPIEngagents" runat="server" OnClick="lbShowRecentAPIEngagents_Click" CssClass="btn-smc btn-smc-success">Recent API Engagements</asp:LinkButton>
                </div>
                <div class="col-sm-4">
                    <asp:LinkButton ID="lbRefreshEngagements" runat="server" OnClick="lbRefreshEngagements_Click" CssClass="btn-smc btn-smc-success">Refresh Engagements</asp:LinkButton>
                </div>
            </div>


        </div>
    </div>
    <h4>Company, Contacts, Owners</h4>
    <div class="row">
        <div class="col-sm-12 card">


            <div class="col-sm-4">
                <asp:LinkButton ID="lbGetCompanyByDomain" runat="server" OnClick="lbGetCompanyByDomain_Click" CssClass="btn-smc btn-smc-success">Company By Domain</asp:LinkButton>
            </div>
            <div class="col-sm-4">
                <asp:LinkButton ID="lbGetOwnerByEmail" runat="server" OnClick="lbGetOwnerByEmail_Click" CssClass="btn-smc btn-smc-success">Owner by Email</asp:LinkButton>
            </div>
            <div class="col-sm-4">
                <asp:LinkButton ID="lbGetContactByEmail" runat="server" OnClick="lbGetContactByEmail_Click" CssClass="btn-smc btn-smc-success">Contact by Email</asp:LinkButton>
            </div>

        </div>
        <div class="col-sm-12 card">


            <div class="col-sm-4">
                <asp:LinkButton ID="lbAssociatePortalAndHubspotUsers" runat="server" OnClick="lbAssociatePortalAndHubspotUsers_Click" CssClass="btn-smc btn-smc-success">Associate Portal and Hubspot Emails</asp:LinkButton>
            </div>


        </div>
    </div>
    <h4>Deals</h4>
    <div class="row">
        <div class="col-sm-4">
            <asp:LinkButton ID="lbGetAllDeals" runat="server" OnClick="lbGetAllDeals_Click" CssClass="btn-smc btn-smc-success">Get All Deals</asp:LinkButton>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbGetDealsForContact" runat="server" OnClick="lbGetDealsForContact_Click" CssClass="btn-smc btn-smc-success">Get Deals for Contact</asp:LinkButton>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbGetPipelineData" runat="server" OnClick="lbGetPipelineData_Click" CssClass="btn-smc btn-smc-success">Get Pipeline Data.</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <asp:LinkButton ID="lbGetRecentDeals" runat="server" OnClick="lbGetRecentDeals_Click" CssClass="btn-smc btn-smc-success">Get Recently Modified Deals</asp:LinkButton>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbCloseRzDealsFromHubspot" runat="server" OnClick="lbCloseRzDealsFromHubspot_Click" CssClass="btn-smc btn-smc-success">Close Rz Deals From Hubspot</asp:LinkButton>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbBatchUpdateDeals" runat="server" OnClick="lbBatchUpdateDeals_Click" CssClass="btn-smc btn-smc-success">Batch Update Deals from Rz</asp:LinkButton>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            BatchName:<asp:TextBox ID="txtBatchName" runat="server" CssClass="form-control"></asp:TextBox><br />
            Part:<asp:TextBox ID="txtPart" runat="server" CssClass="form-control"></asp:TextBox><br />
            Amount:<asp:TextBox ID="txtAmount" runat="server" CssClass="form-control"></asp:TextBox><br />
            <asp:Button ID="btnCreateTestDeal" runat="server" Text="Create Test Deal" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnCreateTestDeal_Click" />
            <asp:Button ID="btnCreateDeal" runat="server" Text="Create Deal From Fields" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnCreateDeal_Click" />
            <asp:Button ID="btnDeleteDeal" runat="server" Text="Delete A Deal" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnDeleteDeal_Click" />
        </div>
        <div class="col-sm-6">
            Deal ID:
                    <asp:TextBox ID="txtCurrentDealID" runat="server" CssClass="form-control"></asp:TextBox><br />

            Current Stages:
                        <asp:Label ID="lblDealStages" runat="server"></asp:Label><br />
            <asp:TextBox ID="txtNewStage" runat="server" CssClass="form-control"></asp:TextBox><br />

            Current Pipeline:
                        <asp:Label ID="lblPipelines" runat="server"></asp:Label><br />
            <asp:TextBox ID="txtNewPipeline" runat="server" CssClass="form-control"></asp:TextBox><br />
            <br />
            <asp:Button ID="btnUpdatePipeline" runat="server" Text="Update Pipeline" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnUpdatePipeline_Click" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            DealID:<asp:TextBox ID="txtDealIDOperation" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <asp:Button ID="btnUpdateCallDisposition" runat="server" Text="Update Call Disposition" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnUpdateCallDisposition_Click" />

        </div>
        <div class="col-sm-4">
            <asp:Button ID="btnFixMissingRecordings" runat="server" Text="Fix Missing Recordings" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnFixMissingRecordings_Click" />

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <label>Days Since:</label><asp:TextBox ID="txtOlderThanDays" runat="server" Text="30" />
            <br />
            <asp:Button ID="btnAgeRecords" runat="server" Text="Age Records" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnAgeRecords_Click" />
            <asp:Button ID="btnTestAgedEmail" runat="server" Text="Test Aged Email" CssClass="btn-smc btn-smc-primary btn-block" OnClick="btnTestAgedEmail_Click" />
        </div>
    </div>
    



    <h4>Forms</h4>
    <div class="row ">
        <div class="col-sm-4">
            <h3>Embedded Form (Embedded from Hubspot via Javascript)</h3>
            <div>
                <!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "1878634",
                        formId: "988e8fb2-004e-4a7e-9b18-2b373bfa7120"
                    });


                </script>

                <input type="button" class="hs-button">Test</>
            </div>
        </div>
        <div class="col-sm-4">
            <asp:LinkButton ID="lbTestQuoteFormFill" runat="server" OnClick="lbTestQuoteFormFill_Click" CssClass="btn-smc btn-smc-primary">Test Quote Form Fill</asp:LinkButton>
        </div>

    </div>

    <h4>HubDB</h4>
    <div class="card">
        <div class="row">
            <div class="col-sm-12">
                <asp:TextBox ID="txtTableName" runat="server" Placeholder="Name / ID" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <asp:LinkButton ID="lbGetHubDbObject" runat="server" OnClick="lbGetHubDbObject_Click" CssClass="btn-smc btn-smc-primary">Get HubDB Object</asp:LinkButton>
            </div>
            <div class="col-sm-3">
                <asp:LinkButton ID="lbGetAllHubDBTables" runat="server" OnClick="lbGetAllHubDBTables_Click" CssClass="btn-smc btn-smc-primary">Get All Tables</asp:LinkButton>
            </div>
            <div class="col-sm-3">
                <asp:LinkButton ID="lbGetHubDBTableByName" runat="server" OnClick="lbGetHubDBTableByName_Click" CssClass="btn-smc btn-smc-primary">Get Table By Name</asp:LinkButton>
            </div>
            <div class="col-sm-3">
                <asp:LinkButton ID="lbGetAllAdwordsIDs" runat="server" OnClick="lbGetAllAdwordsIDs_Click" CssClass="btn-smc btn-smc-primary">Get All AdwordsIDs</asp:LinkButton>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <uc1:sm_datatable runat="server" id="smdt_hubDbTables" />
            </div>
        </div>
    </div>


    <script>

        $("input.hs-button").on('click', function (event) {
            event.stopPropagation();
            event.stopImmediatePropagation();
            var message = 'Thank you!\n';
            var partnumber = $("#part_number-988e8fb2-004e-4a7e-9b18-2b373bfa7120").value + '\n';
            var phoneNumber = $("#phone-988e8fb2-004e-4a7e-9b18-2b373bfa7120").value + '\n';
            var targetPrice = $("#target_price-988e8fb2-004e-4a7e-9b18-2b373bfa7120").value + '\n';
            var quantity = 'Qty: ' + $("#quantity-988e8fb2-004e-4a7e-9b18-2b373bfa7120").value + '\n';
            var email = 'Email: ' + $("#email-988e8fb2-004e-4a7e-9b18-2b373bfa7120").value + '\n';


            message = message + partnumber + quantity + targetPrice + phoneNumber + email
            alert(message);
        });


    </script>



</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

