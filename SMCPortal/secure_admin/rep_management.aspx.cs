﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public partial class secure_admin_rep_management : System.Web.UI.Page
{
    //SMCPortalDataContext pdc = new SMCPortalDataContext();
    RzDataContext rdc = new RzDataContext();
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();
    List<n_user> TheRzUserList = new List<n_user>();
    List<Rep> TheRepList = new List<Rep>();

    n_user TheRzUser;
    Rep TheRep;

    protected void Page_Load(object sender, EventArgs e)
    {



        LoadRzUserData();
        if (!Page.IsPostBack)
        {

            LoadRepGridview();

        }


    }

    private void LoadRzUserData()
    {
        List<string> hiddenUsers = new List<string>();
        hiddenUsers.Add("Vendor");
        hiddenUsers.Add("E-Sales");
        hiddenUsers.Add("Dead Accounts (Do Not Call)");
        hiddenUsers.Add("Vendor");
        hiddenUsers.Add("Test");
        hiddenUsers.Add("Warehouse");
        hiddenUsers.Add("recognin");


        TheRzUserList = rdc.n_users.Where(w => w.is_inactive == false && !hiddenUsers.Contains(w.name)).ToList();

    }

    //Data Events
    protected void LoadRzRepDDL()
    {
        ddlRzRep.Items.Clear();



        if (TheRzUserList == null || TheRzUserList.Count <= 0)
        {
            ddlRzRep.Items.Add(new ListItem("--No Rz Users Available--", "0"));
            ddlRzRep.Enabled = false;
        }

        else
        {
            ddlRzRep.Items.Add(new ListItem("--Choose--", "0"));
            ddlRzRep.DataSource = TheRzUserList.OrderBy(o => o.name);
            ddlRzRep.DataTextField = "Name";
            ddlRzRep.DataValueField = "unique_id";
            ddlRzRep.AppendDataBoundItems = true;
            ddlRzRep.DataBind();
            ddlRzRep.SelectedValue = "0";
            ddlRzRep.Enabled = true;
        }
    }

    protected void LoadRepGridview()
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
            TheRepList = pdc.Reps.ToList();
        if (TheRepList.Count > 0)
        {
            GridView1.DataSource = TheRepList;
            GridView1.DataKeyNames = new string[] { "RzRepID" };
            GridView1.DataBind();
        }

    }

    protected Rep LoadSelectedRep(string RzRepID)
    {

        try
        {
            using (sm_portalEntities pdc = new sm_portalEntities())
                TheRep = pdc.Reps.Where(w => w.RzRepID == RzRepID).FirstOrDefault();

            if (TheRep == null)
                throw new Exception("Failed to load rep.");

            ViewState["RepFullName"] = TheRep.FullName;
            ViewState["RepID"] = RzRepID;
            ViewState["RepEmail"] = TheRep.Email;
            LoadHeadshot();
            pnlEditRep.Visible = true;

            return TheRep;


        }
        catch (Exception ex)
        {
            HandleError(ex.ToString());
            return null;
        }
    }
    protected void LoadHeadshot()
    {
        TheRzUser = LoadRzUser(TheRep.RzRepID);
        if (TheRzUser == null)
            return;
        if (TheRzUser.leaderboard_image_url != null)
        {
            imgHeadshot.ImageUrl = TheRzUser.leaderboard_image_url;
            imgHeadshot.Visible = true;
            lblHeadshotAlert.Text = "Current Headshot for " + TheRep.FullName + ":";
        }
        else
        {
            lblHeadshotAlert.Text = "No Headshot selected for current user.";
        }

    }

    //Control Events
    protected void Button_HeadshotUpload_Click(object sender, EventArgs e)
    {

        UploadHeadshotFile();
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        LoadRepGridview();
        if (pnlEditRep.Visible == true)
            pnlEditRep.Visible = false;
    }

    protected void ddlRzRep_SelectedIndexChanged(object sender, EventArgs e)
    {

        btnCreateRep.Visible = true;

    }

    private n_user LoadRzUser(string unique_id)
    {
        return TheRzUserList.Where(w => w.unique_id == unique_id).FirstOrDefault();
    }

    protected void btnDeleteRep_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        string rep_uid = GridView1.DataKeys[row.RowIndex].Value.ToString();
        DeleteRep(rep_uid);
    }

    protected void btnCreateRep_Click(object sender, EventArgs e)
    {
        CreateRep();
        ResetFields();
        LoadRzRepDDL();
    }

    protected void btnAddRep_Click(object sender, EventArgs e)
    {

        LoadRzRepDDL();
        if (btnAddRep.Text == "Add Rep from Rz")
        {
            ResetFields();
            pnlCreateRep.Visible = true;
            btnAddRep.Text = "Cancel Add";
        }
        else
        {
            pnlCreateRep.Visible = false;
            btnAddRep.Text = "Add Rep from Rz";
            ResetFields();
        }

    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string RzRepID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        DeleteRep(RzRepID);
        ResetFields();
        LoadRzRepDDL();

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

        ResetFields();
        string userID = GridView1.SelectedDataKey["RzRepID"].ToString();
        LoadUserAndRep(userID);



    }

    private void LoadUserAndRep(string userID)
    {
        //Loading this first so the rap cabn pull hedshot url from rz
        TheRzUser = LoadRzUser(userID);
        if (TheRzUser == null)
            throw new Exception("Could not load the RzUser.");


        TheRep = LoadSelectedRep(userID);
        if (TheRep == null)
            throw new Exception("Could not load the Rep.");
    }

    protected void btnSaveTawkTo_Click(object sender, EventArgs e)
    {
        //if (btnSaveTawkTo.Text == "Save")
        //{
        //    if (!String.IsNullOrEmpty(btnSaveTawkTo.Text))
        //    {
        //        SaveTawkToURL();
        //    }
        //    else
        //    {
        //        HandleError("Please input a value for the Tawk.To URL");
        //    }
        //}


    }



    //Page Events


    protected void HandleError(string msg)
    {

        tools.HandleResult("fail", msg);

    }

    protected void HandleSuccess(string msg)
    {
        tools.HandleResult("success", msg);

    }



    protected void ResetFields()
    {
        ddlRzRep.SelectedValue = "0";
        imgHeadshot.ImageUrl = "";
        pnlCreateRep.Visible = false;
        btnCreateRep.Visible = false;
        //pnlEditRep.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "hidden";

    }

    //CRUD Events

    protected void CreateRep()
    {
        try
        {
            Rep rep = new Rep();
            rep.RzRepID = ddlRzRep.SelectedValue.ToString();
            rep.FullName = ddlRzRep.SelectedItem.ToString();
            TheRzUser = LoadRzUser(rep.RzRepID);
            if (TheRzUser == null)
                throw new Exception("Could not find Rz User.");
            rep.Email = TheRzUser.email_address;

            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                pdc.Reps.Add(rep);
                pdc.SaveChanges();
            }



            LoadSelectedRep(rep.RzRepID);
            LoadRepGridview();
            btnAddRep.Text = "Add Rep from Rz";
        }
        catch (Exception ex)
        {
            HandleError(ex.ToString());

        }
    }

    protected void UploadHeadshotFile()
    {

        if (fuHeadshot.HasFile)
        {
            try
            {
                //Get Details on File and Upload Location
                string filePath = fuHeadshot.FileName;
                string virtualFolder = "~/Images/Headshots/";
                string physicalFolder = Server.MapPath(virtualFolder);
                //string fileName = Path.GetFileName(filePath);
                string extension = System.IO.Path.GetExtension(filePath);
                //string fileName = rep.FullName + extension;
                string ext = Path.GetExtension(filePath);
                string contenttype = String.Empty;
                //Get the "HeadshotURL"
                string HeadshotUrl = virtualFolder + filePath;

                //Set the contenttype based on File Extension
                switch (ext)
                {

                    case ".jpg":
                        contenttype = "image/jpg";
                        break;
                    case ".png":
                        contenttype = "image/png";
                        break;
                    case ".gif":
                        contenttype = "image/gif";
                        break;
                }

                if (!string.IsNullOrEmpty(contenttype))
                {
                    string userID = GridView1.SelectedDataKey["RzRepID"].ToString();
                    LoadUserAndRep(userID);
                    fuHeadshot.SaveAs(System.IO.Path.Combine(physicalFolder, filePath));

                    string image_url = "~/Images/Headshots/" + filePath;





                    //update
                    TheRzUser.leaderboard_image_url = image_url;
                    rdc.SubmitChanges();


                    //update
                    TheRep.leaderboard_image_path = image_url;
                    using (sm_portalEntities pdc = new sm_portalEntities())
                        pdc.SaveChanges();





                    //Load the new Headshot into the preview window
                    LoadHeadshot();

                    //Reload the GridView to get the new image
                    LoadRepGridview();
                    HandleSuccess("Succesfully updated headshot for " + TheRep.FullName);

                }
                else
                {
                    HandleError("Gotta be an Image File (.jpg, .png, .gif)");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex.ToString());
            }
        }
        else
        {
            HandleError("No headshot image file selected");
        }
    }


    protected void DeleteRep(string RzRepID)
    {

        try
        {
            LoadSelectedRep(RzRepID);
            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                pdc.Reps.Remove(TheRep);
                pdc.SaveChanges();
            }

            LoadRepGridview();

        }
        catch (Exception ex)
        {
            HandleError(ex.ToString());
        }
    }





}