﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/themes/AdminLTE/AdminLTE.master" AutoEventWireup="true" CodeFile="admin_dashboard.aspx.cs" Inherits="secure_admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    

    <div class="row">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
            <div style="padding-bottom: 9px; margin: 40px 0 20px;">
                <asp:Button ID="btnKillSwitch" runat="server" Text="BIG RED BUTTON" CssClass="btn btn-danger btn-lg pull-right" OnClick="btnKillSwitch_Click" OnClientClick="if(!ConfirmKillSwitch())return false;" />
            </div>
        </div>
        <div class="col-lg-4">
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <a href="/secure_admin/security/user_management.aspx">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Users</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Manage Users</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>

        <a href="/secure_admin/security/role_management.aspx">
            <div class="col-lg-3 col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-graduation-cap fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Roles</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Manage Roles</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </a>

        <a href="/secure_admin/security/profile_management.aspx">
            <div class="col-lg-3 col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-address-book fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Profiles</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Manage Profiles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </a>
        <a href="/secure_admin/rep_management.aspx">
            <div class="col-lg-3 col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-id-badge fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Reps</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<a href="#">--%>
                    <div class="panel-footer">
                        <span class="pull-left">Manage Reps</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                    <%--  </a>--%>
                </div>
            </div>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <a href="/secure_admin/integrations/integrations_dashboard.aspx">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-cogs fa-5x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Integrations</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Go</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="/secure_admin/maintenance/maintenance_dashboard.aspx">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-wrench fa-5x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Maintenance</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Go</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="/secure_admin/logs/sm_logging.aspx">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-list fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Logs</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Go</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href=" /secure_admin/sandbox/admin_sandbox.aspx">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-flask  fa-5x" aria-hidden="true"></i>
                                <%--<i class="fa fa-list fa-5x"></i>--%>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div>
                                    <h4>Sandbox</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Go</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>
    </div>

</asp:Content>

