﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="block_page.aspx.cs" Inherits="public_block_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .block-alert-div {
            padding-bottom: 100px;
            text-align: center;
        }

        .block-alert-icon {
            margin-bottom: 80px;
            font-size: 7vh;
        }

        .block-alert-text {
            font-size: 28px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="Server">
    <div style="text-align: center; font-size: 40px;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BannerContent" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="block-alert-div">
        <i class="far fa-hand-paper block-alert-icon"></i>
        <p class="block-alert-text">
            Our system has detected anomalous behaviour from your session, and has blocked access to the site.  If you feel this is in error, please email 
            <a href="mailto:webmaster@sensiblemicro.com?subject=Account Block Removal Request">webmaster@sensiblemicro.com</a> or call 877-992-1930.  Thank you.
        </p>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainContent1" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="MainContent2" runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="FullWidthContent" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SubMain" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="NonFormContent" runat="Server">
</asp:Content>

