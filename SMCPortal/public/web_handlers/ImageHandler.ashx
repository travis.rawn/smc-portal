﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>
using SensibleDAL.ef;
using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;

public class ImageHandler : IHttpHandler
{
    SM_Tools tools = new SM_Tools();
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string fileName;

            string strId = Tools.Strings.SanitizeInput(context.Request.QueryString["id"]);
            if (string.IsNullOrEmpty(strId))
                return;
            string inspectionType = Tools.Strings.SanitizeInput(context.Request.QueryString["type"]);
            if (!string.IsNullOrEmpty(inspectionType))
            {
                fileName = inspectionType + "_image.jpg";
            }
            else
            {
                inspectionType = "unknown";
                fileName = "image.jpg";
            }

            string id = Tools.Strings.SanitizeInput(context.Request.QueryString["id"].ToString());
            using (sm_binaryEntities smb = new sm_binaryEntities())
            {
                string imgPath = smb.insp_images.Where(w => w.insp_image_id == id && w.insp_type == inspectionType).Select(s => s.img_path_web).FirstOrDefault();
                if (!string.IsNullOrEmpty(imgPath))
                {
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                    context.Response.ContentType = "image/jpeg";
                    byte[] imgBytes = File.ReadAllBytes(imgPath);
                    context.Response.OutputStream.Write(imgBytes, 0, imgBytes.Length);
                }

            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message + " :  " + ex.InnerException);
            //string error = ex.Message + " :  " + ex.InnerException;
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}