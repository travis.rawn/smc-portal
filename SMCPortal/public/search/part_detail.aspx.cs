﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using HubspotApis;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class public_search_part_detail : System.Web.UI.Page
{
    //silicon_expert se = new silicon_expert();
    SM_Tools tools = new SM_Tools();
    RzDataContext rdc = new RzDataContext();
    RzTools rzt = new RzTools();
    MembershipUser User;
    string UserName;
    company Comp;
    bool ShowSiliconExpertDetails = false;
    protected DataSet ListPartDataSet;
    protected DataSet PartDetailDataSet;
    protected DataTable dtPArtDto;
    protected string PackageType;
    List<long> ListAdwordIds;
    //long HubDbFranchisePartID;
    protected bool SkipCaptcha = false;
    long ComID = 0;




    protected string PartSearchString
    {
        get
        {
            return (string)ViewState["PartSearchString"];
        }
        set
        {
            ViewState["PartSearchString"] = value;
        }
    }
    protected string ComIDString
    {
        get
        {
            return (string)ViewState["ComIDString"];
        }
        set
        {
            ViewState["ComIDString"] = value;
        }
    }
    protected string QtyString
    {
        get
        {
            return (string)ViewState["QtyString"];
        }
        set
        {
            ViewState["QtyString"] = value;
        }
    }
    protected string utm_campaign;
    protected string ImageString;
    protected string DataSheetUrl;



    //PartDto Elements
    protected string SePartNumber;
    protected string SeComID;
    protected string SeManufacturer;
    protected string SePlName;
    protected string SeDescription;
    protected string SeDatasheet;
    protected string SeLifecycle;
    protected string SeRoHS;
    protected string SeRoHSVersion;
    protected string SeMatchRating;
    protected string SeTaxonomyPath;
    protected string SePartList_Id;

    //Rz Elements
    List<string> RzPartrecordIDList = new List<string>();
    string RzPartNumber = "";
    string RzManufacturer = "";
    //long RzCurrentQuantity = 0;

    //HubDB Elements
    HubspotApi.HubDbPart HubDbPart;
    HubspotApi.HubDbPart HubDbAdwordPart;
    //long HubDbFranchisePartID;
    //string hubDBImageUrl;
    //string hubDBPartNumber;
    //string hubDBManufacturer;

    //Page Elements
    string PagePartNumber = "";
    string PageManufacturer = "";

    //When no Silicon Expert Result, populate with Partrecord Data.
    List<partrecord> RzPartMatches;
    //String to carry the current availability, i.e. Ships Today, or Parts Available.
    string StrAvailable;

    //Default Image
    string DefaultImageString = "/Images/nf.png";





    protected void Page_Load(object sender, EventArgs e)
    {




        try
        {
            ((hs_flex)this.Master).ToggleTitleDiv(false);
            ((hs_flex)this.Master).ToggleBannerDiv(false);

            string URL = Request.Url.AbsoluteUri.ToString();
            //Get Querystring varaibles
            LoadQueryString();

            CheckSkipCaptcha();

            //Verify ComID if any
            TrySetComID();

            //Check if this is an adword
            //LoadAdwordsExceptions();
            //If no ComID or Part, redirect to Search
            if (string.IsNullOrEmpty(ComIDString) && string.IsNullOrEmpty(PartSearchString))
                Response.Redirect("part_search.aspx", false);
            //Main Loading of all sections
            CompleteLoad();

            //Update the meta tag when all variables calculated.
            string metaDescription = "Part Number: " + PagePartNumber + " | MFG: " + PageManufacturer;
            Page.MetaDescription = metaDescription;
            //((hs_flex)this.Master).UpdateDescriptionMetaTag("Part Number: "+ PagePartNumber + "MFG: "+PageManufacturer);
            //tools.HandleResult("success", "Successfully loaded Part Detail for ComID: "+ComID.ToString());
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Successfully loaded Part Detail for ComID: " + ComID.ToString(), false);

        }
        catch (Exception ex)
        {

            string errorMsg = ex.Message + " Inner: " + ex.InnerException;
            SensibleDAL.SystemLogic.Logs.LogEvent(ex, errorMsg);
            //else if (ex.Message == "We are currently undergoing maintenance, please try again later.")
            //    SendErrorEmail(ex);

            tools.HandleResult("warning", ex.Message, 300000);
        }
    }


    private void CompleteLoad()
    {
        //The the current user and Company.
        GetUserAndCompany();

        bool partSearchDisabled = !SiliconExpertLogic.IsEnabled();

        if (partSearchDisabled)
        {
            tools.HandleResult("warning", "Our part search subsystem is currently undergoing maintenance.  Please try again later.", 1000000);
            return;
        }
        //Check to see if the user / session meets theh criteria for Silicon Expert Detail Results
        CheckLoadSiliconExpetDetails();

        //Get List Dataset
        if (!string.IsNullOrEmpty(PartSearchString))
            LoadListDataSet();



        //This gets ComIDs from HubDB Tables like Adwords and FranchiseParts
        if (ComID > 0)
        {
            LoadHubDbTables();
            //IF authenticated or SkipCaptcha, load the details dataset. 
            //This is necessary for to support Async Calls with Page Lifecycle.  This both registers as Async and runs the method.  Must include  Async="true" in the webform Page Directive
            //RegisterAsyncTask(new PageAsyncTask(LoadDetailDataSet));
            LoadDetailDataSet(ComID.ToString());
        }

        //If we have a Silicon Expert Match at this point, set that as part number        
        if (string.IsNullOrEmpty(PartSearchString))
            if (!string.IsNullOrEmpty(SePartNumber))
                PartSearchString = SePartNumber;
        if (string.IsNullOrEmpty(PartSearchString) || PartSearchString.Length < 3)
        {
            //tools.HandleResult("fail", "Sorry no components were found that match your specfication.");
            //throw new Exception("Sorry no components were found that match your specfication.");
            ShowNoResults();
            return;
        }

        if (ListPartDataSet == null)
            LoadListDataSet();


        if (SiliconExpertLogic.PartIsBlacklisted(PartSearchString))
        {
            ShowNoResults();
            return;
        }

        //LoadListDataSet();

        //If we have a list dataset now, and we didn't start with a comid, try getting details now
        if (ListPartDataSet != null)
            if (PartDetailDataSet == null)
                if (ShowSiliconExpertDetails)
                {
                    DataTable dt = ListPartDataSet.Tables["PartDto"];
                    foreach (DataRow r in dt.AsEnumerable())
                    {
                        string comID = r[0].ToString();
                        if (!string.IsNullOrEmpty(comID))
                            LoadDetailDataSet(comID);
                    }
                    
                }
                    

        //At this point we should have an identified PagePartNumber;
        if (string.IsNullOrEmpty(PartSearchString))
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, "Null or empty 'SePartNumber' for Part Deatails page.");
            Response.Redirect("~/public/search/part_search.aspx", false);
        }
        quote_form.SetPartNumber(PartSearchString);


        //Load RzPartData - call this after List Search so it works for id= and pn= since id will need a API call to get a valid PN for List Search
        LoadRzPartData();

        //Load Page Part Details - Either Silicon Expert or Rz Part Data may be available, set PN, MFG accordingly
        LoadPagePartDetails();
        //Set the page title to match the search string
        Page.Title = PagePartNumber.ToUpper() + " | Sensible Micro Corp";
        //Load the Overview Section
        LoadOverView();


        //Specifications Tab
        LoadSpecifications();

        //Environmental Tab
        LoadEnvironment();
        //PCN Tab
        LoadPCN();
        //Lifecycle Tab
        LoadLifecycle();
        //Registration Buttons
        //ShowRegisterButtons(true);

    }

    private void ShowNoResults()
    {
        pnlNoResults.Visible = true;
        pnlResults.Visible = false;
        pnlTabs.Visible = false;
    }

    //private void LoadHubdbPartData()
    //{
    //    DataTable dtHubDBParts = GetHubDbTable();
    //    {
    //        if (dtHubDBParts != null && dtHubDBParts.Rows.Count > 0)
    //        {

    //            var dataRow = (from product in dtHubDBParts.AsEnumerable()
    //                           where product.Field<string>("PartNumber") == PartSearchString
    //                           select product).FirstOrDefault();

    //            if (dataRow != null)
    //            {
    //                hubDBPartNumber = dataRow.Field<string>("PartNumber");
    //                hubDBManufacturer = dataRow.Field<string>("Mfg");
    //                hubDBImageUrl = dataRow.Field<string>("ImageUrl");

    //            }


    //        }
    //    }
    //}

    //private DataTable GetHubDbTable()
    //{
    //    DataTable dt = new DataTable();
    //    dt.Columns.Add("PartNumber", typeof(string));
    //    dt.Columns.Add("Mfg", typeof(string));
    //    dt.Columns.Add("ImageUrl", typeof(string));

    //    //Search the HubDB Table
    //    //HubspotApi.HubDbRows HubDbRowCollection = HubspotApi.HubDBs.GetHubDBTableRows("1056285");
    //    HubspotApi.HubDbRows HubDbRowCollection = HubspotApi.HubDBs.GetHubDBTableRows("1041110");
    //    if (HubDbRowCollection != null && HubDbRowCollection.Rows.Count > 0)
    //    {
    //        List<HubspotApi.Values> listValues = HubDbRowCollection.Rows.Select(s => s.values).ToList();

    //        foreach (var v in listValues)
    //        {
    //            DataRow row = dt.NewRow();
    //            if (v.two == null)
    //                continue;
    //            row["PartNumber"] = v.two.ToString();
    //            if (v.three != null)
    //                row["Mfg"] = v.three.ToString();
    //            if (v.graphic != null)
    //                row["ImageUrl"] = v.graphic.Url.ToString();
    //            dt.Rows.Add(row);

    //        }
    //    }
    //    return dt;
    //}

    //private bool CheckDoDetailSearch()
    //{
    //    //Proper ComID
    //    if (ComID <= 0)
    //        return false;

    //    //AdWord ID
    //    if (HubDbAdwordPart != null)
    //        return true;

    //    //HubDB ID
    //    //if (ListFranchiseParts.Contains(ComID))
    //    if (HubDbPart != null)
    //        return true;

    //    //Authenticated
    //    return Request.IsAuthenticated;

    //}

    private bool TrySetComID()
    {
        ComID = 0;
        //1st check seComID
        long.TryParse(SeComID, out ComID);
        //next try querystring
        if (ComID <= 0)
            long.TryParse(ComIDString, out ComID);
        //If still no ComID, return
        if (ComID <= 0)
            return false;
        return true;
    }

    private void LoadPagePartDetails()
    {
        PagePartNumber = "N/A";
        PageManufacturer = "N/A";


        //HubDbFranchisePartID = ListFranchisePartIds.Where(w => w == ComID).Select(s => s.)


        //Set the part Number on the Quote Form
        if (!string.IsNullOrEmpty(RzPartNumber))
            PagePartNumber = RzPartNumber;
        else if (HubDbPart != null)
            PagePartNumber = HubDbPart.PartNumber;
        else if (!string.IsNullOrEmpty(SePartNumber))
            PagePartNumber = SePartNumber;
        else PagePartNumber = PartSearchString;

        //Trim and UpperCase
        PagePartNumber = PagePartNumber.Trim().ToUpper();

        //PAge MFG        
        if (!string.IsNullOrEmpty(RzManufacturer))
            PageManufacturer = RzManufacturer;
        else if (HubDbPart != null)
            PageManufacturer = HubDbPart.Manufacturer;
        else if (!string.IsNullOrEmpty(SePartNumber))
            PageManufacturer = SeManufacturer;
        //Trim and UpperCase
        PageManufacturer = PageManufacturer.Trim().ToUpper();

        //Set the Quote Form Part Number
        quote_form.SetPartNumber(PagePartNumber);
    }

    private void LoadQueryString()
    {

        //ComID
        ComIDString = Tools.Strings.SanitizeInput(Request.QueryString["id"]);
        //Override Part Number
        PartSearchString = Tools.Strings.SanitizeInput(Request.QueryString["pn"]);
        //This can contain HTML unfriendly characters - remove them.
        PartSearchString = HttpUtility.UrlDecode(PartSearchString);
        //Set a Quantity
        QtyString = Tools.Strings.SanitizeInput(Request.QueryString["q"]);
        //Image URL
        ImageString = Tools.Strings.SanitizeInput(Request.QueryString["im"]);
        //utm_campaign
        utm_campaign = Tools.Strings.SanitizeInput(HttpUtility.UrlDecode(Request.QueryString["utm_campaign"]));
        if (!string.IsNullOrEmpty(utm_campaign))
            utm_campaign = utm_campaign.ToLower();
        ////source
        //string source = Tools.Strings.SanitizeInput(HttpUtility.UrlDecode(Request.QueryString["source"]));
        //if (!string.IsNullOrEmpty(utm_campaign))
        //    hfUtmCampaign.Value = utm_campaign;
        //if (!string.IsNullOrEmpty(source))
        //    hfUtmCampaign.Value = source;




    }

    private void LoadAvailability()
    {
        //Stock / consign
        //lblOverViewStatus IN STOCK - SHIPS TODAY
        //lblOverViewQty QTY: #

        //Excess & Offers
        //lblOverViewStatus PARTS AVAILABLE
        //lblOverViewQty QTY: # 

        //All Others
        //lblOverViewStatus SOURCING AVAILABLE
        //lblOverViewQty.Visible = false;


        //Set the default variables

        //spanStockQty.Visible = false;
        //spanExcessQty.Visible = false;
        //lblOverViewStatus.Text = "SOURCING AVAILABLE";
        spnSourcing.Visible = true;
        spnStock.Visible = false;
        spnExcess.Visible = false;
        if (RzPartMatches != null && RzPartMatches.Count > 0)
        {
            //No Rz Stock, hide qty
            //stock, consign, excess, offers    
            //Check matched stock types
            List<string> matchedStockTypes = RzPartMatches.Select(s => s.stocktype.ToLower()).Distinct().ToList();
            bool hasStockConsign = matchedStockTypes.Contains("stock") || matchedStockTypes.Contains("consign");
            bool hasExcessOffers = matchedStockTypes.Contains("excess") || matchedStockTypes.Contains("offers");

            //Set stocktype label
            if (hasStockConsign)
            {

                //Stock QTY
                long rzStockQty = RzPartMatches.Where(w => w.stocktype.ToLower() == "stock" || w.stocktype.ToLower() == "consign").Sum(s => (s.quantity ?? 0) - (s.quantityallocated ?? 0));
                if (rzStockQty > 0)
                {
                    spnStock.Visible = true;
                    spnSourcing.Visible = false;
                    lblStockQty.Text = rzStockQty.ToString();

                }
            }

            if (hasExcessOffers)
            {


                long rzExcessQty = RzPartMatches.Where(w => w.stocktype.ToLower() == "excess" || w.stocktype.ToLower() == "offers").Sum(s => (s.quantity ?? 0) - (s.quantityallocated ?? 0));
                if (rzExcessQty > 0)
                {
                    spnExcess.Visible = true;
                    spnSourcing.Visible = false;
                    lblExcessQty.Text = rzExcessQty.ToString();
                }
            }

        }

    }


    private void GetUserAndCompany()
    {
        if (Request.IsAuthenticated)
        {
            if (Membership.GetUser() != null)
            {
                User = Membership.GetUser();
                UserName = User.UserName;
                if (!string.IsNullOrEmpty(Profile.CompanyID))
                    Comp = rdc.companies.Where(w => w.unique_id == Profile.CompanyID).FirstOrDefault();
            }


        }
        else
        {
            UserName = "Public / Anonymous";
        }
    }


    private void LoadHubDbTables()
    {


        ////Franchise Parts
        //List<HubspotLogic.HubDbPart> ListFranchiseParts = await HubspotLogic.GetHubDbFranchiseParts();
        //if (ListFranchiseParts != null && ListFranchiseParts.Count > 0)
        //{
        //    HubDbPart = ListFranchiseParts.Where(w => w.ComID == ComID.ToString()).FirstOrDefault();
        //    if (HubDbPart == null)//Sometimes, like Franchised Parts, there may be no silicon Expert result, this no ComID, but part is in Database.  Check for that now
        //        if (!string.IsNullOrEmpty(PartSearchString))
        //            HubDbPart = ListFranchiseParts.Where(w => w.PartNumber == PartSearchString.ToString()).FirstOrDefault();
        //}



        //ADwords        
        //List<HubspotApi.HubDbPart> ListAdWordsParts = await HubspoHubspotApitLogic.GetHubDbAdwordsParts();
        //if (ListAdWordsParts != null && ListAdWordsParts.Count > 0)
        //{
        //    HubDbAdwordPart = ListAdWordsParts.Where(w => w.ComID == ComID.ToString()).FirstOrDefault();
        //    if (HubDbAdwordPart == null)//Sometimes, like Franchised Parts, there may be no silicon Expert result, this no ComID, but part is in Database.  Check for that now
        //        if (!string.IsNullOrEmpty(PartSearchString))
        //            HubDbAdwordPart = ListAdWordsParts.Where(w => w.PartNumber == PartSearchString.ToString()).FirstOrDefault();
        //}
    }


    private void LoadDetailDataSet(string comID)
    {

        if (!ShowSiliconExpertDetails)
            return;
        List<string> comIDLIst = new List<string>() { comID};
        

        PartDetailDataSet = SiliconExpertLogic.GetPartDetailDataSetJSON_Synchronous(comIDLIst, UserName, null, false, 1);
        if (PartDetailDataSet != null)
        {
            if (PartDetailDataSet.Tables.Count > 0)
            {
                ////If more than 1 row in Results then more than 1 part found.
                //if (PartDetailDataSet.Tables["ResultDto"].Rows.Count > 1)
                //    throw new Exception("Search returned multiple results.");//WE only want single-part results.  This will happen if we use ComID
                if(PartDetailDataSet.Tables["Status"] != null)
                {
                    string statusMsg = PartDetailDataSet.Tables["Status"].Rows[0]["Message"].ToString();
                    if (statusMsg == "No Results Found")
                        tools.JS_Modal_Redirect("No results found.  Please try searching for a different part.", "part_search.aspx");
                    return;
                }
                    
                //silicon_expert.sePartDetail sePart = new silicon_expert.sePartDetail();
                //Set the part Number on the Quote Form
                DataTable summDT = PartDetailDataSet.Tables["SummaryData"];
                SePartNumber = summDT.Rows[0]["PartNumber"].ToString();
                SeManufacturer = summDT.Rows[0]["Manufacturer"].ToString();
                SeDescription = summDT.Rows[0]["PartDescription"].ToString();
                //quote_form.SetPartNumber(SePartNumber);
                if (!string.IsNullOrEmpty(SePartNumber))
                    PartSearchString = SePartNumber.Trim().ToUpper();
                //Keep log of search context in Rz (username, part searched, ip, etc)  

                if (User != null)
                    rzt.LogPortalSearchedPart(User, SePartNumber, SeManufacturer ?? "not found", SeDescription ?? "not found", Comp);
                else
                    rzt.LogPortalSearchedPart("", SePartNumber, SeManufacturer ?? "not found", SeDescription ?? "not found");
            }
            else
            {
                //If Null (shouldn't be because fuzzy, it'll be null if CheckMaxSEarches or RateLimit is hit
                Response.Redirect("~/part_search.aspx", false);
            }
        }
    }


    private void LoadRzPartData()
    {
        //RZ Partrecord matches       
        RzPartMatches = PartLogic.GetRzPartMatchList(PartSearchString);
        if (RzPartMatches.Count > 0)
        {
            //Save unique id's to list for future in-memory use
            RzPartrecordIDList = RzPartMatches.Select(s => s.unique_id).ToList();
            //Grab 
            RzPartNumber = RzPartMatches.Where(w => (w.fullpartnumber ?? "").Length > 0).Select(s => s.fullpartnumber).FirstOrDefault();
            RzManufacturer = RzPartMatches.Where(w => (w.manufacturer ?? "").Length > 0).Select(s => s.manufacturer).FirstOrDefault();
        }
    }

    private void LoadListDataSet()
    {
        //The the partrecord matches and the Silicon Expert data - FREE stuff

        //[PartDto]
        //ComID:43853735	
        //PartNumber: LNJ347W83RA
        //Manufacturer: Panasonic
        //PlName: LEDs  
        //Description: LED Uni-Color Green 575nm 2-Pin Chip LED
        //Datasheet: https://industrial.panasonic.com/content/data/SC/ds/ds4/LNJ347W83RA_E_discon.pdf	
        //Lifecycle: Obsolete
        //RoHS: Yes
        //RoHSVersion: 2011/65/EU, 2015/863	
        //MatchRating: Exact
        //Taxonomy: LEDs and LED Lighting > LED Indication > LEDs
        //PartListID: 0	

        //Default Values
        SeManufacturer = "Not Available";
        SePlName = "Not Available";
        SeDescription = "Not Available";
        SeDatasheet = "Not Available";
        SeLifecycle = "Not Available";
        SeRoHS = "Not Available";
        SeRoHSVersion = "Not Available";



        //Add the string(s) to the search list
        char[] delimiterChars = { ',' };
        //string siliconExpertSearchString = SiliconExpertLogic.StripInvalidSiliconExpertSearchCharacters(PartSearchString);
        List<string> pList = new List<string>() { PartSearchString };

        //Get the Dataset via Silicon Expert API

        ListPartDataSet = SiliconExpertLogic.GetListPartSearchDataSetJSON_Synchronous(pList);
        //if (pList.Count <= 0)
        //    return;

        //Set Variables from Silicon Expert Data
        if (ListPartDataSet != null)
        {
            if (ListPartDataSet.Tables["PartDto"] == null)
                return;

            dtPArtDto = ListPartDataSet.Tables["PartDto"];

            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["PartNumber"].ToString()))
                SePartNumber = dtPArtDto.Rows[0]["PartNumber"].ToString(); ;
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["ComID"].ToString()))
                SeComID = dtPArtDto.Rows[0]["ComID"].ToString(); ;
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["Manufacturer"].ToString()))
                SeManufacturer = dtPArtDto.Rows[0]["Manufacturer"].ToString();
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["PlName"].ToString()))
                SePlName = dtPArtDto.Rows[0]["PlName"].ToString();
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["Description"].ToString()))
                SeDescription = dtPArtDto.Rows[0]["Description"].ToString();
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["Datasheet"].ToString()))
                SeDatasheet = dtPArtDto.Rows[0]["Datasheet"].ToString();
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["Lifecycle"].ToString()))
                SeLifecycle = dtPArtDto.Rows[0]["Lifecycle"].ToString();
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["RoHS"].ToString()))
                SeRoHS = dtPArtDto.Rows[0]["RoHS"].ToString();
            if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["RoHSVersion"].ToString()))
                SeRoHSVersion = dtPArtDto.Rows[0]["RoHSVersion"].ToString();
            //if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["MatchRating"].ToString()))
            //    MatchRating = dtPArtDto.Rows[0]["MatchRating"].ToString();
            //if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["TaxonomyPath"].ToString()))
            //    TaxonomyPath = dtPArtDto.Rows[0]["TaxonomyPath"].ToString();
            //if (!string.IsNullOrEmpty(dtPArtDto.Rows[0]["PartList_Id"].ToString()))
            //    PartList_Id = dtPArtDto.Rows[0]["PartList_Id"].ToString();
        }


    }


    private string LoadOverViewImage()
    {

        imgOverview.AlternateText = "Image for " + PagePartNumber;
        //Check for Hubspot Images (Geyer, etc)
        //Order of precedence = Adword Image, HubDB Image, Silicon Expert Image

        if (HubDbAdwordPart != null)
        {
            if (!string.IsNullOrEmpty(HubDbAdwordPart.ImageUrl))
                return HubDbAdwordPart.ImageUrl;
        }
        else if (HubDbPart != null)
        {
            if (!string.IsNullOrEmpty(HubDbPart.ImageUrl))
                return HubDbPart.ImageUrl;
        }


        //CHeck Custom /Manual Imagery (Adwords, etc.)
        if (!string.IsNullOrEmpty(GetCustomImageURL()))
            return GetCustomImageURL();

        if (!string.IsNullOrEmpty(ImageString))
            return "/Images/pkg_img/" + ImageString;
        //Comid based Image Search based on Package Type
        //if (PartDetailDataSet != null)
        //    return GetImageFromPackageData();

        string datatSetImageUrl = "";
        if (PartDetailDataSet != null)
        {
            if (PartDetailDataSet.Tables["ProductImage"] != null)
                if (PartDetailDataSet.Tables["ProductImage"].Rows.Count > 0)
                    datatSetImageUrl = PartDetailDataSet.Tables["ProductImage"].Rows[0]["ProductImageLarge"].ToString();
            if (!string.IsNullOrEmpty(datatSetImageUrl))
                return datatSetImageUrl;
        }
        //Default image
        return DefaultImageString;
    }

    private string GetImageFromPackageData()
    {
        PackageType = LoadPackageData();
        if (string.IsNullOrEmpty(PackageType)) //REturn default here, because can't do a ToUppper on null
            return "/Images/nf.png";
        switch (PackageType.ToUpper())
        {

            case "DPAK":
                {
                    return "/Images/pkg_img/DPAK.png";
                }
            case "DIP":
                {
                    return "/Images/pkg_img/DIP.png";
                }
            case "LED":
                {
                    return "/Images/pkg_img/LED.png";
                }
            case "MEMORY":
                {
                    return "/Images/pkg_img/DRAM.png";
                }
            case "BGA":
                {
                    return "/Images/pkg_img/BGA.png";
                }
            case "CHIPCARRIER":
                {
                    return "/Images/pkg_img/PLCC.png";
                }
            case "THROUGHHOLE":
                {
                    return "/Images/pkg_img/TO-220.png";
                }
            case "TRANSISTORDIODE":
                {
                    return "/Images/pkg_img/TO-220.png";
                }
            case "CASON":
                {
                    return "/Images/pkg_img/CASON.png";
                }
            case "SOP":
                {
                    return "/Images/pkg_img/TSSOP.png";
                }
            case "PGA":
                {
                    return "/Images/pkg_img/QFP.png";
                }
            case "SURFACEMOUNT":
                {
                    return "/Images/pkg_img/SOIC.png";
                }
            //Taxonomy
            case "CRYSTALOSC":
                {
                    return "/Images/pkg_img/CO.png";
                }
            case "CONNECTOR":
                {
                    return "/Images/pkg_img/CONNECTOR.png";
                }
            case "RESISTOR":
                {
                    return "/Images/pkg_img/RESISTOR.png";
                }
            case "CAPACITOR":
                {
                    return "/Images/pkg_img/CAPACITOR.png";
                }
            case "INDUCTOR":
                {
                    return "/Images/pkg_img/INDUCTOR.png";
                }
            case "WIRECABLE":
                {
                    return "/Images/pkg_img/WIRECABLE.png";
                }
            case "MULTIPOWERSO":
                {
                    return "/Images/pkg_img/MPSO.png";
                }
            default:
                return DefaultImageString;//Image Not Found
        }
    }



    private string LoadPackageData()
    {
        string ret = null;
        string strPkg = Tools.Strings.SanitizeInput(Request.QueryString["pkg"]);
        if (!string.IsNullOrEmpty(strPkg))//testing override
            return strPkg;
        if (PartDetailDataSet != null)
        {
            if (PartDetailDataSet.Tables["PackageData"] != null)
            {
                if (PartDetailDataSet.Tables["PackageData"].Rows[0].ItemArray.Count() >= 11)//If not here, check 
                {
                    if (PartDetailDataSet.Tables["PackageData"].Rows[0].ItemArray[11].ToString() != "N/A")
                    {
                        ret = SiliconExpertLogic.SearchPackagingDTForPackageType(PartDetailDataSet.Tables["PackageData"].Rows[0].ItemArray[11].ToString());
                    }
                }

            }
            else if (PartDetailDataSet.Tables["PackagingData"] != null)
            {
                if (PartDetailDataSet.Tables["PackagingData"].Rows[0].ItemArray.Count() >= 11)//If not here, check 
                {
                    if (PartDetailDataSet.Tables["PackagingData"].Rows[0].ItemArray[11].ToString() != "N/A")
                    {
                        ret = SiliconExpertLogic.SearchPackagingDTForPackageType(PartDetailDataSet.Tables["PackagingData"].Rows[0].ItemArray[11].ToString());
                    }
                }

            }
            if (ret == null) //Search Taxonomy.
            {
                if (PartDetailDataSet.Tables["SummaryData"].Columns["TaxonomyPath"].Table.Rows.Count > 0)
                {
                    string[] taxonomySearchArr = SiliconExpertLogic.CleanSiliconExpertSearchString(PartDetailDataSet.Tables["SummaryData"].Columns["TaxonomyPath"].Table.Rows[0].ItemArray[9].ToString());
                    string TaxonomySearch = SiliconExpertLogic.SearchTaxonomyDTForPackageType(taxonomySearchArr);
                    ret = TaxonomySearch;//Summary - Taxonomy
                }
            }

            if (ret == null) //Search Taxonomy with Summary Text.
            {

                if (PartDetailDataSet.Tables["SummaryData"].Rows[0].ItemArray.Count() >= 4)
                {
                    string[] descSearchArr = SiliconExpertLogic.CleanSiliconExpertSearchString(PartDetailDataSet.Tables["SummaryData"].Rows[0].ItemArray[4].ToString());
                    string DescriptionSearch = SiliconExpertLogic.SearchTaxonomyDTForPackageType(descSearchArr);
                    ret = DescriptionSearch;//Summary - Taxonomy
                }

            }
            if (ret == null)
            {
                if (PartDetailDataSet.Tables["SummaryData"].Rows[0].ItemArray.Count() >= 9)
                {
                    string[] taxSearchArr = SiliconExpertLogic.CleanSiliconExpertSearchString(PartDetailDataSet.Tables["SummaryData"].Rows[0].ItemArray[9].ToString());
                    string TaxonomySearch = SiliconExpertLogic.SearchTaxonomyDTForPackageType(taxSearchArr);
                    ret = TaxonomySearch;//Summary - Taxonomy
                }
            }


        }
        return ret;
    }

    private string GetCustomImageURL()
    {

        if (!string.IsNullOrEmpty(ImageString))
            return ImageString;

        return null;
    }

    private void LoadOverView()
    {
        //This needs to be the partsearchstring since the se partnumber might be slightly different that what we have it listed as ?pn=TS861ILT returns TS861ILT-E

        //Part Number
        if (!string.IsNullOrEmpty(PagePartNumber))
            lblOverViewPN.Text = PagePartNumber;



        //Load availability String (in stock  / parts available)
        LoadAvailability();



        //Part Details
        lblMPN.Text = PagePartNumber;
        lblMFG.Text = PageManufacturer;
        lblDesc.Text = SeDescription ?? "Not Available.";
        lblPopupPartNumber.Text = PagePartNumber;




        //DataSheet
        if (SeDatasheet == "Not Available")
        {
            hlDataSheet.Text = "Not Available";
            hlDataSheet.NavigateUrl = "#";
            hlDataSheet.Enabled = false;
        }
        else
        {
            hlDataSheet.Text = "View / Download";
            hlDataSheet.NavigateUrl = SeDatasheet;
            hlDataSheet.Target = "_blank";
        }

        //Imagery
        string imageUrl = LoadOverViewImage();
        imgOverview.ImageUrl = imageUrl;
        imgPopup.ImageUrl = imageUrl;

    }



    private void SendMissingImageEmail()
    {
        //Send email if no Image Found        
        //if (Request.IsAuthenticated)
        //    if (Membership.GetUser().UserName.ToLower() != "kevint")
        string subject = PagePartNumber + " is missing an image.  (Search String: " + PartSearchString + ")";
        string url = @"https://portal.sensiblemicro.com/public/search/part_details.aspx?id=" + SeComID;
        string body = @"<b>Part Number</b>: " + PagePartNumber + @"<br />";
        body += @"<b>MFG</b>: " + PageManufacturer + @"<br />";
        body += @"<b>URL</b>: " + url + @"<br />";
        body += @"<b>DESCRIPTION</b>: " + SeDescription + @"<br />";
        body += @"<b>IP</b>: " + NetworkLogic.GetVisitorIpAddress() + @"<br />";
        //tools.SendMail("missing_image@sensiblemicro.com", "ktill@sensiblemicro.com", subject, body, cc );
        tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, subject, body);
    }

    private void SendErrorEmail(Exception ex)
    {
        string body = "";
        string URL = Request.Url.AbsoluteUri.ToString();
        string ip = NetworkLogic.GetVisitorIpAddress();
        string part = PagePartNumber + "Search String = " + PartSearchString;
        StringBuilder sb = new StringBuilder();
        sb.Append("URL: " + URL + "<br />");
        sb.Append("IP: " + ip + "<br />");
        sb.Append("Part: " + part + "<br />");
        sb.Append("<br />");
        sb.Append("Detail: ");
        sb.Append("<br />");
        sb.Append("<br />");
        body = sb.ToString();
        body += "<br /><br />";


        body += ex.Message;
        body += "<br /><br />";
        if (ex.Message.Contains("does not belong to table SummaryData"))
            body += "(" + SePartNumber + ") Column missing from datatable.  Partstring = " + PartSearchString;
        else if (ex.Message.Contains("A field or property with the name"))
            body += "Property Mismatch - Silicon Expert datatable";

        tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Part Detail Error Detected", body);

    }

    private void LoadSpecifications()
    {

        //PartNumber: LNJ347W83RA
        //Manufacturer: Panasonic
        //PlName: LEDs  
        //Description: LED Uni-Color Green 575nm 2-Pin Chip LED
        //Datasheet: https://industrial.panasonic.com/content/data/SC/ds/ds4/LNJ347W83RA_E_discon.pdf	
        //Lifecycle: Obsolete
        //RoHS: Yes
        //RoHSVersion: 2011/65/EU, 2015/863	
        lblSpecPartNumber.Text = PagePartNumber;
        lblSpecMfg.Text = PageManufacturer;
        lblSpecPlName.Text = SePlName;
        lblSpecDescription.Text = SeDescription;
        if (SeDatasheet == "Not Available")
        {
            hlSpecDatasheet.Text = "Not Available";
            hlSpecDatasheet.NavigateUrl = "#";
        }
        else
        {
            hlSpecDatasheet.Text = "View / Download";
            hlSpecDatasheet.NavigateUrl = SeDatasheet;
            hlSpecDatasheet.Target = "_blank";
        }

        lblSpecLifecycle.Text = SeLifecycle;
        lblSpecRoHS.Text = SeRoHS;
        lblSpecRoHSVersion.Text = SeRoHSVersion;



    }

    private void LoadEnvironment()
    {
        dtvEnvironment.Visible = false;
        divEnvReg.Visible = true;
        if (!ShowSiliconExpertDetails)
            return;

        dtvEnvironment.Visible = true;
        divEnvReg.Visible = false;
        DataTable EnvDataTable = null;
        dtvEnvironment.DataSource = null;

        if (PartDetailDataSet != null)
        {
            EnvDataTable = PartDetailDataSet.Tables["EnvironmentalDto"];
            if (EnvDataTable != null)
                dtvEnvironment.DataSource = EnvDataTable;


        }
        dtvEnvironment.DataBind();

    }

    private void CheckLoadSiliconExpetDetails()
    {
        ShowSiliconExpertDetails = false;
        if (Request.IsAuthenticated)
            ShowSiliconExpertDetails = true;
        else if (ComID > 0)
            ShowSiliconExpertDetails = true;
        else if (HubDbPart != null)
            ShowSiliconExpertDetails = true;
        else if (HubDbAdwordPart != null)
            ShowSiliconExpertDetails = true;
        else if (SkipCaptcha)
            ShowSiliconExpertDetails = true;
        else if (IsAuthorizedCampaign())
            ShowSiliconExpertDetails = true;

    }

    private bool IsAuthorizedCampaign()
    {
        if (!string.IsNullOrEmpty(utm_campaign))
        {
            if (SM_Security.authorized_search_campaignList.Contains(utm_campaign))
                return true;
        }
        return false;
    }

    private void CheckSkipCaptcha()
    {
        //Authenticated
        if (Request.IsAuthenticated)
            SkipCaptcha = true;


    }


    protected class PcnResult
    {
        public string PCNNumber { get; set; }
        public string TypeOfChange { get; set; }
        public string DescriptionOfChange { get; set; }
        public string PcnSource { get; set; }
        public DateTime NotificationDate { get; set; }
    }

    private void LoadPCN()
    {
        divPCNReg.Visible = true;
        gvPcnData.Visible = false;
        gvPcnData.DataSource = null;

        if (!ShowSiliconExpertDetails)
            return;


        divPCNReg.Visible = false;
        gvPcnData.Visible = true;
        DataTable dtPCN = null;
        //gvPcnData.DataSource = se.GetPcnDataTableXML(sePart.ComID, sePart.partNumber, User.UserName);
        if (PartDetailDataSet != null)
        {
            dtPCN = PartDetailDataSet.Tables["PCNDto"];
            if (dtPCN != null)
            {
                //For PCN, 1st row is the "Latest" info.Remove that rown, and just return the child rows in reverse date order
                DataRow latestPcn = dtPCN.Rows[0];
                dtPCN.Rows.Remove(latestPcn);
                List<PcnResult> listPcnResults = new List<PcnResult>();
                foreach (DataRow row in dtPCN.Rows)
                {
                    PcnResult pcr = new PcnResult();
                    pcr.PCNNumber = "N/A";
                    if (!string.IsNullOrEmpty(row.Field<string>("PCNNumber")))
                        pcr.PCNNumber = row.Field<string>("PCNNumber");
                    pcr.TypeOfChange = row.Field<string>("TypeOfChange");
                    pcr.DescriptionOfChange = row.Field<string>("DescriptionOfChange");
                    pcr.PcnSource = row.Field<string>("PcnSource");
                    //string strNotificationDate = "N/A";
                    DateTime? notificationDate = null;
                    if (!string.IsNullOrEmpty(row.Field<string>("NotificationDate")))
                        notificationDate = DateTime.Parse(row.Field<string>("NotificationDate"));
                    if (notificationDate != null)
                        pcr.NotificationDate = notificationDate.Value;
                    listPcnResults.Add(pcr);


                }

                var query = listPcnResults.Where(w => (w.TypeOfChange ?? "").Length > 0).OrderByDescending(o => o.NotificationDate).Select(s => new
                {
                    //PCNNumber can sometimes be blank in the datatable
                    PCNNumber = s.PCNNumber,
                    TypeOfChange = s.TypeOfChange,
                    DescriptionOfChange = s.DescriptionOfChange,
                    PcnSource = s.PcnSource,
                    NotificationDate = s.NotificationDate.ToShortDateString(),


                });

                gvPcnData.DataSource = query;

            }
        }

        gvPcnData.DataBind();


    }

    private void LoadLifecycle()
    {
        divLifecycleReg.Visible = true;
        dtvLifecycle.Visible = false;
        if (!ShowSiliconExpertDetails)
            return;
        divLifecycleReg.Visible = false;
        dtvLifecycle.Visible = true;

        dtvLifecycle.DataSource = null;

        if (PartDetailDataSet != null)
        {
            DataTable dtLifeCycle = PartDetailDataSet.Tables["LifeCycleData"];
            if (dtLifeCycle != null)
                dtvLifecycle.DataSource = dtLifeCycle;
        }

        dtvLifecycle.DataBind();
    }


    //private void MakeGvHeadersBold(GridViewRow row)
    //{
    //    row.Cells[0].Style.Add("font-weight", "700");
    //}

    //Make DetailsView Headers Bold
    private void MakeDTVHeadersBold(DetailsViewRow row)
    {
        row.Cells[0].Style.Add("font-weight", "700");
    }

    //Make DetailsView Headers Bold
    private void MakeGVHeadersBold(GridViewRow row)
    {
        row.Cells[0].Style.Add("font-weight", "700");
    }



    //Common dtv Databound Logic
    protected void dtv_DataBound(object sender, EventArgs e)
    {
        var dv = sender as DetailsView;
        if (dv.Rows.Count > 0)
        {
            foreach (DetailsViewRow row in dv.Rows)
            {
                foreach (DataControlFieldCell cell in row.Cells)
                {

                    //if (!(cell.ContainingField is TemplateField))
                    //{
                    if (string.IsNullOrEmpty(cell.Text) || cell.Text == "&nbsp;")
                        cell.Text = "Not Available";
                    //}

                }
            }
        }
    }

    //Common Gridview Databound Logic
    protected void gv_DataBound(object sender, EventArgs e)
    {
        var gv = sender as GridView;
        if (gv.Rows.Count > 0)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                MakeGVHeadersBold(row);
                foreach (DataControlFieldCell cell in row.Cells)
                {
                    if (!(cell.ContainingField is TemplateField))
                    {
                        cell.Text = SetEmptyCellData(cell);

                    }

                }
            }
        }
    }

    private string SetEmptyCellData(DataControlFieldCell cell)
    {
        string defaultText = "Not Available";

        if (string.IsNullOrEmpty(cell.Text))
            return defaultText;
        if (cell.Text == "&nbsp;")
            return defaultText;
        if (cell.Text == "%")
            return defaultText;

        return cell.Text;
    }

    //PCN Gridview DataBound
    protected void gvPcnData_DataBound(object sender, EventArgs e)
    {
        //Fire Common Databound Event
        gv_DataBound(sender, e);
        foreach (GridViewRow row in gvPcnData.Rows)
        {
            //Unlike Details View, I can't find a DataView to reference to set TemplateField's Hyperlink URL, e.Row.Cells[3] is always "" after databind, probably because of the templatefield.
            //So, amd converting that to a boundfield, and adding a control server-side.
            string pcnSourceUrl = row.Cells[3].Text;
            HyperLink hl = new HyperLink();
            hl.ID = "hlPcnSource";
            SeTDetailsViewHyperlinkProperties(hl, pcnSourceUrl);
            row.Cells[3].Text = "";
            row.Cells[3].Controls.Add(hl);
        }
    }


    //Details Specifications DataBound 
    protected void dtvSpecs_DataBound(object sender, EventArgs e)
    {
        //Fire Common Databound Event
        dtv_DataBound(sender, e);
        var dv = sender as DetailsView;
        DataRowView rv = dv.DataItem as DataRowView;
        string specDataSheetUrl = rv["Source"].ToString();
        HyperLink hl = (HyperLink)dv.FindControl("hlSpecDataSheet");
        if (hl != null)
            SeTDetailsViewHyperlinkProperties(hl, specDataSheetUrl);

    }


    //DetailsView Lifecycle DataBound
    protected void dtvLifecycle_DataBound(object sender, EventArgs e)
    {
        //Fire Common Databound Event
        dtv_DataBound(sender, e);
        var dv = sender as DetailsView;
        if (dv.DataItem == null)
            return;
        DataRowView rv = dv.DataItem as DataRowView;
        string lifeCycleSourceUrl = rv["Source"].ToString();
        HyperLink hl = (HyperLink)dv.FindControl("hlLifecycleSource");
        if (hl != null)
            SeTDetailsViewHyperlinkProperties(hl, lifeCycleSourceUrl);

    }
    //DetailsView Environment DataBound
    protected void dtvEnvironment_DataBound(object sender, EventArgs e)
    {
        //Fire Common Databound Event
        dtv_DataBound(sender, e);
        var dv = sender as DetailsView;
        if (dv.DataItem == null)
            return;
        DataRowView rv = dv.DataItem as DataRowView;
        string conflictUrl = rv["Source"].ToString();
        string rohsSourceUrl = rv["Source"].ToString(); ;
        HyperLink hl = (HyperLink)dv.FindControl("hlConflictStatement");
        if (hl != null)
            SeTDetailsViewHyperlinkProperties(hl, conflictUrl);
        hl = (HyperLink)dv.FindControl("hlRohsSource");
        if (hl != null)
            SeTDetailsViewHyperlinkProperties(hl, rohsSourceUrl);

    }



    //Dynamic Hyperlink for Datasheet
    private void SeTDetailsViewHyperlinkProperties(HyperLink hl, string url)
    {

        hl.Text = "Not Available";
        hl.Target = "_blank";
        string navigateUrl = url.Trim().ToLower();
        string hlID = hl.ID;
        if (!string.IsNullOrEmpty(navigateUrl))
        {
            hl.Text = "View / Download";
            hl.NavigateUrl = navigateUrl;
        }

    }
}