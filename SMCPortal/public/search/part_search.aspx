﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="part_search.aspx.cs" Inherits="public_part_search_search" Async="true" %>

<%@ Register Src="~/assets/controls/quote_form.ascx" TagPrefix="uc1" TagName="quote_form" %>

<asp:Content ID="HeadContent1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--Part Search CSS:--%>
    <%--Search Results--%>
    <%-- <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9497943201/1587672402157/module_9497943201_bz-portal-search-results.min.css"> --%>
    <%--<link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9497943201/1592420090409/module_9497943201_bz-portal-search-results.min.css">--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9497943201/1592421018531/module_9497943201_bz-portal-search-results.min.css">
    <%--Related Results--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1557339400809/module_9502989582_Sensible_Micro_-_Early_2019_Custom_Modules_bz-portal-search-results-related.min.css">
    <%--Certifications--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1564517078299/module_9561873698_Sensible_Micro_-_Early_2019_Custom_Modules_bz-portal-certs.min.css">
    <style>
        .panel-title {
            margin: 0px;
            padding-bottom: 10px;
        }

        .register-title {
            text-align: center;
        }

        .results-title {
            font-size: 18px;
            color: rgb(238, 38, 55);
        }

        .card-header {
            padding: 0px;
        }

        .card-body {
            padding: 0px;
        }

        .portal-title {
            font-size: 2.8em;
        }

        .gv-pcn td {
            text-align: left;
        }

        .gv-datasheet {
            text-align: center;
        }

            .gv-datasheet span {
                font-size: 27px;
            }



        /*.quote-button {
            vertical-align: middle;
            color: #ffffff;
            font-weight: bold;
            max-height: 34px;
            border-radius: 0px;
        }*/
    </style>


    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/1878634.js"></script>
    <!-- End of HubSpot Embed Code -->
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent1" runat="Server">





    <asp:HiddenField ID="hfIP" runat="server" />
    <asp:HiddenField ID="hfUrl" runat="server" />

    <div class="section-container__narrow padding-top__large padding-bottom__medium section-header__container">
        <div class="align__center">
            <h1 class="main-title relative" style="font-size: 2.5em;">Millions of Components at Your Fingertips
            </h1>
            <asp:Panel ID="pnlPortalSearch" runat="server" DefaultButton="lbPartSearch">
                <div class="portal-search">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="portal-search__field" placeholder="Search by part number" name="ppn"></asp:TextBox>
                    <asp:LinkButton ID="lbPartSearch" runat="server" CssClass="portal-search__button" title="search" OnClick="lbPartSearch_Click" OnClientClick="SendGAEvent('online_quote');" />
                </div>
            </asp:Panel>



            <div class="search-results-container">
                <!-- unmodified search result html -->
                <asp:Panel ID="pnlResults" runat="server" Visible="false">


                    <div class="card" style="margin-top: 5px;">
                        <div class="card-header">
                            <h3 class="panel-title">
                                <asp:Label ID="lblResultsTitle" runat="server" CssClass="results-title"></asp:Label>
                            </h3>
                        </div>


                        <div class="card-body" style="overflow-x: auto;">
                            <asp:GridView ID="gvResults" runat="server" CssClass="table table-hover table-striped tablesorter" GridLines="None" AutoGenerateColumns="false" DataKeyNames="ComID" CellPadding="5" OnSelectedIndexChanged="gvResults_SelectedIndexChanged" OnRowDataBound="gvResults_RowDataBound">

                                <EmptyDataTemplate>
                                    <div class="intro">
                                        <p class="text-center">No results found.</p>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="PartNumber" HeaderText="PartNumber" SortExpression="PartNumber" HtmlEncode="false" />

                                    <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" SortExpression="Manufacturer" />
                                    <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                    <asp:BoundField DataField="Datasheet" HeaderText="Datasheet" SortExpression="Datasheet" />
                                    <asp:TemplateField SortExpression="lifecycle" HeaderText="Lifecycle">
                                        <HeaderStyle Width="80" />
                                        <ItemStyle Width="80" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbLifecycle" runat="server" CssClass="btn-smc btn-smc-danger btn-block btn-search-detail" OnClick="lbLifecycle_Click" OnClientClick="ShowUpdateProgress();"><span class="fa fa-heartbeat"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField SortExpression="pcn" HeaderText="PCN">
                                        <HeaderStyle Width="80" />
                                        <ItemStyle Width="80" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbPcn" runat="server" CssClass="btn-smc btn-smc-pcn btn-block btn-search-detail" OnClick="lbPcn_Click" OnClientClick="ShowUpdateProgress();"><span  class="fa fa-microchip"></span></asp:LinkButton>
                                            </button>
                                        </ItemTemplate>
                                        <ItemStyle Wrap="True" />
                                    </asp:TemplateField>

                                    <asp:TemplateField SortExpression="env" HeaderText="ENV">
                                        <HeaderStyle Width="80" />
                                        <ItemStyle Width="80" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEnv" runat="server" CssClass="btn-smc btn-smc-danger btn-block btn-search-detail" OnClick="lbEnv_Click" OnClientClick="ShowUpdateProgress();"><span class="fa fa-leaf"></span></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Wrap="True" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
                <!-- end unmodified search result html -->



            </div>

        </div>
    </div>
    <%--End Results Panel--%>


    <%-- Quote Modal--%>

    <uc1:quote_form runat="server" ID="quote_form" />

    <%-- END Quote Modal--%>

    <!-- Details Modal -->
    <div class="modal fade" id="divModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header ">
                    <h4 class="modal-title">
                        <asp:Label ID="lblDetailType" runat="server"></asp:Label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default" style="margin-top: 5px;">

                        <%--  Modal Register Panel--%>
                        <asp:Panel ID="pnlRegister" runat="server" CssClass="register-title">

                            <div>
                                <h5>Register for a free account, and get access to this and much more!</h5>
                                <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                    <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                        <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                        <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                            <a href="/login.aspx" onclick="ShowUpdateProgress();">
                                                <input type="button" value="Register" class="hs-button primary btn-block" onclick="SetPartNumber(this);">
                                            </a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>
                        <%-- End Modal Register Panel--%>

                        <%-- Modal Detail Panels--%>
                        <div style="text-align: center; height: 100%;">
                            <%--PCN Gridview--%>
                            <asp:GridView ID="gvPcnData" runat="server" CssClass="table table-hover table-striped gv-pcn" GridLines="None" AutoGenerateColumns="false" OnRowDataBound="gv_RowDataBound">
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning" style="text-align: center;">
                                        <h5>No PCN Data found for search term.</h5>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="LastPCNDate" HeaderText="Date" SortExpression="LastPCNDate" DataFormatString="{0:d}" HeaderStyle-Width="120px" />
                                    <asp:BoundField DataField="LastPCNNumber" HeaderText="Source" SortExpression="LastPCNNumber" />
                                    <asp:BoundField DataField="LastPCNType" HeaderText="Type" SortExpression="LastPCNType" HeaderStyle-Width="300px" />
                                    <asp:BoundField DataField="LastPCNSource" HeaderText="Description" SortExpression="LastPCNSource" HeaderStyle-Width="500px" />
                                    <asp:BoundField DataField="LastEOLSource" HeaderText="Description" SortExpression="LastEOLSource" />

                                </Columns>
                            </asp:GridView>
                            <%--END PCN Gridview--%>

                            <%--LifeCycle dtv--%>

                            <asp:DetailsView ID="dtvLifecycleData" runat="server" CssClass="table table-hover table-striped" GridLines="None" EmptyDataText="No Lifecycle data found." AutoGenerateRows="false" OnDataBound="dtv_DataBound">
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning" style="text-align: center;">
                                        <h5>No Lifecycle Data found for search term.</h5>
                                    </div>
                                </EmptyDataTemplate>
                                <Fields>
                                    <asp:BoundField DataField="PartStatus" HeaderText="Current Lifecycle Status:" SortExpression="PartStatus" />
                                    <asp:BoundField DataField="LTBDate" HeaderText="LTB Date:" SortExpression="LTBDate" />
                                    <asp:BoundField DataField="EstimatedYearsToEOL" HeaderText="Est. Years until EOL" SortExpression="EstimatedYearsToEOL" />
                                    <asp:BoundField DataField="EstimatedEOLDate" HeaderText="Est. EOL Date" SortExpression="EstimatedEOLDate" />
                                    <asp:BoundField DataField="PartLifecycleStage" HeaderText="Lifecycle Stage:" SortExpression="PartLifecycleStage" />
                                    <asp:BoundField DataField="MinimumEstimatedYearsToEOL" HeaderText="Min. Years until EOL:" SortExpression="ExemptionType" />
                                    <asp:BoundField DataField="MaximumEstimatedYearsToEOL" HeaderText="Max. Years until EOL:" SortExpression="MaximumEstimatedYearsToEOL" />
                                    <asp:BoundField DataField="LifeCycleRiskGrade" HeaderText="Lifecycle Risk Grade" SortExpression="LifeCycleRiskGrade" />
                                    <asp:BoundField DataField="OverallRisk" HeaderText="Overall Risk Grade" SortExpression="OverallRisk" />
                                </Fields>
                            </asp:DetailsView>
                            <%--END LifeCycle dtv--%>



                            <%-- Environmental dtv--%>
                            <%--<uc1:sm_datatable runat="server" ID="smdtEnv" />--%>

                            <asp:DetailsView ID="dtvEnvData" runat="server" CssClass="table table-hover table-striped" GridLines="None" AutoGenerateRows="false" OnDataBound="dtvEnvData_DataBound">
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning" style="text-align: center;">
                                        <h5>No Environmental Data found for search term.</h5>
                                    </div>
                                </EmptyDataTemplate>
                                <Fields>
                                    <asp:BoundField DataField="ConflictMineralStatus" HeaderText="Conflict Mineral Status:" SortExpression="ConflictMineralStatus" />
                                    <asp:BoundField DataField="ConflictMineralStatement" HeaderText="Conflict Mineral Statment:" SortExpression="ConflictMineralStatement" />
                                    <%--  <asp:TemplateField SortExpression="ConflictMineralStatement" HeaderText="Conflict Mineral Statement:">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlConflictMineralStatement" runat="server" Text="View / Download" Target="_blank"></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle Wrap="True" />
                                    </asp:TemplateField>--%>
                                    <asp:BoundField DataField="Source" HeaderText="Source:" SortExpression="Source" />
                                    <asp:BoundField DataField="LeadFree" HeaderText="LeadFree:" SortExpression="LeadFree" />
                                    <asp:BoundField DataField="RoHSStatus" HeaderText="RoHSStatus:" SortExpression="RoHSStatus" />
                                    <asp:BoundField DataField="RoHSVersion" HeaderText="RoHSVersion:" SortExpression="RoHSVersion" />
                                    <asp:BoundField DataField="SourceType" HeaderText="Source Type:" SortExpression="SourceType" />
                                    <asp:BoundField DataField="Exemption" HeaderText="Exemption:" SortExpression="Exemption" />
                                    <asp:BoundField DataField="ExemptionType" HeaderText="Exemption Type:" SortExpression="ExemptionType" />
                                    <asp:BoundField DataField="LeadFree" HeaderText="Lead Free:" SortExpression="LeadFree" />
                                    <asp:BoundField DataField="EICCMembership" HeaderText="EICC Membership:" SortExpression="EICCMembership" />
                                </Fields>
                            </asp:DetailsView>
                            <%--END Environmental dtv--%>


                            <%-- Example Image--%>
                            <asp:Image ID="imgExample" runat="server" CssClass="img img-responsive img-thumbnail" Style="width: 100%" />

                        </div>
                        <%--End Modal Detail Panels--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Details Modal -->




</asp:Content>



<%--Certifications Panel--%>
<asp:Content ID="cpCertifications" ContentPlaceHolderID="SubMain" runat="Server">






    <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
        <div class="row-fluid ">
            <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
                <div id="hs_cos_wrapper_module_1557509487380601" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">



                    <section class=" image-row__wrapper padding-bottom__large">

                        <%--  <div class="background__grey">--%>
                        <div class="background__white">
                            <div class="section-container__extreme-narrow padding-top__large padding-bottom__medium section-header__container">
                                <div class="main-content align__center">
                                    <h2 class="relative portal-title">Certified Quality by Accredited Organizations
                                    </h2>
                                    <div class="large-paragraph">
                                        <p>Our customers are building the critical infrastructure that our society relies on, so having a certified due diligence approach to quality sourcing and inspection is a must.</p>
                                    </div>
                                </div>
                            </div>

                            <!-- certs -->

                            <div class="section-container__narrow image-row__container cert-container">




                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=200&amp;name=IDEA-QMS-9090-seal.jpeg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=100&amp;name=IDEA-QMS-9090-seal.jpeg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=200&amp;name=IDEA-QMS-9090-seal.jpeg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=300&amp;name=IDEA-QMS-9090-seal.jpeg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=400&amp;name=IDEA-QMS-9090-seal.jpeg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=500&amp;name=IDEA-QMS-9090-seal.jpeg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/IDEA-QMS-9090-seal.jpeg?width=600&amp;name=IDEA-QMS-9090-seal.jpeg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=200&amp;name=CCAP-LOGO.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=100&amp;name=CCAP-LOGO.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=200&amp;name=CCAP-LOGO.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=300&amp;name=CCAP-LOGO.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=400&amp;name=CCAP-LOGO.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=500&amp;name=CCAP-LOGO.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/CCAP-LOGO.jpg?width=600&amp;name=CCAP-LOGO.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=200&amp;name=NSF-9001.jpeg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=100&amp;name=NSF-9001.jpeg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=200&amp;name=NSF-9001.jpeg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=300&amp;name=NSF-9001.jpeg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=400&amp;name=NSF-9001.jpeg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=500&amp;name=NSF-9001.jpeg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NSF-9001.jpeg?width=600&amp;name=NSF-9001.jpeg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=200&amp;name=AS9120.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=100&amp;name=AS9120.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=200&amp;name=AS9120.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=300&amp;name=AS9120.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=400&amp;name=AS9120.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=500&amp;name=AS9120.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/AS9120.jpg?width=600&amp;name=AS9120.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=200&amp;name=NQA_ESDS20.20.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=100&amp;name=NQA_ESDS20.20.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=200&amp;name=NQA_ESDS20.20.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=300&amp;name=NQA_ESDS20.20.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=400&amp;name=NQA_ESDS20.20.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=500&amp;name=NQA_ESDS20.20.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NQA_ESDS20.20.jpg?width=600&amp;name=NQA_ESDS20.20.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>





                                <a href="https://www.sensiblemicro.com/certifications">


                                    <div class="image-row__item style__opaque-full-color">

                                        <img src="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=200&amp;name=NEWEraiMember_Logo.jpg" alt="" width="200" srcset="https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=100&amp;name=NEWEraiMember_Logo.jpg 100w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=200&amp;name=NEWEraiMember_Logo.jpg 200w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=300&amp;name=NEWEraiMember_Logo.jpg 300w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=400&amp;name=NEWEraiMember_Logo.jpg 400w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=500&amp;name=NEWEraiMember_Logo.jpg 500w, https://www.sensiblemicro.com/hs-fs/hubfs/2019/Cert%20Logos/NEWEraiMember_Logo.jpg?width=600&amp;name=NEWEraiMember_Logo.jpg 600w" sizes="(max-width: 200px) 100vw, 200px">
                                    </div>


                                </a>




                            </div>

                            <!-- END certs -->

                        </div>

                    </section>
                </div>

            </div>
            <!--end widget-span -->
        </div>
        <!--end row-->
    </div>



    <div class="row-fluid-wrapper row-depth-1 row-number-5 ">
        <div class="row-fluid ">
            <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
                <div id="hs_cos_wrapper_module_1557320680122665" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">

                    <div class="background__blue offer-section">

                        <div class="section-container__narrow padding-top__medium padding-bottom__medium section-header__container">
                            <div class="main-content align__center">
                                <h2 class="relative">Expand Your Results
                                </h2>
                                <div class="paragraph-large">
                                    <p>There’s even more in our customer portal. Get additional resources and info about your search queries when you log in.</p>
                                    <p>Inside you’ll find conflict mineral alerts, lifecycle information, PCNs and much more ― 100% free of charge.</p>
                                    <p><a href="/login.aspx" target="_blank" class="hs-button primary">Sign In</a>          <a href="/Account/register.aspx" target="_blank" class="hs-button primary">New Account</a></p>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!--end widget-span -->
        </div>
        <!--end row-->
    </div>
</asp:Content>
<%--End Certifications Panel--%>




