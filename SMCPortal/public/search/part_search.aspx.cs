﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class public_part_search_search : Page
{
    //silicon_expert si = new silicon_expert();
    //silicon_expert.siPart siPart = new silicon_expert.siPart();
    SM_Tools tools = new SM_Tools();
    RzTools.parts rzp = new RzTools.parts();
    RzTools rzt = new RzTools();
    //SMCPortalDataContext pdc = new SMCPortalDataContext();
    //List<partrecord> RzMatchList = new List<partrecord>();
    //long RzCurrentQty = 0;
    //DataTable dtGridResults = new DataTable();

    MembershipUser User;
    string SearchPart;//The URLDecoded (Human recognizable) version of the part.
    string QueryPart;//The URLEncoded version of the part
    public string SelectedPart; //Must be public fro %EVAL% to work
    string Mfg;
    //string Des;
    string UserName;
    company Comp;

    //Not Available Text
    string naTExt = "Not Available";

    protected DataTable dtGridResults
    {
        get
        {
            return (DataTable)ViewState["dtGridResults"];
        }
        set
        {
            ViewState["dtGridResults"] = value;
        }
    }


    protected DataSet SiliconExpertDataSet
    {
        get
        {
            return (DataSet)ViewState["SiliconExpertDataSet"];
        }
        set
        {
            ViewState["SiliconExpertDataSet"] = value;
        }
    }


    protected DataTable PcnDataTable
    {
        get
        {

            return (DataTable)ViewState["pcnDataTable"];
        }
        set
        {
            ViewState["pcnDataTable"] = value;
        }
    }

    protected DataTable EnvDataTable
    {
        get
        {

            return (DataTable)ViewState["EnvDataTable"];
        }
        set
        {
            ViewState["EnvDataTable"] = value;
        }
    }

    protected DataTable LifecycleDataTable
    {
        get
        {

            return (DataTable)ViewState["LifecycleDataTable"];
        }
        set
        {
            ViewState["LifecycleDataTable"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        ((hs_flex)this.Master).ToggleTitleDiv(false);
        ((hs_flex)this.Master).ToggleBannerDiv(false);

        try
        {

            hfIP.Value = NetworkLogic.GetVisitorIpAddress();
            hfUrl.Value = Request.RawUrl;
            QueryPart = Request.QueryString["pn"];
            QueryPart = HttpUtility.UrlDecode(QueryPart);//Get Encoded part from QueryString
            SearchPart = Tools.Strings.SanitizeInput(QueryPart);//Decode part from QueryString

            if (Page.IsPostBack)
            {
                //If postback, then because of click, therefore use Txtbox for new URL, then redirect to that URL               
                SearchPart = txtSearch.Text;
            }
            else
            {
                //Else it's a new load, and the URL should have the updated part, use that to update Textbox on load.                
                txtSearch.Text = SearchPart;
                SetCustomerEmail();
            }

            bool partSearchDisabled = !SiliconExpertLogic.IsEnabled();
            if (partSearchDisabled)
            {
                tools.HandleResult("warning", "Our part search subsystem is currently undergoing maintenance.  Please try again later.", 1000000);
                return;
            }




            //Authenticated users don't need to do catpcha
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                //If user can answer captcha, give them 15 minutes of searching before re-check
                bool SkipCaptcha = tools.CheckCaptchaByMuniutesPerDay(15);
                if (!SkipCaptcha)
                    return;
            }

            doPartSearch();

            //Update the meta tag when all variables calculated.
            Page.MetaDescription = "Part Search Results for: " + SearchPart;
        }
        catch (Exception ex)
        {


            if (ex.Message == "Thread was being aborted.")
                return;
            //Log Events
            else if (ex.Message.Contains("You have performed too many searches"))
                tools.HandleResult("fail", ex.Message, 300000);
            else if (ex.Message == "We are currently undergoing maintenance, please try again later.")
                tools.HandleResult("fail", ex.Message, 300000);

            //Skip Email for ad campaign - bot related likely.
            if (IsAddCampaign())
                tools.HandleResult("warning", "Error related to an add campaign, likely bot related.");
            else
                tools.HandleResult("fail", "Something went wrong and the support team has been alerted.  Please try again later.");
            SendErrorEmail(ex);


        }
    }

    private bool IsAddCampaign()
    {
        string oddCampaign = "PPC_2021";
        string urlCampaign = Request.QueryString["utm_campaign"];
        if (string.IsNullOrEmpty(urlCampaign))
            return false;
        if (urlCampaign.Contains(oddCampaign))
            return true;
        return false;
    }

    private void SendErrorEmail(Exception ex)
    {

        string body = "";
        string URL = Request.Url.AbsoluteUri.ToString();
        string ip = NetworkLogic.GetVisitorIpAddress();
        string part = SearchPart ?? "<Part Not Identified>";

        StringBuilder sb = new StringBuilder();
        sb.Append("URL: " + URL + "<br />");
        sb.Append("IP: " + ip + "<br />");
        sb.Append("Part: " + part + "<br />");
        sb.Append("<br />");
        sb.Append("Detail: ");
        sb.Append("<br />");
        sb.Append("<br />");
        body = sb.ToString();
        body += "<br /><br />";


        string innerException = "<br /><br />Inner Exception:<br /> " + ex.InnerException + " <br /><br />Stack Trace:+<br />" + ex.StackTrace;
        string fullException = ex.Message + innerException;


        body += fullException;
        body += "<br /><br />";

        tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Part Search Error Detected", body);

    }

    private void SetCustomerEmail()
    {
        if ((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated)
        {//If user is valid and authenticated, check if username is valid email, autofill if so.
            MembershipUser u = Membership.GetUser();
            string email = u.UserName;
            if (tools.ValidateEmailAddress(email))
                quote_form.CustomerEmail = email;
        }
    }

    private void ValidatePart()
    {
        if (SearchPart.Trim().Length > 50)
            throw new Exception("Too many characters entered.  Please check your part number and try again.");
    }

    protected void doPartSearch()
    {


        //if (si.AllowedToPartSearch("list"))
        if (string.IsNullOrEmpty(SearchPart))
            return;

        //trim leading Zeroes
        //SearchPart = SearchPart.TrimStart(new Char[] { '0' });

        if (SearchPart.Length < 4)
        {
            tools.HandleResult("fail", "Please provide more than 3 characters of the part number.");
            return;
        }
        //Search Silicon Expert
        //Re-Bind the DataSet

        SiliconExpertDataSet = SiliconExpertLogic.GetListPartSearchDataSetJSON_Synchronous(SearchPart, true);
        dtGridResults = buildResultsDataTable();
        if (SiliconExpertDataSet != null)
        {
            BindResultsFromSiliconExpertDataSet();
        }
        else
        {
            BindResultsNoMatch();
        }

        if (gvResults.Rows.Count > 0)
        {
            gvResults.UseAccessibleHeader = true;
            gvResults.HeaderRow.TableSection = TableRowSection.TableHeader;
            gvResults.FooterRow.TableSection = TableRowSection.TableFooter;
        }

        GetUserAndCompany();
        setShowDemoData();

        //Keep records of all unique ComID's returned by SE
        //Keep log of search context in Rz (username, part searched, ip, etc)
        if (Membership.GetUser() == null)
            rzt.LogPortalSearchedPart(UserName, SearchPart, Mfg ?? "not found", "not found", Comp);
        else
            rzt.LogPortalSearchedPart(User, SearchPart, Mfg ?? "not found", "not found", Comp);

        pnlResults.Visible = true;


    }

    private void BindResultsNoMatch()
    {

        DataRow gridRow = dtGridResults.NewRow();
        string partNumber = SearchPart;

        gridRow["PartNumber"] = partNumber;
        gridRow["Manufacturer"] = naTExt;
        gridRow["Description"] = "No description available.";
        gridRow["Qty"] = "Sourcing";
        gridRow["Datasheet"] = string.Empty;
        gridRow["ComId"] = "";
        //These Results come from a DetailSearch.
        //gridRow["lifecycle"] = row["lifecycle"];
        //gridRow["pcn"] = row["pcn"];
        //gridRow["PartNumber"] = row["PartNumber"];
        //gridRow["env"] = row["env"];
        dtGridResults.Rows.Add(gridRow);


        gvResults.DataSource = dtGridResults;
        lblResultsTitle.Text = "Search Results";
        gvResults.DataBind();

    }

    private DataTable buildResultsDataTable()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PartNumber", typeof(string));
        dt.Columns.Add("Manufacturer", typeof(string));
        dt.Columns.Add("Description", typeof(string));
        dt.Columns.Add("Qty", typeof(string));
        dt.Columns.Add("datasheet", typeof(string));
        dt.Columns.Add("lifecycle", typeof(string));
        dt.Columns.Add("pcn", typeof(string));
        dt.Columns.Add("env", typeof(string));
        dt.Columns.Add("ComId", typeof(string));
        return dt;
    }


    private void BindResultsFromSiliconExpertDataSet()
    {
        //Get the silicon Expert DataTable
        //RzMatchList = PartLogic.GetRzPartMatchList(SearchPart);

        foreach (DataRow row in SiliconExpertDataSet.Tables["PartDto"].Rows)
        {
            DataRow gridRow = dtGridResults.NewRow();
            string partNumber = row["PartNumber"].ToString();
            gridRow["PartNumber"] = partNumber;
            gridRow["Manufacturer"] = row["Manufacturer"];
            gridRow["Description"] = row["Description"];
            gridRow["Qty"] = "Sourcing";
            gridRow["Datasheet"] = row["Datasheet"];
            gridRow["ComId"] = row["ComId"];
            //These Results come from a DetailSearch.
            //gridRow["lifecycle"] = row["lifecycle"];
            //gridRow["pcn"] = row["pcn"];
            //gridRow["PartNumber"] = row["PartNumber"];
            //gridRow["env"] = row["env"];
            dtGridResults.Rows.Add(gridRow);
        }

        gvResults.DataSource = dtGridResults;
        lblResultsTitle.Text = "Search Results";
        gvResults.DataBind();



    }


    private void ShowUndergoindMaintenance()
    {
        tools.HandleResult("fail", "We are currently undergoing maintenance, please try again later.", 300000);
        //throw new NotImplementedException();
    }

    private DataTable RemovePoorMatchesFromDataTable(DataTable dt)
    {
        //Clone structure of PartDto
        DataTable dtGoodMatches = dt.Clone();

        //Fill clone with good matches
        dtGoodMatches = BuildGoodMatchesDataTable(dtGoodMatches);



        return dtGoodMatches;
    }

    private DataTable BuildGoodMatchesDataTable(DataTable dtGoodMatches)
    {
        foreach (DataRow dr in dtGoodMatches.Rows)
        {
            string matchedPart = dr.ItemArray[1].ToString();
            if (IsGoodMatch(SearchPart, matchedPart))
            {
                dtGoodMatches.Rows.Add(dr.ItemArray);
            }
        }
        return dtGoodMatches;
    }

    private bool IsGoodMatch(string searchedPart, string matchedPart)
    {
        string searchedTrimmed = Tools.Strings.FilterTrash(searchedPart.ToLower().Trim());
        string matchTrimmed = Tools.Strings.FilterTrash(matchedPart.ToLower().Trim());
        if (searchedTrimmed == matchTrimmed)
            return true;
        else if (searchedPart.Length >= 5 && matchedPart.Length >= 5)
            if (searchedPart.Substring(0, 5) == matchedPart.Substring(0, 5))
                return true;
        return false;

    }
    private void GetUserAndCompany()
    {
        if (Request.IsAuthenticated)
        {
            if (Membership.GetUser() != null)
            {   //GetUser
                User = Membership.GetUser();
                UserName = User.UserName;
                //Get Company
                if (!string.IsNullOrEmpty(Profile.CompanyID))
                {
                    using (RzDataContext rdc = new RzDataContext())
                        Comp = rdc.companies.Where(w => w.unique_id == Profile.CompanyID).FirstOrDefault();
                    UserName = Profile.FirstName + " " + Profile.LastName;
                    if (UserName.Length < 3)
                        UserName += " (" + User.UserName + ")";//Add username if not many chars for name
                }
            }
        }
        else
            UserName = "Public / Anonymous";
    }

    private void setShowDemoData()
    {

        if (Request.IsAuthenticated)
        {
            //pull actual PCN, ENV, and LifeCycle, else PlaceHolder images.
            imgExample.Visible = false;
            pnlRegister.Visible = false;
        }
        else
        {
            imgExample.Visible = true;
            pnlRegister.Visible = true;
            UserName = "Public / Anonymous";

        }
    }



    protected void GetTopQuotedParts()
    {
        //company c = SM_Global.DisplayCompany ?? null;
        //if (c != null)
        //    lvSearchedParts.DataSource = rzp.MostSearchedParts(6, c);
        //else
        //    lvSearchedParts.DataSource = rzp.MostSearchedParts(6);

        //lvSearchedParts.DataBind();
    }
    protected void gvResults_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvResults_Sorting(object sender, GridViewSortEventArgs e)
    {
        tools.HandleResult("fail", "Sorting not implemented yet.");
    }
    protected void gvResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tools.HandleResult("fail", "Paging not implemented yet.");
    }
    protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        GridViewRow gr = e.Row;

        //Get the TableCells
        //TableCell cellAvailability = gr.Cells[3];
        TableCell cellPartNumber = gr.Cells[0];
        TableCell cellManufacturer = gr.Cells[1];
        TableCell cellDescription = gr.Cells[2];
        TableCell cellDatasheet = gr.Cells[3];
        TableCell cellLifecycle = gr.Cells[4];
        TableCell cellPcn = gr.Cells[5];
        TableCell cellEnv = gr.Cells[6];


        //Find the grid controls        
        LinkButton lbLifecycle = (LinkButton)cellLifecycle.FindControl("lbLifecycle");
        LinkButton lbPcn = (LinkButton)cellPcn.FindControl("lbPcn");
        LinkButton lbEnv = (LinkButton)cellEnv.FindControl("lbEnv");

        //Part Number
        SearchPart = HttpUtility.UrlDecode(cellPartNumber.Text.Trim()).ToUpper();

        //Part Details Link     
        QueryPart = HttpUtility.UrlEncode(SearchPart);
        string navigateUrl = "/public/search/part_detail.aspx?pn=" + QueryPart;
        //navigateUrl = HttpUtility.UrlEncode(navigateUrl);

        //Check for ComID
        string comID = gvResults.DataKeys[gr.RowIndex].Value.ToString();
        if (!string.IsNullOrEmpty(comID))//If we have a ComID, then addit to the url
            navigateUrl += "&id=" + comID;


        //Add PartNumber Hyperlink 
        HyperLink hlPartNumber = new HyperLink();
        hlPartNumber.Text = SearchPart;
        hlPartNumber.NavigateUrl = navigateUrl.ToString();
        hlPartNumber.Target = "_blank";
        cellPartNumber.Text = "";
        cellPartNumber.Controls.Add(hlPartNumber);

        //DataSheet Link;   
        string dataSheetUrl = cellDatasheet.Text;
        if (!string.IsNullOrEmpty(dataSheetUrl) && dataSheetUrl != "&nbsp;")
        {
            //Add DataSheep Hyperlink and Span element
            HyperLink hlDatasheet = new HyperLink();
            hlDatasheet.NavigateUrl = dataSheetUrl;
            hlDatasheet.Target = "_blank";
            //Clear the initial text.
            hlDatasheet.Attributes["class"] = "btn-smc btn-smc-datasheet btn-block btn-search-detail";
            cellDatasheet.Text = "";
            //Add the Icon Span
            var span = new HtmlGenericControl("span");
            span.Attributes["class"] = "fa fa-download";
            hlDatasheet.Controls.Add(span);
            cellDatasheet.Controls.Add(hlDatasheet);
        }
        else
        {
            cellDatasheet.Text = naTExt;
        }



        //No ComID = no Silicon Expert results, handle it.
        if (string.IsNullOrEmpty(comID))
        {
            //Hide the non-dynamic buttons           
            lbLifecycle.Visible = false;
            lbPcn.Visible = false;
            lbEnv.Visible = false;

            //Set the empty text           
            cellLifecycle.Text = naTExt;
            cellPcn.Text = naTExt;
            cellEnv.Text = naTExt;
        }
    }

    protected void lbPcn_Click(object sender, EventArgs e)
    {
        try
        {

            if (!Request.IsAuthenticated)
            {
                lblDetailType.Text = "Product Change Notification Details for: (Sample)";
                imgExample.ImageUrl = "/Images/pps_pcn2.png";
                //pnlIntroPanel.Visible = true;
            }
            else
            {
                LinkButton btn = sender as LinkButton;
                GridViewRow row = btn.NamingContainer as GridViewRow;
                SelectedPart = GetSelectedPartFromRowIndex(row);


                dtvEnvData.Visible = false;
                dtvLifecycleData.Visible = false;
                gvPcnData.Visible = Request.IsAuthenticated;


                //Part = tools.TrimAllWhiteSpace(row.Cells[0].Text);
                string comid = gvResults.DataKeys[row.RowIndex].Values[0].ToString();
                //PcnDataTable = SiliconExpertLogic.GetPcnDataTableXML(comid, SelectedPart, UserName);
                PcnDataTable = SiliconExpertLogic.GetPcnDataTableJSON(comid, SelectedPart, UserName);
                //pnlIntroPanel.Visible = false;
                lblDetailType.Text = "Product Change Notifrication Details for: " + SelectedPart;

                var results = from myRow in PcnDataTable.AsEnumerable()
                              select myRow;

                var query = results.Select(s => new
                {
                    PartNumber = SelectedPart,
                    LastPCNNumber = s["LastPCNNumber"],
                    LastPCNDate = s["LastPCNDate"],
                    LastPCNType = s["LastPCNTypeOfChange"],
                    LastPCNSource = s["LastPCNSource"],
                    LastEOLSource = s["LastEOLSource"]
                });



                gvPcnData.DataSource = query;
                gvPcnData.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#divModal').modal('show');", true);

        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }




    protected void lbEnv_Click(object sender, EventArgs e)
    {
        try
        {
            if (!Request.IsAuthenticated)
            {
                lblDetailType.Text = "Environmental Details for: (Sample)";
                imgExample.ImageUrl = "/Images/pps_env2.png";
            }
            else
            {


                dtvEnvData.Visible = Request.IsAuthenticated;
                dtvLifecycleData.Visible = false;
                gvPcnData.Visible = false;

                LinkButton btn = sender as LinkButton;
                GridViewRow row = btn.NamingContainer as GridViewRow;
                SelectedPart = GetSelectedPartFromRowIndex(row);
                lblDetailType.Text = "Environmental Details for: " + SelectedPart;
                string comid = gvResults.DataKeys[row.RowIndex].Values[0].ToString();

                EnvDataTable = SiliconExpertLogic.GetEnvDataTableJSON(comid, SelectedPart, UserName);
                var results = from myRow in EnvDataTable.AsEnumerable()
                              select myRow;

                var query = results.Select(s => new
                {
                    PartNumber = SelectedPart,
                    LeadFree = s["LeadFree"],
                    RoHSStatus = s["RoHSStatus"],
                    RoHSVersion = s["RoHSVersion"],
                    Source = s["Source"],
                    SourceType = s["SourceType"],
                    Exemption = s["Exemption"],
                    ExemptionType = s["ExemptionType"],
                    EICCMembership = s["EICCMembership"],
                    ConflictMineralStatement = s["ConflictMineralStatement"],
                    ConflictMineralStatus = s["ConflictMineralStatus"],

                });


                //smdtEnv.dataSource = query.ToList(;
                //smdtEnv.loadGrid();
                dtvEnvData.DataSource = query;
                dtvEnvData.DataBind();
                // SetControlValues();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#divModal').modal('show');", true);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }


    protected void lbLifecycle_Click(object sender, EventArgs e)
    {
        try
        {
            if (!Request.IsAuthenticated)
            {
                lblDetailType.Text = "Lifecycle Details for: (Sample)";
                imgExample.ImageUrl = "/Images/pps_lifecycle2.png";
            }
            else
            {
                //SelectedPart = gvResults.SelectedRow.Cells[0].Text;
                dtvEnvData.Visible = false;
                dtvLifecycleData.Visible = Request.IsAuthenticated;
                gvPcnData.Visible = false;

                LinkButton btn = sender as LinkButton;
                GridViewRow row = btn.NamingContainer as GridViewRow;

                SelectedPart = GetSelectedPartFromRowIndex(row);

                string comid = gvResults.DataKeys[row.RowIndex].Values[0].ToString();

                lblDetailType.Text = "Lifecycle Details for: " + SelectedPart;
                LifecycleDataTable = SiliconExpertLogic.GetLifecycleDataTableJSON(comid, SelectedPart, UserName);
                dtvLifecycleData.Visible = true;
                dtvLifecycleData.DataSource = LifecycleDataTable;
                dtvLifecycleData.DataBind();
                //SetControlValues();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#divModal').modal('show');", true);
        }
        catch (Exception ex)
        {
            tools.HandleResult("fail", ex.Message);
        }
    }

    private string GetSelectedPartFromRowIndex(GridViewRow row)
    {
        //The Row Index *Should* match the index of the Viewstate DataSet
        //int idx = row.RowIndex;
        //Row from the DataTable NOT The grid Row
        //DataRow dr = dtGridResults.Rows[idx];
        //string ret = (string)dr["PartNumber"];
        //Get the Hyperlink from column 0
        HyperLink hl = (HyperLink)row.Cells[0].Controls[0];

        string partNumber = hl.Text;
        if (String.IsNullOrEmpty(partNumber))
            throw new Exception("Part number could not be determined by Hyperlink.  (Empty string): " + row.ID);
        //Grab the part number from the Hyperlink
        //SelectedPart = hl.Text;
        return partNumber;
    }

    protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            //Set Empty Cell Text

            //setEmptyCellText(e.Row);
            string PcnSource = e.Row.Cells[3].Text;
            string EolSource = e.Row.Cells[4].Text;
            string pcnSourceUrl = e.Row.Cells[3].Text = "<a href=" + PcnSource + " target=\"_blank\"  >View / Download</a>";
            string eolourceUrl = e.Row.Cells[4].Text = "<a href=" + EolSource + " target=\"_blank\"  >View / Download</a>";
            //HyperLink hl = (HyperLink)e.Row.Controls.Cell   //gr.FindControl("hlPartNumber");
            //e.Row.Cells[1].Text = "<a href=" + pcnSourceUrl + " target=\"_blank\"  >View / Download</a>";



        }
    }

    protected void dtv_DataBound(object sender, EventArgs e)
    {
        var dv = sender as DetailsView;
        if (dv.Rows.Count > 0)
        {

            foreach (DetailsViewRow row in dv.Rows)
            {
                //Make Headers Bold
                MakeDTVHeadersBold(row);

            }
        }
    }

    private void MakeDTVHeadersBold(DetailsViewRow row)
    {
        row.Cells[0].Style.Add("font-weight", "700");
    }

    protected void dtvEnvData_DataBound(object sender, EventArgs e)
    {
        var dv = sender as DetailsView;
        if (dv.Rows.Count > 0)
        {
            foreach (DetailsViewRow row in dv.Rows)
            {
                //if (row.RowType == DataControlRowType.Header)
                //    continue;
                //MakeDTVHeadersBold(row);
                //Conflict Mineral Status: http://download.siliconexpert.com/pdfs/2017/7/16/2/27/54/926/mcp_/manual/14form20sd20and20cmr20as20filed205-31-201720flict20minerals.pdf
                //Source: http://download.siliconexpert.com/pdfs/2007/04/12/isys/atm/china_rohs_5-6_compliance.pdf
                string title = "";
                string value = "";
                title = row.Cells[0].Text;
                value = row.Cells[1].Text;
                if (value.ToLower().Contains("http"))
                    AddHyperlinkToCell(row, value);




                foreach (DataControlFieldCell cell in row.Cells)
                {

                    //if (!(cell.ContainingField is TemplateField))
                    //{
                    if (cell.Controls.Count <= 0)
                        if (string.IsNullOrEmpty(cell.Text) || cell.Text == "&nbsp;")
                            cell.Text = "Not Available";
                    //}

                }


            }
        }



        //var dv = sender as DetailsView;
        //if (dv.Rows.Count > 0)
        //{
        //    //Row3 is the Rohs Row, set as hyperlink.
        //    string rohsUrl = dv.Rows[3].Cells[1].Text;
        //    dv.Rows[3].Cells[1].Text = "<a href=" + rohsUrl + " target=\"_blank\"  >View / Download</a>";
        //    foreach (DetailsViewRow row in dv.Rows)
        //    {
        //        //Make Headers Bold
        //        MakeDTVHeadersBold(row);
        //    }

        //}
    }

    private void AddHyperlinkToCell(DetailsViewRow row, string value)
    {
        HyperLink hl = new HyperLink();
        hl.Text = "View / Download";
        hl.NavigateUrl = value;
        row.Cells[1].Text = "";
        row.Cells[1].Controls.Add(hl);
    }

    protected void lbGetQuote_Click(object sender, EventArgs e)
    {
        LinkButton btn = sender as LinkButton;
        GridViewRow row = btn.NamingContainer as GridViewRow;
        HiddenField hfPartNumber = (HiddenField)row.FindControl("hfRowPartNumber");
        string partNumber = hfPartNumber.Value.Trim().ToUpper();
        quote_form.SetPartNumber(partNumber);
    }

    protected void lbPartSearch_Click(object sender, EventArgs e)
    {
        //SearchPart = txtSearch.Text.Trim().ToUpper();
        //doPartSearch();

        QueryPart = txtSearch.Text.Trim().ToUpper();
        QueryPart = HttpUtility.UrlEncode(QueryPart);
        RedirectToSearch();



    }


    private void RedirectToSearch()
    {
        Response.Redirect("/public/search/part_search.aspx?pn=" + QueryPart, false);
    }

}