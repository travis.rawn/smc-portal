﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="part_detail.aspx.cs" Inherits="public_search_part_detail" Async="true" %>

<%@ Register Src="~/assets/controls/quote_form.ascx" TagPrefix="uc1" TagName="quote_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--Part Details CSS:--%>
    <%--Product Overview:--%>
    <%--<link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9619731406/1588699519616/module_9619731406_bz-portal-product-overview.min.css">--%>
    <%--<link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9619731406/1593443448185/module_9619731406_bz-portal-product-overview.min.css">--%>
    <%--<link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9619731406/1593443448185/module_9619731406_bz-portal-product-overview.min.css">--%>
    <link rel="stylesheet" href="https://cdn2.hubspot.net/hub/1878634/hub_generated/module_assets/9619731406/1598301361218/module_9619731406_bz-portal-product-overview.min.css">


    <%--Product Tabs:--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1563844943581/module_9621052063_Sensible_Micro_-_Early_2019_Custom_Modules_bz-portal-product-tabs.min.css">
    <%--custom Popups--%>
    <link rel="stylesheet" href="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1587591560738/module_10802807380_Custom_Notification_Popup.css">
    <%-- As of 8-24-2020--%>


    <style>
        .emptyDataText {
            font-weight: 700;
        }

        .no-results {
            text-align: center;
        }

            .no-results p {
                font-size: 3vh;
                font-weight: 700;
            }
    </style>

    <%--<script src="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/9619731406/1593443448063/module_9619731406_bz-portal-product-overview.min.js"></script>--%>
    <%-- <script src="https://cdn2.hubspot.net/hub/1878634/hub_generated/module_assets/1597595772844/module_9619731406_bz-portal-product-overview.js"></script>--%>
    <script src="https://www.sensiblemicro.com/hs-fs/hub/1878634/hub_generated/module_assets/1598287046614/module_9619731406_bz-portal-product-overview.js"></script>
    <%-- As of 8-24-2020--%>

    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/1878634.js"></script>
    <!-- End of HubSpot Embed Code -->


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent1" runat="Server">

   
    <asp:Panel ID="pnlNoResults" runat="server" Visible="false">
        <div class="no-results">
            <p>No results were found for the requested part.</p>
        </div>

    </asp:Panel>
    <asp:Panel ID="pnlResults" runat="server">
        <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
            <div class="row-fluid ">
                <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
                    <div id="hs_cos_wrapper_module_15577672957051317" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
                        <div class="section-container__narrow padding-top__large padding-bottom__medium section-header__container single-part">
                            <div class="row-fluid">

                                <div class="span2 single-part__graphic">
                                    <asp:Image ID="imgOverview" runat="server" ImageUrl="/Images/nf.png" AlternateText="Part Image" />
                                </div>

                                <div class="image-popup">
                                    <button title="Close (Esc)" type="button" class="popup-close">×</button>
                                    <asp:Label runat="server" ID="lblPopupPartNumber"></asp:Label>
                                    <%-- <h6>5962-0325001QXC</h6>--%>
                                    <asp:Image ID="imgPopup" runat="server" ImageUrl="/Images/nf.png" AlternateText="Part Image" />
                                    <%-- <img src="https://www.sensiblemicro.com/hubfs/QFP-1.png" alt="QFP-1">--%>
                                </div>


                                <div class="span10 single-part__overview overview">
                                    <h1 class="overview__title">
                                        <asp:Label ID="lblOverViewPN" runat="server" Text="Label"></asp:Label>
                                    </h1>

                                    <%--Sourcing--%>

                                    <div class="overview__availability" runat="server" id="spnSourcing">
                                        <span class="overview__part-status">SOURCING AVAILABLE</span>
                                    </div>
                                    <%--Stock--%>

                                    <div class="overview__availability" runat="server" id="spnStock">
                                        <span class="overview__part-status">IN STOCK QTY:</span>
                                        <span class="overview__quantity">
                                            <asp:Label ID="lblStockQty" runat="server"></asp:Label>
                                        </span>
                                        <span class="overview__additional-info">SHIPS TODAY </span>
                                    </div>
                                    <%--Excess--%>
                                    <div class="overview__availability" runat="server" id="spnExcess">
                                        <span class="overview__part-status">EXCESS QTY: </span>
                                        <span class="overview__quantity">
                                            <asp:Label ID="lblExcessQty" runat="server"></asp:Label></span>
                                        <span class="overview__additional-info">SUBJECT TO AVAILABILITY </span>
                                    </div>






                                    <%--Availability section--%>
                                    <%-- <div class="overview__availability overview__availability--in-stock">--%>
                                    <%-- <div class="overview__availability overview__availability">
                                    <!-- If is available --->
                                    <!-- Part status --->
                                    <span class="overview__part-status">
                                        <asp:Label ID="lblOverViewStatus" runat="server"></asp:Label>
                                    </span>
                                    <!-- Stock Quantity ---->
                                    <span class="overview__quantity" id="spanStockQty" runat="server">
                                        <br>
                                        <asp:Label ID="lblStockQty" runat="server"></asp:Label>
                                    </span>
                                    <!-- Excess Quantity ---->
                                    <span class="overview__quantity" id="spanExcessQty" runat="server">
                                        <br>
                                        <asp:Label ID="lblExcessQty" runat="server"></asp:Label>
                                    </span>
                                </div>--%>





                                    <!-- Part sources --->

                                </div>
                                <div class="span12 single-part__overview overview">

                                    <div class="overview__table overview-table">
                                        <h2 class="overview-table__title">Overview
                                        </h2>
                                        <div class="overview-table__row overview-table__row--man">
                                            <b>Manufacturer</b>: &nbsp; 
                                            <asp:Label ID="lblMFG" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div class="overview-table__row overview-table__row--mpn">
                                            <b>Part Number</b>: &nbsp;
                                            <asp:Label ID="lblMPN" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div class="overview-table__row overview-table__row--des">
                                            <b>Description</b>: &nbsp;
                                        <asp:Label ID="lblDesc" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <div class="overview-table__row overview-table__row--dsl">
                                            <b>Datasheet</b>: &nbsp;
                                        <asp:HyperLink ID="hlDataSheet" runat="server" Target="_blank">View / Download</asp:HyperLink>
                                        </div>
                                    </div>

                                    <!-- Popup form link here -->
                                    <a href="#" class="hs-button" onclick="PrepareQuote(false)" id="aQuote" runat="server" title="Get Quote:">Get Quote</a>

                                </div>

                            </div>
                        </div>



                    </div>

                </div>
                <!--end widget-span -->
            </div>
            <!--end row-->
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent2" runat="Server">
    <asp:Panel ID="pnlTabs" runat="server">
        <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
            <div id="hs_cos_wrapper_module_15577695227011499" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
                <div class="section-container__narrow padding-top__small padding-bottom__small section-header__container ">
                    <div class="parts-tabs js-tabber">

                        <div class="parts-tabs__tabs">
                            <button type="button" class="js-tab parts-tabs__tab parts-tabs__tab--is-active" data-target="specs">Specifications</button>
                            <button type="button" class="js-tab parts-tabs__tab" data-target="pcn">PCN</button>
                            <button type="button" class="js-tab parts-tabs__tab" data-target="lifecycle">Lifecycle</button>
                            <button type="button" class="js-tab parts-tabs__tab" data-target="env">Environment</button>
                        </div>

                        <!-- Tab Specifications -->
                        <div class="parts-tabs__panel parts-tabs__panel--is-active js-panel" id="specs">
                            <h4 class=""><span>SPECIFICATIONS: </span></h4>

                            <table class="tablepress table-striped" border="1">
                                <tr>
                                    <th>Part Number:</th>
                                    <td>
                                        <asp:Label ID="lblSpecPartNumber" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>Manufacturer:</th>
                                    <td>
                                        <asp:Label ID="lblSpecMfg" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>Type:</th>
                                    <td>
                                        <asp:Label ID="lblSpecPlName" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>Description:</th>
                                    <td>
                                        <asp:Label ID="lblSpecDescription" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>Datasheet:</th>
                                    <td>
                                        <asp:HyperLink ID="hlSpecDatasheet" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>Lifecycle:</th>
                                    <td>
                                        <asp:Label ID="lblSpecLifecycle" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>RoHS:</th>
                                    <td>
                                        <asp:Label ID="lblSpecRoHS" runat="server" /></td>
                                </tr>
                                <tr>
                                    <th>RoHS Version:</th>
                                    <td>
                                        <asp:Label ID="lblSpecRoHSVersion" runat="server" /></td>
                                </tr>
                            </table>
                        </div>
                        <!-- END Tab Specifications -->

                        <!-- Tab PCN -->
                        <div class="parts-tabs__panel js-panel" id="pcn">
                            <h4 class=""><span>PCN ALERT(S): </span></h4>
                            <div id="divPCNReg" runat="server">
                                <div class="row register-label">
                                    <div class="col-sm-12">
                                        <label>Register for a free account, and get access to this and much more!</label>
                                        <button type="button" class="btn btn-block btn-success register-button" onclick="window.location.href = '/';">
                                            <h4>Register / Login</h4>
                                        </button>
                                    </div>
                                </div>
                                <asp:Image ID="imgPcnSample" runat="server" ImageUrl="/Images/pps_pcn2.png" CssClass="x-img x-img-thumbnail bottom sampleimage" />
                            </div>


                            <asp:GridView runat="server" ID="gvPcnData" CssClass="tablepress table-striped" AutoGenerateColumns="false" OnDataBound="gvPcnData_DataBound">
                                <Columns>

                                    <asp:BoundField DataField="PCNNumber" HeaderText="PCN#" SortExpression="PCNNumber" />
                                    <asp:BoundField DataField="TypeOfChange" HeaderText="Type" SortExpression="TypeOfChange" />
                                    <asp:BoundField DataField="DescriptionOfChange" HeaderText="Description" SortExpression="DescriptionOfChange" />
                                    <asp:BoundField DataField="PcnSource" HeaderText="Source" SortExpression="PcnSource" />
                                    <%--    <asp:TemplateField HeaderText="Source">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlPcnSource" runat="server" Target="_blank">View / Download</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                    <asp:BoundField DataField="NotificationDate" HeaderText="Date" SortExpression="NotificationDate" DataFormatString="{0:d}" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="emptyDataText">
                                        No PCN data available for this part.
                                    </div>

                                </EmptyDataTemplate>
                            </asp:GridView>


                        </div>
                        <!-- End Tab PCN -->

                        <!-- Tab Lifecycle -->
                        <div class="parts-tabs__panel js-panel" id="lifecycle">
                            <h4 class=""><span>LIFECYCLE: </span></h4>
                            <div id="divLifecycleReg" runat="server">
                                <div class="row  register-label">
                                    <div class="col-sm-12">
                                        <label>Register for a free account, and get access to this and much more!</label>
                                        <button type="button" class="btn btn-block btn-success register-button" onclick="window.location.href = '/';">
                                            <h4>Register / Login</h4>
                                        </button>
                                    </div>
                                </div>
                                <asp:Image ID="imgLifecycleSample" runat="server" ImageUrl="/Images/pps_lifecycle2.png" CssClass="x-img x-img-thumbnail bottom sampleimage" />
                            </div>
                            <asp:DetailsView ID="dtvLifecycle" runat="server" CssClass="tablepress table-striped" AutoGenerateRows="false" OnDataBound="dtvLifecycle_DataBound">
                                <Fields>
                                    <asp:BoundField DataField="PartStatus" HeaderText="Status" SortExpression="PartStatus" />
                                    <asp:BoundField DataField="LTBDate" HeaderText="LTB Date" SortExpression="LTBDate" />
                                    <asp:TemplateField HeaderText="Source">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlLifecycleSource" runat="server" Target="_blank">View / Download</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EstimatedEOLDate" HeaderText="Est. EOL" SortExpression="EstimatedEOLDate" />
                                    <asp:BoundField DataField="PartLifecycleStage" HeaderText="Lifecycle" SortExpression="PartLifecycleStage" />
                                    <asp:BoundField DataField="LifeCycleRiskGrade" HeaderText="Risk Grade" SortExpression="LifeCycleRiskGrade" />
                                    <asp:BoundField DataField="OverallRisk" HeaderText="Overall Risk" SortExpression="OverallRisk" />
                                </Fields>
                                <EmptyDataTemplate>
                                    <div class="emptyDataText">
                                        No lifecycle data available for this part.  
                                    </div>

                                </EmptyDataTemplate>
                            </asp:DetailsView>

                        </div>
                        <!-- End Tab Lifecycle -->

                        <!-- Tab Environmental -->
                        <div class="parts-tabs__panel js-panel" id="env">
                            <h4 class=""><span>ENVIRONMENT: </span></h4>
                            <div id="divEnvReg" runat="server">
                                <div class="row  register-label">
                                    <div class="col-sm-12">
                                        <label>Register for a free account, and get access to this and much more!</label>
                                        <button type="button" class="btn btn-block btn-success register-button" onclick="window.location.href = '/';">
                                            <h4>Register / Login</h4>
                                        </button>
                                    </div>
                                </div>
                                <asp:Image ID="imgEnvironmentSample" runat="server" ImageUrl="/Images/pps_env2.png" CssClass="x-img x-img-thumbnail bottom sampleimage" />
                            </div>

                            <asp:DetailsView ID="dtvEnvironment" runat="server" CssClass="tablepress table-striped" AutoGenerateRows="false" OnDataBound="dtvEnvironment_DataBound">
                                <Fields>
                                    <asp:BoundField DataField="RoHSStatus" HeaderText="RoHS Status" SortExpression="RoHSStatus" />
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hEnvSource" runat="server" Target="_blank">View / Download</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SourceType" HeaderText="Source Type" SortExpression="SourceType" />
                                    <asp:BoundField DataField="Exemption" HeaderText="Exemption" SortExpression="Exemption" />
                                    <asp:BoundField DataField="ExemptionType" HeaderText="Exemption Type" SortExpression="ExemptionType" />
                                    <asp:BoundField DataField="ExemptionCodes" HeaderText="Exemption Codes" SortExpression="ExemptionCodes" />
                                    <asp:BoundField DataField="RohsIdentifier" HeaderText="Rohs Identifier" SortExpression="RohsIdentifier" />
                                    <asp:BoundField DataField="LeadFree" HeaderText="Lead Free" SortExpression="LeadFree" />
                                    <asp:BoundField DataField="RareEarthElementInformation" HeaderText="Rare Earth Element Information" SortExpression="RareEarthElementInformation" />
                                    <asp:BoundField DataField="ConflictMineralStatus" HeaderText="Conflict Mineral Status" SortExpression="ConflictMineralStatus" />
                                    <asp:TemplateField HeaderText="Conflict Mineral Statement">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEnvStatement" runat="server" Target="_blank">View / Download</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EICCMembership" HeaderText="EICC Membership" SortExpression="EICCMembership" />
                                </Fields>
                                <EmptyDataTemplate>
                                    <div class="emptyDataText">
                                        No Environmental data available for this part.
                                    </div>


                                </EmptyDataTemplate>
                            </asp:DetailsView>

                        </div>
                        <!-- End Tab Environmental-->

                    </div>
                </div>

            </div>

        </div>
        <!--end widget-span -->

        <%-- Quote Modal--%>
        <!-- Modal -->
        <uc1:quote_form runat="server" ID="quote_form" />
        <%-- END Quote Modal--%>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent3" runat="Server">
</asp:Content>

