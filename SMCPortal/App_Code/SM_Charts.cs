﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;

/// <summary>
/// Summary description for SM_Charts
/// </summary>
public class SM_Charts
{
    public SM_Charts()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class ChartJS
    {


        public ChartJS()
        {
            showLegend = true;
        }
        public string chartType { get; set; }
        public List<chartJsDataSet> dataSetList { get; set; }
        public string options { get; set; }
        public bool stacked { get; set; }
        public string onAnimationComplete { get; set; }
        public bool maintainnAspectRatio { get; set; }


        public string canvasID { get; set; }
        public string controlPrefix { get; set; } // This is to help translate chart controls
        public List<string> labels { get; set; }
        public string toolTipMode { get; set; }
        public bool tooltipIntersect { get; set; }
        public int canvasHeight { get; set; }

        //Axes
        public bool displayXaxisLabel { get; set; }
        public bool displayYaxisLabel { get; set; }
        public bool xAxisLabelAutoskip { get; set; }
        public bool yAxisLabelAutoskip { get; set; }
        public string afterBodyFunctionString { get; set; }
        public object afterLabelFunctionString { get; set; }
        public string informationalText { get; set; }
        public string titleText { get; set; }

        //x axis
        public bool xAxisShowLabels = true;
        public int xAxisStepSize { get; set; }
        public string xAxisLabelFormat { get; set; }
        public int xAxisLabelFontSize { get; set; }
        public string xAxisLabelFontColor { get; set; }
        public int xAxisMin { get; set; }
        public int xAxisMax { get; set; }


        //y axis
        public bool yAxisShowLabels = true;
        public int yAxisStepSize { get; set; }
        public string yAxisLabelFormat { get; set; }
        public int yAxisLabelFontSize { get; set; }
        public string yAxisLabelFontColor { get; set; }
        public int yAxisMin { get; set; }
        public int yAxisMax { get; set; }

        




        //Legend
        public bool showLegend { get; set; }
        public int legendLabelFontSize { get; set; }
        public string legendLabelFontColor { get; set; }

    }
    public class chartJsDataSet
    {

        public string dataSetLabel { get; set; }
        public List<string> data { get; set; }
        public string backGroundColor { get; set; }
        public List<string> backGroundColorList { get; set; }
        public bool randomizeColors { get; set; }
        public string borderColor { get; set; }
        public int borderWidth { get; set; }
        public bool responsive { get; set; }
        public bool maintainAspectRatio { get; set; }
        public string chartType { get; set; }
        public bool fillColors { get; set; }
        public bool hidden { get; set; }
        public string labelFormat { get; set; }
        
        public string footerString { get; set; }

        //Tooltips
        public bool isTooltipAfterLabel { get; set; }
        public string tooltipAfterLabelFormat { get; set; }
        public bool isToolTipAfterBody { get; set; }
        public string ToolTipAfterBodyFormat { get; set; }
        public string tooltipPointStyle { get; set; }
        public string tooltipPointColor { get; set; }
        public string tooltipPointRadius { get; set; }
        public string tooltipHitRadius { get; set; }

        //DataLables Plugin
        public bool useDataLabelsPlugin { get; set; }
        public bool showDataLabels { get; set; }
        public int dataLablesPluginFontSize { get; set; }
        public string dataLablesPluginFontColor { get; set; } //
        public string dataLablesPluginAlign { get; set; } //end
        public string dataLablesPluginAnchor { get; set; } //start
    }


    //Default Black Color
    string defaultBlack = "rgba(0, 0, 0)";


    //List<ChartScript> scriptList = new List<ChartScript>();
    JavaScriptSerializer js = new JavaScriptSerializer();
    public void LoadAllCharts(Page page, List<ChartJS> chartList)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("\n");
        sb.Append("\n");
        sb.Append("window.onload = function () {");

        foreach (ChartJS chart in chartList)
        {
            sb.Append("\n");
            sb.Append("\n");
            string chartName = chart.canvasID;
            sb.Append("//Start " + chartName);
            sb.Append("\n");
            chartName = (chartName).ToString();
            sb.Append("\n");
            sb.Append(renderChartJsScript(chartName, chart));
            sb.Append("\n");
            sb.Append("//End " + chartName);
            sb.Append("\n");
        }
        string guid = Guid.NewGuid().ToString();
        sb.Append("};");
        sb.Append("\n");
        sb.Append("\n");
        ScriptManager.RegisterStartupScript(page, page.GetType(), guid, sb.ToString(), true);
    }
    public void LoadSingleChart(Page page, ChartJS theChart)
    {
        List<ChartJS> chartList = new List<ChartJS>();
        chartList.Add(theChart);
        LoadAllCharts(page, chartList);
    }
    public string renderChartJsScript(string chartName, ChartJS cData)
    {


        StringBuilder sb = new StringBuilder();
        //The Function        
        sb.Append("\n");
        sb.Append(chartName + "();");
        sb.Append("\n");
        sb.Append("};");
        sb.Append("\n");
        sb.Append("function " + chartName + "() {");
        sb.Append("\n");
        //Config
        sb.Append("var config = {");
        sb.Append("\n");
        sb.Append("type: '" + cData.chartType + "',");
        sb.Append("\n");
        sb.Append("titleFontSize: 24,");
        sb.Append("\n");
        sb.Append("titleFontColor: '#eee',");
        sb.Append("\n");
       


        //Options
        sb.Append(AddChartOptions(cData));
        sb.Append("\n");

        //Data
        sb.Append("data: {");
        sb.Append("\n");
        sb.Append("labels: " + js.Serialize(cData.labels.ToArray()) + ",");
        sb.Append("\n");
        sb.Append("datasets: [");
        foreach (chartJsDataSet cjd in cData.dataSetList)
        {
            sb.Append(GetChartJsDataSetString(cjd, cData.chartType));
        }
        sb.Append("],");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");
        sb.Append("};");
        sb.Append("\n");

        //render the chart
        sb.Append("var ctx = document.getElementById('" + GetCanvasID(cData) + "').getContext('2d');");
        sb.Append("\n");
        sb.Append("var " + chartName + " = new Chart(ctx, config);");
        sb.Append("\n");

        return sb.ToString();
    }
    private string GetCanvasID(ChartJS cData)
    {
        string ret = "";
        if (!string.IsNullOrEmpty(cData.controlPrefix))//This is to handle sm_chartjs controls
            ret += cData.controlPrefix + "_";
        ret += cData.canvasID;
        return ret;
    }
    private string GetChartJsDataSetString(chartJsDataSet cds, string masterChartType, bool fillColor = true)
    {
        if (cds.data == null)
            throw new Exception("Chart Data is null");

        //Type of chart, default to master chart type when empty dataset type
        if (string.IsNullOrEmpty(cds.chartType))
            cds.chartType = masterChartType;

        int lastIndex = cds.data.Count - 1; //minus one because indexes are zero based
        StringBuilder sb = new StringBuilder();
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");

        //Dataset Chart Type
        sb.Append("type: '" + cds.chartType + "',");
        sb.Append("\n");
        //Hidden
        sb.Append("hidden: " + (cds.hidden).ToString().ToLower() + ",");
        sb.Append("\n");

        //Dataset Label
        AddDataSetLabels(sb, cds);

        //ToolTips
        ApplyTooltipSettings(sb, cds);

        //Handle Color and fill options
        ApplyChartColors(sb, cds);

        //Handle Border Options
        ApplyBorderSettings(sb, cds);

        //Handle DataLabels
        if (cds.useDataLabelsPlugin)
            FormatForDataLabelsPlugin(sb, cds);

        //End DataSetString
        sb.Append("},");
        sb.Append("\n");
        return sb.ToString();
    }

    private void AddDataSetLabels(StringBuilder sb, chartJsDataSet cds)
    {
        if (!string.IsNullOrEmpty(cds.dataSetLabel))
        {
            sb.Append("label: '" + cds.dataSetLabel + "',");
            sb.Append("\n");
        }

        //LabelFOrmat
        if (!string.IsNullOrEmpty(cds.labelFormat))
        {
            sb.Append("labelFormat: '" + cds.labelFormat + "',");
            sb.Append("\n");
        }
    }

    private void ApplyTooltipSettings(StringBuilder sb, chartJsDataSet cds)
    {
        sb.Append("isToolTipAfterLabel: " + cds.isTooltipAfterLabel.ToString().ToLower() + ",");
        sb.Append("\n");
        if (!string.IsNullOrEmpty(cds.tooltipAfterLabelFormat))
            sb.Append("afterLabelFormat: '" + cds.tooltipAfterLabelFormat + "',");
        sb.Append("\n");
        sb.Append("isToolTipAfterBody: " + cds.isToolTipAfterBody.ToString().ToLower() + ",");
        sb.Append("\n");
        if (!string.IsNullOrEmpty(cds.ToolTipAfterBodyFormat))
            sb.Append("afterBodyFormat: '" + cds.ToolTipAfterBodyFormat + "',");
        sb.Append("\n");
        sb.Append("data: " + js.Serialize(cds.data) + ",");
        sb.Append("\n");

        if (!string.IsNullOrEmpty(cds.tooltipPointStyle))
        {
            sb.Append("pointStyle: '" + cds.tooltipPointStyle + "',");
            sb.Append("\n");
        }

        if (!string.IsNullOrEmpty(cds.tooltipPointRadius))
        {//8
            sb.Append("pointRadius: '" + cds.tooltipPointRadius + "',");
            sb.Append("\n");
        }

        if (!string.IsNullOrEmpty(cds.tooltipPointRadius))
        { //16
            sb.Append("pointHitRadius: '" + cds.tooltipHitRadius + "',");
            sb.Append("\n");
        }
        if (!string.IsNullOrEmpty(cds.tooltipPointColor))
        { //16
            sb.Append("pointBackgroundColor: '" + cds.tooltipPointColor + "',");
            sb.Append("\n");
        }





    }

    private void ApplyBorderSettings(StringBuilder sb, chartJsDataSet cds)
    {

        if (!string.IsNullOrEmpty(cds.borderColor))
        {
            sb.Append("borderColor: '" + cds.borderColor + "',");
            sb.Append("\n");
        }
        else
        {
            sb.Append("borderColor: 'rgba(255,255,255',");
            sb.Append("\n");
        }

        if (cds.borderWidth != null)
            sb.Append("borderWidth: '" + cds.borderWidth + "',");
        //else
        //    sb.Append("borderWidth: '0',");
        sb.Append("\n");
        ////else
        ////    sb.Append("borderColor: dynamicColors(),");
        //else
        //    sb.Append("borderColor: '255,255,255,0'),");

        sb.Append("\n");
    }

    private void ApplyChartColors(StringBuilder sb, chartJsDataSet cds)
    {

        //Priority order if multiple params provided:  backgroundCoor -> Randomized -> backgroundColorList -> Default black
        sb.Append("\n");
        if (!string.IsNullOrEmpty(cds.backGroundColor))//use specified dataset color
            sb.Append("backgroundColor: '" + cds.backGroundColor + "',");
        else if (cds.randomizeColors)//Randomize            
            sb.Append("backgroundColor: dynamicColors(),");
        else if (cds.backGroundColorList != null && cds.backGroundColorList.Count > 0)
            //We're using a List Of Colors           
            sb.Append("backgroundColor: " + GenerateColorArray(cds.backGroundColorList));
        else//Default color if none provided        
            sb.Append("backgroundColor: '" + defaultBlack + "',");
        sb.Append("\n");

        //Fill the Colors?
        if (!cds.fillColors)
            sb.Append("fill:false,");
        else
            sb.Append("fill:true,");
        sb.Append("\n");

    }

    private string GenerateColorArray(List<string> dataSetColorList)
    {
        string ret = "[";
        foreach (string data in dataSetColorList)
        {
            foreach (string color in dataSetColorList)
            {
                ret += "'" + color + "',";
                ret += "\n";
            }
        }
        ret += "],";
        ret += "\n";
        return ret;
    }

    private void FormatForDataLabelsPlugin(StringBuilder sb, chartJsDataSet cds)
    {
        sb.Append("\n");
        sb.Append("datalabels:");
        sb.Append("\n");

        sb.Append("{");
        sb.Append("\n");

        //Display
        sb.Append("display: " + cds.showDataLabels.ToString().ToLower() + ",");
        sb.Append("\n");
        //Align
        if (!string.IsNullOrEmpty(cds.dataLablesPluginAlign))
        {
            sb.Append("align: '" + cds.dataLablesPluginAlign + "',");
            sb.Append("\n");
        }

        //Anchor
        if (!string.IsNullOrEmpty(cds.dataLablesPluginAnchor))
        {
            sb.Append("anchor: '" + cds.dataLablesPluginAnchor + "',");
            sb.Append("\n");
        }

        //label Color
        if (!string.IsNullOrEmpty(cds.dataLablesPluginFontColor))
        {
            sb.Append("color: '" + cds.dataLablesPluginFontColor + "',");
            sb.Append("\n");
        }
        //Font
        sb.Append("font: {");
        sb.Append("\n");



        //Font Size
        if (cds.dataLablesPluginFontSize > 0)
        {
            sb.Append("size: " + cds.dataLablesPluginFontSize.ToString() + ",");
            sb.Append("\n");
        }




        //End Font
        sb.Append("}");
        sb.Append("\n");


        sb.Append("}");
        sb.Append("\n");
    }

    private string AddChartOptions(ChartJS cData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("options:{");
        sb.Append("\n");
        sb.Append("maintainnAspectRatio: " + cData.maintainnAspectRatio.ToString().ToLower() + ",");
        sb.Append("\n");
        //Legend
        sb.Append("legend:{");
        sb.Append("\n");
        //Show / Hide (bool)
        sb.Append("display: " + cData.showLegend.ToString().ToLower() + ",");
        sb.Append("\n");
        //Legend LAbels
        sb.Append("labels: {");
        sb.Append("\n");
        //font Size (int)
        if (cData.legendLabelFontSize != 0)
            SetLegendLabelFontSize(sb, cData.legendLabelFontSize);
        if (!string.IsNullOrEmpty(cData.legendLabelFontColor))
            SetLegendLabelFontColor(sb, cData.legendLabelFontColor);

        //End Legend Labels
        sb.Append("}");
        //End Legend    
        sb.Append("},");

        sb.Append("\n");
        sb.Append("responsive: true,");
        sb.Append("\n");
        //This is where Axes are controlled
        sb.Append("scales:{");
        sb.Append("\n");
        //X Axis
        sb.Append("xAxes: [{");
        sb.Append("\n");



        //Ticks / Autoskip, Label Fonts etc.
        sb.Append("ticks:");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        //Axis min Max, i.e. from 0-100
        if (cData.xAxisMin > 0)
        {
            sb.Append("min: " + cData.xAxisMin + ",");
            sb.Append("\n");
        }
        if (cData.xAxisMax > 0)
        {
            sb.Append("max: " + cData.xAxisMax + ",");
            sb.Append("\n");
        }



        //display: false, //this will remove only the label
        sb.Append("display: '" + (cData.yAxisShowLabels).ToString().ToLower() + "',");
        sb.Append("\n");
        //Auto Skip, Dictates whether or not to skip x-axis labels if they too long.
        if (cData.xAxisLabelAutoskip)
            sb.Append("autoSkip: " + (cData.xAxisLabelAutoskip).ToString() + ","); //Dictates whether or not to skip x-axis labels if they too long.     
        sb.Append("\n");
        if (cData.xAxisLabelFontSize > 0)
            SetAxisLabelFontSize(sb, cData.xAxisLabelFontSize);
        if (!string.IsNullOrEmpty(cData.xAxisLabelFontColor))
            SetAxisLabelFontColor(sb, cData.xAxisLabelFontColor);
        sb.Append("callback: function(value, index, values) {");
        sb.Append("\n");
        sb.Append("return formatNumberString(value, '" + cData.xAxisLabelFormat + "');");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");

        //End X Axis Ticks
        sb.Append("},");
        sb.Append("\n");

        sb.Append("stacked: " + cData.stacked.ToString().ToLower() + ",");
        sb.Append("\n");
        //Show or hide X Axis labels on bottom of chart.
        sb.Append("display: " + (cData.displayXaxisLabel).ToString().ToLower() + ",");
        sb.Append("\n");
        sb.Append("gridLines:");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        sb.Append("offsetGridLines: true");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");

        sb.Append("}],");
        sb.Append("\n");

        //yAxes



        sb.Append("yAxes: [{");
        sb.Append("\n");
        //Show or hide Y Axis Labels on side of chart
        sb.Append("display: " + (cData.displayYaxisLabel).ToString().ToLower() + ",");
        sb.Append("\n");


        //Y Axis Ticks
        sb.Append("ticks:");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        //Axis min Max, i.e. from 0-100
        if (cData.yAxisMax > 0)
        {
            sb.Append("max: " + cData.yAxisMax + ",");
            sb.Append("\n");
        }
        if (cData.yAxisMin > 0)
        {
            sb.Append("min: " + cData.yAxisMin + ",");
            sb.Append("\n");
        }

        //display: false, //this will remove only the label
        sb.Append("display: '" + (cData.yAxisShowLabels).ToString().ToLower() + "',");
        sb.Append("\n");

        sb.Append("suggestedMin: 0,    // minimum will be 0, unless there is a lower value.");
        sb.Append("\n");
        sb.Append("// OR //");
        sb.Append("\n");
        sb.Append("beginAtZero: true,   // minimum value will be 0.");
        sb.Append("\n");
        if (cData.yAxisStepSize > 0)
            SetAxisSetpSize(sb, cData.yAxisStepSize);
        if (cData.yAxisLabelFontSize > 0)
            SetAxisLabelFontSize(sb, cData.yAxisLabelFontSize);
        if (!string.IsNullOrEmpty(cData.yAxisLabelFontColor))
            SetAxisLabelFontColor(sb, cData.yAxisLabelFontColor);

        if (cData.yAxisLabelAutoskip)
            sb.Append("autoSkip: true,"); //Dictates whether or not to skip x-axis labels if they too long.
        else
            sb.Append("autoSkip: false,"); //Dictates whether or not to skip x-axis labels if they too long.


        sb.Append("userCallback: function(value, index, values) {");
        sb.Append("\n");
        sb.Append("return formatNumberString(value, '" + cData.yAxisLabelFormat + "');");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");
        //End Y Axis Ticks




        sb.Append("\n");            
        sb.Append(" },");
        sb.Append("\n");


        //Stacked
        sb.Append("stacked: " + cData.stacked.ToString().ToLower() + ",");
        sb.Append("\n");

        //GridLines
        sb.Append("gridLines:");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        sb.Append("offsetGridLines: true");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");
        sb.Append("}],");
        sb.Append("\n");
        sb.Append("},"); // scales
        sb.Append("\n");


        if (!string.IsNullOrEmpty(cData.toolTipMode)) //http://www.chartjs.org/docs/latest/general/interactions/modes.html#interaction-modes
        {

            sb.Append("mode: '" + cData.toolTipMode + "',");
            sb.Append("\n");
        }
        if (cData.tooltipIntersect)//if true, the tooltip mode applies only when the mouse position intersects with an element. If false, the mode will be applied at all times.
        {
            sb.Append("intersect: " + cData.tooltipIntersect.ToString().ToLower() + ",");
            sb.Append("\n");
        }

       

        //Animations
        if (!string.IsNullOrEmpty(cData.onAnimationComplete))
            sb.Append(AddAnimation(cData));

        //ToolTips
        sb.Append(HandleTooltips(cData));

        sb.Append("},");
        sb.Append("\n");
        return sb.ToString();
    }

    private void SetAxisSetpSize(StringBuilder sb, object yAxisStepSize)
    {
        sb.Append("stepSize: " + yAxisStepSize + ",");
        sb.Append("\n");
    }

    private void SetAxisLabelFontColor(StringBuilder sb, string xAxisLabelFontColor)
    {
        sb.Append("fontColor: '" + xAxisLabelFontColor + "',");
        sb.Append("\n");
    }

    private void SetAxisLabelFontSize(StringBuilder sb, int xAxisLabelFontSize)
    {
        sb.Append("fontSize: " + xAxisLabelFontSize + ",");
        sb.Append("\n");
    }

    private void SetLegendLabelFontSize(StringBuilder sb, int size)
    {
        sb.Append("fontSize: " + size + ",");
        sb.Append("\n");
    }

    private void SetLegendLabelFontColor(StringBuilder sb, string colorHex)
    {
        sb.Append("fontColor: '" + colorHex + "',");
        sb.Append("\n");
    }

    private string HandleTooltips(ChartJS cData)
    { //documentation:  http://www.chartjs.org/docs/latest/configuration/tooltip.html#external-custom-tooltips
        StringBuilder sb = new StringBuilder();

        //ToolTips
        sb.Append("tooltips:");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");

        //The background color of the tooltip bubble that shows on hover
        sb.Append("backgroundColor: '#000000',");
        sb.Append("\n");


        //CallBacks
        sb.Append("callbacks:");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");

        //Labels
        sb.Append("label: function(tooltipItem, data) {");
        sb.Append("\n");
        //ToolTips
        sb.Append(HandleToolTipLabel());
        sb.Append("},");
        sb.Append("\n");
        //End Labels

        //AFter Label - checks the NEXT dataset for now (datasetIndex +1)
        sb.Append("afterLabel: function(tooltipItem, data) {");
        sb.Append("\n");
        //foreach (chartJsDataSet cds in cData.dataSetList.Where(w => w.hidden == true))
        //sb.Append(HandleExtraToolTips("Label"));
        sb.Append(cData.afterLabelFunctionString);
        sb.Append("},");
        sb.Append("\n");
        //EndAfterLabels

        //AFter Body
        sb.Append("afterBody: function(tooltipItem, data) {");
        sb.Append("\n");
        //sb.Append(HandleExtraToolTips("Body"));       
        sb.Append(cData.afterBodyFunctionString);
        sb.Append("},");
        sb.Append("\n");
        //EndAfterBody


        //Footers               
        sb.Append(HandleToolTipFooter(cData));
        //End Footers

        //End Callbacks
        sb.Append("},");
        sb.Append("\n");

        sb.Append("footerFontStyle: 'normal'");
        sb.Append("\n");

        //End ToolTips
        sb.Append("},");
        sb.Append("\n");
        return sb.ToString();
    }
    private string HandleToolTipLabel()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];");
        sb.Append("\n");
        sb.Append("var label;");
        sb.Append("\n");
        sb.Append("label = data.datasets[tooltipItem.datasetIndex].label;");
        sb.Append("\n");
        sb.Append("if(!label)");
        sb.Append("\n");
        sb.Append("label = data.labels[tooltipItem.index]");
        sb.Append("\n");
        sb.Append("var format = data.datasets[tooltipItem.datasetIndex].labelFormat");
        sb.Append("\n");
        sb.Append("var valueString =  formatNumberString(value, format);");
        sb.Append("\n");
        //sb.Append("return label + ': ' + valueString;");
        sb.Append("return valueString;");
        sb.Append("\n");
        return sb.ToString();
    }
    private string HandleToolTipFooter(ChartJS cData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("footer: function(tooltipItem, data) {");
        sb.Append("\n");
        //sb.Append("return 'This is a footer!'");



        sb.Append("},");
        sb.Append("\n");
        return sb.ToString();
    }
    //Tooltip Model - Currently Not using, but if I could create an object out if it, it may make tooltips easier to work with.  Leaving code in for future reference since tedious work is done.      
    private string GetToolTipModel()
    {
        StringBuilder sb = new StringBuilder();

        // The items that we are rendering in the tooltip. See Tooltip Item Interface section
        sb.Append(" dataPoints: TooltipItem[],");

        // Positioning
        sb.Append("xPadding: Number,");
        sb.Append("yPadding: Number,");
        sb.Append("xAlign: String,");
        sb.Append("yAlign: String,");

        // X and Y properties are the top left of the tooltip
        sb.Append("x: Number,");
        sb.Append("y: Number,");
        sb.Append("width: Number,");
        sb.Append("height: Number,");
        // Where the tooltip points to
        sb.Append("caretX: Number,");
        sb.Append("caretY: Number,");

        // Body
        // The body lines that need to be rendered
        // Each pbject contains 3 parameters
        // before: String[] // lines of text before the line with the color square
        // lines: String[], // lines of text to render as the main item with color square
        // after: String[], // lines of text to render after the main lines
        sb.Append("body: Object[],");
        // lines of text that appear after the title but before the body
        sb.Append(" beforeBody: String[],");
        // line of text that appear after the body and before the footer
        sb.Append("afterBody: String[],");
        sb.Append(" bodyFontColor: Color,");
        sb.Append(" _bodyFontFamily: String,");
        sb.Append(" _bodyFontStyle: String,");
        sb.Append(" _bodyAlign: String,");
        sb.Append(" bodyFontSize: Number,");
        sb.Append(" bodySpacing: Number,");

        // Title
        // lines of text that form the title
        sb.Append(" title: String[],");
        sb.Append("titleFontColor: Color,");
        sb.Append("_titleFontFamily: String,");
        sb.Append(" _titleFontStyle: String,");
        sb.Append(" titleFontSize: Number,");
        sb.Append(" _titleAlign: String,");
        sb.Append(" titleSpacing: Number,");
        sb.Append(" titleMarginBottom: Number,");

        // Footer
        // lines of text that form the footer
        sb.Append(" footer: String[],");
        sb.Append(" footerFontColor: Color,");
        sb.Append("_footerFontFamily: String,");
        sb.Append("_footerFontStyle: String,");
        sb.Append("footerFontSize: Number,");
        sb.Append("_footerAlign: String,");
        sb.Append(" footerSpacing: Number,");
        sb.Append(" footerMarginTop: Number,");

        // Appearance
        sb.Append(" caretSize: Number,");
        sb.Append("cornerRadius: Number,");
        sb.Append("backgroundColor: Color,");

        // colors to render for each item in body[]. This is the color of the squares in the tooltip
        sb.Append("labelColors: Color[],");

        // 0 opacity is a hidden tooltip
        sb.Append("opacity: Number,");
        sb.Append("legendColorBackground: Color,");
        sb.Append("displayColors: Boolean,");
        sb.Append("}");
        sb.Append("},");

        return sb.ToString();
    }
    private string AddAnimation(ChartJS cData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("animation: {");
        sb.Append("\n");
        sb.Append("onComplete: function() {");
        sb.Append('\n');

        switch (cData.onAnimationComplete)
        {

            case "alertTest":
                {
                    sb.Append("alert('Farts!!');");
                    break;
                }
            case "sampleAnimation":
                {
                    sb.Append(sampleAnimation());
                    break;
                }
            case "addDataLabels":
                {
                    sb.Append(AddAnimationDataLabels());
                    break;
                }

        }
        sb.Append("}");
        sb.Append("\n");
        sb.Append("},");
        sb.Append("\n");
        return sb.ToString();
    }
    private string sampleAnimation()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("hover: { mode: false },");
        sb.Append("\n");
        sb.Append("animation:{");
        sb.Append("\n");
        sb.Append("duration: 0,");
        sb.Append("\n");
        sb.Append("easing: 'easeOutQuart',");
        sb.Append("\n");
        sb.Append("onProgress: function() {");
        sb.Append("\n");
        sb.Append("var ctx = this.chart.ctx;");
        sb.Append("\n");
        sb.Append("ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);");
        sb.Append("\n");
        sb.Append(" ctx.textAlign = 'center';");
        sb.Append("\n");
        sb.Append("ctx.textBaseline = 'bottom';");
        sb.Append("\n");
        sb.Append("this.data.datasets.forEach(function(dataset) {");
        sb.Append("\n");
        sb.Append(" for (var i = 0; i < dataset.data.length; i++)");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        sb.Append("var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,");
        sb.Append("\n");
        sb.Append("scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;");
        sb.Append("\n");
        sb.Append("ctx.fillStyle = '#444';");
        sb.Append("\n");
        sb.Append("var y_pos = model.y - 5;");
        sb.Append("\n");
        sb.Append("// Make sure data value does not get overflown and hidden");
        sb.Append("\n");
        sb.Append("// when the bar's value is too close to max value of scale");
        sb.Append("\n");
        sb.Append("// Note: The y value is reverse, it counts from top down");
        sb.Append("\n");
        sb.Append("if ((scale_max - model.y) / scale_max >= 0.93)");
        sb.Append("\n");
        sb.Append("y_pos = model.y + 20;");
        sb.Append("\n");
        sb.Append(" ctx.fillText(dataset.data[i], model.x, y_pos);");
        sb.Append("\n");
        sb.Append("\n");
        sb.Append("\n");
        sb.Append(" }");
        sb.Append("\n");
        sb.Append("});");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");
        sb.Append("},");
        return sb.ToString();
    }
    private string AddAnimationDataLabels()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("var ctx = this.chart.ctx;");
        sb.Append("ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);");
        sb.Append("ctx.fillStyle = 'black';");
        sb.Append("ctx.textAlign = 'center';");
        sb.Append("ctx.textBaseline = 'bottom';");
        sb.Append("this.data.datasets.forEach(function(dataset)");
        sb.Append("{");
        sb.Append("for (var i = 0; i < dataset.data.length; i++)");
        sb.Append("{");
        sb.Append("for(var key in dataset._meta)");
        sb.Append("{");
        sb.Append("var model = dataset._meta[key].data[i]._model;");
        sb.Append("ctx.fillText(dataset.data[i], model.x, model.y - 5);");
        sb.Append("}");
        sb.Append("}");
        sb.Append("});");

        return sb.ToString();
    }




}

