﻿using SensibleDAL;
using SensibleDAL.dbml;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;

/// <summary>
/// Summary description for SM_Security
/// </summary>
public class SM_Security


{




    public SM_Security()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool is_consignment_authorized(out string strNoAccess)
    {
        strNoAccess = "";
        bool consignment_Authorized = (HttpContext.Current.User.IsInRole("admin") || HttpContext.Current.User.IsInRole("consign") || HttpContext.Current.User.IsInRole("portal_admin"));
        if (!consignment_Authorized)
        {
            strNoAccess = "Sorry, this account is currently not authorized for access to the Consignment Center.  Please notify your Sensible Micro reprenentative if you feel this is in error.";
            return false;
        }
        return true;
    }

    public static List<string> authorized_search_campaignList = new List<string>() { "octopart" };
    public static void CheckForBlockedIP()
    {

        SM_Tools tools = new SM_Tools();
        string ip = NetworkLogic.GetVisitorIpAddress();
        if (!NetworkLogic.IsIpBlocked(ip))
            return;


        //Check for special utm_campaign
        string utm_campaign = HttpContext.Current.Request.QueryString["utm_campaign"];
        if (!string.IsNullOrEmpty(utm_campaign))
        {
            utm_campaign = utm_campaign.ToLower();
            Tools.Strings.SanitizeInput(utm_campaign);
            if (authorized_search_campaignList.Contains(utm_campaign))
                return;
        }

        if (tools.IsCaptchaNeeded(ip))
            tools.RedirectToCaptcha();
        //else
        //{
        //    //IP Blocked, Captcha not satisfied
        //    RedirectToBlockPage();


        //    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, ip + " has been denied access due to a current IP Block. ", false);
        //}

    }

    public static void RedirectToBlockPage()
    {
        if (HttpContext.Current.Request.Url.AbsolutePath != "/public/block_page.aspx")
            HttpContext.Current.Response.Redirect("/public/block_page.aspx", false);

    }

    public static bool HandleUserSecurityValidation()
    {


        bool securityValid = HandleUserSecurityTasks(SM_Global.CurrentUser);
        return securityValid;
    }


    protected static bool HandleUserSecurityTasks(MembershipUser u)
    {
        string currentUrl = HttpContext.Current.Request.Url.AbsolutePath;
        //This is laid out by importance and chronological order in which the tasks should occer


        //IF locked out or not-authorized, will return to login with message.
        if (!CheckUserNotAuthorized(u))
            return false;

        //Aged User Login
        if (IsAgendUserLogin(u))
        {
            HandleAgedLogin(u);

            return false;
        }

        //If account is valid, handle temp password now.
        if (HasTempPassword(u))
        {

            if (currentUrl != "/Account/password_change.aspx")
                HttpContext.Current.Response.Redirect("/Account/password_change.aspx?id=" + u.ProviderUserKey.ToString() + "&temp=1", false);
            Logout();
            return false;
        }

        //If not temp password, confirm T&C at this time
        if (!HasAcceptedLatestTC(u))
        {
            if (currentUrl != "/Account/portal_terms.aspx")
                HttpContext.Current.Response.Redirect("~/Account/portal_terms.aspx?id=" + u.ProviderUserKey.ToString() + "&ReturnUrl=" + HttpContext.Current.Request.Url.AbsolutePath, false);
            return false;
            //I don't want to logout here, because, after accepting the terms, for the 1st postback, it will check this, find false initially and log the user out.

        }

        return true;
    }

    public static bool CheckUserNotAuthorized(MembershipUser u)
    {
        string currentUrl = HttpContext.Current.Request.Url.AbsolutePath;
        if (u.IsLockedOut || !u.IsApproved)
        {
            //if (currentUrl != "/login.aspx")
            //    HttpContext.Current.Response.Redirect("/login.aspx" + HttpContext.Current.Request.Url.Query ?? "" + "&authorized=false", false);
            Logout();
            return false;
        }
        return true;
    }

    private static void HandleAgedLogin(MembershipUser u, bool sendEmailToUser = true)
    {
        //Reset the password   

        ResetPassword(u, true);
        Logout();
        FormsAuthentication.RedirectToLoginPage(HttpContext.Current.Request.Url.AbsolutePath);
    }



    public static bool HasTempPassword(MembershipUser u)
    {
        //if (u.Comment == "1" || u.Comment == ValidationStatus.NeedsTempPasswordReset.ToString())
        ProfileCommon p = (ProfileCommon)ProfileCommon.Create(u.UserName);
        if (p.HasTemporaryPassword == true)
            return true;
        return false;
    }


    //public static MembershipUser LoadCurrentUser()
    //{
    //    return Membership.GetUser();
    //}
    public static bool IsAgendUserLogin(MembershipUser u)
    {
        if (!CheckLastLoginDate(u))
            return true;
        if (u.Comment == "2")//Comment = 2 = "30 day lockout password reset"   
            return true;
        //if (u.Comment == ValidationStatus.AgedLogin.ToString())           
        return false;
    }


    //Check Last Login Date
    protected static bool CheckLastLoginDate(MembershipUser u)
    {
        //False - is Aged
        double test = (DateTime.Today - u.LastLoginDate).TotalDays;
        //KT def use last login date, because last activity date increments on failed logins.        
        if ((DateTime.Today - u.LastLoginDate).TotalDays > 30)
            return false;
        else
            return true;
    }


    public static bool HasAcceptedLatestTC(MembershipUser u)
    {
        bool ret = false;
        using (sm_portalEntities PDC = new sm_portalEntities())
        {
            ret = PDC.Terms.Where(t => t.UserId == (Guid)u.ProviderUserKey).Select(s => s.accepted).FirstOrDefault();
        }

        return ret;
    }

    public static string GenerateUserPassword()
    {
        int passwordLength = 12;
        int nonAlphaNumericCharacterCount = 2;
        string password = System.Web.Security.Membership.GeneratePassword(passwordLength, nonAlphaNumericCharacterCount);
        return password;
    }

    public static company GetCompanyforEmailAddress(string email, out companycontact contact)
    {
        contact = null;
        //since this contact email could potentially belon g to multiple companies, let's put in list and check count == 1'        
        List<companycontact> contactList = new List<companycontact>();
        using (RzDataContext rdc = new RzDataContext())
        {
            contactList = rdc.companycontacts.Where(w => w.primaryemailaddress.ToLower() == email).ToList();
            if (contactList.Count == 1)
            {
                companycontact cc = contactList[0];
                contact = cc;
                company c = rdc.companies.Where(w => w.unique_id == cc.base_company_uid).FirstOrDefault();
                return c;
            }

        }
        return null;

    }

    public static bool RegisterNewExternalUser(string firstName, string lastName, string email, out MembershipCreateStatus createStatus)
    {

        //Check valid Email Address
        if (!Tools.Email.IsEmailAddress(email))
        {
            //ShowMessage(registerEmail + " is not a valid email address.");
            createStatus = MembershipCreateStatus.InvalidEmail;
            return false;
        }

        //Check Valid Customer Email Address
        if (!SM_Security.IsValidPortalEmail(email))
        {
            //ShowMessage(registerEmail + " is not a valid customer email address.");
            createStatus = MembershipCreateStatus.InvalidUserName;
            return false;
        }

        //Generate the Password
        string passWord = GenerateUserPassword();


        //Create the User
        MembershipUser newExternalUser = System.Web.Security.Membership.CreateUser(email, passWord, email, null, null, true, out createStatus);
        if (createStatus == MembershipCreateStatus.Success)
            SM_Security.HandleRegistrationTasks(newExternalUser, firstName, lastName, passWord, true);
        else
            return false;

        //ShowCreateStatusMessage(createStatus);
        return true;

    }

    public static bool RegisterNewInternalUser(string firstName, string lastName, string userName, string email, out MembershipCreateStatus createStatus)
    {
        //Generate the Password
        string passWord = GenerateUserPassword();

        MembershipUser newInternalUser = System.Web.Security.Membership.CreateUser(userName, passWord, email, null, null, true, out createStatus);
        if (createStatus == MembershipCreateStatus.Success)
            SM_Security.HandleRegistrationTasks(newInternalUser, firstName, lastName, passWord, true);

        //ShowCreateStatusMessage(createStatus);
        return true;

    }

    public static void HandleRegistrationTasks(MembershipUser u, string firstName, string lastName, string passWord, bool isTest = false)
    {
        try
        {
            SM_Tools tools = new SM_Tools();
            if (u == null)
            {
                //HandleError("User not found.");
                return;
            }

            string email = u.UserName.Trim().ToLower();
            if (!IsValidPortalEmail(email))
            {
                //Delete the user if not associated with an allowed company (NO vendors, etc)
                System.Web.Security.Membership.DeleteUser(email);
            }
            else
            {
                if (!Roles.IsUserInRole(email, SM_Enums.PortalRole.admin.ToString())) // Allow Admins to do mock Creations.
                {
                    //Set the email Address, for external users, this is the email.    
                    u.Email = email;
                    //Set Comment = 1 for needs temp password. - This happens on the Register page for now.
                    //u.Comment = ValidationStatus.NeedsTempPasswordReset.ToString();
                    //SM_Security.SetAccountNeedsPasswordChange(u);
                    //Updadte the User with comment
                    System.Web.Security.Membership.UpdateUser(u);
                    //Create Initital Profile
                    CreateInitialProfile(u, firstName, lastName);
                    //Get List of CC Notifications
                    string[] notificationemaillist = new string[] { "ctorrioni@sensiblemicro.com", "joemar@sensiblemicro.com" };
                    if (isTest)//For Tests, don't CC peeps
                        Array.Clear(notificationemaillist, 0, notificationemaillist.Length);
                    tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, email + " has successfully registered for the Portal.", "Registered at: " + DateTime.Now + "<br /> From IP: " + HttpContext.Current.Request.UserHostAddress, notificationemaillist, null, true);
                    tools.SendNewUserEmail(u, passWord);


                }
                else
                {
                    System.Web.Security.Membership.DeleteUser(email);

                }
            }

            //if (isTest)
            //    Membership.DeleteUser(email);


        }
        catch (Exception ex)
        {
            //tools.HandleResultJS(ex.ToString(), true, Page);
        }
    }


    public static void SendLoginEmailAlert(MembershipUser user)
    {
        SM_Tools tools = new SM_Tools();
        string CurrentIP = NetworkLogic.GetVisitorIpAddress();//HttpContext.Current.Request.UserHostAddress;
        string from = SystemLogic.Email.EmailGroupAddress.PortalAlert;
        string to = SystemLogic.Email.EmailGroup.PortalAlerts;

        //Identify rz companies that have contacts with this person's email address
        using (RzDataContext rdc = new RzDataContext())
        {
            List<string> matchedCompanyIDs = rdc.companycontacts.Where(w => w.primaryemailaddress == user.Email && w.companyname != "KT Portal Test").Select(s => s.base_company_uid).ToList();
            //Identify Rz Users who own any companies related to this person't email address (in the above list)
            List<string> matchedRzUserIDs = rdc.companies.Where(w => matchedCompanyIDs.Contains(w.unique_id)).Select(s => s.base_mc_user_uid).ToList();
            //Get the email address for users that own the identified company(s)
            string[] cc = rdc.n_users.Where(w => matchedRzUserIDs.Contains(w.unique_id)).Select(s => s.email_address).ToArray();
            string subject = user.UserName.ToString() + " has logged in.";
            string body = "Woot! " + user.UserName + "'s Current IP is: " + CurrentIP;
            tools.SendMail(from, to, subject, body, cc, null, true);


        }

    }

    public static void Logout()
    {
        HttpContext.Current.Session.Clear();
        HttpContext.Current.Session.Abandon();
        HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
        FormsAuthentication.SignOut();

    }


    public static bool ResetPassword(MembershipUser u, bool sendEmailToUser = true)
    {

        //If accoutn is locked out our unauthorized, password reset will throw exception
        if (!CheckUserNotAuthorized(u))
            return false;

        //Do Reset
        string tempPassword = u.ResetPassword();
        //Set new Status to require temp change.       
        SM_Security.SetTemporaryPasswordStatus(u, true);

        //Send the Email
        if (sendEmailToUser)
            SendPasswordRecoveryEmail(u, tempPassword);

        return true;
    }

    private static void SendPasswordRecoveryEmail(MembershipUser u, string tempPassword)
    {

        string StrContent = "";
        StringBuilder strb = new StringBuilder();
        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplates/PasswordRecoveryEmail.txt"));
        string readFile = reader.ReadToEnd();
        StrContent = readFile;
        //Build the Message Body
        string messageBody = BuildPasswordResetMessageBody(u);
        StrContent = StrContent.Replace("[MessageBody]", messageBody);
        StrContent = StrContent.Replace("[FirstName]", "");
        StrContent = StrContent.Replace("[UserName]", u.UserName);
        StrContent = StrContent.Replace("[Password]", tempPassword);
        strb.Append(StrContent);
        SM_Tools tools = new SM_Tools();
        tools.SendMail("password_reset@sensiblemicro.com", u.Email, "Password Reset Request: Sensible Micro Portal", strb.ToString(), null, null, true);

    }

    public static void SetTemporaryPasswordStatus(MembershipUser u, bool hasTemporaryPassword, string accountStatusMessage = "")
    {
        ProfileCommon userProfile = (ProfileCommon)ProfileCommon.Create(u.UserName, true);
        userProfile.HasTemporaryPassword = hasTemporaryPassword;
        if (!string.IsNullOrEmpty(accountStatusMessage))
            userProfile.AccessStatusMessage = accountStatusMessage;
        userProfile.Save();
    }

    private static string BuildPasswordResetMessageBody(MembershipUser u)
    {
        string ret = "Hello, we have received a password reset request, and you have been assigned a temporary password.";

        if (IsAgendUserLogin(u))
            ret = "Hello, it has been 30 days or more since your last successful login to the Sensible Micro Portal, for your security your password has been reset to a temporary password.";
        return ret;
    }

    protected static void CreateInitialProfile(MembershipUser u, string firstame, string lastName)
    {
        ProfileCommon userProfile = (ProfileCommon)ProfileCommon.Create(u.UserName, true);
        userProfile.CompanyID = null;
        userProfile.CompanyName = null;
        userProfile.ContactID = null;
        userProfile.ContactName = null;
        userProfile.FirstName = firstame;
        userProfile.LastName = lastName;
        userProfile.SensibleRepID = "24172ac97b0f4d9688ae9945bc64d395"; //JoeMAr
        userProfile.HasTemporaryPassword = true;
        companycontact cc;
        company c = SM_Security.GetCompanyforEmailAddress(u.Email, out cc);
        if (c != null)
        {
            userProfile.CompanyID = c.unique_id;
            userProfile.CompanyName = c.companyname;
            userProfile.ContactID = cc.unique_id;
            userProfile.ContactName = cc.contactname;
            userProfile.SensibleRep = cc.agentname;
            userProfile.SensibleRepID = cc.base_mc_user_uid;
        }
        else
        {
            //if no positiviely identified company, set Joe Mar as the rep. and email users that this needs to be setup internally
            userProfile.SensibleRepID = "24172ac97b0f4d9688ae9945bc64d395";
            string[] arrCC = new string[] { "joemar@sensiblemicro.com", "ctorrioni@sensiblemicro.com", "mristoff@sensiblemicro.com" };
            SM_Tools tools = new SM_Tools();
            tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Portal - Company Association Required", firstame + " " + lastName + " (" + u.Email + @") has successfully registered for the portal, however, we found more than one match for the email address.  Please address this in Rz, and <a href=""https://portal.sensiblemicro.com/secure_admin/security/profile_management.aspx"">manually associate the user in the portal.</a>", null, null, true);
        }

        userProfile.Save();
    }

    public static List<string> disallowedEmailDomains = new List<string>() { "gmail.com", "yahoo.com", "hotmail.com", "aol.com", "hotmail.co.uk", "hotmail.fr", "msn.com", "yahoo.fr", "wanadoo.fr", "orange.fr", "comcast.net", "yahoo.co.uk", "yahoo.com.br", "yahoo.co.in", "live.com", "rediffmail.com","free.fr", "gmx.de", "web.de", "yandex.ru",
"ymail.com", "libero.it", "outlook.com",  "uol.com.br", "bol.com.br", "mail.ru", "cox.net", "hotmail.it", "sbcglobal.net", "sfr.fr", "live.fr", "verizon.net", "live.co.uk",
"googlemail.com", "yahoo.es", "ig.com.br", "live.nl", "bigpond.com", "terra.com.br", "yahoo.it", "neuf.fr", "yahoo.de", "alice.it", "rocketmail.com", "att.net", "laposte.net",
"facebook.com", "bellsouth.net", "yahoo.in",  "hotmail.es", "charter.net", "yahoo.ca", "yahoo.com.au", "rambler.ru", "hotmail.de", "tiscali.it", "shaw.ca", "yahoo.co.jp", "sky.com",
"earthlink.net", "optonline.net", "freenet.de", "t-online.de", "aliceadsl.fr", "virgilio.it", "home.nl", "qq.com", "telenet.be", "me.com", "yahoo.com.ar", "tiscali.co.uk", "yahoo.com.mx", "voila.fr", "gmx.net", "mail.com", "planet.nl", "tin.it", "live.it", "ntlworld.com", "arcor.de", "yahoo.co.id",  "frontiernet.net", "hetnet.nl", "live.com.au", "yahoo.com.sg", "zonnet.nl", "club-internet.fr", "juno.com",  "optusnet.com.au", "blueyonder.co.uk", "bluewin.ch", "skynet.be", "sympatico.ca", "windstream.net", "mac.com", "centurytel.net", "chello.nl", "live.ca", "aim.com", "bigpond.net.au", "123.com", "126.com", "mail.com"};

    public static bool IsValidPortalEmail(string email)
    {
        List<string> testEmails = new List<string>() { "shmootill@gmail.com", "shmootill@outlook.com" };

        if (testEmails.Contains(email.ToLower()))
            return true;

        using (RzDataContext rdc = new RzDataContext())
        {
            //Matched Contacts
            List<companycontact> MatchedContacts = rdc.companycontacts.Where(c => c.primaryemailaddress.Trim() == email.Trim()).ToList();
            //Matched Companies
            if (MatchedContacts.Count <= 0)
                return false;
            List<company> MatchedCompanies = new List<company>();
            foreach (companycontact c in MatchedContacts)
            {
                MatchedCompanies.Add(rdc.companies.Where(w => w.unique_id == c.base_company_uid).First());
            }
            if (MatchedCompanies.Count <= 0)
                return false;

            foreach (company co in MatchedCompanies)
            {
                if (!IsValidCompany(co))
                    return false;
            }
        }
        return true;
    }

    private static bool IsValidCompany(company co)
    {
        //Only allow companies that have the followign companytypes
        List<string> allowedTypes = new List<string>() { "p", "c", "revenue sharing partner" };
        List<string> allowedCompanies = new List<string>();

        //Further filter by never allowing companies with the following agent designations    
        //Dead Accounts = "f2569b12718645e382e0346716f6785d"
        //Vendor = "4b8bfdf2196a4c1cbf388e041fcd526c"
        //E-Sales = "9411ca5ec9d949528f078aa24d35ed29"
        //CFT = "7b7dc669d0b7425da812df0e60936925"
        //Phil Scott == "2f948876da1a47a8b37e954c26e27892"
        List<string> disallowedAgentIds = new List<string>() { "f2569b12718645e382e0346716f6785d", "4b8bfdf2196a4c1cbf388e041fcd526c", "9411ca5ec9d949528f078aa24d35ed29", "7b7dc669d0b7425da812df0e60936925" };

        //can't be owned by disallowed agent --  !disallowedAgentIds.Contains(w.base_mc_user_uid)
        //must be an allowed companytype -- allowedTypes.Contains(w.companytype.ToLower())

        if (disallowedAgentIds.Contains(co.base_mc_user_uid))
            return false;

        //Allow customers that have consignment
        using (RzDataContext rdc = new RzDataContext())
        {
            RzTools rzt = new RzTools();
            if (rzt.IsConsignmentPartner(rdc, co))
                return true;
        }


        if (!allowedTypes.Contains(co.companytype.Trim().ToLower()))
            return false;





        return true;
    }


}