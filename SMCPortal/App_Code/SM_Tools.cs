﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Tools;
using System.Drawing;
using System.Diagnostics;
using SensibleDAL.dbml;
using SensibleDAL;
using Newtonsoft.Json;
using SensibleDAL.ef;
using System.Threading.Tasks;

/// <summary>
/// Summary description for SM_Tools
/// </summary>

public class SM_Tools
{
    RzDataContext RDC = new RzDataContext();

    public SM_Tools()
    {

    }

    public void winLog(string eventSource, EventLogEntryType type, string logMessage)
    {
        if (!EventLog.SourceExists("SMCPortal"))
        {
            EventLog.CreateEventSource("SMCPortal", "Application");
        }
        EventLog.WriteEntry(eventSource, logMessage, type);
    }



    public List<ImageJob> ResizeImages(List<byte[]> SourceList, int MaxWidth)  //DEpricated, will call this for each file when looping through files.
    {

        List<ImageJob> ret = new List<ImageJob>();
        if (SourceList.Count > 0)
        {

            if (HttpContext.Current.Request.Files.Count > 0)
            {
                ////Loop through each uploaded file               
                foreach (byte[] b in SourceList)
                {
                    byte[] OutPutByteStream = new byte[0];
                    //You can specify any of 30 commands.. See http://imageresizing.net         
                    Instructions resizeinstructions = new Instructions("maxwidth=" + MaxWidth + "&format=jpg");
                    ImageJob j = new ImageJob(b, OutPutByteStream, resizeinstructions);
                    j.Build();
                    ret.Add(j);
                }
            }
        }
        else
        {
            return null;
        }

        if (ret.Count > 0)
            return ret;
        else
            return null;
    }



    public byte[] ResizeImage(byte[] SourceByte, int MaxWidth, int quality = 100, string format = "jpg", int MaxHeight = 0)  //DEpricated, will call this for each file when looping through files.
    {
        //defaults:
        //format = jpg
        //quality = 85;

        byte[] ret = new byte[0];
        using (MemoryStream ms = new MemoryStream())
        {
            Instructions resizeinstructions = new Instructions("maxwidth=" + MaxWidth + "&format=" + format + "&quality=" + quality);
            if (MaxHeight > 0)
                resizeinstructions = new Instructions("maxwidth=" + MaxWidth + "&format=" + "jpg" + "&quality=" + quality + "&maxheight=" + MaxHeight + "&mode=max");
            ImageJob j = new ImageJob(SourceByte, ms, resizeinstructions);
            j.Build();
            ret = ms.ToArray();
        }
        return ret;
    }

    public byte[] LoadImageFromPath(string imagePath)
    {
        if (File.Exists(imagePath))
            return File.ReadAllBytes(imagePath);
        return null;
    }


    //User Experience / UI
    public void ShowNavHelp(Page page)
    {
        ScriptManager.RegisterStartupScript(page, page.GetType(), "showNavHelpKey", "$('#aHelpToggle').css('visibility', 'visible');", true);
        //$(".Deposit").css('visibility', 'visible');
        //page.Master.FindControl("aHelpToggle").Visible = true;
    }

    public void JS_Modal(string message, Control control)
    {
        //ScriptManager.RegisterClientScriptBlock(page.Page, page.Page.GetType(), "alert", "alert('" + message + "');", true);
        //ScriptManager.RegisterStartupScript(control, GetType(), "JS_Modal", "alert('" + message + "');", true);
        ScriptManager.RegisterStartupScript(control, GetType(), "showalert", "alert('" + message + "');", true);
    }

    public void JS_Modal_Confirm(string message, Page page)
    {
        ScriptManager.RegisterClientScriptBlock(page, GetType(), "JS_Modal_Confirm", "$(confirm(" + message + "));", true);

    }




    public void LoadGridDataLINQ(LinqDataSource l, GridView g, EventArgs e, StateBag v, Page page)
    {
        GridViewPageEventArgs p = null;
        GridViewSortEventArgs s = null;
        string CurrentSort = g.SortExpression;

        try
        {
            switch (e.ToString())
            {
                case ("System.Web.UI.WebControls.GridViewPageEventArgs"):
                    p = (GridViewPageEventArgs)e;
                    g.PageIndex = p.NewPageIndex;
                    if (v["CurrentSort"] != null)//Maintain Sort Order on Paging
                        l.OrderBy = v["CurrentSort"].ToString();
                    break;
                case ("System.Web.UI.WebControls.GridViewSortEventArgs"):

                    s = (GridViewSortEventArgs)e;
                    v["CurrentSort"] = SortGridViewLinqDataSource(v, s, g);
                    l.OrderBy = v["CurrentSort"].ToString();
                    break;
            }
            g.DataSource = l;
            g.DataBind();

        }
        catch (Exception ex)
        {
            HandleResult(page, "fail", ex.Message);
        }
    }




    public void LoadGridDataDataTablesNetDefault(Page page, GridView gv, string momentDateTimeString = null, Dictionary<int, List<Tuple<string, string>>> columnDefs = null)
    {
        if (string.IsNullOrEmpty(momentDateTimeString))
            momentDateTimeString = "M/D/YYYY";

        if (gv.Rows.Count > 0)
        {
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            StringBuilder sb = new StringBuilder();
            sb.Append("\n");//JIC previous script had no line break.
            sb.Append("$(function loadDatatable()");
            sb.Append("{ ");
            sb.Append("\n");
            sb.Append("$.fn.dataTable.moment('" + momentDateTimeString + "');");
            sb.Append("\n");
            sb.Append("$(\"#" + gv.ClientID + "\").DataTable(");
            sb.Append("\n");
            ////DatatableOptions
            //sb.Append(GetDefaultDataTableOptions(columnDefs));

            sb.Append(");");
            sb.Append("\n");


            //Set a listner for modals to apply DataTables() js on modal launch

            sb.Append("$('.modal').on('shown.bs.modal', function(e) {");
            sb.Append("\n");
            //$(".dataTable-modal").DataTable();'
            sb.Append("$(\".dataTable-modal\").DataTable(");
            sb.Append("\n");
            //DatatableOptions

            sb.Append(GetDefaultDataTableOptions(columnDefs));





            sb.Append(");");
            sb.Append("\n");

            //Fade DataTable in after Load.  
            sb.Append("\n");
            sb.Append("$('.dataTable-container').fadeIn();");
            sb.Append("\n");


            ScriptManager.RegisterStartupScript(page, page.GetType(), "gridKey", sb.ToString(), true);
        }


    }



    private string GetDefaultDataTableOptions(Dictionary<int, List<Tuple<string, string>>> columnDefs = null)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("{ ");
        sb.Append("\n");
        sb.Append("'order': [[0, 'asc']], ");
        sb.Append("\n");
        sb.Append("'language': { ");
        sb.Append("\n");
        sb.Append("'infoEmpty': 'No record found.', ");
        sb.Append("\n");
        sb.Append("'zeroRecords': 'No records match your query.', ");
        sb.Append("\n");
        sb.Append("}, ");
        sb.Append("\n");
        sb.Append("'bFilter': true, ");
        sb.Append("\n");
        sb.Append("'bPaginate': true, ");
        sb.Append("\n");
        sb.Append("'bInfo': true, ");
        sb.Append("\n");
        sb.Append("'autoWidth': true, ");
        sb.Append("\n");
        sb.Append("'pageLength': 10, ");
        sb.Append("\n");
        sb.Append("'responsive': true, ");
        sb.Append("\n");
        sb.Append("'width': '100%', ");
        sb.Append("\n");

        //Start colDefs
        sb.Append(" 'columnDefs':");
        sb.Append("\n");
        sb.Append("[");
        sb.Append("\n");
        if (columnDefs != null)
            sb.Append(SetCustomColumnDefinitions(columnDefs));
        sb.Append("]");
        sb.Append("\n");
        //End colDefs
        sb.Append("}, ");//Close the DataTable() call
        sb.Append("\n");

        //This is the close of the main DataTable() method
        sb.Append("); ");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");

        return sb.ToString();
    }

    private string SetCustomColumnDefinitions(Dictionary<int, List<Tuple<string, string>>> columnDefs)
    {
        StringBuilder sb = new StringBuilder();





        foreach (KeyValuePair<int, List<Tuple<string, string>>> kvp in columnDefs)
        {
            //This line carrys over from the previous method,
            sb.Append("{ ");
            sb.Append("\n");

            //{
            //    "targets": [ 2 ],
            //    "visible": false,
            //    "searchable": false
            //},
            int target = kvp.Key;
            sb.Append("'targets': [" + target + "],");
            sb.Append("\n");

            foreach (Tuple<string, string> d in kvp.Value)
            {


                //sb.Append("{");
                //sb.Append("\n");
                int value;
                //ints are represented with brackets?
                if (int.TryParse(d.Item2, out value))
                    sb.Append(d.Item1 + " : [" + d.Item2 + "],");
                else
                    sb.Append(d.Item1 + " : " + d.Item2 + ",");
                sb.Append("\n");
                //sb.Append("},");
                //sb.Append("\n");
            }
            //If this is the last, don't include comma
            KeyValuePair<int, List<Tuple<string, string>>> kvpLast = columnDefs.Last();
            //if (kvp.Key == kvpLast.Key)
            //    sb.Append("}");
            //else
            //    sb.Append("},");
            sb.Append("},");
            sb.Append("\n");

        }

        return sb.ToString();
    }





    //Handle Sorting for LinqDataSource in GridViews
    //Sometimes, like with Details, you can't set the DataSource ID in the Gridview, otherwise it will fire onselecting on every page load, and when that grid is dependent
    //on a parent grid, it will have no datakey to use.  In these cases, best to set DataSource ID in codebehind as needed.  However, this breaks automatic sorting.  This helper
    //method handles sorting
    public string SortGridViewLinqDataSource(StateBag viewState, GridViewSortEventArgs e, GridView g)
    {
        string ret = null;
        //From https://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.linqdatasource.orderby(v=vs.110).aspx
        //Need to save current or previous direction to State, else it's always ascending -- http://stackoverflow.com/questions/250037/gridview-sorting-sortdirection-always-ascending?
        //Got idea from : http://stackoverflow.com/questions/250037/gridview-sorting-sortdirection-always-ascending
        if (e.SortExpression == (string)viewState["prevColumn"])
            // We are resorting the same column, so flip the sort direction
            e.SortDirection = ((SortDirection)viewState["prevDirection"] == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
        else
            e.SortDirection = SortDirection.Ascending;

        // Apply the sort        
        ret = e.SortExpression + ((e.SortDirection == SortDirection.Ascending) ? " ASC" : " DESC");

        //Set Viewstate Values
        viewState["prevColumn"] = e.SortExpression;
        viewState["prevDirection"] = e.SortDirection;
        SetgvHeaderSortCss(g, e.SortExpression, e.SortDirection.ToString().ToLower(), "sort");
        return ret;
    }

    public void ShowModal(Page page, string divId)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("$(function showModal(){");
        sb.Append("\n");
        sb.Append("$('#" + divId + "').modal('show');");
        sb.Append("\n");


        //sb.Append("$('#code').on('shown.bs.modal', function(e) {");
        //sb.Append("\n");
        //sb.Append("$(\"#MainContent_" + divId + "\").DataTable();");
        //sb.Append("\n");
        //sb.Append("})");
        //sb.Append("\n");



        sb.Append("});");
        sb.Append("\n");

        ScriptManager.RegisterStartupScript(page, page.GetType(), "modal", sb.ToString(), true);
    }

    private void SetgvHeaderSortCss(GridView grid, string columnName, string direction, string defaultCss)
    {
        string sortClass = null;
        sortClass = "sort" + direction;
        for (int i = 0; i < grid.Columns.Count; i++)
        {
            if (grid.Columns[i].SortExpression.ToLower().Trim() == columnName.ToLower().Trim())
            {
                grid.Columns[i].HeaderStyle.CssClass = sortClass; //Set sorted Css
            }
            else
            {
                grid.Columns[i].HeaderStyle.CssClass = defaultCss; //Clear Others Css
            }
        }
    }

    public virtual void JS_Modal_Redirect(string message, string RedirectrURL)
    {
        Page page = HttpContext.Current.Handler as Page;

        if (page != null)
        {
            // Use page instance.
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "JS_Modal_Redirect", string.Format("alert('{0}');window.location.href = '" + RedirectrURL + "'", message.Replace("'", @"\'").Replace("\n", "\\n").Replace("\r", "\\r")), true);

        }


    }



    //Error HAndling
    public void HandleResultJS(string message, bool alertOnly, Control control, string redirectPage = null)
    {
        string typeguess = null;
        List<string> successClues = new List<string>() { "success", "successful", "successfully", "yay" };
        List<string> failClues = new List<string>() { "fail", "failed", "failure", "uncaught", "can't", "cannot", "sorry", "not" };

        foreach (string word in message.Split(' '))
        {
            if (failClues.Contains(word.ToLower()))
            {
                typeguess = "danger";
                break;
            }
            else if (successClues.Contains(word.ToLower()))
            {
                typeguess = "success";
                break;
            }
            else
                typeguess = "info";
        }
        //if (alertOnly)
        //    JS_Modal(message, control);
        //else if (redirectPage != null)
        //    JS_Modal_Redirect(message, control, redirectPage);
        //else
        //    JS_Modal_Confirm(message, control);
        HandleResult((Page)control, typeguess ?? "info", message);
    }
    private void HandleResult(Page page, string type, string message = null, int fadeDuration = 7000)
    {
        HandleResult(type, message, fadeDuration);
    }


    public void HandleResult(string type, string message, int fadeDuration = 7000, bool sendEmail = false, string redirectUrl = "")
    {

        if (message != null)
        {
            if (message == "Thread was being aborted.")
                return;

            message = message.Replace("'", "\\'");
            message = message.Replace("\r", " ");
            message = message.Replace("\n", " ");

            if (message.Contains("truncated"))
                message = "Sorry, one of your fields has too much data in it.  If this is a businees need, plese tell IT so this limit can be increased.";
        }

        Page page = HttpContext.Current.Handler as Page;
        if (page != null)
        {
            //Either shown banner or redirect.
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                //window.alert("This post was successfully deleted.");
                //window.location.href = "window-location.html";
                //window.location.replace("http://www.w3schools.com");
                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", " $(function () {alert('" + message + "')" + "});", true);
                ScriptManager.RegisterStartupScript(page, page.GetType(), "redirect", " $(function () {window.location.replace('" + redirectUrl + "')});", true);
            }

            else
                ScriptManager.RegisterStartupScript(page, page.GetType(), "timeout", " $(function () {" + "slideAlert('" + type.ToLower() + "', '" + message + "', '" + fadeDuration + "')" + "});", true);
        }


        //log it
        switch (type.ToLower())
        {
            case "success":
                {
                    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, message, sendEmail);
                    break;
                }
            case "warning":
                {
                    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, message, sendEmail);
                    break;
                }
            case "fail":
                {

                    SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, message);
                    break;
                }
        }


    }




    //Security
    //IS user in any of a list of roles
    public bool IsInAnyRole(string userName, List<string> roles)
    {
        var userRoles = Roles.GetRolesForUser(userName);

        return userRoles.Any(u => roles.Contains(u));
    }


    public bool IsInternalUser()
    {

        if (Roles.IsUserInRole("buyer") || Roles.IsUserInRole("consign"))
            return false;
        if (Roles.IsUserInRole("sm_internal_executive") || Roles.IsUserInRole("sm_insp_user") || Roles.IsUserInRole("admin") || Roles.IsUserInRole("sm_internal"))
            return true;
        return false;

    }




    public static string HandleLoginExceptions(Exception ex)
    {
        if (ex.Message.Contains("The length of parameter"))
            return "The new password length must be at least 8 characters.";
        else if (ex.Message.Contains("Non alpha numeric characters"))
            return "The new password must have at least one non-alphanumeric character. (examples:  #, &, *)";
        else if (ex.Message.Contains("The parameter 'newPassword' must not be empty"))
            return "Please enter your new password.";
        else if (ex.Message.Contains("The parameter 'oldPassword' must not be empty"))
            return "Please enter your current password.";
        else
            return ex.Message;
    }



    public bool AllowedToPartSearch(string type)
    {
        string ip_address = NetworkLogic.GetVisitorIpAddress();
        //if (ip_address == "::1")        
        //    return true;

        if (SiliconExpertLogic.IPBlockBlackList.Contains(ip_address))
            return false;
        //Allow Whitelist
        if (SiliconExpertLogic.IPBlockWhiteList.Contains(ip_address))
            return true;
        //Check the rate Limit
        if (!CheckRateLimitbyIP(ip_address, type))
        {
            //if (ip_address != "::1")            
            NetworkLogic.BlockIP(ip_address);
            RedirectToCaptcha();
        }

        if (type.ToLower() == "detail")
            return HttpContext.Current.Request.IsAuthenticated;

        return true;
    }

    public static bool CheckRateLimitbyIP(string IP, string type)
    {
        if (IP == "::1")
            return true;
        DateTime lastSearchTime = DateTime.MinValue;
        using (RzDataContext rdc = new RzDataContext())
        {
            if (type == "detail")
                lastSearchTime = rdc.se_part_detail_summaries.Where(w => w.ip_address == IP).OrderByDescending(o => o.date_created).Select(s => s.date_created).FirstOrDefault() ?? DateTime.MinValue;
            if (type == "list")
                lastSearchTime = rdc.se_list_part_searches.Where(w => w.requestorIP == IP).Select(s => s.last_search).Max() ?? DateTime.MinValue;

            if (!CheckMaxSearchesPerInterval(rdc, IP, lastSearchTime, type))
                return false;
            if (!CheckMaxSearchesPerMinute(rdc, IP, lastSearchTime, type))
                return false;
            if (!CheckMaxSearchesPerHour(rdc, IP, lastSearchTime, type))
                return false;

        }

        return true;
    }

    private static bool CheckMaxSearchesPerInterval(RzDataContext rdc, string IP, DateTime lastSearchTime, string type, int interval = 10)
    {

        DateTime rightNow = DateTime.Now;
        DateTime intervalTime = rightNow.AddSeconds(-interval);
        int searchesInLastInterval = 0;
        if (type == "detail")
            searchesInLastInterval = rdc.se_part_detail_summaries.Where(w => w.ip_address == IP && w.date_created >= intervalTime).Count();

        if (type == "list")
            searchesInLastInterval = rdc.portal_searched_parts.Where(w => w.requestorIP == IP && w.date_created >= intervalTime).Count();

        //More than 3 in 10 seconds, won't get hit much but good to check.  
        if (searchesInLastInterval > 3)
        {

            //Google Bot?  Anyhow, this can decmate our count, so, block for 30
            SystemLogic.Email.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, IP + " has been rate-limited. (More than 3 searches in 10 seconds : " + searchesInLastInterval + ")", IP + "<br />" + HttpContext.Current.Request.Url.ToString());
            //Aritifical time limiter to precent fast lookups   

            return false;
        }
        return true;
    }
    public static bool CheckMaxSearchesPerMinute(RzDataContext rdc, string IP, DateTime lastSearchTime, string type)
    {
        DateTime OneMinuteAgo = DateTime.Now.AddMinutes(-1);
        //DateTime Now = DateTime.Now;
        int searchesPerMinute = 0;

        if (type == "detail")
            searchesPerMinute = rdc.se_part_detail_summaries.Where(w => w.ip_address == IP && (w.date_created >= OneMinuteAgo)).Count();

        if (type == "list")
            searchesPerMinute = rdc.portal_searched_parts.Where(w => w.date_created >= OneMinuteAgo && w.requestorIP == IP).Count();

        if (searchesPerMinute <= 10)//Only 10 in a minute, that's ok
            return true;
        else //AT more than per minute, take Action
        {

            SystemLogic.Email.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, IP + " has tried to search more than 10 parts in a minute.", IP + "<br />" + HttpContext.Current.Request.Url.ToString());
            return false;
        }
    }
    private static bool CheckMaxSearchesPerHour(RzDataContext rdc, string IP, DateTime lastSearchTime, string type)
    {
        if (IP == "::1")//Since I do a crazy amount of tests, suppress this check for Dev IP
            return true;

        DateTime oneHourAgo = DateTime.Now.AddHours(-1);

        int maxAllowedSearchesPerHour = 50;
        int searchesPerHour = 0;
        if (type == "detail")
            searchesPerHour = rdc.se_part_detail_summaries.Where(w => w.ip_address == IP && w.date_created >= oneHourAgo).Count();

        if (type == "list")
            searchesPerHour = rdc.portal_searched_parts.Where(w => w.requestorIP == IP && w.date_created >= oneHourAgo).Count();

        if (searchesPerHour > maxAllowedSearchesPerHour)
        {

            lastSearchTime = rdc.se_part_detail_summaries.Where(w => w.ip_address == IP && w.UserName == "Public / Anonymous").OrderByDescending(o => o.date_created).Select(s => s.date_created).FirstOrDefault() ?? DateTime.MinValue;
            SystemLogic.Email.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, IP + " has been rate-limited. (Max searches per hour : " + searchesPerHour + ")", IP + "<br />" + HttpContext.Current.Request.Url.ToString());
            return false;
        }
        return true;


    }



    public bool IsCaptchaNeeded(string ipAddress)
    {
        ip_blocking ipb = NetworkLogic.GetLastIPBLock(ipAddress);
        if (ipb == null)
            return false;
        if (ipb.captcha_needed ?? false == true)
        {
            if (ipb.captcha_accepted ?? false == false)
                return true;//Captcha Needed
        }
        return false;
    }
    public int GetTotalAcceptedCaptchasCurrentDay(string ip)
    {
        DateTime TwentyFourHoursAgo = DateTime.Now.AddHours(-24);
        int count = 0;
        using (sm_portalEntities pdc = new sm_portalEntities())
            pdc.ip_blocking.Where(w => w.ip_address == ip && w.captcha_accepted_date >= TwentyFourHoursAgo && w.captcha_accepted == true).Count();


        return count;
    }

    public bool CheckCaptchaByCountPerDay(int countPerDay = 3)
    {
        //We allow 3 searches per day for unauthenticated users.
        int dailyCount = GetCurrentDailyCount();
        if (dailyCount > countPerDay)
        {
            string ip = NetworkLogic.GetVisitorIpAddress();
            return false;
        }

        return true;

    }

    private int GetCurrentDailyCount()
    {
        throw new NotImplementedException();
    }







    public bool ValidateCaptcha(bool isDev = false)
    {
        string sessionID = NetworkLogic.SetSessionID(HttpContext.Current);
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Validating Captcha for Session ID " + sessionID);
        string EncodedResponse = HttpContext.Current.Request.Form["g-Recaptcha-Response"];
        bool IsCaptchaValid = (ReCaptcha.Validate(EncodedResponse, isDev) == "true" ? true : false);
        if (IsCaptchaValid)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Captcha succeeded for Session ID " + sessionID);
            return true;
        }
        else
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, "Captcha failed for Session ID " + sessionID, false);
            return false;
        }

    }




    public void AcceptCaptcha(string ip, string returnUrl)
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            ip_blocking ipb = pdc.ip_blocking.Where(w => w.ip_address == ip).OrderByDescending(o => o.block_start).FirstOrDefault();
            if (ipb != null)
            {
                ipb.captcha_needed = false;
                ipb.captcha_accepted = true;
                ipb.captcha_accepted_date = DateTime.Now;
                pdc.SaveChanges();
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "User Successully answered CAPTCHA.  Return Url: " + returnUrl);
            }
        }

        System.Threading.Thread.Sleep(3000); // THis should allow enough time not to re-trigger 5 second lockout.
        HttpContext.Current.Response.Redirect(returnUrl, false);
    }

    public bool CheckCaptchaByMuniutesPerDay(int minutesPerDay = 2) //Time to allow before another check is required, default 2
    {

        string ip = NetworkLogic.GetVisitorIpAddress();
        //IF somehow no IP, redirect to captcha?
        if (ip == null)
            RedirectToCaptcha();

        //Get the most recent IP Blocking
        ip_blocking todaysFirstBlock = NetworkLogic.GetTodaysFirstSessionIDBlock();
        //IF no block, set intitial block.
        if (todaysFirstBlock == null)
            todaysFirstBlock = NetworkLogic.BlockIP(ip);

        //IF the last block was more than minutesPerDay, notify of unregistered limit, return false. 
        DateTime now = DateTime.Now;

        //If latest IP block for user needs captcha, do that first.
        ip_blocking lastBlock = NetworkLogic.GetLastIPBLock(ip);
        if (lastBlock.captcha_needed ?? false == true)
            RedirectToCaptcha();


        TimeSpan duration = now - (DateTime)todaysFirstBlock.block_start;
        //There's plenty of built in methods in the timespan class to do what we need, i.e.
        double totalUsedSeconds = duration.TotalSeconds;
        double totalUsedMinutes = duration.TotalMinutes;
        if (totalUsedMinutes > minutesPerDay)
        {
            NetworkLogic.BlockIP(ip);
            RedirectToCaptcha();
            //SendMail("portal_debug@sensiblemicro.com", "ktill@sensiblemicro.com", "Daily Limit Reached for IP: " + ip, ip);
            return false;
        }
        return true;
    }


    public void RedirectToCaptcha()
    {
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "User Redirected to Captcha.  Return Url: " + HttpContext.Current.Request.Url.ToString());
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        if (path != "/Account/reCaptcha.aspx")
        {
            HttpContext.Current.Response.Redirect("/Account/reCaptcha.aspx?ReturnUrl=" + HttpContext.Current.Request.Url.ToString(), false);
        }

    }


    private void RedirectToRegister()
    {
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "User redirected to register page.");
        HttpContext.Current.Response.Redirect("/Account/register.aspx?r=partsearch_unregistered", false);
    }


    public void RedirectToSuccess(string rawUrl, string additionalText = null)
    {
        //Success Text
        //Request.RawUrl;
        string redirectUrl = rawUrl;
        if (!redirectUrl.Contains("?success=1"))
            //Append the Succcess querystring
            redirectUrl += "?success=1";
        //Additional Text, set to hiddenfield in Master Page.
        //Current Page
        //Page page = HttpContext.Current.Handler as Page;
        //if (!string.IsNullOrEmpty(additionalText))
        //{
        //    //redirectUrl += "&adt=" + additionalText;
        //    HiddenField hfAddText = page.Master.FindControl("hfAddText") as HiddenField;
        //    if (hfAddText != null)
        //        hfAddText.Value = HttpUtility.HtmlEncode(additionalText);
        //}
        //Encode for Redirect
        //redirectUrl = HttpUtility.UrlEncode(redirectUrl);
        //Log the event
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "User redirected to '" + redirectUrl + "'");
        //Do the Redirect
        HttpContext.Current.Response.Redirect(redirectUrl);
    }


    public void LoadSuccessQueryString(HttpRequest request)
    {
        //Request.RawUrl
        string successText = HttpUtility.UrlDecode(request.QueryString["success"]);
        if (string.IsNullOrEmpty(successText))
            return;

        //Main Success Text
        string successResponse = "Success!";
        if (!string.IsNullOrEmpty(successText))
            if (successText != "1")//default value, don't update successResponse
                successResponse = successText;//Customer value, update successResponse

        //Additional Text, from hiddenfield since can contain HTML, thus unsuitable for querystring
        string addText = "";
        //Current Page
        //Page page = HttpContext.Current.Handler as Page;
        ////Find the Hiddenfield in current page
        //HiddenField hfAddText = page.Master.FindControl("hfAddText") as HiddenField;
        //if (hfAddText != null)
        //{
        //    //Confirm contents
        //    if (!String.IsNullOrEmpty(hfAddText.Value))
        //        addText = HttpUtility.HtmlDecode(hfAddText.Value);
        //    if (!string.IsNullOrEmpty(addText))
        //        successResponse += addText;
        //}


        //Handle the result
        HandleResult("success", successResponse);
    }












    //Email functions
    public void SentTmpPasswordEmail(MembershipUser u)
    {
        if (u != null)
        {
            string tmpPassword = System.Web.Security.Membership.GeneratePassword(12, 4);
            u.ChangePassword(u.GetPassword(), tmpPassword);
            System.Web.Security.Membership.UpdateUser(u);
            string StrContent = "";


            StringBuilder strb = new StringBuilder();
            try
            {

                using (FileStream fs = new FileStream("~/EmailTemplates/PasswordRecoveryEmail.txt", FileMode.Open))
                {
                    StreamReader reader = new StreamReader(fs);
                    string readFile = reader.ReadToEnd();
                    StrContent = readFile;
                    StrContent = StrContent.Replace("[Password]", tmpPassword);
                }
                SendMail(SystemLogic.Email.EmailGroupAddress.PortalWelcome, SystemLogic.Email.EmailGroup.PortalAlerts, "Password Reset", strb.ToString(), null, null);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
        }
    }

    public void SendNewUserEmail(MembershipUser u, string tmpPassword)
    {
        if (u != null)
        {
            string email = u.Email;
            //needs password reset
            //u.Comment = SM_Security.ValidationStatus.NeedsTempPasswordReset.ToString();
            SM_Security.SetTemporaryPasswordStatus(u, true);
            System.Web.Security.Membership.UpdateUser(u);

            string StrContent = "";
            StringBuilder strb = new StringBuilder();
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplates/NewUser.htm"));
            string readFile = reader.ReadToEnd();
            StrContent = readFile;
            StrContent = StrContent.Replace("[FirstName]", "");
            StrContent = StrContent.Replace("[UserName]", u.UserName);
            StrContent = StrContent.Replace("[Password]", tmpPassword);
            strb.Append(StrContent);
            string[] bcc = new string[] { SystemLogic.Email.EmailGroup.PortalAlerts };
            SendMail(SystemLogic.Email.EmailGroupAddress.PortalWelcome, u.Email, "Welcome to the Sensible Micro Portal", strb.ToString(), null, bcc, true);
        }
    }

    public void SendMail(SystemLogic.Email.EmailGroupAddress fromAddress, SystemLogic.Email.EmailGroup toGroup, string subject, string body, string[] cc = null, string[] bcc = null, bool isHTML = true, string smtpSrv = null, string fromName = "")
    {
        SensibleDAL.SystemLogic.Email.SendMail(fromAddress.ToString(), toGroup.ToString(), subject, body, cc, bcc, isHTML, smtpSrv, fromName);
    }


    public void SendMail(string from, string to, string subject, string body, string[] cc = null, string[] bcc = null, bool isHTML = true, string smtpSrv = null, string fromName = "")
    {
        SensibleDAL.SystemLogic.Email.SendMail(from, to, subject, body, cc, bcc, isHTML, smtpSrv, fromName);
    }



    public void SendMailWithAttachment(string from, string to, string subject, string body, MemoryStream attachment, string attachmentName, string[] cc = null, string[] bcc = null, bool isHTML = true)
    {
        using (var writer = new StreamWriter(attachment))    // using UTF-8 encoding by default
        using (var mailClient = new SmtpClient("smtp.sensiblemicro.local", 25))
        using (var message = new MailMessage(from, to, subject, body))
        {
            writer.WriteLine("Comma,Seperated,Values,...");
            writer.Flush();
            attachment.Position = 0;     // read from the start of what was written

            message.Attachments.Add(new Attachment(attachment, attachmentName, System.Net.Mime.MediaTypeNames.Application.Pdf));

            mailClient.Send(message);
        }
    }


    public void SendQuoteEmail(List<string> to, string subject, string bodyText, dealheader theBatch, bool debug = false, string[] cc = null, string[] bcc = null)
    {
        try
        {
            MailMessage Msg = new MailMessage();
            Msg.From = new MailAddress("sm_quotes@sensiblemicro.com");
            foreach (string s in to)
            {
                Msg.To.Add(s);
            }
            //Add Portal Alerts       
            if (!debug)
                Msg.To.Add(SystemLogic.Email.EmailGroup.PortalAlerts);


            string from_email = SystemLogic.Email.EmailGroupAddress.PortalQuote;
            string from_name = from_email;

            //SmtpClient objsmtp = new SmtpClient("smtp.sensiblemicro.local", 25);
            Msg.IsBodyHtml = true;
            Msg.Subject = subject;
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplates/portal_quote_request.htm"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;

            //Set base Body Information.
            if (!string.IsNullOrEmpty(bodyText))
                StrContent = StrContent.Replace("[BodyHtml]", bodyText);
            else
                StrContent = StrContent.Replace("[BodyHtml]", "No Quote Details Available");

            //Set Batch Information
            string batchHtml = GenerateBatchHtml(theBatch);
            StrContent = StrContent.Replace("[BatchHtml]", batchHtml);

            Msg.Body = StrContent.ToString();
            if (bcc != null)
                foreach (string s in bcc)
                    Msg.Bcc.Add(s);
            if (cc != null)
                foreach (string s in cc)
                    Msg.CC.Add(s);


            Dictionary<string, string> toAddresses = new Dictionary<string, string>();
            //this us redundant
            if (bcc != null)
                foreach (string s in bcc)
                    if (!toAddresses.ContainsKey(s))
                        toAddresses.Add(s, s);
            if (cc != null)
                foreach (string s in cc)
                    if (!toAddresses.ContainsKey(s))
                        toAddresses.Add(s, s);
            foreach (string s in to)
                toAddresses.Add(s, s);

            //If we wait, the Progress Spinner will keep spinning on pages.
            SystemLogic.Email.SendSendGridEmail_Single_ToMulti(from_email, from_email, subject, toAddresses, null, Msg.Body);
            


            

            //objsmtp.EnableSsl = false;
            //objsmtp.Send(Msg);

        }
        catch (Exception ex)
        {
            string test = ex.Message;
        }
    }

    private string GenerateBatchHtml(dealheader theBatch)
    {
        string batchTitle = "No Batch Details Available";
        if (theBatch == null)
            return batchTitle;

        batchTitle = "<p style=\"font-size:16px; font-weight:bold;\">Batch Information:</p>";
        string batchName = theBatch.dealheader_name ?? "<em>Not Available.</em>";
        string batchCompanyName = theBatch.customer_name ?? "<em>Not Available.</em>";
        string batchContactName = theBatch.contact_name ?? "<em>Not Available.</em>";
        string batchNotes = theBatch.notes ?? "<em>Not Available.</em>";

        string ret = batchTitle;
        ret += "<strong>Batch Name:&nbsp;</strong>" + batchName + "<br />";
        ret += "<strong>Company:&nbsp;</strong>" + batchCompanyName + "<br />";
        ret += "<strong>Contact:&nbsp;</strong>" + batchContactName + "<br />";
        ret += "<strong>Batch Notes:&nbsp;</strong>" + batchNotes + "<br />";

        return ret;

    }

    public void SendQuoteEmail(List<string> to, string subject, string partnumber, string qty, string tPrice, string description, string custEmail, string custPhone, string firstName, string lastName, bool batchCreated, company comp, Page page, string[] cc = null, string[] bcc = null)
    {
        try
        {

            string ip = NetworkLogic.GetVisitorIpAddress();

            MailMessage Msg = new MailMessage();
            Msg.From = new MailAddress("sm_quotes@sensiblemicro.com");
            foreach (string s in to)
            {
                Msg.To.Add(s);
            }
            SmtpClient objsmtp = new SmtpClient("smtp.sensiblemicro.local", 25);
            Msg.IsBodyHtml = true;
            Msg.Subject = subject;
            StreamReader reader = new StreamReader(page.Server.MapPath("~/EmailTemplates/quote_request.htm"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;


            if (string.IsNullOrEmpty(tPrice))
                tPrice = "Not provided";
            else
                tPrice = "$" + tPrice;
            if (string.IsNullOrEmpty(description))
                tPrice = "Not provided";

            string batchCreatedMsg = "No.";
            if (batchCreated)
                batchCreatedMsg = "Yes.";

            string companyName;
            if (comp != null)
                companyName = comp.companyname;
            else
                companyName = "No Company Found";
            StrContent = StrContent.Replace("[PartNumber]", partnumber);
            StrContent = StrContent.Replace("[Qty]", qty);
            StrContent = StrContent.Replace("[TargetPrice]", tPrice);
            StrContent = StrContent.Replace("[Notes]", description);
            StrContent = StrContent.Replace("[CompanyName]", companyName);
            StrContent = StrContent.Replace("[CustEmail]", custEmail);
            StrContent = StrContent.Replace("[CustPhone]", custPhone ?? "Not provided");
            StrContent = StrContent.Replace("[BatchCreated]", batchCreatedMsg);
            StrContent = StrContent.Replace("[IpAddr]", ip);
            StrContent = StrContent.Replace("[FirstName]", firstName);
            StrContent = StrContent.Replace("[LastName]", lastName);

            Msg.Body = StrContent.ToString();
            if (bcc != null)
                foreach (string s in bcc)
                    Msg.Bcc.Add(s);
            if (cc != null)
                foreach (string s in cc)
                    Msg.CC.Add(s);
            objsmtp.EnableSsl = false;
            objsmtp.Send(Msg);

        }
        catch (Exception ex)
        {
            string test = ex.Message;
        }
    }

    public class Colors
    {

        private static readonly string _smNavyRgba = "rgba(36, 55, 70)";
        private static readonly string _smRedRgba = "rgba(238, 39, 56)";
        private static readonly string _smGreenRgba = "rgba(110, 155, 0)";
        private static readonly string _smOrangeRgba = "rgba(246, 125, 40)";
        private static readonly string _smLightBlueRgba = "rgba(25, 155, 137)";
        private static readonly string _smBlackRgba = "rgba(0,0,0)";
        private static readonly string _smWhiteRgba = "rgba(255,255,255)";

        public static string smNavyRgba { get { return _smNavyRgba; } }
        public static string smRedRgba { get { return _smRedRgba; } }
        public static string smGreenRgba { get { return _smGreenRgba; } }
        public static string smOrangeRgba { get { return _smOrangeRgba; } }
        public static string smLightBlueRgba { get { return _smLightBlueRgba; } }
        public static string smBlackRgba { get { return _smBlackRgba; } }
        public static string smWhiteRgba { get { return _smWhiteRgba; } }


        public static string GetRandomColor(Random rnd)
        {

            Color randomColor = Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
            string hex = ColorTranslator.ToHtml(randomColor);
            return hex;
        }


    }










    //Data Management



    //Export to CSV
    public void ExportCSV(string vendoruid, string filename, string sql)
    {
        string constr = ConfigurationManager.ConnectionStrings["Rz3ConnectionString"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(sql))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);

                        //Build the CSV file data as a Comma separated string.
                        string csv = string.Empty;

                        foreach (DataColumn column in dt.Columns)
                        {
                            //Add the Header row for CSV file.
                            csv += column.ColumnName + ',';
                        }

                        //Add new line.
                        csv += "\r\n";

                        foreach (DataRow row in dt.Rows)
                        {
                            foreach (DataColumn column in dt.Columns)
                            {
                                //Add the Data rows.
                                csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
                            }

                            //Add new line.
                            csv += "\r\n";
                        }
                        //Download the CSV file.
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.Buffer = true;
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                        HttpContext.Current.Response.Charset = "";
                        HttpContext.Current.Response.ContentType = "application/text";
                        HttpContext.Current.Response.Output.Write(csv);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                }
            }
        }
    }

    //public static string QuoteValue(string value)
    //{
    //    return String.Concat("\"",
    //    value.Replace("\"", "\"\""), "\"");
    //}
    //End Export DataTable to CSV



    public void ExportDataTableToCsv(DataTable dt, string fileName)
    {
        StringBuilder sb = new StringBuilder();

        IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                          Select(column => column.ColumnName);
        sb.AppendLine(string.Join(",", columnNames));

        foreach (DataRow row in dt.Rows)
        {
            IEnumerable<string> fields = row.ItemArray.Select(field =>
              string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
            sb.AppendLine(string.Join(",", fields));
        }

        //File.WriteAllText(fileName+".csv", sb.ToString());
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".csv");
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.ContentType = "application/text";
        HttpContext.Current.Response.Output.Write(sb.ToString());
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }
    public void ExportListToCsv<T>(List<T> list, string fileName)
    {

        //ExportDataTableToCsv(DataTable dt, string fileName);
        DataTable dt = Lists.ToDataTable(list);
        if (dt != null)
            ExportDataTableToCsv(dt, fileName);
    }


    //String Tools
    //String Functions
    public string ConvertToUnicode(string fromString)
    {
        //fromString = "This string contains the unicode character Pi (\u03a0)";



        // Create two different encodings.
        Encoding ascii = Encoding.ASCII;
        Encoding unicode = Encoding.Unicode;

        // Convert the string into a byte array.
        byte[] unicodeBytes = unicode.GetBytes(fromString);

        // Perform the conversion from one encoding to the other.
        byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

        // Convert the new byte[] into a char[] and then into a string.
        char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
        ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
        string asciiString = new string(asciiChars);

        // Display the strings created before and after the conversion.
        Console.WriteLine("Original string: {0}", fromString);
        Console.WriteLine("Ascii converted string: {0}", asciiString);
        return asciiString;

    }


    public string GetPartNumberFromWords(string input, string type, bool capitalize)
    {

        var punctuation = input.Where(Char.IsPunctuation).Distinct().ToArray();
        List<string> words = input.Split().Select(x => x.Trim(punctuation)).ToList();
        return ProcessWordsToPartNumber(words, type, capitalize);

    }

    private string ProcessWordsToPartNumber(List<string> words, string type, bool capitalize)
    {
        string ret = "";
        foreach (string s in words)
        {
            string punc = StringIsPunctuation(s);
            if (!string.IsNullOrEmpty(punc))
                ret += punc;
            else if (StringIsANumber(s))
            {
                ret += ToLong(s).ToString();
            }
            else
                switch (type)
                {
                    case "wordmatch":
                        ret += capitalize ? ConvertWorsToLetter(s).ToUpper() : ConvertWorsToLetter(s).ToLower();
                        break;
                    case "firstletter":
                        ret += capitalize ? GetFirstLetterOfWord(s).ToUpper() : GetFirstLetterOfWord(s).ToLower();
                        break;
                }

        }

        return ret;
    }

    private string StringIsPunctuation(string s)
    {
        string ret = "";
        switch (s.ToLower())
        {
            case "-":
            case "dash":
            case "hyphen":
            case "minus":
                return "-";
            case "/":
            case "slash":
                return "/";
            case "\\":
            case "backslash":
                return "\\";
            case ".":
            case "point":
            case "dot":
            case "period":
                return ".";
            case ",":
            case "comma":
                return ",";
            case "#":
            case "pound":
            case "hash":
            case "hashtag":
                return "#";
            case "$":
            case "dollarsign":
                return "$";
            case "€":
            case "euro":
                return "€";
            case "&":
            case "ampersand":
            case "and":
                return "&";
            case "?":
            case "question":
                return "?";
        }


        return ret;
    }

    public bool StringIsANumber(string s)
    {
        if (numberTable.ContainsKey(s))
            return true;
        return false;
    }

    private string ConvertWorsToLetter(string s)
    {
        switch (s.ToLower())
        {
            case "a":
            case "alpha":
            case "apple":
            case "adams":
                return "a";
            case "b":
            case "bravo":
            case "baker":
            case "boston":
                return "b";
            case "c":
            case "charlie":
            case "cookie":
            case "chicago":
                return "c";
            case "d":
            case "delta":
            case "denver":
                return "d";
            case "e":
            case "echo":
            case "easy":
            case "edgar":
                return "e";
            case "f":
            case "foxtrot":
            case "frank":
                return "f";
            case "g":
            case "golf":
            case "george":
                return "g";
            case "h":
            case "hotel":
            case "henry":
                return "h";
            case "i":
            case "india":
            case "ida":
            case "indigo":
                return "i";
            case "j":
            case "juliet":
            case "john":
            case "jump":
                return "j";
            case "k":
            case "kilo":
            case "king":
                return "k";
            case "l":
            case "lima":
            case "lincoln":
                return "l";
            case "m":
            case "mike":
            case "mary":
                return "m";
            case "n":
            case "in":
            case "november":
            case "new york":
            case "nancy":
                return "n";
            case "o":
            case "oscar":
            case "ocean":
                return "o";
            case "p":
            case "papa":
            case "peter":
                return "p";
            case "q":
            case "quebec":
            case "queen":
                return "q";
            case "r":
            case "romeo":
            case "roger":
                return "r";
            case "s":
            case "sierra":
            case "sugar":
            case "sam":
                return "s";
            case "t":
            case "tango":
            case "thomas":
                return "t";
            case "u":
            case "uniform":
            case "union":
            case "umbrella":
                return "u";
            case "v":
            case "victor":
            case "victory":
                return "v";
            case "w":
            case "whiskey":
            case "william":
                return "w";
            case "x":
            case "x-ray":
            case "xray":
                return "x";
            case "y":
            case "yankee":
            case "young":
                return "y";
            case "z":
            case "zulu":
            case "zero":
            case "zed":
            case "zebra":
                return "z";
            default:
                return "";
        }
    }

    public string GetFirstLetterOfWord(string s)
    {
        if (string.IsNullOrEmpty(s))
            return "";
        return s.Substring(0, 1);
    }


    public Dictionary<string, long> numberTable =
    new Dictionary<string, long>
        {{"zero",0},{"0",0},{"won",1},{"one",1},{"2",2},{"two",2},{"too",2},{"to",2},{"3",3},{"three",3},{"4",4},{"four",4},{"for",4},{"fore",4},
        {"five",5},{"5",5},{"six",6},{"6",6},{"sicks",6},{"seven",7},{"7",7},{"eight",8},{"8",8},{"ate",8},{"nine",9},{"9",9},
        {"ten",10},{"10",10},{"eleven",11},{"11",11},{"twelve",12},{"12",12},{"thirteen",13},{"13",13},
        {"fourteen",14},{"14",14},{"fifteen",15},{"15",15},{"sixteen",16},{"16",16},
        {"seventeen",17},{"17",17},{"eighteen",18},{"18",18},{"nineteen",19},{"19",19},{"twenty",20},{"20",20},
        {"thirty",30},{"30",30},{"forty",40},{"40",40},{"fifty",50},{"50",50},{"sixty",60},{"60",60 },
        {"seventy",70},{"70",70},{"eighty",80},{"80",80},{"ninety",90},{"90",90},{"hundred",100},{"100",100},
        {"thousand",1000},{"million",1000000},{"billion",1000000000},
        {"trillion",1000000000000},{"quadrillion",1000000000000000},
        {"quintillion",1000000000000000000}};
    public long ToLong(string numberString)
    {
        var numbers = Regex.Matches(numberString, @"\w+").Cast<Match>()
             .Select(m => m.Value.ToLowerInvariant())
             .Where(v => numberTable.ContainsKey(v))
             .Select(v => numberTable[v]);
        long acc = 0, total = 0L;
        foreach (var n in numbers)
        {
            if (n >= 1000)
            {
                total += (acc * n);
                acc = 0;
            }
            else if (n >= 100)
            {
                acc *= n;
            }
            else acc += n;
        }
        return (total + acc) * (numberString.StartsWith("minus",
              StringComparison.InvariantCultureIgnoreCase) ? -1 : 1);
    }



    public string ConvertUrlsToLinks(string url, string anchorText = null, string titleText = null)
    {
        if (anchorText == null)
            anchorText = url;
        if (titleText == null)
            titleText = anchorText;
        //string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
        //Regex r = new Regex(regex, RegexOptions.IgnoreCase);
        // return r.Replace(url, "<a href=\"$1\" title=\"Click to open in a new window or tab\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
        string linkString = "<a href=\"$url\" title=\"$titleText\" target=\"&#95;blank\">$anchorText</a>";
        linkString = linkString.Replace("$url", url);
        linkString = linkString.Replace("$anchorText", anchorText);
        linkString = linkString.Replace("$titleText", titleText);
        return linkString;


    }

    public string CapitalizeOneFirstLetter(string str)
    {
        if (str == null)
            return null;

        if (str.Length > 1)
            return char.ToUpper(str[0]) + str.Substring(1);

        return str.ToUpper();

    }

    public string CapitalizeAllFirstLetters(string str)
    {
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
    }




    public int GetCountOfWordsInString(string s)
    {


        int result = 0;
        //Trim whitespace from beginning and end of string
        s = s.Trim();

        //Necessary because foreach will execute once with empty string returning 1
        if (s == "")
            return 0;
        //Ensure there is only one space between each word in the passed string
        while (s.Contains("  "))
            s = s.Replace("  ", " ");

        //Count the words
        foreach (string y in s.Split(' '))
            result++;

        return result;


    }

    public bool DoesStringContainString(string searchString, string keyword)
    {
        if (searchString.Contains(keyword))
            return true;
        return false;
    }




    public string concatStringwithSeparator(string baseString, string newString, string separator = ",")
    {
        string ret = baseString;
        if (!string.IsNullOrEmpty(baseString))
            ret += separator + " ";
        ret += newString;
        return ret;

    }

    public bool StringExistsRegex(string matchString, string pattern)
    {
        string text = matchString;
        string pat = pattern;
        Regex r = new Regex(pat, RegexOptions.IgnoreCase);
        Match m = r.Match(text);
        return m.Success;

    }

    public bool ValidateInputStringRegex(string s)
    {
        //KT found this, might be useful, not using until I learn what the Regex is doing.
        if (!Regex.IsMatch(s, @"^\d{3}-\d{2}-\d{4}$"))
            return true;
        return false;

    }
    public bool ValidateInputStringRegexAlphaNumberic(string s)
    {
        if (Regex.IsMatch(s, "^[a-zA-Z0-9_]*$"))
            return true;
        return false;

    }
    public bool ValdateInputStringInt(string s)
    {
        int value;
        if (int.TryParse(s, out value))
            return true;
        return false;
    }



    public bool ValdateInputStringDecimal(string s)
    {
        //KT Confirm text field contains only Numbers or decimals (for 1, .5, 1.5, etc)
        if (Regex.IsMatch(s, "^[1 - 9]\\d * (\\.\\d +)?$"))
            return true;
        return false;
    }

    public bool ValidateInputStringDateTime(string s)
    {
        DateTime dt;
        if (DateTime.TryParseExact("08/30/2009", "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
        {
            return false;
        }
        return true;

    }
    public bool ValidateEmailAddress(string s)
    {
        //!!Make sure to trim the usernam (email) un user-cretion too!  Regex is valid because it's ignoring whitespace, but that could allow whitespaces in username if not sanitized.
        s = s.Trim();
        Regex regex = new Regex(@"^\s*([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)\s*$");
        Match match = regex.Match(s);
        if (match.Success)
            return true;
        return false;
    }

    public bool IsPublicEmailDomain(string emailAddress)
    {
        List<string> publicDomainSuffixList = new List<string>() {
        "yahoo.com",
        "hotmail.com",
        "aol.com",
        "hotmail.co.uk",
        "hotmail.fr",
        "msn.com",
        "yahoo.fr",
        "wanadoo.fr",
        "orange.fr",
        "comcast.net",
        "yahoo.co.uk",
        "yahoo.com.br",
        "yahoo.co.in",
        "live.com",
        "rediffmail.com",
        "free.fr",
        "gmx.de",
        "web.de",
        "yandex.ru",
        "ymail.com",
        "libero.it",
        "outlook.com",
        "uol.com.br",
        "bol.com.br",
        "mail.ru",
        "cox.net",
        "hotmail.it",
        "sbcglobal.net",
        "sfr.fr",
        "live.fr",
        "verizon.net",
        "live.co.uk",
        "googlemail.com",
        "yahoo.es",
        "ig.com.br",
        "live.nl",
        "bigpond.com",
        "terra.com.br",
        "yahoo.it",
        "neuf.fr",
        "yahoo.de",
        "alice.it",
        "rocketmail.com",
        "att.net",
        "laposte.net",
        "facebook.com",
        "bellsouth.net",
        "yahoo.in",
        "hotmail.es",
        "charter.net",
        "yahoo.ca",
        "yahoo.com.au",
        "rambler.ru",
        "hotmail.de",
        "tiscali.it",
        "shaw.ca",
        "yahoo.co.jp",
        "sky.com",
        "earthlink.net",
        "optonline.net",
        "freenet.de",
        "t-online.de",
        "aliceadsl.fr",
        "virgilio.it",
        "home.nl",
        "qq.com",
        "telenet.be",
        "me.com",
        "yahoo.com.ar",
        "tiscali.co.uk",
        "yahoo.com.mx",
        "voila.fr",
        "gmx.net",
        "mail.com",
        "planet.nl",
        "tin.it",
        "live.it",
        "ntlworld.com",
        "arcor.de",
        "yahoo.co.id",
        "frontiernet.net",
        "hetnet.nl",
        "live.com.au",
        "yahoo.com.sg",
        "zonnet.nl",
        "club-internet.fr",
        "juno.com",
        "optusnet.com.au",
        "blueyonder.co.uk",
        "bluewin.ch",
        "skynet.be",
        "sympatico.ca",
        "windstream.net",
        "mac.com",
        "centurytel.net",
        "chello.nl",
        "live.ca",
        "aim.com",
        "bigpond.net.au",
        };


        string emailSuffix = "";
        string[] emailSplit = emailAddress.Trim().ToLower().Split('@');
        if (emailSplit.Count() > 0)
            emailSuffix = emailSplit[1];
        if (publicDomainSuffixList.Contains(emailSuffix))
            return true;
        return false;
    }

    public bool ValidateNonGenericEmailAddress(string email)
    {
        //!!Make sure to trim the usernam (email) un user-cretion too!  Regex is valid because it's ignoring whitespace, but that could allow whitespaces in username if not sanitized.
        MailAddress emailAddress = new MailAddress(email.ToLower().Trim());
        string domain = emailAddress.Host;
        //Regex regex = new Regex(@"^\s*([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)\s*$");
        //Match match = regex.Match(s);         
        List<string> genericDomains = new List<string>() { "gmail.com", "hotmail.com", "outlook.com", "msn.com", "mail.com", "apple.com", "aol.com", "yahoo.com", "inbox.com", "icloud.com", "zoho.com", "yandex" };
        return !genericDomains.Contains(domain);//True if generic domains does not contain the string.
    }

    public void ValidateImageFileType(string fileName)
    {
        List<string> AllowedMIMETypes = new List<string>();
        AllowedMIMETypes.Add("image/jpeg");
        AllowedMIMETypes.Add("image/gif");
        AllowedMIMETypes.Add("image/png");
        AllowedMIMETypes.Add("image/bmp");
        string AllowedMIMETypesString = string.Join(",", AllowedMIMETypes.ToArray());
        string ext = SM_Tools.MIMEAssistant.GetMIMEType(fileName);


        if (!AllowedMIMETypes.Contains(ext))
        {
            throw new Exception("Sorry Homey, that file type isn't allowed.  Allowed filetypes are: " + AllowedMIMETypesString);
        }

    }



    public string TrimAllWhiteSpace(string s)
    {
        string ret = s.Replace(" ", "");
        return ret;
    }



    public string trimControlNumberSuffix(string s)
    {
        string ret = "";
        string pattern = @"\d+$"; //Numerics at end of string
        string replacement = "";
        Regex rgx = new Regex(pattern);
        ret = rgx.Replace(s, replacement);
        return ret;
    }




    //Page Control Methods
    public void GetControlList<T>(ControlCollection controlCollection, List<T> resultCollection) where T : Control
    {
        foreach (Control control in controlCollection)
        {
            if (control is T)
                resultCollection.Add((T)control);
            if (control.HasControls())
                GetControlList(control.Controls, resultCollection);
        }
    }

    public void DisableAllControls(Control parent, bool customerMode)
    {
        bool enabled = !customerMode;
        foreach (Control c in parent.Controls)
        {
            if (c is DropDownList)
            {
                ((DropDownList)(c)).Enabled = enabled;
            }
            if (c is FileUpload)
            {
                ((FileUpload)(c)).Enabled = enabled;
            }
            if (c is LinkButton)
            {
                ((LinkButton)(c)).Enabled = enabled;
            }
            if (c is Button)
            {
                ((Button)(c)).Enabled = enabled;
            }
            if (c is TextBox)
            {
                ((TextBox)(c)).Enabled = enabled;
            }
            if (c is CheckBox)
            {
                ((CheckBox)(c)).Enabled = enabled;
            }
            if (c is CheckBoxList)
            {
                ((CheckBoxList)(c)).Enabled = enabled;
            }
            DisableAllControls(c, customerMode); //KEep this as custoemr mode, else it will get negated on each iteration.
        }
    }

    //End Page Control Methods

    //Math Tools
    public decimal RoundtoNearestHalf(decimal d)
    {
        decimal dd = d * 2;
        dd = Math.Round(dd);
        dd = dd / 2;
        return dd;
    }

    public static class MIMEAssistant
    {
        private static readonly Dictionary<string, string> MIMETypesDictionary = new Dictionary<string, string>
  {
    {"ai", "application/postscript"},
    {"aif", "audio/x-aiff"},
    {"aifc", "audio/x-aiff"},
    {"aiff", "audio/x-aiff"},
    {"asc", "text/plain"},
    {"atom", "application/atom+xml"},
    {"au", "audio/basic"},
    {"avi", "video/x-msvideo"},
    {"bcpio", "application/x-bcpio"},
    {"bin", "application/octet-stream"},
    {"bmp", "image/bmp"},
    {"cdf", "application/x-netcdf"},
    {"cgm", "image/cgm"},
    {"class", "application/octet-stream"},
    {"cpio", "application/x-cpio"},
    {"cpt", "application/mac-compactpro"},
    {"csh", "application/x-csh"},
    {"css", "text/css"},
    {"dcr", "application/x-director"},
    {"dif", "video/x-dv"},
    {"dir", "application/x-director"},
    {"djv", "image/vnd.djvu"},
    {"djvu", "image/vnd.djvu"},
    {"dll", "application/octet-stream"},
    {"dmg", "application/octet-stream"},
    {"dms", "application/octet-stream"},
    {"doc", "application/msword"},
    {"docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
    {"dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
    {"docm","application/vnd.ms-word.document.macroEnabled.12"},
    {"dotm","application/vnd.ms-word.template.macroEnabled.12"},
    {"dtd", "application/xml-dtd"},
    {"dv", "video/x-dv"},
    {"dvi", "application/x-dvi"},
    {"dxr", "application/x-director"},
    {"eps", "application/postscript"},
    {"etx", "text/x-setext"},
    {"exe", "application/octet-stream"},
    {"ez", "application/andrew-inset"},
    {"gif", "image/gif"},
    {"gram", "application/srgs"},
    {"grxml", "application/srgs+xml"},
    {"gtar", "application/x-gtar"},
    {"hdf", "application/x-hdf"},
    {"hqx", "application/mac-binhex40"},
    {"htm", "text/html"},
    {"html", "text/html"},
    {"ice", "x-conference/x-cooltalk"},
    {"ico", "image/x-icon"},
    {"ics", "text/calendar"},
    {"ief", "image/ief"},
    {"ifb", "text/calendar"},
    {"iges", "model/iges"},
    {"igs", "model/iges"},
    {"jnlp", "application/x-java-jnlp-file"},
    {"jp2", "image/jp2"},
    {"jpe", "image/jpeg"},
    {"jpeg", "image/jpeg"},
    {"jpg", "image/jpeg"},
    {"js", "application/x-javascript"},
    {"kar", "audio/midi"},
    {"latex", "application/x-latex"},
    {"lha", "application/octet-stream"},
    {"lzh", "application/octet-stream"},
    {"m3u", "audio/x-mpegurl"},
    {"m4a", "audio/mp4a-latm"},
    {"m4b", "audio/mp4a-latm"},
    {"m4p", "audio/mp4a-latm"},
    {"m4u", "video/vnd.mpegurl"},
    {"m4v", "video/x-m4v"},
    {"mac", "image/x-macpaint"},
    {"man", "application/x-troff-man"},
    {"mathml", "application/mathml+xml"},
    {"me", "application/x-troff-me"},
    {"mesh", "model/mesh"},
    {"mid", "audio/midi"},
    {"midi", "audio/midi"},
    {"mif", "application/vnd.mif"},
    {"mov", "video/quicktime"},
    {"movie", "video/x-sgi-movie"},
    {"mp2", "audio/mpeg"},
    {"mp3", "audio/mpeg"},
    {"mp4", "video/mp4"},
    {"mpe", "video/mpeg"},
    {"mpeg", "video/mpeg"},
    {"mpg", "video/mpeg"},
    {"mpga", "audio/mpeg"},
    {"ms", "application/x-troff-ms"},
    {"msh", "model/mesh"},
    {"mxu", "video/vnd.mpegurl"},
    {"nc", "application/x-netcdf"},
    {"oda", "application/oda"},
    {"ogg", "application/ogg"},
    {"pbm", "image/x-portable-bitmap"},
    {"pct", "image/pict"},
    {"pdb", "chemical/x-pdb"},
    {"pdf", "application/pdf"},
    {"pgm", "image/x-portable-graymap"},
    {"pgn", "application/x-chess-pgn"},
    {"pic", "image/pict"},
    {"pict", "image/pict"},
    {"png", "image/png"},
    {"pnm", "image/x-portable-anymap"},
    {"pnt", "image/x-macpaint"},
    {"pntg", "image/x-macpaint"},
    {"ppm", "image/x-portable-pixmap"},
    {"ppt", "application/vnd.ms-powerpoint"},
    {"pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"},
    {"potx","application/vnd.openxmlformats-officedocument.presentationml.template"},
    {"ppsx","application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
    {"ppam","application/vnd.ms-powerpoint.addin.macroEnabled.12"},
    {"pptm","application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
    {"potm","application/vnd.ms-powerpoint.template.macroEnabled.12"},
    {"ppsm","application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
    {"ps", "application/postscript"},
    {"qt", "video/quicktime"},
    {"qti", "image/x-quicktime"},
    {"qtif", "image/x-quicktime"},
    {"ra", "audio/x-pn-realaudio"},
    {"ram", "audio/x-pn-realaudio"},
    {"ras", "image/x-cmu-raster"},
    {"rdf", "application/rdf+xml"},
    {"rgb", "image/x-rgb"},
    {"rm", "application/vnd.rn-realmedia"},
    {"roff", "application/x-troff"},
    {"rtf", "text/rtf"},
    {"rtx", "text/richtext"},
    {"sgm", "text/sgml"},
    {"sgml", "text/sgml"},
    {"sh", "application/x-sh"},
    {"shar", "application/x-shar"},
    {"silo", "model/mesh"},
    {"sit", "application/x-stuffit"},
    {"skd", "application/x-koan"},
    {"skm", "application/x-koan"},
    {"skp", "application/x-koan"},
    {"skt", "application/x-koan"},
    {"smi", "application/smil"},
    {"smil", "application/smil"},
    {"snd", "audio/basic"},
    {"so", "application/octet-stream"},
    {"spl", "application/x-futuresplash"},
    {"src", "application/x-wais-source"},
    {"sv4cpio", "application/x-sv4cpio"},
    {"sv4crc", "application/x-sv4crc"},
    {"svg", "image/svg+xml"},
    {"swf", "application/x-shockwave-flash"},
    {"t", "application/x-troff"},
    {"tar", "application/x-tar"},
    {"tcl", "application/x-tcl"},
    {"tex", "application/x-tex"},
    {"texi", "application/x-texinfo"},
    {"texinfo", "application/x-texinfo"},
    {"tif", "image/tiff"},
    {"tiff", "image/tiff"},
    {"tr", "application/x-troff"},
    {"tsv", "text/tab-separated-values"},
    {"txt", "text/plain"},
    {"ustar", "application/x-ustar"},
    {"vcd", "application/x-cdlink"},
    {"vrml", "model/vrml"},
    {"vxml", "application/voicexml+xml"},
    {"wav", "audio/x-wav"},
    {"wbmp", "image/vnd.wap.wbmp"},
    {"wbmxl", "application/vnd.wap.wbxml"},
    {"wml", "text/vnd.wap.wml"},
    {"wmlc", "application/vnd.wap.wmlc"},
    {"wmls", "text/vnd.wap.wmlscript"},
    {"wmlsc", "application/vnd.wap.wmlscriptc"},
    {"wrl", "model/vrml"},
    {"xbm", "image/x-xbitmap"},
    {"xht", "application/xhtml+xml"},
    {"xhtml", "application/xhtml+xml"},
    {"xls", "application/vnd.ms-excel"},
    {"xml", "application/xml"},
    {"xpm", "image/x-xpixmap"},
    {"xsl", "application/xml"},
    {"xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
    {"xltx","application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
    {"xlsm","application/vnd.ms-excel.sheet.macroEnabled.12"},
    {"xltm","application/vnd.ms-excel.template.macroEnabled.12"},
    {"xlam","application/vnd.ms-excel.addin.macroEnabled.12"},
    {"xlsb","application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
    {"xslt", "application/xslt+xml"},
    {"xul", "application/vnd.mozilla.xul+xml"},
    {"xwd", "image/x-xwindowdump"},
    {"xyz", "chemical/x-xyz"},
    {"zip", "application/zip"}
  };

        public static string GetMIMEType(string fileName)
        {
            //get file extension
            string extension = Path.GetExtension(fileName).ToLowerInvariant();

            if (extension.Length > 0 &&
                MIMETypesDictionary.ContainsKey(extension.Remove(0, 1)))
            {
                return MIMETypesDictionary[extension.Remove(0, 1)];
            }
            return "unknown/unknown";
        }
    }


    //String tools
    public List<string> GetListFromHiddenField(HiddenField hf)
    {
        List<string> ret = new List<string>();

        string hfContent = hf.Value;
        if (!string.IsNullOrEmpty(hfContent))
            ret = JsonConvert.DeserializeObject<List<string>>(hfContent);
        return ret;
    }

}

