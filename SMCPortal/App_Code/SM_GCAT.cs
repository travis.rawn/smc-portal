﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Collections.Generic;
using System.Linq;
using SensibleDAL.dbml;
using SensibleDAL.ef;

/// <summary>
/// Summary description for SM_PDF_GCAT
/// </summary>
public class SM_GCAT
{
    SM_PDF smpdf = new SM_PDF();
    SM_Tools tools = new SM_Tools();
    Page page = new Page();
    public int GCATID;
    RzDataContext rz = new RzDataContext();
    gcatDataContext ciq = new gcatDataContext();
    SM_Quality_Logic ql = new SM_Quality_Logic();
    sm_binaryEntities smb = new sm_binaryEntities();
    List<insp_images> listExtendedImagery = new List<insp_images>();

    //Variables
    ValidationReport vr;
    ManufacturerPart mp;
    MarkingPermanency mperm;
    XRay xray;
    HeatedSolvent hs;
    Decapsulation decap;
    Solderability sol;
    EPROM ep;
    SentryVI sv;
    XRF xrf;
    BasicElectrical be;
    ExtendedTesting et;
    MoistureBaking mb;
    Summary_new sum;
    string companyName;
    string accountManager;
    string customerTypes;
    string standardTests;
    string customerRequirements;
    string componentCondition;
    string testsPerformed;
    string extendedTestingCategory;
    bool testsGathered = false; //Boolean set if the tests have already been gathered to prevent unnecessary round trip, since GetTestsPerformed requires that they are gather and is called form other places like the gcat report that won't necessarily have gathered the tests.

    string dateCodes;

    byte[] bytes;//Trying to handle this as a memorystream so I don't need to save and delete.
    HttpContext context = HttpContext.Current;
    Document document;
    public string fileName;
    public string documentTitle;
    PdfPCell cell;//placeholder cell, will overwrite as needed
    PdfPTable table;//placeholder table, will overwrite as needed
    PdfPTable nested;//placeholder for nested tables




    //BackGroundColors
    BaseColor Gray = new BaseColor(224, 224, 224);
    BaseColor White = new BaseColor(255, 255, 255);

    //Font Style
    SM_PDFFont theFont = SM_PDFFont.GetFont("Montserrat");

    public SM_GCAT(int gcatID)
    {
        //
        // TODO: Add constructor logic here
        //  
        GCATID = gcatID;
    }


    public void CreatePDF()
    {
        //try
        //{

        LoadVariables();
        using (var ms = new MemoryStream())
        {

            document = new Document(PageSize.LETTER, 50, 50, 110, 25);
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            //Footer
            writer.PageEvent = new PDFFooter();
            //writer.PageEvent = new PDFHeader();
            var header = new PDFHeader();
            header.headerTable = addSectionTable(getSectionTables("header"), 0, 0);
            writer.PageEvent = header;
            //The document must be opened after being used in PdfWriter.GetInstance() otherwise there no writer associated and it does nothing.
            //http://stackoverflow.com/questions/30672700/itextsharp-the-document-is-not-open-error-when-it-actually-is
            document.Open();
            setPDFStyling();
            generatePDF();
            //smpdf.CreateSamplePdf(MS, document);


            //Finish Producing Document
            document.Close();
            //Dump Memorystream to byte[] variable
            //Add Page Numbers
            bytes = AddPageNumber(ms).ToArray();
        }
        //process bytes for client download
        SM_PDF pdf = new SM_PDF();
        string pdfTitle = "GCAT_Analysis_Report_" + GCATID.ToString();
        pdf.DownloadPDF(bytes, HttpContext.Current, pdfTitle);

    }

    private void LoadVariables()
    {
        //This can probably be the place where I consolidate all future queries for individual test types into one place.
        if (GCATID == 0)
            return;
        vr = ciq.ValidationReports.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        if (vr == null)
            return;
        GatherTests(vr);
        //Extended Imagery
        listExtendedImagery = ql.LoadInspectionImagery("GCAT", GCATID, "ExtendedImagery");

        companyName = GetCompany();
        accountManager = GetAccountManager();
        customerTypes = GetCustomerTypes();
        standardTests = GetStandardTests(vr);
        customerRequirements = GetCustomerRequirements(vr);
        componentCondition = GetComponentCondition(vr);
        dateCodes = GetDateCodes(vr);
        testsPerformed = GetTestsPerformed(vr);
    }

    public void GatherTests(ValidationReport vr)
    {
        mp = ciq.ManufacturerParts.Where(w => w.MPNID == vr.MPNID).SingleOrDefault();
        decap = ciq.Decapsulations.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        hs = ciq.HeatedSolvents.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        mperm = ciq.MarkingPermanencies.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        be = ciq.BasicElectricals.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        sv = ciq.SentryVIs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        sol = ciq.Solderabilities.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        xray = ciq.XRays.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        xrf = ciq.XRFs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        ep = ciq.EPROMs.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        et = ciq.ExtendedTestings.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        mb = ciq.MoistureBakings.Where(w => w.validationreportid == GCATID).SingleOrDefault();
        if (et != null)
            extendedTestingCategory = GetExtendedTestingCategory(vr);
        sum = ciq.Summary_news.Where(w => w.ValidationReportID == GCATID).SingleOrDefault();
        testsGathered = true;
    }

    private string GetCompany()
    {
        if (!string.IsNullOrEmpty(vr.company_name))
            return vr.company_name;
        else if (!string.IsNullOrEmpty(vr.company_uid))
            return rz.companies.Where(w => w.unique_id == vr.company_uid).Select(s => s.companyname).SingleOrDefault() ?? "Unknown";
        else
        {
            CustomerPart cp = ciq.CustomerParts.Where(w => w.CPNID == vr.CPNID).SingleOrDefault();
            if (cp != null)
                return cp.Customer ?? "Unknown";
        }
        return "Unknown";
    }
    private string GetAccountManager()
    {

        if (!string.IsNullOrEmpty(vr.agent_name))
            return vr.agent_name;
        if (vr.AccountManagerID != null && vr.AccountManagerID > 0)
        {
            AccountManager am = ciq.AccountManagers.Where(w => w.AccountManagerID == vr.AccountManagerID).SingleOrDefault();
            if (am.FullName != null)
            {
                n_user u = rz.n_users.Where(w => w.name == am.FullName).SingleOrDefault();
                if (u != null)
                {
                    vr.agent_name = u.name;
                    vr.agent_uid = u.unique_id;
                    ciq.SubmitChanges();
                    return u.name;
                }
            }
        }
        return ("Unknown");
    }

    private string GetCustomerTypes()
    {
        string ret = null;
        if (!string.IsNullOrEmpty(vr.CustomerTypeA))
            ret = vr.CustomerTypeA;
        if (!string.IsNullOrEmpty(vr.CustomerTypeB))
            ret += ", " + vr.CustomerTypeB;
        if (!string.IsNullOrEmpty(vr.CustomerTypeC))
            ret += ", " + vr.CustomerTypeC;
        if (!string.IsNullOrEmpty(vr.CustomerTypeD))
            ret += ", " + vr.CustomerTypeD;

        return ret;
    }

    public string GetStandardTests(ValidationReport vr)
    {
        string ret = null;
        ret = "IDEA 1010, SMC-GCAT";
        if (vr.is_CCAP == true)
            ret += ", CCAP-101";
        if (vr.is_AS6081 == true)
            ret += ", AS-6081";
        return ret;
    }

    public string GetCustomerRequirements(ValidationReport vr)
    {
        string ret = null;
        if (!string.IsNullOrEmpty(vr.Customer_RequirementA))
            ret = vr.Customer_RequirementA;
        if (!string.IsNullOrEmpty(vr.Customer_RequirementB))
            ret += ", " + vr.Customer_RequirementB;
        if (!string.IsNullOrEmpty(vr.Customer_RequirementC))
            ret += ", " + vr.Customer_RequirementC;
        if (!string.IsNullOrEmpty(vr.Customer_RequirementD))
            ret += ", " + vr.Customer_RequirementD;
        if (ret == null)
            ret = "No addtional requirements specified by customer";

        return ret;
    }

    public string GetComponentCondition(ValidationReport vr)
    {
        //Can be both used and refurbished if not new

        string ret = null;
        if (vr.Used == true)
            ret = tools.concatStringwithSeparator(ret, "Used");
        if (vr.New == true)
            ret = tools.concatStringwithSeparator(ret, "New");
        if (vr.Refurbished == true)
            ret = tools.concatStringwithSeparator(ret, "Refurbished");
        return ret;
    }



    public string GetTestsPerformed(ValidationReport vr)
    {
        string ret = null;
        if (!testsGathered)
            GatherTests(vr);

        List<string> testList = new List<string>();

        if (decap != null)
            testList.Add("Decapsulation");
        if (hs != null)
            testList.Add("Heated Solvents");
        if (mperm != null)
            testList.Add("Marking Permenancy");
        if (be != null)
            testList.Add("Basic Electrical");
        if (sv != null)
            testList.Add("Sentry VI Analysis");
        if (sol != null)
            testList.Add("Solderability");
        if (xray != null)
            testList.Add("X-Ray Analysis");
        if (xrf != null)
            testList.Add("XRF Analysis");
        if (ep != null)
            testList.Add("EPROM Analysis");
        if (et != null)
            testList.Add("Extended Testing");
        if (mb != null)
            testList.Add("Moisture / Baking");


        ret = string.Join(", ", testList);

        //if (!string.IsNullOrEmpty(vr.Test_TypeA))
        //    ret = vr.Test_TypeA;
        //if (!string.IsNullOrEmpty(vr.Test_TypeB))
        //    ret += ", " + vr.Test_TypeB;
        //if (!string.IsNullOrEmpty(vr.Test_TypeC))
        //    ret += ", " + vr.Test_TypeC;
        //if (!string.IsNullOrEmpty(vr.Test_TypeD))
        //    ret += ", " + vr.Test_TypeD;
        //if (!string.IsNullOrEmpty(vr.Test_TypeE))
        //    ret += ", " + vr.Test_TypeE;
        //if (!string.IsNullOrEmpty(vr.Test_TypeF))
        //    ret += ", " + vr.Test_TypeF;
        //if (!string.IsNullOrEmpty(vr.Test_TypeG))
        //    ret += ", " + vr.Test_TypeG;
        //if (!string.IsNullOrEmpty(vr.Test_TypeH))
        //    ret += ", " + vr.Test_TypeH;
        //if (!string.IsNullOrEmpty(vr.Test_TypeI))
        //    ret += ", " + vr.Test_TypeI;
        //if (!string.IsNullOrEmpty(vr.Test_TypeJ))
        //    ret += ", " + vr.Test_TypeF;
        return ret;

    }



    public string GetDateCodes(ValidationReport vr)
    {
        string ret = "";
        List<string> dateCodesList = new List<string>();
        if (!string.IsNullOrEmpty(vr.DateCode1))
            dateCodesList.Add(vr.DateCode1);
        if (!string.IsNullOrEmpty(vr.DateCode2))
            dateCodesList.Add(vr.DateCode2);
        if (!string.IsNullOrEmpty(vr.DateCode3))
            dateCodesList.Add(vr.DateCode3);
        if (!string.IsNullOrEmpty(vr.DateCode4))
            dateCodesList.Add(vr.DateCode4);
        if (!string.IsNullOrEmpty(vr.DateCode5))
            dateCodesList.Add(vr.DateCode5);
        if (!string.IsNullOrEmpty(vr.DateCode6))
            dateCodesList.Add(vr.DateCode6);
        if (!string.IsNullOrEmpty(vr.DateCodeUnltd))
            dateCodesList.Add(vr.DateCodeUnltd);
        ret = string.Join(", ", dateCodesList);
        return ret;
    }

    public string GetExtendedTestingCategory(ValidationReport vr)
    {
        string ret = null;
        ret = et.ExtCategory;
        if (et.ExtCategory.ToLower() == "other" && !string.IsNullOrEmpty(et.ExtCategoryOther))
            ret += ": " + et.ExtCategoryOther;
        return ret;
    }


    //private void initDocument(Rectangle pageSize)
    //{
    //    //Set the Overall Size of the pdf
    //    //Rectangle r = new Rectangle(pageSize);
    //    //the below floats (points) equate to about 1/72nd of an inch. 
    //    document = new Document(PageSize.LETTER, 50, 50, 25, 25);
    //}

    private void setPDFStyling()
    {

        //Spacer Column
        PdfPCell cellSpacer = new PdfPCell(new Phrase());
        cellSpacer.Border = Rectangle.NO_BORDER;
    }

    protected void generatePDF()
    {
        //Header        
        //document.Add(addSectionTable(getSectionTables("header"), 0, 0));
        //Main Imagery
        document.Add(addSectionTable(getSectionTables("headerimagery"), 15, 10));
        //Header Summary (Report Summary)
        if (!string.IsNullOrEmpty(vr.summaryNotes))
            document.Add(addSectionTable(getSectionTables("headersummary"), 15, 10));
        //Customer Testing Requirements
        document.Add(addSectionTable(getSectionTables("customerreq"), 15, 10));
        //Component Identification
        document.Add(addSectionTable(getSectionTables("componentident"), 15, 10));
        //External visual surface
        document.Add(addSectionTable(getSectionTables("externalvisualsurface"), 15, 10));
        //External visual component
        document.Add(addSectionTable(getSectionTables("externalvisualcomponent"), 15, 10));
        //Marking Permenancy
        if (mperm != null)
            document.Add(addSectionTable(getSectionTables("markingperm"), 15, 10));
        //Xray
        if (xray != null)
            document.Add(addSectionTable(getSectionTables("xray"), 15, 10));
        //Solderability
        if (sol != null)
            document.Add(addSectionTable(getSectionTables("solderability"), 15, 10));
        //XrF
        if (xrf != null)
            document.Add(addSectionTable(getSectionTables("xrf"), 15, 10));
        //EPROM
        if (ep != null)
            document.Add(addSectionTable(getSectionTables("eprom"), 15, 10));
        //Basic Electrical
        if (be != null)
            document.Add(addSectionTable(getSectionTables("basicelectrical"), 15, 10));
        //SentryVI
        if (sv != null)
            document.Add(addSectionTable(getSectionTables("sentryvi"), 15, 10));
        //Extended Testing
        if (et != null)
            document.Add(addSectionTable(getSectionTables("extendedtesting"), 15, 10));
        //Moisture / Baking
        if (mb != null)
            document.Add(addSectionTable(getSectionTables("moisturebaking"), 15, 10));
        //Heated solvents
        if (hs != null)
            document.Add(addSectionTable(getSectionTables("solvents"), 15, 10));
        //Decapsulation
        if (decap != null)
            document.Add(addSectionTable(getSectionTables("decap"), 15, 10));
        //Extended Imagery
        if (listExtendedImagery.Count > 0)
        {
            List<PdfPTable> imageTables = getSectionTables("extendedImagery");
            PdfPTable imageTable = addSectionTable(imageTables, 15, 10);
            // PdfPTable testTable = new PdfPTable(3);
            document.Add(imageTable);

        }

        //Summary
        document.Add(addSectionTable(getSectionTables("summary"), 15, 10));

    }

    private List<Image> TestImageList(int count)
    {
        Image img = Image.GetInstance(context.Server.MapPath("~/Images/trans_logo.png"));
        List<Image> ret = new List<Image>();
        for (int i = 1; i <= count; i++)
        {
            ret.Add(img);
        }

        return ret;
    }

    private List<Image> getSectionImages(string sectionID)
    {
        List<Image> ret = new List<Image>();
        switch (sectionID)
        {
            case "externalvisualcomponent":
                {
                    sectionID = "VisualComponent";
                    break;
                }
            case "externalvisualsurface":
                {
                    sectionID = "VisualSurface";
                    break;
                }
            case "decap":
                {
                    sectionID = "Decap";
                    break;
                }

            case "solvents":
                {
                    sectionID = "Heated";
                    break;
                }

            case "markingperm":
                {
                    sectionID = "MarkingPerm";
                    break;
                }

            case "basicelectrical":
                {
                    break;
                }

            case "sentryvi":
                {
                    sectionID = "Sentry";
                    break;
                }

            case "solderability":
                {
                    sectionID = "Solder";
                    break;
                }

            case "xray":
                {
                    sectionID = "Xray";
                    break;
                }

            case "xrf":
                {

                    break;
                }

            case "eprom":
                {

                    break;
                }

            case "extendedtesting":
                {
                    sectionID = "Ext";
                    break;
                }

        }
        //Get List of images that exist for a section
        List<string> sectionImageIDs = new List<string>();
        if (vr.is_demo ?? false)
            sectionImageIDs = smb.insp_images.Where(w => w.insp_section_id.Contains(sectionID) && w.insp_id == vr.demo_source_id).Select(s => s.insp_image_id).ToList();
        else
            sectionImageIDs = smb.insp_images.Where(w => w.insp_section_id.Contains(sectionID) && w.insp_id == GCATID).Select(s => s.insp_image_id).ToList();
        //Iterate through the list and retrieve the image from db    
        foreach (string s in sectionImageIDs)
        {
            //Image i = Image.GetInstance(smb.insp_images.Where(w => w.insp_image_id == s).Select(sel => sel.img_blob).SingleOrDefault());
            //get byte[] from DB
            //byte[] imgByte = smb.insp_images.Where(w => w.insp_image_id == s).Select(sel => sel.img_blob).SingleOrDefault();
            string filePath = smb.insp_images.Where(w => w.insp_image_id == s).Select(sel => sel.img_path_web).SingleOrDefault();
            byte[] imgByte = File.ReadAllBytes(filePath);
            //resize the byte[]Image pdfImage = Image.GetInstance(kvp.Key);
            imgByte = tools.ResizeImage(imgByte, 800, 50);
            //set byte[] as image
            Image i = Image.GetInstance(imgByte);
            ret.Add(i);
        }
        return ret;
    }

    private List<PdfPTable> getSectionTables(string sectionID)
    {
        List<PdfPTable> listTables = new List<PdfPTable>();
        List<Image> imageList = new List<Image>();
        string secID = sectionID.ToLower();
        switch (secID)
        {
            case "header":
                {
                    listTables.Add(generateHeader());
                    break;
                }
            case "headerimagery":
                {
                    imageList = getSectionImages("header");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList, null, 20, 150));
                    break;
                }
            case "customerreq":
                {
                    listTables.Add(addSectionTitle("Customer Testing Requirements"));
                    listTables.Add(generateCustomerReq());
                    break;
                }
            case "componentident":
                {
                    listTables.Add(addSectionTitle("Component Identification (From Manufacturer Datasheet)"));
                    listTables.Add(generateComponentIdent());
                    listTables.Add(generateComponentCharacteristics());
                    break;
                }
            //case "componentchar":
            //    {
            //        l.Add(addSectionTitle("Component Characteristics (From Manufacturer Datasheet)"));
            //        l.Add(generateComponentCharacteristics());
            //        break;
            //    }
            //case "datecodes":
            //    {
            //        if (!string.IsNullOrEmpty(dateCodes))
            //        {
            //            l.Add(addSectionTitle("Date Code Information"));
            //            l.Add(generateDateCodes());
            //        }
            //        break;
            //    }
            case "headersummary":
                {
                    string title = "Report Summary";
                    if (vr.Quantity1 > 0)
                        title += " |  Quantity: " + vr.Quantity1;
                    listTables.Add(addSectionTitle(title));
                    listTables.Add(generateHeadersummary());
                    break;
                }
            case "externalvisualsurface":
                {
                    listTables.Add(addSectionTitle("External Visual - Surface", (int)(vr.VisualSurfaceSampleQty ?? 0), dateCodes));
                    imageList = getSectionImages("externalvisualsurface");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateExternalVisualSurface());
                    break;
                }

            case "externalvisualcomponent":
                {
                    listTables.Add(addSectionTitle("External Visual - Component", (int)vr.VisualComponentSampleQty));
                    imageList = getSectionImages("externalvisualcomponent");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateExternalVisualComponent());
                    //l.Add(generateDateCodes());
                    break;
                }

            case "decap":
                {
                    listTables.Add(addSectionTitle("Decapsulation Analysis", (int)decap.DeCap_SampleQty));
                    imageList = getSectionImages("decap");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateDecap());
                    break;
                }

            case "solvents":
                {
                    listTables.Add(addSectionTitle("Heated Solvent Analysis", (int)hs.HeatedSolventSampleQty));
                    imageList = getSectionImages("solvents");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateHeatedSolvents());
                    break;
                }
            case "markingperm":
                {
                    listTables.Add(addSectionTitle("Marking Permenancy Analysis", (int)mperm.MarkingSample_Qty));
                    imageList = getSectionImages("markingperm");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateMarkingPermenancy());
                    break;
                }
            case "basicelectrical":
                {
                    listTables.Add(addSectionTitle("Basic Electrical Analysis", be.SampleQty));
                    imageList = getSectionImages("basicelectrical");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList, null, 50));

                    listTables.Add(generateBasicElectrical());
                    break;
                }
            case "sentryvi":
                {
                    listTables.Add(addSectionTitle("Sentry VI Analysis", (int)sv.SentryVISampleQty, (bool)sv.ServiceProviderRequiredSentryVI));
                    imageList = getSectionImages("sentryvi");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList, null, 50));
                    listTables.Add(generateSentryVI());
                    break;
                }
            case "solderability":
                {
                    listTables.Add(addSectionTitle("Solderability Analysis", (int)sol.SolderabilitySampleQty, (bool)sol.SolderabilityServiceProviderRequired));
                    imageList = getSectionImages("solderability");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateSolderability());
                    break;
                }
            case "xray":
                {
                    listTables.Add(addSectionTitle("Xray Analysis / Die Verification", (int)xray.XRaySampleQty));
                    imageList = getSectionImages("xray");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateXray());
                    break;
                }
            case "xrf":
                {
                    listTables.Add(addSectionTitle("XRF Analysis", (int)xrf.XRFSampleQty, (bool)xrf.XRFServiceProviderRequired));
                    imageList = getSectionImages("xrf");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateXRF());
                    break;
                }
            case "eprom":
                {
                    listTables.Add(addSectionTitle("EEPROM Analysis", (int)ep.EPROMSampleQty, (bool)ep.EPROMServiceProviderRequired));
                    imageList = getSectionImages("eprom");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList));
                    listTables.Add(generateEPROM());
                    break;
                }
            case "extendedtesting":
                {
                    listTables.Add(addSectionTitle("Extended Testing", (int)et.SampleQty));
                    imageList = getSectionImages("extendedtesting");
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList, null, 50));
                    listTables.Add(generateExtended());
                    break;
                }

            case "moisturebaking":
                {
                    listTables.Add(addSectionTitle("Moisture / Baking", (int)(mb.total_quantity ?? 0)));
                    imageList = getSectionImages(secID);
                    if (imageList.Count > 0)
                        listTables.Add(addSectionImagery(imageList, null, 50));
                    listTables.Add(generateMoistureBaking());
                    break;
                }
            case "extendedimagery":
                {
                    listTables.Add(GenerateExtendedImageryTable());
                    break;
                }
            case "summary":
                {
                    listTables.Add(addSectionTitle("Summary of Results", theFont.H2, White));
                    //l.Add(addSectionImagery(TestImageList(2)));
                    listTables.Add(generateSummary());
                    break;
                }

        }
        return listTables;
    }


    private PdfPTable addSectionTable(List<PdfPTable> tableList, int cellBorder, float spacingAfter)
    {
        PdfPTable table = new PdfPTable(1);
        table.WidthPercentage = 100;
        table.KeepTogether = true;
        foreach (PdfPTable t in tableList)
        {
            PdfPCell c = new PdfPCell(t);
            c.Border = cellBorder;
            table.AddCell(c);
        }
        if (spacingAfter > 0)
            table.SpacingAfter = spacingAfter;
        //Wrapping in a cell so I Can control the border of the table. 
        return table;

    }

    private PdfPTable generateHeader()
    {
        documentTitle = "GCAT ANALYSIS REPORT";
        // Header part
        PdfPTable HeaderTable = new PdfPTable(2);
        //Make table full width
        HeaderTable.WidthPercentage = 100;
        //Set column Widths
        HeaderTable.SetWidths(new int[] { 30, 70 });
        // first cell
        cell = GCATAddCellImage(context.Server.MapPath("~/Images/pdf_logo_2018.png"), 3, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER, 25, 3);
        cell.PaddingTop = 10f;
        cell.PaddingBottom = 5f;
        HeaderTable.AddCell(AddCellTable(1, 30, cell));
        //Second 2 column table
        PdfPTable table3 = new PdfPTable(4);
        table3.SetWidths(new int[] { 10, 50, 10, 30 });
        //Row 1     
        cell = GCATAddCell(documentTitle, 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER, theFont.H0, 1);
        cell.PaddingTop = 10;
        cell.PaddingBottom = 5;
        table3.AddCell(cell);
        //Row 2
        table3.AddCell(GCATAddCell("MFG:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table3.AddCell(GCATAddCell(vr.Manufacturer, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table3.AddCell(GCATAddCell("Date:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table3.AddCell(GCATAddCell(vr.ReportDate.Value.ToString("d"), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Row 3
        table3.AddCell(GCATAddCell("MPN:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table3.AddCell(GCATAddCell(vr.MPN, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table3.AddCell(GCATAddCell("Form:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table3.AddCell(GCATAddCell(vr.Form, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Row 4
        table3.AddCell(GCATAddCell("CPN:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table3.AddCell(GCATAddCell(vr.CPN, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table3.AddCell(GCATAddCell("ID:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table3.AddCell(GCATAddCell(GCATID.ToString(), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //NEsted Date and ID Table
        //PdfPTable table4 = new PdfPTable(4);
        //table4.SetWidths(new int[] { 15, 20, 20, 45 });

        //table4.AddCell(GCATAddCell("Page:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table4.AddCell(GCATAddCell("1/6", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //cell = new PdfPCell(table4);
        //cell.Colspan = 2;
        //table3.AddCell(cell);
        cell = new PdfPCell(table3);
        cell.Border = 0;
        HeaderTable.AddCell(cell);

        //HeaderTable.AddCell((GCATAddCell("Sales Row", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, H1, 1)));
        PdfPTable table5 = new PdfPTable(4);
        table5.SetWidths(new int[] { 20, 30, 30, 20 });
        //Row 1
        table5.AddCell(GCATAddCell("Sales Order#", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table5.AddCell(GCATAddCell("Customer Name", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table5.AddCell(GCATAddCell("Customer PO#", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table5.AddCell(GCATAddCell("Account Manager", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //Row 2
        table5.AddCell(GCATAddCell(vr.SalesOrderNumber, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table5.AddCell(GCATAddCell(companyName, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table5.AddCell(GCATAddCell(vr.CustomerPO ?? "Unknown", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table5.AddCell(GCATAddCell(accountManager, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        cell = new PdfPCell(table5);
        cell.Colspan = 3;

        HeaderTable.AddCell(cell);
        //HeaderTable.SpacingAfter = 10f;
        return HeaderTable;
    }


    private PdfPTable generateCustomerReq()
    {
        //Section Data Haader
        table = GCATAddTable(2);
        table.SetWidths(new int[] { 22, 78 });
        table.AddCell(GCATAddCell("Customer Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(customerTypes, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Standard Tests", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(standardTests, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Additional Requirements", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(customerRequirements, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Test(s) Performed", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(testsPerformed, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Section Data
        //Row 1





        return table;
    }

    private PdfPTable generateComponentIdent()
    {
        //Section Data Haader
        table = GCATAddTable(6);
        table.SetWidths(new int[] { 20, 14, 20, 13, 20, 13 });
        //Section Data
        //Row 1
        table.AddCell(GCATAddCell("RoHS Compliant?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.RoHS_Compliant, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Condition", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(componentCondition, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //table.AddCell(GCATAddCell("Package Type", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell(mp.PackageType, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Row2 
        table.AddCell(GCATAddCell("MSD / MSL", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.MoistureLevel, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Memory Device?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(GetResult(mp.MemoryDevice, true), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Traceability", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(GetResult(vr.Traceability, true), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));


        return table;
    }
    private PdfPTable generateComponentCharacteristics()
    {
        //Section Data Haader
        table = GCATAddTable(6);
        table.SetWidths(new int[] { 20, 14, 20, 13, 20, 13 });
        //Section Data
        //Row 1
        table.AddCell(GCATAddCell("Package Type Detail", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.PackageType, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Lead / Pin Count", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.Lead_PinCount, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Lead / Pin Pitch", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.Lead_PinPitch, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Row 2
        table.AddCell(GCATAddCell("Length", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.Length, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Width", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.Width, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Thickness", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(mp.Thickness, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        return table;

    }


    private PdfPTable generateHeadersummary()
    {
        //Section Data Haader
        table = GCATAddTable(1);
        table.SetWidths(new int[] { 100 });
        //Section Data
        //Row 1
        table.AddCell(GCATAddCell(vr.summaryNotes, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //Row 2
        return table;
    }

    private PdfPTable generateExternalVisualComponent()
    {

        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Package Type: " + vr.ComponentType, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Date codes:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(dateCodes, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Package Type
        //table.AddCell(GCATAddCell("Package Type:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell(vr.ComponentType, 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Package Dimensions", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Actual", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //Nested 2 column     
        nested = GCATAddTable(2, 100);
        nested.SetWidths(new int[] { 15, 85 });
        nested.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        nested.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddNestedCellTable(nested, 15));

        //Section Data
        //Width
        table.AddCell(GCATAddCell("Width", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(vr.Width_Actual.ToString() + vr.UnitWidth, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Nested 2 column     
        nested = GCATAddTable(2, 100);
        nested.SetWidths(new int[] { 15, 85 });
        nested.AddCell(GCATAddCell(GetResult(vr.Pass_W), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        nested.AddCell(GCATAddCell(vr.CommentsWidth, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddNestedCellTable(nested, 15));

        //Length        
        table.AddCell(GCATAddCell("Length", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(vr.Length_Actual.ToString() + vr.UnitLength, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Nested 2 column     
        nested = GCATAddTable(2, 100);
        nested.SetWidths(new int[] { 15, 85 });
        nested.AddCell(GCATAddCell(GetResult(vr.Pass_L), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        nested.AddCell(GCATAddCell(vr.CommentsLength, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddNestedCellTable(nested, 15));

        //Thickness        
        table.AddCell(GCATAddCell("Thickness", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(vr.Thickness_Actual.ToString() + vr.UnitThickness, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Nested 2 column     
        nested = GCATAddTable(2, 100);
        nested.SetWidths(new int[] { 15, 85 });
        nested.AddCell(GCATAddCell(GetResult(vr.Pass_T), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        nested.AddCell(GCATAddCell(vr.CommentsThickness, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddNestedCellTable(nested, 15));

        //Package Type
        //All
        table.AddCell(GCATAddCell("Oxidation / Discoloration", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(vr.Oxidation_Discoloration), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(vr.CommentO2Discoloration, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //Leadless
        if (vr.ComponentType == "Leadless" || vr.ComponentType == "Lead")
        {
            table.AddCell(GCATAddCell("Lead and Pad Condition", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(vr.Lead_PadCondition), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(vr.CommentLead_Pad, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Signs of Exposed base metal", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(vr.SignsExposedBaseMetal), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(vr.CommentExpBaseMetal, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }
        //Lead
        if (vr.ComponentType == "Lead")
        {
            table.AddCell(GCATAddCell("Lead Coplanarity", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(vr.LeadCoplanarity), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(vr.CommentLeadCoplanarity, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //BGA     
        if (vr.ComponentType == "BGA")
        {
            table.AddCell(GCATAddCell("Solder Balls / Spheres", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(vr.SolderBalls_Spheres), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(vr.CommentSolderBalls, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Uniform / Concentricity", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(vr.Uniform_Concentricity), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(vr.CommentUniformity, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Crushed Damaged Spheres", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(vr.Crushed_Damages_Spheres), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(vr.CommentDamagedSpheres, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }
        return table;
    }

    private PdfPTable generateExternalVisualSurface()
    {
        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Surface", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        table.AddCell(GCATAddCell("Top", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(vr.Top_Pass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(vr.Comments_Top, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell("Bottom", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(vr.Bottom_Pass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(vr.Comments_Bottom, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //document.Add(table);
        return table;
    }

    private PdfPTable generateDecap()
    {
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        table.AddCell(GCATAddCell("MFG Logo / ID Present", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(decap.MFGLogoPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(decap.MFGLogoComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Logo / ID Match Casing", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(decap.LogoMatchPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(decap.LogoMatchComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("P/N and Die# Match", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(decap.DiePNMatchPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(decap.DiePNMatchComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Die and Bond Wires", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(decap.DieBondWiresPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(decap.DieBondWiresComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        return table;
    }

    private PdfPTable generateHeatedSolvents()
    {
        table = GCATAddTable(3);

        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Solvent Tested", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        table.AddCell(GCATAddCell("Methyl Pyrrolidinone", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(hs.Methyl_PyrrolidinonePass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(hs.Methyl_PyrrolidinoneComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Dynasolve 750", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(hs.DynasolvePass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(hs.DynasolveComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Secondary Coating (no evidence)", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(hs.SecondaryCoatingPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(hs.SecondaryCoatingComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        return table;
    }




    private PdfPTable generateMarkingPermenancy()
    {

        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        table.AddCell(GCATAddCell("Acetone Test Results", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(mperm.AcetoneTestPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(mperm.Comments_Acetone, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Alcohol Mineral Spirits", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(mperm.Alcohol_MineralSpiritsTestPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(mperm.Comments_Alcohol, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Scrape Test", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(mperm.ScrapeTestPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(mperm.Comments_ScrapeTest, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Secondary Coating", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(mperm.SecondaryCoatingTestPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(mperm.Comments_SecondaryCoating, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Part Number / Marking", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(mperm.PartNumber_MarkingTestPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(mperm.Comments_PartNumber, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Case Marking / Logo", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(mperm.CaseMarking_LogoTestPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(mperm.Comments_CaseMarking, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        return table;
        //document.Add(table);
    }

    private PdfPTable generateXray()
    {
        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Die / Bond Wires", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        table.AddCell(GCATAddCell("Bond Wires Present?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xray.BondWiresPresentPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xray.BondWiresPresentComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Die Present?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xray.DiePresentPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xray.DiePresentComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Damaged / Missing Bond Wires?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xray.Damaged_MissingWiresPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xray.Damaged_MissingWiresComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Die Lead Uniformity", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xray.DieLeadUniformityPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xray.DieLeadUniformityComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Die Frame Size / Location Consistent?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xray.DieFRameSize_LocationPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xray.DieFrameSize_LocationComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //document.Add(table);
        return table;
    }



    private PdfPTable generateSentryVI()
    {
        table = GCATAddTable(3);

        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data        
        table.AddCell(GCATAddCell("Package Mount Type: " + sv.PackageMountTypeSentryVI, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));

        table.AddCell(GCATAddCell("Signatures Match Standard?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sv.SignaturesMatch), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sv.SignatureMatchComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Open / shorts detected?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sv.Open_Shorts_Detection, true), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sv.OpenShortsComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Passed all signature analysis?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sv.SignatureAnalysisPass), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sv.SignatureAnalysisComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //table.AddCell(GCATAddCell("Signature VI Report Required?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        //table.AddCell(GCATAddCell(GetResult(sv.SignatureReportRequired, true), 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        ////table.AddCell(GCATAddCell(sv.SignatureReportComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15,theFont.Normal, 1));

        table.AddCell(GCATAddCell("Overall Result:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sv.SentryOverAllResult ?? 0), 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        return table;
    }

    private PdfPTable generateSolderability()
    {
        table = GCATAddTable(3);
        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data        
        table.AddCell(GCATAddCell("Package Mount Type: " + sol.PackageMountTypeSolderability, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));

        table.AddCell(GCATAddCell("Wetting Acceptable?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sol.SolderWettingAcceptable), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sol.SolderWettingComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("RoHS?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sol.RoHS, true, false), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sol.RoHSComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Void-free Leads?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sol.VoidFree), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sol.VoidFreeComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Leads free of comtaminants?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(sol.ContaminantFree), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sol.ContaminantFreeComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        return table;
    }

    private PdfPTable generateXRF()
    {
        table = GCATAddTable(3);
        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data        
        table.AddCell(GCATAddCell("Package Mount Type: " + xrf.PackageMountTypeXRF, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));

        table.AddCell(GCATAddCell("Lead Materials Match?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xrf.LeadMaterialsMatch), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xrf.LeadMaterialsMatchComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Foreign Materials Detected?", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(xrf.ForeignMaterials, true, true), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(xrf.ForeignMaterialsComments, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        return table;
    }

    private PdfPTable generateEPROM()
    {
        table = GCATAddTable(3);
        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data        
        table.AddCell(GCATAddCell("Package Mount Type: " + ep.PackageMountTypeEPROM, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));

        table.AddCell(GCATAddCell("Checksum Analysis:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(GetResult(ep.ChecksumAnalysis), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(ep.ChecksumComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));


        if (ep.ProgrammingRequired == 1)
        {
            table.AddCell(GCATAddCell("Verify Program Load:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(GetResult(ep.VerifyProgramLoad), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(ep.ProgramLoadComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        return table;
    }


    private PdfPTable generateBasicElectrical()
    {

        //Section Data Haader
        table = GCATAddTable(5);
        table.SetWidths(new int[] { 20, 20, 20, 20, 20 });
        table.AddCell(GCATAddCell("Test / Application Requirement", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Temperature", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        table.AddCell(GCATAddCell(be.Test1, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(be.Cond1, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(be.Actual1, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Result Notes", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(be.notes, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));



        //table = GCATAddTable(2);
        //table.SetWidths(new int[] { 30, 70 });
        ////Section Data
        //table.AddCell(GCATAddCell("Overall Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell(GetResult(be.TestingResult ?? "0"), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15,theFont.Normal, 1));

        return table;
    }

    private PdfPTable generateMoistureBaking()
    {

        table = GCATAddTable(4);

        //Section Data Haader
        table = GCATAddTable(4);
        table.SetWidths(new int[] { 25, 25, 25, 25 });
        //table.AddCell(GCATAddCell("Test Type", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data        



        //strat sales work order
        //table.AddCell(GCATAddCell("Strategic Sales Work Order: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        //table.AddCell(GCATAddCell(mb.strategic_sales_work_order_number.ToString(), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));



        //process
        table.AddCell(GCATAddCell("Process: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(mb.process, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //bake temp
        table.AddCell(GCATAddCell("Bake Temperature: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(mb.bake_temperature, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //bake duration        
        table.AddCell(GCATAddCell("Bake Duration: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(mb.bake_duration, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //msl
        table.AddCell(GCATAddCell("MSL: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(mb.msl, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //result
        table.AddCell(GCATAddCell("Result: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(mb.result, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //comments
        table.AddCell(GCATAddCell("Comments: ", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(mb.result_comment, 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        return table;
    }

    private PdfPTable generateExtended()
    {
        //Category
        //Result
        //Condition TEsted
        //results notes

        table = GCATAddTable(8);
        table.SetWidths(new int[] { 20, 20, 10, 10, 10, 10, 10, 10 });
        //table.AddCell(GCATAddCell("Category:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell(extendedTestingCategory, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15,theFont.Normal, 1));
        //table.AddCell(GCATAddCell("Overall Result:", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //table.AddCell(GCATAddCell(GetResult(et.ExtTestingResult ?? "0"), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15,theFont.Normal, 1));

        table.AddCell(GCATAddCell("Test Requirement", 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Condition", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));


        //Section Data      

        table.AddCell(GCATAddCell(extendedTestingCategory, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(et.Cond1, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(GetResult(et.Actual1), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Result Notes", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell(et.notes, 7, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));



        return table;
    }

    private PdfPTable generateSummary()
    {
        table = GCATAddTable(2);
        //Section Data Haader
        table = GCATAddTable(3);
        table.SetWidths(new int[] { 25, 15, 60 });
        table.AddCell(GCATAddCell("Test Performed", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        table.AddCell(GCATAddCell("Comments", 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

        //Section Data
        //External Visual    
        table.AddCell(GCATAddCell("External Visual", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

        table.AddCell(GCATAddCell("Logos / Part Numbers", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(sum.ExtVisLogosPartsResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sum.ExtVisLogosPartsComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Dimensional Requirements", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(sum.ExtVisDimensionalResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sum.ExtVisDimensionalComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Lead / Package Condition", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(sum.ExtVisLeadPkgResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sum.ExtVisLeadPkgComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        table.AddCell(GCATAddCell("Check for Signs of Surface Alteration", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
        table.AddCell(GCATAddCell(sum.ExtVisSurfaceAltResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        table.AddCell(GCATAddCell(sum.ExtVisSurfaceAltComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        //Marking Permenancy   
        if (mperm != null)
        {
            table.AddCell(GCATAddCell("Marking Permenancy", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Acetone Test", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.MarkingAcetoneResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.MarkingAcetoneComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Scrape Test", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.MarkingScrapeResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.MarkingScrapeComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Check for Signs of Marking Alteration", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.MarkingAlterationResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.MarkingAlterationComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }
        //X-Ray Analysis  
        if (xray != null)
        {
            table.AddCell(GCATAddCell("X-Ray Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Uniform Die / Wire Bonds", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.XrayDieWireResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.XrayDieWireComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));


        }
        //Heated Solvents Analysis
        if (hs != null)
        {
            table.AddCell(GCATAddCell("Heated Solvents Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));

            table.AddCell(GCATAddCell("Methyl Pyrrolidinone Results", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.HeatedMethResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.HeatedMethComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Dynasolve 750 Results", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.HeatedDynaResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.HeatedDynaComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Secondary Coating Results", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.HeatedSecondaryResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.HeatedSecondaryComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //De-Cap Analysis
        if (decap != null)
        {
            table.AddCell(GCATAddCell("De-Cap Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Wire Bond Integrity", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.DecapWireBondResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.DecapWireBondComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Die Marking Verification", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.DecapDieMarkingResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.DecapDieMarkingComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //Solderability Analysis
        if (sol != null)
        {
            table.AddCell(GCATAddCell("Solderability Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Solder Wetting Acceptable", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.SolWettingResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.SolWettingComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Passed Solderability Testing", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.SolPassedResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.SolPassedComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }
        //EPROM Testing
        if (ep != null)
        {
            table.AddCell(GCATAddCell("EPROM Testing", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Checksum and Blank Check Validation", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.EPROMChecksumResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.EPROMCheckSumComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));


            if (ep.ProgrammingRequired == 1)
            {
                table.AddCell(GCATAddCell("Program Validation", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
                table.AddCell(GCATAddCell(sum.EPROMProgramValidationResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
                table.AddCell(GCATAddCell(sum.EPROMProgramValidationComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            }
        }



        if (sv != null)
        {
            //Sentry VI Electrical Analysis
            table.AddCell(GCATAddCell("Sentry VI Electrical Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Sentry VI Signature", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.SentryVIResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.SentryVIComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //Basic Electrical Analysis
        if (be != null)
        {
            table.AddCell(GCATAddCell("Basic Electrical Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Basic Electrical Testing", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.BasicElectricalResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.BasicElectricalComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //Extended Testing
        if (et != null)
        {
            table.AddCell(GCATAddCell("Extended Testing", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell(extendedTestingCategory, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.ExtendedTestingResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.ExtendedTestingComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //XRF Analysis
        if (xrf != null)
        {
            table.AddCell(GCATAddCell("XRF Analysis", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Lead Materials Match", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.XRFLeadMatsResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.XRFLeadMatsComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

            table.AddCell(GCATAddCell("Evidence of Foreign Material", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.XRFForeignResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.XRFForeignComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        }

        //Moisture / Baking
        if (mb != null)
        {
            table.AddCell(GCATAddCell("Moisture / Baking", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.H2, 1, Gray));

            table.AddCell(GCATAddCell("Result", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1));
            table.AddCell(GCATAddCell(sum.MoistureBakingResult, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
            table.AddCell(GCATAddCell(sum.MoistureBakingComment, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));

        }
        return table;
    }

    public string GetResult(string s, bool yesNo = false, bool invert = false)
    {

        if (invert)
            switch (s)
            {
                case "y":
                    {
                        s = "n";
                        break;
                    }
                case "n":
                    {
                        s = "y";
                        break;
                    }
            }

        switch (s.ToLower())
        {
            case "y":
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case "n":
                {
                    if (yesNo)
                        return "No";
                    return "Fail";
                }
            case "u":
                {

                    return "Unknown";
                }
            case "i":
                {
                    return "Inconclusive";
                }
            default:
                return s;
        }
    }

    public string GetResult(short? i, bool yesNo = false, bool invert = false)
    {

        if (invert)
            switch (i)
            {
                case 1:
                    {
                        i = 2;
                        break;
                    }
                case 2:
                    {
                        i = 1;
                        break;
                    }
            }

        switch (i)
        {
            case 0:
                {
                    return "-Choose-";
                }
            case 1:
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case 2:
                {

                    if (yesNo)
                        return "No";
                    return "Fail";
                }
            case 3:
                {
                    return "Inconclusive";
                }
            default:
                return i.ToString();
        }
    }

    public string GetResult(int? i, bool yesNo = false, bool invert = false)
    {
        if (invert)
            switch (i)
            {
                case 1:
                    {
                        i = 2;
                        break;
                    }
                case 2:
                    {
                        i = 1;
                        break;
                    }
            }

        switch (i)
        {
            case 0:
                {
                    return "-Choose-";
                }
            case 1:
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case 2:
                {

                    if (yesNo)
                        return "No";
                    return "Fail";
                }
            default:
                return i.ToString();
        }
    }


    public string GetResult(bool? b, bool yesNo = false, bool invert = false)
    {
        if (invert)
            b = !b;

        switch (b)
        {
            case true:
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case false:
                {
                    if (yesNo)
                        return "No";
                    return "Fail";
                }

            default:
                return "Unknown";
        }
    }


    private PdfPTable addSectionTitle(string title, Font font = null, BaseColor bgColor = null)
    {
        PdfPTable ret = new PdfPTable(1);
        if (font == null)
            font = theFont.Bold;
        if (bgColor == null)
            bgColor = Gray;
        ret.WidthPercentage = 100;
        ret.SetWidths(new int[] { 100 });
        //ret.SpacingBefore = spacingBefore;        
        ret.AddCell(GCATAddCell(title, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, font, 1, bgColor));
        return ret;
    }

    private PdfPTable addSectionTitle(string title, int sampleQTY)
    {
        PdfPTable ret = new PdfPTable(2);
        ret.WidthPercentage = 100;
        ret.SetWidths(new int[] { 80, 20 });
        //ret.SpacingBefore = spacingBefore;
        ret.AddCell(GCATAddCell(title, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        PdfPTable nested = new PdfPTable(2);
        nested.SetWidths(new int[] { 70, 30 });
        nested.AddCell(GCATAddCell("Sample QTY:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        nested.AddCell(GCATAddCell(sampleQTY.ToString(), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        cell = new PdfPCell(nested);
        cell.Border = 0;
        ret.AddCell(cell);
        return ret;
    }

    private PdfPTable addSectionTitle(string title, int sampleQTY, string dateCodes)
    {
        PdfPTable ret = new PdfPTable(3);
        ret.WidthPercentage = 100;
        ret.SetWidths(new int[] { 30, 50, 20 });
        ret.AddCell(GCATAddCell(title, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        //Extra 2 Columns (i.e. External Visual Surface)
        PdfPTable nested = new PdfPTable(2);
        nested.SetWidths(new int[] { 30, 70 });
        nested.AddCell(GCATAddCell("Date Codes:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        nested.AddCell(GCATAddCell(dateCodes.ToString(), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        cell = new PdfPCell(nested);
        ret.AddCell(cell);
        //Sample Quantity
        nested = new PdfPTable(2);
        nested.SetWidths(new int[] { 70, 30 });
        nested.AddCell(GCATAddCell("Sample QTY:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        nested.AddCell(GCATAddCell(sampleQTY.ToString(), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        cell = new PdfPCell(nested);
        ret.AddCell(cell);
        return ret;
    }
    private PdfPTable addSectionTitle(string title, int sampleQTY, bool serviceProviderRequired)
    {
        PdfPTable ret = new PdfPTable(3);
        ret.WidthPercentage = 100;
        ret.SetWidths(new int[] { 30, 50, 20 });
        ret.AddCell(GCATAddCell(title, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        string spRequired;
        if (serviceProviderRequired)
            spRequired = "Yes";
        else
            spRequired = "No";
        ret.AddCell(GCATAddCell("External Service Provider Required: " + spRequired, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        //Sample Quantity
        nested = new PdfPTable(2);
        nested.SetWidths(new int[] { 70, 30 });
        nested.AddCell(GCATAddCell("Sample QTY:", 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray));
        nested.AddCell(GCATAddCell(sampleQTY.ToString(), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1));
        cell = new PdfPCell(nested);
        ret.AddCell(cell);
        return ret;
    }

    private PdfPTable GenerateExtendedImageryTable()
    {
        //listTables.Add(addSectionTitle("Extended Imagery"));
        //Carry the byte[] AND The description
        Dictionary<byte[], string> dictImageData = new Dictionary<byte[], string>();
        foreach (insp_images im in listExtendedImagery)
        {
            //get byte[] 
            string filePath = im.img_path_web;
            byte[] imgBytes = File.ReadAllBytes(filePath);
            //imgByte = tools.ResizeImage(imgByte, 150);            
            string desc = im.img_description;
            dictImageData.Add(imgBytes, desc);

        }
        if (dictImageData.Count <= 0)
            return null;
        //Instantiate the table
        PdfPTable eximTable = new PdfPTable(3);
        //Prevent last row on page from growing to fill whitespace

        //Add Title
        PdfPCell titleCell = GCATAddCell("Extended Imagery", 3, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Bold, 1, Gray);
        eximTable.AddCell(titleCell);


        while (dictImageData.Count > 0)
        {
            //Take 3 at a time
            Dictionary<byte[], string> tempDict = dictImageData.Take(3).ToDictionary(t => t.Key, t => t.Value);
            foreach (KeyValuePair<byte[], string> kvp in tempDict)
            {
                //Created a single column nested table to hold image cell and description cell
                PdfPTable table = new PdfPTable(1);
                //Set the default Cell border to no border, so onComplete() will just have empty spaces, not bordered-cells with no content.
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                Image pdfImage = Image.GetInstance(kvp.Key);
                pdfImage.ScaleToFitHeight = true;
                PdfPCell imageCell = new PdfPCell(pdfImage, true);
                imageCell.Border = Rectangle.NO_BORDER;
                imageCell.FixedHeight = 120f;
                //imageCell.FixedHeight = 50f;                
                imageCell.HorizontalAlignment = Element.ALIGN_CENTER;
                imageCell.VerticalAlignment = Element.ALIGN_TOP;
                imageCell.PaddingBottom = 5;
                table.AddCell(imageCell);

                string desc = kvp.Value;
                if (!string.IsNullOrEmpty(desc))
                {
                    PdfPCell descCell = GCATAddCell(desc, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 15, theFont.Normal, 1);
                    //descCell.Border = Rectangle.NO_BORDER;
                    //descCell.PaddingTop = 5;
                    //descCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    descCell.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(descCell);
                }



                eximTable.ExtendLastRow = false;
                //Allow iText to split potenitally large Image table rows as soon as they break a page, else lots of whitespace between large tables.
                //eximTable.SplitLate = false;

                eximTable.AddCell(table);
                //Remove these 3 from the master list
                dictImageData.Remove(kvp.Key);
            }
        }


        //Make sure any empty cells iText has to create with CompleteRow() have no border.           
        eximTable.DefaultCell.Border = Rectangle.NO_BORDER;
        //CompleteRow() is required to support adding a number of images that isn't EXACTLY divisible by the number of columns
        eximTable.CompleteRow();





        return eximTable;
    }



    private PdfPTable addSectionImagery(List<Image> ImageList, string title = null, int scalePercent = 20, int fixedHeight = 0)
    {
        if (ImageList == null)
            return null;
        int imageCount = ImageList.Count;
        //int MaxColumns = 3;
        if (imageCount > 0)
        {

            ////Images width for max 3 columns = 33%
            //int[] columnWidths = new int[] { 33, 33, 33 };
            //BAse Column Widths on the number of images, for sections with 3 images or less (i.e. not extendedImagery)
            float[] columnWidths = new float[] { 33, 33, 33 };
            if (imageCount <= 3)
                columnWidths = getRelativeColumnWidthbyInt(imageCount);
            //Max number of columns is 3
            PdfPTable imageTable = new PdfPTable(columnWidths);

            imageTable.SpacingBefore = 5;
            imageTable.SpacingAfter = 5;


            if (!string.IsNullOrEmpty(title))
            {
                PdfPCell titleCell = GCATAddCell(title, imageCount, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, theFont.H2);
                titleCell.Padding = 5; //Paddign to space out the title
                imageTable.AddCell(titleCell);
            }

            while (ImageList.Count > 0)
            {
                //Take 3 at a time
                List<Image> tempList = ImageList.Take(3).ToList();

                foreach (Image i in tempList)
                {
                    int currentIndex = ImageList.IndexOf(i);
                    cell = new PdfPCell(i, true);
                    cell.Padding = 2;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    imageTable.AddCell(cell);

                    //Remvoe these 3 from the master list
                    ImageList.RemoveAt(currentIndex);
                }
            }

            //Make sure any empty cells iText has to create with CompleteRow() have no border.           
            imageTable.DefaultCell.Border = Rectangle.NO_BORDER;
            //CompleteRow() is required to support adding a number of images that isn't EXACTLY divisible by the number of columns
            imageTable.CompleteRow();
            //Allow iText to split potenitally large Image table rows onto new pages, else white space between header row
            imageTable.SplitLate = false;

            return imageTable;
        }
        return null;
    }


    private PdfPTable addSectionImagery(List<insp_images> imList, string title = null)
    {
        if (imList == null || imList.Count <= 0)
            return null;
        int imageCount = imList.Count;
        int MaxColumns = 3;
        PdfPTable ret = new PdfPTable(MaxColumns);
        int[] columnWidths = new int[] { 33, 33, 33 };
        ret.SpacingBefore = 5;
        ret.SpacingAfter = 5;

        if (!string.IsNullOrEmpty(title))
        {
            PdfPCell titleCell = new PdfPCell(new Phrase(title));
            titleCell.Padding = 5; //Paddign to space out the title
            ret.AddCell(titleCell);
        }

        while (imList.Count > 0)
        {
            //Take 3 at a time
            List<insp_images> tempList = imList.Take(3).ToList();
            foreach (insp_images i in tempList)
            {
                string filePath = i.img_path_web;
                byte[] imgBytes = File.ReadAllBytes(filePath);
                Image pdfImage = Image.GetInstance(imgBytes);
                //Image pdfImage = Image.GetInstance(i.img_blob);
                string desc = i.img_description;
                int currentIndex = imList.IndexOf(i);
                PdfPCell imgCell = new PdfPCell(pdfImage, true);
                imgCell.Padding = 2;
                PdfPCell descCell = new PdfPCell(new Phrase(desc));
                descCell.Padding = 2;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                ret.AddCell(imgCell);
                ret.AddCell(imgCell);

                //Remvoe these 3 from the master list
                imList.RemoveAt(currentIndex);

            }

            //Make sure any empty cells iText has to create with CompleteRow() have no border.           
            ret.DefaultCell.Border = Rectangle.NO_BORDER;
            //CompleteRow() is required to support adding a number of images that isn't EXACTLY divisible by the number of columns
            ret.CompleteRow();
            //Allow iText to split potenitally large Image table rows onto new pages, else white space between header row
            ret.SplitLate = false;

            return ret;
        }
        return null;
    }




    //private int[] getRelativeColumnWidthbyInt(int i)
    //{
    //    int maxColumns = 3;
    //    int[] ret = new int[] { 33, 33, 33 }; ;
    //    //Use modulus to divide image count by 5 which is the max number of columns.
    //    //Last row is the remainder.


    //    //This is how many rows of 5
    //    int numberOfRows = i / maxColumns;

    //    //This is how many are left over is not divisible by 5.
    //    int remainingImages = i % maxColumns;

    //    switch (maxColumns)
    //    {
    //        case 1:
    //            ret = new int[] { 100 };
    //            break;
    //        case 2:
    //            ret = new int[] { 50, 50 };
    //            break;
    //        case 3:
    //            ret = new int[] { 33, 33, 33 };
    //            break;
    //            //case 4:
    //            //    return new int[] { 25, 25, 25, 25 };
    //            //case 5:
    //            //    return new int[] { 20, 20, 20, 20, 20 };
    //            //default:
    //            //    return new int[] { 100 };
    //    }
    //    return ret;
    //}

    private float[] getRelativeColumnWidthbyInt(int i)
    {

        switch (i)
        {
            case 1:
                return new float[] { 100 };
            case 2:
                return new float[] { 50, 50 };
            case 3:
                return new float[] { 33, 33, 33 };
            case 4:
                return new float[] { 25, 25, 25, 25 };
            case 5:
                return new float[] { 20, 20, 20, 20, 20 };
            default:
                return new float[] { 100 };
        }
    }


    private PdfPCell AddCellTable(int columnCount, int minHeight, PdfPCell contentCell)
    {
        PdfPTable table1 = new PdfPTable(columnCount);
        table1.DefaultCell.MinimumHeight = minHeight;
        table1.AddCell(contentCell);
        PdfPCell cell = new PdfPCell(table1);
        cell.Border = Rectangle.NO_BORDER;
        return cell;
    }



    private PdfPCell GCATAddCell(string text, int colSpan, int horiz_align, int vert_align, int border, Font font, int rowSpan = 1, BaseColor backgroundColor = null)
    {



        Font customFont = null;


        //since theFont is global, if we set this while creating the phrase, it overrides the entire global varuable and ALL cells get the formal.
        //to avoid, create a custom font only for Pass Fail cells'

        //if (text == "Pass")
        //{            
        //   customFont = FontFactory.GetFont("Montserrat", 9f, BaseColor.GREEN);
        //}

        //else if (text == "Fail")
        //{
        //    customFont = FontFactory.GetFont("Montserrat", 9f, BaseColor.RED); 
        //}

        //If we have a customFont, use it now, else fall back on Global Font.
        Phrase phrase = new Phrase(text, font);



        //if (customFont != null)
        //    phrase = new Phrase(text, customFont);


        PdfPCell ret = new PdfPCell(phrase);
        ret.Colspan = colSpan;
        ret.HorizontalAlignment = horiz_align;
        ret.VerticalAlignment = vert_align;
        ret.Border = border;     
        if (rowSpan != 1)
            ret.Rowspan = rowSpan;
        if (backgroundColor != null)
            ret.BackgroundColor = backgroundColor;
        else if (text == "Pass")
        {
            ret.BackgroundColor = BaseColor.GREEN;
        }

        else if (text == "Fail")
        {
            ret.BackgroundColor = BaseColor.RED;
        }



        return ret;
    }
    private PdfPCell GCATAddCell(PdfPTable table, int rowSpan = 1)
    {
        PdfPCell ret = new PdfPCell(table);
        return ret;
    }


    private PdfPCell GCATAddCellImage(string imagePath, int colSpan, int horiz_align, int vert_align, int border, int scalePercent, int rowSpan = 1)
    {
        Image i = Image.GetInstance(imagePath);
        PdfPCell cell = new PdfPCell(i);
        //cell.FixedHeight = fixedHeight;
        cell.HorizontalAlignment = horiz_align;
        cell.VerticalAlignment = vert_align;
        //if (fixedHeight > 0)
        //{
        //    cell.FixedHeight = fixedHeight;
        //}
        //else
        //    i.ScalePercent(12);
        i.ScalePercent(scalePercent);
        //img.ScaleAbsolute();
        cell.Colspan = colSpan;
        cell.Border = border;
        if (rowSpan != 1)
            cell.Rowspan = rowSpan;
        return cell;
    }

    private PdfPCell GCATAddCellImage(Image i, bool fitImageToCell)
    {

        PdfPCell cell = new PdfPCell(i, fitImageToCell); //Boolean true == fit the image to the cell          
                                                         //if (i.Height > 150) //Fix for Tall Images where no fixed heigh specified.  the few large images like basic elec, should have the fixed set.
                                                         //    cell.FixedHeight = 150;
        cell.Border = Rectangle.NO_BORDER;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        cell.Colspan = 1;
        return cell;
    }

    private PdfPCell GCATAddCellImage(Image i, int colSpan, int horiz_align, int vert_align, int border, int scalePercent, int rowSpan = 1, int fixedHeight = 0)
    {

        PdfPCell cell = new PdfPCell(i, true); //Boolean true == fit the image to the cell  
        if (fixedHeight > 0)//With the below fix, 600f only applies to things like Basic Electrical which can take up a whole page.  Sometimes even header imagery can be taller than 600, but 600F would be too big, thus need ability to dictate a fixed height if needed.
            cell.FixedHeight = fixedHeight;
        if (fixedHeight == 0)
            if (i.Height > 150) //Fix for Tall Images where no fixed heigh specified.  the few large images like basic elec, should have the fixed set.
                cell.FixedHeight = 150;

        cell.HorizontalAlignment = horiz_align;
        cell.VerticalAlignment = vert_align;

        cell.Colspan = colSpan;
        cell.Border = border;
        if (rowSpan != 1)
            cell.Rowspan = rowSpan;
        return cell;
    }


    private PdfPTable GCATAddTable(int columnCount, int widthPct = 100, int spacingAfter = 0)
    {
        PdfPTable ret = new PdfPTable(columnCount);
        ret.WidthPercentage = widthPct;
        ret.SpacingAfter = spacingAfter;
        return ret;
    }

    private PdfPCell GCATAddNestedCellTable(PdfPTable table, int border = 0)
    {
        PdfPCell ret = new PdfPCell(table);
        ret.Border = border;
        return ret;
    }



    private MemoryStream AddPageNumber(MemoryStream ms)
    {
        using (ms)
        {
            byte[] bytes = ms.ToArray();
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString() + "/" + reader.NumberOfPages, blackFont), 568f, 15f, 0);
                    }
                }
                return stream;
            }

        }
    }

    public static int GetNextGcatSectionImageIndex(sm_binaryEntities smb, SM_Enums.InspectionType inspType, int inspId, string sectionName)
    {
        //Get a list of the current Sections, Images


        List<insp_images> imList = smb.insp_images.Where(w => w.insp_type == inspType.ToString() && w.insp_id == inspId && w.insp_section_id.Contains(sectionName)).ToList();
        List<int> currentImageIndexes = new List<int>();
        foreach (insp_images im in imList)
        {

            string strIndex = im.insp_section_id.Replace("im", "");
            strIndex = strIndex.Replace(sectionName, "");
            int imIndex = 0;
            if (int.TryParse(strIndex, out imIndex))
                currentImageIndexes.Add(imIndex);

        }
        if (currentImageIndexes.Count == 0)
            return 0;
        int maxInt = currentImageIndexes.Max();
        return maxInt + 1;

    }
}


