﻿using SensibleDAL.dbml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for sitemap_generator
/// </summary>
public class sitemap_generator : System.Web.UI.Page //Needs to inherit Page for Server.Mappath();
{

    RzDataContext rdc = null;
    public sitemap_generator(RzDataContext _rdc)
    {
        //
        // TODO: Add constructor logic here
        //
        rdc = _rdc;
    }

    //This list will hold the names of all sitemaps generated in a session for use with the Robots.txt creation
    public static List<string> SitemapFileNames = new List<string>();

    class PartrecordObject
    {
        //Linq has limitations about instantiating dbml objects inside a query.  Custom class solves that.
        public string fullpartnumber { get; set; }
        public string stocktype { get; set; }
        public long quantity_available { get; set; }
        public DateTime date_created { get; set; }
    }

    public void CreateSitemaps(string stocktype)
    {
        switch (stocktype)
        {
            case "stock":
                {
                    CreateSitemap("stock");
                    break;
                }
            case "consign":
                {
                    CreateSitemap("consign");
                    break;
                }
            case "excess":
                {
                    CreateSitemap("excess");
                    break;
                }
            case "all":
                {
                    CreateSitemap("stock");
                    CreateSitemap("consign");
                    CreateSitemap("excess");
                    break;
                }
        }

        //Generate a robots.txt containing all created sitemaps
        GenerateRobotsTxt();


    }

    private void CreateSitemap(string stockType)
    {
        //This is the max number of URLS in any given Sitemap
        int takeAmount = 40000;
        //This is will be updated with every iteration of map generation
        int startIndex = 0;
        // this list will hold each generated XML Sitemap
        List<XmlDocument> listSiteMaps = new List<XmlDocument>();
        //This is the master list, for in-memory reference of the working list below.
        List<PartrecordObject> masterListPartRecords = new List<PartrecordObject>();

        rdc.CommandTimeout = 6000;
        masterListPartRecords = (from p in rdc.partrecords
                                 where p.stocktype == stockType
                                 && (p.fullpartnumber.Length > 0 && p.fullpartnumber != null)
                                  && !p.fullpartnumber.ToLower().Contains("gcat")
                                  && (p.quantity ?? 0 - p.quantityallocated ?? 0) > 0//and (isnull(quantity, 0) - isnull(quantityallocated, 0)) > 0 
                                 group p by new { p.fullpartnumber, p.stocktype } into partrecordGroup
                                 select new PartrecordObject
                                 {
                                     fullpartnumber = partrecordGroup.Key.fullpartnumber,
                                     stocktype = partrecordGroup.Key.stocktype,
                                     quantity_available = partrecordGroup.Sum(t => (t.quantity ?? 0 - t.quantityallocated ?? 0)),
                                     date_created = partrecordGroup.Max(m => m.date_created).Value
                                 }).OrderBy(o => o.fullpartnumber).ToList();
        int count = masterListPartRecords.Count();

        //This list is the working list, obeying the takeamount and startindex of the master list.
        List<PartrecordObject> siteMapPartList = new List<PartrecordObject>();

        siteMapPartList = masterListPartRecords.Skip(startIndex).Take(takeAmount).ToList();
        while (siteMapPartList.Count() > 0)
        {
            //Ad to list of sitemaps
            listSiteMaps.Add(GenerateSiteMapFromPartList(siteMapPartList));
            //Iterate the start Index
            startIndex += takeAmount;

            //If the remaining parts is more than the take amount, get the next batch per take amount, else get the parts remaining.
            int partsRemaining = masterListPartRecords.Count() - startIndex;
            //If no parts left, break
            if (partsRemaining <= 0)
                break;
            if (partsRemaining > takeAmount)
                partsRemaining = takeAmount;

            //Get the new range of partnumbers
            siteMapPartList = masterListPartRecords.GetRange(startIndex, partsRemaining);
        }



        //Carry an index in case any given stocktype generates more than 1 sitemap due to 40000 url limit
        int index = 1;
        foreach (XmlDocument x in listSiteMaps)
        {
            string siteMapFileName = "";
            //Save the sitemap to disk
            SaveSiteMap(x, stockType, index, out siteMapFileName);

            if (!string.IsNullOrEmpty(siteMapFileName))
            {
                //Add the sitemap to the list (For use with robots.txt)
                SitemapFileNames.Add(siteMapFileName);
                index++;
            }
        }
    }

    private void GenerateRobotsTxt()
    {

        //Set the path where the file will be saved
        string fileName = Server.MapPath("~/public/sitemap/robots.txt");

        // Check if file already exists. If yes, delete it.     
        if (File.Exists(fileName))
        {
            File.Delete(fileName);
        }

        // Create a new file   
        using (StreamWriter sw = new StreamWriter(File.Open(fileName, FileMode.Create), Encoding.UTF8))
        {
            foreach (string s in SitemapFileNames)
            {
                sw.WriteLine("Sitemap:  {0}", s);
            }
            sw.WriteLine("User-agent: *");
            sw.WriteLine("Allow: /");
        }
    }

    private static XmlDocument GenerateSiteMapFromPartList(List<PartrecordObject> listPartRecords)
    {
        //https://www.thatsoftwaredude.com/content/2060/how-to-dynamically-create-your-sitemap-in-aspnet

        // Declare an XmlDocument object (using System.Xml)
        XmlDocument doc = new XmlDocument();
        //Create an XML declaration (xml version="1.0"?)
        XmlDeclaration xmldecl;
        xmldecl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
        //Add the new node to the document
        //declaration must be the first line in the file
        XmlElement root = doc.DocumentElement;
        doc.InsertBefore(xmldecl, root);

        //adding the urlset namespace attributes to the urlset element
        XmlNode urlset = doc.CreateNode(XmlNodeType.Element, "urlset", "");
        XmlAttribute att = doc.CreateAttribute("xmlns");
        att.Value = "http://www.sitemaps.org/schemas/sitemap/0.9";
        XmlAttribute att2 = doc.CreateAttribute("xmlns:image");
        att2.Value = "http://www.google.com/schemas/sitemap-image/1.1";
        urlset.Attributes.Append(att);
        urlset.Attributes.Append(att2);


        foreach (PartrecordObject p in listPartRecords)
        {

            // declaring each sitemap tag that we'll be using
            XmlNode node = doc.CreateNode(XmlNodeType.Element, "url", "");
            XmlNode loc = doc.CreateNode(XmlNodeType.Element, "loc", "");
            XmlNode image = doc.CreateNode(XmlNodeType.Element, "image", "");
            XmlNode priority = doc.CreateNode(XmlNodeType.Element, "priority", "");
            XmlNode lastmod = doc.CreateNode(XmlNodeType.Element, "lastmod", "");
            XmlNode changefreq = doc.CreateNode(XmlNodeType.Element, "changefreq", "");
            // DateTime object used to format our date later
            DateTime dt = p.date_created;
            //Add the url for each part
            string urlPrefix = @"https://portal.sensiblemicro.com/public/search/part_detail.aspx?pn=";
            string partString = Tools.Strings.SanitizeInput(p.fullpartnumber.Trim().ToUpper());
            //partString = Tools.Strings.StripHtmlSymbols(partString);

            string htmlEncodedPartString = HttpUtility.UrlEncode(partString);
            string finalUrl = urlPrefix + htmlEncodedPartString;
            loc.InnerText = finalUrl;
            lastmod.InnerText = dt.ToString("yyyy-MM-dd");  // date must be in this format to be valid
            priority.InnerText = "0.9";
            changefreq.InnerText = "daily";
            node.AppendChild(loc);
            node.AppendChild(lastmod);
            node.AppendChild(priority);
            node.AppendChild(changefreq);
            urlset.AppendChild(node);  // everything gets appended to our first urlset element
        }

        doc.AppendChild(urlset);   // urlset is appended to our main document
        return doc;
    }


    private void SaveSiteMap(XmlDocument doc, string stockType, int index, out string siteMapFileName)
    {

        // sitemaps are normally found in the root of your website
        // but feel free to save the sitemap in whatever location makes sense to your website
        string suffix = "s";
        switch (stockType)
        {
            case "consign":
                {
                    suffix = "c";
                    break;
                }
            case "excess":
                {
                    suffix = "e";
                    break;
                }
        }

        if (stockType == "consign")
            suffix = "c";
        siteMapFileName = "sitemap_" + suffix + index.ToString() + ".xml";
        string path = Server.MapPath("~/public/sitemap/" + siteMapFileName);
        doc.Save(path);
        siteMapFileName = path;

    }


}