﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using SensibleDAL.dbml;
using Serilog.Core;

/// <summary>
/// Summary description for SM_Global
/// </summary>
public static class SM_Global
{
    public static RzTools rzt = new RzTools();


    /// <summary>
    /// Get or set the static important data.
    /// </summary>
    /// 
    public static bool isDemoUser { get { return CheckIsDemoUser(); } set { isDemoUser = value; } }
    public static bool isTest { get { return CheckIsTest(); } set { isTest = value; } }
    //public static bool isPreviewUser { get { return CheckIsPreviewUser(); } set { HttpContext.Current.Session["isPreviewUser"] = value; } }



    private static bool CheckIsDemoUser()
    {
        //string DemoCompanyID = "64DCFCC2-1E68-4AE8-98D1-09CC3E757C29";
        //XG 
        string DemoUserID = "875850A2-2D4B-4181-985F-EB9DFB9F410E";
        if (CurrentUser.ProviderUserKey.ToString().ToUpper() == DemoUserID)
            return true;
        return false;
    }

    private static bool CheckIsTest()
    {
        bool ret = false;
        //ret = System.Diagnostics.Debugger.IsAttached;
        return ret;
    }

    public static MembershipUser CurrentUser
    {
        get
        {
            if (HttpContext.Current.Session["CurrentUser"] == null)
                HttpContext.Current.Session["CurrentUser"] = Membership.GetUser();
            return (MembershipUser)HttpContext.Current.Session["CurrentUser"];
        }
        set { HttpContext.Current.Session["CurrentUser"] = value; }
    }

    //private static MembershipUser SetCurrentUser(string userName = null)
    //{
    //    MembershipUser ret = null;
    //    if (!string.IsNullOrEmpty(userName))
    //        ret = Membership.GetUser(userName);
    //    else
    //        ret = Membership.GetUser();
    //    HttpContext.Current.Session["CurrentUser"] = ret;
    //    return ret;
    //    //get { return Membership.GetUser(); }
    //    //set { HttpContext.Current.Session["CurrentUser"] = value; }
    //}

    public static ProfileCommon CurrentProfile
    {
        get
        {

            //MUST have a PreviewUser In Session
            if (CurrentUser == null)
                throw new Exception("Current User is null");
            ProfileCommon currentProfile = (ProfileCommon)ProfileCommon.Create(CurrentUser.UserName);
            HttpContext.Current.Session["CurrentProfile"] = currentProfile;
            return currentProfile;
        }


        set { HttpContext.Current.Session["CurrentProfile"] = value; }
    }

    public static company CurrentCompany
    {
        get
        {
            if (HttpContext.Current.Session["CurrentCompany"] != null)
                return (company)HttpContext.Current.Session["CurrentCompany"];

            //MUST have a PreviewUser In Session
            if (CurrentUser == null)
                throw new Exception("Cannot Determine Company:  CurrentUser is null");
            if (CurrentProfile == null)
                throw new Exception("Cannot Determine Company:  CurrentProfile is null");
            company currentCompany = null;
            using (RzDataContext rdc = new RzDataContext())
                currentCompany = rdc.companies.Where(w => w.unique_id == CurrentProfile.CompanyID).FirstOrDefault();
            HttpContext.Current.Session["CurrentCompany"] = currentCompany;
            return currentCompany;
        }
        set { HttpContext.Current.Session["CurrentCompany"] = value; }
    }

    public static MembershipUser PreviewUser
    {
        get
        {
            //Do we already have a Display User In Session

            return (MembershipUser)HttpContext.Current.Session["PreviewUser"];

        }
        set
        {
            HttpContext.Current.Session["PreviewUser"] = value;
        }

    }



    public static ProfileCommon PreviewProfile
    {
        get
        {

            //MUST have a PreviewUser In Session
            if (PreviewUser == null)
                return null;
            //throw new Exception("Preview Profile is null");
            ProfileCommon previewProfile = (ProfileCommon)ProfileCommon.Create(PreviewUser.UserName);
            HttpContext.Current.Session["PreviewProfile"] = previewProfile;
            return previewProfile;
        }
        set
        {
            HttpContext.Current.Session["PreviewProfile"] = value;


        }

    }

    public static company PreviewCompany
    {
        get
        {
            //MUST have a PreviewUser In Session
            if (PreviewUser == null)
                return null;
            //throw new Exception("Cannot Determine Preview Company:  Preview User is null");
            if (PreviewProfile == null)
                return null;
            //throw new Exception("Cannot Determine Preview Company:  Preview Profile is null");
            company previewCompany = null;
            using (RzDataContext rdc = new RzDataContext())
                previewCompany = rdc.companies.Where(w => w.unique_id == PreviewProfile.CompanyID).FirstOrDefault();

            HttpContext.Current.Session["PreviewCompany"] = previewCompany;
            return previewCompany;
        }
        set
        {
            HttpContext.Current.Session["PreviewCompany"] = value;


        }

    }


    private static bool CheckIsPreviewUser()
    {
        bool isPreviewProfile = HttpContext.Current.Session["isPreviewUser"] != null;
        return isPreviewProfile;

    }


    public static string GetHeadshotImagePath(SensibleDAL.ef.Rep rep)
    {
        string defaultImagePath = "~/Images/logo_flat_1.png";

        if (rep != null)
        {
            if (!string.IsNullOrEmpty(rep.HSPath))
                return rep.HSPath;
        }
        return defaultImagePath;
    }

    public static string GetLeaderboardImagePath(n_user u)
    {
        string defaultImagePath = "~/Images/logo_flat_1.png";

        if (u != null)
        {
            if (!string.IsNullOrEmpty(u.leaderboard_image_url))
                return u.leaderboard_image_url;
        }
        return defaultImagePath;
    }


    //public static ProfileCommon DisplayProfile
    //{
    //    get
    //    {
    //        if (HttpContext.Current.Session["DisplayProfile"] != null)
    //            return (ProfileCommon)HttpContext.Current.Session["DisplayProfile"];

    //        else if (!HttpContext.Current.Request.IsAuthenticated)
    //            return null;

    //        MembershipUser user = Membership.GetUser();
    //        if (user == null)
    //            return null;

    //        ProfileCommon displayProfile = (ProfileCommon)ProfileCommon.Create(user.UserName);
    //        HttpContext.Current.Session["DisplayProfile"] = displayProfile;
    //        return displayProfile;


    //    }
    //    set
    //    {
    //        HttpContext.Current.Session["DisplayProfile"] = value;
    //    }

    //}



    //public static company DisplayCompany
    //{
    //    get
    //    {
    //        if (HttpContext.Current.Session["DisplayCompany"] == null)
    //        {
    //            MembershipUser user = Membership.GetUser();
    //            if (user == null)
    //                return null;
    //            if (DisplayProfile.CompanyID != null) //Get the companyID from DisplayProfile
    //                HttpContext.Current.Session["DisplayCompany"] = rzt.GetCompanyByID(DisplayProfile.CompanyID);
    //        }
    //        return (company)HttpContext.Current.Session["DisplayCompany"];
    //    }
    //    set
    //    {
    //        HttpContext.Current.Session["DisplayCompany"] = value;
    //    }


    //}

    //public static companycontact DisplayContact
    //{
    //    get
    //    {
    //        if (HttpContext.Current.Session["DisplayContact"] != null)
    //            return (companycontact)HttpContext.Current.Session["DisplayContact"];

    //        else if (!HttpContext.Current.Request.IsAuthenticated)
    //            return null;

    //        MembershipUser user = Membership.GetUser();
    //        if (user == null)
    //            return null;

    //        companycontact displayContact = null;

    //        using (RzDataContext rdc = new RzDataContext())
    //            displayContact = rdc.companycontacts.Where(w => w.primaryemailaddress.Trim().ToLower() == user.Email.Trim().ToLower()).SingleOrDefault();
    //        if (displayContact == null)
    //            return null;

    //        HttpContext.Current.Session["DisplayContact"] = displayContact;
    //        return displayContact;
    //    }
    //    set
    //    {
    //        HttpContext.Current.Session["DisplayContact"] = value;
    //    }


    //}


    public static company DemoSalesCompany
    {
        get
        {
            //Kimball: "1075d0ea5a224c51a4ab053bf6ac8732"
            //Brooks: "64DCFCC2-1E68-4AE8-98D1-09CC3E757C29"
            //TRW: "b9723c6b3a9845c394e5bd24178d63b5"
            HttpContext.Current.Session["DemoSalesCompany"] = rzt.GetCompanyByID("1075d0ea5a224c51a4ab053bf6ac8732");
            return (company)HttpContext.Current.Session["DemoSalesCompany"];
        }
        set
        {
            HttpContext.Current.Session["DisplayCompany"] = value;
        }


    }


    public static company DemoConsignmentCompany
    {
        get
        {
            //string harrisRochesterUID = "54b32ae75a8f45ddbe5acc19abeee0c2";
           string xgUID = "29A199FF-0AAA-40EF-BAF6-8F7FBFAEC286";
            HttpContext.Current.Session["DemoConsignmentCompany"] = rzt.GetCompanyByID(xgUID);
            return (company)HttpContext.Current.Session["DemoConsignmentCompany"];
        }
        set
        {
            HttpContext.Current.Session["DisplayCompany"] = value;
        }

    }

    public static company DemoQualityCompany
    {
        get
        {

            //1075d0ea5a224c51a4ab053bf6ac8732 - Kimball -  Quality
            HttpContext.Current.Session["DemoConsignmentCompany"] = rzt.GetCompanyByID("1075d0ea5a224c51a4ab053bf6ac8732");
            return (company)HttpContext.Current.Session["DemoConsignmentCompany"];
        }
        set
        {
            HttpContext.Current.Session["DisplayCompany"] = value;
        }

    }


}
