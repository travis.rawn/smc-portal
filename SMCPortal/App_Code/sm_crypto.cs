﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for sm_crypto
/// </summary>
public class sm_crypto
{
    public sm_crypto()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string CreateHMAC256(string message, string key = null)
    {
        if (key == null)
            key = "d7edd96725a0d6ea37ada16e22b45da7c3ed6052";

        //key = "hello";
        //key = "d7edd96725a0d6ea37ada16e22b45da7c3ed6052"; //Tawk.To
        //message = "testing123";        
        ASCIIEncoding encoding = new ASCIIEncoding();
        byte[] keyByte = encoding.GetBytes(key);

        //HMACMD5 hmacmd5 = new HMACMD5(keyByte);
        //HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
        HMACSHA256 hmac256 = new HMACSHA256(keyByte);

        byte[] messageBytes = encoding.GetBytes(message);
        byte[] hashmessage = hmac256.ComputeHash(messageBytes);
        //lblResult.Text = ByteToString(hashmessage);
        return ByteToString(hashmessage);

        //byte[] messageBytes = encoding.GetBytes(message);
        //byte[] hashmessage = hmacmd5.ComputeHash(messageBytes);
        //this.hmac1.Text = ByteToString(hashmessage);


    }
    public static string ByteToString(byte[] buff)
    {
        string sbinary = "";

        for (int i = 0; i < buff.Length; i++)
        {
            sbinary += buff[i].ToString("X2"); // hex format
        }
        return (sbinary);
    }


}