﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;
using SensibleDAL;

/// <summary>
/// Summary description for RzTools
/// </summary>
public class RzTools
{

    RzDataContext RDC = new RzDataContext();


    public class SalesGoal
    {
        public string GoalName { get; set; }
        public string Agent { get; set; }
        public double GoalValue { get; set; }
        public double AchievedValue { get; set; }
        public double Percent { get; set; }
        public string Text { get; set; }

        public string OrderType { get; set; }
        public string ImageURL { get; set; }

        public List<double> SubGoals { get; set; }
    }

    public class NewCustomer
    {
        public string Companyname { get; set; }
        public string Agent { get; set; }
        public DateTime DateAdded { get; set; }
        public string QualifyingSaleNumber { get; set; } // The Sale Number that makes this a new customer
    }

    public List<orddet_line> GetSalesLinesForAgentBetweenDates(string agent_id, DateTime startDate, DateTime endDate, bool includeVoid = false, bool includeRMA = false)
    {
        return RDC.orddet_lines.Where(l => l.seller_uid == agent_id && l.customer_uid.Length > 0 && (l.orderdate_sales >= startDate && l.orderdate_sales < endDate)).ToList();
    }
    public List<ordhed_sale> GetSalesOrdersForAgentBetweenDates(string agent_id, DateTime startDate, DateTime endDate, bool includeVoid = false)
    {
        return RDC.ordhed_sales.Where(l => l.base_mc_user_uid == agent_id && l.companyname.Length > 0 && (l.orderdate >= startDate && l.orderdate < endDate) && l.isvoid == includeVoid).ToList();
    }




    List<string> ExcludedUsersList = new List<string> { "house", "vendor", "e-sales", "dead accounts (do not call)", "Chris Torrioni(SalesPhone)", "Adam Lang (Soft Phone)", "Ardian Zika", "Jackie Hatfield" };
    SM_Tools tools = new SM_Tools();
    public RzTools()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    //Rz Users
    public n_user GetRzUser(string rzUserID)
    {
        if (rzUserID.Length > 10)
            return RDC.n_users.Where(u => u.unique_id == rzUserID).FirstOrDefault();
        else
            throw new Exception("Invalid Rz User ID");

    }

    public double GetProjectedProfit(string n_user_uid, DateTime start, DateTime end, string type, List<string> badstatus)
    {

        switch (type.ToLower())
        {
            case "np":
                {
                    return (double)RDC.orddet_lines.Where(w => (w.customer_dock_date >= start && w.customer_dock_date <= end) && w.seller_uid == n_user_uid && !badstatus.Contains(w.status)).Sum(s => s.net_profit);
                }
            case "gp":
                {
                    return (double)RDC.orddet_lines.Where(w => (w.customer_dock_date >= start && w.customer_dock_date <= end) && w.seller_uid == n_user_uid && !badstatus.Contains(w.status)).Sum(s => s.gross_profit);
                }
            default:
                return 0;
        }
    }

    public IEnumerable<SalesGoal> GetMagicNumberCalculation(string goalName, int quotedWeight, int bookedWeight, int invoicedWeight)
    {
        var query = (from o in RDC.ordheds
                     join u in RDC.n_users on o.base_mc_user_uid equals u.unique_id
                     where o.orderdate >= DateTime.Now.AddDays(-30)
                     select new
                     {
                         Agent = u.name,
                         QuoteGoal = u.monthly_quote_goal,
                         BookedGoal = u.monthly_invoiced_goal,
                         InvoicedGoal = u.monthly_invoiced_goal,
                         QuotedAchieved = o.ordertype == "quote" ? o.profitamount : 0,
                         BookedAchieved = o.ordertype == "sales" ? o.profitamount : 0,
                         InvoicedAchieved = o.ordertype == "invoice" ? o.profitamount : 0,
                         OrderType = o.ordertype
                     }).ToList();

        //Calculate Magic Number Percentages
        var percentageQuery = query.GroupBy(g => g.Agent).Select(s => new
        {
            Agent = s.Key,
            QuotePercent = s.Sum(ss => ss.QuotedAchieved / ss.QuoteGoal) * 100 ?? 0,
            BookedPercent = s.Sum(ss => ss.BookedAchieved / ss.BookedGoal) * 100 ?? 0,
            InvoicedPercent = s.Sum(ss => ss.InvoicedAchieved / ss.InvoicedGoal) * 100 ?? 0,
        }).ToList();

        //Normalize max percent at 100
        percentageQuery = percentageQuery.Select(s => new
        {
            Agent = s.Agent,
            QuotePercent = s.QuotePercent >= 100 ? 100 : s.QuotePercent,
            BookedPercent = s.BookedPercent >= 100 ? 100 : s.BookedPercent,
            InvoicedPercent = s.InvoicedPercent >= 100 ? 100 : s.InvoicedPercent,
        }).ToList();


        //Group the query into a GoalList
        var GoalList = (percentageQuery.GroupBy(g => g.Agent).Select(s => new SalesGoal
        {
            Agent = s.Key,
            GoalName = goalName,
            GoalValue = 0,
            AchievedValue = s.Sum(ss => ss.QuotePercent + ss.BookedPercent + ss.InvoicedPercent) / 3
        })).OrderByDescending(o => o.AchievedValue).ToList();

        return GoalList;
    }

    public Dictionary<string, string> GetUsersFromLineItems(DateTime startDate, DateTime endDate)
    {
        Dictionary<string, string> ret = new Dictionary<string, string>();
        using (RzDataContext rdc = new RzDataContext())
        {
            //Sale Lines
            List<string> user_ids = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= startDate.Date && w.date_created.Value.Date <= endDate.Date).Select(s => s.seller_uid).Distinct().ToList();

            foreach (string id in user_ids)
            {
                string name = rdc.orddet_lines.Where(w => w.seller_uid == id).Select(s => s.seller_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(name))
                    if (!ret.ContainsKey(id))
                        ret.Add(id, name);
            }
            //Quote Lines
            List<string> quote_user_ids = rdc.orddet_quotes.Where(w => w.date_created.Value.Date >= startDate.Date && w.date_created.Value.Date <= endDate.Date).Select(s => s.base_mc_user_uid).Distinct().ToList();
            foreach (string id in quote_user_ids)
            {
                string name = rdc.orddet_quotes.Where(w => w.base_mc_user_uid == id).Select(s => s.agentname).FirstOrDefault();
                if (!string.IsNullOrEmpty(name))
                    if (!ret.ContainsKey(id))
                        ret.Add(id, name);
            }

            //Invoice Lines
            List<string> invoice_user_ids = rdc.orddet_lines.Where(w => w.orderdate_invoice.Value.Date >= startDate.Date && w.orderdate_invoice.Value.Date <= endDate.Date).Select(s => s.seller_uid).Distinct().ToList();
            foreach (string id in invoice_user_ids)
            {
                string name = rdc.orddet_lines.Where(w => w.seller_uid == id).Select(s => s.seller_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(name))
                    if (!ret.ContainsKey(id))
                        ret.Add(id, name);
            }

            //Split Lines
            List<string> split_user_ids = rdc.orddet_lines.Where(w => w.date_created.Value.Date >= startDate.Date && w.date_created.Value.Date <= endDate.Date).Select(s => s.split_commission_agent_uid).Distinct().ToList();
            foreach (string id in quote_user_ids)
            {
                string name = rdc.n_users.Where(w => w.unique_id == id).Select(s => s.name).FirstOrDefault();
                if (!string.IsNullOrEmpty(name))
                    if (!ret.ContainsKey(id))
                        ret.Add(id, name);
            }
        }


        return ret;
    }

    public IEnumerable<SalesGoal> GetMagicNumberCalculation2(string goalName, List<n_user> salesAgentsList, List<string> orderTypes, DateTime start, int quoteWeight = 1, int bookedWeight = 30, int invoicedWeight = 50)
    {


        IEnumerable<SalesGoal> ret = null;
        var query = (from o in RDC.ordheds
                     join u in RDC.n_users on o.base_mc_user_uid equals u.unique_id
                     where o.orderdate >= start && orderTypes.Contains(o.ordertype.ToLower()) && salesAgentsList.Select(s => s.unique_id).ToList().Contains(o.base_mc_user_uid)
                     select new
                     {
                         Agent = u.name,
                         quoteGoal = u.monthly_quote_goal,
                         bookedGoal = u.monthly_invoiced_goal,
                         invoicedGoal = u.monthly_invoiced_goal,
                         quotedProfit = o.ordertype == "quote" ? o.profitamount : 0,
                         bookedProfit = o.ordertype == "sales" ? o.profitamount : 0,
                         invoicedProfit = o.ordertype == "invoice" ? o.profitamount : 0,

                     }).ToList();

        var querySum = query.GroupBy(g => g.Agent).Select(s => new
        {
            Agent = s.Key,
            quotedProfit = s.Sum(ss => ss.quotedProfit ?? 0),
            bookedProfit = s.Sum(ss => ss.bookedProfit ?? 0),
            invoicedProfit = s.Sum(ss => ss.invoicedProfit ?? 0),
            quotedGoal = s.Max(ss => ss.quoteGoal ?? 0),
            bookedGoal = s.Max(ss => ss.bookedGoal ?? 0),
            invoicedGoal = s.Max(ss => ss.invoicedGoal ?? 0)

        });

        var queryWeighted = querySum.GroupBy(g => g.Agent).Select(s => new
        {
            Agent = s.Key,
            quotedProfit = s.Sum(ss => ss.quotedProfit / ss.quotedGoal) * quoteWeight,
            bookedProfit = s.Sum(ss => ss.bookedProfit / ss.bookedGoal) * bookedWeight,
            invoicedProfit = s.Sum(ss => ss.invoicedProfit / ss.invoicedGoal) * invoicedWeight
        });


        ret = (queryWeighted.GroupBy(g => g.Agent).Select(s => new RzTools.SalesGoal
        {
            Agent = s.Key,
            GoalName = goalName,
            AchievedValue = Math.Round(s.Sum(m => m.quotedProfit + m.bookedProfit + m.invoicedProfit) / 3, 2, MidpointRounding.AwayFromZero)
        })).ToList();



        List<RzTools.SalesGoal> missing = salesAgentsList.Where(w => !ret.Select(s => s.Agent).Distinct().ToList().Contains(w.name)).Select(s => new RzTools.SalesGoal
        {
            Agent = s.name,
            GoalName = goalName,
            AchievedValue = Convert.ToDouble(0)
        }).ToList();

        if (missing.Count > 0)
            ret.Concat(missing);
        return ret;
    }

    public Dictionary<string, string> GetCurrentRzUsers_DDL(string TeamType = null, bool showInactive = true)
    {
        Dictionary<string, string> ret = new Dictionary<string, string>();
        List<string> listTeams = new List<string>();
        List<n_user> rzUserList = new List<n_user>();
        if (TeamType != null)
        {
            switch (TeamType.ToLower())
            {
                case "sales":
                    {
                        listTeams = RDC.n_teams.Where(t => t.name.Contains("sales")).Distinct().OrderBy(o => o.name).Select(s => s.unique_id).ToList();
                        break;
                    }
            }

            rzUserList = RDC.n_users.Where(n => listTeams.Contains(n.main_n_team_uid)).ToList();


        }
        else
            rzUserList = RDC.n_users.OrderBy(n => n.name).ToList();

        rzUserList = rzUserList.Where(w => (w.is_inactive ?? false) != showInactive).ToList();

        ret = rzUserList.OrderBy(n => n.name).ToDictionary(n => n.unique_id, n => n.name);
        return ret;
    }



    public Dictionary<string, string> GetCompaniesForRzUser(MembershipUser u, bool includeVendors = false, bool includehouse = false, bool showAll = false)
    {
        ProfileCommon p = (ProfileCommon)ProfileCommon.Create(u.UserName, true);
        Dictionary<string, string> ret = null;
        if (Roles.IsUserInRole("admin"))
            ret = RDC.companies.ToDictionary(d => d.unique_id, d => d.companyname);
        else if (Roles.IsUserInRole("sm_internal"))
            ret = RDC.companies.Where(c => c.base_mc_user_uid == p.RzUserID).ToDictionary(d => d.unique_id, d => d.companyname);
        return ret;
    }

    public Dictionary<string, string> GetContactsForCompany(string CompanyID, string email = null)
    {
        //List<companycontact> ret = new List<companycontact>();
        Dictionary<string, string> ret = null;
        if (email != null)
        {
            ret = RDC.companycontacts.Where(w => w.base_company_uid == CompanyID && w.contactname.Trim().Length > 0 && w.primaryemailaddress.Trim().ToLower() == email.Trim().ToLower()).OrderBy(o => o.contactname).ToDictionary(d => d.unique_id, d => d.contactname);
        }
        else
        {
            ret = RDC.companycontacts.Where(w => w.base_company_uid == CompanyID && w.contactname.Trim().Length > 0).OrderBy(o => o.contactname).ToDictionary(d => d.unique_id, d => d.contactname);
        }
        if (ret.Count > 0)
            return ret;
        else
            return null;
    }
    public List<company> GetCompaniesByEmailDomain(string emailDomain)
    {
        List<company> ret = new List<company>();
        List<string> companyIDsforMatchedContacts = RDC.companycontacts.Where(w => w.primaryemailaddress.Contains(emailDomain)).Select(s => s.base_company_uid).ToList();

        if (companyIDsforMatchedContacts.Count > 0)
        {

            foreach (string s in companyIDsforMatchedContacts.Distinct())
            {
                company c = RDC.companies.Where(w => w.unique_id == s).SingleOrDefault();
                if (c != null)
                    if (!ret.Contains(c))
                        ret.Add(c);
            }

        }
        return ret;
    }

    public bool AddCompanyContactByEmailDomain(string customerEmail, out company rzCompany, out companycontact rzContact)
    {
        string trimmedAddress = customerEmail.Trim();
        MailAddress address = new MailAddress(trimmedAddress);
        string domain = address.Host; // host contains yahoo.com              
        rzCompany = null;
        rzContact = null;
        //find the company that matches the email domain if exactly one match

        List<company> companyList = GetCompaniesByEmailDomain(domain);
        //If exactly on company match
        if (companyList.Count == 1)
        {
            rzCompany = companyList[0];
            n_user u = RDC.n_users.Where(w => w.unique_id == companyList[0].base_mc_user_uid).FirstOrDefault();
            //check if a contact for the address already exists.
            if (!RDC.companycontacts.Where(w => w.primaryemailaddress == customerEmail).Any())
            {
                rzContact = new companycontact();
                string guid = Guid.NewGuid().ToString();
                rzContact.primaryemailaddress = trimmedAddress;
                rzContact.base_company_uid = rzCompany.unique_id;
                rzContact.companyname = rzCompany.companyname;
                rzContact.date_created = DateTime.Now;
                rzContact.unique_id = guid;
                rzContact.base_mc_user_uid = rzCompany.base_mc_user_uid;
                rzContact.agentname = rzCompany.agentname;
                rzContact.source = "Part Search Identified";
                RDC.companycontacts.InsertOnSubmit(rzContact);
                RDC.SubmitChanges();
                return true;

            }

        }
        //send email to notify sales agent
        return false;
    }

    public void SendNewContactEmail(string customerEmail, List<string> agentEmails, company matchedCompany)
    {
        //Email Subject
        string emailSubject = "New contact added in Rz for " + matchedCompany.companyname;

        n_user u = RDC.n_users.Where(w => w.unique_id == matchedCompany.base_mc_user_uid).SingleOrDefault();
        if (u != null)
            if (!string.IsNullOrEmpty(u.email_address))
                if (!agentEmails.Contains(u.email_address))
                    agentEmails.Add(u.email_address);

        if (u != null)
            if (u.email_address != null)
            {
                string toAddress;
                //if (u.name.ToLower() == "house")
                //    email = "ktill@sensiblemicro.com";
                //else              
                toAddress = u.email_address;
                string body = @"<b>Company Name: </b>" + matchedCompany.companyname + @"<br />";
                body += @"<b>Email Address: </b>" + customerEmail;
                tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, toAddress, emailSubject, body, new string[] { SystemLogic.Email.EmailGroupAddress.PortalAlert, "ctorrioni@sensiblemicro.com", "joemar@sensiblemicro.com" });

            }
    }



    public List<KeyValuePair<string, string>> GetCustomersForRzUser(string RzUserID)
    {

        List<KeyValuePair<string, string>> ret = (from c in RDC.orddet_lines
                                                  where c.seller_uid == RzUserID && c.customer_uid.Length > 0
                                                  select (new KeyValuePair<string, string>(c.customer_uid, c.customer_name))
                                          ).AsEnumerable().OrderBy(o => o.Value).Distinct().ToList();

        return ret;
    }

    //public List<KeyValuePair<string, string>> GetCompaniesForRzUser(string RzUserID, bool includeHouse = false)
    //{

    //    List<KeyValuePair<string, string>> ret = new List<KeyValuePair<string, string>>();
    //    if (includeHouse == false)
    //        ret = (from c in RDC.companies
    //               where c.base_mc_user_uid == RzUserID && c.unique_id.Length > 0
    //               select (new KeyValuePair<string, string>(c.unique_id, c.companyname))
    //                                      ).AsEnumerable().OrderBy(o => o.Value).Distinct().ToList();
    //    else
    //        ret = (from c in RDC.companies
    //               where (c.base_mc_user_uid == RzUserID || c.agentname.ToLower() == "house") && c.unique_id.Length > 0
    //               select (new KeyValuePair<string, string>(c.unique_id, c.companyname))
    //                                  ).AsEnumerable().OrderBy(o => o.Value).Distinct().ToList();

    //    return ret;
    //}

    //public Dictionary<string, string> GetCompaniesForRzUser(string RzUserID, bool includeHouse = false)
    //{

    //    Dictionary<string, string> ret = new Dictionary<string, string>();
    //    if (includeHouse == false)
    //        ret = RDC.companies.Where(w => w.base_mc_user_uid == RzUserID && w.unique_id.Length > 0).Select(s => new
    //        {
    //            s.unique_id,
    //            s.companyname
    //        }).OrderBy(o => o.companyname).Distinct().ToDictionary(x => x.unique_id, x => x.companyname);
    //    else
    //        ret = RDC.companies.Where(w => (w.base_mc_user_uid == RzUserID || w.agentname.ToLower() == "house") && w.unique_id.Length > 0).Select(s => new
    //        {
    //            s.unique_id,
    //            s.companyname
    //        }).OrderBy(o => o.companyname).Distinct().ToDictionary(x => x.unique_id, x => x.companyname);

    //    if (!ret.ContainsKey("534d0d3c2e42456796bc7f281e8cc4e1"))
    //        ret.Add("534d0d3c2e42456796bc7f281e8cc4e1", "KT Portal Test");

    //    return ret;
    //}

    public Dictionary<string, string> GetCompaniesForRzUser(string email_address)
    {
        string email = Tools.Strings.SanitizeInput(email_address);
        if (string.IsNullOrEmpty(email))
            throw new Exception(email + " is an invalid email address.");
        Dictionary<string, string> ret = new Dictionary<string, string>();

        ret = RDC.companies.Where(w => w.primaryemailaddress.Trim().ToLower() == email.Trim().ToLower() && w.unique_id.Length > 0).Select(s => new
        {
            s.unique_id,
            s.companyname
        }).OrderBy(o => o.companyname).Distinct().ToDictionary(x => x.unique_id, x => x.companyname);


        if (!ret.ContainsKey("534d0d3c2e42456796bc7f281e8cc4e1"))
            ret.Add("534d0d3c2e42456796bc7f281e8cc4e1", "KT Portal Test");

        return ret;
    }

    internal bool IsConsignmentPartner(RzDataContext rdc, company co)
    {

        List<string> consignmentPartnerUidList = rdc.consignment_manifests.AsEnumerable().Where(w => w.base_company_uid.Length > 0).Select(s => s.base_company_uid.Trim().ToUpper()).Distinct().ToList();
        if (consignmentPartnerUidList.Contains(co.unique_id.Trim().ToUpper()))
            return true;
        return false;
    }

    public double GetInvoiceCredits(List<string> invoiceIDs)
    {
        double ret = 0;
        if (invoiceIDs.Count > 0 && invoiceIDs != null)
        {

            double credits = 0;
            List<ordhit> hitList = new List<ordhit>();
            hitList = RDC.ordhits.Where(w => w.is_credit == true && invoiceIDs.Contains(w.the_ordhed_uid)).ToList();
            if (hitList.Count > 0)
                credits = hitList.Sum(s => s.hit_amount ?? 0);
            if (credits > 0)
                ret = credits;
            //From Rz's Query:
            //"select SUM(hit_amount) from ordhit where is_credit = 1 and the_ordhed_uid  = '" + InvoiceID + "'"
        }

        return ret;
    }

    public double GetInvoiceCharges(List<string> invoiceIDs)
    {
        double ret = 0;
        if (invoiceIDs.Count > 0 && invoiceIDs != null)
        {

            double credits = 0;
            List<ordhit> hitList = new List<ordhit>();
            hitList = RDC.ordhits.Where(w => w.is_credit != true && invoiceIDs.Contains(w.the_ordhed_uid)).ToList();
            if (hitList.Count > 0)
                credits = hitList.Sum(s => s.hit_amount ?? 0);
            if (credits > 0)
                ret = credits;
            //From Rz's Query:
            //"select SUM(hit_amount) from ordhit where is_credit = 1 and the_ordhed_uid  = '" + InvoiceID + "'"
        }
        return ret;
    }

    public List<KeyValuePair<string, string>> GetCustomersWithSalesForRzUser(string RzUserID, DateTime startDate, DateTime endDate)
    {

        //Get a list of lines from Rz
        var salesLines = RDC.orddet_lines.Where(l => l.seller_uid == RzUserID && l.customer_uid.Length > 0 && (l.orderdate_sales >= startDate && l.orderdate_sales < endDate) && l.status.ToLower() != "void" && !l.status.ToLower().Contains("rma"));
        //Identify their total sales
        var companiesAndSales = from s in salesLines
                                group s by s.customer_uid into g
                                select new
                                {
                                    g.First().customer_uid,
                                    g.First().customer_name,
                                    total = g.Sum(t => t.total_price)
                                };
        //Select the ones that have sales into Dict
        List<KeyValuePair<string, string>> ret = (from c in companiesAndSales
                                                  where c.total > 0
                                                  select (new KeyValuePair<string, string>(c.customer_uid, c.customer_name))
                                          ).AsEnumerable().Distinct().OrderBy(o => o.Value).ToList();
        return ret;
    }



    public Dictionary<string, string> GetCustomersWithSales(DateTime startDate, DateTime endDate)
    {
        Dictionary<string, string> ret = RDC.orddet_lines
            .Where(l => l.customer_uid.Length > 0 && l.customer_name.Length > 0 && l.customer_name != "Zetron Inc." && (l.orderdate_sales >= startDate && l.orderdate_sales < endDate))
            .Select(m => new { m.customer_uid, m.customer_name })
            .Distinct()
            .ToDictionary(g => g.customer_uid, g => g.customer_name, StringComparer.OrdinalIgnoreCase);

        return ret;
    }

    public List<KeyValuePair<string, string>> GetGCATCustomers(DateTime startDate, DateTime endDate, string partnumber = null)
    {
        List<KeyValuePair<string, string>> ret;
        if (!string.IsNullOrEmpty(partnumber))
        {
            partnumber = Tools.Strings.FilterTrash(partnumber);
            if (partnumber.Length >= 6)
                partnumber = partnumber.Trim().Substring(0, 6);

            ret = RDC.orddet_lines.Where(l => l.customer_uid.Length > 0 && (l.orderdate_sales >= startDate.Date && l.orderdate_sales <= endDate.Date.AddDays(1) && l.part_number_stripped.Trim().Contains(partnumber))).Select(s => new KeyValuePair<string, string>(s.customer_uid, s.customer_name)).Distinct().ToList();
            ret.Add(new KeyValuePair<string, string>("037ED306-8D90-42D6-AAAA-AD91B900F263", "Sensible Micro Corporation"));
        }
        else
            ret = RDC.orddet_lines.Where(l => l.customer_uid.Length > 0 && (l.orderdate_sales >= startDate && l.orderdate_sales < endDate)).Select(s => new KeyValuePair<string, string>(s.customer_uid, s.customer_name)).Distinct().ToList();

        return ret;
    }


    public List<KeyValuePair<string, string>> GetCurrentRzReps()
    {
        List<KeyValuePair<string, string>> ret = RDC.n_users.Select(u => new KeyValuePair<string, string>(u.unique_id, u.name)).AsEnumerable().OrderBy(u => u.Value).ToList();
        return ret;
    }

    public string GetRzRepEmail(string RzID)
    {
        return RDC.n_users.Where(u => u.unique_id == RzID).Select(e => e.email_address).FirstOrDefault().ToString();
    }

    public List<n_user> GetUsersForTeams(List<string> teamNames, bool includeInactive = false)
    {
        List<n_user> ret = new List<n_user>();
        using (RzDataContext rdc = new RzDataContext())
            ret = RzLogic.Teams.GetUsersForTeams(rdc, teamNames, includeInactive);

        return ret;


    }



    public List<n_team> GetTeams(List<string> teamNames = null)
    {
        List<n_team> ret = new List<n_team>();
        if (teamNames != null)
            ret = RDC.n_teams.Where(w => teamNames.ConvertAll(c => c.ToLower()).Contains(w.name.ToLower())).Distinct().ToList();
        else
            ret = RDC.n_teams.Distinct().ToList();
        List<n_team> test = ret.Where(w => !string.IsNullOrEmpty(w.name)).ToList();

        return test;
    }

    public bool isUserOnTeam(n_user u, string team_name)
    {

        string teamID = RDC.n_teams.Where(w => w.name.ToLower() == team_name.ToLower()).Select(s => s.unique_id).FirstOrDefault();
        if (string.IsNullOrEmpty(teamID))
            return false;
        List<string> userTeamIDs = new List<string>();
        userTeamIDs = GetTeamsForUser(u).Select(s => s.unique_id).ToList();
        if (userTeamIDs.Count == 0)
            return false;
        return userTeamIDs.Contains(teamID);
    }

    public List<n_team> GetTeamsForUser(n_user u)
    {
        List<n_team> ret = new List<n_team>();
        if (u == null)
            return ret;
        List<n_member> memberships = RDC.n_members.Where(w => w.the_n_user_uid == u.unique_id).ToList();
        foreach (n_member m in memberships)
        {
            n_team t = RDC.n_teams.Where(w => w.unique_id == m.the_n_team_uid).FirstOrDefault();
            if (t != null)
                ret.Add(t);
        }
        return ret;
    }



    public DataTable GetOutstandingInvoices_SalesAgents(List<n_user> n_userList, DateTime start, DateTime end)
    {

        DataTable d = new DataTable();
        d.Columns.Add("companyname", typeof(string));
        d.Columns.Add("invoicenumber", typeof(string));
        d.Columns.Add("ordertotal", typeof(double));
        d.Columns.Add("outstandingamount", typeof(double));
        d.Columns.Add("terms", typeof(string));
        d.Columns.Add("duedate", typeof(DateTime));
        d.Columns.Add("daysoverdue", typeof(int));
        //d.Columns.Add("overdueDays", typeof(Int32)); 
        foreach (n_user u in n_userList)
        {
            List<ordhed_invoice> InvoiceList = RDC.ordhed_invoices.Where(i => i.base_mc_user_uid == u.unique_id && i.orderdate > start && i.orderdate <= end).ToList();

            foreach (ordhed_invoice i in InvoiceList)
            {
                double InvoicePayments = RDC.checkpayments.Where(c => c.base_ordhed_uid == i.unique_id).Select(s => s.transamount).Sum() ?? 0;
                double outstandingamount = (double)i.ordertotal - InvoicePayments;
                DateTime DueDate = Convert.ToDateTime(i.orderdate).AddDays((int)i.days_to_pay);
                int DaysOverdue = (DateTime.Today - DueDate).Days;
                if (outstandingamount > 0 && DaysOverdue > 0)
                    d.Rows.Add(i.companyname, i.ordernumber, i.ordertotal, outstandingamount, i.terms, DueDate, DaysOverdue);

            }
        }
        return d;
    }
    //Get Current Total Invoiced Amount by Agent
    public double GetTotalInvoiced_Agent(List<string> agentIDList, DateTime start, DateTime end)
    {
        double? ret = 0;
        // ret = RDC.orddet_lines.Where(c => agentIDList.Contains(c.seller_uid) && !ExcludedStatus.Contains(c.status)).Select(s => s.total_price).Sum();
        ret = RDC.ordhed_invoices.Where(c => agentIDList.Contains(c.base_mc_user_uid) && c.orderdate > start && c.orderdate <= end).Select(s => s.ordertotal).Sum() ?? 0;

        return (double)ret;
    }




    public List<NewCustomer> GetNewCustomerListSingleUser(string n_user_uid, DateTime start, DateTime end)
    {


        List<NewCustomer> ret = new List<NewCustomer>();
        List<string> CustomerIDsWithSalesWithinYearOfStart = RDC.orddet_lines.Where(l => l.seller_uid == n_user_uid && l.orderdate_invoice >= start.AddYears(-1) && l.orderdate_invoice <= start).Select(s => s.customer_uid).Distinct().ToList();
        List<orddet_line> lines = RDC.orddet_lines.Where(l => l.seller_uid == n_user_uid && l.orderdate_invoice >= start && l.orderdate_invoice < end && l.status.ToLower() == "shipped" && !CustomerIDsWithSalesWithinYearOfStart.Contains(l.customer_uid)).ToList();

        ret = lines.GroupBy(g => g.customer_name).Select(s => new NewCustomer
        {
            Agent = s.Key,
            Companyname = s.Max(f => f.customer_name),
            DateAdded = (DateTime)s.Max(f => f.orderdate_sales),
            QualifyingSaleNumber = s.Max(f => f.ordernumber_sales)
        }).ToList();

        return ret;
    }


    public bool ValidateEmailInRz(string email)//check if email is present for any contact in Rz.
    {
        if (RDC.companycontacts.Any(c => c.primaryemailaddress.Trim() == email.Trim()))
        {
            return true;
        }
        return false;
    }



    public string GetCleanCompanyName(string companyname)
    {
        string ret = "";
        List<string> companyDelimiters = new List<string>() { "{", "[", "(" };
        List<int> foundIndexes = new List<int>();
        foreach (string s in companyDelimiters)
        {
            int i = companyname.IndexOf(s);
            if (i > 0)
                foundIndexes.Add(i);
        }
        int firstOccurrence = foundIndexes.Min(m => Convert.ToInt32(m.ToString()));
        if (firstOccurrence > 0)
            ret = companyname.Substring(0, firstOccurrence);
        else ret = companyname;
        //Get the Index of each character
        //trim at the lowest index value (earliest occurrence in string)


        return ret.TrimEnd();
    }


    public List<validation_tracking> GetAllValidationTracking(DateTime start, DateTime end)
    {
        List<validation_tracking> ret = RDC.validation_trackings.Where(w => w.date_created >= start && w.date_created <= end && w.new_stage.Length > 0).ToList();
        return ret;
    }

    public List<validation_tracking> GetInitialValidationTracking(DateTime start, DateTime end)
    {
        //The 1st tracking event should have PreValidation for both new and previous stages
        List<validation_tracking> ret = RDC.validation_trackings.Where(w => w.date_created >= start && w.date_created <= end && w.new_stage == "Prevalidation" && w.previous_stage == "Prevalidation").ToList();
        return ret;
    }

    public List<validation_tracking> GetCurrentValidationTracking(List<string> initialSalesIds)
    {
        //Current Trackings will be the latest (Max date_created) record in a group of sales ids
        //List<validation_tracking> ret = RDC.validation_trackings.Where(initialTracking.OrderBy(o => o.date_created).Select(s => s.orderid_sales).ToList();

        //List<validation_tracking> ret = (from p in RDC.validation_trackings
        //            where initialSalesIds.Contains(p.orderid_sales)
        //            group p by p.orderid_sales into op
        //            select new validation_tracking
        //            {

        //                orderid_sales = op.Key,
        //                date_created = op.Max(x => x.date_created),  

        //            }).ToList();

        List<validation_tracking> ret = new List<validation_tracking>();
        foreach (string s in initialSalesIds)
        {
            validation_tracking latesetValidation = RDC.validation_trackings.Where(w => w.orderid_sales == s).OrderByDescending(o => o.date_created).First();
            ret.Add(latesetValidation);
        }
        //        List<validation_tracking> ret = RDC.validation_trackings
        //      .Where(a => initialSalesIds.Contains(a.orderid_sales))
        //      .Select(b => b).OrderByDescending(y => y.date_created).Take(1).ToList();

        //        ret = RDC.validation_trackings.GroupBy(l => l.orderid_sales)
        //.Select(g => g.OrderByDescending(c => c.date_created).FirstOrDefault())
        //.ToList();

        return ret;
    }


    public List<validation_tracking> Validation_tracking_AverageTimetoValidationComplete()
    {
        List<validation_tracking> ret = new List<validation_tracking>();
        //Get all the Validation tracking Records Grouped by Orderid_Sales
        ret = RDC.validation_trackings.ToList();
        ret = ret.OrderBy(o => o.orderid_sales).ThenByDescending(d => d.date_created).ToList();
        return ret;
    }

    public List<validation_tracking> GetValidationTrackingCleanSO()
    {
        List<validation_tracking> ret = new List<validation_tracking>();
        ret = (from t in RDC.validation_trackings
               join s in RDC.ordhed_sales on t.orderid_sales equals s.unique_id
               where !s.ordernumber.ToLower().Contains("cr") && s.isvoid != true
               select new validation_tracking
               {
                   unique_id = t.unique_id,
                   orderid_sales = s.unique_id,
                   ordernumber_sales = s.ordernumber,
                   previous_stage = t.previous_stage,
                   new_stage = t.new_stage,
                   date_created = t.date_created,
                   agentname = t.agentname,
                   agent_uid = t.agent_uid,
                   companyname = t.companyname,
                   company_uid = t.company_uid,
                   hold_reason = t.hold_reason
               }).ToList();
        return ret;
    }





    public List<company> GetCompaniesByEmail(string email)//check if email is present for any contact in Rz.
    {
        List<company> ret = new List<company>();
        List<string> companyIDs = RDC.companycontacts.Where(w => w.primaryemailaddress.Trim().ToLower() == email.Trim().ToLower()).Select(s => s.base_company_uid).Distinct().ToList();
        if (companyIDs.Count > 0)
        {
            foreach (string s in companyIDs)
            {
                company c = GetCompanyByID(s);
                ret.Add(c);
            }
        }
        else
            return null;
        return ret;
    }

    public company GetCompanyByID(string s)
    {
        return RDC.companies.Where(w => w.unique_id == s).FirstOrDefault();
    }


    public bool isOverSeasTestVendor(string vendor_uid)
    {
        bool ret = false;
        Dictionary<string, string> dictServiceVendor = new Dictionary<string, string>();
        dictServiceVendor.Add("7a28f08ba2a1467d87bc5a2311f9d0b2", "White Horse Laboratories");
        dictServiceVendor.Add("c75d3d332e0849ab8794fba5a8a921f6", "Emporium Partners Aps");
        if (dictServiceVendor.ContainsKey(vendor_uid))
            ret = true;

        return ret;
    }




    public dealheader CreateRzBatch(company company, companycontact contact, string Part, string qty, string notes, string targetPrice, HubspotApis.HubspotApi.Deal theHubspotDeal = null)
    {

        //Initally set RepID to house in case unidentified.
        string RzRepID = "17a7e95b7bcb47b0a2501d422f899100"; //House Account, unless company object is not null
        if (company != null)
            if (!string.IsNullOrEmpty(company.base_mc_user_uid))
                RzRepID = company.base_mc_user_uid;

        List<orddet_quote> quoteLines = new List<orddet_quote>();
        List<orddet_rfq> rfqLines = new List<orddet_rfq>();
        List<partrecord> partrecordMatches = new List<partrecord>();

        //Create Batch
        dealheader batch = CreateDealHeader(company, contact, Part, notes, theHubspotDeal);

        //Create quote / req line
        orddet_quote q = CreateReqLine(new string[] { Part, qty, targetPrice }, batch);
        quoteLines.Add(q);

        string internalConcat = "";
        foreach (orddet_quote qq in quoteLines)
        {
            internalConcat += qq.fullpartnumber;
            if (qq != quoteLines.Last())
                internalConcat += ",";
        }
        batch.internal_parts = internalConcat;
        ////Add Bidlines from Rz Stock
        //partrecordMatches = GetBidLineMatches(Part);
        //if (partrecordMatches.Count > 0)
        //{
        //    foreach (partrecord p in partrecordMatches)            {
        //        orddet_rfq r = CreateBidLine(p, batch, qty, q.unique_id);
        //        RDC.orddet_rfqs.InsertOnSubmit(r);
        //        rfqLines.Add(r);
        //    }
        //

        //Set the base deal name, ex: AT453H QTY: 124
        batch.dealheader_name = Part + " QTY: " + qty + " (Portal Quote)";
        //Update the batch with Hubspot Details
        if (theHubspotDeal != null)
        {
            string hsCreateDate = theHubspotDeal.properties["createdate"].value;
            long longHSCreateDate = Convert.ToInt64(hsCreateDate);
            batch.hubspot_deal_id = theHubspotDeal.dealId;
            batch.hubspot_deal_created = true;
            batch.hubspot_deal_created_date = HubspotApis.HubspotApi.UnixTimeStampToDateTime(longHSCreateDate);
            batch.dealheader_name += " [HubID: " + theHubspotDeal.dealId + "]";

        }

        //Commit the Batch
        RDC.dealheaders.InsertOnSubmit(batch);
        //Commit the Req Lines
        RDC.orddet_quotes.InsertOnSubmit(q);
        //Submit changes
        RDC.SubmitChanges();

        //BatchEmailNotify(RzRepID, q, rfqLines, batch);
        return batch;

    }


    public dealheader CreateDealHeader(company company, companycontact contact, string partNumber, string notes = null, HubspotApis.HubspotApi.Deal theHubspotDeal = null)
    {
        dealheader ret = new dealheader();



        if (company == null)
            company = Companies.UnidentifiedCompanyAccount;
        Guid g = Guid.NewGuid();
        ret.unique_id = g.ToString();
        ret.companyid = company.unique_id;
        ret.base_mc_user_uid = company.base_mc_user_uid;
        ret.start_date = DateTime.Now;
        ret.is_closed = false;
        ret.grid_color = 0;
        ret.icon_index = 0;
        ret.date_created = DateTime.Now;
        ret.agentname = company.agentname;
        ret.internal_parts = partNumber.ToUpper().Trim();
        ret.date_modified = DateTime.Now;
        ret.manually_created = false;
        ret.is_approved = false;
        ret.customer_name = company.companyname;
        ret.customer_uid = company.unique_id;
        if (contact != null)
        {
            ret.contact_name = contact.contactname;
            ret.contact_uid = contact.unique_id;
        }
        ret.user_name = "";
        ret.is_sourced = false;
        ret.opportunity_stage = "rfq_received";
        ret.notes = notes ?? "";


        return ret;

    }


    public orddet_quote CreateReqLine(string[] arr, dealheader d)
    {

        double dblTarg_price = 0;
        int intTarg_quantity = 0;
        string part = "";


        //Part Number
        part = arr.GetValue(0).ToString().Trim().ToUpper();

        orddet_quote ret = new orddet_quote();
        Guid g = Guid.NewGuid();
        ret.unique_id = g.ToString();
        ret.icon_index = 0;
        ret.grid_color = 0;
        ret.ordertype = "Quote";
        ret.fullpartnumber = part;
        ret.source = "Portal Quote";
        //Target Qty
        //Need to strip alphanumerics for Rz Save, of Target Price, etc, users might include $, etc
        string str_targ_qty = "";
        if (arr[1] != null)
        {
            str_targ_qty = arr.GetValue(1).ToString();
            str_targ_qty = Tools.Strings.StripNonAlphaNumeric(str_targ_qty, false);

        }
        //Confirm valid int
        if (!int.TryParse(str_targ_qty, out intTarg_quantity))
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, str_targ_qty + " is not a valid target quantity.");
        ret.target_quantity = intTarg_quantity;


        //target price will defauylt to Zero if can't strip characters accurately.
        string strTargetPrice = "";
        if (arr[2] != null)
            strTargetPrice = arr[2].ToString();



        if (!string.IsNullOrEmpty(strTargetPrice))
        {

            //Strip any Dollar Signs to prevent the trailing Zeros of a double to become 500 instead of 5.
            strTargetPrice = strTargetPrice.Replace("$", "");

            if (!double.TryParse(strTargetPrice, out dblTarg_price))
            {
                strTargetPrice = Tools.Strings.StripNonAlphaNumeric(strTargetPrice, false);
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, strTargetPrice + " is not a valid target price.");
            }
        }
        ret.target_price = dblTarg_price;

        //Set the prefix to the 1st letter for Rz Part Search, not same as how Rz does it, but haven't found a good way to expose that logic (PartLogic) to the portal, short of including Rz Project in the portal. 
        char prefix = part[0];
        ret.prefix = prefix.ToString();
        //Set the baseNumber as everything after the prefix
        ret.basenumberstripped = part.Substring(1);
        //InternalStripped
        ret.internalstripped = part;
        //InternalPart_stripped - redundant??
        ret.internalpartnumber_stripped = part;
        //Set quote line to blue
        ret.grid_color = -16776961;

        ret.base_dealheader_uid = d.unique_id;
        ret.base_company_uid = d.customer_uid;
        ret.companyname = d.customer_name;
        ret.base_companycontact_uid = d.contact_uid;
        ret.contactname = d.contact_name;
        ret.base_mc_user_uid = d.base_mc_user_uid;
        ret.agentname = d.agentname;
        ret.orderdate = DateTime.Now;
        ret.date_created = DateTime.Now;
        ret.date_modified = DateTime.Now;
        return ret;
    }


    public List<partrecord> GetBidLineMatches(string Part)
    {
        return RDC.partrecords.Where(w => w.fullpartnumber == Part).ToList();
    }

    public orddet_rfq CreateBidLine(string[] arr, dealheader d, string the_orddet_quote_uid)
    {
        orddet_rfq ret = new orddet_rfq();
        Guid g = Guid.NewGuid();
        partrecord p = RDC.partrecords.Where(pp => pp.unique_id == arr.GetValue(3).ToString()).FirstOrDefault();
        ret.unique_id = g.ToString();
        ret.icon_index = 0;
        ret.grid_color = 0;
        ret.ordertype = "RFQ";
        ret.fullpartnumber = arr.GetValue(0).ToString();
        ret.manufacturer = arr.GetValue(1).ToString();
        ret.base_dealheader_uid = d.unique_id;
        ret.base_company_uid = p.base_company_uid;
        ret.companyname = p.companyname;
        //ret.base_companycontact_uid = p.contactid;
        //ret.contactname = d.contact_name;
        ret.base_mc_user_uid = d.base_mc_user_uid;
        ret.agentname = d.agentname;
        ret.orderdate = DateTime.Now;
        ret.date_created = DateTime.Now;
        ret.date_modified = DateTime.Now;
        ret.quantityordered = Convert.ToInt32(arr.GetValue(2));
        ret.the_orddet_quote_uid = the_orddet_quote_uid;

        if (p.stocktype.ToLower() == "stock" || p.stocktype.ToLower() == "consign")
        {
            ret.isinstock = true;
            ret.stockid = p.unique_id;
        }
        else if (p.stocktype.ToLower() == "excess")
        {
            ret.isinstock = false;
            ret.base_company_uid = p.base_company_uid;
            ret.companyname = p.companyname;
        }


        return ret;
    }

    public orddet_rfq CreateBidLine(partrecord p, dealheader d, string qty, string the_orddet_quote_uid)
    {
        orddet_rfq ret = new orddet_rfq();
        Guid g = Guid.NewGuid();
        //partrecord p = RDC.partrecords.Where(pp => pp.unique_id == arr.GetValue(3).ToString()).FirstOrDefault();
        ret.unique_id = g.ToString();
        ret.icon_index = 0;
        ret.grid_color = 0;
        ret.ordertype = "RFQ";
        ret.fullpartnumber = p.fullpartnumber.ToUpper();
        ret.manufacturer = p.manufacturer.ToUpper();
        ret.base_dealheader_uid = d.unique_id;
        ret.base_company_uid = p.base_company_uid;
        ret.companyname = p.companyname;
        //ret.base_companycontact_uid = p.contactid;
        //ret.contactname = d.contact_name;
        ret.base_mc_user_uid = d.base_mc_user_uid;
        ret.agentname = d.agentname;
        ret.orderdate = DateTime.Now;
        ret.date_created = DateTime.Now;
        ret.date_modified = DateTime.Now;
        ret.quantityordered = Convert.ToInt32(qty);
        ret.the_orddet_quote_uid = the_orddet_quote_uid;

        if (p.stocktype.ToLower() == "stock" || p.stocktype.ToLower() == "consign")
        {
            ret.isinstock = true;
            ret.stockid = p.unique_id;
        }
        else if (p.stocktype.ToLower() == "excess")
        {
            ret.isinstock = false;
            ret.base_company_uid = p.base_company_uid;
            ret.companyname = p.companyname;
        }


        return ret;
    }

    public void LogPortalSearchedPart(MembershipUser user, string partnumber, string mfg = null, string description = null, company c = null, companycontact cc = null, string qty = null, string notes = null, bool isQuote = false)
    {
        string ip = NetworkLogic.GetVisitorIpAddress();
        portal_searched_part psp = new portal_searched_part();
        psp.is_internal = RDC.n_users.Where(w => w.login_name == user.UserName).Any();
        psp.userID = user.ProviderUserKey.ToString();
        psp.userName = user.UserName;
        psp.requestorIP = ip;
        psp.fullpartnumber = partnumber;
        if (!string.IsNullOrEmpty(mfg))
            psp.manufacturer = mfg;
        if (!string.IsNullOrEmpty(description))
            psp.description = description;
        if (!string.IsNullOrEmpty(qty))
            psp.quantity = Convert.ToInt32(qty);
        if (!string.IsNullOrEmpty(notes))
            psp.notes = notes;
        if (c != null)
        {
            psp.company_uid = c.unique_id;
            psp.companyname = c.companyname;
        }
        if (cc != null)
        {
            psp.contact_uid = cc.unique_id;
            psp.contactname = cc.contactname;
        }

        psp.quote_requested = isQuote;
        psp.date_created = DateTime.Now;
        RDC.portal_searched_parts.InsertOnSubmit(psp);
        RDC.SubmitChanges();
    }

    //public void LogPortalSearchedPart(string userName, string partnumber, string mfg = null, string description = null, company c = null, companycontact cc = null, string qty = null, string notes = null, bool isQuote = false, string requestor_email = null)
    //{
    //    if (string.IsNullOrEmpty(userName))
    //        userName = "Public / Anonymous";


    //    string ip = tools.GetExternalIpAddress();
    //    portal_searched_part psp = new portal_searched_part();
    //    psp.is_internal = RDC.n_users.Where(w => w.login_name == userName).Any();
    //    psp.userID = userName;
    //    psp.userName = userName;
    //    psp.requestorIP = ip;
    //    psp.fullpartnumber = partnumber;
    //    if (!string.IsNullOrEmpty(mfg))
    //        psp.manufacturer = mfg;
    //    if (!string.IsNullOrEmpty(description))
    //        psp.description = description;
    //    if (!string.IsNullOrEmpty(qty))
    //        psp.quantity = Convert.ToInt32(qty);
    //    if (!string.IsNullOrEmpty(notes))
    //        psp.notes = notes;
    //    if (c != null)
    //    {
    //        psp.company_uid = c.unique_id;
    //        psp.companyname = c.companyname;
    //    }
    //    if (cc != null)
    //    {
    //        psp.contact_uid = cc.unique_id;
    //        psp.contactname = cc.contactname;
    //    }

    //    psp.quote_requested = isQuote;
    //    psp.requestor_email = requestor_email;
    //    psp.date_created = DateTime.Now;
    //    RDC.portal_searched_parts.InsertOnSubmit(psp);
    //    RDC.SubmitChanges();
    //}

    public void LogPortalSearchedPart(string userName, string partnumber, string mfg = null, string description = null, company c = null, companycontact cc = null, string qty = null, string notes = null, bool isQuote = false, string requestor_email = null, string campaignSource = null, string campaignId = null, string referrerUrl = null)
    {
        if (string.IsNullOrEmpty(userName))
            userName = "Public / Anonymous";


        string ip = NetworkLogic.GetVisitorIpAddress();
        portal_searched_part psp = new portal_searched_part();
        psp.is_internal = RDC.n_users.Where(w => w.login_name == userName).Any();
        psp.userID = userName;
        psp.userName = userName;
        psp.requestorIP = ip;
        psp.fullpartnumber = partnumber;
        if (!string.IsNullOrEmpty(mfg))
            psp.manufacturer = mfg;
        if (!string.IsNullOrEmpty(description))
            psp.description = description;
        if (!string.IsNullOrEmpty(qty))
            psp.quantity = Convert.ToInt32(qty);
        if (!string.IsNullOrEmpty(notes))
            psp.notes = notes;
        if (c != null)
        {
            psp.company_uid = c.unique_id;
            psp.companyname = c.companyname;
        }
        if (cc != null)
        {
            psp.contact_uid = cc.unique_id;
            psp.contactname = cc.contactname;
        }

        psp.quote_requested = isQuote;
        psp.requestor_email = requestor_email;
        psp.campaign_source = campaignSource ?? "";
        psp.campaign_id = campaignId ?? "";
        psp.referrer_url = referrerUrl ?? "";

        psp.date_created = DateTime.Now;
        RDC.portal_searched_parts.InsertOnSubmit(psp);
        RDC.SubmitChanges();
    }


    public List<orddet_line> GetLinePricing(string partNumber, string orderType)
    {
        //RzTools rzt = new RzTools();
        LineData lineData = new LineData();
        RzDataContext rzdc = new RzDataContext();
        string cleanedpart = Tools.Strings.FilterTrash(partNumber);
        List<orddet_line> pricingList = (rzdc.orddet_lines.Where(w => !LineData.GetInvalid_orddet_Status(true, true).Contains(w.status.ToLower())
                           && w.part_number_stripped.Contains(cleanedpart.Substring(0, partNumber.Length - 4)))).ToList();

        return pricingList;

    }



    public class parts
    {
        RzDataContext RDC = new RzDataContext();
        public Dictionary<string, int> MostSearchedParts(int maxResults = 0, company c = null)
        {
            Dictionary<string, int> returnList = new Dictionary<string, int>();
            //List<string> defaultList = new List<string>() { "AT45DB642D-CNU", "AT45DB642D-TU", "HMC862LP3E", "ADM2587EBRWZ", "AD8253ARMZ", "AD9650BCPZ25", "NFM55PC155F1H4L", "EWIXP465BADT", "KMFE10012MB214", "MC9S12UF32PBE", "SAA1064T" };
            //make a default filler list, using a List so I can know the index of the parts, Dictionaries have no order.
            List<KeyValuePair<string, int>> defaultList = new List<KeyValuePair<string, int>>()
                {
                     new KeyValuePair<string, int>("AT45DB642D-CNU", 124),
                     new KeyValuePair<string, int>("AT45DB642D-TU", 76),
                     new KeyValuePair<string, int>("HMC862LP3E", 856 ),
                     new KeyValuePair<string, int>("ADM2587EBRWZ", 213),
                     new KeyValuePair<string, int>("AD8253ARMZ", 77),
                     new KeyValuePair<string, int>("AD9650BCPZ25", 21),
                     new KeyValuePair<string, int>("NFM55PC155F1H4L", 12),
                     new KeyValuePair<string, int>("EWIXP465BADT", 976),
                     new KeyValuePair<string, int>("KMFE10012MB214", 28 ),
                };

            if (c != null)
            {
                //Get any partsearches that have been made, add to returnList     
                Dictionary<string, int> searchList = new Dictionary<string, int>();
                if (c != null)
                {
                    searchList = RDC.portal_searched_parts.Where(w => w.fullpartnumber.Length > 3 && w.company_uid == c.unique_id && w.is_internal == false).GroupBy(n => n.fullpartnumber.ToLower()).
                             Select(group =>
                                 new
                                 {
                                     Part = group.Key.ToUpper(),
                                     Count = group.Count()
                                 }).OrderByDescending(o => o.Count).Take(10).ToDictionary(x => x.Part, y => y.Count);
                    if (searchList.Count > 0)
                        foreach (var element in searchList)
                            if (!returnList.ContainsKey(element.Key))
                                returnList.Add(element.Key, element.Value);
                }
                //Get any quotes that have been made, add to returnList
                Dictionary<string, int> quoteList = new Dictionary<string, int>();
                quoteList = RDC.orddet_quotes.Where(w => w.fullpartnumber.Length > 3 && w.base_company_uid == c.unique_id).GroupBy(n => n.fullpartnumber.ToLower()).
                         Select(group =>
                             new
                             {
                                 Part = group.Key.ToUpper(),
                                 Count = group.Count()
                             }).OrderByDescending(o => o.Count).Take(10).ToDictionary(x => x.Part, y => y.Count);
                if (quoteList.Count > 0)
                    foreach (var element in quoteList)
                        if (!returnList.ContainsKey(element.Key))
                            returnList.Add(element.Key, element.Value);
            }
            //if returnList is less than 12, fill with defaultList unitl list has 10 items

            if (returnList.Count < 10)
            {
                foreach (KeyValuePair<string, int> kvp in defaultList)
                    if (returnList.Count < 10)
                        if (!returnList.ContainsKey(kvp.Key))
                            returnList.Add(kvp.Key, kvp.Value);
            }
            if (maxResults > 0)
                returnList = returnList.Take(maxResults).ToDictionary(pair => pair.Key, pair => pair.Value); ;
            return returnList;
        }







        public object SearchRzParts(string Part)
        {

            //pnlRzParts.Visible = true;
            //string strippedPart = tools.RemoveSpecialCharacters(Part);
            //List<string> stockTypesList = GetStockTypes();
            var query = (from p in RDC.partrecords
                             //where p.fullpartnumber.Contains(part) && stockTypesList.Contains(p.stocktype.ToLower()) && (p.quantity - p.quantityallocated) > 0
                         where p.fullpartnumber.Contains(Part) && (p.quantity - p.quantityallocated) > 0
                         orderby p.fullpartnumber
                         select new
                         {
                             partercord_uid = p.unique_id,
                             PartNumber = p.fullpartnumber.ToUpper(),
                             MFG = p.manufacturer.ToUpper(),
                             QTY = p.quantity - p.quantityallocated,
                             DateCode = p.datecode,
                             RoHS = p.rohs_info,
                             Type =
                             (
                             p.stocktype.ToLower() == "stock" || p.stocktype.ToLower() == "consign" ? "Stock" : "Excess"
                             ),
                             LeadTime = (
                             p.stocktype.ToLower() == "stock" ? "Stock" :
                             p.stocktype.ToLower() == "consign" ? "Stock" :
                             p.stocktype.ToLower() == "excess" ? "1-2 weeks" : "Unknown"
                             )
                         }).OrderByDescending(o => o.Type);
            if (query.Any())
                return query;
            else return null;

        }
    }

}