﻿using Newtonsoft.Json;
using System.Collections.Generic;

public class ReCaptcha
{
    public static string Validate(string EncodedResponse, bool isDev)
    {
        var client = new System.Net.WebClient();
        string PrivateKey = null;
        if (isDev)
            PrivateKey = "6Lf_zyYUAAAAALcBWFV4e05bz_3GVKKgj-jkQC8W";
        else
            PrivateKey = "6Lc1qSUTAAAAABkOkQkb3vy9xsqOjsPGv29lZyBD";

        var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse));

        var captchaResponse = JsonConvert.DeserializeObject<ReCaptcha>(GoogleReply);

        return captchaResponse.Success;
    }

    [JsonProperty("success")]
    public string Success
    {
        get { return m_Success; }
        set { m_Success = value; }
    }

    private string m_Success;
    [JsonProperty("error-codes")]
    public List<string> ErrorCodes
    {
        get { return m_ErrorCodes; }
        set { m_ErrorCodes = value; }
    }


    private List<string> m_ErrorCodes;
}