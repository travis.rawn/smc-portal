﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL.dbml;

/// <summary>
/// Summary description for Reward
/// </summary>
/// 
public class RewardSystem
{
    RzDataContext RDC = new RzDataContext();
    SM_Tools tools = new SM_Tools();
    RzTools rt = new RzTools();

    [Serializable]
    public class Goal
    {
        public Goal(string name, decimal targetValue, string ID = null)
        {
            if (ID == null)
                GoalID = new Guid();
            else
            {
                GoalID = new Guid(ID);
            }
            Name = name;
            TargetValue = targetValue;
        }
        private Guid _goalID;
        private string _name;  // the name field
        private string _private;
        private decimal _targetValue;
        private decimal _currentValue;
        private decimal _currentPercent;
        private string _goalTerm;
        private string _goalPeriod;
        private DateTime _periodStart;
        private DateTime _periodEnd;
        private DateTime _termStart;
        private DateTime _termEnd;



        public Guid GoalID    // the Name property
        {
            get
            {
                return _goalID;
            }
            set
            {
                _goalID = value;
            }
        }
        public string Name    // the Name property
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public decimal TargetValue
        {
            get
            {
                return _targetValue;
            }
            set
            {
                _targetValue = value;
            }
        }
        public decimal CurrentValue
        {
            get
            {
                return _currentValue;
            }
            set
            {
                _currentValue = value;
            }
        }
        public decimal CurrentPercent
        {
            get
            {
                return _currentPercent;
            }
            set
            {
                _currentPercent = value;
            }
        }

        public string GoalTerm    //Term that the entire goal spans before all counters reset, i.e. 1 year.
        {
            get
            {
                return _goalTerm;
            }
            set
            {
                _goalTerm = value;
            }
        }
        public string GoalPeriod    //Date range that the goal periods span, periodic counters reset, i.e. 4 months or Quarterly.
        {
            get
            {
                return _goalPeriod;
            }
            set
            {
                _goalPeriod = value;
            }
        }

        public DateTime PeriodStart    //Date range that the goal periods span, periodic counters reset, i.e. 4 months or Quarterly.
        {
            get
            {
                return _periodStart;
            }
            set
            {
                _periodStart = value;
            }
        }
        public DateTime PeriodEnd   //Date range that the goal periods span, periodic counters reset, i.e. 4 months or Quarterly.
        {
            get
            {
                return _periodEnd;
            }
            set
            {
                _periodEnd = value;
            }
        }
        public DateTime TermStart    //Date range that the goal periods span, periodic counters reset, i.e. 4 months or Quarterly.
        {
            get
            {
                return _termStart;
            }
            set
            {
                _termStart = value;
            }
        }

        public DateTime TermEnd    //Date range that the goal periods span, periodic counters reset, i.e. 4 months or Quarterly.
        {
            get
            {
                return _termEnd;
            }
            set
            {
                _termEnd = value;
            }
        }


        public void GetGoalTermDateRange(string goalTerm, out DateTime start, out DateTime end)
        {
            start = DateTime.MinValue;
            end = DateTime.MaxValue;
            switch (goalTerm.ToLower())
            {
                case "yearly":
                    {
                        int year = DateTime.Now.Year;
                        start = new DateTime(year, 1, 1);
                        end = new DateTime(year, 12, 31);
                        break;
                    }
                case "netprofitchallenge":
                    {
                        int year = DateTime.Now.Year;
                        start = new DateTime(2016, 4, 1);
                        end = new DateTime(year, 12, 31);
                        break;
                    }
            }
        }
        public void GetGoalPeriodDateRange(string goalPeriod, out DateTime start, out DateTime end)
        {
            start = DateTime.MinValue;
            end = DateTime.MaxValue;
            switch (goalPeriod.ToLower())
            {
                case "quarterly":
                    {
                        DateTime date = DateTime.Today;
                        int quarterNumber = (date.Month - 1) / 3 + 1;
                        start = new DateTime(date.Year, (quarterNumber - 1) * 3 + 1, 1);
                        end = start.AddMonths(3).AddDays(-1);
                        break;
                    }
            }
        }

    }

    [Serializable]
    public class RewardTier
    {
        public RewardTier(Guid goalID, string name, decimal minthresh, decimal maxthresh, decimal increment, decimal maxvalue, decimal multiplier = 1)
        {
            //
            // TODO: Add constructor logic here
            //   
            ID = new Guid();
            GoalID = goalID;
            Name = name;
            MinThresh = minthresh;
            MaxThresh = maxthresh;
            MaxValue = maxvalue;
            Increment = increment;
            Multiplier = multiplier;

        }
        private Guid _ID;
        private Guid _goalID;
        private string _Name;
        private decimal _maxThresh;
        private decimal _minThresh;
        private decimal _maxValue;  //maximum value that can be achieved in this tier
        private decimal _increment; //Increments of a Goal that earn rewards.   For Net Profit, it's 10,000 (10K in NP)
        private decimal _multiplier;   //Multiplier = at certain tiers the reward can be work more.  I.e. above 100k, multiplier = 1.5 for hours.




        public Guid ID    // the Name property
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        public Guid GoalID    // the Name property
        {
            get
            {
                return _goalID;
            }
            set
            {
                _goalID = value;
            }
        }

        public string Name  // the Name property
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        public decimal MinThresh
        {
            get
            {
                return _minThresh;
            }

            set
            {
                _minThresh = value;
            }
        }


        public decimal MaxThresh
        {
            get
            {
                return _maxThresh;
            }

            set
            {
                _maxThresh = value;
            }
        }
        public decimal MaxValue
        {
            get
            {
                return _maxValue;
            }

            set
            {
                _maxValue = value;
            }
        }

        public decimal Increment
        {
            get
            {
                return _increment;
            }

            set
            {
                _increment = value;
            }
        }

        public decimal Multiplier
        {
            get
            {
                return _multiplier;
            }

            set
            {
                _multiplier = value;
            }
        }



        public void SetTierValues(string tierName, decimal minthresh, Guid goalID)
        {
            Name = tierName;
            MinThresh = minthresh;
            //TierMinValue = tierMin;
            //TierLowerThreshold = tierLower;
            //TierUpperThreshold = tierUpper;
            GoalID = goalID;
        }


    }

    [Serializable]
    public class Reward
    {
        public Reward(decimal incrementvalue)
        {
            //
            // TODO: Add constructor logic here
            //        
            IncrementValue = incrementvalue; // NONO, the amount to divide the current value by.
        }



        private string _unique_id;
        private string _rewardUserID;
        private string _name;
        private decimal _value;
        private string _description;
        private decimal _incrementValue; //Reward Value earned per Tier increment achieved.  i.e hours would be 1, and in Tier4 it woulde be goal.IncrementValue * tier.Multiplier (1.5)

        private List<RewardTier> _tierList;
        //Moved from Goal
        private decimal _periodValue;
        private decimal _earnedPeriodValue; //Reward Value earned per Tier increment achieved.  i.e hours would be 1, and in Tier4 it woulde be goal.IncrementValue * tier.Multiplier (1.5)
        private decimal _termValue;
        private decimal _earnedTermValue; //Reward Value earned per Tier increment achieved.  i.e hours would be 1, and in Tier4 it woulde be goal.IncrementValue * tier.Multiplier (1.5)


        public string unique_id    // the Name property
        {
            get
            {
                return _unique_id;
            }
            set
            {
                _unique_id = value;
            }
        }

        public string rewardUserID
        {
            get
            {
                return _rewardUserID;
            }
            set
            {
                _rewardUserID = value;
            }
        }
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public decimal value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public string description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }
        public decimal IncrementValue //Declared value of reward per increment achieved.
        {
            get
            {
                return _incrementValue;
            }

            set
            {
                _incrementValue = value;
            }
        }

        public decimal PeriodValue
        {
            get
            {
                return _periodValue;
            }
            set
            {
                _periodValue = value;
            }
        }
        public decimal EarnedPeriodValue
        {
            get
            {
                return _earnedPeriodValue;
            }

            set
            {
                _earnedPeriodValue = value;
            }
        }
        public decimal TermValue
        {
            get
            {
                return _termValue;
            }
            set
            {
                _termValue = value;
            }
        }
        public decimal EarnedTermValue
        {
            get
            {
                return _earnedTermValue;
            }

            set
            {
                _earnedTermValue = value;
            }
        }
        public List<RewardTier> TierList
        {
            get
            {
                if (_tierList == null)
                    _tierList = new List<RewardTier>();
                return _tierList;
            }
            set { _tierList = value; }
        }

    }





    public void ClaimReward(reward_claim claim, Reward reward, Goal goal)//can't use n_users unless I plan on setting up all users in Rz.
    {
        //ensure claim amount is not more than available rewards.

        decimal totalRewards = reward.EarnedTermValue;
        decimal claimedRewards = GetClaimedRewardAmount(reward, claim.userid);
        if (claim.value > (totalRewards - claimedRewards))
            throw new Exception("Claim amount is more than the available remaining reward value.  Cannot claim.");

        Guid g = Guid.NewGuid();
        claim.is_deleted = false;
        claim.unique_id = g.ToString();
        claim.date_created = DateTime.Now;
        claim.date_modified = DateTime.Now;
        RDC.reward_claims.InsertOnSubmit(claim);
        RDC.SubmitChanges();
    }

    public decimal GetClaimedRewardAmount(Reward r, string userID)
    {
        decimal? usedAmount = RDC.reward_claims.Where(c => c.reward_uid == r.unique_id && c.userid == userID && c.approved == true && c.is_deleted == false).Select(s => s.value).Sum();
        if (usedAmount == null)
            usedAmount = 0;
        return (decimal)usedAmount;
    }
    public decimal GetClaimedRewardAmount(Reward r, List<string> userIDs)
    {
        decimal? usedAmount = RDC.reward_claims.Where(c => c.reward_uid == r.unique_id && userIDs.Contains(c.userid) && c.approved == true && c.is_deleted == false).Select(s => s.value).Sum();
        if (usedAmount == null)
            usedAmount = 0;
        return (decimal)usedAmount;
    }

    public decimal GetClaimedRewardAmount(Reward r)
    {
        decimal? usedAmount = RDC.reward_claims.Where(c => c.reward_uid == r.unique_id && c.approved == true && c.is_deleted == false).Select(s => s.value).Sum();
        if (usedAmount == null)
            usedAmount = 0;
        return (decimal)usedAmount;
    }

    public List<reward_claim> GetRewardClaims(string userID, bool ShowDeleted = false)
    {
        List<reward_claim> ret = (from c in RDC.reward_claims
                                  where c.userid == userID && c.is_deleted == ShowDeleted
                                  select c).OrderByDescending(c => c.date_created).ToList();
        return ret;
    }


    //Probably need to show this in a GridView, and let delete from there.
    public void DeleteClaim(string claimID, Page page, bool AllowOverride = false)
    {
        try
        {
            reward_claim claim = GetClaim(claimID);
            if (claim != null)
            {
                switch (claim.name.ToLower())
                {
                    case "reward hours":
                        {
                            if (!AllowOverride)
                                if (claim.usage_date < DateTime.Today)
                                    throw new Exception("Reward Hours claim cannot be deleted since it was used in the past.  Please see management to remove this claim.");
                            break;
                        }
                    case "sensible bux":
                        {
                            if (!AllowOverride)
                                if (claim.approved == true)
                                    throw new Exception("Sensible Bux claim cannot be deleted since it has aleready been approved and potentially already used.  Please see management to remove this claim.");
                            break;

                        }
                }
                claim.is_deleted = true;
                //RDC.reward_claims.DeleteOnSubmit(claim);
                RDC.SubmitChanges();
            }
            else
                tools.HandleResultJS("Claim does not exist, cannot delete.", true, page);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, page);
        }
    }
    public List<string> GetClaimIDsForUser(List<string> RzUserIDList, List<Reward> rList)
    {
        List<string> ret = new List<string>();
        List<string> rewardIDs = rList.Select(r => r.unique_id).ToList();
        ret = RDC.reward_claims.Where(c => RzUserIDList.Contains(c.userid) && c.is_deleted == false && rewardIDs.Contains(c.reward_uid)).Select(s => s.unique_id).ToList();
        return ret;
    }
    public List<string> GetClaimIDsForUser(string RzUserID, List<Reward> rList)
    {
        List<string> ret = new List<string>();
        List<string> rewardIDs = rList.Select(r => r.unique_id).ToList();
        ret = RDC.reward_claims.Where(c => c.userid == RzUserID && c.is_deleted == false && rewardIDs.Contains(c.reward_uid)).Select(s => s.unique_id).ToList();
        return ret;
    }
    public List<string> GetClaimIDsForUser(string UserID)
    {
        List<string> ret = new List<string>();
        ret = RDC.reward_claims.Where(c => c.userid == UserID && c.is_deleted == false).Select(s => s.unique_id).ToList();
        return ret;
    }

    public void ApproveClaim(string claimID, Page page)
    {
        try
        {
            reward_claim claim = GetClaim(claimID);
            if (claim != null)
            {
                claim.approved = !claim.approved;
                RDC.SubmitChanges();
            }
            else
                tools.HandleResultJS("Claim does not exist, cannot delete.", true, page);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, page);
        }
    }
    public reward_claim GetClaim(string claimID)
    {
        reward_claim claim = RDC.reward_claims.Where(c => c.unique_id == claimID).FirstOrDefault();
        return claim;
    }

    public decimal CaluclateEarnedPeriodReward(Reward r, Goal g, decimal pv = 0) //if isTerm == false, then it's a period calc.
    {

        decimal CurrentValue = 0;
        decimal EarnedValue = 0;
        //pv is a temp variable to handle Term Calculation withough modifyint r.PeriodValue
        //pv will only be greater thatn zero if an aug is explicitly passed to increase it
        if (pv > 0)
            CurrentValue = pv;
        else
            CurrentValue = r.PeriodValue;
        foreach (RewardTier t in r.TierList)
        {
            if (CurrentValue >= r.IncrementValue)// ensure earned amount is at least the base required Increment Value.
            {
                if (CurrentValue >= t.MinThresh) //if g.CurrentValue is within range of  this tier.  This tier potentially needs to be calculated.
                {
                    if (CurrentValue < t.MaxThresh)// Tier is withing calculable range
                    {
                        //increment is $10 for Bux, $10000 for Hours
                        decimal eligibleTierAmount = Convert.ToInt32(Math.Floor(CurrentValue) - t.MinThresh);
                        decimal incrementsEarned = Math.Floor((eligibleTierAmount / r.IncrementValue));
                        decimal incrementsMult = incrementsEarned * t.Multiplier;
                        EarnedValue += incrementsMult;//KT Note that this will always present earned rewards as an integer.  OK by me.  wait, what about half hours?

                    }
                    else
                    {
                        EarnedValue += t.MaxValue;
                    }
                }
            }
            else // g.CurrentValue is lower than current tier, 0 dollars earned this tier.
            {
                EarnedValue += 0;
            }
        }
        r.EarnedPeriodValue = EarnedValue;
        return EarnedValue;
    }

    //public decimal CaluclateEarnedPeriodReward(Reward r, Goal g, decimal pv = 0) //if isTerm == false, then it's a period calc.
    //{

    //    decimal CurrentValue = 0;
    //    decimal EarnedValue = 0;
    //    //pv is a temp variable to handle Term Calculation withough modifyint r.PeriodValue
    //    //pv will only be greater thatn zero if an aug is explicitly passed to increase it
    //    if (pv > 0)
    //        CurrentValue = pv;
    //    else
    //        CurrentValue = r.PeriodValue;
    //    foreach (RewardTier t in r.TierList)
    //    {
    //        if (CurrentValue >= r.IncrementValue)// ensure earned amount is at least the base required Increment Value.
    //        {
    //            if (CurrentValue >= t.MinThresh) //if g.CurrentValue is within range of  this tier.  This tier potentially needs to be calculated.
    //            {
    //                if (CurrentValue < t.MaxThresh)// Tier is withing calculable range
    //                {
    //                    //increment is $10 for Bux, $10000 for Hours
    //                    decimal eligibleTierAmount = Convert.ToInt32(Math.Floor(CurrentValue) - t.MinThresh);
    //                    decimal incrementsEarned = (eligibleTierAmount / r.IncrementValue);
    //                    decimal incrementsMult = incrementsEarned * t.Multiplier;
    //                    EarnedValue += Math.Floor(incrementsMult);//KT Note that this will always present earned rewards as an integer.  OK by me.  wait, what about half hours?

    //                }
    //                else
    //                {
    //                    EarnedValue += t.MaxValue;
    //                }
    //            }
    //        }
    //        else // g.CurrentValue is lower than current tier, 0 dollars earned this tier.
    //        {
    //            EarnedValue += 0;
    //        }
    //    }
    //    r.EarnedPeriodValue = EarnedValue;
    //    return EarnedValue;
    //}

    public Goal GetNpChallengeGoal()
    {

        Guid ID = new Guid("FE83E49A-ECD2-4CD0-9F7A-A631B230C5D0");
        Goal ret = new RewardSystem.Goal("Quarterly Net Profit", 50000, ID.ToString());
        //ret.GoalTerm = "yearly";
        ret.GoalTerm = "netprofitchallenge";
        ret.GoalPeriod = "quarterly";
        DateTime pStart;
        DateTime pEnd;
        DateTime tStart;
        DateTime tEnd;
        ret.GetGoalPeriodDateRange(ret.GoalPeriod, out pStart, out pEnd);
        if (DateTime.Today.Year == 2016)
        {
            ret.TermStart = new DateTime(2016, 4, 1);
            ret.TermEnd = new DateTime(2016, 12, 31);
        }
        else
        {
            ret.GetGoalTermDateRange(ret.GoalTerm, out tStart, out tEnd);
            ret.TermStart = tStart;
            ret.TermEnd = tEnd;
        }
        ret.PeriodStart = pStart;
        ret.PeriodEnd = pEnd;

        return ret;
    }

    public List<Reward> GetNpChallengeRewards(Goal goal)
    {
        List<Reward> ret = new List<Reward>();

        RewardSystem.Reward rBux = new RewardSystem.Reward(10);
        rBux.EarnedPeriodValue = 0;
        rBux.EarnedTermValue = 0;
        rBux.name = "Sensible Bux";
        rBux.unique_id = new Guid("b17ae131-b9c6-4892-a2bd-130b1e24e591").ToString();
        //public RewardTier(Guid goalID, string name, decimal minthresh, decimal maxthresh, decimal increment, decimal maxvalue, decimal multiplier = 1)
        rBux.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier1Bux", 0, 50000, 10000, 250, (decimal).05));//Tier 1
        rBux.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier2Bux", 50000, 80000, 10000, 150, (decimal).05));//Tier 2  
        rBux.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier3Bux", 80000, 100000, 10000, 150, (decimal).075));//Tier 3   
        rBux.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier4Bux", 100000, 1000000, 10000, 1000, (decimal).075));//Tier 4

        RewardSystem.Reward rHours = new RewardSystem.Reward(10000);
        rHours.EarnedPeriodValue = 0;
        rHours.EarnedTermValue = 0;
        rHours.name = "Reward Hours";
        rHours.unique_id = new Guid("f407244f-eb5c-477a-98a7-f11f19237cf4").ToString();
        rHours.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier1Hours", 0, 50000, 10000, 0, 0));//Tier 1
        rHours.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier2Hours", 50000, 80000, 10000, 3, 1));//Tier 2  
        rHours.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier3Hours", 80000, 100000, 10000, 2, 1));//Tier 3   
        rHours.TierList.Add(new RewardSystem.RewardTier(goal.GoalID, "Tier4Hours", 100000, 1000000, 10000, 10, (decimal)1.5));//Tier 4 //add multiplier        

        ret.Add(rBux);
        ret.Add(rHours);

        return ret;

    }

    public Dictionary<string, decimal> CalculateEarnedRewards(string userid, RewardSystem.Goal g, RewardSystem.Reward r)
    {
        Dictionary<string, decimal> ret = new Dictionary<string, decimal>();
        decimal PeriodEarned = 0;
        decimal TermEarned = 0;
        decimal quarterEarnedValue = 0;
        //pv is a temp variable to handle Term Calculation withough modifyint r.PeriodValue
        decimal pv = 0;
        //Period
        PeriodEarned += CaluclateEarnedPeriodReward(r, g);
        //Term
        //List<Tuple<string, DateTime, DateTime>> Quarters = tools.GetCurrentYearQuarterDateRanges();
        List<Tuple<string, DateTime, DateTime>> Quarters = GetNetProfitChallengeDateRanges();
        foreach (Tuple<string, DateTime, DateTime> q in Quarters)
        {
            pv = SensibleDAL.SalesLogic.GetShippedNetProfit_SingleUser(userid, q.Item2, q.Item3);
            if (pv > 0)//If anything has been earned this period
            {
                quarterEarnedValue = CaluclateEarnedPeriodReward(r, g, pv);
                TermEarned += quarterEarnedValue;
            }
              
        }

        ret.Add("periodEArned", PeriodEarned);
        r.EarnedPeriodValue = PeriodEarned;
        ret.Add("termEArned", TermEarned);
        r.EarnedTermValue = TermEarned;
        return ret;
    }


    public List<Tuple<string, DateTime, DateTime>> GetNetProfitChallengeDateRanges()
    {
        //int count_of_years_since_start = (new DateTime(2016, 1, 1) - DateTime.Today).Years;
        //var range = Enumerable.Range(2016, count_of_years_since_start);
        //List<Tuple<string, DateTime, DateTime>> ret = new List<Tuple<string, DateTime, DateTime>>();
        //foreach (int year in range)
        //{

        //    if (year != 2016)//2016 starts at Q2
        //        ret.Add(Tuple.Create("Q1", new DateTime(year, 1, 1), new DateTime(year, 3, 31)));
        //    ret.Add(Tuple.Create("Q2", new DateTime(year, 4, 1), new DateTime(year, 6, 30)));
        //    ret.Add(Tuple.Create("Q3", new DateTime(year, 7, 1), new DateTime(year, 9, 30)));
        //    ret.Add(Tuple.Create("Q4", new DateTime(year, 10, 1), new DateTime(year, 12, 31)));

        //}
        //return ret;
        return null;
    }

    public string GetRandomMessage()
    {       
        Random rnd = new Random();
        int n;
        n = rnd.Next(0, GetMessage().Count); //Generate random number between 0 and List Count

        return GetMessage().ElementAt(n);

    }

    public string GetRandomMessageMotivate()
    {

        Random rnd = new Random();
        int n;       
        n = rnd.Next(0, GetMessageMotivate().Count);
        return GetMessageMotivate().ElementAt(n);

    }

    public List<string> GetMessage()
    {
        List<string> messages = new List<string>();
        messages.Add("Great start, keep it up!");
        messages.Add("Whoah! You're killing it!");
        messages.Add("Chippin' Away!");
        messages.Add("You're on a gravy train with busciut wheels!");
        messages.Add("Son, you put that in a bottle, you got sumthin' sweeter than YooHoo!");
        messages.Add("The force is strong with this one!");
        messages.Add("Can I get an A-MEN!");
        messages.Add("You are handsome (or beautiful .... honestly, not sure, turn on your web cam?)");
        messages.Add("You look nice, have you done something with your hair?");
        messages.Add("No, those pants don't make you look fat at all!");
        messages.Add("Well played!  Almost as good as the (insert national sporting team reference here) last night!");
        messages.Add("That's how winning is done!");
        return messages;
    }

    public List<string> GetMessageMotivate()
    {
        List<string> messages = new List<string>();
        messages.Add("Will you fight?!");
        messages.Add("Win this one for the Gipper!");
        messages.Add("The day may come when the courage of men fail.  But it is not this day!");
        messages.Add("You've got to get mad, I mean plum mad-dog mean!");
        messages.Add("One inch at a time!");
        messages.Add("Let no man forget ... we are LIONS!");
        messages.Add("You're like a big bear man!");
        messages.Add("Sieze the day!");
        messages.Add("Could use a little wind here ...");
        messages.Add("You can’t wait for inspiration. You have to go after it with a club.");
        messages.Add("If you’re going to be thinking, you may as well think big.");
        messages.Add("Opportunity does not knock, it presents itself when you beat down the door.");
        messages.Add("A diamond is merely a lump of coal that did well under pressure.");
        messages.Add("Even if you are on the right track, you’ll get run over if you just sit there.");
        messages.Add("Failure is the condiment that gives success its flavor.");
        messages.Add("If you think you are too small to make a difference, try sleeping with a mosquito.");
        messages.Add("Life is like photography. You need the negatives to develop.");
        messages.Add("Life is a blank canvass, and you need to throw all the paint on it you can.");
        messages.Add("If you hit the target every time it’s too near or too big.");
        messages.Add("Life is a shipwreck but we must not forget to sing in the lifeboats.");
        messages.Add("I didn’t fail the test. I just found 100 ways to do it wrong.");
        messages.Add("Consider the postage stamp: its usefulness consists in the ability to stick to one thing ’til it gets there.");
        messages.Add("Good things come to those who wait… greater things come to those who get off their ass and do anything to make it happen.");
        messages.Add("Edison failed 10, 000 times before he made the electric light. Do not be discouraged if you fail a few times.");


        return messages;
    }

}

