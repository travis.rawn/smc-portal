﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for SM_PDF
/// </summary>
public class SM_PDF
{
    public SM_PDF()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Variables
    PdfPCell cell;//placeholder cell, will overwrite as needed
    PdfPTable table;//placeholder table, will overwrite as needed
    PdfPTable nested;//placeholder for nested tables
    SM_Tools tools = new SM_Tools();







    public void CreateSamplePdf(MemoryStream ms, Document document)
    {
        // Header part
        PdfPTable table = new PdfPTable(2);
        table.SetWidths(new int[] { 50, 50 });//50% widths for the 2 columns
        // first cell
        PdfPTable table1 = new PdfPTable(1);
        table1.DefaultCell.MinimumHeight = 30;
        table1.AddCell("Address 1");
        table1.AddCell("Address 2");
        table1.AddCell("Address 3");
        table.AddCell(new PdfPCell(table1));
        // second cell
        PdfPTable table2 = new PdfPTable(2);
        table2.AddCell("Date");
        table2.AddCell("Place");
        PdfPCell cell = new PdfPCell(new Phrase("References"));
        cell.MinimumHeight = 40;
        cell.Colspan = 2;
        table2.AddCell(cell);
        cell = new PdfPCell(new Phrase("destination"));
        cell.Colspan = 2;
        table2.AddCell(cell);
        table.AddCell(new PdfPCell(table2));
        // second row
        cell = new PdfPCell();
        cell.Colspan = 2;
        cell.MinimumHeight = 16;
        table.AddCell(cell);
        document.Add(table);
        // Body part
        table = new PdfPTable(6);
        table.SetWidths(new int[] { 1, 2, 6, 1, 2, 2 });
        table.AddCell("sl no");
        table.AddCell("qty");
        table.AddCell("Product");
        table.AddCell("units");
        table.AddCell("rate");
        table.AddCell("total");
        table.HeaderRows = 1;
        for (int i = 0; i < 6;)
        {
            table.DefaultCell.Border = Rectangle.ALIGN_LEFT | Rectangle.ALIGN_RIGHT;
            table.DefaultCell.MinimumHeight = 16;
            table.AddCell(i++.ToString());
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
        }
        table.DefaultCell.FixedHeight = (3);
        table.DefaultCell.Border = (Rectangle.ALIGN_LEFT | Rectangle.ALIGN_RIGHT | Rectangle.ALIGN_BOTTOM);
        table.AddCell("");
        table.AddCell("");
        table.AddCell("");
        table.AddCell("");
        table.AddCell("");
        table.AddCell("");
        document.Add(table);
    }

    public void CreateSamplePDF2(MemoryStream MS, Document document)
    {
        PdfPTable HeaderTable = new PdfPTable(12);
        HeaderTable.WidthPercentage = 100;
        //HeaderTable.AddCell("R1 C1");
        PdfPCell cell = new PdfPCell(new Phrase("Logo Area"));
        cell.Colspan = 3;
        cell.Rowspan = 3;
        HeaderTable.AddCell(cell);
        HeaderTable.AddCell("R1 C2");
        HeaderTable.AddCell("R1 C3");
        HeaderTable.AddCell("R1 C4");
        HeaderTable.AddCell("R1 C5");
        HeaderTable.AddCell("R1 C6");
        HeaderTable.AddCell("R1 C7");
        HeaderTable.AddCell("R1 C8");
        HeaderTable.AddCell("R1 C9");
        HeaderTable.AddCell("R1 C10");
        HeaderTable.AddCell("R1 C11");//not shown because I'm out of rows (because colspan on 1st element is 3)
        HeaderTable.AddCell("R1 C12");
        //Add table to document
        document.Add(HeaderTable);
    }

    public PdfPCell CreateSampleCell()
    {
        // second (spacer) cell
        //create table to insert into nested cell
        PdfPTable table2 = new PdfPTable(1);
        //create nested cell to insert into the nested table cell
        PdfPCell cell = new PdfPCell();
        //this is logo, so make it span all 4 rows
        cell.Rowspan = 4;
        //Remove the border of this cell
        cell.Border = Rectangle.NO_BORDER;
        //Add the nested cell to the nested table
        table2.AddCell(cell);
        cell = new PdfPCell(table2);
        cell.Border = Rectangle.NO_BORDER;
        //insert the nested table into the main table
        return cell;
    }



    public void generatePDF(MemoryStream ms, Document doc, List<PdfPTable> sectionTables, int cellBorder)
    {
        foreach (PdfPTable table in sectionTables)
        {
            doc.Add(table);
        }

        //KT Used to be wrapping all this in a table, which made it hard to control splitting, etc.  This adds each individually and is easier to manage.
        //doc.Add(addSectionTable(sectionTables, cellBorder, 10));
    }

    public void setPDFStyling()
    {

        //Spacer Column
        PdfPCell cellSpacer = new PdfPCell(new Phrase());
        cellSpacer.Border = Rectangle.NO_BORDER;
    }


    public PdfPCell CreateCell(string text, Font font, int colSpan = 1, BaseColor bgColor = null, int horiz_align = Element.ALIGN_LEFT, int vert_align = Element.ALIGN_MIDDLE, int border = Rectangle.NO_BORDER, int rowSpan = 1)
    {
        Phrase phrase = new Phrase(text, font);
        PdfPCell ret = new PdfPCell(phrase);
        ret.Colspan = colSpan;
        if (bgColor != null)
            ret.BackgroundColor = bgColor;
        ret.HorizontalAlignment = horiz_align;
        ret.VerticalAlignment = vert_align;
        ret.Border = border;
        ret.Rowspan = rowSpan;
        return ret;

        //throw new NotImplementedException();
    }


    public PdfPCell CreateCell(PdfPTable table, BaseColor bgColor = null, int border = Rectangle.NO_BORDER, int rowSpan = 1)
    {
        PdfPCell ret = new PdfPCell(table);
        ret.Border = border;
        ret.Rowspan = rowSpan;
        if (bgColor != null)
            ret.BackgroundColor = bgColor;
        return ret;
    }


    public PdfPCell CreateCellImage(string imagePath, int colSpan, int horiz_align, int vert_align, int border, int scalePercent, int rowSpan = 1)
    {
        Image i = Image.GetInstance(imagePath);
        PdfPCell cell = new PdfPCell(i);
        cell.HorizontalAlignment = horiz_align;
        cell.VerticalAlignment = vert_align;
        i.ScalePercent(scalePercent);
        cell.Colspan = colSpan;
        cell.Border = border;
        if (rowSpan != 1)
            cell.Rowspan = rowSpan;
        return cell;
    }

    public PdfPCell CreateCellImage(Image i, int colSpan, int horiz_align, int vert_align, int border, int scalePercent, int rowSpan = 1)
    {
        PdfPCell cell = new PdfPCell(i, true);//true is a bill for shrink to fit image
        cell.HorizontalAlignment = horiz_align;
        //adn fatrt.
        cell.VerticalAlignment = vert_align;
        i.ScalePercent(scalePercent);

        cell.Colspan = colSpan;
        cell.Border = border;
        if (rowSpan != 1)
            cell.Rowspan = rowSpan;
        return cell;
    }

    public PdfPCell CreateSingleCellImage(Image i, int horiz_align, int vert_align, int border, int fixedHeight = 0)
    {
        PdfPCell cell = new PdfPCell(i, true);//true is a bill for shrink to fit image
        cell.Colspan = 3;
        cell.HorizontalAlignment = horiz_align;
        cell.VerticalAlignment = vert_align;

        cell.Border = border;

        if (fixedHeight > 0)
            cell.FixedHeight = fixedHeight;

        return cell;
    }


    public PdfPTable CreateTable(int columnCount, int widthPct = 100, int spacingAfter = 0)
    {
        PdfPTable ret = new PdfPTable(columnCount);
        ret.WidthPercentage = widthPct;
        ret.SpacingAfter = spacingAfter;
        return ret;
    }

    public PdfPCell CreateNestedCellTable(PdfPTable table, int border = 0)
    {
        PdfPCell ret = new PdfPCell(table);
        ret.Border = border;
        return ret;
    }


    public PdfPTable addSectionTitle(string title)
    {
        PdfPTable ret = new PdfPTable(2);
        return ret;
    }

    public PdfPTable addSectionImagery(List<insp_images> ImageList, Font font, string title = null, int scalePercent = 20)
    {

        if (ImageList == null)
            return null;
        if (ImageList.Count <= 0)
            return null;
        //Variable to hold count of images for section
        int imageCount = ImageList.Count;



        //Get the number of columns, Max 4
        int numberOfColumns = 4;
        if (imageCount == 1)
            numberOfColumns = 1;
        else if (imageCount == 2)
            numberOfColumns = 2;
        else if (imageCount == 3)
            numberOfColumns = 3;

        //Instantiate the return variable using n columns
        PdfPTable ret = new PdfPTable(numberOfColumns);

        //Set table options
        ret.DefaultCell.Border = PdfPCell.NO_BORDER;
        ret.SpacingBefore = 5;
        ret.SpacingAfter = 5;
        ret.WidthPercentage = 100;
        int[] arrWidths = getRelativeColumnWidthbyInt(numberOfColumns);
        ret.SetWidths(arrWidths);

        //4 images overlap at 20 percent.
        if (numberOfColumns == 4)
            scalePercent = 15;

        //Title Cell
        PdfPCell titleCell = null;
        //1st add teh title cell before images (if any)
        if (!string.IsNullOrEmpty(title))
        {
            titleCell = CreateCell(title, font, imageCount, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            titleCell.Padding = 5; //Paddign to space out the title
            ret.AddCell(titleCell);
        }

        //for each section, get teh smallest image, and make that that max height for the other images. 
        //If that height is greated that 600, cap at 600
        int MaxImageHeight = GetMaxImageHeight(ImageList);
        if (MaxImageHeight > 600)
            MaxImageHeight = 600;

        //Loop through all images, craete the stuff.
        foreach (insp_images i in ImageList)
        {
            //Create Table to up to 2 cells, 1 column wide so the second cell is below( i.e. description)
            PdfPTable imageTable = new PdfPTable(1);
            //var test = "/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + i.insp_type;

            
            byte[] imgBytes = File.ReadAllBytes(i.img_path_web);
            imgBytes = tools.ResizeImage(imgBytes, 800, 50, "jpg", MaxImageHeight);
            Image image = Image.GetInstance(imgBytes);
            //ensure image scaled correctly
            cell = CreateCellImage(image, 1, Element.ALIGN_CENTER, Element.ALIGN_TOP, Rectangle.NO_BORDER, scalePercent);
            cell.Padding = 5;

            //For single images, need them to be large, but not too large.  Therefore, need to control the fixed Cell Height.
            if (imageCount < 3)
                cell.FixedHeight = MaxImageHeight / imageCount; //i.e. 1 image = 600 max, 2 images = 300 max,  images  = 200 max
            imageTable.AddCell(cell);


            //Include any text below the image;
            if (!string.IsNullOrEmpty(i.img_description))
            {
                cell = CreateCell(i.img_description, font);
                imageTable.AddCell(cell);
            }
            ret.AddCell(imageTable);

        }

        //You are creating a table with 4 columns. If you add 4 cells, one row will be rendered. 
        //If you add 8 cells, two rows will be rendered. However: if you only add 7 cells, then a 
        //single row will be added. The 3 cells in the incomplete row will be omitted because 
        //iText only renders complete rows.
        //By Default, iText will only print complete rows.  This overrides that.
        ret.CompleteRow();
        return ret;
    }

    private int GetMaxImageHeight(List<insp_images> insp_image_list)
    {
        int ret = 0;
        List<System.Drawing.Image> imageList = new List<System.Drawing.Image>();
        foreach (insp_images i in insp_image_list)
        {
            byte[] imgBytes = File.ReadAllBytes(i.img_path_web);
            using (var ms = new MemoryStream(imgBytes))
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                imageList.Add(img);

            }
            //using (var ms = new MemoryStream(i.img_blob))
            //{
            //    System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            //    imageList.Add(img);

            //}


        }

        ret = imageList.Min(s => s.Height);
        return ret;
    }

    private int GetImageRowCount(int imageCount, int numberOfColumns)
    {
        int rowSpan = 1;
        //IF we have an evenly divisble number of columns
        if (imageCount % numberOfColumns == 0)
            rowSpan = imageCount / numberOfColumns;
        else  //Else add number of needed rows to rowSpan
            rowSpan += imageCount / numberOfColumns;
        return rowSpan;
    }

    public string GetResult(string s, bool yesNo = false, bool invert = false)
    {
        if (invert)
            switch (s)
            {
                case "y":
                    {
                        s = "n";
                        break;
                    }
                case "n":
                    {
                        s = "y";
                        break;
                    }
            }

        switch (s.ToLower())
        {
            case "y":
            case "1":
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case "n":
            case "2":
                {
                    if (yesNo)
                        return "No";
                    return "Fail";
                }
            case "u":
                {

                    return "Unknown";
                }
            case "i":
                {
                    return "Inconclusive";
                }
            default:
                return s;
        }
    }



    public string GetResult(short? i, bool yesNo = false, bool invert = false)
    {

        if (invert)
            switch (i)
            {
                case 1:
                    {
                        i = 2;
                        break;
                    }
                case 2:
                    {
                        i = 1;
                        break;
                    }
            }

        switch (i)
        {
            case 0:
                {
                    return "-Choose-";
                }
            case 1:
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case 2:
                {

                    if (yesNo)
                        return "No";
                    return "Fail";
                }
            case 3:
                {
                    return "Inconclusive";
                }
            default:
                return i.ToString();
        }
    }

    public string GetResult(int? i, bool yesNo = false, bool invert = false)
    {
        if (invert)
            switch (i)
            {
                case 1:
                    {
                        i = 2;
                        break;
                    }
                case 2:
                    {
                        i = 1;
                        break;
                    }
            }

        switch (i)
        {
            case 0:
                {
                    return "-Choose-";
                }
            case 1:
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case 2:
                {

                    if (yesNo)
                        return "No";
                    return "Fail";
                }
            default:
                return i.ToString();
        }
    }


    public string GetResult(bool? b, bool yesNo = false, bool invert = false)
    {
        if (invert)
            b = !b;

        switch (b)
        {
            case true:
                {
                    if (yesNo)
                        return "Yes";
                    return "Pass";
                }
            case false:
                {
                    if (yesNo)
                        return "No";
                    return "Fail";
                }

            default:
                return "Unknown";
        }
    }


    public int[] getRelativeColumnWidthbyInt(int i)
    {
        switch (i)
        {

            case 2:
                return new int[] { 50, 50 };
            case 3:
                return new int[] { 33, 33, 33 };
            case 4:
                return new int[] { 25, 25, 25, 25 };
            default://Default = single image
                return new int[] { 100 };
        }

    }
    public MemoryStream AddPageNumber(MemoryStream ms, Font font, float xLoc, float yLoc)
    {
        using (ms)
        {
            byte[] bytes = ms.ToArray();
            if (font == null)
                font = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {//GetOVerConent makes sure it's placed Over the content, instead of being potentially covered up behind an existing element.
                        ColumnText.ShowTextAligned(stamper.GetOverContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString() + " of " + reader.NumberOfPages, font), xLoc, yLoc, 0);
                    }
                }
                return stream;
            }

        }
    }

    public void DownloadPDF(byte[] bytes, HttpContext x, string title)
    {
        string fileName = title + ".pdf";
        //Transmit the File
        x.Response.Clear();
        x.Response.ContentType = "application/pdf";
        x.Response.ClearHeaders();
        x.Response.ClearContent();
        //Tell the browser that you want the file downloaded (ideally) and give it a pretty filename
        x.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        //Write our bytes to the stream
        x.Response.BinaryWrite(bytes);
        //Close the stream (otherwise ASP.Net might continue to write stuff on our behalf)
        x.Response.End();
        //throw new NotImplementedException();
    }




}
public class SM_PDFFont
{

    public SM_PDFFont()
    {

    }
    //internal string LoadFontFamily(string family)
    //{
    //    switch (family.ToLower())
    //    {
    //        case "montserrat":
    //            string fpMontserrat = HttpContext.Current.Server.MapPath("~/Content/fonts/Montserrat-Bold.ttf");

    //    }

    //    ///Need to embed the font so we get access to special ASCII characters like Ω (alt+234) and µ (alt-230)
    //    string fp = HttpContext.Current.Server.MapPath("~/Content/fonts/arial.ttf");
    //    string georgiaFontString = @"/Content/fonts/georgia.ttf";

    //    if (!string.IsNullOrEmpty(fpMontserrat))
    //        fp = fpMontserrat;

    //    return fp;
    //}

    public static SM_PDFFont GetFont(string fontFamily)
    {
        SM_PDFFont ret = new SM_PDFFont();
        string fp = null;
        string fpBold = null;
        string fpLight = null;
        string arialPath = HttpContext.Current.Server.MapPath("~/Content/fonts/arial.ttf");//Isntantiate now so it will use Arial if I don't define a light version below;
        fp = arialPath;
        fpBold = arialPath;//Isntantiate now so it will use Arial if I don't define a bold version below;
        fpLight = arialPath;//Isntantiate now so it will use Arial if I don't define a light version below;

        switch (fontFamily.ToLower())
        {
            case "montserrat":
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath("~/Content/fonts/Montserrat-Light.ttf")))
                        fp = HttpContext.Current.Server.MapPath("~/Content/fonts/Montserrat-Light.ttf");
                    if (File.Exists(HttpContext.Current.Server.MapPath("~/Content/fonts/Montserrat-Bold.ttf")))
                        fpBold = HttpContext.Current.Server.MapPath("~/Content/fonts/Montserrat-Bold.ttf");
                    break;
                }
            case "georgia":
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath("~/Content/fonts/georgia.ttf")))
                        fp = HttpContext.Current.Server.MapPath("~/Content/fonts/georgia.ttf");
                    break;
                }
        }

        //string georgiaFontString = @"/Content/fonts/georgia.ttf";
        //string fpMontserrat = HttpContext.Current.Server.MapPath("~/Content/fonts/Montserrat-Bold.ttf");
        //if (!string.IsNullOrEmpty(fpMontserrat))
        //    fp = fpMontserrat;



        ret.fontPath = fp;
        ret.fontPathBold = fpBold;
        ret.fontPathLight = fpLight;

        ret.H0 = FontFactory.GetFont(ret.fontPathBold, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 13f, Font.NORMAL);
        ret.H0Light = FontFactory.GetFont(ret.fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 13f, Font.BOLD);
        ret.H1 = FontFactory.GetFont(ret.fontPathBold, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 11f, Font.NORMAL);
        ret.H2 = FontFactory.GetFont(ret.fontPathBold, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.NORMAL);
        ret.Normal = FontFactory.GetFont(ret.fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.NORMAL);
        ret.NormalArial = FontFactory.GetFont(arialPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.NORMAL); //for special characters, etc.
        ret.Bold = FontFactory.GetFont(ret.fontPathBold, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 8f, Font.NORMAL);
        ret.SmallBold = FontFactory.GetFont(ret.fontPathBold, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 6f, Font.NORMAL);
        ret.SmallNormal = FontFactory.GetFont(ret.fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 7f, Font.NORMAL);
        return ret;
    }

    private string fontFamily;
    private string fontPath { get; set; }
    private string fontPathBold { get; set; }
    private string fontPathLight { get; set; }
    public Font H0 { get; set; }
    public Font H0Light { get; set; }
    public Font H1 { get; set; }
    public Font H2 { get; set; }
    public Font Normal { get; set; }
    public Font NormalArial { get; set; }
    public Font Bold { get; set; }
    public Font SmallBold { get; set; }
    public Font SmallNormal { get; set; }

   


}
public class PDFFooter : PdfPageEventHelper
{
    // write on top of document
    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        base.OnOpenDocument(writer, document);
        //IF I wanted a constant Header:
        //PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        //tabFot.SpacingAfter = 10F;
        //PdfPCell cell;
        //tabFot.TotalWidth = 300F;
        //cell = new PdfPCell(new Phrase("Header"));
        //tabFot.AddCell(cell);
        //tabFot.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
    }

    // write on start of each page
    public override void OnStartPage(PdfWriter writer, Document document)
    {
        base.OnStartPage(writer, document);
    }

    // write on end of each page
    public override void OnEndPage(PdfWriter writer, Document document)
    {
        base.OnEndPage(writer, document);
        //PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        PdfPTable tabFot = new PdfPTable(8);
        PdfPCell cell;
        Font SmallFont = FontFactory.GetFont("georgia", 10f, Font.NORMAL);
        tabFot.TotalWidth = document.Right - document.Left;
        tabFot.HorizontalAlignment = Element.ALIGN_CENTER;
        string currentYear = DateTime.Today.Year.ToString();
        cell = new PdfPCell(new Phrase(new Chunk("\u00A9Copyright " + currentYear + " - Sensible Micro Corporation", SmallFont)));
        cell.Border = Rectangle.NO_BORDER;
        cell.Colspan = 8;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        tabFot.AddCell(cell);
        //KT setting the xstart position to 40, only way I can center footer.
        tabFot.WriteSelectedRows(0, -1, 40, document.Bottom, writer.DirectContent);
    }
    //write on close of document
    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
        base.OnCloseDocument(writer, document);
    }

}

public class PDFHeader : PdfPageEventHelper
{

    public PdfPTable headerTable { get; set; }
    public PDFHeader()
    {

    }

    public void AddTabletoHeader(PdfPTable table)
    {

    }

    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        base.OnOpenDocument(writer, document);
    }

    // write on start of each page
    public override void OnStartPage(PdfWriter writer, Document document)
    {
        base.OnStartPage(writer, document);
    }

    // write on end of each page
    public override void OnEndPage(PdfWriter writer, Document document)
    {
        PdfPCell cell;
        Font SmallFont = FontFactory.GetFont("georgia", 10f, Font.NORMAL);
        base.OnEndPage(writer, document);
        if (headerTable == null)
        {
            headerTable = new PdfPTable(8);
            cell = new PdfPCell(new Phrase(new Chunk("This is a BADASS HEADER!", SmallFont)));
            cell.Border = Rectangle.NO_BORDER;
            cell.Colspan = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            headerTable.AddCell(cell);
        }

        headerTable.TotalWidth = document.Right - document.Left;
        headerTable.HorizontalAlignment = Element.ALIGN_CENTER;
        //KT setting the xstart position to 40, only way I can center footer.
        //"50" below matches the left margin set.  
        headerTable.WriteSelectedRows(0, -1, document.LeftMargin, document.Top + document.TopMargin, writer.DirectContent);


    }
    //write on close of document
    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
        base.OnCloseDocument(writer, document);
    }

}