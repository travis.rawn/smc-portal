﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using SensibleDAL.dbml;
using SensibleDAL.ef;

public class SM_Quality_Logic
{
    gcatDataContext ciq = new gcatDataContext();
    //SMCPortalDataContext pdc = new SMCPortalDataContext();
    RzDataContext rdc = new RzDataContext();
    RzTools rzt = new RzTools();
    DataTable d = new DataTable();
    SM_Tools tools = new SM_Tools();
    sm_binaryEntities smb = new sm_binaryEntities();





    private static SM_PDF pdf = new SM_PDF();

    public SM_Quality_Logic()
    {

    }

    public List<insp_head> GetActiveInspections()//Inspectiosn wtih Fullpartnumber, and not Demo
    {
        List<string> inspectors = rzt.GetUsersForTeams(new List<string>() { "Warehouse" }).Select(s => s.unique_id).ToList();
        List<insp_head> ret = new List<insp_head>();
        using (sm_portalEntities pdc = new sm_portalEntities())
            ret = pdc.insp_head.Where(w => (w.fullpartnumber.Length > 0 && w.fullpartnumber != null) && inspectors.Contains(w.inspector_uid)).ToList();
        return ret;
    }

    public insp_head CreateNewIDEAInspectionHeader(ProfileCommon profile)
    {
        insp_head header = new insp_head();
        header = new insp_head();
        header.date_created = DateTime.Now;
        header.inspector = profile.FirstName + " " + profile.LastName;
        if (string.IsNullOrEmpty(profile.RzUserID))
            throw new Exception("Rz User id not Detected.  Please contact IT to ensure this property is set in your profile.");
        header.inspector_uid = profile.RzUserID;
        header.username = System.Web.Security.Membership.GetUser().ToString();
        header.insp_date = DateTime.Now;
        header.date_modified = DateTime.Now;
        header.status = "In Progress";
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            pdc.insp_head.Add(header);
            header.is_complete = false;
            pdc.SaveChanges();
        }



        return header;
    }



    public List<string> GetAllLegacyImageSectionIDs()
    {
        List<string> ret = new List<string>();
        ret = ret.Union(GetLegacyDetailImageSectionIDs()).Union(GetLegacyWhseImageSectionIDs()).ToList();
        return ret;
    }



    public List<string> GetLegacyDetailImageSectionIDs()
    {
        return new List<string>() { "AddTestingImage", "BulkImage", "DetailSolventsImage", "DetailVisualImage", "InitInspectionImage", "InnerContentsImage", "MechImage", "ReelImage", "Test", "TrayImage", "TubeImage", };

    }
    public List<string> GetLegacyWhseImageSectionIDs()
    {
        return new List<string>() { "GenCartonImage", "OuterCartonImage" };

    }

    public bool CheckInspectionLinesShipped(string insp_id)
    {
        //KT shelving for now, see ticket# 1799
        return false;
    }

    public int GetCustomerInspectionQuantity(insp_head header)
    {
        //IT's possible an inspection can be for more lines than the customer ordered, i.e. if we ordered more for future sale, stock buy, etc. 
        //Need to evaluate all lines on this inspection
        //See which are related to teh current customer( possible multiple customers could have multiple lines inspection on same PO?  Yes, but if different sales, each line would be a new  inspection).
        //Therefore there should only be 1 cvustomer per inspectoin, so we can use that.
        //Then return that count.
        //string ret = Header.quantity.ToString();
        int qty = 0;
        string salesOrderID = header.orderid_sales;
        if (salesOrderID == null)
            qty = (int)header.quantity;
        else
        {
            List<orddet_line> lineList = rdc.orddet_lines.Where(w => w.insp_id == header.insp_id.ToString()).ToList();
            if (lineList.Count > 0)
            {
                foreach (orddet_line l in lineList)
                {
                    if (l.orderid_sales == salesOrderID)
                        qty += (int)l.quantity;
                }
            }

        }
        return qty;

    }

    public DataTable GetGCATandIDEADataTable(string companyid, string searchString, string testtype, bool samples = false, string customerPO = null)
    {
        DataTable d = new DataTable();
        d.Columns.Add("testtype", typeof(string));
        d.Columns.Add("id", typeof(string));
        d.Columns.Add("companyname", typeof(string));
        d.Columns.Add("companyid", typeof(string));
        d.Columns.Add("date", typeof(DateTime));
        d.Columns.Add("customer_po", typeof(string));
        d.Columns.Add("part_number", typeof(string));

        //if (companyid == null)
        //    companyid = "64DCFCC2-1E68-4AE8-98D1-09CC3E757C29";

        switch (testtype)
        {
            case "GCAT":
                {
                    if (samples)
                        LoadGCATSamples(d);
                    else
                        LoadGCAT(d, companyid, customerPO);
                    // LoadTest(companyid, testtype, searchString;)
                    break;
                }
            case "IDEA":
                {
                    if (samples)
                        LoadIDEASamples(d);
                    else
                        LoadIDEA(d, companyid, customerPO);
                    break;
                }
            case "Both":
                {
                    if (samples)
                    {
                        LoadGCATSamples(d);
                        LoadIDEASamples(d);
                    }

                    else
                    {
                        LoadGCAT(d, companyid, customerPO);
                        LoadIDEA(d, companyid, customerPO);
                    }

                    break;
                }
        }
        if (searchString != null)
            d = FilterDT(d, searchString);
        if (customerPO != null)
            d = FilterDT(d, customerPO);
        d.DefaultView.Sort = "date desc";
        return d;
    }




    private void LoadGCAT(DataTable d, string companyid, string customerPO = null)
    {
        var query = ciq.ValidationReports.Where(w => w.is_deleted != true && w.company_uid == companyid && w.is_approved == true).Select(s => new { ValidationReportID = s.ValidationReportID, company_name = s.company_name, s.company_uid, date = s.ReportDate, customer_po = s.CustomerPO, part_number = s.MPN });
        if (!string.IsNullOrEmpty(customerPO))
            query = query.Where(w => w.customer_po == customerPO);
        foreach (var v in query)
        {
            d.Rows.Add("GCAT", v.ValidationReportID, v.company_name, v.company_uid, v.date, v.customer_po, v.part_number);
        }
    }
    private void LoadGCATSamples(DataTable d)
    {
        var query = ciq.ValidationReports.Where(w => w.is_deleted != true && (w.is_demo ?? false == true)).Select(s => new { ValidationReportID = s.ValidationReportID, company_name = s.company_name, s.company_uid, date = s.ReportDate, customer_po = s.CustomerPO, part_number = s.MPN });
        foreach (var v in query)
        {
            d.Rows.Add("GCAT", v.ValidationReportID, v.company_name, v.company_uid, v.date, v.customer_po, v.part_number);
        }
    }

    private void LoadIDEA(DataTable d, string companyid, string customerPO = null)
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            var query = pdc.insp_head.Where(w => (w.is_deleted ?? false != true) && w.result == "Pass" && (w.customer_uid == companyid)).Select(s => new { insp_id = s.insp_id, customer_name = s.customer_name, customer_id = s.customer_uid, date = s.insp_date, customer_po = s.customer_po, part_number = s.fullpartnumber });
            if (!string.IsNullOrEmpty(customerPO))
                query = query.Where(w => w.customer_po == customerPO);
            foreach (var v in query)
            {
                d.Rows.Add("IDEA", v.insp_id, v.customer_name, v.customer_id, v.date, v.customer_po, v.part_number);
            }
        }

    }

    private void LoadIDEASamples(DataTable d)
    {
        using (sm_portalEntities pdc = new sm_portalEntities())
        {
            var query = pdc.insp_head.Where(w => w.is_deleted != true && w.customer_name == "Sensible Micro Corporation" && (w.is_demo == true && w.is_insp_complete == true)).Select(s => new { insp_id = s.insp_id, customer_name = s.customer_name, customer_id = s.customer_uid, date = s.insp_date, customer_po = s.customer_po, part_number = s.fullpartnumber });
            foreach (var v in query)
            {
                d.Rows.Add("IDEA", v.insp_id, v.customer_name, v.customer_id, v.date, v.customer_po, v.part_number);
            }
        }
    }



    private DataTable FilterDT(DataTable d, string searchString = null)
    {
        int count = d.Rows.Count;
        DataView dv = new DataView(d);
        dv.RowFilter = "id LIKE '%" + searchString + "%' OR testtype LIKE '%" + searchString + "%'" + "OR part_number LIKE '%" + searchString + "%'" + "OR customer_po LIKE '%" + searchString + "%'";
        return dv.ToTable();
    }

    public void CreateIDEAInspectionPDF(string inspID)
    {
        if (string.IsNullOrEmpty(inspID))
            return;
        IDEAPdf ipdf = new IDEAPdf(inspID);
        ipdf.CreatePDF();

    }

    public void CreateNonconPDF(int nonConID)
    {
        if (nonConID == 0)
            return;
        NonconPdf npdf = new NonconPdf(nonConID);
        npdf.CreatePDF();

    }


    //InspectionImageControl Methods
    public string GetSectionIDfromClientID(string clientID, bool trimLastNumber = false)
    {
        //string contentplaceholder = trimPrefix ?? "MainContent_im";
        string ret = "";
        //int trim = clientID.LastIndexOf(contentplaceholder+"_im"); //trim to the 3rd index digit after "_" 1= _, 2 = i, 3= m - This will help me use this no matter what the contentID is
        ret = clientID.Replace("MainContent_", "");
        ret = ret.Replace("im", "");

        if (trimLastNumber)
            ret = tools.trimControlNumberSuffix(ret);
        return ret;
    }

    public List<insp_images> LoadInspectionImagery(string insp_type, int insp_id, string sectionID = null)
    {
        //Get a list of the Images for this section.
        List<insp_images> images = new List<insp_images>();
        var query = smb.insp_images.Where(w => w.insp_type == insp_type && w.insp_id == insp_id);

        if (!string.IsNullOrEmpty(sectionID))
        {
            query = query.Where(w => w.insp_section_id.ToLower().Contains(sectionID.ToLower()));
        }


        if (query.Any())
            images = query.ToList();
        return images;
    }
    public string GetInspectionImageryControlPrefix(string insp_type, string section)
    {

        switch (insp_type.ToLower())
        {
            case "gcat":
                return GetGCATControlPrefix(section);


        }
        return null;
    }

    private string GetGCATControlPrefix(string section)
    {
        switch (section.ToLower())
        {
            default:
                return "im";
        }
    }

    private string GetGCATSectionId(string section)
    {
        switch (section.ToLower())
        {
            default:
                return section;
        }

    }








    public class IDEAPdf
    {
        string strInspID = "";
        int intInspID = 0;

        public IDEAPdf(string ID)
        {
            strInspID = ID;
            intInspID = Convert.ToInt32(strInspID);
        }


        sm_binaryEntities smb = new sm_binaryEntities();
        SM_Tools tools = new SM_Tools();
        SM_PDF smpdf = new SM_PDF();
        //SMCPortalDataContext pdc = new SMCPortalDataContext();

        //Variables        
        insp_head idea_head;
        insp_whse idea_whse;
        insp_detail idea_detail;
        List<insp_images> HeaderImages = new List<insp_images>();
        List<insp_images> WhseImages = new List<insp_images>();
        List<insp_images> DetailImages = new List<insp_images>();
        List<insp_images> ExtendedImages = new List<insp_images>();

        //Too Confusing to make these global, easy for be accessing the wrong var, makes code less readable
        //PdfPCell cell;
        //PdfPTable table;
        byte[] bytes;
        HttpContext context = HttpContext.Current;
        Document document;
        public string fileName;
        public string documentTitle;




        //Font Styles 

        //public static string fontpath //Creating Name property
        //{
        //    get //get method for returning value
        //    {
        //        return GetFontPath();

        //    }
        //    set // set method for storing value in name field.
        //    {
        //        fontpath = value;
        //    }
        //}

        // public static string fontpath = HttpContext.Current.Server.MapPath("~/Content/fonts/arial.ttf");
        //private static string arial_fontpath = HttpContext.Current.Server.MapPath("~/Content/fonts/arial.ttf");
        //private Font H0 = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 13f, Font.BOLD);
        //private Font H1 = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 11f, Font.BOLD);
        //private Font H2 = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.BOLD);
        //private Font Normal = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.NORMAL);
        //private Font NormalArial = FontFactory.GetFont(arial_fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9f, Font.NORMAL);
        //private Font Bold = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 8f, Font.BOLD);
        //private Font SmallBold = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 6f, Font.BOLD);
        //private Font SmallNormal = FontFactory.GetFont(fontpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 7f, Font.NORMAL);


        //BackGroundColors
        BaseColor Gray = new BaseColor(224, 224, 224);
        BaseColor White = new BaseColor(255, 255, 255);
        SM_PDFFont theFont = SM_PDFFont.GetFont("Montserrat");


        public void CreatePDF()
        {
            LoadPDFData();
            LoadImageryData();


            if (!(idea_head.is_insp_complete ?? false))
                throw new Exception("This inspection is not complete.  Please try again when it is.");


            Page page = new Page();
            try
            {
                fileName = "IDEA_Inspection_Report_" + intInspID;


                using (var MS = new MemoryStream())
                {
                    //Main Document
                    document = new Document(PageSize.LETTER, 30, 30, 90, 25);
                    PdfWriter writer = PdfWriter.GetInstance(document, MS);

                    //Header             
                    var header = new PDFHeader();
                    header.headerTable = generatePageHeader();
                    writer.PageEvent = header;
                    //Footer
                    writer.PageEvent = new PDFFooter();
                    //The document must be "Opened" after being used in PdfWriter.GetInstance() otherwise there no writer associated and it does nothing.
                    //http://stackoverflow.com/questions/30672700/itextsharp-the-document-is-not-open-error-when-it-actually-is
                    document.Open();
                    smpdf.setPDFStyling();
                    smpdf.generatePDF(MS, document, generateSectionTables(), Rectangle.NO_BORDER);

                    //Finish Producing Document
                    document.Close();
                    //Add Page Numbers
                    bytes = smpdf.AddPageNumber(MS, theFont.Bold, 550f, 704f).ToArray();
                }
                //process bytes for client download
                smpdf.DownloadPDF(bytes, context, fileName);
            }
            catch (Exception ex)
            {
                page = context.Handler as Page;
                string s = ex.Message;
                tools.HandleResult("fail", ex.Message);
            }
        }


        private void LoadPDFData()
        {
            using (sm_portalEntities pdc = new sm_portalEntities())
            {
                idea_head = pdc.insp_head.Where(w => w.insp_id == intInspID).FirstOrDefault();
                if (idea_head == null)
                    return;
                idea_whse = pdc.insp_whse.Where(w => w.insp_id == intInspID).FirstOrDefault();
                idea_detail = pdc.insp_detail.Where(w => w.insp_id == intInspID).FirstOrDefault();
            }

            LoadHeaderData();
            if (idea_whse != null)
                LoadWhseData();
            if (idea_detail != null)
                LoadDetailData();
        }


        private void LoadImageryData()
        {
            List<string> HeaderSections = new List<string>() { "HeaderImageTop", "HeaderImageBot", "HeaderImageOther" };
            //List<string> DetailSections = new List<string>() { "DetailSolvents1", "DetailSolvents2", "DetailSolvents3", "DetailSolventsImage", "DetailMech1", "DetailMech2", "DetailMech3", "MechImage", "AddTesting1", "AddTesting2", "AddTesting3" };
            //List<string> DetailSections = new List<string>() { "DetailSolvents", "DetailSolvents2", "DetailSolvents3", "DetailSolventsImage", "DetailMech1", "DetailMech2", "DetailMech3", "MechImage", "AddTesting1", "AddTesting2", "AddTesting3" };
            if (idea_head.source_insp_id > 0)
            {
                HeaderImages = smb.insp_images.Where(w => w.insp_id == idea_head.source_insp_id && HeaderSections.Contains(w.insp_section_id)).ToList();
                DetailImages = smb.insp_images.Where(w => w.insp_id == idea_head.source_insp_id && !HeaderSections.Contains(w.insp_section_id)).ToList();

            }

            else
            {
                HeaderImages = smb.insp_images.Where(w => w.insp_id == intInspID && HeaderSections.Contains(w.insp_section_id)).ToList();
                DetailImages = smb.insp_images.Where(w => w.insp_id == intInspID && !HeaderSections.Contains(w.insp_section_id)).ToList();
            }
            
            ExtendedImages = smb.insp_images.Where(w => w.insp_id == intInspID && w.insp_section_id.Contains("ExtendedImagery")).ToList();
        }

        private void LoadHeaderData()
        {

            //throw new NotImplementedException();
        }


        private void LoadWhseData()
        {
            //throw new NotImplementedException();
        }



        private void LoadDetailData()
        {
            //throw new NotImplementedException();
        }




        private List<PdfPTable> generateSectionTables()
        {
            //Generaic containing table to prevent sections from breaking at page breaks           
            PdfPTable headerTab = GetNewSectionTable();
            List<PdfPTable> l = new List<PdfPTable>();
            List<Image> imageList = new List<Image>();
            //Header
            headerTab.AddCell(GenerateHeaderSection());
            if (HeaderImages.Count > 0)
                headerTab.AddCell(smpdf.addSectionImagery(HeaderImages, theFont.H2, "Header Imagery:", 17));
            l.Add(headerTab);
            //Warehouse

            if (idea_whse != null && idea_whse.is_complete == true)
            {
                PdfPTable whseTab = GetNewSectionTable();
                whseTab.AddCell(GenerateSectionTitle("Section 1: Warehouse Inspection"));
                whseTab.AddCell(GenerateSectionTable(GetSection1aCells()));
                whseTab.AddCell(GenerateSectionTable(GetSection1bCells()));
                l.Add(whseTab);
            }


            //Detail

            if (idea_detail != null && idea_detail.is_complete == true)
            {
                PdfPTable detailInsp = GetNewSectionTable();
                detailInsp.AddCell(GenerateSectionTitle("Section 2: Detailed Inspection"));
                //Inner Contents
                detailInsp.AddCell(GenerateSectionTable(GetSection2aCells()));
                List<insp_images> innerContentsImages = DetailImages.Where(w => w.insp_section_id.Contains("InnerContents")).ToList();
                if (innerContentsImages.Count > 0)
                    detailInsp.AddCell(smpdf.addSectionImagery(innerContentsImages, theFont.H2, "Inner Contents Imagery:", 17));
                //Carrier Type
                detailInsp.AddCell(GenerateSectionTable(GetSection2bCells(GetCarrierType())));
                List<insp_images> carrierImages = DetailImages.Where(w => w.insp_section_id.Contains("Reel") ||
                 w.insp_section_id.Contains("Tube") ||
                  w.insp_section_id.Contains("Tray") ||
                   w.insp_section_id.Contains("Bulk")).ToList();
                if (carrierImages.Count > 0)
                    detailInsp.AddCell(smpdf.addSectionImagery(carrierImages, theFont.H2, "Carrier Imagery:", 17));


                //2C
                detailInsp.AddCell(GenerateSectionTable(GetSection2cCells()));
                //2D
                detailInsp.AddCell(GenerateSectionTable(GetSection2dCells(), 3, new[] { 50, 40, 10 }));
                l.Add(detailInsp);
                if (idea_detail.is_solvent ?? false == true)
                {
                    PdfPTable solventTab = GetNewSectionTable();
                    solventTab.AddCell(GenerateSectionTable(GetSection2eCells()));
                    if (DetailImages.Where(w => w.insp_section_id.Contains("DetailSolvents") || w.insp_section_id.Contains("DetailSolventsImage")).ToList().Count > 0)
                        solventTab.AddCell(smpdf.addSectionImagery(DetailImages.Where(w => w.insp_section_id.Contains("DetailSolvents") || w.insp_section_id.Contains("DetailSolventsImage")).ToList(), theFont.H2, "Detail Solvent Imagery:", 17));
                    l.Add(solventTab);
                }

                if (idea_detail.is_mech ?? false == true)
                {

                    PdfPTable mechTab = GetNewSectionTable();
                    mechTab.AddCell(GenerateSectionTable(GetSection2fCells(), 3, new[] { 50, 40, 10 }));
                    //l.Add(GenerateSectionTable(GetSection2fCells(), 3, new[] { 50, 40, 10 }));
                    if (DetailImages.Where(w => w.insp_section_id.Contains("DetailMech")).ToList().Count > 0)
                        mechTab.AddCell(smpdf.addSectionImagery(DetailImages.Where(w => w.insp_section_id.Contains("DetailMech") || w.insp_section_id.Contains("MechImage")).ToList(), theFont.H2, "Mechanical Imagery:", 17));
                    l.Add(mechTab);

                }

                if (idea_detail.is_addtesting ?? false == true)
                {
                    PdfPTable addTestingTab = GetNewSectionTable();
                    addTestingTab.AddCell(GenerateSectionTable(GetSectionAdditionalCells(), 2, new[] { 20, 80 }));
                    List<insp_images> addTestingImages = DetailImages.Where(w => w.insp_section_id.Contains("AddTesting")).ToList();
                    if (addTestingImages.Count > 0)
                        addTestingTab.AddCell(smpdf.addSectionImagery(addTestingImages, theFont.H2, "Additional Testing Imagery:", 17));
                    l.Add(addTestingTab);
                }



            }

            if (ExtendedImages != null && ExtendedImages.Count > 0)
            {
                PdfPTable extendedImageryTab = GetNewSectionTable();
                extendedImageryTab.AddCell(GenerateSectionTitle("Section 3: Extended Imagery"));
                extendedImageryTab.AddCell(smpdf.addSectionImagery(ExtendedImages, theFont.H2, "", 17));
                l.Add(extendedImageryTab);
            }

            return l;
        }

        private PdfPTable GetNewSectionTable()
        {
            PdfPTable ret = smpdf.CreateTable(1, 100);
            ret.DefaultCell.Border = Rectangle.NO_BORDER;
            //Keep Together Will Attempt to keep an entire table together if possible.  
            //This means that  because details is so  bit, it will not try to fill the 1st page, instead
            //it will skip to page 2 and try to keep it all on its own page.
            //ret.KeepTogether = true;
            return ret;
        }

        private string GetCarrierType()
        {
            if (idea_detail.is_bulk == true)
                return "bulk";
            if (idea_detail.is_reel == true)
                return "reel";
            if (idea_detail.is_tray == true)
                return "tray";
            if (idea_detail.is_tube == true)
                return "tube";
            return null;

        }

        private PdfPTable GenerateHeaderSection()
        {
            PdfPTable sectionContainer = smpdf.CreateTable(4);
            //sectionContainer.SpacingAfter = 10f;
            sectionContainer.SetWidths(new int[] { 20, 30, 20, 30 });
            foreach (PdfPCell c in generateHeaderSectionCells())
                sectionContainer.AddCell(c);
            return sectionContainer;
        }



        private PdfPTable GenerateSectionTable(List<PdfPCell> cellList, int columns = 2, int[] widthArray = null, int spacingAfter = 15)
        {
            PdfPTable sectionContainer = smpdf.CreateTable(columns);
            if (widthArray != null)
                sectionContainer.SetWidths(widthArray);
            else
                sectionContainer.SetWidths(new int[] { 60, 40 });
            sectionContainer.SpacingAfter = spacingAfter;
            foreach (PdfPCell c in cellList)
            {
                sectionContainer.AddCell(c);
            }
            return sectionContainer;
        }



        private PdfPTable GenerateSectionTitle(string TitleName)
        {
            PdfPTable sectionTitle = smpdf.CreateTable(1);
            sectionTitle.AddCell(smpdf.CreateCell(TitleName, theFont.H0));
            sectionTitle.SpacingAfter = 20;
            sectionTitle.SpacingBefore = 10;
            return sectionTitle;
        }

        private PdfPCell GenerateSectionHeading(string headingTitle, int colspan = 2)
        {
            //Section Header Details
            PdfPCell cell = new PdfPCell();
            cell = smpdf.CreateCell(headingTitle, theFont.Bold, colspan, Gray);
            return cell;


        }
        //PageHeader
        private PdfPTable generatePageHeader()
        {
            //Main Header
            PdfPCell cell = smpdf.CreateCellImage(context.Server.MapPath("~/Images/pdf_logo_2018.png"), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER, 25, 1);
            PdfPTable HeaderContainer = smpdf.CreateTable(1, 100);
            PdfPTable table = smpdf.CreateTable(4, 100);
            table.SpacingBefore = 15f;
            table.SpacingAfter = 5f;
            //Set column Widths
            table.SetWidths(new int[] { 25, 25, 35, 15 });
            table.AddCell(cell);//Logo
            cell = smpdf.CreateCell("IDEA-STD-1010 ", theFont.H0Light, 1, null, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("INSPECTION REPORT", theFont.H0, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("F-840-007 Rev A", theFont.Bold, 1, null, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            HeaderContainer.AddCell(smpdf.CreateCell(table));


            //Sub-Header Details
            table = smpdf.CreateTable(8, 100);
            //Set column Widths
            table.SetWidths(new int[] { 12, 13, 12, 13, 12, 20, 10, 8 });
            cell = smpdf.CreateCell("Inspection ID: ", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell(strInspID, theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("Date: ", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell(idea_head.insp_date.Value.ToString("MM-dd-yyyy"), theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("Inspector: ", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell(idea_head.inspector, theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("Page:", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("", theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            HeaderContainer.AddCell(smpdf.CreateCell(table, Gray));

            return HeaderContainer;
        }
        //Header Section
        private List<PdfPCell> generateHeaderSectionCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();


            ret.Add(smpdf.CreateCell("Customer", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.customer_name, theFont.Normal));
            ret.Add(smpdf.CreateCell("Customer PO", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.customer_po, theFont.Normal));


            ret.Add(smpdf.CreateCell("Manufacturer", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.manufacturer, theFont.Normal));
            ret.Add(smpdf.CreateCell("Sales Order", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.ordernumber_sales, theFont.Normal));

            ret.Add(smpdf.CreateCell("Part Number", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.fullpartnumber, theFont.Normal));
            ret.Add(smpdf.CreateCell("Date Code", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.date_code, theFont.Normal));

            ret.Add(smpdf.CreateCell("Quantity", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.quantity.ToString(), theFont.Normal));
            ret.Add(smpdf.CreateCell("Package Type", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.package_type, theFont.Normal));

            ret.Add(smpdf.CreateCell("Packaging", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.packaging, theFont.Normal));
            ret.Add(smpdf.CreateCell("COO", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.coo, theFont.Normal));

            ret.Add(smpdf.CreateCell("Lot", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.lot, theFont.Normal));
            ret.Add(smpdf.CreateCell("Inspection Result", theFont.Bold));
            ret.Add(smpdf.CreateCell(idea_head.result, theFont.Normal));
            return ret;


        }
        //Inner Carton
        private List<PdfPCell> GetSection1aCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            //new nested table
            ret.Add(GenerateSectionHeading("1a: General Carton Inspection (IDEA 10.2.5.1)"));
            ret.Add(smpdf.CreateCell("Weigh product contents and record.", theFont.Normal));
            ret.Add(smpdf.CreateCell("Weight: " + idea_whse.gencarton1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Inspect outer package for evidence of damage.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.gencarton2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Photograph / Record any findings.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.gencarton3_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Inspect the sealing tape for evidence of tampering.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.gencarton4_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("If damage or evidence of tampering is present, note findings and alert vendor and carrier for a possible freight claim.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.gencarton5_text, theFont.Bold));
            return ret;


        }
        //Outer Carton
        private List<PdfPCell> GetSection1bCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("1b: Outer Product Carton Inspection (IDEA 10.1.5.2)"));
            ret.Add(smpdf.CreateCell("Inspect package for any signs of damage or signs or being opened.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Photograph contents while in box if they exhibit damage, tampering, or nonconformance.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Photograph labels and sealing tape if they exhibit damage, tampering, or nonconformance.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton3_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Inspect sealing tapes.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton4_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Inspect labels and verify data.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton5_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Examine the box and compare with manufacturer's website or golden sample.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton6_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Scan barcode to verify information is readable.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_whse.outcarton7_text, theFont.Bold));
            return ret;
        }
        //Inner Contents
        private List<PdfPCell> GetSection2aCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("2a: Inner Contents Inspection (IDEA 10.1.5.3)"));

            ret.Add(smpdf.CreateCell("Inspect inner contents, packaging, bag seal, and labels; photograph if they exhibit damage, tampering, or nonconformance.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Compare inner labels with outer carton labels and photograph.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Verify bag seal date with product date code.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont3_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Scan the barcode to verify the information is readable and matches the product identification, record results.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont4_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Verify the product is properly packaged for the required Moisture Sensitivity Label (MSL) and compare the MSL level with datasheet.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont5_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Verify the package is ESD compliant.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont6_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Read and Record Status of the Humidity Indicator Card (HIC).", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.inner_cont7_text, theFont.Bold));
            return ret;
        }
        //Carrier Type
        private List<PdfPCell> GetSection2bCells(string carrierType)
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("2b: Inspection of Carrier (IDEA 10.1.5.4)", 1));
            ret.Add(GenerateSectionHeading("Carrier Type: " + tools.CapitalizeOneFirstLetter(carrierType), 1));
            switch (carrierType.ToLower())
            {
                case "tube":
                    {
                        ret.Add(smpdf.CreateCell("If factory packaged, verify tubes are imprinted with manufacturer name / logo.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tube1_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify all tubes are the same length and look clean and new (not yellowed or excessively scratched).", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tube2_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify parts in the tube are all oriented in the same direction.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tube3_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify quantity of parts in tubes is consistent.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tube4_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify all tubes have stoppers and they are the same in each tube.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tube5_text, theFont.Bold));
                        break;
                    }
                case "reel":
                    {
                        ret.Add(smpdf.CreateCell("Verify labels and label placement.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel1_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Scan and verify that barcode scan and printed information on label are consistent with each other.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel2_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify that size of reel and the material (paper /plastic) match manufacturer spec sheet.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel3_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify factory reel has proper leader tape.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel4_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify reel count (no empty pockets) and cover film is properly attached.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel5_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Compare part orientation in tape with manufacturer spec sheet and verify the parts oriented in the tape are consistent.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel6_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify that there are no splices or cuts in the tape.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.reel7_text, theFont.Bold));
                        break;
                    }
                case "tray":
                    {
                        ret.Add(smpdf.CreateCell("Verify the color and width of the banding is what is expected.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tray1_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Note if the banding has any pre-printed markings.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tray2_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify that the trays are oriented the same way.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tray3_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify that there is a top tray to protect the parts.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tray4_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify and record if covered by cardboard and verify if cardboard is anti-static.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tray5_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify the chips' orientation in the tray is consistent with no missing pieces.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.tray6_text, theFont.Bold));
                        break;
                    }
                case "bulk":
                    {
                        ret.Add(smpdf.CreateCell("Verify count.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.bulk1_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify the bag is correct for the type of parts received, (i.e.. ESD or MBB).", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.bulk2_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Verify any manufacturer markings on the bag match specification.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.bulk3_text, theFont.Bold));
                        ret.Add(smpdf.CreateCell("Scan and verify that barcode scan and printed information on label are consistent with each other.", theFont.Normal));
                        ret.Add(smpdf.CreateCell(idea_detail.bulk4_text, theFont.Bold));
                        break;
                    }
            }

            return ret;
        }
        //Initial Inspection
        private List<PdfPCell> GetSection2cCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("2c: Initial Inspection (IDEA 10.2.1)"));


            ret.Add(smpdf.CreateCell("Verify the part number, manufacturer, and quantity match the purchase order and packing slip.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.initial1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Verify that there is an original factory label; insure there is not a label over label - Confirm MFG Logo is printed on label and spelled correctly & smudge free.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.initial2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Confirm the date code meets any restrictions specified on the purchase agreement.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.initial4_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Are the receiving parts (as defined by MPN or datasheet) RoHS Compliant?", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.initial5_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Does the Customer's PO, Quality Notes, or other requirements define RoHS compliance?", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.initial6_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Record any signs of damage to the product or packaging.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.initial7_text, theFont.Bold));
            return ret;
        }
        //Detail Inspection
        private List<PdfPCell> GetSection2dCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("2d: Detailed Inspection (Visual) (IDEA 10.3.1)", 3));

            ret.Add(smpdf.CreateCell("Verify the logo and markings match the manufacturer's specifications.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail1_result), theFont.Bold));

            ret.Add(smpdf.CreateCell("Confirm the markings are clear and do not appear to be re-marked or re-stamped.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail2_result), theFont.Bold));

            ret.Add(smpdf.CreateCell("Confirm the markings are consistent throughout the package type from part to part and of the top and bottom of the parts (placement, font type, color, and texture).", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail3_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail3_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Inspect laser marks for burn holes caused by after market laser equipment.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail4_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail4_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Inspect for Inconsistencies in package indents shape, size, and locations.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail5_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail5_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Confirm that there are no burn or blister marks, or evidence of exposure to excessive heat.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail6_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail6_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Ensure there are no colored dots or ink marks on the tops of components indicating previous testing or programming, unless allowed or required by the purchase order.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail7_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail7_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Look for flux or chemical residue and tool marks or heat sink markings indicating refurbished parts.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail8_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail8_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Confirm there are no cracks on the surface of the parts.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail9_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail9_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Verify the lead / pin count and formation/terminal layout or type of lead (DIP, SMB, GULL WING, etc.) match the datasheet.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail10_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail10_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Inspect for damaged leads (bent, scratched, broken, dented, missing, coplanarity, etc.) indicating the part has been salvaged or mishandled.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail11_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail11_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Ensure that leads are not oxidized, re-tinned with solder (re-balled for BGAs), show signs of corrosion, or contamination from foreign substances.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail12_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail12_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Look for leads that are too shiny for older date codes, or too dull for new date codes; the pins should be similar in gloss or shine, color, and texture.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail13_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail13_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Confirm there are no scratches on the inside and outside of leads; scratches under the BGA spheres, are typically a sign of re-balled parts.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail14_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail14_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Inspect BGAs, LGAs and any terminals, lugs, or connectors to ensure that the component has not been used, refurbished, mishandled, or contaminated.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail15_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail15_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Photograph markings front and back for records.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail16_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail16_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("If nonconforming, document and photograph nonconformance(s).", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.detail17_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.detail17_result), theFont.Bold));

            return ret;
        }
        //Solvents
        private List<PdfPCell> GetSection2eCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("2e: Detailed Inspection (Solvents) (IDEA 10.3.2)"));
            ret.Add(smpdf.CreateCell("Perform device marking test.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.solvent1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Perform device surface test.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.solvent2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell("Perform scrape test (as needed).", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.solvent3_text, theFont.Bold));
            return ret;
        }
        //Mechanical
        private List<PdfPCell> GetSection2fCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            ret.Add(GenerateSectionHeading("2f: Detailed Inspection (Mechanical) (IDEA 10.3.3)", 3));

            ret.Add(smpdf.CreateCell("Determine the min / max or acceptable tolerance range of each measurement being taken from the manufacturer datasheet.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.mech1_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.mech1_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Measure, verify and record the package dimensions.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.mech2_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.mech2_result), theFont.Bold));


            ret.Add(smpdf.CreateCell("Measure for thickness variation.", theFont.Normal));
            ret.Add(smpdf.CreateCell(idea_detail.mech3_text, theFont.Bold));
            ret.Add(smpdf.CreateCell(smpdf.GetResult(idea_detail.mech3_result), theFont.Bold));


            return ret;
        }
        //Additional
        private List<PdfPCell> GetSectionAdditionalCells()
        {
            List<PdfPCell> ret = new List<PdfPCell>();
            if (!string.IsNullOrEmpty(idea_detail.addtesting_text))
            {

                ret.Add(GenerateSectionHeading("Additional Testing"));
                ret.Add(smpdf.CreateCell("Details:", theFont.Normal));
                ret.Add(smpdf.CreateCell(idea_detail.addtesting_text, theFont.Bold));

            }
            return ret;
        }






    } //End IDEA PDF


    public class NonconPdf
    {
        int nonConID = 0;

        public NonconPdf(int _nonconID)
        {
            nonConID = _nonconID;
        }

        //static NONCON_RMADataContext ndc = new NONCON_RMADataContext();


        //Variables    
        //Too Confusing to make these global, easy for be accessing the wrong var, makes code less readable    

        NonCon_Payment ncPayment;
        List<insp_images> ncImagery = new List<insp_images>();
        sm_binaryEntities smb = new sm_binaryEntities();
        sm_nonconEntities ndc = new sm_nonconEntities();
        SM_PDF smpdf = new SM_PDF();
        SM_Tools tools = new SM_Tools();

        SensibleDAL.ef.NonCon_Head ncHead;
        SensibleDAL.ef.NonCon_Details ncDetail;
        SensibleDAL.ef.NonCon_Disposition ncDispo;

        byte[] bytes;
        HttpContext context = HttpContext.Current;
        Document document;
        public string fileName;
        public string documentTitle;

        //BackGroundColors
        BaseColor Gray = new BaseColor(224, 224, 224);
        BaseColor White = new BaseColor(255, 255, 255);
        SM_PDFFont theFont = SM_PDFFont.GetFont("Montserrat");


        public void CreatePDF()
        {
            LoadNonconData();
            LoadNonconImagery();



            Page page = new Page();
            try
            {
                fileName = "nonconformance_report" + nonConID;


                using (var MS = new MemoryStream())
                {
                    //Main Document
                    document = new Document(PageSize.LETTER, 30, 30, 90, 25);
                    PdfWriter writer = PdfWriter.GetInstance(document, MS);

                    //Header             
                    var header = new PDFHeader();
                    header.headerTable = GenerateNonconPageHeader();
                    writer.PageEvent = header;
                    //Footer
                    writer.PageEvent = new PDFFooter();
                    //The document must be "Opened" after being used in PdfWriter.GetInstance() otherwise there no writer associated and it does nothing.
                    //http://stackoverflow.com/questions/30672700/itextsharp-the-document-is-not-open-error-when-it-actually-is
                    document.Open();
                    smpdf.setPDFStyling();
                    List<PdfPTable> nonconTables = GenerateNoncoNPDFTables();
                    smpdf.generatePDF(MS, document, nonconTables, Rectangle.NO_BORDER);

                    //Finish Producing Document
                    document.Close();
                    //Add Page Numbers
                    bytes = smpdf.AddPageNumber(MS, theFont.Bold, 550f, 704f).ToArray();
                }
                //process bytes for client download
                smpdf.DownloadPDF(bytes, context, fileName);
            }
            catch (Exception ex)
            {
                page = context.Handler as Page;
                string s = ex.Message;
                tools.HandleResult("fail", ex.Message);
            }
        }

        private List<PdfPTable> GenerateNoncoNPDFTables()
        {
            List<PdfPTable> ret = new List<PdfPTable>();



            return ret;
        }

        private void LoadNonconData()
        {

            ncHead = (SensibleDAL.ef.NonCon_Head)ndc.NonCon_Head.Where(w => w.NonConID == nonConID).FirstOrDefault();
            ncDetail = (SensibleDAL.ef.NonCon_Details)ndc.NonCon_Details.Where(w => w.NonConID == nonConID).FirstOrDefault();
            ncDispo = (SensibleDAL.ef.NonCon_Disposition)ndc.NonCon_Disposition.Where(w => w.NonConID == nonConID).FirstOrDefault();
        }

        private void LoadNonconImagery()
        {
            ncImagery = smb.insp_images.Where(w => w.insp_type == SM_Enums.InspectionType.noncon.ToString() && w.insp_id == nonConID).ToList();
        }



        private PdfPTable GenerateNonconPageHeader()
        {

            //Main Header
            PdfPCell cell = smpdf.CreateCellImage(context.Server.MapPath("~/Images/pdf_logo_2018.png"), 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER, 25, 1);
            PdfPTable HeaderContainer = smpdf.CreateTable(1, 100);
            PdfPTable table = smpdf.CreateTable(4, 100);
            table.SpacingBefore = 15f;
            table.SpacingAfter = 5f;
            //Set column Widths
            table.SetWidths(new int[] { 25, 25, 35, 15 });
            table.AddCell(cell);//Logo
            cell = smpdf.CreateCell("Nonconformance ", theFont.H0Light, 1, null, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("INSPECTION REPORT", theFont.H0, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("F-PLACE-HOLDER Rev A", theFont.Bold, 1, null, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            HeaderContainer.AddCell(smpdf.CreateCell(table));


            //Sub-Header Details
            table = smpdf.CreateTable(8, 100);
            //Set column Widths
            table.SetWidths(new int[] { 12, 13, 12, 13, 12, 20, 10, 8 });
            cell = smpdf.CreateCell("Inspection ID: ", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell(nonConID.ToString(), theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("Date: ", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell(ncHead.NonConDate.Value.ToString("MM-dd-yyyy"), theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("Inspector: ", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell(ncHead.submittedBy, theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("Page:", theFont.Normal, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            cell = smpdf.CreateCell("", theFont.Bold, 1, null, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, Rectangle.NO_BORDER);
            table.AddCell(cell);
            HeaderContainer.AddCell(smpdf.CreateCell(table, Gray));

            return HeaderContainer;

        }


    } //End Noncon PDF

}//End SM Quality Logic

