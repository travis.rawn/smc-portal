﻿<%@ Page Title="Error - Unable to Process" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="ErrorPages_Oops" %>


<asp:Content ID="LeadContent" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">

    <div style="text-align: center; margin-bottom: 15px;">
        <h3>That was NOT supposed to happen. </h3>
        <%-- <h2>Honestly, this is a little awkward.</h2>--%>
        <h4>I *think* I see the problem.  Let me go take a look ...</h4>
        <%--Image--%>
        <div class="row"style="margin-top:15px;">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Error.png" CssClass="img img-responsive" />
            </div>
            <div class="col-sm-4"></div>
        </div>

        <h4>...  meanwhile, why don't you head
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/login.aspx"> here </asp:HyperLink>and try again?</h4>
    </div>


</asp:Content>

