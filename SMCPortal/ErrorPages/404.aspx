﻿<%@ Page Title="404 - Page Not Found" Language="C#" MasterPageFile="~/hs_flex.master" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="ErrorPages_Oops" %>

<asp:Content ID="LeadContent" ContentPlaceHolderID="LeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div style="text-align: center; margin-bottom: 15px;">
        <h3>404</h3>
        <h4>I&#39;m SURE that page was just here.&nbsp; Hang on, give me a sec to try to find it...</h4>

        <div class="row" style="margin-top: 15px;">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/404img.jpg" CssClass="img img-responsive" />
            </div>
            <div class="col-sm-4"></div>
        </div>
        <h4><em>... meanwhile, why don&#39;t you go <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/login.aspx">here.</asp:HyperLink></em></h4>
    </div>
</asp:Content>

