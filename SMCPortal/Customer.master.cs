﻿using SensibleDAL.ef;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using SensibleDAL;
using System.Web.UI.WebControls;


public partial class Customer : System.Web.UI.MasterPage
{


    SM_Tools tools = new SM_Tools();
    //SMCPortalDataContext PDC = new SMCPortalDataContext();
    Rep SensibleRep = new Rep();
    RzTools rzt = new RzTools();
    public sm_crypto smcrypto = new sm_crypto();

    protected void Page_Load(object sender, EventArgs e)
    {

        string sessionID = NetworkLogic.SetSessionID(HttpContext.Current);
        HideMenuItems();
        LoadCopyright();


        //IF problem, user should get stopped here





        if (SM_Security.HandleUserSecurityValidation())
        {
            LoadMenuItems();
            LoadUserData();
            string returnUrl = Tools.Strings.SanitizeInput(Request.QueryString["ReturnUrl"]);
            if (!string.IsNullOrEmpty(returnUrl))
                Response.Redirect(returnUrl, false);
        }
        else
        {

            //    if (HttpContext.Current.User.Identity.IsAuthenticated)
            //        //If the user was logged in, and something happened with their account (i.e. admin lock / disapprove), this is how we handle that. 
            //        SM_Security.Logout();
            //else
            footerCopyRight.Visible = false;
        }


        //Check for success in querystring.
        tools.LoadSuccessQueryString(Request);
        
    }

    private void LoadCopyright()
    {
        lblCopyright.Text = "© Sensible Micro Corporation " + DateTime.Today.Year.ToString();
    }

    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        SM_Security.Logout();
    }



    private void HideMenuItems()
    {
        LoadMenuItems(true);
    }


    protected void LoadMenuItems(bool hideAll = false)
    {
        if (hideAll == true)
        {
            ShowAll(false);
            return;
        }

        if (!Page.User.Identity.IsAuthenticated && !hideAll)
        {
            //mnuCustomer.Visible = false;
            mnuResources.Visible = false;
            //liCurrentConsignment.Visible = false;
            //liConsignmentHistory.Visible = false; 
            mnuInternal.Visible = false;
            mnuAccount.Visible = false;
            //string path = HttpContext.Current.Request.Url.AbsolutePath;           
        }
        else
        {
            if (!Page.User.IsInRole("admin"))
            {
                ShowAll(false);

                ShowMember(true);

                if (Page.User.IsInRole(SM_Enums.PortalRole.consign.ToString()))
                    ShowConsignment(true);
                //else
                //    ShowConsignment(false);


                if (Page.User.IsInRole(SM_Enums.PortalRole.sm_internal.ToString()))
                    ShowInternal(true);
                //else
                //    ShowInternal(false);


                if (Page.User.IsInRole(SM_Enums.PortalRole.sm_internal_executive.ToString()))
                    ShowInternalExec(true);
                //else
                //    ShowInternalExec(false);


                if (Page.User.IsInRole(SM_Enums.PortalRole.sm_warehouse.ToString()))
                    ShowWarehouse(true);
                //else
                //    ShowWarehouse(false);

                if (Page.User.IsInRole(SM_Enums.PortalRole.portal_admin.ToString()))
                    ShowPortalAdmin(true);
                //else
                //    ShowPortalAdmin(false);

            }
            else
            {
                ShowAll(true);
            }
            //if (SM_Global.DisplayProfile != null)
            //    lblDisplayCompany.Text = " [" + SM_Global.DisplayProfile.FirstName + " " + SM_Global.DisplayProfile.LastName + "]";

        }


    }

    private void ShowConsignment(bool show)
    {
        mnuConsignment.Visible = show;

    }
    private void ShowMember(bool show)
    {
        mnuResources.Visible = show;
        mnuOrders.Visible = show;
        mnuQcCenter.Visible = show;
        mnuAccount.Visible = show;
    }

    private void ShowInternal(bool show)
    {
        mnuInternal.Visible = show;
        lblDisplayCompany.Visible = show;

        mniSiliconExpertSearch.Visible = show;

        ShowReports(show);
        ShowTools(show);
        mnuScreenContent.Visible = show;
    }

    private void ShowTools(bool show)
    {
        mnuTools.Visible = show;
    }

    private void ShowInternalExec(bool show)
    {

    }
    private void ShowWarehouse(bool show)
    {
        mnuInventoryUpdater.Visible = show;
    }

    private void ShowPortalAdmin(bool show)
    {
        mnuAdmin.Visible = show;
        ShowConsignment(show);
        mnuOrders.Visible = show;
    }

    private void ShowReports(bool show)
    {
        mnuReports.Visible = show;
    }
    private void ShowAll(bool show)
    {
        ShowConsignment(show);
        ShowMember(show);
        ShowInternal(show);
        ShowInternalExec(show);
        ShowWarehouse(show);
        ShowPortalAdmin(show);
        ShowReports(show);
    }

    protected void LoadUserData()
    {
        if (!Page.IsPostBack)
        {
            //LoadRepPanel();
            //LoadScripts();
        }
    }
    //protected void lbResetDisplayProfile_Click(object sender, EventArgs e)
    //{
    //    SM_Global.DisplayProfile = Profile;
    //    SM_Global.DisplayCompany = rzt.GetCompanyByID(Profile.CompanyID);
    //    LoadUserData();
    //}



    protected void lbPartSearch_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtPartSearch.Text))
        {
            tools.HandleResult("fail", "Please type a part number to search for.");
            return;
        }
        else
        {
            //if (!Request.IsAuthenticated)
            Response.Redirect("/public/search/part_search.aspx?pn=" + txtPartSearch.Text.Trim().ToUpper(), false);
            //else
            //    Response.Redirect("~/secure_members/tools/preferred_part_search.aspx?p=" + txtPartSearch.Text.Trim().ToUpper());
        }

    }

}


