﻿using SensibleDAL;
using SensibleDAL.dbml;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Account_Login : Page
{

    string LoginUserName;
    Label sitealert;
    FormsAuthenticationTicket ticket;
    SM_Tools tools = new SM_Tools();
    bool isLocal = false; //Check for local Dev Environment

    protected void Page_Load(object sender, EventArgs e)
    {
        ////Hide the title to avoid padding issues       
        //hs_flex master = (hs_flex)this.Master;
        //master.HideTitleDiv();

        ((hs_flex)this.Master).ToggleTitleDiv(false);
        HandleQueryStringTasks();

        if (!Page.IsPostBack)
        {
            //switch between userName and Email for the placeholder
            SetLoginTextBoxPlaceholder();
        }

        //Identify local clients
        isLocal = HttpContext.Current.Request.IsLocal;

        if (CheckCookie()) //If Auth Cookie not present, Redirect to Home
        {
            Response.Redirect("~/secure_members/Default.aspx" + Request.Url.Query ?? "", false);
        }
        //If ISAuthenticated, Redirect to Home
        if (HttpContext.Current.Request.IsAuthenticated)
        {
            Response.Redirect("~/secure_members/Default.aspx" + Request.Url.Query ?? "", false);
        }

    }

    private void HandleQueryStringTasks()
    {
        string strIsAuthorized = Tools.Strings.SanitizeInput(Request.QueryString["authorized"]);
        if (!string.IsNullOrEmpty(strIsAuthorized))
        {
            if (strIsAuthorized == "false")
                ShowNotAuthorizedMessage();
        }
    }

    private void ShowNotAuthorizedMessage()
    {
        if (SM_Global.CurrentUser.IsLockedOut)
            HandleError("You account currently locked out.  Please contact your Sales Representative for resolution.");
        else if (!SM_Global.CurrentUser.IsApproved)
            HandleError("Your account is not currently approved for access.  Please contact your sales representative for resolution.");
    }

    private void SetLoginTextBoxPlaceholder()
    {
        //string ip = tools.GetUser_IP();
        string ip = NetworkLogic.GetVisitorIpAddress();
        if (ip == "::1" || ip == "72.91.227.24" || ip == "72.17.76.114" || ip.Contains("192.168.1.") || ip.Contains("10.2.0.") || ip.Contains("10.4.0."))
            txtUserName.Attributes.Add("placeholder", "User Name");
        else
            txtUserName.Attributes.Add("placeholder", "Email Address");

        txtPassword.Attributes.Add("placeholder", "Password");
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        DoLogin();
    }

    private void DoLogin()
    {
        try
        {
            //Validate the fields:
            if (!ValidateFields())
                return;

            //Get User and Passwrod Variables
            LoginUserName = txtUserName.Text.Trim();

            //Confirm Valid User Account By User Name, if invalid, don't bother checking credentials.
            //Currently need to use a local variable, since SM_Global doesn't have an overload to pass the username, breaks the Get.
            ////Currently need to use a local variable, since SM_Global doesn't have an overload to pass the username, breaks the Get.
            MembershipUser CurrentUser = Membership.GetUser(LoginUserName);
            if (CurrentUser == null)
            {
                //Invalid UserName
                HandleError("Username or password is incorrect.");
                return;
            }
            else
                SM_Global.CurrentUser = CurrentUser;
            //Confirm valid account, if I authetnicate first, it will return false if any of these conditions are true, which is misleading.  Therefor checking these before trying to authenticate.
            if (!ConfirmValidAccountStatus())
            {
                ShowNotAuthorizedMessage();
                return;
            }

            //Authenticate the user   
            bool IsAuthenticated = DoAuthentication();
            ProfileCommon currentProfile = LoadProfile(IsAuthenticated);

            //unauthenticated sessiond are valid in a temp password scendario.  Check that.
            if (currentProfile.HasTemporaryPassword)
            {
                HandleTempPassword(currentProfile);
                return;
            }

            //At this point we need to be authenticated, temp password would have been handled already
            if (!IsAuthenticated)
            {
                //Username and password Mismatch
                HandleError("Username or password is incorrect.");
                return;
            }


            HandleSuccessfulLogin();//Do the Login tasks     
        }
        catch (Exception ex)
        {
            HandleError(ex.Message + Environment.NewLine + "Inner Exception: " + ex.InnerException);
            return;
        }
    }

    private ProfileCommon LoadProfile(bool IsAuthenticated)
    {
        ProfileCommon ret = null;
        if (!IsAuthenticated)
        {
            // If the password failed, it's either wrong, or the user doesn't know it, possible due to forced password change, check for that.
            // To do that we must retrieve the profile manually, since tis user is not logged in.
            ret = (ProfileCommon)ProfileCommon.Create(SM_Global.CurrentUser.UserName);
        }
        else
        {
            ret = Profile;
        }
        return ret;
    }

    private bool ConfirmValidAccountStatus()
    {
        if (SM_Global.CurrentUser.IsLockedOut || !SM_Global.CurrentUser.IsApproved)
            return false;
        return true;

    }

    private void HandleSuccessfulLogin()
    {
        //SM_Global.CurrentUser = CurrentUser;
        HandleAgedLogin();//more than 30 days since last successful locin
        HandleRememberMe();
        HandleLoginEmailAlert();
        HandleReturnUrl();
        HubspotLogic.HubspotPortalLink hl = HubspotLogic.LinkPortalUserToHubspotContact(SM_Global.CurrentUser);
        //SetGlobalCompany();
    }

    //private void SetGlobalCompany()
    //{
    //    SM_Global.CurrentCompany = null;
    //    using (RzDataContext rdc = new RzDataContext())
    //    {
    //        string companyID = Profile.CompanyID;
    //        if (string.IsNullOrEmpty(companyID))
    //            return;

    //        company c = rdc.companies.Where(w => w.unique_id == companyID).FirstOrDefault();
    //        SM_Global.CurrentCompany = c;

    //    }

    //}

    private void HandleAgedLogin()
    {
        if (SM_Security.IsAgendUserLogin(SM_Global.CurrentUser))//Not setting specific page, Master page logic should direct the user.  Just need to tell them why they are being redirected there when they log in.       
            tools.JS_Modal_Redirect("It has been more than 30 days since your last successful login.  For your security, your password has been reset and emailed to you.  Please refer to this email to resume access.", "/");


    }

    private void HandleTempPassword(ProfileCommon currentProfile)
    {
        if (SM_Security.HasTempPassword(SM_Global.CurrentUser))
        {
            string msg = "Your password has recently been reset, and you should have received an email with a temporary password.  If you did not receive the email, please use the 'Forgot Password' link below to send another temporary password email.";
            if (!String.IsNullOrEmpty(currentProfile.AccessStatusMessage))
                msg = currentProfile.AccessStatusMessage;
            HandleError(msg);
        }
        else
            //Invalid Username / password combo
            HandleError("Username or password is incorrect.");

    }

    private bool DoAuthentication()
    {


        //Confirm Proper credentials        
        string passWord = txtPassword.Text.Trim();
        if (Membership.ValidateUser(LoginUserName, passWord))
        {

            FormsAuthentication.SetAuthCookie(LoginUserName, chkRememberMe.Checked);

            return true;
        }
        return false;

    }

    private void HandleReturnUrl()
    {
        string returnUrl = Tools.Strings.SanitizeInput(Request.QueryString["ReturnUrl"]);
        if (!string.IsNullOrEmpty(returnUrl))
            Response.Redirect(returnUrl, false);
        else
            Response.Redirect("~/secure_members/Default.aspx", false);
    }

    private void HandleRememberMe()
    {
        if (chkRememberMe.Checked)
            FormsAuthentication.SetAuthCookie(SM_Global.CurrentUser.UserName, true);
        else
            FormsAuthentication.SetAuthCookie(SM_Global.CurrentUser.UserName, false);
    }

    private void HandleLoginEmailAlert()
    {
        if (!SM_Global.CurrentUser.Email.Contains("sensiblemicro.com") && !SM_Global.CurrentUser.Email.Contains("shmootill.com") && !isLocal)
        {
            try
            {
                //if (!isLocal)
                //    SetHubSpotCookie();
                //tools.SendLoginEmailAlert(CurrentUser);
                SM_Security.SendLoginEmailAlert(SM_Global.CurrentUser);
            }
            catch (Exception ex)
            {//Handling this in a nested try catch because I want the code to continue (i.e. to allow the user in) but not show them the error, yet I need to know the error, thus the email.
                tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Login email alert not sent.", "The error was: " + ex.Message + "<br />" + ex.InnerException + "<br />" + NetworkLogic.GetVisitorIpAddress(), null, null, false);
            }
        }
    }

    protected void HandleError(string message)
    {
        lblError.InnerText = message;
        //lblError.Visible = true;
        //divError.Visible = true;

    }
    protected bool CheckCookie()
    {
        //KT NOTE: Persistent Cookie = cookie will remain after browser close
        // Non-persistent is destroyed when all instances of browser is closed (not just say 1 chrome browser that you were working in)
        HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
        if (authCookie != null)
            ticket = FormsAuthentication.Decrypt(authCookie.Value);
        else
            return false;
        if (ticket != null)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    protected bool ValidateFields()
    {
        if (string.IsNullOrEmpty(txtUserName.Text))
        {
            HandleError("Please provide a user name.");
            return false;
        }
        if (string.IsNullOrEmpty(txtPassword.Text))
        {
            HandleError("Please provide a password.");
            return false;
        }
        return true;
    }

    protected void ForgotPassword_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/Account/PasswordReset.aspx");
        Response.Redirect("~/Account/password_reset.aspx", false);
    }



    //protected void SetHubSpotCookie()
    //{
    //    string strHubSpotUTK = Request.Cookies["hubspotutk"].Value;
    //    string strIpAddress = HttpContext.Current.Request.UserHostAddress;

    //    // Tracking Code Variables 
    //    string strPageTitle = this.Title;
    //    string strPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

    //    //Build dictionary of field names / values(must match the HS field names)
    //    Dictionary<string, string> dictFormValues = new Dictionary<string, string>();
    //    dictFormValues.Add("email", user.Email);

    //    //Form Variables (from the HubSpot Form Edit Screen)
    //    int intPortalID = 1878634; //place your portal ID here
    //                               //string strFormGUID = "3220ecd1-85fa-48a5-a5e9-6a33e51dcf76"; //place your form guid here
    //                               //2019 New Form
    //                               //https://share.hsforms.com/16LIB5716RNO6qPRc7gW4tQ149ka
    //    string strFormGUID = "e8b201e7-bd7a-44d3-baa8-f45cee05b8b5";
    //    string strError = "Oh snap, that didn't work!";
    //    bool blnRet = Post_To_HubSpot_FormsAPI(intPortalID, strFormGUID, dictFormValues, strHubSpotUTK, strIpAddress, strPageTitle, strPageURL, ref strError);
    //    HttpCookie HubSpotCookie = Request.Cookies["hubspotutk"];


    //    if (HubSpotCookie != null)
    //        Response.Cookies.Add(HubSpotCookie);
    //}

    //public bool Post_To_HubSpot_FormsAPI(int intPortalID, string strFormGUID, Dictionary<string, string> dictFormValues, string strHubSpotUTK, string strIpAddress, string strPageTitle, string strPageURL, ref string strMessage)
    //{
    //    // Build Endpoint URL
    //    string strEndpointURL = string.Format("https://forms.hubspot.com/uploads/form/v2/{0}/{1}", intPortalID, strFormGUID);
    //    // Setup HS Context Object
    //    Dictionary<string, string> hsContext = new Dictionary<string, string>();
    //    hsContext.Add("hutk", strHubSpotUTK);
    //    hsContext.Add("ipAddress", strIpAddress);
    //    hsContext.Add("pageUrl", strPageURL);
    //    hsContext.Add("pageName", strPageTitle);

    //    // Serialize HS Context to JSON (string)
    //    System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
    //    string strHubSpotContextJSON = json.Serialize(hsContext);

    //    // Create string with post data
    //    string strPostData = "";

    //    // Add dictionary values
    //    foreach (var d in dictFormValues)
    //    {
    //        strPostData += d.Key + "=" + Server.UrlEncode(d.Value) + "&";
    //    }

    //    // Append HS Context JSON
    //    strPostData += "hs_context=" + Server.UrlEncode(strHubSpotContextJSON);

    //    // Create web request object
    //    System.Net.HttpWebRequest r = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strEndpointURL);

    //    // Set headers for POST
    //    r.Method = "POST";
    //    r.ContentType = "application/x-www-form-urlencoded";
    //    r.ContentLength = strPostData.Length;
    //    r.KeepAlive = false;

    //    // POST data to endpoint
    //    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(r.GetRequestStream()))
    //    {
    //        try
    //        {
    //            sw.Write(strPostData);
    //        }
    //        catch (Exception ex)
    //        {
    //            // POST Request Failed
    //            strMessage = ex.Message;
    //            return false;
    //        }
    //    }
    //    return true; //POST Succeeded
    //}
}