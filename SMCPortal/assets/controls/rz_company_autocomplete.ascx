﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rz_company_autocomplete.ascx.cs" Inherits="assets_controls_rz_company_autocomplete" %>
<script>

    $(document).ready(function () {
        //Listener for blank companyname
        $("#<% =txtCompanyName.ClientID%>").on("keyup", function () {
            if ($("#<% =txtCompanyName.ClientID%>").val() == null || $("#<% =txtCompanyName.ClientID%>").val() == '')
                $("#<% =hfCompanyName.ClientID%>").val('');
        });

        //Set the Autocomplete Ajax settings
        $("#<% =txtCompanyName.ClientID%>").on("autocompleteselect", function (event, ui) { });
        $("#<% =txtCompanyName.ClientID%>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/assets/webmethods/sm_webmethods.aspx/LoadCompanies",
                    data: "{input:'" + request.term + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var parsed = JSON.parse(data.d);
                        response(parsed);
                    },

                    failure: function (response) {
                        //alert(response.d);                     
                    },
                    error: function (response) {
                        //alert(response.d);                       
                    }
                });
            },
            focus: function (event, ui) {
                $("#<% =txtCompanyName.ClientID%>").val(ui.item.label);
                return false;
            },
            select: function (event, ui) {

                if (ui.item) {
                    event.preventDefault();//Prevents default behavior of showing value instead of label on select 
                    //Set Hidden Fields
                    $("#<% =hfCompanyID.ClientID%>").val(ui.item.value);
                    $("#<% =hfCompanyName.ClientID%>").val(ui.item.label);
                    //Check if control is set to include contacts
                    if ($("#<% =hfShowContacts.ClientID%>").val() === "true") {
                        //Populate the contactd SSL via triggering hidden click event.
                        //ShowUpdateProgress();
                        $("#<% =btnFillDdlContact.ClientID%>").trigger('click');
                    }
                    //Set Textbox
                    $("#<% =txtCompanyName.ClientID%>").val(ui.item.label);//Manually filling the textbox with the selection since not doing default                    
                }
                else {
                    //Clear Textboxes
                    $("#<% =txtCompanyName.ClientID%>").val(null);
                    $("#<% =hfCompanyName.ClientID%>").val(null);
                    $("#<% =hfCompanyID.ClientID%>").val(null);
                }
            },

        });
    })

    //Set Contact ddl Onchange event
    function SetContactFields() {
        $("#<% =hfContactID.ClientID%> ").val($("#<% =ddlContactName.ClientID%>  option:selected").val());
        $("#<% =hfContactName.ClientID%>").val($("#<% =ddlContactName.ClientID%>  option:selected").text());
    }
</script>

<style>
    input.ui-autocomplete-loading {
        background: white url("/Images/autocomplete_loader.gif") right center no-repeat;
    }

    .ui-widget-content {
        z-index: 15;
    }

    .ui-menu-item {
        background-color: ghostwhite;
        max-width: 300px;
        border: solid;
        border-width: 1px;
        border-color: darkgray;
    }

    ul {
        padding: 0;
        z-index: 10;
        list-style-type: none;
    }
</style>

<asp:Panel ID="pnlContactSearch" runat="server" DefaultButton="lbSelect">
    <asp:Button ID="btnFillDdlContact" runat="server" OnClick="btnFillDdlContact_Click" Style="display: none;" />
    <div class="input-group">
        <input type="text" class="form-control ui-autocomplete-input" placeholder="Type Company Name" runat="server" id="txtCompanyName">
        <span class="input-group-btn">
            <asp:LinkButton ID="lbSelect" runat="server" CssClass="btn btn-default" OnClick="OnCompanySelected" Text="Select" OnClientClick="ShowUpdateProgress();"></asp:LinkButton>            
        </span>
    </div>
</asp:Panel>
<!-- /input-group -->

<asp:DropDownList ID="ddlContactName" runat="server" CssClass="form-control" Style="display: none;" onchange="SetContactFields();">
    <asp:ListItem Text="--Choose--" Value="choose"></asp:ListItem>
</asp:DropDownList>
<asp:HiddenField ID="hfShowContacts" runat="server" />
<asp:HiddenField ID="hfCompanyName" runat="server" />
<asp:HiddenField ID="hfCompanyID" runat="server" />
<asp:HiddenField ID="hfContactName" runat="server" />
<asp:HiddenField ID="hfContactID" runat="server" />




