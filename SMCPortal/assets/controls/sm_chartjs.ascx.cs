﻿using System;

public partial class assets_controls_sm_chartjs : System.Web.UI.UserControl
{
    SM_Charts smr = new SM_Charts();
    //private double chartHeight = 90;
    private string controlPrefix;
    private string canvasID;
    public bool EnableToggle = false;
    //public SM_Charts.ChartJS theChart = null;    

    //This is only needed to set teh HTML elements.  Since all charts on a page need to be in the same window.onload event,
    //we don't want to render the JS per control, rather at the master page.  We just need predictable canvasIDs.
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (theChart != null)
        //    LoadChart();
    }

    public SM_Charts.ChartJS LoadChart(SM_Charts.ChartJS theChart)
    {

        canvasID = theChart.canvasID;
        controlPrefix = theChart.controlPrefix;

        if (string.IsNullOrEmpty(theChart.titleText))
            pnlChartTitle.Visible = false;
        else
        {
            lblChartTitle.Text = theChart.titleText;
            lblInformation.Text = theChart.informationalText;
        }
       



        if (!string.IsNullOrEmpty(theChart.informationalText))
            divChartInfo.Visible = true;
        else divChartInfo.Visible = false;

        System.Web.UI.HtmlControls.HtmlGenericControl canvas = generateCanvas();
        if (theChart.canvasHeight > 0)
            canvas.Attributes.Add("height", theChart.canvasHeight.ToString());
        else
            canvas.Attributes.Add("height", "120");//default
        if (EnableToggle)
            SetToggleButton();

        return theChart;
    }

    private void SetToggleButton()
    {
        btnToggle.Visible = true;
        pnlchartSection.ID = pnlchartSection.ID.Replace(pnlchartSection.ID, "pnl" + canvasID); // Need to differentiate generic sections for multiple charts on same page).        
        btnToggle.Attributes.Add("onclick", "toggleElement('" + controlPrefix + "_" + pnlchartSection.ID + "'); return false;");

    }

    private System.Web.UI.HtmlControls.HtmlGenericControl generateCanvas()
    {
        pnlChartCanvas.Controls.Clear();
        System.Web.UI.HtmlControls.HtmlGenericControl canvas = new System.Web.UI.HtmlControls.HtmlGenericControl("canvas");
        canvas.ID = canvasID;

        pnlChartCanvas.Controls.Add(canvas);
        return canvas;

    }
}