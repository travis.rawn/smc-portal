﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_controls_bs_collapsepanel : System.Web.UI.UserControl
{
    public bool AutoShowPanel = false;
    public bool Checked = false;
    public string CheckBoxText = "";
    public string PanelText = ""; //"Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.";
    public bool EnableCheckbox = true;

    public Panel BodyPanel
    {
        get { return pnlBody; }
    }
    public DropDownList PanelDropdown
    {
        get { return ddlPanelDropdown; }
    }
  

    //Paradigm.  Use code behind to set Hidden Field variables, reference these hiddenb fields for JS ui.



    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        lblCheckboxText.InnerText = CheckBoxText;
        lblPanelText.Text = PanelText;
        cbx.Checked = Checked;      
           
        //cbx.Enabled = EnableCheckbox;
        
    }

    protected void ddlPanelDropdown_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}