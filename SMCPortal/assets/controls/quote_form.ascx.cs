﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Linq;
using System.Web.UI;
using System.Net.Mail;
using SensibleDAL;
using SensibleDAL.dbml;
using System.Web.UI.WebControls;
using HubspotApis;
using System.Web;


public partial class quote_form : UserControl
{
    SM_Tools tools = new SM_Tools();
    RzTools rzt = new RzTools();

    public MembershipUser user;
    //string IPaddr;
    company RzCompany = null;
    companycontact RzContact = null;
    RzLogic.RzDomainMatchResults dmr;
    HubspotLogic.HubspotMatchResults hmr;
    public string CustomerEmail;
    List<string> agentEmails = new List<string>();
    HubspotApi.Deal TheHubspotDeal = null;
    HubspotApi.Owner hsOwner = null;
    dealheader TheBatch = null;

    //String that holds HTML formatted values for email etc.
    string FormInputTextHTML;


    string PartNumber = "";
    string FirstName = "";
    string LastName = "";
    string Email = "";
    string Qty = "";
    string ContactPhone = "";
    string CompanyName = "";
    string TargetPrice = "";
    string Notes = "";
    string VisitorIpAddress = "";
    string utm_campaign = "";
    string utm_source = "";
    DateTime TimeStamp;


    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsDevUser())
            lbTestQuoteProcess.Visible = true;
         get_utm_tracking();
        AutofillEmail();

    }




    public void SetPartNumber(string partNumber)
    {
        string val = Tools.Strings.SanitizeInput(partNumber).Trim().ToUpper();
        if (!string.IsNullOrEmpty(val))
        {
            hfPartNumber.Value = val;
            partNumber = val;
        }



    }

    private void AutofillEmail()
    {
        if (!Page.IsPostBack)
        {
            string email = CustomerEmail;
            if (!string.IsNullOrEmpty(email))
                if (tools.ValidateEmailAddress(email))
                    txtCustEmail.Text = email;

        }

    }

    private void DoQuoteTasks()
    {


        //Set Variables
        CustomerEmail = Tools.Strings.SanitizeInput(hfCustEmail.Value.ToLower().Trim());
        if (string.IsNullOrEmpty(CustomerEmail))
            throw new Exception("Invalid customer email.");

        if (!Tools.Email.IsEmailAddress(CustomerEmail))
        {
            string invalidText = "";
            invalidText += "IP: " + NetworkLogic.GetVisitorIpAddress();
            invalidText += "Name: " + FirstName + " " + LastName + "<br />";
            invalidText += "Email Field Content: '" + CustomerEmail + "'" + "<br />";
            invalidText += "Part Number: " + PartNumber + "<br />";
            invalidText += "Phone: " + ContactPhone + "<br />";
            invalidText += "Notes: " + Notes + "<br />";
            invalidText += "Company Name: " + Notes + "<br />";
            tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Invalid Email Address in Quote Form (Server Side)", invalidText);
            throw new Exception(CustomerEmail + "  is not a valid email address");
        }

        //Hubspot
        if (!IsDevUser())
            DoHubspotQuoteTasks();



        //Rz Batch, etc
        if (!IsDevUser())
            DoRzQuoteTasks();



        //SendEmail  
        //if (!IsDevUser())
            SendQuoteEmail();

        //Set the labels for the success div
        //SetSuccessLabels();

    }




    public string SetSearchBatchID(string search_batch_guid)
    {
        if (string.IsNullOrEmpty(search_batch_guid))
            throw new Exception("Invalid Search Batch ID");
        hfQuoteBatchGuid.Value = search_batch_guid;
        return search_batch_guid;
    }

    public string GetSearchBatchID()
    {
        return hfQuoteBatchGuid.Value;
    }

    protected void lbSendQuote_Click(object sender, EventArgs e)
    {
        try
        {

            GatherFormVariables();



            if (string.IsNullOrEmpty(PartNumber))
                return;
            string invalidInput = "";
            if (!ValidateInputFields(out invalidInput))
            {
                string message = "Illegal input detected('" + invalidInput + "')";
                message += FormInputTextHTML;
                tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, "Illegal input detected on Quote Form.", message + "<br /> Form Values:<br />" + FormInputTextHTML);
                throw new Exception(message);

            }

            //Confirm Email Domain
            string domain = Email.Split('@')[1];
            if (SM_Security.disallowedEmailDomains.Select(s => s.ToLower()).Contains(domain))
            {
                string message = "Please submit your RFQ using your company email address. ";
                //Public facing message, no email.
                tools.HandleResult("fail", message, 10000, false);
                //Private Log for email                
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Warning, Email + ":  " + "was denied RFQ due to a public email address." + message + FormInputTextHTML, true);
                return;
            }


            DoQuoteTasks();

            //Show the Success div and hide the form
            Response.Redirect(@"https://info.sensiblemicro.com/thank-you-quote?pn=testpart&qty=1234&email=someemail@test.com", false);
            //pnlQuoteForm.Style.Add("display", "none");
            //pnlQuoteSuccess.Style.Add("display", "block");

        }
        catch (Exception ex)
        {
            string detailMessage = "Exception: " + ex.Message + "<br />";
            if (ex.InnerException != null)
                if (!string.IsNullOrEmpty(ex.InnerException.Message))
                    detailMessage += "Inner Exception: " + ex.InnerException.Message + "<br /><br />";

            tools.HandleResult("fail", ex.Message + "<br /><br />" + detailMessage);
            tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.Systems, "Portal Quote Exception", FormInputTextHTML + detailMessage);
        }
    }



    private string GetHtmlQuoteDetails()
    {
        string message = "";
        message += "<br />";
        message += "IP: " + VisitorIpAddress + "<br />";
        message += "Part: " + PartNumber + "<br />";
        message += "Name: " + FirstName + " " + LastName + "<br />";
        message += "Email: " + Email + "<br />";
        message += "Qty: " + Qty + "<br />";
        message += "Phone: " + ContactPhone + "<br />";
        message += "Company: " + CompanyName + "<br />";
        message += "TargetPrice: " + TargetPrice + "<br />";
        message += "Date: " + TimeStamp + "<br />";
        message += "<br />";
        return message;
    }

    private bool IsDevUser()
    {
        MembershipUser u = Membership.GetUser();
        if (u == null)
            return false;
        if (u.UserName == "kevint" || u.Email.ToLower().Contains("ktill@sensiblelmicro.com"))
            return true;
        if (!Page.User.IsInRole(SM_Enums.PortalRole.portal_admin.ToString()))
            return true;
        return false;
    }

    private bool ValidateInputFields(out string invalidString)
    {
        invalidString = "";

        if (!validatePartNumber())
        {
            invalidString = "'" + PartNumber + "' is not a valid part number.";
            //throw new Exception("'" + PartNumber + "' is not a valid part number.");
            return false;
        }

        //Confirm valid email for quote form.
        if (!validateEmail())
        {
            invalidString = Email + " is not a valid email address.";
            //throw new Exception(Email + " is not a valid email address.");
            return false;
        }

        //Valid QTY
        if (!validateQty())
        {
            invalidString = Qty + " is not a valid quantity.";
            //throw new Exception(Qty + " is not a valid quantity.");
        }

        //Valid Phone
        if (!validatePhone())
        {
            invalidString = Qty + " is not a valid quantity.";
            //throw new Exception(Qty + " is not a valid quantity.");
        }

        //Valid Company
        if (!validateCompany())
        {
            invalidString = Qty + " is not a valid quantity.";
            //throw new Exception(Qty + " is not a valid quantity.");
        }




        //Check for illegal or dangerous input
        List<string> inputList = new List<string>() { FirstName, LastName, Email, Qty, ContactPhone, TargetPrice, Notes };
        foreach (string s in inputList)
        {
            if (Tools.Strings.IsIllegalUserInput(s))
            {
                invalidString = s;
                return false;
            }
        }
        return true;
    }

    private bool validateCompany()
    {
        bool ret = false;
        if (!string.IsNullOrEmpty(CompanyName))
            ret = true;
        return ret;
    }

    private bool validatePhone()
    {
        bool ret = false;
        if (!string.IsNullOrEmpty(ContactPhone))
            ret = true;
        //Phone number must be at least 7 digits.
        if (ContactPhone.Trim().Length < 7)
            ret = false;
        return ret;
    }

    private bool validatePartNumber()
    {
        if (string.IsNullOrEmpty(PartNumber))
            return false;
        if (PartNumber.Length < 3)
            return false;
        return true;
    }

    private bool validateQty()
    {
        bool ret;
        int i;
        ret = int.TryParse(Qty, out i);
        return ret;
    }

    private bool validateEmail()
    {
        bool ret = true;
        //Must NOT be empty
        ret = !string.IsNullOrEmpty(Email);
        //Must be a valid email address
        ret = Tools.Email.IsEmailAddress(Email);
        //Must be greater than 3 digits
        ret = Email.Length > 3;
        return ret;
    }

    private void GatherFormVariables()
    {


        //Gather up all critical quote variables
        PartNumber = Tools.Strings.SanitizeInput(hfPartNumber.Value ?? "No Part Detected");
        FirstName = Tools.Strings.SanitizeInput(hfCustFirstName.Value ?? "");
        //FirstName = Tools.Strings.SanitizeInput(txtCustFirstName.Text ?? "");
        LastName = Tools.Strings.SanitizeInput(hfCustLastName.Value ?? "");
        //LastName = Tools.Strings.SanitizeInput(txtCustLastName.Text ?? "");
        Email = Tools.Strings.SanitizeInput(hfCustEmail.Value ?? "");
        //Email = Tools.Strings.SanitizeInput(txtCustEmail.Text ?? "");
        Qty = Tools.Strings.SanitizeInput(hfQty.Value ?? "");
        //Qty = Tools.Strings.SanitizeInput(txtTargetQTY.Text ?? "");
        ContactPhone = Tools.Strings.SanitizeInput(hfPhone.Value ?? "");
        //ContactPhone = Tools.Strings.SanitizeInput(txtPhone.Text ?? "");
        CompanyName = Tools.Strings.SanitizeInput(hfCompanyName.Value ?? "");
        TargetPrice = Tools.Strings.SanitizeInput(hfPrice.Value ?? "");
        //TargetPrice = Tools.Strings.SanitizeInput(txtTargetPrice.Text ?? "");
        Notes = Tools.Strings.SanitizeInput(hfNotes.Value ?? "");
        //Notes = Tools.Strings.SanitizeInput(txtBatchNotes.Text ?? "");
        VisitorIpAddress = NetworkLogic.GetVisitorIpAddress() ?? "";
        TimeStamp = DateTime.Now;
        //Quote Modal Label loses it's value on Postback, unlike TextBoxes.  Set it now.
        lblQuotePartNumber.Text = hfPartNumber.Value.Trim().ToUpper();
        //Build the HTML String for emails, etc.
        BuildHtmlString();
    }


    private void SetSuccessLabels()
    {
        lblSuccessPN.Text = PartNumber;
        lblSuccessQty.Text = Qty;
        lblSuccessPrice.Text = TargetPrice;
        lblSuccessNotes.Text = Notes;
    }

    private void BuildHtmlString()
    {
        //Cobmine these in case of email alert.
        FormInputTextHTML += "<p style =\"font-size:16px; font-weight:bold;\">Quote Information:</p>";
        FormInputTextHTML += "<b>Part Number:</b> " + PartNumber;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>First Name:</b> " + FirstName;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Last Name:</b> " + LastName;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Email:</b> " + Email;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>QTY:</b> " + Qty;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Phone:</b> " + ContactPhone;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Company:</b> " + CompanyName;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Price:</b> " + TargetPrice;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Notes:</b> " + Notes;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>IP:</b> " + VisitorIpAddress;
        FormInputTextHTML += "<br />";
        FormInputTextHTML += "<b>Timestamp:</b> " + TimeStamp;
        FormInputTextHTML += "<br />";
    }

    private void DoRzQuoteTasks(bool makeBatch = true)
    {
        using (RzDataContext rdc = new RzDataContext())
        {

            string[] firstLastPhone = { FirstName ?? "", LastName ?? "", ContactPhone ?? "" };
            RzLogic.MatchVisitorToRzByDomain(rdc, CustomerEmail, true, firstLastPhone, out dmr);

            if (makeBatch)
                TheBatch = rzt.CreateRzBatch(dmr.rzCompany, dmr.rzContact, PartNumber, Qty, Notes, TargetPrice, TheHubspotDeal);
            //More than 1 company found or company is Undentified
            if (dmr.rzCompanyList.Count != 1 || dmr.rzContactList.Count != 1)
                SendRzContactAssociationAlert(rdc, dmr);
        }

        //Log to Database
        LogQuoteToDataBase();
    }







    private void SendRzContactAssociationAlert(RzDataContext rdc, RzLogic.RzDomainMatchResults dmr)
    {
        string messgeSubject = "Rz Association Required: " + CustomerEmail;
        string[] ccList = new string[] { "ktill@sensiblemicro.com", SystemLogic.Email.EmailGroupAddress.PortalAlert };
        if (TheHubspotDeal != null)
            messgeSubject += " HubID: " + TheHubspotDeal.dealId;
        string messageBody = "Problem associating quote request with Rz objects: " + CustomerEmail + "<br /><br />";
        //Get list of unique companies to include in email to make things easier.
        //List<string> uniqueCompanyIds = dmr.rzContactList.Select(s => s.base_company_uid).ToList();
        //List<company> companyList = rdc.companies.Where(w => uniqueCompanyIds.Contains(w.unique_id)).ToList();
        //int i = 1;

        messageBody += dmr.matchResultLog;


        messageBody += "<br /><br />  Please create and or associate this quote with the correct company in Rz.  This quote can be found in batch# " + TheBatch.dealheader_name + " .";
        tools.SendMail(SystemLogic.Email.EmailGroupAddress.PortalAlert, SystemLogic.Email.EmailGroup.PortalAlerts, messgeSubject, messageBody, ccList);
    }





    private void DoHubspotQuoteTasks()
    {
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Starting Hubspot quote tasks");

        if (string.IsNullOrEmpty(CustomerEmail))
            return;


        //Contact Matching
        hmr = new HubspotLogic.HubspotMatchResults();
        hmr.matchResultLog = "";
        hmr.hubspotContactCreated = false;


        //With Hubspot Match, should be single 1-1 match between email and contact, and then 1 or less matches to companies.   

        //Validate the input string for security
        //Note that I can't use dmr.customer email to set values inside this dmrbecuase it's part of an out parameter
        //Exampe, can't do dmr.rzContactList = rdc.companycontacts.Where(w => w.primaryemailaddress == dmr.customerEmail).ToList();
        //That's why I don't reuse dmr.customerEmail below.
        if (!Tools.Email.IsEmailAddress(CustomerEmail))
            throw new Exception(CustomerEmail + " is not a valid email address.");

        //We have a valid email, set the prop
        hmr.customerEmail = CustomerEmail;

        //Get Hubspot Contact Match by Email.  
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Getting Hubspot contact");
        hmr.hsContact = HubspotApi.Contacts.GetContactByEmail(CustomerEmail);

        //No contacts found with this address, create 
        string hsContactFirstName = "";
        string hsContactLastName = "";
        string hsContactFullName = "";
        if (hmr.hsContact == null)
        {
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "No Hubspot contact found or created for email: " + CustomerEmail);
            hmr.matchResultLog = "No contact found and no contact created for " + CustomerEmail + ".  Hubspot Deal not created. ";
            return;
        }
        else
        {
            hsContactFirstName = hmr.hsContact.Properties.Where(w => w.Key == "firstname").Select(s => s.Value.value).FirstOrDefault();
            hsContactLastName = hmr.hsContact.Properties.Where(w => w.Key == "lastname").Select(s => s.Value.value).FirstOrDefault();
            hsContactFullName = hsContactFirstName + " " + hsContactLastName;
        }


        //Deal Specific
        //Associations
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Getting Hubspot deal associations");
        HubspotApi.Associations ass = HubspotApi.Deals.CreateDealAssociations(CustomerEmail);

        //Get the HubspotOwner based on the associated Company
        string hsCompanyName = "No Company Match Found";
        if (ass != null)//Can't associate with a company, if we can't find any associations related to the email address.
            if (ass.associatedCompanyIds != null)
            {
                HubspotApi.Company hsCompany = HubspotApi.Companies.GetCompanyByID(ass.associatedCompanyIds[0]);
                if (hsCompany != null)
                    hsCompanyName = hsCompany.Properties.Where(w => w.Key == "name").Select(s => s.Value.value).FirstOrDefault();
                hsOwner = HubspotApi.Owners.GetOwnerOfHubspotCompany(ass.associatedCompanyIds);
            }

        string ownerName = "No Owner Found.";
        if (hsOwner != null)
            ownerName = hsOwner.firstName + " " + hsOwner.lastName;

        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Company: " + hsCompanyName);
        //Generate the Properties
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Generating Hubspot deal properties");
        Dictionary<string, string> dealProps = new Dictionary<string, string>();
        dealProps = GenerateHubspotQuoteProperties(hsOwner, CustomerEmail);

        //IF this is from an Ad campaign, associate with Deal.
        if (!string.IsNullOrEmpty(utm_campaign))
            dealProps.Add("utm_campaign", utm_campaign);
        if (!string.IsNullOrEmpty(utm_source))
            dealProps.Add("utm_source", utm_source);

        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Generated Hubspot deal properties");
        //Create the Deal, Associations can be null
        if (dealProps.Count > 0)
        {
            {
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Creating Hubspot deal");
                TheHubspotDeal = HubspotApi.Deals.CreateDeal(ass, dealProps);
                SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Created Hubspot deal.  ID: " + TheHubspotDeal.dealId);
            }
        }



        //Fill the match log
        //Log Matched Objects and log results of the process for debug /notify
        hmr.matchResultLog = "<p style=\"font-size:16px; font-weight:bold;\">Hubspot Match Log:</p>";
        hmr.matchResultLog += "<b>Matched Contact: </b>" + hsContactFullName + " (" + hmr.customerEmail + ")";
        hmr.matchResultLog += "<br /><b>Hubspot Contact Created: </b>" + hmr.hubspotContactCreated;
        if (hmr.hubspotContactCreated)
            hmr.matchResultLog += " (vid: " + hmr.hsContact.vid + ")";
        hmr.matchResultLog += "<br /><b>Matched Company: </b>" + hsCompanyName;
        hmr.matchResultLog += "<br /><b>Matched Agent: </b>" + ownerName + "<br />";


        ///Auto-fill Hubpsot Form
        //if (!IsDevUser() && CustomerEmail != "ktill@sensiblemicro.com")
        //{

        //    //hmr.matchResultLog += "<br />";
        //    //hmr.matchResultLog += "<br /><b>Filling Hubspot Form</b><br />ID:31d496c1-4001-437b-89fd-70dc7d2fc7c4";
        //    //hmr.matchResultLog += "<br />";
        //    ////Fill the Hubspot Form    
        //    //SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Generating Hubspot form properties");
        //    Dictionary<string, string> formProps = BuildQuoteHSFormProperties();
        //    string formResult = "";
        //    bool formSuccess = AutoFillHubspotQuoteForm(formProps, out formResult);
        //    hmr.matchResultLog += "<b>API Form Fill Result:</b> " + formResult;
        //    hmr.matchResultLog += "<br />";
        //}


        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Completed Hubspot quote tasks" + Environment.NewLine + "Match Log" + hmr.matchResultLog);
    }

    private Dictionary<string, string> GenerateHubspotQuoteProperties(HubspotApi.Owner hsOwner, string customer_email)
    {
        Dictionary<string, string> props = new Dictionary<string, string>();

        //Set the owner if known
        if (hsOwner != null)
            props.Add("hubspot_owner_id", hsOwner.ownerId.ToString());

        //On Create Only, set the businesstype
        //Get the company uid from the companycontact   
        string businessType = HubspotLogic.GetHubpsotDealBusinessType(customer_email);
        props.Add("dealtype", businessType);
        props.Add("part_number", PartNumber);
        props.Add("target_price", TargetPrice);
        props.Add("quote_quantity", Qty);
        props.Add("dealname", "Portal Search: " + PartNumber);
        props.Add("pipeline", HubspotApi.Pipeline.sales_pipeline);//Quote Pipeline        
        props.Add("dealstage", HubspotApi.DealStage.rfq_received);//dealstage = "Rz Batch Created"        
        props.Add("description", Notes);
        props.Add("quote_source", "portal_search");
        return props;
    }

    private Dictionary<string, string> BuildQuoteHSFormProperties()
    {


        Dictionary<string, string> props = new Dictionary<string, string>();
        //ret.Add("part_number", PartNumber);
        //ret.Add("target_price", TargetPrice);
        //ret.Add("firstname", FirstName);
        //ret.Add("lastname", LastName);
        //ret.Add("email", CustomerEmail);
        //ret.Add("part_requirements", Notes);
        //ret.Add("quote_quantity", Qty);
        //ret.Add("phone", ContactPhone);




        props.Add("part_number", PartNumber);
        props.Add("target_price", TargetPrice);
        props.Add("firstname", FirstName);
        props.Add("lastname", LastName);
        props.Add("email", CustomerEmail);
        props.Add("part_requirements", Notes);
        props.Add("quote_quantity", Qty);
        props.Add("phone", ContactPhone);
        props.Add("company", CompanyName);
        props.Add("hs_persona", "portal_quote");
        //Get the hubspot tracking cookie for orginal source and othe analytics.



        return props;




    }

    private bool AutoFillHubspotQuoteForm(Dictionary<string, string> props, out string formResult)
    {
        SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Filling Hubspot quote form");
        //throw new Exception("Exception Test1");
        // Form Variables (from the HubSpot Form Edit Screen)
        //31d496c1-4001-437b-89fd-70dc7d2fc7c4 - Quote Form 2019
        //1878634 - Sensible Portal ID
        int intPortalID = 1878634;

        //Form Name: [2019] Request a Quote
        //Form ID:  31d496c1-4001-437b-89fd-70dc7d2fc7c4
        HttpContext x = HttpContext.Current;
        string strFormGUID = "31d496c1-4001-437b-89fd-70dc7d2fc7c4"; //place your form guid here
        string strIpAddress = NetworkLogic.GetVisitorIpAddress();//x.Request.UserHostAddress;

        // Page Variables

        string strPageTitle = PartNumber + " | Sensible Micro Corp";
        string strPageURL = x.Request.Url.AbsoluteUri;

        // Do the post, returns true/false
        // string strResult = "Success";

        formResult = "";
        bool blnRet = HubspotApi.Forms.Post_To_HubSpot_FormsAPI(x, intPortalID, strFormGUID, props, strIpAddress, strPageTitle, strPageURL, ref formResult);
        if (!blnRet)
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Error, "Failure Filling Hubspot quote form: " + formResult);
        else
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Success Filling Test Hubspot quote form: " + formResult);
        //SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, "Finished filling Hubspot quote form");


        return blnRet;

    }



    private void LogQuoteToDataBase()
    {
        string UserName = null;
        if (Request.IsAuthenticated)
        {
            MembershipUser u = Membership.GetUser();
            if (u != null)
                UserName = u.UserName;

        }


        string search_vendor = "silicon_expert";
        string referrerUrl = Request.UrlReferrer.ToString() ?? "";



        using (RzDataContext rdc = new RzDataContext())
        {
            portal_quote pq = new portal_quote();
            pq.date_created = TimeStamp;
            pq.phone_number = ContactPhone;
            pq.part_number = PartNumber;
            int q = 0;
            if (int.TryParse(Qty, out q))
                pq.qty = Convert.ToInt32(q);
            pq.email_address = CustomerEmail;
            pq.first_name = FirstName;
            pq.last_name = LastName;
            pq.ip_address = VisitorIpAddress;
            pq.quote_notes = Notes;
            pq.company_name = CompanyName;


            pq.search_vendor = search_vendor;
            if (!string.IsNullOrEmpty(referrerUrl))
                if (referrerUrl.Length >= 221)
                    referrerUrl = referrerUrl.Substring(0, 220);
            pq.referrerUrl = referrerUrl;
            pq.campaign_source = utm_source;
            pq.campaing_id = utm_campaign;
            if (TheBatch != null)
                pq.search_batch_guid = TheBatch.unique_id;

            rdc.portal_quotes.InsertOnSubmit(pq);
            rdc.SubmitChanges();

        }
        //rzt.LogPortalSearchedPart(UserName ?? "Public / Anonymous", hfPartNumber.Value, "N/A", null, null, null, hfQty.Value, hfNotes.Value ?? "N/A", true, txtCustEmail.Text, campaignSource, campaignId, referrerUrl);

    }

    private void get_utm_tracking()
    {

        utm_source = get_utm_source();
        utm_campaign = get_utm_campaign();
    }


    private string get_utm_campaign()
    {
        string ret = Request.QueryString["utm_campaign"] ?? "";
        if (!string.IsNullOrEmpty(ret))
            ret = Tools.Strings.SanitizeInput(ret).ToLower();
        return ret;
    }



    private string get_utm_source()
    {
        //bing URL:  https://portal.sensiblemicro.com/public/search/part_details.aspx?id=30416703&q=3%2c000&ads_cmpid=337651332&ads_adid=1142393051421701&ads_matchtype=e&ads_network=o&ads_creative=%7bcreative%7d&utm_term=GRM21BR61C106KE15L&ads_targetid=kwd-71399964266866%3aloc-190&utm_campaign=&utm_source=bing&utm_medium=ppc&ttv=2&keyword_session_id=vt~adwords%7ckt~GRM21BR61C106KE15L%7cmt~e%7cta~%7bcreative%7d&_vsrefdom=wordstream&msclkid=221829dafb4e1f1b9a31729594123e2c&utm_content=GRM21BR61C106KE15L
        //Adwords URL:  https://portal.sensiblemicro.com/public/search/part_details.aspx?id=23863948&q=800,000&utm_campaign=1102030050&utm_source=google&utm_medium=cpc&utm_content=0&utm_term=&adgroupid=56694304041


        //Bing Variable Examples:
        //id = 30416703
        //q=3%2c000
        //ads_cmpid = 337651332
        //ads_adid = 1142393051421701 
        //ads_matchtype = e 
        //ads_network = o 
        //ads_creative =% 7bcreative % 7d 
        //utm_term = GRM21BR61C106KE15L 
        //ads_targetid = kwd - 71399964266866 % 3aloc - 190 
        //utm_campaign = 
        //&utm_source = bing 
        //utm_medium = ppc 
        //ttv = 2 
        //keyword_session_id = vt~adwords % 7ckt~GRM21BR61C106KE15L % 7cmt~e % 7cta~% 7bcreative % 7d 
        //& _vsrefdom = wordstream 
        //& msclkid = 221829dafb4e1f1b9a31729594123e2c 
        //utm_content = GRM21BR61C106KE15L
        //string ret = Tools.Strings.SanitizeInput(Request.QueryString["utm_source"] ?? "");
        //return ret;


        string ret = Request.QueryString["utm_source"] ?? "";
        if (!string.IsNullOrEmpty(ret))
            ret = Tools.Strings.SanitizeInput(ret).ToLower();
        return ret;
    }

    private void SendQuoteEmail()
    {
        List<string> toList = new List<string>();
        MembershipUser u = Membership.GetUser();

        //Add any Identified angents to the email.
        if (dmr != null)
            if (dmr.rzCompanyAgent != null)
            {
                if (!string.IsNullOrEmpty(dmr.rzCompanyAgent.email_address))
                    if (Tools.Email.IsEmailAddress(dmr.rzCompanyAgent.email_address))
                        agentEmails.Add(dmr.rzCompanyAgent.email_address);
            }

        if (agentEmails.Count > 0)
            toList.AddRange(agentEmails);
        toList.Add("incoming_quotes@sensiblemicro.com");
        if (u != null)
        {
            if (u.UserName.ToLower() == "kevint" || CustomerEmail.ToLower().Contains("shmootill"))
            {
                toList.Clear();
                toList.Add("ktill@sensiblemicro.com");
            }

        }

        string subject = "Quote from Sensiblemicro.com";
        if (TheBatch != null)
            subject += "- " + TheBatch.dealheader_name;
        //Build the match log
        string matchLog = "";
        if (dmr != null)
            matchLog += dmr.matchResultLog;
        if (hmr != null)
            matchLog += hmr.matchResultLog;

        if (matchLog != null)
            SystemLogic.Logs.LogEvent(SM_Enums.LogType.Information, matchLog);

        bool isDebug = IsDevUser();
        tools.SendQuoteEmail(toList, subject, FormInputTextHTML + "<br /> " + matchLog, TheBatch, isDebug);
    }

}