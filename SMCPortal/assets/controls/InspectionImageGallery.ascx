﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InspectionImageGallery.ascx.cs" Inherits="assets_InspectionImageGallery" %>
<%@ Register Src="~/assets/controls/InspectionImageManagerControl.ascx" TagPrefix="uc1" TagName="InspectionImageManagerControl" %>

<style>
    .flex-grid {
        display: flex;
        flex-wrap: wrap;
    }

    .col {
        flex: 1;
        flex-basis: 30%;
    }
</style>

<div class="panel ">
    <%--<asp:HiddenField ID="hfInspectionType" runat="server" />
    <asp:HiddenField ID="hfInspectionID" runat="server" />
    <asp:HiddenField ID="hfSectionName" runat="server" />--%>

    <div class="panel panel-heading" id="divUploadPanel" runat="server">
        Add Images: 
            <asp:FileUpload ID="fuAddImages" runat="server" AllowMultiple="True" />
        <asp:Button ID="btnAddImages" runat="server" Text="Upload" OnClick="btnAddImages_Click" OnClientClick="ShowUpdateProgress();"/>
    </div>
    <div class="panel panel-body">
        <asp:Panel ID="pnlImageGallery" CssClass="flex-grid" runat="server">
        </asp:Panel>
    </div>
</div>
