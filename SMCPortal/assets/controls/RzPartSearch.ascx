﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RzPartSearch.ascx.cs" Inherits="customControls_WebUserControl" %>


    <asp:Panel ID="pnlSearch" runat="server" Visible="false">
        <div class="row">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
            <div class="col-sm-4">
                A
            </div>
        </div>
    </asp:Panel>


        <asp:GridView ID="gvRzParts" runat="server" CssClass="table table-hover table-striped" GridLines="None" OnRowDataBound="gvRzParts_RowDataBound" AutoGenerateColumns="false" DataKeyNames="partercord_uid">
            <PagerStyle CssClass="pagination-ys" />
            <EmptyDataTemplate>
                <div class="alert alert-warning" style="text-align: center;">
                    <h5>We currently do not have this item in stock or available through our preferred supply chain partners. If you’d like to have our team source this item now, please click a <strong>Get Quote</strong> button above, and we will begin our search.</h5>
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:BoundField DataField="PartNumber" HeaderText="Part Number" SortExpression="PartNumber" />
                <asp:BoundField DataField="MFG" HeaderText="Manufacturer" SortExpression="MFG" />
                <asp:BoundField DataField="QTY" HeaderText="QTY" SortExpression="QTY" />
                <asp:BoundField DataField="DateCode" HeaderText="Date Code" SortExpression="DateCode" />
                <asp:BoundField DataField="RoHS" HeaderText="RoHS" SortExpression="RoHS" />
                <asp:BoundField DataField="LeadTime" HeaderText="LeadTime" SortExpression="LeadTime" />
            </Columns>
        </asp:GridView>
   



