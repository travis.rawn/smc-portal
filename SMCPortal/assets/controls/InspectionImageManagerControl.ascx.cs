﻿using SensibleDAL;
using SensibleDAL.ef;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InspectionImageManagerControl : UserControl
{

    //Private Varaibles
    SM_Tools tools = new SM_Tools();
    sm_binaryEntities smb = new sm_binaryEntities();
    insp_images TheImage = new insp_images();
    public event EventHandler Delete;






    //Public properties
    public int ControlWidth = 0;
    public bool txtChanged = false;
    public bool isPostBack = false;
    public event EventHandler ImageDeleted;

    public FileUpload fileUpload
    {
        get
        {
            return fu;
        }
    }


    public string imageDescription
    {
        get
        {
            if (ViewState["imageDescription"] != null)
                return (string)ViewState["imageDescription"];
            return "";
        }
        set
        {

            ViewState["imageDescription"] = value;
            txtDescription.Text = ViewState["imageDescription"].ToString();
        }

    }


    public bool imageLoaded
    {
        get
        {
            if (ViewState["imageLoaded"] == null)
                //ViewState["imageLoaded"] = !string.IsNullOrEmpty(img.ImageUrl);
                ViewState["imageLoaded"] = !string.IsNullOrEmpty(img.Src);
            return (bool)ViewState["imageLoaded"];

        }
        set
        {
            ViewState["imageLoaded"] = value;
        }
    }
    public bool hasFile
    {
        get
        {
            //if (ViewState["hasFile"] == null)
            ViewState["hasFile"] = fu.HasFile;
            return (bool)ViewState["hasFile"];

        }
        set
        {
            ViewState["hasFile"] = value;
        }
    }
    public bool EnableControls
    {
        get
        {
            if (ViewState["EnableControls"] == null)
                ViewState["EnableControls"] = true;
            return (bool)ViewState["EnableControls"];

        }
        set
        {
            ViewState["EnableControls"] = value;
        }
    }

    public int ImageUID
    {
        get
        {
            if (ViewState["ImageUID"] != null)
                return (int)ViewState["ImageUID"];
            return 0;
        }
        set
        {
            ViewState["ImageUID"] = value;
        }
    }


    public int InspectionID
    {
        get
        {
            if (ViewState["InspectionID"] != null)
                return (int)ViewState["InspectionID"];
            return 0;
        }
        set
        {
            ViewState["InspectionID"] = value;
        }

    }

    public string InspectionType
    {
        get
        {
            if (ViewState["InspectionType"] != null)
                return (string)ViewState["InspectionType"];
            return "";
        }
        set
        {

            ViewState["InspectionType"] = value;
        }

    }

    public string SectionID
    {
        get
        {
            if (ViewState["SectionID"] != null)
                return (string)ViewState["SectionID"];
            return "";
        }
        set
        {

            ViewState["SectionID"] = value;
        }

    }

    public string OrderType
    {
        get
        {
            if (ViewState["OrderType"] != null)
                return (string)ViewState["OrderType"];
            return null;
        }
        set
        {

            ViewState["OrderType"] = value;
        }

    }

    public string OrderID
    {
        get
        {
            if (ViewState["OrderID"] != null)
                return (string)ViewState["OrderID"];
            return null;
        }
        set
        {

            ViewState["OrderID"] = value;
        }

    }

    public bool showDescription
    {
        get
        {
            if (ViewState["showDescription"] != null)
                return (bool)ViewState["showDescription"];
            return true;
        }
        set
        {

            ViewState["showDescription"] = value;
        }
    }

    //private functions
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
            isPostBack = true;
        pnlControls.Visible = EnableControls;
        txtDescription.Enabled = EnableControls;
        imDescriptopm.Visible = showDescription;
        if (ControlWidth > 0)
            divImageControlContainer.Style.Add("width", ControlWidth.ToString() + "px");
    }




    public void init(insp_images i, bool test = false)
    {

        try
        {
            LoadImage(i);

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    public void initTest()
    {
        try
        {
            img.Src = "~/public/web_handlers/ImageHandler.ashx?id=f10fe992-2693-417e-b76b-40dc2821e049&amp;type=IDEA";
            txtDescription.Text = "Description Ya!";
            fancybox.HRef = img.Src;
            //Visibility
            fancybox.Visible = true;
            img.Visible = true;
            //Loaded
            imageLoaded = true;

        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

    }

    private insp_images getTestImage()
    {
        InspectionID = 249;
        SectionID = "HeaderImageTop";

        insp_images i = smb.insp_images.Where(w => w.unique_id == 11239).SingleOrDefault();
        string OriginPath = Path.GetFileName(fu.FileName);
        i.img_name = OriginPath;
        i.img_type = "jpg";
        i.img_blob = i.img_blob;
        i.img_description = txtDescription.Text;
        i.insp_id = InspectionID;
        i.insp_section_id = SectionID;
        i.insp_image_id = Guid.NewGuid().ToString();
        i.date_modified = DateTime.Now;
        i.img_path = fu.FileName;
        i.insp_type = InspectionType;
        return i;
    }

    protected void lbDelete_Click(object sender, EventArgs e)
    {

        try
        {
            OnDelete(e);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

        //bubble the event up to the parent
        if (this.ImageDeleted != null)
            this.ImageDeleted(this, e);

    }

    protected void OnDelete(EventArgs e)
    {
        DeleteImage();
        // raise the Delete event
        EventHandler delete = Delete;
        if (delete != null)
        {
            delete(this, e);
        }
    }


    private void DeleteImage()
    {

        insp_images i = smb.insp_images.Where(w => w.unique_id == ImageUID && w.insp_type == InspectionType).FirstOrDefault();
        if (i == null)
            throw new Exception("Cannot delete.  InspectionID image not found.");

        //Delete the local file as well if present.
        if (!string.IsNullOrEmpty(i.img_path_web))
            if (File.Exists(i.img_path_web))
                File.Delete(i.img_path_web);
        //Finally delete from database.
        smb.insp_images.Remove(i);
        smb.SaveChanges();
        fancybox.Visible = false;
        txtDescription.Text = "";
    }

    protected void lbUpload_Click(object sender, EventArgs e)
    {
        SaveImageAndData();
    }

    public void SaveImageAndData()
    {
        try
        {

            if (fu == null || InspectionID == 0 || string.IsNullOrEmpty(InspectionType))
                throw new Exception("Missing critical identifiers, cannot upload.");
            if (fu.HasFile)
            {
                //var FileExtension = Path.GetExtension(fu.PostedFile.FileName).Substring(1);
                tools.ValidateImageFileType(fu.FileName);

                if (InspectionID > 0)
                    SaveImage(InspectionID, InspectionType);
                else
                    throw new Exception("Invalid Inspection ID.");
            }          
            SaveDescription(InspectionID, InspectionType);
        
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
            if (ex.InnerException != null)
                msg += "Inner Exception: "+ex.InnerException.Message;
            tools.HandleResult("fail", msg );
        }
    }

    public void SaveImage(int inspectionID, string inspectionType)
    {

        if (inspectionID <= 0)
            throw new Exception("Invalid Inspection ID");
        if (string.IsNullOrEmpty(inspectionType))
            throw new Exception("Invalid Inspection Type");
        //insp_images i = smb.insp_images.Where(w => w.unique_id == ImageUID && w.insp_type == inspectionType).SingleOrDefault();
        insp_images i = smb.insp_images.Where(w => w.insp_id == inspectionID && w.insp_type == inspectionType && w.insp_section_id == SectionID).SingleOrDefault();
        if (i == null)
        {
            i = new insp_images();
            i.date_created = DateTime.Now;
            smb.insp_images.Add(i);
        }
        string OriginPath = Path.GetFileName(fu.FileName);
        i.img_name = OriginPath;
        i.img_type = Path.GetExtension(OriginPath);
        //i.img_blob = tools.ResizeImage(fu.FileBytes, 1024);
        Byte[] emptyBlob = new Byte[0];
        i.img_blob = emptyBlob;
        i.img_description = imageDescription;
        i.insp_id = inspectionID;
        i.insp_section_id = SectionID;
        i.insp_image_id = Guid.NewGuid().ToString();
        i.date_modified = DateTime.Now;
        string fileName = fu.FileName;
        i.img_path = fileName;
        i.insp_type = inspectionType;
        //string fileName = tools.GenerateFileNameFromInspection(i);
        
        //string fileName = inspectionType.ToUpper() + "_" + i.insp_id + "_" + i.insp_section_id + "_" + i.img_name;
        byte[] imgBytes = tools.ResizeImage(fu.FileBytes, 1024);
        string report = "";
        BinaryLogic bl = new BinaryLogic();//sTatic, needs this.
        string img_path_web = bl.SaveImageFileToFolder(i, imgBytes, out report);
        if (string.IsNullOrEmpty(img_path_web))
            throw new Exception("Error saving file to folder.");

        i.img_path_web = img_path_web;
        smb.SaveChanges();

        LoadImage(i);
    }






    public void SaveDescription(int inspectionID, string inspectionType)
    {

        insp_images i = null;
        if (inspectionType == SM_Enums.InspectionType.noncon.ToString())
        {
            //NonCon Specific, will some day refactor this to be more all encompassing.
            i = smb.insp_images.Where(w => w.unique_id == ImageUID && w.insp_type == inspectionType).SingleOrDefault();
        }
        else
            i = smb.insp_images.Where(w => w.insp_id == inspectionID && w.insp_section_id == SectionID && w.insp_type == inspectionType).FirstOrDefault();
        if (i == null)
            return;


        i.img_description = txtDescription.Text;
        i.date_modified = DateTime.Now;
        smb.SaveChanges();

        LoadImage(i);



    }



    private void GetImage()
    {
        if (InspectionID == 0)
            throw new Exception("Inspectio ID Not found.");
        TheImage = smb.insp_images.Where(w => w.insp_image_id == TheImage.insp_image_id).SingleOrDefault();
        InspectionID = TheImage.insp_id;
    }

    protected void LoadImage(insp_images i)
    {
        try
        {
            if (string.IsNullOrEmpty(InspectionType))
                return;
            //throw new Exception("Invalid inspection type.");
            //img.ImageUrl = "/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + InspectionType;
            //img.Src = "/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + InspectionType;
            //if (!string.IsNullOrEmpty(i.img_path_web))
            //    img.Src = "/public/web_handlers/ImageHandler.ashx?path=" + i.img_path_web + "&type=" + InspectionType;
            //else
            img.Src = "/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + InspectionType;
            txtDescription.Visible = showDescription;
            ImageUID = i.unique_id;
            txtDescription.Text = i.img_description;
            if (!string.IsNullOrEmpty(img.Src))
            {
                fancybox.HRef = img.Src;
                fancybox.Visible = true;
            }

            img.Visible = true;
            imageLoaded = true;
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }

    }
    //protected void LoadImageFromPath(insp_images i)
    //{
    //    try
    //    {
    //        if (string.IsNullOrEmpty(InspectionType))
    //            return;
    //        //throw new Exception("Invalid inspection type.");
    //        //img.ImageUrl = "/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + InspectionType;
    //        if (!string.IsNullOrEmpty(i.img_path_web))
    //            img.Src = "/public/web_handlers/ImageHandler.ashx?path=" + i.img_path_web + "&type=" + InspectionType;
    //        else
    //            img.Src = "/public/web_handlers/ImageHandler.ashx?id=" + i.insp_image_id + "&type=" + InspectionType;
    //        txtDescription.Visible = showDescription;
    //        ImageUID = i.unique_id;
    //        txtDescription.Text = i.img_description;
    //        if (!string.IsNullOrEmpty(img.Src))
    //        {
    //            fancybox.HRef = img.Src;
    //            fancybox.Visible = true;
    //        }

    //        img.Visible = true;
    //        imageLoaded = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        tools.HandleResultJS(ex.Message, true, Page);
    //    }

    //}
    protected void txtDescription_TextChanged(object sender, EventArgs e)
    {
        txtChanged = true;
    }

}
