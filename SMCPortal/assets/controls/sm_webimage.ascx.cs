﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class assets_controls_sm_webimage : System.Web.UI.UserControl
{

    private string ThumbNailUrl;
    private string ImageUrl;
    public bool imageLoaded;

    public void init(string imageUrl, string thumnailUrl)
    {
        ThumbNailUrl = thumnailUrl;
        ImageUrl = imageUrl;
        LoadImage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {     

    }
 

    private void LoadImage()
    {             

        if (string.IsNullOrEmpty(ImageUrl))
            return;
        if (string.IsNullOrEmpty(ThumbNailUrl))
            return;        
        img.Src = ImageUrl;        
        img.Visible = true;
        imageLoaded = true;

        fancybox.HRef = ThumbNailUrl;
        fancybox.Visible = true;        
      

    }


}