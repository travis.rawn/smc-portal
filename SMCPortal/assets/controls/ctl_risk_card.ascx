﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ctl_risk_card.ascx.cs" Inherits="assets_controls_ctl_risk_card" %>

<script src="/Content/plugins/svg-gauge/dist/gauge.min.js"></script>

<style>
    .part-number {
        font-weight: bold;
    }

    .part-mfg {
        margin-bottom: 10px;
    }

    .flex-parent {
        /*border: 1px solid #f76707;*/
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        /*align-items: flex-start;*/
    }

        .flex-parent > div > p {
            font-size: 12px;
        }

    .flex-child {
        /*border: 1px solid #f76707;*/
        width: 48%;
    }

    .gauge {
        margin-top: 15px;
    }


    .gauge-container {
        max-width: 300px;
    }

        .gauge-container > .gauge .dial {
            stroke: #334455;
            stroke-width: 10;
        }

        .gauge-container > .gauge .value {
            stroke: orange;
            stroke-dasharray: none;
            stroke-width: 13;
        }

        .gauge-container > .gauge .value-text {
            fill: #ccc;
            font-weight: 100;
            font-size: 1em;
        }
</style>



<asp:HiddenField ID="hfGaugeValue" runat="server" />
<div class="row">
    <div class="col-sm-12">
        <div class="part-number">
            <asp:Label ID="lblPartNumber" runat="server" Text="TI456245322-SU"></asp:Label>
            <div class="pull-right">+</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="part-mfg">
            <asp:Label ID="lblMfg" runat="server" Text="TEXAS INSTRUMENTS"></asp:Label>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="flex-parent">
            <div class="flex-child">
                <p>
                    <label>Lifecycle :</label>
                    <asp:Label ID="lblLifeCycle" runat="server"></asp:Label>
                </p>

                <p>
                    <label>Lead Time : </label>
                    <asp:Label ID="lblLeadTime" runat="server"></asp:Label>
                </p>

                <p>
                    <label>Pcn Notices : </label>
                    <asp:Label ID="lblPcn" runat="server"></asp:Label>
                </p>
                <p>
                    <label>Datasheet : </label>
                    <asp:Label ID="lblDatasheet" runat="server"></asp:Label>
                </p>
            </div>
            <div class="flex-child gauge">
                <div id="svgGauge" class="gauge-container" runat="server"></div>
            </div>
        </div>
    </div>
</div>














