﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_controls_ctl_risk_card : System.Web.UI.UserControl
{

    public decimal riskScore { get; set; }
    public string riskLabel { get; set; }
    public string partNumber { get; set; }
    public string manuFacturer { get; set; }
    public string lifeCycle { get; set; }
    public string pCn { get; set; }
    public string leadTime { get; set; }
    public string dataSheet { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // assets_controls_ctl_gauge_meter gaugeMeter = (assets_controls_ctl_gauge_meter)Page.LoadControl("~/assets/controls/ctl_gauge_meter.ascx");




        hfGaugeValue.Value = riskScore.ToString();
        lblPartNumber.Text = (partNumber ?? "Unknown").Trim().ToUpper();
        lblMfg.Text = (manuFacturer ?? "Unknown").Trim().ToUpper();
        lblLeadTime.Text = (leadTime ?? "Unknown").Trim();
        lblLifeCycle.Text = (lifeCycle ?? "Unknown").Trim();
        lblPcn.Text = (pCn ?? "Unknown").Trim();
        lblDatasheet.Text = (dataSheet ?? "Unknown").Trim();


        LoadGaugeControl();





    }

    private void LoadGaugeControl()
    {
        //svgGauge.ID = "svgGauge_" + partNumber;
        //Get the manually set div ID For the gauge
        string divID = svgGauge.ClientID;


        StringBuilder sb = new StringBuilder();

        sb.Append("\n");
        sb.Append(" $(function loadSVGGauge() {");
        sb.Append("\n");
        //Get the Risk score INSIDE the function.
        sb.Append("var score =" + riskScore);
        sb.Append("\n");
        // Create a new Gauge 
        sb.Append(" var svgGauge = Gauge(");
        sb.Append("\n");
        //Get Gauge based on Div ID
        sb.Append("document.getElementById('" + divID + "'), {");
        //Options
        sb.Append("\n");
        sb.Append(" min: 0,"); //Min Value
        sb.Append("\n");
        sb.Append(" max: 5,");  //Max Value
        sb.Append("\n");
        sb.Append("dialStartAngle: 180,");
        sb.Append("\n");
        sb.Append(" dialEndAngle: 0,");
        sb.Append("\n");
        sb.Append("value: score,");
        sb.Append("\n");
        //Color Function Array
        sb.Append("color: function(score) {");
        sb.Append("\n");
        sb.Append("switch (score)");
        sb.Append("\n");
        sb.Append("{");
        sb.Append("\n");
        sb.Append("case 2:");
        sb.Append("\n");
        sb.Append(" return '#4cb944';"); //Green
        sb.Append("\n");
        sb.Append("case 3:");
        sb.Append("\n");
        sb.Append("return '#FFBA08';  ");//Yellow 
        sb.Append("\n");
        sb.Append(" case 4:");
        sb.Append("\n");
        sb.Append(" return '#EE2738';"); //Red  
        sb.Append("\n");
        sb.Append(" case 5:");
        sb.Append("\n");
        sb.Append("return '#5F0F40';"); //Tyrian Purple  
        sb.Append("\n");
        sb.Append("default:");
        sb.Append("\n");
        sb.Append("return '#C2B8B2';");//Silver
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");
        sb.Append("}");
        sb.Append("\n");
        sb.Append("");
        sb.Append("\n");
        sb.Append(" });");
        sb.Append("\n");
        sb.Append("});");
        sb.Append("\n");


        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "RiskCard_" + partNumber, sb.ToString(), true);














    }
}