﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rz_contact_autocomplete.ascx.cs" Inherits="assets_controls_rz_contact_autocomplete" %>
<script>

    $(document).ready(function () {
        //Listener for blank companyname
        $("#<% =txtContactEmail.ClientID%>").on("keyup", function () {
            if ($("#<% =txtContactEmail.ClientID%>").val() == null || $("#<% =txtContactEmail.ClientID%>").val() == '')
                $("#<% =hfContactID.ClientID%>").val('');
        });

        //Set the Autocomplete Ajax settings
        $("#<% =txtContactEmail.ClientID%>").on("autocompleteselect", function (event, ui) { });
        $("#<% =txtContactEmail.ClientID%>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/assets/webmethods/sm_webmethods.aspx/LoadContactEmails",
                    data: "{input:'" + request.term + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var parsed = JSON.parse(data.d);
                        response(parsed);
                    },

                    failure: function (response) {
                        //alert(response.d);                     
                    },
                    error: function (response) {
                        //alert(response.d);                       
                    }
                });
            },
            focus: function (event, ui) {
                $("#<% =txtContactEmail.ClientID%>").val(ui.item.label);
                return false;
            },
            select: function (event, ui) {

                if (ui.item) {
                    event.preventDefault();//Prevents default behavior of showing value instead of label on select 
                    //Set Hidden Fields
                    $("#<% =hfContactID.ClientID%>").val(ui.item.value);
                    $("#<% =hfContactEmail.ClientID%>").val(ui.item.label);
                    //Set Textbox
                    $("#<% =txtContactEmail.ClientID%>").val(ui.item.label);//Manually filling the textbox with the selection since not doing default                    
                }
                else {
                    //Clear Textboxes
                    $("#<% =txtContactEmail.ClientID%>").val(null);
                    $("#<% =hfContactEmail.ClientID%>").val(null);
                    $("#<% =hfContactID.ClientID%>").val(null);
                }
            },

        });
    })

    //Set Contact ddl Onchange event
    function SetContactFields() {
        $("#<% =hfContactID.ClientID%> ").val($("#<% =ddlContactName.ClientID%>  option:selected").val());
        $("#<% =hfContactName.ClientID%>").val($("#<% =ddlContactName.ClientID%>  option:selected").text());
    }
</script>

<style>
    input.ui-autocomplete-loading {
        background: white url("/Images/autocomplete_loader.gif") right center no-repeat;
    }

    .ui-widget-content {
        z-index: 15;
    }

    .ui-menu-item {
        background-color: ghostwhite;
        max-width: 300px;
        border: solid;
        border-width: 1px;
        border-color: darkgray;
    }

    ul {
        padding: 0;
        z-index: 10;
        list-style-type: none;
    }
</style>

<asp:Panel ID="pnlContactSearch" runat="server" DefaultButton="lbSelect">
    <div class="input-group">
        <input type="text" class="form-control ui-autocomplete-input" placeholder="Contact email" runat="server" id="txtContactEmail">
        <span class="input-group-btn">
            <asp:LinkButton ID="lbSelect" runat="server" CssClass="btn btn-default" OnClick="OnContactSelected" Text="Select" OnClientClick="ShowUpdateProgress();"></asp:LinkButton>
        </span>
    </div>
</asp:Panel>
<!-- /input-group -->

<asp:DropDownList ID="ddlContactName" runat="server" CssClass="form-control" Style="display: none;" onchange="SetContactFields();">
    <asp:ListItem Text="--Choose--" Value="choose"></asp:ListItem>
</asp:DropDownList>
<asp:HiddenField ID="hfContactEmail" runat="server" />
<asp:HiddenField ID="hfContactName" runat="server" />
<asp:HiddenField ID="hfContactID" runat="server" />




