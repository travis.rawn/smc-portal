﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class customControls_WebUserControl : System.Web.UI.UserControl
{
    public bool ShowSearchBox { get; set; }
    public string Part { get; set; }

    RzTools.parts rzp = new RzTools.parts();
    RzTools rzt = new RzTools();
    SM_Tools tools = new SM_Tools();


    protected void Page_Load(object sender, EventArgs e)
    {
        //Don't htink I need to make a "LoadControl", unless
    }

    public void init(string partNumber)
    {
        //Part = Tools.Strings.StripNonAlphaNumeric(partNumber, true);
        Part = Tools.Strings.FilterTrash(partNumber);
        if (Part != null)
            txtSearch.Text = Part;
        LoadResultsGrid();
        if (ShowSearchBox)
        {
            pnlSearch.Visible = true;
        }
    }

    private void LoadResultsGrid()
    {
        if (Part == null)
            return;

        var query = rzp.SearchRzParts(Part);
        gvRzParts.DataSource = query;
        gvRzParts.DataBind();
    }


    protected void gvRzParts_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCell cell = e.Row.Cells[5];
            string stocktype = cell.Text.ToLower();

            if (stocktype == "consign" || stocktype == "stock")
            {
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#dff0d8");
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3c763d");

            }
            if (stocktype == "excess" || stocktype.Contains("week"))
            {
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#bcdff1");
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#31708f");
            }
        }
    }
}