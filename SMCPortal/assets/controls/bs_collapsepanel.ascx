﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="bs_collapsepanel.ascx.cs" Inherits="assets_controls_bs_collapsepanel" %>

<style>
    .header-text {
        font-weight: bold;
    }

    .panel-text {
        font-weight: bold;
    }

    .checkbox-group {
        max-width: 500px;
    }
</style>


<asp:HiddenField ID="hfChecked" runat="server" />
<asp:HiddenField ID="hfPanelText" runat="server" />
<asp:HiddenField ID="hfEnableCheckbox" runat="server" />


<%-- Checkbox Collapse Panel--%>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="pnlHeading">
            <div class="input-group checkbox-group">
                <span class="input-group-addon" id="spanCbx">
                    <%-- <input type="checkbox" aria-label="..." ID="cbx" runat="server" onclick="CheckboxToggle(this);">--%>
                    <asp:CheckBox ID="cbx" runat="server" AutoPostBack="false" CssClass="bs-cbx" onclick="checkboxToggle(this);" />
                </span>
                <%-- <asp:Label ID="lblCheckboxText" runat="server" Text="Label"></asp:Label>--%>
                <label class="form-control header-label" id="lblCheckboxText" runat="server"></label>
            </div>
        </div>
        <div runat="server" id="cpl" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <asp:Panel ID="pnlBody" runat="server">
                    <asp:Label ID="lblPanelText" runat="server" CssClass="panel-text "></asp:Label>
                    <asp:DropDownList ID="ddlPanelDropdown" runat="server" Visible="false" AutoPostBack="false" OnSelectedIndexChanged="ddlPanelDropdown_SelectedIndexChanged">
                        <asp:ListItem Text="Choose" Value="choose"></asp:ListItem>
                        <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                        <asp:ListItem Text="No" Value="no"></asp:ListItem>
                        <asp:ListItem Text="N/A" Value="na"></asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>

<script>
    $(".bs-cbx").each(function () {
        //Get the ID of the checkbox, then pass
        //var cbx = this.find('input[type=checkbox]')[0];
        var span = $("#spanCbx");
        var cbx = $(span).find(':checkbox')[0];


        var collapsePanelID = cbx.id.replace("cbx", "cpl"); //clp = collapse panel
        var collapsePanel = $("#" + collapsePanelID + "");
        var hiddenFieldID = cbx.id.replace("cbx", "hfChecked"); //clp = collapse panel
        var hiddenField = $("#" + hiddenFieldID);
        var boolIsChecked = false;
        boolIsChecked = hiddenField.val() === 'true';
        if (boolIsChecked) {
            collapsePanel.collapse('show');
            cbx.checked = true;
        }

        else {
            collapsePanel.collapse('hide');
            cbx.checked = false;
        }





    });



</script>
