﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class customControls_RewardClaimForm : System.Web.UI.UserControl
{
    RzDataContext RDC = new RzDataContext();
    RzTools rt = new RzTools();
    RewardSystem rs = new RewardSystem();
    SM_Tools tools = new SM_Tools();
    decimal goalValue = 100000;
    //public event EventHandler Click;
    public event EventHandler ButtonClick;
    public event EventHandler RowDelete;


    protected RewardSystem.Goal goal
    {
        get
        {
            if (ViewState["goal"] != null)
                return (RewardSystem.Goal)ViewState["goal"];
            else
                return null;

        }
        set
        {
            ViewState["goal"] = value;
        }
    }
    string RewardUserID
    {
        get
        {
            if (ViewState["RewardUserID"] != null)
                return ViewState["RewardUserID"].ToString();
            else
                return null;
        }
        set
        {
            ViewState["RewardUserID"] = value;
        }
    }
    List<string> claimIDsForUser
    {
        get
        {
            if (ViewState["claimIDsForUser"] != null)
                return (List<string>)ViewState["claimIDsForUser"];
            else
                ViewState["claimIDsForUser"] = new List<string>();
            return (List<string>)ViewState["claimIDsForUser"];
        }
        set
        {
            ViewState["claimIDsForUser"] = value;
        }
    }
    List<RewardSystem.Reward> rewardsForUser
    {
        get
        {
            if (ViewState["rewardsForUser"] != null)
                return (List<RewardSystem.Reward>)ViewState["rewardsForUser"];
            else
                ViewState["rewardsForUser"] = new List<RewardSystem.Reward>();
            return (List<RewardSystem.Reward>)ViewState["rewardsForUser"];
        }
        set
        {
            ViewState["rewardsForUser"] = value;
        }
    }
    public List<RewardSystem.Reward> RewardList
    {
        get
        {
            if (ViewState["RewardList"] != null)
                return (List<RewardSystem.Reward>)ViewState["RewardList"];
            else
                RewardList = new List<RewardSystem.Reward>();
            return RewardList;
        }
        set
        {
            ViewState["RewardList"] = value;
        }
    }
    protected decimal currentPercent
    {
        get
        {
            if (ViewState["currentPercent"] != null)
                return (decimal)(ViewState["currentPercent"]);
            else
                return 0;
        }
        set
        {
            ViewState["currentPercent"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
                UpdateUI();
            }
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }


    }
    public void BindDDlAgentCompanies()
    {
        ddlAgentCompanies.Items.Clear();
        ddlAgentCompanies.DataSource = null;
        ddlAgentCompanies.DataBind();
        ddlAgentCompanies.Items.Add(new ListItem("--Select a company--", "0"));
        ddlAgentCompanies.AppendDataBoundItems = true;
        //ddlAgentCompanies.DataSource = rt.GetCompaniesForRzUser(Membership.GetUser());
        ddlAgentCompanies.DataSource = rt.GetCompaniesForRzUser(RewardUserID);
        ddlAgentCompanies.DataTextField = "Value";
        ddlAgentCompanies.DataValueField = "Key";
        ddlAgentCompanies.DataBind();
    }

    protected void BindDDLCompanyContacts()

    {
        ddlCompanyContacts.Items.Clear();
        ddlCompanyContacts.Items.Add(new ListItem("--Select a contact--", "0"));
        ddlCompanyContacts.AppendDataBoundItems = true;
        ddlCompanyContacts.DataSource = rt.GetContactsForCompany(ddlAgentCompanies.SelectedValue);
        ddlCompanyContacts.DataTextField = "Value";
        ddlCompanyContacts.DataValueField = "Key";
        ddlCompanyContacts.DataBind();
        ddlCompanyContacts.Visible = true;
    }


    protected void ddlAgentCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDDLCompanyContacts();
    }

    protected void btnClaimBux_Click(object sender, EventArgs e)
    {
        ClaimReward(RewardList.Where(s => s.name.ToLower() == "sensible bux").FirstOrDefault());
        ButtonClick(sender, e);

    }
    protected void btnClaimHours_Click(object sender, EventArgs e)
    {
        ClaimReward(RewardList.Where(s => s.name.ToLower() == "reward hours").FirstOrDefault());
        ButtonClick(sender, e);
    }

    protected void ClaimReward(RewardSystem.Reward r)
    {
        try
        {
            //rs.CalculateEarnedRewards(RewardUserID, g, r);
            reward_claim c = new reward_claim();
            switch (r.name.ToLower())
            {
                case "sensible bux":
                    {
                        if (ddlAgentCompanies.SelectedValue == "0")
                            throw new Exception("Please choose a company.");
                        if (ddlCompanyContacts.SelectedValue == "0")
                            throw new Exception("Please choose a contact.");
                        if (string.IsNullOrEmpty(txtClaimBuxValue.Text))
                            throw new Exception("Please enter a dollar dmount amount.");
                        if (string.IsNullOrEmpty(txtClaimBuxDesc.Text))
                            throw new Exception("Please enter a description of the claim.");
                        c.company_id = ddlAgentCompanies.SelectedValue;
                        c.company_name = ddlAgentCompanies.SelectedItem.ToString();
                        c.contact_id = ddlCompanyContacts.SelectedValue;
                        c.contact_name = ddlCompanyContacts.SelectedItem.ToString();
                        c.value = Convert.ToDecimal(txtClaimBuxValue.Text);
                        c.description = txtClaimBuxDesc.Text;
                        c.approved = false;

                        break;
                    }
                case "reward hours":
                    {

                        if (string.IsNullOrEmpty(txtClaimHours.Text))
                            throw new Exception("Please enter an amount of hours to claim (in half-hour increments)");
                        if (!(Convert.ToDouble(txtClaimHours.Text) % .5 == 0))
                            throw new Exception("You can only clam hours in half hour increments.  (i.e. .5, 1, 4, 8, etc.)");
                        //if (!tools.ValdateInputStringDecimal(txtClamHoursUseDate.Text))
                        //    throw new Exception("Value must consist of numbers or decimals");            
                        c.value = Convert.ToDecimal(txtClaimHours.Text);
                        c.usage_date = Convert.ToDateTime(txtClamHoursUseDate.Text);
                        c.description = "Reward Time Off";
                        c.company_name = "N/A";
                        c.contact_name = "N/A";
                        //c.approved = true; - No Longer auto-approving hour claims
                        c.approved = false;
                        break;
                    }
            }

            c.name = r.name;
            c.reward_uid = r.unique_id;
            c.userid = r.rewardUserID;
            rs.ClaimReward(c, r, goal);
            SendClaimEmail(c);
            ClearControls();
            UpdateUI();
        }
        catch (Exception ex)
        {
            switch (ex.Message)
            {
                case "String was not recognized as a valid DateTime.":
                    tools.HandleResultJS("Please choose a valid date.", true, Page);
                    break;
                default:
                    tools.HandleResultJS(ex.Message, true, Page);
                    break;
            }
        }
    }

    protected void SendClaimEmail(reward_claim claim)
    {
        string approved;
        if (claim.approved == true)
            approved = "Yes";
        else
            approved = "No";
        n_user RzUser = rt.GetRzUser(claim.userid);
        MailMessage Msg = new MailMessage("reward_claim@sensiblemicro.com", "ktill@sensiblemicro.com", "New Reward Claim", "");
        if (Membership.GetUser().UserName != "kevint")
        {
            Msg.To.Add("ctorrioni@sensiblemicro.com");
            Msg.To.Add("joemar@sensiblemicro.com");
            Msg.To.Add("ftorrioni@sensiblemicro.com");
            Msg.To.Add("ltorrioni@sensiblemicro.com");
        }

        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/reward_claim_notify.htm"));
        string readFile = reader.ReadToEnd();
        string StrContent = "";
        StrContent = readFile;
        StrContent = StrContent.Replace("[claim_name]", claim.name);
        StrContent = StrContent.Replace("[claim_user]", RzUser.name);
        StrContent = StrContent.Replace("[claim_date]", claim.date_created.Value.ToString("d"));
        StrContent = StrContent.Replace("[claim_amount]", claim.value.ToString());
        StrContent = StrContent.Replace("[claim_approved]", approved);
        if (claim.name.ToLower() == "sensible bux")
        {
            StrContent = StrContent.Replace("[bux_company]", claim.company_name + "<br />");
            StrContent = StrContent.Replace("[bux_contact]", claim.contact_name + "<br />");
            StrContent = StrContent.Replace("[bux_description]", claim.description + "<br />");
        }
        else
        {
            StrContent = StrContent.Replace("[IndendedUseDate]", "Indended Use Date: " + claim.usage_date.ToString() + "<br />");
            StrContent = StrContent.Replace("[bux_company]", "");
            StrContent = StrContent.Replace("[bux_contact]", "");
            StrContent = StrContent.Replace("[bux_description]", "");
        }

        Msg.Body = StrContent.ToString();
        Msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.Send(Msg);
    }


    protected void gvClaimedHours_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        DeleteClaim(e.Keys[0].ToString());
        RowDelete(sender, e);
    }
    protected void gvClaimedBux_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DeleteClaim(e.Keys[0].ToString());
        RowDelete(sender, e);
    }
    protected void DeleteClaim(string claimID)
    {
        rs.DeleteClaim(claimID, Page);
        UpdateUI();
    }

    public void BindgvClaimedHours(string UserId)
    {
        //show exsiting claims for selected user in gridview 

        GetClaimIDsForUser(UserId);
        if (claimIDsForUser != null && claimIDsForUser.Count > 0)
        {
            var query = (from c in RDC.reward_claims
                         where claimIDsForUser.Contains(c.unique_id) && c.name.ToLower() == "reward hours" && c.is_deleted == false
                         select new
                         {
                             unique_id = c.unique_id,
                             Reward = c.name,
                             Company = c.company_name,
                             Contact = c.contact_name,
                             Value = c.value,
                             Description = c.description,
                             claim_date = c.date_created,
                             usage_date = c.usage_date ?? c.date_created,
                             approved = c.approved ?? false

                         }).OrderByDescending(c => c.claim_date).ToList();
            gvClaimedHours.DataSource = query;
            gvClaimedHours.DataKeyNames = new string[] { "unique_id" };
            gvClaimedHours.DataBind();
        }
        else
        {
            gvClaimedHours.DataSource = null;
            gvClaimedHours.DataBind();
        }
    }

    public void BindgvClaimedBux(string UserID)
    {
        //show exsiting claims for selected user in gridview 
        GetClaimIDsForUser(UserID);
        if (claimIDsForUser != null && claimIDsForUser.Count > 0)
        {
            var query = (from c in RDC.reward_claims
                         where claimIDsForUser.Contains(c.unique_id) && c.name.ToLower() == "sensible bux" && c.is_deleted == false
                         select new
                         {
                             unique_id = c.unique_id,
                             Reward = c.name,
                             Company = c.company_name,
                             Contact = c.contact_name,
                             Value = c.value,
                             Description = c.description,
                             claim_date = c.date_created,
                             usage_date = c.usage_date ?? c.date_created,
                             approved = c.approved ?? false

                         }).OrderByDescending(c => c.claim_date).ToList();
            gvClaimedBux.DataSource = query;
            gvClaimedBux.DataKeyNames = new string[] { "unique_id" };
            gvClaimedBux.DataBind();
        }
        else
        {
            gvClaimedBux.DataSource = null;
            gvClaimedBux.DataBind();
        }
    }

    private void LoadGridviews()
    {
        BindgvClaimedBux(RewardUserID);
        BindgvClaimedHours(RewardUserID);
    }
    public void GetClaimIDsForUser(string UserID)
    {
        claimIDsForUser = rs.GetClaimIDsForUser(UserID);
    }

    public void LoadControl(RewardSystem.Goal g, List<RewardSystem.Reward> rl, string rUsrID)
    {

        //Need term value so that the claim can compare overall earned when claiming a reward.
        try
        {
            goal = g;
            RewardUserID = rUsrID;
            CalculateNP();
            BindDDlAgentCompanies();
            RewardList = rl;
            //kt 9-2-2016 - NEed to be able to properly account for claimed rewards on control load.
            foreach (RewardSystem.Reward r in RewardList)
            {
                rs.CalculateEarnedRewards(RewardUserID, goal, r);
            }

            LoadGridviews();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }



    protected void UpdateUI()
    {
        try
        {
            LoadGridviews();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }
    public void ClearControls()
    {
        txtClaimBuxDesc.Text = "";
        txtClaimBuxValue.Text = "";
        txtClaimHours.Text = "";
        txtClamHoursUseDate.Text = "";
        if (ddlAgentCompanies.DataSource != null)
            ddlAgentCompanies.SelectedIndex = 0;
        if (ddlCompanyContacts.DataSource != null)
            ddlCompanyContacts.SelectedIndex = 0;
        ddlCompanyContacts.Visible = false;
    }

    protected void GetGoals(string RzUserID)
    {

        if (ViewState["goal"] == null)
            ViewState["goal"] = rs.GetNpChallengeGoal();
        else
            goal = (RewardSystem.Goal)ViewState["goal"];

    }
    protected void CalculateNP()
    {
        try
        {
            //n_user u = RDC.n_users.Where(w => w.unique_id == RewardUserID).FirstOrDefault();
            SalesLogic sl = new SalesLogic();
            goal.CurrentValue = sl.GetShippedNetProfit_SingleAgent(RewardUserID, goal.PeriodStart, goal.PeriodEnd);
            goal.CurrentPercent = (goal.CurrentValue / goalValue) * 100;
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }
}