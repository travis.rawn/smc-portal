﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class assets_controls_sm_tooltip : System.Web.UI.UserControl
{
    public string toolTipTitle = "ToolTip Title";
    public string toolTipBody = "ToolTip Body";
    public string dataPlacement = "top";
    public int toolTipWidth = 14;
    public int toolTipHeight = 14;
    public string CssClass { get; set; }
    //Handle Font size in parent page with .tooltip class
    //public int toolTipFontSize = 11;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(CssClass))
            lblToolTipTitle.CssClass = CssClass;
    }

    public void LoadTooltip()
    {
        //Title
        anchor.Attributes.Add("title", toolTipBody);

        //Text
        //anchor.InnerText = toolTipTitle;

        //Body
        lblToolTipTitle.Text = toolTipTitle;

        //Placement
        anchor.Attributes.Add("data-placement", dataPlacement.ToLower());





    }
}