﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class assets_controls_rz_partrecord_autocomplete : System.Web.UI.UserControl
{

    public event EventHandler PartSelected;

    public LinkButton TheSelectButton
    {
        get { return lbSelect; }
    }

    public string PartNumber
    {
        get { return hfPartNumber.Value; }
        set { hfPartNumber.Value = value; }
       
    }
    public string PartUID
    {
        get { return hfPartUID.Value; }
        set { hfPartUID.Value = value; }
    }

    public string TextBoxPlaceHolder { get; set; }
    public string SelectButtonText { get; set; }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TextBoxPlaceHolder))
            txtPartNumber.Attributes.Add("placeholder", TextBoxPlaceHolder);
        if (!string.IsNullOrEmpty(SelectButtonText))
            lbSelect.Text = SelectButtonText;


    }


    protected void OnPartSelected(object sender, EventArgs e)
    {
        if (this.PartSelected != null)
            this.PartSelected(this, e);

    }

    public void Clear()
    {
        txtPartNumber.Value = "";
        PartNumber = null;
        PartUID = null;
    }


}