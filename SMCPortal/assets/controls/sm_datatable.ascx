﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sm_datatable.ascx.cs" Inherits="sm_datatable" %>

<header>

    <style type="text/css">
        /*Use thead DataBound event stop hide columns while keeping datakeys accessible with this class*/
        .hiddencol {
            display: none;
        }

        p,
        ul li,
        ol {
            font-size: 14px;
            /*color: #617283;*/
            /*font-weight: 300;*/
            /*line-height: 1.75;*/
        }
    </style>
</header>

<div class="pull-right">
    <button type="button" id="btnToggle" runat="server" visible ="false"><i class="fas fa-bars"></i></button>
</div>

<div>
    <%--Mobile overflow--%>
    <div class="dataTable-container table-resonsive">
        <asp:GridView ID="gvMain" ClientID="gvMainClientID" runat="server"
            OnDataBound="gvMain_DataBound"
            OnRowDataBound="gvMain_RowDataBound"
            OnRowCreated="gvMain_RowCreated"
            OnPageIndexChanging="gvMain_PageIndexChanging"
            OnSelectedIndexChanged="gvMain_SelectedIndexChanged"
            ShowHeaderWhenEmpty="true"
            Width="100%"
            EmptyDataText="No Records Found">
        </asp:GridView>
    </div>
    <%--<asp:HiddenField ID="hfSelectedValue" runat="server" />  --%>
    <%--  <hr />--%>
</div>


