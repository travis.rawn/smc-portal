﻿using System;
using System.Collections.Generic;
using System.Linq;
using SensibleDAL.dbml;

public partial class customControls_RewardUsage : System.Web.UI.UserControl
{
    RewardSystem rs = new RewardSystem();
    RzTools rt = new RzTools();
    SM_Tools tools = new SM_Tools();
    RzDataContext RDC = new RzDataContext();
    protected decimal PeriodEarned
    {
        get
        {
            if (ViewState["PeriodEarned"] != null)
                return (decimal)(ViewState["PeriodEarned"]);
            else
                return 0;
        }
        set
        {
            ViewState["PeriodEarned"] = value;
        }
    }
    protected decimal TermEarned
    {
        get
        {
            if (ViewState["TermEarned"] != null)
                return (decimal)(ViewState["TermEarned"]);
            else
                return 0;
        }
        set
        {
            ViewState["TermEarned"] = value;
        }
    }

    protected decimal Used
    {
        get
        {
            if (ViewState["Used"] != null)
                return (decimal)(ViewState["Used"]);
            else
                return 0;
        }
        set
        {
            ViewState["Used"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {


    }
    public void LoadControl(List<string> UserIDList, RewardSystem.Goal g, List<RewardSystem.Reward> rl)
    {
        try
        {
            foreach (RewardSystem.Reward r in rl)
            {
                PeriodEarned = 0;
                TermEarned = 0;
                Used = 0;
                string usernameTest = "";
                foreach (string s in UserIDList)
                {
                    usernameTest = RDC.n_users.Where(u => u.unique_id == s).Select(n => n.name).FirstOrDefault();
                    Used += rs.GetClaimedRewardAmount(r, s);
                    r.PeriodValue = 0;
                    r.TermValue = 0;
                    CalculateGoalProgress(s, g, r);
                    CalculateRewardUsage(s, g, r);                    

                }
                switch (r.name.ToLower())
                {
                    case "reward hours":
                        {
                            UpdateHourLabels(Used);
                            break;
                        }
                    case "sensible bux":
                        {
                            UpdateBuxLabels(Used);
                            break;
                        }
                }
            }
            UpdateUI(g);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }
    public void LoadControl(string RewardUserID, RewardSystem.Goal g, List<RewardSystem.Reward> rl)
    {
        try
        {
            foreach (RewardSystem.Reward r in rl)
            {
                PeriodEarned = 0;
                TermEarned = 0;
                Used = 0;
                Used += rs.GetClaimedRewardAmount(r, RewardUserID);
                r.PeriodValue = 0;
                r.TermValue = 0;
                CalculateGoalProgress(RewardUserID, g, r);
                CalculateRewardUsage(RewardUserID, g, r);
                switch (r.name.ToLower())
                {
                    case "reward hours":
                        {
                            UpdateHourLabels(Used);
                            break;
                        }
                    case "sensible bux":
                        {
                            UpdateBuxLabels(Used);
                            break;
                        }
                }

            }
            UpdateUI(g);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }


    protected void UpdateUI(RewardSystem.Goal g)
    {
        lblGoalName.Text = "Goal: " + g.Name;
        lblPeriodRange.Text = g.PeriodStart.ToString("M/d/yyyy") + " - " + g.PeriodEnd.ToString("M/d/yyyy");
        lblTermRange.Text = g.TermStart.ToString("M/d/yyyy") + " - " + g.TermEnd.ToString("M/d/yyyy");
    }

    protected void UpdateHourLabels(decimal Used)
    {
        lblPeriodEarnedHours.Text = "" + PeriodEarned.ToString();
        lblTermEarnedHours.Text = "" + TermEarned.ToString("#,##0.##");
        lblTermUsedHours.Text = "" + Used.ToString();
        lblTermRemHours.Text = "" + (TermEarned - Used).ToString();

    }

    protected void UpdateBuxLabels(decimal Used)
    {
        lblPeriodEarnedBux.Text = "$" + PeriodEarned.ToString("#,##0.##");
        lblTermEarnedBux.Text = "$" + TermEarned.ToString("#,##0.##");
        lblTermUsedBux.Text = "$" + Used.ToString("#,##0.##");
        lblTermRemBux.Text = "$" + (TermEarned - Used).ToString("#,##0.##");

    }


    protected void CalculateGoalProgress(string userid, RewardSystem.Goal g, RewardSystem.Reward r)
    {

        r.PeriodValue += SensibleDAL.SalesLogic.GetShippedNetProfit_SingleUser(userid, g.PeriodStart, g.PeriodEnd);
        r.TermValue += SensibleDAL.SalesLogic.GetShippedNetProfit_SingleUser(userid, g.TermStart, g.TermEnd);
    }

    protected void CalculateRewardUsage(string userid, RewardSystem.Goal g, RewardSystem.Reward r)
    {

        Dictionary<string, decimal> earnedRewards = rs.CalculateEarnedRewards(userid, g, r);
        PeriodEarned += earnedRewards["periodEArned"];
        TermEarned += earnedRewards["termEArned"];
    }



    //Unused
    protected void SetPeriodLabels()
    {

    }

    //Unused
    protected void SetTermLabels()
    {

    }





}