﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="quote_form.ascx.cs" Inherits="quote_form" %>
<asp:HiddenField ID="hfPartNumber" runat="server" />
<asp:HiddenField ID="hfGTMLabel" runat="server" />
<asp:HiddenField ID="hfQty" runat="server" />
<asp:HiddenField ID="hfPrice" runat="server" />
<asp:HiddenField ID="hfNotes" runat="server" />
<asp:HiddenField ID="hfCustFirstName" runat="server" />
<asp:HiddenField ID="hfCustLastName" runat="server" />
<asp:HiddenField ID="hfCustEmail" runat="server" />
<asp:HiddenField ID="hfPhone" runat="server" />
<asp:HiddenField ID="hfQuoteBatchGuid" runat="server" />
<asp:HiddenField ID="hfCompanyName" runat="server" />


<style>
    ul, ol {
        margin-left: 0;
    }

    .row {
        margin-bottom: 15px;
    }
</style>

<asp:Panel ID="pnlQuote" runat="server" DefaultButton="lbSendQuote">
    <div class="modal fade" id="divQuoteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <asp:UpdatePanel runat="server" ID="upQuote">
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title">
                                <asp:Label ID="lblQuotePartNumber" runat="server"></asp:Label>
                            </h3>
                        </div>
                        <div class="modal-body">
                            <asp:Panel ID="pnlQuoteForm" runat="server">
                                <%--Row1--%>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtCustFirstName" runat="server" CssClass="form-control" Placeholder="First Name:" ToolTip="Please provide your first name."></asp:TextBox>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtCustLastName" runat="server" CssClass="form-control" Placeholder="Last Name:" ToolTip="Please provide your last name."></asp:TextBox>
                                    </div>
                                </div>
                                <%--Row2--%>
                                <div class="row">

                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtTargetPrice" runat="server" CssClass="form-control" Placeholder="Target Price:" ToolTip="Please provide your target price."></asp:TextBox>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtTargetQTY" runat="server" CssClass="form-control" Placeholder="Target QTY:" ToolTip="Please provide the target quantity you are looking for."></asp:TextBox>
                                    </div>
                                </div>
                                <%--Row3--%>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtCustEmail" runat="server" CssClass="form-control" Placeholder="Email Address:" ToolTip="Please provide a valid email address."></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" Placeholder="Contact Phone:" ToolTip="Please provide the best phone number to reach you."></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control" Placeholder="Company Name:" ToolTip="Please provide your company name."></asp:TextBox>
                                    </div>
                                </div>
                                <%--row4--%>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtBatchNotes" runat="server" TextMode="MultiLine" CssClass="form-control" Placeholder="Additional Notes:" ToolTip="Please provide any addition notes that may be useful (date code restrictions, RoHS, etc.)."></asp:TextBox>

                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                            <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                                <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                                <asp:Button ID="lbSendQuote" runat="server" CssClass="hs-button primary large btn-block" OnClientClick="return doQuote(false);" OnClick="lbSendQuote_Click" Text="Submit Quote" />
                                                <asp:Button ID="lbTestQuoteProcess" runat="server" CssClass="hs-button primary large btn-block" OnClientClick="return doQuote(true);" OnClick="lbSendQuote_Click" Text="Test Quote" Visible="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlQuoteSuccess" runat="server">
                                <div style="text-align: center;">
                                    <h3>Quote Request In Process.</h3>
                                    <h5>
                                        <em>Thank you, we will be in touch with you shortly.</em>
                                    </h5>
                                    <hr />
                                </div>
                                <div style="text-align: center;">
                                    <h4>Details:</h4>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <asp:Label ID="lblSuccessPN" runat="server"></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <asp:Label ID="lblSuccessQty" runat="server"></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <asp:Label ID="lblSuccessPrice" runat="server"></asp:Label></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <asp:Label ID="lblSuccessNotes" runat="server"></asp:Label></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</asp:Panel>



