﻿using SensibleDAL;
using SensibleDAL.ef;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_InspectionImageGallery : System.Web.UI.UserControl
{


    //int id = 1821;
    SM_Quality_Logic ql = new SM_Quality_Logic();
    public List<insp_images> ImageList = new List<insp_images>();
    SM_Tools tools = new SM_Tools();


    public int InspectionID
    {
        get { return (ViewState["InspectionID"] != null) ? (int)ViewState["InspectionID"] : 0; }
        set { ViewState["InspectionID"] = value; }
    }
    public string InspectionType
    {
        get { return (ViewState["InspectionType"] != null) ? ViewState["InspectionType"].ToString() : ""; }
        set { ViewState["InspectionType"] = value; }
    }
    public string SectionName
    {
        get { return (ViewState["SectionName"] != null) ? ViewState["SectionName"].ToString() : ""; }
        set { ViewState["SectionName"] = value; }
    }

    public bool EnableUpload
    {
        get
        { return (ViewState["EnableUpload"] != null ? (bool)ViewState["EnableUpload"] : false); }
        set
        { ViewState["EnableUpload"] = value; }
    }

    public bool EnableControls
    {
        get
        { return (ViewState["EnableControls"] != null ? (bool)ViewState["EnableControls"] : false); }
        set
        { ViewState["EnableControls"] = value; }
    }

    public bool ShowDescription
    {
        get
        { return (ViewState["ShowDescription"] != null ? (bool)ViewState["ShowDescription"] : false); }
        set
        { ViewState["ShowDescription"] = value; }
    }




    protected void Page_Load(object sender, EventArgs e)
    {

    }



    public void LoadGallery()
    {
        if (ImageList == null || ImageList.Count == 0)
            return;
        //LoadImageData();
        LoadImageControls();
    }

    public void LoadGallery(List<insp_images> imageList, string inspectionType, int inspectionID, string sectionName)
    {
        //This is necessary to set the critical values. 
        //May need to call from PageLoad so variables can survive the page lifecycle
        if (inspectionID <= 0)
            throw new Exception("Invalid Inspection ID");
        SectionName = sectionName;
        InspectionType = inspectionType;
        InspectionID = inspectionID;


        if (imageList == null || imageList.Count == 0)
            return;
        ImageList = imageList;
        LoadImageControls();
    }

    public void SaveAllImages()
    {
        if (InspectionID <= 0)
            throw new Exception("Invalid Inspection ID");
        if (string.IsNullOrEmpty(InspectionType))
            throw new Exception("Invalid Inspection Type");
        if (string.IsNullOrEmpty(SectionName))
            throw new Exception("Invalid Section Name");

        using (sm_binaryEntities smb = new sm_binaryEntities())
        {
            List<InspectionImageManagerControl> listImc = new List<InspectionImageManagerControl>();
            tools.GetControlList(this.Page.Master.FindControl("MainContent").Controls, listImc);
            foreach (InspectionImageManagerControl imc in listImc)
            {
                imc.InspectionID = InspectionID;
                imc.SaveImageAndData();
            }
            smb.SaveChanges();

        }

       

    }



    private int GetHighestSectionIdIndex()
    {
        int ret = 0;
        List<insp_images> sectionImages = ImageList.Where(w => w.insp_section_id.Contains(SectionName)).ToList();
        List<int> sectionIndices = new List<int>();
        foreach (insp_images ii in sectionImages)
        {
            string sectionIndex = ii.insp_section_id.Replace(SectionName, "");
            if (string.IsNullOrEmpty(sectionIndex))
                return 0;
            int index = 1;
            if (!int.TryParse(sectionIndex, out index))
                throw new Exception("Please provide a valid number of images to add.");
            sectionIndices.Add(index);
        }
        if (sectionIndices.Count > 0)
            ret = sectionIndices.Max();
        return ret;
    }



    private void LoadImageControls()
    {
        if (!EnableUpload)
            divUploadPanel.Visible = false;

        pnlImageGallery.Controls.Clear();
        //int highestIndex = GetHighestSectionIdIndex();
        foreach (insp_images i in ImageList)
        {
            //highestIndex++;

            //Create a container Div to apply the flex class to:
            System.Web.UI.HtmlControls.HtmlGenericControl divIm = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            divIm.Attributes.Add("class", "col");

            //Intantiate the imagemanager control.
            InspectionImageManagerControl c = (InspectionImageManagerControl)Page.LoadControl("~/assets/controls/InspectionImageManagerControl.ascx");
            c.ID = "im" + i.insp_section_id;// + highestIndex;
            c.ClientIDMode = ClientIDMode.Static;//Required for predicatble control ID, otherwise they come up as  ct100 etc.       
            c.showDescription = ShowDescription;
            //c.imageDescription = i.img_description;
            c.EnableControls = EnableControls;
            c.InspectionType = InspectionType;
            c.SectionID = i.insp_section_id;
            c.init(i);
            //Add teh event Handler that will delete the control from paretn as well when deleting image.
            c.Delete += new EventHandler(DeleteImage_Click);
            //Add the Image Control to the Div
            divIm.Controls.Add(c);
            pnlImageGallery.Controls.Add(divIm);


        }
    }

    void DeleteImage_Click(object sender, EventArgs e)
    {

        Page.Response.Redirect(Page.Request.Url.ToString(), false);


    }

    protected void LoadImageData()
    {

        using (sm_binaryEntities smb = new sm_binaryEntities())
        {
            ImageList = ql.LoadInspectionImagery(InspectionType, InspectionID, SectionName);
        }

    }

    protected void btnAddImages_Click(object sender, EventArgs e)
    {

        try
        {

            int index = GetHighestSectionIdIndex();
            using (sm_binaryEntities smb = new sm_binaryEntities())
            {
                foreach (HttpPostedFile f in fuAddImages.PostedFiles)
                {
                    index++;
                    SaveImage(smb, f, index);

                }
                smb.SaveChanges();
            }


            LoadImageData();
            LoadImageControls();
        }
        catch (Exception ex)
        {
            string message = ex.Message + " <br />";
            if (ex is DbEntityValidationException)
            {
                DbEntityValidationException eve = (DbEntityValidationException)ex;
                message += "Entity Failure: <br />";
                foreach (DbEntityValidationResult d in eve.EntityValidationErrors)
                {
                    foreach (DbValidationError dve in d.ValidationErrors)
                    {
                        message += dve.ErrorMessage + "<br />";
                    }

                }
            }

            tools.HandleResult("fail", ex.Message);
        }
    }

    public void SaveImage(sm_binaryEntities smb, HttpPostedFile f, int index)
    {
        if (InspectionID <= 0)
            throw new Exception("Invalid Inspection ID");
        if (string.IsNullOrEmpty(InspectionType))
            return;
        string sectionID = SectionName + index.ToString();
        string fileName = f.FileName;
        var FileExtension = Path.GetExtension(fileName).Substring(1);
        tools.ValidateImageFileType(fileName);
        insp_images i = new insp_images();
        i.date_created = DateTime.Now;
        smb.insp_images.Add(i);

        i.insp_id = InspectionID;
        i.insp_section_id = sectionID;
        i.insp_image_id = Guid.NewGuid().ToString();
        i.date_modified = DateTime.Now;
        i.img_path = f.FileName;
        i.insp_type = InspectionType;
        i.img_description = "";

        //Save Empty image blob for now
        Byte[] emptyBlob = new Byte[0];
        i.img_blob = emptyBlob;
        string OriginPath = Path.GetFileName(f.FileName);
        i.img_name = OriginPath;
        i.img_type = FileExtension;
        byte[] fileBytes;
        using (var binaryReader = new BinaryReader(f.InputStream))
        {
            fileBytes = binaryReader.ReadBytes(f.ContentLength);
        }
        //i.img_blob = tools.ResizeImage(fileBytes, 1024);
        //i.img_description = txtDescription.Text;
        fileBytes = tools.ResizeImage(fileBytes, 1024);
        string report = "";
        BinaryLogic bl = new BinaryLogic();//static reference
        string img_path_web = bl.SaveImageFileToFolder(i, fileBytes, out report);
        if (string.IsNullOrEmpty(img_path_web))
            throw new Exception("Error saving file to folder.");
        i.img_path_web = img_path_web;
      
    }


}