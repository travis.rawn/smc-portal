﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class assets_controls_rz_contact_autocomplete : System.Web.UI.UserControl
{

    public event EventHandler ContactSelected;

   
    public string ContactEmail
    {
        get { return hfContactEmail.Value; }
    }

    public string ContactName
    {
        get { return hfContactName.Value; }
    }
    public string ContactID
    {
        get { return hfContactID.Value; }
    }    


    public string TextBoxPlaceHolder { get; set; }
    public string SelectButtonText { get; set; }
          

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TextBoxPlaceHolder))
            txtContactEmail.Attributes.Add("placeholder", TextBoxPlaceHolder);
        if (!string.IsNullOrEmpty(SelectButtonText))
            txtContactEmail.Value = SelectButtonText;

    }

    //protected void btnFillDdlContact_Click(object sender, EventArgs e)
    //{

    //}
    //private void Search(object sender, EventArgs e)
    //{
    //    RzDataContext rdc = new RzDataContext();

    //    if (this.ContactSelected != null)
    //        this.OnContactSelected(this, e);

    //    if (!string.IsNullOrEmpty(CompanyID))
    //    {
    //        ddlContactName.Items.Clear();
    //        ddlContactName.Style.Remove("display");
    //        ddlContactName.Items.Add(new ListItem { Text = "--Choose--", Value = "choose", Selected = true });
    //        ddlContactName.AppendDataBoundItems = true;
    //        List<companycontact> contactList = rdc.companycontacts.Where(w => w.base_company_uid == CompanyID).OrderBy(o => o.contactname).ToList();
    //        ddlContactName.DataSource = contactList;
    //        ddlContactName.DataTextField = "contactname";
    //        ddlContactName.DataValueField = "unique_id";
    //        ddlContactName.DataBind();
    //    }
    //    else
    //    {
    //        ddlContactName.Style.Add("display", "none");
    //    }
    //}  


    protected void OnContactSelected(object sender, EventArgs e)
    {
        if (this.ContactSelected != null)
            this.ContactSelected(this, e);

    }
}