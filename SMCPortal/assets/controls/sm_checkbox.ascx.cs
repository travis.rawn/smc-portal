﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_controls_sm_checkbox : System.Web.UI.UserControl
{

    //Server Side event Handler, since users controls events happen after parent page_load.
    public event EventHandler CheckedChanged;
    public bool autoPostBack = true;
    public bool isChecked { get { return cbx.Checked; } set { cbx.Checked = value; } }    
    public string theText = "";
    public string cssClass = "";

    public HiddenField theHiddenField { get {return hfCbxValue; } }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadCheckboxOptions();
    }

    private void LoadCheckboxOptions()
    {
        cbx.AutoPostBack = autoPostBack;
        if (!string.IsNullOrEmpty(cssClass))
            cbx.CssClass = cssClass;
        isChecked = cbx.Checked;
        if (!string.IsNullOrEmpty(theText))
            lblCheckBox.InnerText = theText;
    }

    //protected void cbx_CheckedChanged(object sender, EventArgs e)
    //{
    //    ViewState["isChecked"] = cbx.Checked;        
    //}

    protected void cbx_CheckedChanged(object sender, EventArgs e)
    {
        //bubble the event up to the parent
        if (this.CheckedChanged != null)
            this.CheckedChanged(this, e);
    }
}