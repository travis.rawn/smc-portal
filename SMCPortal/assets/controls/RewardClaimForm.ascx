﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RewardClaimForm.ascx.cs" Inherits="customControls_RewardClaimForm" %>


<%@ Register Src="~/assets/controls/RewardUsage.ascx" TagPrefix="uc1" TagName="RewardUsage" %>

<asp:UpdatePanel ID="upRewardClaimForm" runat="server">
    <ContentTemplate>        
        <asp:HiddenField ID="TabName" runat="server" />

        <div class="panel panel-default">
            <div class="panel-heading">
                <label>Submit a Claim</label>
            </div>
            <div class="panel-body">
                <div style="text-align: center; padding-left: 10px; padding-right: 10px;">
                    <div id="Tabs" role="tabpanel">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tabBux">Bux</a></li>
                            <li><a data-toggle="tab" href="#tabHours">Hours</a></li>
                        </ul>
                        <div class="row">
                            <div class="tab-content">
                                <div id="tabBux" class="tab-pane fade in active">
                                    <asp:UpdatePanel ID="upClaimBux" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlAgentCompanies" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlAgentCompanies_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:DropDownList ID="ddlCompanyContacts" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
                                            <asp:TextBox ID="txtClaimBuxValue" runat="server" CssClass="form-control" placeholder="Enter Bux Amount"></asp:TextBox>
                                            <asp:TextBox ID="txtClaimBuxDesc" runat="server" CssClass="form-control" placeholder="Enter Description"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="btnClaimBux" runat="server" Text="Submit Bux Claim" OnClick="btnClaimBux_Click" CssClass="btn btn-block btn-success" />

                                    <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
                                        <asp:UpdatePanel ID="upBuxGrid" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvClaimedBux" runat="server" OnRowDeleting="gvClaimedBux_RowDeleting" CssClass="table table-hover table-striped gvstyling" GridLines="None" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="Reward" HeaderText="Reward" SortExpression="Reward" />
                                                        <asp:BoundField DataField="Company" HeaderText="Company" SortExpression="Company" />
                                                        <asp:BoundField DataField="Contact" HeaderText="Contact" SortExpression="Contact" />
                                                        <asp:BoundField DataField="Value" HeaderText="Value" SortExpression="Value" />
                                                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                                        <asp:BoundField DataField="claim_date" HeaderText="Claim Date" SortExpression="claim_date" DataFormatString="{0:MMMM d, yyyy}" HtmlEncode="false" />
                                                        <asp:BoundField DataField="approved" HeaderText="Approved" SortExpression="approved" />
                                                        <asp:CommandField ButtonType="Button" ShowDeleteButton="True">
                                                            <ControlStyle CssClass="btn btn-block btn-danger" />
                                                        </asp:CommandField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div id="tabHours" class="tab-pane fade">
                                        <asp:UpdatePanel ID="upClaimHours" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtClaimHours" runat="server" CssClass="form-control" Placeholder="Enter hours to claim."></asp:TextBox>
                                                <asp:TextBox ID="txtClamHoursUseDate" runat="server" CssClass="form-control input-append date" Placeholder="Choose a date."></asp:TextBox>
                                              
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:Button ID="btnClaimHours" runat="server" Text="Submit Hours Claim" OnClick="btnClaimHours_Click" CssClass="btn btn-block btn-success" />

                                        <asp:UpdatePanel ID="upHoursGrid" runat="server">
                                            <ContentTemplate>

                                                <div style="vertical-align: top; left: auto; height: auto; overflow: auto; width: auto;">
                                                    <asp:GridView ID="gvClaimedHours" runat="server" OnRowDeleting="gvClaimedHours_RowDeleting" CssClass="table table-hover table-striped gvstyling" GridLines="None" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:BoundField DataField="Reward" HeaderText="Reward" SortExpression="Reward" />
                                                            <asp:BoundField DataField="Value" HeaderText="Value" SortExpression="Value" />
                                                            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                                            <asp:BoundField DataField="claim_date" HeaderText="Claim Date" SortExpression="claim_date" DataFormatString="{0:MMMM d, yyyy}" HtmlEncode="false" />
                                                            <asp:BoundField DataField="usage_date" HeaderText="Usage Date" SortExpression="usage_date" DataFormatString="{0:MMMM d, yyyy}" HtmlEncode="false" />
                                                            <asp:CommandField ButtonType="Button" ShowDeleteButton="True">
                                                                <ControlStyle CssClass="btn btn-block btn-danger" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    $(function () {
        var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "tabBux";
        $('#Tabs a[href="#' + tabName + '"]').tab('show');
        $("#Tabs a").click(function () {
            $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
        });
    });
</script>
