﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sm_datatable : UserControl
{

    //Source Data
    public object dataSource;
    public bool serverSide = false;//declare that we're doing data server side.
    public string emptyDataText = "No Data Found";

    //Moment DateTime Format
    //public string dateTimeFormat = "'LT'";
    public string dateTimeFormat = "'l'"; //

    //Sorting / Ordering
    public bool allowSorting = true;
    public int sortColumn = -1; //default is negative one, because Zero is a valid column
    public string sortDirection = "asc";
    public int sortColumnFixed = -1; //default is negative one, because Zero is a valid column
    public string sortDirectionFixed = "asc";



    public bool showRecordsInfo = true;
    public int pageLength = 10;

    public bool compactStyle = true;
    //public bool showCheckboxes = false;
    //public int checkBoxColumnNumber;
    public bool showGridButton = false;
    public bool showToggleButton = false;
    public int buttonColumnNumber;
    public bool AddGridSelectButton = false;


    //Pagination
    public bool allowPaging = true;
    public bool allowPageLengthChange = true;
    public bool showPageInfo = true;
    public bool allowPageResize = true;


    //Search
    public bool allowSearch = true;



    public List<Tuple<string, string>> jqColDefs = new List<Tuple<string, string>>();//Use this if you need to set column specific stuff like column visibility
    public GridView theGridView
    {
        get { return gvMain; }
    }

    public event EventHandler SelectedIndexChanged;
    public event EventHandler DataBound;
    public event GridViewRowEventHandler RowDataBound;
    public event GridViewRowEventHandler RowCreated; 
    public void Clear()
    {//clear the contents
        gvMain.DataSource = null;
        gvMain.DataBind();
    }




    protected void Page_Load(object sender, EventArgs e)
    {        
        SetToggleButton();
    }

    private void SetToggleButton()
    {
        if (!showToggleButton)
            return;
        string id = this.ClientID;
        btnToggle.Attributes.Add("onclick", "toggleElement('"+id+ "_gvMain_wrapper'); return false;");
        btnToggle.Visible = true;

    }
    public void loadGrid(bool useJqueryDatatables = true, bool useTeableSorter = false)
    {

        //Clear any Existing.
        gvMain.EmptyDataText = emptyDataText;
        gvMain.DataSource = null;
        gvMain.DataBind();


        if (dataSource != null)
        {
            //Add the Select Button
            theGridView.AutoGenerateSelectButton = AddGridSelectButton;
            //Load the Data
            LoadDataSource();
            //Bind the Data
            gvMain.DataBind();
            if (useJqueryDatatables)
            {
                if (gvMain.Rows.Count > 0) //Required for DataTables
                    gvMain.HeaderRow.TableSection = TableRowSection.TableHeader;
                //Note, for ASP.net Gridview, it's necessary to add the following as Datatables (And tablesorter) both require well-formed thead and tbody tags
                //Also, required is some JS to fix the Gridview headers:  https://weblog.west-wind.com/posts/2010/Nov/18/Adding-proper-THEAD-sections-to-a-GridView
                //Gridviews render thead and tbody ALL in the tbody section, omitting proper th and tr tags, which breaks any kind of client side sorting.

                //Handle all the Options
                //if (jqColDefs.Count > 0)
                HandleDataTableOptions();

                //Handle Dattable Callback Events
                AppendCallbackEvents();
            }
            else if (useTeableSorter)
                gvMain.Attributes.Add("class", "tablesorter");
        }
        else
        {
            gvMain.DataSource = null;
            gvMain.DataBind();
        }
    }

    private void AppendCallbackEvents()
    {
        AppendPageChangingEvent();
    }

    private void AppendPageChangingEvent()
    {

        StringBuilder sb = new StringBuilder();

        //re load pagination ui on mouse over after pagination.




        sb.Append("$(function() {");
        sb.Append("handleTooltipHover();");


        sb.Append("});");



        sb.Append("function handleTooltipHover()");
        sb.Append("{");
        sb.Append("$('#" + gvMain.ClientID + " tbody').on('mouseover', 'tr', function() {");
        sb.Append(" $('[data-toggle=\"tooltip\"]').tooltip({");
        sb.Append("trigger: 'hover click',");
        sb.Append("html: true");
        sb.Append("});");
        sb.Append("});");
        sb.Append("};");



        string key = Guid.NewGuid().ToString();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), key, sb.ToString(), true);


    }

    private void HandleDataTableOptions()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("//Loading Datatable for : " + gvMain.DataSourceID.ToString());
        sb.Append("\n");

        sb.Append(" $(document).ready(function() {");
        //Handle Moment.js sorting
        //sb.Append(" $.fn.dataTable.moment('M/D/YYYY a');");    //This is the proper format per Moment syntax for "12 /12/2012"
        //{ 5/17/2018 12:00:00 AM }
        sb.Append("\n");
        //sb.Append(" $.fn.dataTable.moment('M/D/YYYY hm:mm:ss A');");
        sb.Append(" $.fn.dataTable.moment(" + dateTimeFormat + ");");
        sb.Append("\n");
        //FullWidthContent_smdtPriority_gvMain
        sb.Append(" $('#" + gvMain.ClientID + "').DataTable( {");
        sb.Append("\n");



        //"serverSide": true, Declare data is server side, for manual things like maintaining sort on programmatic page change.
        sb.Append(" 'serverSide': " + serverSide.ToString().ToLower() + ",");
        sb.Append("\n");

        //Responsive Resize Plugin:
        sb.Append("'pageResize': true,");
        sb.Append("\n");

        //destroy: true = In the current version of DataTables(1.10.4) you can simply add destroy: true to the configuration to make sure any table already present is removed before being re-initialised.,
        sb.Append("'destroy': true,");
        sb.Append("\n");

        //Sorting    

        sb.Append(" 'ordering': " + allowSorting.ToString().ToLower() + ",");
        sb.Append("\n");

        //-1 is default.  Zero is also a valid column.
        if (sortColumn > -1)
        {
            //sb.Append(" 'orderFixed': [[ " + sortColumn.ToString() + ", 'desc' ]],");
            //sb.Append("\n");
            sb.Append(" 'order': [[ " + sortColumn.ToString() + ", '" + sortDirection + "' ]],");
            sb.Append("\n");
        }
        else if (sortColumnFixed > -1)
        {
            sb.Append(" 'order': [[ " + sortColumnFixed.ToString() + ", '" + sortDirectionFixed + "' ]],");
            sb.Append("\n");
        }



        //default text / empty
        sb.Append(" 'language': {");
        sb.Append("\n");
        sb.Append(" 'infoEmpty': 'No record found.',");
        sb.Append("\n");
        sb.Append(" 'zeroRecords': 'No records match your query.',");
        sb.Append("\n");
        sb.Append(" },");
        sb.Append("\n");

        //Search / Filter
        sb.Append(" 'bFilter': " + allowSearch.ToString().ToLower() + ",");//Disables Filtering
        sb.Append("\n");


        //Paging
        sb.Append(" 'bPaginate': " + allowPaging.ToString().ToLower() + ",");
        sb.Append("\n");
        sb.Append(" 'bLengthChange': " + allowPageLengthChange.ToString().ToLower() + ",");
        sb.Append("\n");
        sb.Append(" 'bInfo': " + showPageInfo.ToString().ToLower() + ",");
        sb.Append("\n");

        //Enable / Disable Info below grid
        sb.Append(" 'bInfo': " + showRecordsInfo.ToString().ToLower() + ",");
        sb.Append("\n");

        //Enable / Disable Auto Width
        sb.Append(" 'autoWidth': true " + ",");
        sb.Append("\n");

        //Set default rows per page:
        sb.Append(" 'pageLength':" + pageLength + ",");
        sb.Append("\n");

        //Save State to preserve sorting, etc. on postback, care, this save to localStorage, so if you don't clear that, it sticks.  Can be complicated.  False by default.
        sb.Append(" 'stateSave': false,");
        sb.Append("\n");


        sb.Append("'responsive': true,");
        sb.Append("\n");

        //Column Defs 
        sb.Append(" \"columnDefs\": [");
        sb.Append("\n");
        sb.Append(GetColumnDefString());
        sb.Append("\n");
        sb.Append("],");
        sb.Append("\n");

        


        //Grid Button
        if (showGridButton)
        {

        }





        sb.Append(" } );");
        sb.Append("\n");

        //Fade DataTable in after Load.  
        sb.Append("\n");
        sb.Append("$('.dataTable-container').fadeIn();");
        sb.Append("\n");

        //Close the main function.
        sb.Append(" } );");
        sb.Append("\n");
        sb.Append("\n");

        //Set the classes
        if (compactStyle)
            gvMain.Attributes.Add("class", " display compact stripe");

        string key = Guid.NewGuid().ToString();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), key, sb.ToString(), true);
    }

    private string GetColumnDefString()
    {
        StringBuilder ret = new StringBuilder();
        //Need to check for int for column index, if so, include brackets.  Not perfect since I may need actual ints in the future, for string values, but for now it's ok.

        //if (showCheckboxes)
        //{
        //    ret.Append("{");
        //    ret.Append("\n");
        //    ret.Append("orderable: false, ");
        //    ret.Append("\n");
        //    ret.Append("className: 'select-checkbox', ");
        //    ret.Append("\n");
        //    ret.Append("targets: " + checkBoxColumnNumber.ToString());
        //    ret.Append("\n");
        //    ret.Append("},");
        //    ret.Append("\n");
        //}



        foreach (Tuple<string, string> d in jqColDefs)
        {

            //{
            //    "targets": [ 2 ],
            //    "visible": false,
            //    "searchable": false
            //},

            ret.Append("{");
            ret.Append("\n");
            int value;
            if (int.TryParse(d.Item2, out value))
                ret.Append(d.Item1 + " : [" + d.Item2 + "],");
            else
                ret.Append(d.Item1 + " : " + d.Item2 + ",");
            ret.Append("\n");
            ret.Append("\n");
            ret.Append("},");
            ret.Append("\n");
        }



        return ret.ToString();
    }


    private void LoadDataSource()
    {
        gvMain.DataSource = dataSource;
    }

    protected void gvMain_DataBound(object sender, EventArgs e)
    {
        //Fires one time when the gv is DataBound
        //This is good for applying styling to the entire grid, for instance, hiding columns and keeping access to the DataKeys

        //bubble the event up to the parent
        if (DataBound != null)
            DataBound(this, e);
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Fires Once Per Row
        //This is good for per row-events, like editing the style / contents of cells in each row

        //bubble the event up to the parent
        if (this.RowDataBound != null)
            this.RowDataBound(this, e);

    }

    protected void gvMain_RowCreated(Object sender, GridViewRowEventArgs e)
    {
        //This is good for adding columns and controls, as it gets fired on postback, as opposed to RowDataBound

        //bubble the event up to the parent
        if (this.RowCreated != null)
            this.RowCreated(this, e);
    }




    protected void gvMain_SelectedIndexChanged(Object sender, EventArgs e)
    {
        //bubble the event up to the parent
        if (this.SelectedIndexChanged != null)
            this.SelectedIndexChanged(this, e);
    }



    protected void gvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMain.PageIndex = e.NewPageIndex;
        gvMain.DataBind();
    }









}

