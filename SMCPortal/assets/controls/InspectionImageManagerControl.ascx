﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InspectionImageManagerControl.ascx.cs" Inherits="InspectionImageManagerControl" ClientIDMode="Inherit" %>

<style>
    .imageManager {
        /*width: 280px;*/
        text-align: center;
    }

    .imageManager-align {
        display: flex;
        flex-wrap: wrap;
        height: 300px;
        max-width: 300px;
        align-items: center;
        justify-content: space-around;
    }

        .imageManager-align > * {
            flex: 0 0 100%;
        }

    .imageManager-top {
        /*Keeps the control section anchored to the bottom even when no image.*/
        min-height: 200px;
    }

        .imageManager-top a {
            display: flex;
            justify-content: center;
            margin-bottom: 2px;
        }

        .imageManager-top img {
            height: 100%;
            width: 100%;
            max-height: 200px;
            max-width: 250px;
        }


    .imageManager-middle {
        display: inline-flex;
        margin-bottom: 2px;
    }

    .fileUpload {
        width: 80%;
    }

        .fileUpload input {
            width: 100%;
        }

    .uploadButton {
        width: 10%;
    }

        .uploadButton a {
            color: forestgreen;
        }


    .deleteButton {
        width: 10%;
    }

        .deleteButton a {
            color: orangered;
        }

    .imageManager-bottom {
    }

    .txtDescription {
        min-height: 30px;
    }
</style>


<div class="card" id="divImageControlContainer" runat="server">
    <div class="imageManager">
        <div class="imageManager-align">
            <div class="imageManager-top">
                <%-- <a class="fancybox" id="fancybox" runat="server" visible="false"></a>
                <asp:Image ID="img" CssClass="img img-responsive" runat="server" />--%>
                <a runat="server" id="fancybox" data-fancybox="gallery" visible="false">
                    <img runat="server" id="img" visible="false"></a>
            </div>
            <asp:Panel ID="pnlControls" runat="server">
                <div class="imageManager-middle">
                    <div class="fileUpload">
                        <asp:FileUpload ID="fu" runat="server" />
                    </div>
                    <div class="uploadButton">
                        <asp:LinkButton runat="server" ID="lbUpload" OnClick="lbUpload_Click" Text="Upload Image" OnClientClick="ShowUpdateProgress();" ToolTip="Upload"> <i class="fas fa-upload"></i></asp:LinkButton>
                    </div>
                    <div class="deleteButton">
                        <asp:LinkButton runat="server" ID="lbDelete" OnClick="lbDelete_Click" OnClientClick="ShowUpdateProgress();" ToolTip="Delete"><i class="fas fa-trash"></i></asp:LinkButton>
                    </div>
                </div>
            </asp:Panel>
            <div class="imageManager-bottom" id="imDescriptopm" runat="server">
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" OnTextChanged="txtDescription_TextChanged" CssClass="form-control " Style="padding-bottom: 10px;" Placeholder="Description..."></asp:TextBox>
            </div>
        </div>
    </div>
</div>

<script>
    //Hide empty imagemanagers from fancybox gallery
    $('img [src=""]').hide();
</script>






