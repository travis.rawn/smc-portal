﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RewardUsage.ascx.cs" Inherits="customControls_RewardUsage" %>
<asp:UpdatePanel ID="upRewardUsage" runat="server">
    <ContentTemplate>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>
                    <asp:Label ID="lblGoalName" runat="server"></asp:Label>

                </h3>
                <asp:Label ID="lblTermRange" runat="server" Text="Label" Font-Size="Smaller" Visible="false"></asp:Label>
                <asp:Label ID="lblPeriodRange" runat="server" Text="Label" Font-Size="Smaller" Visible="false"></asp:Label>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <label>Reward</label><br />
                        <span class="fa fa-clock-o" style="font-size: 18px;"></span>
                        <label style="padding-left: 10px;">Hours</label><br />
                        <span class="fa fa-dollar" style="font-size: 18px;"></span>
                        <label style="padding-left: 10px;">Bux</label><br />
                    </div>
                    <div class="col-sm-2">
                        <label>Used</label><br />
                        <asp:Label ID="lblTermUsedHours" runat="server" Text="Label"></asp:Label><br />
                        <asp:Label ID="lblTermUsedBux" runat="server" Text="Label"></asp:Label><br />
                    </div>
                    <div class="col-sm-2">
                        <label>Rem</label><br />
                        <asp:Label ID="lblTermRemHours" runat="server" Text="Label"></asp:Label><br />
                        <asp:Label ID="lblTermRemBux" runat="server" Text="Label"></asp:Label><br />
                    </div>
                    <div class="col-sm-2">
                        <label>Period</label><br />
                        <asp:Label ID="lblPeriodEarnedHours" runat="server" Text="Label"></asp:Label><br />
                        <asp:Label ID="lblPeriodEarnedBux" runat="server" Text="Label"></asp:Label><br />
                    </div>
                    <div class="col-sm-2">
                        <label>Overall</label><br />
                        <asp:Label ID="lblTermEarnedHours" runat="server" Text="Label"></asp:Label><br />
                        <asp:Label ID="lblTermEarnedBux" runat="server" Text="Label"></asp:Label><br />
                    </div>
                </div>

            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
