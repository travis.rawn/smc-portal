﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sm_checkbox.ascx.cs" Inherits="assets_controls_sm_checkbox" %>
<style>
   
    .form-check-input  input[type=checkbox] {
        margin-right: 10px;
    }
  
</style>
<!-- Hidden Field to persist value on postback. -->
<asp:HiddenField ID="hfCbxValue" runat="server" />
<div class="form-check">
    <asp:CheckBox ID="cbx" runat="server" OnCheckedChanged="cbx_CheckedChanged" CssClass="form-check-input" type="checkbox" />     
    <label class="form-check-label" for="cbx" id="lblCheckBox" runat="server"></label>
</div>

