﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class customControls_RzAgentPicker : System.Web.UI.UserControl
{
    RzTools rt = new RzTools();
    SM_Tools tools = new SM_Tools();
    RzDataContext rdc = new RzDataContext();
    public event EventHandler SelectedIndexChanged;
    //The Initial Value for the "choose" item
    public string DefaultSelectionName;
    //Control AutoPostback
    public bool autoPostBack = false;
    //Control whether ot append databound items
    public bool AppendDataBoundItems = true;


    private List<string> TeamNames = new List<string>();
    private Dictionary<string, string> SelectedUserDict = new Dictionary<string, string>();
    //public List<string> TeamNames
    //{
    //    get { return (List<string>)ViewState["TeamNames"]; }
    //    set { ViewState["TeamNames"] = value; }
    //}

    //private Dictionary<string, string> SelectedUserDict
    //{
    //    get { return (Dictionary<string, string>)ViewState["SelectionItemsList"] ?? new Dictionary<string, string>(); }
    //    set { ViewState["SelectionItemsList"] = value; }
    //}

    public string SelectedUserID
    {
        get
        {
            return (ddlAgentPicker.SelectedValue);
        }
        set
        {
            ddlAgentPicker.SelectedValue = value;
        }
    }


    public string SelectedUserName
    {
        get
        {
            return (ddlAgentPicker.SelectedItem.ToString());
        }
    }

    public bool Enabled
    {
        get
        {
            return (ddlAgentPicker.Enabled);
        }
        set
        {
            ddlAgentPicker.Enabled = value;
        }
    }
    public ListItemCollection Items
    {      get
        {
            return (ddlAgentPicker.Items);
        }
        
    }

    protected override void OnInit(EventArgs e)
    {
        //This ensures that the ddl loads before the other controls on the page, so selection is maintained.
        if (!Page.IsPostBack)
            SetDefaults();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void SetDefaults()
    {
        ddlAgentPicker.AppendDataBoundItems = AppendDataBoundItems;
        SetAuotPostBack();
        ddlAgentPicker.Items.Add(new ListItem(GetDefaultSelectionName(), "choose"));
        //Add Admin Users
        if (Roles.IsUserInRole("admin"))
            SelectedUserDict.Add("a192e149aca4495585d2934e037d5898", "Kevin Till");
    }

    private string GetDefaultSelectionName()
    {
        //"--Please Choose--"
        string ds = "Please Choose";
        if (!string.IsNullOrEmpty(DefaultSelectionName))
            ds = DefaultSelectionName;
        return "--" + ds + "--";
    }

    //Handle AutoPostback
    private void SetAuotPostBack()
    {
        if (autoPostBack == false)
        {
            ddlAgentPicker.Attributes.Remove("onchange");
            ddlAgentPicker.AutoPostBack = false;
        }
        else
            ddlAgentPicker.AutoPostBack = true;
    }

    //Load Choices Teams  

    public void LoadPicker(List<string> teamNames, DateTime startDate, DateTime endDate)
    {
        
        try
        {
            TeamNames = teamNames;
            BindAgentDDL(startDate, endDate);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    public void LoadPicker(List<string> teamNames)
    {

        try
        {
            TeamNames = teamNames;
            BindAgentDDL();
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }

    public void LoadPicker(List<n_user> uList)
    {

        try
        {
            BindAgentDDL(uList);
        }
        catch (Exception ex)
        {
            tools.HandleResultJS(ex.Message, true, Page);
        }
    }



    private string GetTeamNamesString()
    {
        string ret = string.Empty;
        foreach (string s in TeamNames)
        {
            if (s != TeamNames.Last())
                ret += s + ",";
            else
                ret += s;
        }
        return ret;
    }

    private void BindAgentDDL()
    {
        ddlAgentPicker.Items.Clear();
        ddlAgentPicker.DataSource = SelectedUserDict.OrderBy(o => o.Value);

        if (TeamNames == null)
            return;
        //Get Team Names 
        string teamNamesString = GetTeamNamesString();
        ddlAgentPicker.Items.Add(new ListItem("All Users on Team (" + teamNamesString + ")", "all"));
        SelectedUserDict = rt.GetUsersForTeams(TeamNames, true).Select(s => new { ID = s.unique_id, Name = s.name }).ToDictionary(d => d.ID, d => d.Name);
        if (SelectedUserDict.Count() > 0)
        {
            ddlAgentPicker.DataSource = SelectedUserDict.OrderBy(o => o.Value);
            ddlAgentPicker.DataTextField = "Value";
            ddlAgentPicker.DataValueField = "Key";
            ddlAgentPicker.DataBind();
        }
    }


    private void BindAgentDDL(DateTime startDate, DateTime endDate)
    {
        ddlAgentPicker.Items.Clear();
        if (TeamNames == null)
            return;
        //Get Team Names 
        string teamNamesString = GetTeamNamesString();
        ddlAgentPicker.Items.Add(new ListItem("All Users on Team (" + teamNamesString + ")", "all"));
        SelectedUserDict = rt.GetUsersFromLineItems(startDate, endDate);
        if (SelectedUserDict.Count() > 0)
        {
            ddlAgentPicker.DataSource = SelectedUserDict.OrderBy(o => o.Value);
            ddlAgentPicker.DataTextField = "Value";
            ddlAgentPicker.DataValueField = "Key";
            ddlAgentPicker.DataBind();
        }
    }

    private void BindAgentDDL(List<n_user> uList)
    {
        ddlAgentPicker.Items.Clear();
        if (uList.Count > 1)
        ddlAgentPicker.Items.Add(new ListItem("All Users", "all"));
        SelectedUserDict = uList.Select(s => new { ID = s.unique_id, Name = s.name }).ToDictionary(d => d.ID, d => d.Name);
       // rt.GetUsersForTeams(TeamNames).Select(s => new { ID = s.unique_id, Name = s.name }).ToDictionary(d => d.ID, d => d.Name);
        if (SelectedUserDict.Count() > 0)
        {
            ddlAgentPicker.DataSource = SelectedUserDict.OrderBy(o => o.Value);
            ddlAgentPicker.DataTextField = "Value";
            ddlAgentPicker.DataValueField = "Key";
            ddlAgentPicker.DataBind();
        }
    }

    protected void ddlAgentPicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (SelectedIndexChanged != null)
            SelectedIndexChanged(this, e);
    }

    //When a team is selected, this will hold the ids for members of the team.
    public List<n_user> GetSelectedUsers()
    {
        List<n_user> ret = new List<n_user>();
        if (ddlAgentPicker.SelectedValue == "choose")
            return ret;
        if (ddlAgentPicker.SelectedValue == "all")
        {//Gotta retreive each n_user, viewstate won't hold n_user.  Maybe consider DataTable or something.
            //ret = rdc.n_users.Where(w => SelectedUserDict.ContainsKey(w.unique_id)).ToList(); -- Containskey - no translated method in SQL
            foreach (KeyValuePair<string, string> kvp in SelectedUserDict)
            {
                string uid = kvp.Key;
                n_user u = rdc.n_users.Where(w => w.unique_id == uid).FirstOrDefault();
                if (u != null)
                    ret.Add(u);
            }
        }
        else//Single User
            ret.Add(rdc.n_users.Where(w => w.unique_id == ddlAgentPicker.SelectedValue).FirstOrDefault());
        return ret;
    }

    



}