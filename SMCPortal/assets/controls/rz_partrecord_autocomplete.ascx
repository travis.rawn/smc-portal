﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rz_partrecord_autocomplete.ascx.cs" Inherits="assets_controls_rz_partrecord_autocomplete" %>
<script>

    $(document).ready(function () {
        //Listener for blank companyname
        $("#<% =txtPartNumber.ClientID%>").on("keyup", function () {
            if ($("#<% =txtPartNumber.ClientID%>").val() == null || $("#<% =txtPartNumber.ClientID%>").val() == '')
                $("#<% =hfPartNumber.ClientID%>").val('');
        });

        //Set the Autocomplete Ajax settings
        $("#<% =txtPartNumber.ClientID%>").on("autocompleteselect", function (event, ui) { });
        $("#<% =txtPartNumber.ClientID%>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/assets/webmethods/sm_webmethods.aspx/LoadParts",
                    data: "{input:'" + request.term + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var parsed = JSON.parse(data.d);
                        response(parsed);
                    },

                    failure: function (response) {
                        //alert(response.d);                     
                    },
                    error: function (response) {
                        //alert(response.d);                       
                    }
                });
            },
            focus: function (event, ui) {
                $("#<% =txtPartNumber.ClientID%>").val(ui.item.label);
                return false;
            },
            select: function (event, ui) {

                if (ui.item) {
                    event.preventDefault();//Prevents default behavior of showing value instead of label on select 
                    //Set Hidden Fields
                    $("#<% =hfPartUID.ClientID%>").val(ui.item.value);
                    $("#<% =hfPartNumber.ClientID%>").val(ui.item.label);                  
                    //Set Textbox
                    $("#<% =txtPartNumber.ClientID%>").val(ui.item.label);//Manually filling the textbox with the selection since not doing default                    
                }
                else {
                    //Clear Textboxes
                    $("#<% =txtPartNumber.ClientID%>").val(null);
                    $("#<% =hfPartNumber.ClientID%>").val(null);
                    $("#<% =hfPartUID.ClientID%>").val(null);
                }
            },

        });
    })

    
</script>

<style>
    input.ui-autocomplete-loading {
        background: white url("/Images/autocomplete_loader.gif") right center no-repeat;
    }

    .ui-widget-content {
        z-index: 15;
    }

    .ui-menu-item {
        background-color: ghostwhite;
        max-width: 300px;
        border: solid;
        border-width: 1px;
        border-color: darkgray;
    }

    ul {
        padding: 0;
        z-index: 10;
        list-style-type: none;
    }
</style>


<asp:Panel ID="pnlPRAutocomplete" runat="server" DefaultButton="lbSelect">
    <div class="input-group">
    <input type="text" class="form-control ui-autocomplete-input" placeholder="Type Part Number" runat="server" id="txtPartNumber">
    <span class="input-group-btn">
        <asp:LinkButton ID="lbSelect" runat="server" CssClass="btn btn-default" OnClick="OnPartSelected" Text="Select" OnClientClick="ShowUpdateProgress();"></asp:LinkButton>
    </span>
</div>
<!-- /input-group -->
</asp:Panel>
<asp:HiddenField ID="hfPartNumber" runat="server" />
<asp:HiddenField ID="hfPartUID" runat="server" />






