﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="datetime_flatpicker.ascx.cs" Inherits="assets_controls_datetime_flatpicker" %>
<%--https://flatpickr.js.org/examples/--%>

<%--FlatPicker--%>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<%--<link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/dark.css">--%>

<%--FlatPicker--%>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/shortcut-buttons-flatpickr@0.1.0/dist/shortcut-buttons-flatpickr.min.js"></script>

<style>
    /*flatpickr-day selected*/
    .selected {
        border-color =#EE2738;
        border-top-color: #EE2738;
        border-bottom-color: #EE2738;
        border-left-color: #EE2738;
        border-right-color: #EE2738;
        background-color: #EE2738;
    }
</style>

<asp:TextBox ID="txtDate" runat="server"></asp:TextBox>