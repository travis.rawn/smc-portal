﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sm_chartjs.ascx.cs" Inherits="assets_controls_sm_chartjs" %>

<style>
    .chart-container {
        margin: 15px;
    }

    .chart-title {
        margin-bottom: 5px;
        font-weight: bolder;
        font-size: x-large;
    }
</style>

<asp:HiddenField ID="hfCanvasID" runat="server" />
<div class="chart-container">
    <asp:Panel ID="pnlChartTitle" runat="server">
         <div class="chart-title">
        <label>
            <asp:Label ID="lblChartTitle" runat="server" /></label>
        <div class="pull-right">
            <button type="button" id="btnToggle" runat="server" visible="false"><i class="fas fa-bars"></i></button>
        </div>
    </div>
    </asp:Panel>
   
    <asp:Panel runat="server" ID="pnlchartSection">
        <div id="divChartInfo" class="card-info" runat="server">
            <em>Information:</em>
            <p>
                <asp:Label ID="lblInformation" runat="server" />
            </p>
        </div>

        <asp:Panel runat="server" ID="pnlChartCanvas" CssClass="chart-canvas">
        </asp:Panel>


    </asp:Panel>
</div>
