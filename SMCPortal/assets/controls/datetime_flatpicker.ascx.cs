﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_controls_datetime_flatpicker : System.Web.UI.UserControl
{



    //Placeholder text for the textbox
    private string _placeholderText;
    public string placeholderText { get { return _placeholderText; } set { _placeholderText = value; } }


    //Enable to disable the control
    private bool _Enabled = true;
    public bool Enabled { get { return _Enabled; } set { _Enabled = value; } }

    //Maxumum width of the textDate textbox
    private int _MaxWidth;
    public int MaxWidth { get { return _MaxWidth; } set { _MaxWidth = value; } } //this is C# 3.0's autoproperty, handles property variables behind scenes.  Was getting recursion stackoverflow exceptions when I dud get{MaxWidth}; set{value = MaxWidth;}

    //Show the time, or date Only.  Date only is default.
    private bool _EnableTime = false;
    public bool EnableTime { get { return _EnableTime; } set { _EnableTime = value; } }


    private DateTime? _SelectedDate = null;
    public DateTime? SelectedDate
    {

        get
        {
            //This is what matters, what we "Get" is what prints on the screen.  But the Datetimepicker will try to interpret the date inthe field.  If what is in the txtDate doesn't conform, the picker errors.
          
            if (!string.IsNullOrEmpty(txtDate.Text))
                _SelectedDate = Convert.ToDateTime(txtDate.Text);
            return _SelectedDate;
            //return DateTime.MinValue;
        }
        set
        {
            _SelectedDate = Convert.ToDateTime(value.ToString());
            txtDate.Text = _SelectedDate.ToString();
        }
    }

    //Formatting dates:  https://flatpickr.js.org/formatting/
    string jsDateTimeFormat = "m/d/Y h:i K";
    string jsDateOnlyFormat = "m/d/Y";


    protected void Page_Load(object sender, EventArgs e)
    {
        txtDate.Enabled = Enabled;
        LoadPicker();
    }

    private void LoadPicker()
    {
        //String variables


        StringBuilder sb = new StringBuilder();
        
        string controlID = GetControlID();
        string options = BuildOptions();
        sb.Append("$(" + controlID + ").flatpickr(" + options + ") \n");
        //Render the script in the DOM
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), sb.ToString(), true);

    }

    private string GetControlID()
    {
        //TextBox txtDate = (TextBox)this.Page.Master.FindControl("txtDate");
        //MasterPage masterPage = (MasterPage)this.Parent.Page.Master;
        //string masterPageClientID = masterPage.ID.Replace('_', '$');
        //string ret = masterPageClientID + "$" + this.ID + "$" + txtDate.ID.ToString();
        return this.ClientID+"_"+txtDate.ID;
       

    }

    private string BuildOptions()
    {

        //string variables
        string enableTime = EnableTime.ToString().ToLower();
        StringBuilder sb = new StringBuilder();
        sb.Append("{enableTime: "+enableTime+", \n ");
        sb.Append("dateFormat: \"" + GetJsDateFormat() + "\", \n ");
        

        //Plugins:
        sb.Append("plugins: [ \n");
        sb.Append("ShortcutButtonsPlugin({ \n");
        sb.Append("button: [ \n");
        sb.Append("{ \n");
        sb.Append(" label: \"Yesterday\" \n");
        sb.Append("}, \n");
        sb.Append("{ \n");
        sb.Append("label: \"Today\" \n");
        sb.Append("}, \n");
        sb.Append("{ \n");
        sb.Append("label: \"Tomorrow\" \n");
        sb.Append("} \n");
        sb.Append("], \n");
        sb.Append(" label: \"or\", \n");
        sb.Append("onClick: (index, fp) => { \n");
        sb.Append("let date; \n");
        sb.Append("switch (index) \n");
        sb.Append("{ \n");
        sb.Append("case 0: \n");
        sb.Append("date = new Date(Date.now() - 24 * 60 * 60 * 1000); \n");
        sb.Append("break; \n");
        sb.Append("case 1: \n");
        sb.Append("date = new Date(); \n");
        sb.Append("break; \n");
        sb.Append("case 2: \n");
        sb.Append("date = new Date(Date.now() + 24 * 60 * 60 * 1000); \n");
        sb.Append("break; \n");
        sb.Append("} \n");
        sb.Append("fp.setDate(date); \n");
        sb.Append("} \n");
        sb.Append("}) \n");
        sb.Append("] \n");




        sb.Append("} \n");
        string ret = sb.ToString();
        return ret;
    }

    private string GetJsDateFormat()
    {
        if (EnableTime)
            return jsDateTimeFormat;
        return jsDateOnlyFormat;
    }
}