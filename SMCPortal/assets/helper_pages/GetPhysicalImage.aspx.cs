﻿using System;
using System.IO;
using System.Web;


public partial class customControls_GetPhysicalImage : System.Web.UI.Page
{

    //Source:  http://www.aspsnippets.com/Articles/Displaying-images-that-are-stored-outside-the-Website-Root-Folder.aspx
    SM_Tools tools = new SM_Tools();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Tools.Strings.SanitizeInput(Request.QueryString["FileName"])))
        {
            try
            {
                // Read the file and convert it to Byte Array
                string basefilepath = Tools.Strings.SanitizeInput(Request.QueryString["FileName"]);
                string filename = Path.GetFileName(basefilepath);
                string filePath = basefilepath.Replace(filename, "");
                string URIPath = filePath.Replace("P:", @"\\SM\Shared_Docs\Part Photos");
                string contenttype = "image/" + Path.GetExtension(filename);
                FileStream fs = new FileStream(URIPath + filename, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();

                //Write the file to response Stream
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = contenttype;
                Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }
    }
}