﻿using System;
using System.Web.Services;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using SensibleDAL;
using SensibleDAL.dbml;

public partial class secure_admin_sandbox_ajax_autocomplete_textbox : System.Web.UI.Page
{

    static RzTools rzt = new RzTools();
    static RzDataContext rdc = new RzDataContext();

    public class ChartData
    {
        public string label { get; set; }
        public string value { get; set; }
        public string color { get; set; }
        public string highlight { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    [WebMethod]
    public static List<string> LoadCountry(string input)
    {
        return GetCountries().FindAll(item => item.ToLower().Contains(input.ToLower()));
    }

    public static List<string> GetCountries()
    {
        List<string> CountryInformation = new List<string>();
        CountryInformation.Add("India");
        CountryInformation.Add("United States");
        CountryInformation.Add("United Kingdom");
        CountryInformation.Add("Canada");
        CountryInformation.Add("South Korea");
        CountryInformation.Add("France");
        CountryInformation.Add("Mexico");
        CountryInformation.Add("Russia");
        CountryInformation.Add("Australia");
        CountryInformation.Add("Turkey");
        CountryInformation.Add("Kenya");
        CountryInformation.Add("New Zealand");
        return CountryInformation;
    }


    [WebMethod]
    public static List<string> LoadCustomers(string input)
    {
        //return rdc.orddet_lines.Where(w => w.orderdate_sales >= new DateTime(2015, 08, 01) && w.customer_name.Length > 0 && w.customer_name.Contains(input)).Select(s => s.customer_name).Distinct().ToList();
        var query = (from o in rdc.ordhed_sales
                     where o.orderdate >= new DateTime(2015, 08, 01)
                     && o.companyname.Contains(input)
                     join c in rdc.companies on o.base_company_uid equals c.unique_id
                     select new
                     {
                         companyname = c.companyname,
                         companyid = c.unique_id
                     });

        return query.Select(s => s.companyname).Distinct().ToList();
    }

    [WebMethod]
    public static List<string> LoadVendors(string input)
    {
        //return rdc.orddet_lines.Where(w => w.orderdate_sales >= new DateTime(2015, 08, 01) && w.customer_name.Length > 0 && w.customer_name.Contains(input)).Select(s => s.customer_name).Distinct().ToList();
        var query = (from o in rdc.ordhed_purchases
                     where o.orderdate >= new DateTime(2015, 08, 01)
                     && o.companyname.Contains(input)
                     join c in rdc.companies on o.base_company_uid equals c.unique_id
                     select new
                     {
                         companyname = c.companyname,
                         companyid = c.unique_id
                     });

        return query.Select(s => s.companyname).OrderBy(o => o).Distinct().ToList();
    }

    [WebMethod]
    public static string LoadCompanies(string input)
    {
        //List<company> companyList = rdc.companies.Where(w => w.companyname.Contains(input)).OrderBy(s => s.companyname).ToList();
        //List<string> companyList2 = companyList.Select(c => c.companyname.ToString() + "," + c.unique_id.ToString()).ToList();
        var companyList = rdc.companies.Where(w => w.companyname.Contains(input)).Select(s => new { value = s.unique_id, label = s.companyname }).OrderBy(s => s.label).ToList();
        string ret = JsonConvert.SerializeObject(companyList);
        return ret;
    }

    [WebMethod]
    public static string LoadContactEmails(string input)
    {
        //List<company> companyList = rdc.companies.Where(w => w.companyname.Contains(input)).OrderBy(s => s.companyname).ToList();
        //List<string> companyList2 = companyList.Select(c => c.companyname.ToString() + "," + c.unique_id.ToString()).ToList();
        var contactList = rdc.companycontacts.Where(w => w.primaryemailaddress.Contains(input)).Select(s => new { value = s.unique_id, label = s.primaryemailaddress }).OrderBy(s => s.label).ToList();
        string ret = JsonConvert.SerializeObject(contactList);
        return ret;
    }
    public class partObject
    {
        public string label { get; set; }
        public string value { get; set; }
    }
    [WebMethod]
    public static string LoadParts(string input)
    {
        //List<company> companyList = rdc.companies.Where(w => w.companyname.Contains(input)).OrderBy(s => s.companyname).ToList();
        //List<string> companyList2 = companyList.Select(c => c.companyname.ToString() + "," + c.unique_id.ToString()).ToList();
        List<partObject> partObjectList = new List<partObject>();

        List<string> partrecordStockTypes = new List<string>() { "Consign", "Stock" };

        var partrecordList = rdc.partrecords.Where(w => partrecordStockTypes.Contains(w.stocktype) && w.fullpartnumber.Contains(input)).Select(s => new partObject { value = s.unique_id, label = s.fullpartnumber }).OrderBy(s => s.label).ToList();
        partObjectList.AddRange(partrecordList);

        var orddetList = rdc.orddet_lines.Where(w => partrecordStockTypes.Contains(w.stocktype) && w.fullpartnumber.Contains(input)).Select(s => new partObject { value = s.unique_id, label = s.fullpartnumber }).OrderBy(s => s.label).ToList();
        partObjectList.AddRange(orddetList);

        string ret = JsonConvert.SerializeObject(partObjectList);
        return ret;
    }

    [WebMethod] //Source: https://codepedia.info/chartjs-asp-net-create-pie-chart-with-database-calling-jquery-ajax-c/
    public static List<ChartData> getAgentReqToQuotePctData()
    {
        List<ChartData> t = new List<ChartData>();
        string[] arrColor = new string[] { "#231F20", "#FFC200", "#F44937", "#16F27E", "#FC9775", "#5A69A6" };

        decimal totalReqs = 0;
        decimal quotedReqs = 0;
        decimal pctReqToQuote = 0;
        List<orddet_quote> quoteList = (from q in rdc.orddet_quotes
                                        join d in rdc.dealheaders on q.base_dealheader_uid equals d.unique_id
                                        join u in rdc.n_users on q.base_mc_user_uid equals u.unique_id
                                        where q.date_created >= new DateTime(2017, 1, 1) && !d.dealheader_name.ToLower().Contains("bom") && (u.main_n_team_uid == "31ce26c99a1644219f7df2ab99cbd63b" && u.name.ToLower() != "house")
                                        select q).ToList();
        if (quoteList != null)
        {

            totalReqs = quoteList.Count();
            quotedReqs = quoteList.Count(w => !string.IsNullOrEmpty(w.base_ordhed_uid));
            pctReqToQuote = quotedReqs / totalReqs;
            pctReqToQuote = pctReqToQuote * 100;
            List<string> reqUserIDs = quoteList.Select(s => s.base_mc_user_uid).Distinct().ToList();
            List<n_user> userList = rdc.n_users.Where(w => reqUserIDs.Contains(w.unique_id)).Distinct().ToList();
            foreach (n_user u in userList)
            {

                decimal totalUserReqs = quoteList.Count(c => c.base_mc_user_uid == u.unique_id);
                decimal totalUserQuotes = quoteList.Count(w => !string.IsNullOrEmpty(w.base_ordhed_uid) && w.base_mc_user_uid == u.unique_id);


                ChartData cData = new ChartData();
                cData.label = u.name;
                cData.value = ((totalUserQuotes / totalUserReqs) * 100).ToString("#.##");
                cData.color = arrColor.OrderBy(x => Guid.NewGuid()).FirstOrDefault();
                t.Add(cData);
            }
        }
        return t.OrderByDescending(o => Convert.ToDecimal(o.value)).ToList();
    }
}
