﻿<%@ Page Title="Portal Login" Language="C#" MasterPageFile="~/hs_flex.Master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="Account_Login" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="HeadContent">

    <style>
        .forgot-password {
            display: inline-block;
            font-style: italic;
            font-size: 14px;
            padding-left: 12px;
            float: right;
            color: #EE2738
        }

        .hs-error-msgs label {
            font-size: 16px;
        }

        .register-link {
            display: inline-block;
            font-style: italic;
            font-size: 14px;
            padding-left: 12px;
            float: right;
            color: #181E1D;
        }

        .remember-me-cbx {
            padding-left: 0px;
        }

            .remember-me-cbx li {
                list-style-type: none;
                margin-top: 10px;
            }
    </style>
      
    <script charset="utf-8" type="text/javascript" src="https://js.hsforms.net/forms/v2.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BannerContent" runat="server">

    <h1 class="relative white">Customer Portal</h1>

</asp:Content>




<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">






    <%--start dual column wrapper--%>
    <div id="hs_cos_wrapper_widget_1550858841443" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
        <div id="section1" class="dual-column__wrapper">
            <div class="dual-column__container row__align--center section-container padding-top__small padding-bottom__large">
                <!-- start of  of column1 -->
                <div class="col6 content__container">
                    <h3 class="">Login</h3>
                    <div class="supporting-content__container supporting-content-bar__container">
                        <div class="supporting-content supporting-content-bar  icon-list__container icon-list__checkmark">
                            <p>&nbsp;The Sensible Micro portal is free for our customers.  In the customer portal, you have access to:</p>
                            <ul>
                                <li>Your orders</li>
                                <li>Tracking numbers and status updates</li>
                                <li>Quality inspection &amp; lab reports</li>
                                <li><span>Component intelligence such as Datasheet, Product Change Notifications, Lifecycle information and Environmental Data</span></li>
                                <li>Unlimited access to our part search</li>
                            </ul>
                            <p>&nbsp;</p>
                        </div>
                    </div>




                </div>
                <!-- end of column1 -->
                <!-- start of column2 -->

                <div class="col6 content__container">



                    <div class="form__container form__container-boxed">
                        <span id="hs_cos_wrapper_widget_1550858841443_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_form" style="" data-hs-cos-general-type="widget" data-hs-cos-type="form">
                            <h3 id="hs_cos_wrapper_form_413081977_title" class="hs_cos_wrapper form-title" data-hs-cos-general-type="widget_field" data-hs-cos-type="text"></h3>

                            <div id="hs_form_target_form_413081977">
                                <div novalidate="" accept-charset="UTF-8" action="https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/1878634/229380ec-e432-42a0-9edc-ea09c3e94af3" enctype="multipart/form-data" id="hsForm_229380ec-e432-42a0-9edc-ea09c3e94af3_4143" method="POST" class="hs-form stacked hs-custom-form hs-form-private hs-form-229380ec-e432-42a0-9edc-ea09c3e94af3_ad825b9b-0735-48ed-82c9-f2a20deef3ca" data-form-id="229380ec-e432-42a0-9edc-ea09c3e94af3" data-portal-id="1878634" target="target_iframe_229380ec-e432-42a0-9edc-ea09c3e94af3_4143" data-reactid=".hbspt-forms-1">

                                    <%--Error--%>
                                    <noscript data-reactid=".hbspt-forms-1.2"></noscript>
                                    <div class="hs_error_rollup" data-reactid=".hbspt-forms-1.4">
                                        <ul class="no-list hs-error-msgs inputs-list" style="display: block;" data-reactid=".hbspt-forms-1.4.0">
                                            <li data-reactid=".hbspt-forms-1.4.0.0">
                                                <%--<label class="hs-main-font-element" data-reactid=".hbspt-forms-1.4.0.0.0">Please change your email address to continue.</label>--%>
                                                <label class="hs-main-font-element" data-reactid=".hbspt-forms-1.4.0.0.0" id="lblError" runat="server"></label>
                                            </li>
                                        </ul>
                                    </div>


                                    <%--UserName--%>
                                    <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$0.$email">
                                        <label id="label-email-229380ec-e432-42a0-9edc-ea09c3e94af3_5096" class="" placeholder="Enter your User name / email:" for="email-229380ec-e432-42a0-9edc-ea09c3e94af3_5096" data-reactid=".hbspt-forms-1.1:$0.$email.0"><span data-reactid=".hbspt-forms-1.1:$0.$email.0.0">User name / email:</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$0.$email.1"></legend>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$0.$email.$email">
                                            <asp:TextBox runat="server" ID="txtUserName" ClientID="email-229380ec-e432-42a0-9edc-ea09c3e94af3_4938" class="hs-input" placeholder="" value="" autocomplete="off" data-reactid=".hbspt-forms-1.1:$0.$email.$email.0" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />
                                        </div>
                                    </div>

                                    <%--Password--%>
                                    <div class="hs_password hs-password hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-1.1:$1">
                                        <label id="label-password-229380ec-e432-42a0-9edc-ea09c3e94af3_5096" class="" placeholder="Enter your Password" for="password-229380ec-e432-42a0-9edc-ea09c3e94af3_5096" data-reactid=".hbspt-forms-1.1:$1.0"><span data-reactid=".hbspt-forms-1.1:$1.0.0">Password</span></label><legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$1.1"></legend>
                                        <div class="input" data-reactid=".hbspt-forms-1.1:$1.$password">
                                            <%--<input id="password-229380ec-e432-42a0-9edc-ea09c3e94af3_5096" class="hs-input" type="text" name="password" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$1.$password.0" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;"></div>--%>
                                            <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" ClientID="password-229380ec-e432-42a0-9edc-ea09c3e94af3_4938" class="hs-input" type="text" name="password" value="" placeholder="" data-reactid=".hbspt-forms-1.1:$1.$password.0" autocomplete="off" Style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" />
                                        </div>
                                        <%--Remember Me--%>
                                        <div class="hs_remember_me hs-remember_me hs-fieldtype-booleancheckbox field hs-form-field" data-reactid=".hbspt-forms-1.1:$2">
                                            <legend class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.1:$2.1"></legend>
                                            <div class="input" data-reactid=".hbspt-forms-1.1:$2.$remember_me">
                                                <ul class="inputs-list remember-me-cbx" data-reactid=".hbspt-forms-1.1:$2.$remember_me.0" style="padding-left: 0px;">
                                                    <li class="hs-form-booleancheckbox" data-reactid=".hbspt-forms-1.1:$2.$remember_me.0.0">
                                                        <label for="remember_me-229380ec-e432-42a0-9edc-ea09c3e94af3_5096" class="hs-form-booleancheckbox-display" data-reactid=".hbspt-forms-1.1:$2.$remember_me.0.0.0">
                                                            <input id="chkRememberMe" class="hs-input" type="checkbox" name="remember_me" value="true" data-reactid=".hbspt-forms-1.1:$2.$remember_me.0.0.0.0" runat="server">
                                                            <span>Remember Me</span>
                                                            <asp:LinkButton ID="lbRegister" runat="server" PostBackUrl="/Account/register.aspx" CssClass="register-link">Register</asp:LinkButton>
                                                            <asp:LinkButton ID="lbForgotPassword" runat="server" PostBackUrl="/Account/password_reset.aspx" CssClass="forgot-password">Forgot password?</asp:LinkButton>
                                                        </label>

                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>

                                                </ul>
                                            </div>

                                        </div>
                                        <%--Button--%>
                                        <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-1.5">
                                            <div class="hs-field-desc" style="display: none;" data-reactid=".hbspt-forms-1.5.0"></div>
                                            <div class="actions" data-reactid=".hbspt-forms-1.5.1">
                                                <%--<input type="submit" value="Login" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0">--%>
                                                <asp:Button ID="btnLogin" runat="server" class="hs-button primary large" data-reactid=".hbspt-forms-1.5.1.0" OnClick="btnLogin_Click" Text="Login" />
                                            </div>
                                        </div>


                                    </div>
                                </div>
                        </span>
                    </div>



                </div>
                <!-- end of column2 -->

            </div>
        </div>
    </div>



</asp:Content>





