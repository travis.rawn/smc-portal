﻿using System;
using System.Web.Security;
using System.Web.UI;


public partial class Public : MasterPage
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        SM_Security.CheckForBlockedIP();


        //Check Validation Status       
        //CurrentUser = SM_Security.LoadCurrentUser();
        SM_Security.HandleUserSecurityValidation();

        if (Request.IsAuthenticated)
        {
            lblLogin.Text = "LOGOUT";
        }
        else
        {
            lblLogin.Text = "REGISTER/LOGIN";
        }
    }

    protected void lbLogin_Click(object sender, EventArgs e)
    {
        if (Request.IsAuthenticated)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
        }
        Response.Redirect(FormsAuthentication.LoginUrl, false);
    }


    public string GetTawkName()
    {
        string ret = "";
        if (Profile != null)//is there a profile loaded?
        {
            //does the loaded profile have a firstname?
            if (!string.IsNullOrEmpty(Profile.FirstName))
                ret = Profile.FirstName;
            //does the profile have a last name?
            if (!string.IsNullOrEmpty(Profile.LastName))
                ret += " " + Profile.LastName;
        }
        if (string.IsNullOrEmpty(ret))// if profile, but somehow no string data, set as email
            ret = GetEmail();// if no profile, let's at least identify by email
        return ret;
    }

    public string GetEmail()
    {
        string ret = "";
        if (Membership.GetUser() != null)
            ret = Membership.GetUser().Email;
        return ret;
    }

}
